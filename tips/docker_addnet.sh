#/bin/bash

if [ -z $1 ] || [ -z $2 ] || [ -z $3 ] || [ -z $4 ] || [ -z $5 ];
then
        echo "*****Input the necessary parameters: CONTAINERID IP MASK GATEWAY IFID"
        echo "*****Call the script like: shi docker_addnet.sh  b0e18b6a4432 192.168.5.123 24 192.168.5.1 1"
        exit
fi
  
CONTAINERID=$1
SETIP=$2
SETMASK=$3
GATEWAY=$4
IFID=$5
 
#判断宿主机网卡是否存在
ifconfig deth$IFID > /dev/null 2>&1
if [ $? -eq 0 ]; then
    read -p "deth$IFID exist,do you want delelte it? y/n " del
    if [[ $del == 'y' ]]; then
    ip link del deth$IFID
    else
    exit
    fi
fi
#
pid=`docker inspect -f '{{.State.Pid}}' $CONTAINERID`
mkdir -p /var/run/netns
find -L /var/run/netns -type l -delete
 
if [ -f /var/run/netns/$pid ]; then
    rm -f /var/run/netns/$pid
fi
ln -s /proc/$pid/ns/net /var/run/netns/$pid
#
ip link add deth$IFID type veth peer name B
brctl addif br-eno$IFID deth$IFID
ip link set deth$IFID up
ip link set B netns $pid
#先删除容器内已存在的eth0
ip netns exec $pid ip link del eth0 > /dev/null 2>&1
#设置容器新的网卡eth0
ip netns exec $pid ip link set dev B name eth0
ip netns exec $pid ip link set eth0 up
ip netns exec $pid ip addr add $SETIP/$SETMASK dev eth0
ip netns exec $pid ip route add default via $GATEWAY
