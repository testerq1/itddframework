#!/bin/bash

arp -s 2.2.2.2 50:e5:49:83:24:83 dev eth3
arp -s 1.1.1.1 40:16:9f:f4:05:e5 dev eth2

iptables -t nat -A POSTROUTING -d 2.2.2.2 -j SNAT --to-source 1.1.1.1
iptables -t nat -A PREROUTING -d 2.2.2.2 -j DNAT --to-destination 192.168.101.1
iptables -t nat -A POSTROUTING -d 1.1.1.1 -j SNAT --to-source 2.2.2.2
iptables -t nat -A PREROUTING -d 1.1.1.1 -j DNAT --to-destination 192.168.100.1

ip route add 1.1.1.1/32 dev eth2
ip route add 2.2.2.2/32 dev eth3

chkconfig iptables off
service iptables save
service iptables restart

echo "arp -s 2.2.2.2 50:e5:49:83:24:83 dev eth3" >>/etc/profile
echo "arp -s 1.1.1.1 40:16:9f:f4:05:e5 dev eth2" >>/etc/profile
echo "ip route add 1.1.1.1/32 dev eth2" >>/etc/profile
echo "ip route add 2.2.2.2/32 dev eth3" >>/etc/profile
