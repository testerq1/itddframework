package ate.ua.ldap;

import org.testng.annotations.Test;

import ate.testcase.UATestcase;

public class LdapUATest extends UATestcase{

	@Test
	public void conn(){
		LdapUA ua=this.create_ua("ldap://dc=com:secret1@172.16.5.118",new LdapUA());
		ua.startup();		
		assertTrue(ua.is_ready());
		
		assertTrue(ua.delete("dc=com"));		
		assertFalse(ua.is_exist("dc=com"));
		assertFalse(ua.is_exist("dc=sohu,dc=com"));
		
		assertTrue(ua.createDC("", "com"));		
		assertTrue(ua.createDC("dc=com", "sohu"));
		assertTrue(ua.is_exist("dc=com"));
		assertTrue(ua.is_exist("dc=sohu,dc=com"));
	}
	
	public void basic(){
		LdapUA ua=this.create_ua("ldap://cn=Manager,dc=my-domain,dc=com:secret1@172.16.5.118",new LdapUA());
		assertTrue("cn=Manager,dc=my-domain,dc=com".equals(ua.get_uname()),ua.get_uname());
		assertTrue("secret".equals(ua.get_passwd()),ua.get_passwd());		
	}
}
