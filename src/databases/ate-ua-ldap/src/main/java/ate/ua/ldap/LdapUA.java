package ate.ua.ldap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.DeleteRequest;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.LDAPResult;
import com.unboundid.ldap.sdk.LDAPSearchException;
import com.unboundid.ldap.sdk.Modification;
import com.unboundid.ldap.sdk.ModificationType;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
import com.unboundid.ldap.sdk.controls.SubentriesRequestControl;
import com.unboundid.ldap.sdk.controls.SubtreeDeleteRequestControl;

import ate.AbstractURIHandler;
import ate.ua.AbstractCrudImp;
import ate.ua.ICrudUA;
import ate.ua.RunFailureException;
import ate.ua.UAOption;

/**
 * ldap://cn=manager,dc=com:passwd@172.16.5.118:389<br>
 * 
 * DC (Domain Component)<br>
 * CN (Common Name) <br>
 * OU (Organizational Unit) <br>
 * DC=redmond,DC=wa,DC=microsoft,DC=com <br>
 * 如果我们类比文件系统的话，可被看作如下文件路径:<br>
 * Com\Microsoft\Wa\Redmond<br>
 * 例如：CN=test,OU=developer,DC=domainname,DC=com<br>
 * cn=test可能代表一个用户名， ou=developer可代表一个 组织单位<br>
 * 
 * @author ravi huang(ravi.huang@gmail.com)
 * 
 */
public class LdapUA extends AbstractURIHandler {
	LDAPConnection connection;
	private int OPERATION_TIMEOUT_MILLIS = 1000;

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			if (this.port <= 0)
				port = 389;
		}
		return this;
	}

	/** 创建DC */
	public boolean createDC(String baseDN, String dc) {
		String entryDN = "dc=" + dc;
		if (baseDN != null && baseDN.length() > 0)
			entryDN += "," + baseDN;

		try {
			// SearchResultEntry entry = connection.getEntry(entryDN);

			ArrayList<Attribute> attributes = new ArrayList<Attribute>();
			attributes.add(new Attribute("objectClass", "top", "organization",
					"dcObject"));
			attributes.add(new Attribute("dc", dc));
			attributes.add(new Attribute("o", dc));
			LDAPResult rst = connection.add(entryDN, attributes);
			// log.info(rst.getResultCode().getName());
			// log.info(rst.getResultCode().intValue()+"");

			return rst.getResultCode() == ResultCode.SUCCESS;

		} catch (Exception e) {
			log.warn("创建 {} 出现错误：{}", entryDN, e.getMessage());
		}
		return false;
	}

	/** 创建组织 */
	public boolean createO(String baseDN, String o) {
		String entryDN = "o=" + o + "," + baseDN;
		try {
			ArrayList<Attribute> attributes = new ArrayList<Attribute>();
			attributes.add(new Attribute("objectClass", "top", "organization"));
			attributes.add(new Attribute("o", o));
			LDAPResult rst = connection.add(entryDN, attributes);
			return rst.getResultCode() == ResultCode.SUCCESS;
		} catch (Exception e) {
			log.warn("创建 {} 出现错误：{}", entryDN, e.getMessage());
		}
		return false;
	}

	/** 创建组织单元 */
	public boolean createOU(String baseDN, String ou) {
		String entryDN = "ou=" + ou + "," + baseDN;
		try {
			ArrayList<Attribute> attributes = new ArrayList<Attribute>();
			attributes.add(new Attribute("objectClass", "top",
					"organizationalUnit"));
			attributes.add(new Attribute("ou", ou));
			LDAPResult rst = connection.add(entryDN, attributes);
			return rst.getResultCode() == ResultCode.SUCCESS;

		} catch (Exception e) {
			log.warn("创建 {} 出现错误：{}", entryDN, e.getMessage());
		}
		return false;
	}

	/**
	 * 注意：在openldap下测试没有成功
	 * @param requestDN
	 * @return
	 */
	public boolean deleteSubTree(String entry) {
		ResultCode rc = null;
		try {
			DeleteRequest deleteRequest = new DeleteRequest(entry);
			deleteRequest.addControl(new SubtreeDeleteRequestControl());

			rc = connection.delete(deleteRequest).getResultCode();
		} catch (LDAPException e) {
			e.printStackTrace();
			rc = e.getResultCode();
			log.warn("删除 {} 出现错误：{}", entry, e.getMessage());
		}
		if (rc == ResultCode.NO_SUCH_OBJECT || rc == ResultCode.SUCCESS)
			return true;
		return false;
	}

	public boolean delete(String entry) {
		SearchRequest searchRequest = null;
		String filter = "objectClass=*";
		SearchResult searchResult;
		try {
			searchRequest = new SearchRequest(entry, SearchScope.ONE, filter);
			searchResult = connection.search(searchRequest);

			if (searchResult.getEntryCount() == 0) {
				deleteEntry(entry);
			} else {
				for (SearchResultEntry entryResult : searchResult
						.getSearchEntries()) {
					if (!entryResult.getDN().equalsIgnoreCase(entry)) {
						//迅速失败
						if (!delete(entryResult.getDN()))
							return false;
					}
				}
				deleteEntry(entry);
			}
			return true;

		} catch (LDAPSearchException e) {
			ResultCode rc = e.getResultCode();
			if (rc == ResultCode.NO_SUCH_OBJECT)
				return true;
			e.printStackTrace();
		} catch (LDAPException e) {
			e.printStackTrace();
		}
		return false;
	}

	private void deleteEntry(String entry) throws LDAPException {
		DeleteRequest deleteRequest = new DeleteRequest(entry);
		LDAPResult ldapResult = null;
		ldapResult = connection.delete(deleteRequest);
	}

	@Override
	public String get_default_schema() {
		return "ldap";
	}

	public boolean is_exist(String entryDN) {
		SearchResultEntry tmp;
		try {
			tmp = connection.getEntry(entryDN);
			return tmp != null;
		} catch (LDAPException e) {
			log.warn("Get Entry Failed:" + entryDN, e);
		}
		return false;
	}

	@Override
	public boolean is_ready() {
		return connection != null && connection.isConnected();
	}

	public void modify(String requestDN, Map<String, String> data) {
		try {
			// 修改信息
			ArrayList<Modification> md = new ArrayList<Modification>();
			for (String key : data.keySet()) {
				md.add(new Modification(ModificationType.REPLACE, key, data
						.get(key)));
			}
			connection.modify(requestDN, md);

			log.debug("修改用户信息成！");
		} catch (Exception e) {
			throw new RunFailureException("修改用户信息出现错误：\n" + e.getMessage());
		}
	}

	/** 查询 */
	public SearchResult query(String searchDN, String filter) {
		try {
			SearchRequest searchRequest = new SearchRequest(searchDN,
					SearchScope.SUB, "(" + filter + ")");
			searchRequest.addControl(new SubentriesRequestControl());
			SearchResult searchResult = connection.search(searchRequest);
			log.debug("共查询到" + searchResult.getSearchEntries().size() + "条记录");
			if (log.isDebugEnabled()) {
				int index = 1;
				for (SearchResultEntry entry : searchResult.getSearchEntries()) {
					log.debug((index++) + "\t" + entry.getDN());
				}
			}
			return searchResult;
		} catch (Exception e) {
			throw new RunFailureException("查询错误，错误信息如下：\n" + e.getMessage());
		}
	}

	@Override
	public void startup() {
		if (connection == null) {
			LDAPConnectionOptions connectionOptions = new LDAPConnectionOptions();
			connectionOptions.setAbandonOnTimeout(true);
			connectionOptions.setConnectTimeoutMillis(OPERATION_TIMEOUT_MILLIS);

			try {
				if (username != null && username.length() > 0)
					connection = new LDAPConnection(host, port, username,
							passwd);
				else
					connection = new LDAPConnection(host, port);

			} catch (Exception e) {
				throw new Error("连接LDAP出现错误：\n" + e.getMessage());
			}
		}
	}

	@Override
	public void teardown() {
		if (connection != null)
			connection.close();

	}

}
