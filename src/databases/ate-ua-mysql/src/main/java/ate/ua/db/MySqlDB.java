package ate.ua.db;

/*
 * #%L
 * ate-ua-mysql
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.util.List;

import ate.AbstractURIHandler;
import ate.rm.dev.Host;
import ate.ua.AbstractCrudImp;
import ate.ua.UAOption;

import com.mysql.jdbc.JDBC4Connection;

/**
 * 
 * @author ravi huang
 * 
 */
public class MySqlDB extends AbstractCrudImp {
	String cmd_prefix="mysql -s --skip-column-names";
	//mysql socket
	String socket;
		
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			if (this.username != null)
				cmd_prefix += " -u" + this.username;
			if(this.passwd!=null&&this.passwd.length()>0)
				cmd_prefix += " -p" + this.passwd;		
			if(port<=0)
				port=3306;			
		}
		return this;
	}
	
	@Override
	public String capabilities() {
		StringBuffer buf = new StringBuffer(super.capabilities());
		buf.append("mysql: " + (runtime.which("mysql") == null ? "no" : "yes")).append("\r\n");

		return buf.toString();
	}
	
	@Override
	public String get_default_schema() {
		return "mysql";
	}

	@Override
	public int get_port() {		
		return port;
	}

	@Override
	protected String getDriver() {
		return "com.mysql.jdbc.Driver";
	}

	@Override
	protected String getJDBCUri() {
		return "jdbc:mysql://" + this.get_host() + ":" + this.get_port() + this.get_path();
	}

	@Override
	public List<String> getSchemas() {
		return this.getDatabases();
	}
	
	/**
	 * taobao oradba tools<br>
	 * svn: http://code.taobao.org/svn/orzdba/trunk<br>
	 * <br>
	 * dep:<br>
	 * tcprstat:http://www.percona.com/docs/wiki/tcprstat:start <br>
	 * 	$ wget http://github.com/downloads/Lowercases/tcprstat/tcprstat-static.v0.3.1.x86_64<br>
	 *  $ mv tcprstat-static.v0.3.1.x86_64 /usr/bin/tcprstat<br>
	 *  $ chmod +x /usr/bin/tcprstat<br>
	 * 
	 * @param paras
	 * @return
	 */
	public String orzdba(String paras){
		if(paras==null)
			return runtime.exec0("orzdba -lazy -d sda -C 5 -i 2");
		else 
			return runtime.exec0("orzdba " + paras);
	}
	/**
	 * tps and qps
	 */
	public void qtps(){
		String tmp="";
		//(1)QPS(每秒Query量)
		//QPS = Questions(or Queries) / seconds
		show("global  status like 'Question%'");
		//(2)TPS(每秒事务量)
		//TPS = (Com_commit + Com_rollback) / seconds
		show("global status like 'Com_commit'");
		show("global status like 'Com_rollback'");
		//(3)key Buffer 命中率
		show("global   status  like   'key%'");
		//key_buffer_read_hits = (1-key_reads / key_read_requests) * 100%
		//key_buffer_write_hits = (1-key_writes / key_write_requests) * 100%
		//(4)InnoDB Buffer命中率
		show("status like 'innodb_buffer_pool_read%'");
		//innodb_buffer_read_hits = (1 - innodb_buffer_pool_reads / innodb_buffer_pool_read_requests) * 100%
		//(5)Query Cache命中率
		show("status like 'Qcache%'");
		//Query_cache_hits = (Qcahce_hits / (Qcache_hits + Qcache_inserts )) * 100%;
		//(6)Table Cache状态量
		show("global  status like 'open%'");
		//比较 open_tables  与 opend_tables 值
		//(7)Thread Cache 命中率
		show("global status like 'Thread%'");
		show("global status like 'Connections'");
		//Thread_cache_hits = (1 - Threads_created / connections ) * 100%
		//(8)锁定状态
		show("global  status like '%lock%'");
		//Table_locks_waited/Table_locks_immediate=0.3%  如果这个比值比较大的话，说明表锁造成的阻塞比较严重
		//Innodb_row_lock_waits innodb行锁，太大可能是间隙锁造成的
		//(9)复制延时量
		show("slave status");
		//查看延时时间
		//(10) Tmp Table 状况(临时表状况)
		show("status like 'Create_tmp%'");
		//Created_tmp_disk_tables/Created_tmp_tables比值最好不要超过10%，如果Created_tmp_tables值比较大，
		//可能是排序句子过多或者是连接句子不够优化
		//(11) Binlog Cache 使用状况
		show("status like 'Binlog_cache%'");
		//如果Binlog_cache_disk_use值不为0 ，可能需要调大 binlog_cache_size大小
		//(12) Innodb_log_waits 量
		show("status like 'innodb_log_waits'");
		//Innodb_log_waits值不等于0的话，表明 innodb log  buffer 因为空间不足而等待
		
	}

	/**
	 * /mysql_reset_passwd
	 * UPDATE mysql.user SET Password=null WHERE User='root';
	 * FLUSH PRIVILEGES;
	 * 
	 * reset passwd:
	 * $ mysqld_safe --init-file=mysql_reset_passwd
	 * 
	 * connect to xampp:
	 * $ mysql -uroot --socket /opt/lampp/var/mysql/mysql.sock -p
	 */
	public String reset_passwd(){
		return runtime.exec0("mysqld_safe --init-file="+Host.HOME+File.separator+"conf"+
				File.separator+"mysql_reset_passwd &");
	}

	public void set_socket(String socket){
		if(socket!=null){
			this.socket=socket;
			this.cmd_prefix+=" --socket "+socket;
		}
	}
	
	public String show(String cmd){
		String tmp=runtime.exec0(cmd_prefix+String.format(" -e 'show %s'",cmd));
		log.debug("show {} \n {}",cmd,tmp);
		return tmp;
	}
	
	public String show_engine_status(String engine) {
		return show(engine+" status");
	}
	
	/**
	 * $ mysqladmin extended-status
	 * @return
	 */
	public String show_extended_status(){
		return runtime.exec0("mysqladmin extended-status");
	}
	
	public String show_process() {
		return show("processlist");
	}
	
	public String show_status() {
		return show("status");
	}
	/**
	 * $ mysql -uroot -p -e 'show variables'
	 * @return
	 */
	public String show_variables() {
		return show("variables");
	}
	
	/**
	 * 
	 * $ yum install mysql-bench perl-DBD-mysql
	 * $ /usr/share/sql-bench/run-all-tests --user=root --socket='/opt/lampp/var/mysql/mysql.sock'
	 * 
	 * PS: set user root's passwd to null
	 * @return
	 */
	public String sql_bench(){
		return runtime.exec0("/usr/share/sql-bench/run-all-tests --user=root");		
	}
	
	@Override
	public void startup() {
		super.startup();
		//由于使能了Groovy Sql功能，因此con也可能不会被初始化，而使用sql句柄
		if(con!=null){
			JDBC4Connection conn = (JDBC4Connection) this.con;
			// batch insert时，打开这个开关可以大幅提高性能
			conn.setRewriteBatchedStatements(true);
		}
	}
	public String uptime(){
		return show("GLOBAL STATUS LIKE 'Uptime'");
	}
	
	public void useDB(String sName) {
		update("database " + sName);
	}
	
	/**
	 * $ sysbench –test=oltp –mysql-table-engine=myisam –mysql-db=test –oltp-table-size=1000000 –mysql-host=localhost –mysql-user=root –mysql-password=123456 –mysql-socket=/tmp/mysql.sock prepare 
	 * 以上是为基准测试做准备，它会在指定的test数据库创建一个sbtest表，然后自动插入1000000条数据，下面开始运行测试性能。
	 * 
	 * 以请求为1000进行10000条数据的事务操作:
	 * $ sysbench –max-requests=1000 –test=oltp –oltp-table-size=10000 –mysql-db=test –mysql-host=localhost –mysql-user=root –mysql-password=123456 –mysql-socket=/tmp/mysql.sock –oltp-read-only run
	 *  
	 *  该函数是空的
	 */
	public void sysbench_oltp(){
		//nothing
	}
}
