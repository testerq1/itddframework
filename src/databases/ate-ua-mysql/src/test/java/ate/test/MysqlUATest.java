package ate.test;

/*
 * #%L
 * ate-ua-mysql
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.testng.annotations.Test;

import ate.testcase.CsUATestcase;
import ate.ua.db.MySqlDB;

public class MysqlUATest extends CsUATestcase{
	String tsip=arg("MYSQL.IP");
	/**
	 * CREATE USER 'root'@'%' IDENTIFIED BY '123456'; GRANT ALL PRIVILEGES ON
	 * *.* TO 'root'@'%' WITH GRANT OPTION;
	 * 
	 * @throws SQLException
	 */
	@Test
	public void create() throws SQLException {
		String create_table= "create table IF NOT EXISTS test.MyEmployees ( "
			      + "   id INT PRIMARY KEY, firstName VARCHAR(20), lastName VARCHAR(20), "
			      + "   title VARCHAR(20), salary INT )";
		
		MySqlDB db = create_ua("mysql://root:123456@"+tsip+":13361");
		db.startup();
		assertTrue(db.update("drop database if exists test") > -1);

		assertTrue(db.update("create database IF NOT EXISTS test") > -1);
		List<String> rs = db.getDatabases();
		assertTrue(rs.contains("mysql"));
		assertTrue(rs.contains("test"));
		List<String> rs1 = db.getSchemas();
		assertTrue(rs.equals(rs1));
		rs1=db.getTables("mysql");
		assertTrue(rs1.contains("user"));
		db.useDB("test");
		
		assertTrue(db.update("drop table if exists MyEmployees") > -1);
		
		assertTrue(db.update(create_table) > -1);
		
		db.update("insert into MyEmployees(id, firstName) values(100, 'A')");
		
		db.update("delete from MyEmployees");		
		
		PreparedStatement prest=db.prepareStatement("insert into MyEmployees(id, firstName) values(?, ?)");
		//db.setAutoCommit(false);
		long a=now();
		int ysize=10;
		
		for(int x = 0; x < ysize; x++){
			for(int y=0;y<ysize;y++){
	            prest.setInt(1, x+y);        
	            prest.setString(2, "abc"+random(0,x+y));        
	            prest.addBatch();	            
			}
			x+=ysize-1;
			prest.executeBatch();
			db.commit();
        }
		
        long cnt=db.countTable("MyEmployees");
        log.info("MySql insert {} row in {}ms", cnt,(now()-a));
        assertTrue(cnt==ysize);
        
        ResultSet rset=db.query("select * from MyEmployees limit 2");
        while(rset.next()){        
        	log.info(rset.getInt(1)+"");
        	log.info(rset.getString(2));
        }        	
	}
}
