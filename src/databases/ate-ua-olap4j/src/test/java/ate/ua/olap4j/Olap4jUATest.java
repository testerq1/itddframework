package ate.ua.olap4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Arrays;

import org.olap4j.AllocationPolicy;
import org.olap4j.Axis;
import org.olap4j.CellSet;
import org.olap4j.OlapConnection;
import org.olap4j.Scenario;
import org.olap4j.mdx.IdentifierNode;
import org.olap4j.metadata.Cube;
import org.olap4j.metadata.Member;
import org.olap4j.query.Query;
import org.olap4j.query.QueryDimension;
import org.olap4j.query.Selection;
import org.olap4j.query.SortOrder;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.testcase.UATestcase;

public class Olap4jUATest extends UATestcase {
	Olap4jUA ua;

	@BeforeClass
	public void beforeclass() {
		// + "JdbcDrivers=com.mysql.jdbc.Driver;"
		// +
		// "Jdbc=jdbc:mysql://localhost/foodmart?user=foodmart&password=foodmart;"

		ua = this
				.create_ua(
						"olap4j:Jdbc=jdbc:hsqldb:file:src/test/foodmart/foodmart;Catalog=file:src/test/foodmart/FoodMart.xml;",
						new Olap4jUA());
		ua.startup();
		ua.show_conn_info();
	}

	@Test
	public void QueryDemo() throws Exception {
		log.info("QueryDemo:");
		String query = "SELECT { Except( {[Product].[All Products].[Drink].[Beverages].Children}, {[Product].[All Products].[Drink].[Beverages].[Carbonated Beverages]} ) } ON COLUMNS, { [Store].[All Stores].[USA].Children } ON ROWS FROM [Sales] WHERE ([Time].[1997])";

		assertTrue(null != ua.parser(query));

		assertTrue(ua.validate(query));

		CellSet cell = ua.query(query);

		assertTrue(cell != null);

		ua.print(cell);
	}
	@Test
	public void QueryModelDemo() throws Exception {
		log.info("QueryModelDemo:");
		// Get a cube object.
		Cube salesCube = ua.olap().getOlapSchema().getCubes().get("Sales");

		// Build a query object.
		Query myQuery = new Query("myQuery", salesCube);

		// Lookup some dimensions
		QueryDimension productDim = myQuery.getDimension("Product");
		QueryDimension storeDim = myQuery.getDimension("Store");
		QueryDimension timeDim = myQuery.getDimension("Time");

		// Place dimensions on some axis
		myQuery.getAxis(Axis.COLUMNS).addDimension(productDim);
		myQuery.getAxis(Axis.ROWS).addDimension(storeDim);
		myQuery.getAxis(Axis.FILTER).addDimension(timeDim);

		// Including a member by metadata
		Member year1997 = salesCube.lookupMember(IdentifierNode.ofNames("Time",
				"1997").getSegmentList());
		timeDim.include(year1997);

		// Including a member by name parts
		productDim.include(Selection.Operator.CHILDREN,
				IdentifierNode.ofNames("Product", "Drink", "Beverages")
						.getSegmentList());

		// We can also exclude members
		productDim.exclude(IdentifierNode.ofNames("Product", "Drink",
				"Beverages", "Carbonated Beverages").getSegmentList());

		myQuery.validate();
		
		CellSet cell =myQuery.execute();
		assertTrue(cell != null);
		ua.print(cell);
		
	}
	@Test
	public void QueryModelSortingDemo() throws Exception {
		log.info("QueryModelSortingDemo:");
		Cube salesCube = ua.olap().getOlapSchema().getCubes().get("Sales");

		// Build a query object.
		Query myQuery = new Query("myQuery", salesCube);

		// Place some dimensions on the axis
		myQuery.getAxis(Axis.ROWS)
				.addDimension(myQuery.getDimension("Product"));

		myQuery.getAxis(Axis.ROWS).addDimension(
				myQuery.getDimension("Store Type"));

		myQuery.getAxis(Axis.COLUMNS)
				.addDimension(myQuery.getDimension("Time"));

		// Include and exclude members
		myQuery.getDimension("Store Type").include(
				Selection.Operator.CHILDREN,
				IdentifierNode.ofNames("Store Type", "All Store Types")
						.getSegmentList());

		myQuery.getDimension("Product").include(
				IdentifierNode.ofNames("Product", "Food", "Seafood")
						.getSegmentList());

		myQuery.getDimension("Product").include(
				IdentifierNode.ofNames("Product", "Food", "Meat")
						.getSegmentList());

		// SORT!!!
		myQuery.getAxis(Axis.ROWS).sort(SortOrder.BDESC);
		myQuery.validate();

		CellSet cell =myQuery.execute();
		assertTrue(cell != null);
		ua.print(cell);
	}
	@Test
	public void ScenarioDemo() throws Exception {
		log.info("ScenarioDemo:");
		// Create a scenario
		Scenario scenario = ua.olap().createScenario();

		// Activate it.
		ua.olap().setScenario(scenario);

		// Build a query against that scenario
		String query = "SELECT { [Product].[All Products].[Drink].[Beverages].Children } ON COLUMNS, "
				+ "{ [Store].[All Stores].[USA], [Store].[All Stores].[USA].Children } ON ROWS FROM [Sales] WHERE ([Time].[1997], "
				+ "[Scenario].[" + scenario.getId() + "])";
		CellSet cellSetBefore = ua.olap().prepareOlapStatement(query)
				.executeQuery();
		assertTrue(cellSetBefore != null);
		ua.print(cellSetBefore);

		// Change a cell
		cellSetBefore.getCell(Arrays.asList(3, 0)).setValue(1000000,
				AllocationPolicy.EQUAL_ALLOCATION);
		CellSet cellSetAfter = ua.olap().prepareOlapStatement(query)
				.executeQuery();
		assertTrue(cellSetAfter != null);
		ua.print(cellSetAfter);
	}

	public void XmlaDriverConnectDemo() throws Exception {
		Class.forName("org.olap4j.driver.xmla.XmlaOlap4jDriver");
		final Connection connection = DriverManager
				.getConnection(
				// This is the SQL Server service end point.
						"jdbc:xmla:Server=http://localhost:81/mondrian/xmla"

						// Tells the XMLA driver to use a SOAP request cache
						// layer.
						// We will use an in-memory static cache.
								+ ";Cache=org.olap4j.driver.xmla.cache.XmlaOlap4jNamedMemoryCache"

								// Sets the cache name to use. This allows
								// cross-connection
								// cache sharing. Don't give the driver a cache
								// name and it
								// disables sharing.
								+ ";Cache.Name=MyNiftyConnection"

								// Some cache performance tweaks.
								// Look at the javadoc for details.
								+ ";Cache.Mode=LFU;Cache.Timeout=600;Cache.Size=100",

						// XMLA is over HTTP, so BASIC authentication is used.
						null, null);

		// We are dealing with an olap connection. we must unwrap it.
		final OlapConnection olapConnection = connection
				.unwrap(OlapConnection.class);

		// Check if it's all groovy
		ResultSet databases = olapConnection.getMetaData().getDatabases();
		databases.first();
		System.out.println(olapConnection.getMetaData().getDriverName()
				+ " -> " + databases.getString(1));

		// Done
		connection.close();
	}
}
