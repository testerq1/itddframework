package ate.ua.olap4j;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.mdx4j.Mdx4jParserFactory;
import org.olap4j.CellSet;
import org.olap4j.OlapConnection;
import org.olap4j.OlapException;
import org.olap4j.layout.RectangularCellSetFormatter;
import org.olap4j.mdx.SelectNode;
import org.olap4j.mdx.parser.MdxParser;
import org.olap4j.mdx.parser.MdxValidator;

import ate.AbstractURIHandler;
import ate.ua.RunFailureException;
import ate.ua.UAOption;

public class Olap4jUA extends AbstractURIHandler {
	String jdbc="jdbc:mondrian";
	Connection connection;
	OlapConnection olapConnection;
	
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			try {
				Class.forName("mondrian.olap4j.MondrianOlap4jDriver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			String tmp=value.toString();
			jdbc+=tmp.substring(tmp.indexOf(":"));
		}
		return this;
	}
	@Override
	public void teardown() {
		try {
			if(connection!=null)
				connection.close();
			olapConnection=null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void startup() {
		try {
			connection = DriverManager.getConnection(jdbc);
			olapConnection = connection.unwrap(OlapConnection.class);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public String get_default_schema() {
		return "olap4j";
	}

	@Override
	public boolean is_ready() {
		return olapConnection!=null;
	}
	
	public CellSet query(String qString){
		try {
			CellSet cellSet = olapConnection.createStatement().executeOlapQuery(qString);
			return cellSet;
		} catch (OlapException e) {
			throw new RunFailureException(e);
		}		
	}
	
	public void print(CellSet cellSet){
		RectangularCellSetFormatter formatter = new RectangularCellSetFormatter(
				false);
		PrintWriter writer = new PrintWriter(System.out);
		formatter.format(cellSet, writer);
		writer.flush();
	}
	
	public SelectNode parser(String qString){		
		MdxParser parser =null;
		if(olapConnection!=null)
			parser = olapConnection.getParserFactory()
				.createMdxParser(olapConnection);
		else 
			parser = Mdx4jParserFactory.createMdxParser();
		
		return parser.parseSelect(qString);
	}
	
	public void show_conn_info(){
		try {
			log.info("driverName:"+olapConnection.getMetaData().getDriverName());
			log.info("DatabaseProductName:"+olapConnection.getMetaData().getDatabaseProductName());
			log.info(" version:"+ olapConnection.getMetaData().getDatabaseMajorVersion() + "."
					+ olapConnection.getMetaData().getDatabaseMinorVersion());
		} catch (OlapException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	public boolean validate(String qString){
		MdxValidator validator=null;
		if(olapConnection!=null){
			validator=olapConnection.getParserFactory()
				.createMdxValidator(olapConnection);
		}else{
			throw new RunFailureException("connection is not exist");
		}
		
		try {
			validator.validateSelect(parser(qString));
			return true;
		} catch (OlapException e) {
			e.printStackTrace();
		}
		return false;		
	}
	public OlapConnection olap(){
		return this.olapConnection;
	}
}
