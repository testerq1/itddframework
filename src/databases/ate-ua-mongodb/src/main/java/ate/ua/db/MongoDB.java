package ate.ua.db;

/*
 * #%L
 * ate-ua-mongodb
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import ate.AbstractURIHandler;
import ate.ua.ICrudUA;
import ate.ua.UAOption;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

/**
 * new MongoDB("mongodb://root:123456@127.0.0.1:12345/db");
 * 
 * 
 * @author xiaoyong.huang
 *
 */
public class MongoDB extends AbstractURIHandler implements ICrudUA{
	private MongoClient con;	
	private DB db;
	private String source="test";
	
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			if(port<=0)
				port=27017;
			if(path!=null&&path.length()>1)
				source=this.get_path().substring(1);
		}
		return this;
	}
	
	@Override
	public long countTable(String table) {
		return db.getCollection(table).getCount();
	}

	public DBCollection createTable(String name){
		return db.getCollection(name);
	}

	public void drop_database(String dbName){
		con.dropDatabase(dbName);		
	}
	
	/**
	 * 如果不存在，db.getCollection(table)会先自动创建
	 * @param table
	 */
	public void drop_table(String table){		
		db.getCollection(table).drop();
		
	}
	
	@Override
	public int update(String qString) {
		db.requestStart();
        try {
            db.requestEnsureConnection();
    
            Object result = db.eval(qString);
    
            if(log.isDebugEnabled()) {
                log.debug("Result : " + result);
            }
            if(result!=null)
    			return 0;
    		return -1;
        } finally {
            db.requestDone();
        }
	}
	
	@Override
	public String get_default_schema() {		
		return "mongodb";
	}
	
	@Override 
	public int get_port(){		
		return port;
	}

	@Override
	public List<String> getDatabases() {
		return con.getDatabaseNames();		
	}
	
	public List<DBObject> getIndexInfo(String table){
		return db.getCollection(table).getIndexInfo();
	}	
	public ArrayList<String> getTables() {
		ArrayList<String> ar =new ArrayList<String>();
		Set<String> collectionNames = db.getCollectionNames();
        for (String s : collectionNames) {
            ar.add(s);
        }
		return ar;
	}
	@Override
	public ArrayList<String> getTables(String dbName) {
		useDB(dbName);
		return getTables();
	}
	
	@Override
	public boolean is_ready() {
		return con!=null;
	}
	@Override
	public Object query(String qString) {
		db.requestStart();
        try {
            db.requestEnsureConnection();
    
            Object result = db.eval(qString);
    
            if(log.isDebugEnabled()) {
                log.debug("Result : " + result);
            }
            return result;
        } finally {
            db.requestDone();
        }
	}

	@Override
	public void startup(){
		try {	
			MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
			builder.socketKeepAlive(true).socketTimeout(30000);
			builder.connectionsPerHost(10);
			builder.threadsAllowedToBlockForConnectionMultiplier(10);
			
			if(username!=null&&username.length()>0){
				con = new MongoClient(new ServerAddress(host, port),
						Arrays.asList(MongoCredential.createMongoCRCredential(username, source,
                        passwd.toCharArray())),builder.build());
			}else
				con = new MongoClient(host+":"+this.get_port(),builder.build());	
			
			useDB(source);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void teardown() {
		con.close();
	}

	/**
	 * 如果不存在该库，则会自动创建
	 */
	public DB useDB(String dbname) {
		if(db==null||!db.getName().equals(dbname))
			db = con.getDB(dbname);		
		return db;
	}
	
	public DB useDB(String dbname,String uname,String passwd) {
		if(db==null||!db.getName().equals(dbname)){
			db = con.getDB(dbname);
			db.authenticate(uname, passwd.toCharArray());
		}
		return db;
	}
}
