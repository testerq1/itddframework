package ate.test;

/*
 * #%L
 * ate-ua-mongodb
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.LinkedList;
import java.util.List;

import org.testng.annotations.Test;

import ate.testcase.CsUATestcase;
import ate.ua.db.MongoDB;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.WriteConcern;

public class MongoDBUATest extends CsUATestcase{

	@Test
	/**
	 * mongod -f /etc/mongodb.conf  --auth --bind_ip=172.16.5.118,127.0.0.1
	 */
	public void auth(){
		MongoDB db=this.create_ua("mongo://sa:sa@"+tsip+"/admin",MongoDB.class);
		db.startup();
		List ll=db.getDatabases();
		assertTrue(ll.size()>=2);
		
		ll=db.getTables();
		assertTrue(ll.size()>=2);
		
		DB testDB = db.useDB("test");		
		DBCollection cc=testDB.getCollection("test");
        long cnt=cc.count();        
        cc.insert(new BasicDBObject());        
        assertTrue(cc.count()==(cnt+1));		
	}
	/**
	 * mongod -f /etc/mongodb.conf --bind_ip=172.16.5.118,127.0.0.1
	 */
	public void mongodb(){
		MongoDB db=this.create_ua("mongo://"+tsip);
		db.startup();
		db.drop_database("test");
		db.drop_database("mpngo");
		db.useDB("mongo");
		db.useDB("test");		
		List<String> rs = db.getDatabases();
		assertTrue(rs.contains("test"));
		assertTrue(rs.contains("mongo"));
		DBCollection table=db.createTable("MyEmployees");
		assertTrue(!db.getTables("test").contains("mongo"));
		
		BasicDBObject doc = new BasicDBObject("f", "ravi").
				                       append("l", "huang").
				                       append("n", 123).
				                       append("p", new BasicDBObject("x", 203).append("y", 102));
		table.setWriteConcern(WriteConcern.UNACKNOWLEDGED);
		table.insert(doc);
		table.createIndex(new BasicDBObject("f", 1));
		long a=now();
		int ysize=1000000;
		List<DBObject> basicDBObjects = new LinkedList<DBObject>();
        for(int i=1;i<=ysize;i++){
        	BasicDBObject bo=new BasicDBObject("f", "ravi"+i);
        	bo.put("l", "huang"+i);
        	bo.put("n",i);
        	bo.put("p", new BasicDBObject("x", 203+i).append("y", 102+i));        	
            basicDBObjects.add(bo);
            
            if (i%12000==0) {
            	long b=now();
            	table.insert(basicDBObjects);
                basicDBObjects.clear();
                System.out.println("Cycle:"+ysize+" duration:"+ (now()-b)+" ms "+table.getCount());
            }
        }
        System.out.println("Cycle:"+ysize+" duration:"+ (now()-a)+" ms "+table.getCount());
        
		assertTrue(table.getCount()==ysize+1);		
	}
	
}
