package ate.ua.memcache;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.spy.memcached.MemcachedClient;

import org.testng.annotations.Test;

import ate.testcase.UATestcase;

public class SmcTest extends UATestcase {

    @Test
    public void crud() {
        SmcUA ua = this.create_ua("smc://172.16.5.219", SmcUA.class);
        ua.startup();
        MemcachedClient client = ua.get_client();

        client.add("hello", 0, "dennis");
        client.replace("hello", 0, "dennis");
        client.append("hello", " good");
        client.prepend("hello", "hello ");
        // get operation
        assertTrue("hello dennis good".equals(client.get("hello")));
        
        client.set("a", 0, "1");
        client.incr("a", 4);
        assertTrue("5".equals(client.get("a")));
        client.decr("a", 4);
        assertTrue("1".equals(client.get("a")));
        
        List<String> keys = new ArrayList<String>();
        keys.add("hello");
        keys.add("a");
        Map<String, Object> map = client.getBulk(keys);
        assertTrue("hello dennis good".equals(map.get("hello")));
        
        assertTrue(ua.cas("a", "40"));
        assertTrue("40".equals(client.get("a")));
        
        assertTrue(ua.cas2("a", "401"));
        assertTrue("401".equals(client.get("a")));
        
        client.flush();
        assertTrue(null==client.get("hello"));
        assertTrue(null==client.get("a"));
    }
}
