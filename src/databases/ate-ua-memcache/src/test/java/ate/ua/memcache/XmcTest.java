package ate.ua.memcache;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import net.rubyeye.xmemcached.CASOperation;
import net.rubyeye.xmemcached.GetsResponse;
import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.exception.MemcachedException;

import org.testng.annotations.Test;

import ate.rm.dev.Host;
import ate.testcase.UATestcase;

public class XmcTest extends UATestcase {
//    @org.testng.annotations.BeforeClass
//    public void befortTest(){
//        Host.localhost();
//    }
    
    @Test
    public void crud() throws TimeoutException, InterruptedException,
            MemcachedException {
        XmcUA ua = this.create_ua("xmc://172.16.5.219", XmcUA.class);
        ua.startup();
        MemcachedClient client = ua.get_client();

        // set add replace append prepend gets
        client.add("hello", 0, "dennis");
        client.replace("hello", 0, "dennis");
        client.append("hello", " good", 500);
        client.prepend("hello", "hello ", 500);
        // get operation
        assertTrue("hello dennis good".equals(client.get("hello")));

        // incr decr
        client.set("a", 0, "1");
        client.incr("a", 4);
        assertTrue("5".equals(client.get("a")));
        client.decr("a", 4);
        assertTrue("1".equals(client.get("a")));
        // cas
        assertTrue(ua.cas("a", "4"));
        assertTrue("4".equals(client.get("a")));
        
        assertTrue(ua.cas2("a", "40"));
        assertTrue("40".equals(client.get("a")));
        
        // stats
        Map<InetSocketAddress, Map<String, String>> result = client.getStats();
        for(Map.Entry tmp:result.entrySet()){
            log.info("result: {}   {}",tmp.getKey(),tmp.getValue());
        }        
        
        // get server versions
        Map<InetSocketAddress, String> version = client.getVersions();
        for(Map.Entry tmp:version.entrySet()){
            log.info("version: {}   {}",tmp.getKey(),tmp.getValue());
        }
        
        // bulk get
        assertTrue("hello dennis good".equals(client.get("hello")));
        List<String> keys = new ArrayList<String>();
        keys.add("hello");
        keys.add("a");
        Map<String, Object> map = client.get(keys);
        assertTrue("hello dennis good".equals(map.get("hello")));
        
        
        //clear all
        client.flushAll();
        assertTrue(null==client.get("hello"));
        assertTrue(null==client.get("a"));
    }
}
