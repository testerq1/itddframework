package ate.ua.memcache;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import net.rubyeye.xmemcached.CASOperation;
import net.rubyeye.xmemcached.GetsResponse;
import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.XMemcachedClientBuilder;
import net.rubyeye.xmemcached.auth.AuthInfo;
import net.rubyeye.xmemcached.command.BinaryCommandFactory;
import net.rubyeye.xmemcached.command.KestrelCommandFactory;
import net.rubyeye.xmemcached.exception.MemcachedException;
import net.rubyeye.xmemcached.utils.AddrUtil;
import ate.AbstractURIHandler;

/**
 * https://github.com/killme2008/xmemcached
 * XMC只支持TCP协议
 * @author ravi huang
 *
 */
public class XmcUA extends AbstractURIHandler {
    MemcachedClient client;
    String serverAddress;
    
    public boolean cas2(String key,String value){        
        try {
            GetsResponse response = client.gets(key);
            return client.cas(key, 0, value, response.getCas());
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (MemcachedException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean cas(String key,final String value){
        try {
            return client.cas(key, 0, new CASOperation<String>() {
                @Override
                public int getMaxTries() {
                    return 1; // max try times
                }

                @Override
                public String getNewValue(long currentCAS, String currentValue) {
                    return value; // return new value to update
                }
            });
        } catch (TimeoutException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MemcachedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public void set_server_addr(String saddr){
        this.serverAddress=saddr;
        
    }
    /**
     * //If you want to store primitive type as String
     * client.setPrimitiveAsString(true);
     * 
     * //Add or remove memcached server dynamically
     * client.addServer("localhost:12001 localhost:12002");
     * client.removeServer("localhost:12001 localhost:12002");
     * 
     * @return
     */
    public MemcachedClient get_client() {
        return client;
    }
        
    
    @Override
    public void teardown() {
        try {
            client.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
        client=null;
    }

    @Override
    public int get_port() {
        if (this.port <= 0)
            port = 11211;
        return port;
    }

    @Override
    public String get_host() {
        if (this.host == null || this.host.length() == 0)
            host = "localhost";
        return host;
    }

    @Override
    public void startup() {
        if(serverAddress==null)
            serverAddress=get_host()+":"+get_port();
        
        XMemcachedClientBuilder builder = new XMemcachedClientBuilder(
                AddrUtil.getAddresses(serverAddress));
        
        try {
            String protocol = get_query_para("protocol");
            if (null == protocol)
                ;
            else if (protocol.startsWith("bin"))// 二进制协议
                builder.setCommandFactory(new BinaryCommandFactory());
            else if (protocol.startsWith("kes"))// Kestrel协议
                builder.setCommandFactory(new KestrelCommandFactory());
            if(username!=null)
                builder.addAuthInfo(AddrUtil.getOneAddress(serverAddress), AuthInfo
                    .typical(username, passwd));
            
            client = builder.build();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String get_default_schema() {
        return "xmc";
    }

    @Override
    public boolean is_ready() {
        return client != null && !client.isShutdown();
    }

}
