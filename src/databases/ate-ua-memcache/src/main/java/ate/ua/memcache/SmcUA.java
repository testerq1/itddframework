package ate.ua.memcache;

import java.io.IOException;
import java.util.*;

import net.spy.memcached.AddrUtil;
import net.spy.memcached.BinaryConnectionFactory;
import net.spy.memcached.CASMutation;
import net.spy.memcached.CASMutator;
import net.spy.memcached.CASResponse;
import net.spy.memcached.ConnectionFactoryBuilder;
import net.spy.memcached.ConnectionFactoryBuilder.Protocol;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.auth.AuthDescriptor;
import net.spy.memcached.auth.PlainCallbackHandler;
import net.spy.memcached.transcoders.Transcoder;
import ate.AbstractURIHandler;

/**
 * https://code.google.com/p/spymemcached/wiki/Examples
 * @author ravi huang
 *
 */
public class SmcUA extends AbstractURIHandler{
    MemcachedClient client;
    String serverAddress;
    
    public void set_server_addr(String saddr){
        this.serverAddress=saddr;
        
    }
    public MemcachedClient get_client() {
        return client;
    }

    @Override
    public int get_port() {
        if (this.port <= 0)
            port = 11211;
        return port;
    }

    @Override
    public String get_host() {
        if (this.host == null || this.host.length() == 0)
            host = "localhost";
        return host;
    }    
    @Override
    public void teardown() {
       client.shutdown();  
       client=null;
    }

    @Override
    public void startup() {
        System.setProperty("net.spy.log.LoggerImpl","net.spy.memcached.compat.log.Log4JLogger");
        //Logger.getLogger("net.spy.memcached").setLevel(Level.INFO);
        try {
            if(serverAddress==null)
                serverAddress=get_host()+":"+get_port();
            
            String protocol = get_query_para("protocol");
            
            if(username!=null){
                AuthDescriptor ad = new AuthDescriptor(new String[]{"PLAIN"},
                        new PlainCallbackHandler(username, passwd));
                client=new MemcachedClient(
                        new ConnectionFactoryBuilder().setProtocol(Protocol.BINARY)
                        .setAuthDescriptor(ad)
                        .build(),
                        AddrUtil.getAddresses(serverAddress));
            }else if (protocol!=null&&protocol.startsWith("bin"))// 二进制协议
                client=new MemcachedClient(
                                new BinaryConnectionFactory(),
                                AddrUtil.getAddresses(serverAddress));
            else {
                client=new MemcachedClient(
                        AddrUtil.getAddresses(serverAddress));
            }          
            
        } catch (IOException e) {
            e.printStackTrace();
            
        }        
    }
    
    public boolean cas2(String k,Object v){        
        return CASResponse.OK==client.cas(k, client.gets(k).getCas(), v);        
    }
    
    public <T extends Comparable> boolean cas(String key,final T value){
        return cas(key,value,(Transcoder<T>)client.getTranscoder());
    }
    public <T extends Comparable> boolean cas(String key,final T value,Transcoder<T> tc) {
        CASMutation<T> mutation = new CASMutation<T>() {
            @Override
            public T getNewValue(T current) {               
                return value;
            }
        };
        
        //List<Object> initialValue=Collections.singletonList(newItem);        
        CASMutator<T> mutator = new CASMutator<T>(client, tc);
        
        try {
            T nv = mutator.cas(key, value, 0, mutation);
            return nv.equals(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
    @Override
    public String get_default_schema() {
        return "smc";
    }

    @Override
    public boolean is_ready() {
        return client!=null;
    }

}
