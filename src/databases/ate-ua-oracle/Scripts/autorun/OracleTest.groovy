import org.testng.*
import org.testng.annotations.*
import static org.testng.AssertJUnit.*;
import java.lang.reflect.*;
import ate.testcase.*
import ate.ua.db.OracleDB;

public class OracleUATest extends UATestcase{	
	@Test
	public void gsql(){
		log.info "gsql:"
		log.info "$STEP.1. 初始化数据库连接："
		def db = create_ua("oracle://scott:tiger@127.0.0.1:1521/?type=thin#gsql");
		db.startup();
		
//		db.gsql.eachRow('show databases') { 
//			println "${it.Database}"
//		}
		
	}
	
	public void oracle(){
		log.info "oracle:"
		log.info "$STEP.1. 初始化数据库连接："
		def db = create_ua("oracle://scott:tiger@127.0.0.1:1521/?type=thin");
		db.startup();
		
		log.info "$STEP.2. 重新创建test数据库："
		assertTrue(db.update("drop database if exists test") > -1);
		assertTrue(db.update("create database IF NOT EXISTS test") > -1);
		
		log.info "$STEP.3. 确认创建test数据库成功："
		def rs = db.getDatabases();
		assertTrue(rs.contains("test"));
		
		log.info "$STEP.4. 确认oracle数据库中包含user表："
//		def rs1 = db.getSchemas();
//		assertTrue(rs.equals(rs1));
//		rs1=db.getTables("mysql");
//		assertTrue(rs1.contains("user"));
		
		log.info "$STEP.5. 在test数据库中创建MyEmployees表："
		def create_table= "create table IF NOT EXISTS test.MyEmployees ( "+ 
		   "   id INT PRIMARY KEY, firstName VARCHAR(20), lastName VARCHAR(20), " + 
		   "   title VARCHAR(20), salary INT )";
		db.useDB("test");		
		assertTrue(db.update("drop table if exists MyEmployees") > -1);		
		assertTrue(db.update(create_table) > -1);
		
		log.info "$STEP.6. 在MyEmployees表中插入测试行："
		assertTrue(db.update("insert into MyEmployees(id, firstName) values(100, 'A')")>-1);		
		assertTrue(db.update("delete from MyEmployees")>-1);		
		
		log.info "$STEP.7. 测试prepareStatement插入："
		def prest=db.prepareStatement("insert into MyEmployees(id, firstName) values(?, ?)");
		//db.setAutoCommit(false);
		def a=now();
		def ysize=10;
		
		for(int x = 0; x < ysize; x++){
			for(int y=0;y<ysize;y++){
	            prest.setInt(1, x+y);        
	            prest.setString(2, "abc"+random(0,x+y));        
	            prest.addBatch();	            
			}
			x+=ysize-1;
			prest.executeBatch();
			db.commit();
        }
		log.info "$STEP.8. 确认采用prepareStatement插入的数据是否正确："
		//countTable等于select *
        def cnt=db.countTable("MyEmployees");
        log.info("DB insert {} row in {}ms", cnt,(now()-a));
        assertTrue(cnt==ysize);
        
		log.info "$STEP.9. 查询结果集ResultSet的遍历方法："
        def rset=db.query("select * from MyEmployees limit 2");
        assertTrue(rset.next());
        
        log.info(rset.getInt(1)+"");
        log.info(rset.getString(2));
        if(rset.next()){
        	log.info(rset.getInt(1)+"");
            log.info(rset.getString(2));
        }  
	}
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		// ;
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}