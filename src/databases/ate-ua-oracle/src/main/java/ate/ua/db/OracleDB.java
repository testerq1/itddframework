package ate.ua.db;

/*
 * #%L
 * ate-ua-mysql
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.util.List;

import ate.AbstractURIHandler;
import ate.rm.dev.Host;
import ate.ua.AbstractCrudImp;
import ate.ua.UAOption;


/**
 * 
 * @author ravi huang
 * 
 */
public class OracleDB extends AbstractCrudImp {
	String type="thin";	//or oci
	
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {			
			if(this.get_query_para("type")!=null){
				type=get_query_para("type");
			}
		}
		return this;
	}
	
	@Override
	public String capabilities() {
		StringBuffer buf = new StringBuffer(super.capabilities());
		//buf.append("oracle: " + (runtime.which("mysql") == null ? "no" : "yes")).append("\r\n");

		return buf.toString();
	}
	
	@Override
	public String get_default_schema() {
		return "oracle";
	}

	@Override
	public int get_port() {
		if (super.get_port() == -1)
			return 1521;
		return super.get_port();
	}

	@Override
	protected String getDriver() {
		return "oracle.jdbc.driver.OracleDriver";
	}

	@Override
	protected String getJDBCUri() {
		String db=get_path();
		//jdbc:oracle:thin:@localhost:1521:ORADB
		if(db!=null&&db.length()>1)
			db=":"+db.substring(1);		
		return "jdbc:oracle:"+type+":@" + this.get_host() + ":" + this.get_port() + db;
		
	}

	@Override
	public List<String> getSchemas() {
		return this.getDatabases();
	}
	
	@Override
	public void startup() {
		super.startup();
		//由于使能了Groovy Sql功能，因此con也可能不会被初始化，而使用sql句柄
		if(con!=null){
			//TODO
		}
	}

}
