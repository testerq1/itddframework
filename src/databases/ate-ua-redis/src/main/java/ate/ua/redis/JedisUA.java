package ate.ua.redis;

import redis.clients.jedis.Jedis;
import ate.AbstractURIHandler;

public class JedisUA extends AbstractURIHandler {
    Jedis conn;
    
    public Jedis get_conn(){
        return conn;
    }
    
    @Override
    public int get_port(){
        if(port<=0)
            port=6379;
        return port;
    }
    
    @Override
    public void teardown() {
       conn.disconnect();
    }

    @Override
    public void startup() {
        log.debug("conntct to {}:{}",get_host(),get_port());
        conn = new Jedis(get_host(),get_port());   
        conn.connect();
    }

    @Override
    public String get_default_schema() {
        return "jedis";
    }

    @Override
    public boolean is_ready() {       
        return conn!=null&&conn.isConnected();
    }

}
