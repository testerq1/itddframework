package ate.ua.redis;

import org.testng.annotations.Test;

import redis.clients.jedis.Jedis;
import ate.testcase.UATestcase;


public class JredisTest extends UATestcase {
    @Test
    public void test(){
        JedisUA ua=this.create_ua("jedis://172.16.5.219", JedisUA.class);
        ua.startup();
        
        Jedis conn=ua.get_conn();
        
        conn.set("foo", "bar");
        assertTrue("bar".equals(conn.get("foo")));
        log.info(conn.del("foo")+"");
        assertTrue(null==conn.get("foo"));
    }
}
