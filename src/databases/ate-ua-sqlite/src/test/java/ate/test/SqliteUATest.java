package ate.test;

import java.io.File;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;

import ate.testcase.UATestcase;
import ate.ua.db.SqliteUA;
import ate.ua.db.SqliteDB;

public class SqliteUATest extends UATestcase {
	List<String> items1 = new ArrayList<String>(){{
		add("huang");
		add("xiao");
		add("yong");		
	}};
	List<String> items2 = new ArrayList<String>(){{
		add("huang1");
		add("xiao1");
		add("yong1");		
	}};
	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		LHOST.rm("target/db1");
		LHOST.rm("target/db2");
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		LHOST.rm("target/db1");
		LHOST.rm("target/db2");
		super.AfterClass();
	}	
	
	@Test
	public void aSqliteUA(){
		crud("db1",items1);		
		SqliteUA ua = new SqliteUA();
		File ff=new File("target/db1");
		ua.build(ua.UA_TYPE, ua.UA).build(ua.UA_URI,
				"sqlite:"+ff.toURI());
		log.info(ff.toURI()+"");
		ua.startup();
		assertTrue(ua.is_ready());			
		
		ua.teardown();
	}	
	
	@Test
	public void attach() throws SQLException{		
		crud("db2",items2);
		SqliteDB ua=this.create_ua("sqlitedb:/./target/db1", SqliteDB.class);
		ua.startup();
		assertTrue(ua.is_ready());	
		ResultSet rs=ua.query("SELECT * FROM db1");	
		int cnt=0;
		while(rs.next()){			
        	assertTrue(items1.get(cnt++).equals(rs.getString(2)));
        } 
		
		ua.attach("target/db2", "db2");
		rs=ua.query("SELECT * FROM db2.db2");
		cnt=0;
		while(rs.next()){        
			assertTrue(items2.get(cnt++).equals(rs.getString(2)));
        } 
	}
	@Test
	public void bSqliteDB(){
		File ff=new File("target/db1");
		SqliteDB ua=this.create_ua("sqlitedb:"+ff.toURI(), SqliteDB.class);
		ua.startup();
		assertTrue(ua.is_ready());	
		ResultSet rs=ua.query("SELECT * FROM db1");		
		try {
			int cnt=0;
			while (rs.next()) {
				assertTrue(items1.get(cnt).equals(rs.getString(2)),rs.getString(2));
				cnt++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		assertTrue(ua.countTable("db1")==3);
		List<String> ll=ua.getTables("main");
		for(String tmp:ll)
			log.info(tmp);
		
		ua.teardown();
	}
	
	void crud(String tablename,List<String> list) {
		SqliteUA ua = new SqliteUA();
		ua.build(ua.UA_TYPE, ua.UA).build(ua.UA_URI,
				"sqlite:/./target/"+tablename);
		ua.startup();
		assertTrue(ua.is_ready());		
		
		SQLiteStatement st1= null,st2=null;
			
		ua.prepare_now("DROP TABLE if EXISTS "+tablename);
		ua.prepare_now("CREATE TABLE if NOT EXISTS "+tablename+" (id int, sig varchar(255))");
		ua.exec("BEGIN TRANSACTION;");
		try {			
			for (int i = 0; i < list.size(); i++) {
				st1 = ua.prepare("INSERT INTO "+tablename+" VALUES (?, ?);",true);
				st1.bind(1, i).bind(2, list.get(i));
				st1.step();
			}			
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			st1.dispose();					
		}
		ua.exec("COMMIT TRANSACTION;");
		
		st2 = ua.query("SELECT * FROM "+tablename);		
		try {
			int cnt=0;
			while (st2.step()) {
				assertTrue(list.get(cnt++).equals(st2.columnString(1)));
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		}finally {
			st2.dispose();			
		}		
		assertTrue(ua.countTable(tablename)==3);
		ua.teardown();
	}
}
