package ate.test;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.testng.annotations.*;

import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;

import ate.testcase.UATestcase;
import ate.ua.db.SqliteUA;
import ate.util.ExcelUtil;

public class DPI extends UATestcase{
	SqliteUA ua = new SqliteUA();
    ExcelUtil f=new ExcelUtil("中创DPI.xls");
    
	@org.testng.annotations.BeforeClass
	public void before(){
		 ua.build(ua.UA_TYPE, ua.UA).build(ua.UA_URI,
	                "sqlite:/./target/dpi.db");
	     ua.startup();
	}    
    
    @Test
    public void print(){
    	 ua.print(ua.query("select * from greaty where apptype='BITTORRENT'"));
    	 ua.print(ua.query("select 子业务名称,id from cmcc"));
    }
    
    @Test
    public void dpi() throws IOException, SQLiteException {        
       
        createGreaty();
        createCmcc();
        createZcxc();        
        //ua.print(ua.query("select 业务类型,SUM(总流量)  from zcxc group by 业务类型  "));
        SQLiteStatement cc=ua.query("select 子业务名称,id from cmcc");
        Sheet cmcc=f.switch_sheet("移动");
        
        while (cc.step()) {
        	String name=cc.columnString(0);
        	int id=cc.columnInt(1);
        	Row row = cmcc.getRow(id);
        	
        	SQLiteStatement yd=ua.query("select 业务类型,SUM(总流量) from zcxc where 业务类型='"+name+"' group by 业务类型");        	
        	if(yd.step()){
        		//System.out.println(name+"="+yd.columnDouble(1));
        		row.createCell(5).setCellValue(yd.columnDouble(1));
        	}
        	SQLiteStatement yd2=ua.query("select 业务分类,SUM(总流量) from zcxc where 业务分类='"+name+"' group by 业务分类");        	
        	if(yd2.step()){
        		//System.out.println(name+"="+yd2.columnDouble(1)/1024/1024);
        		row.createCell(6).setCellValue(yd2.columnDouble(1));
        	}else{
        		
        	}
        	
        	SQLiteStatement gd=ua.query("select byteTotal from greaty where apptype='"+name+"'");
        	if(gd.step()){
        		//System.out.println(name+"="+gd.columnDouble(0)/1024/1024);
        		row.createCell(7).setCellValue(gd.columnDouble(0)/1024/1024);
        	}
        	
        	if(row.getCell(7)!=null){
        		if(row.getCell(6)!=null&&row.getCell(6).getNumericCellValue()>0){
        			row.createCell(8).setCellValue(row.getCell(7).getNumericCellValue()/row.getCell(6).getNumericCellValue());        			
        		}else if(row.getCell(5)!=null&&row.getCell(5).getNumericCellValue()>0){
        			row.createCell(8).setCellValue(row.getCell(7).getNumericCellValue()/row.getCell(5).getNumericCellValue());        			
        		}        		
        	}

        }
        f.dump();
                
    }
    void createZcxc()throws IOException{
        //小时    业务分组    业务分类    业务类型    总流量(MB) 上行流量(MB)    下行流量(MB)    上行速率(Kbps)  下行速率(Kbps)  总次数
        ua.prepare_now("DROP TABLE if EXISTS zcxc");
        ua.prepare_now("CREATE TABLE if NOT EXISTS zcxc("
                + "小时                   varchar(255), "
                + "业务分组         varchar(255),"
                + "业务分类         varchar(255),"
                + "业务类型         varchar(255),"
                + "总流量              REAL,"
                + "上行流量         REAL,"
                + "下行流量         REAL,"
                + "总次数              INT)");
        ua.exec("BEGIN TRANSACTION;");     
        
        SQLiteStatement st1= null;
        Object[][] yd=f.load_sheet("中创");
        try {           
            for (int i = 1; i < yd.length; i++) {
                if(yd[i]==null)
                    break;
                st1 = ua.prepare("INSERT INTO zcxc VALUES (?, ?, ?, ?, ?, ?, ?, ?);",true);   
                
                st1.bind(1, yd[i][0].toString()).bind(2, yd[i][1].toString().toUpperCase());
                st1.bind(3, yd[i][2].toString().toUpperCase()).bind(4, yd[i][3].toString().toUpperCase());
                st1.bind(5, Double.parseDouble(yd[i][4]+"")).bind(6, Double.parseDouble(yd[i][5]+""));
                st1.bind(7, Double.parseDouble(yd[i][6]+"")).bind(8, Double.parseDouble(yd[i][9]+""));
                st1.step();
            }           
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            st1.dispose();                  
        }
        ua.exec("COMMIT TRANSACTION;");
        ua.print_all("zcxc");
    }
    void createGreaty()throws IOException{
        ua.prepare_now("DROP TABLE if EXISTS greaty");
        ua.prepare_now("CREATE TABLE if NOT EXISTS greaty(id int, "
                + "apptype varchar(255),"
                + "byteTotal INT,"
                + "byteSent INT,"
                + "byteRecv INT,"
                + "newSession INT)");
        ua.exec("BEGIN TRANSACTION;");     
        
        SQLiteStatement st1= null;
        Object[][] yd=f.load_sheet("网鼎");
        try {           
            for (int i = 2; i < yd.length; i++) {
                if(yd[i]==null)
                    break;
                st1 = ua.prepare("INSERT INTO greaty VALUES (?, ?, ?, ?, ?, ?);",true);                
                st1.bind(1, (int)Double.parseDouble(yd[i][0]+""));
                
                String name=yd[i][1].toString().split("]")[1].trim().toUpperCase();
                st1.bind(2, name);
                st1.bind(3, (long)Double.parseDouble(yd[i][2]+"")).bind(4, (long)Double.parseDouble(yd[i][3]+""));
                st1.bind(5, (long)Double.parseDouble(yd[i][4]+"")).bind(6, (long)Double.parseDouble(yd[i][5]+""));
                st1.step();
            }           
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            st1.dispose();                  
        }
        ua.exec("COMMIT TRANSACTION;");
        ua.print_all("greaty");
    }
    void createCmcc()throws IOException{
        ua.prepare_now("DROP TABLE if EXISTS cmcc");
        ua.prepare_now("CREATE TABLE if NOT EXISTS cmcc("
        		+ "id     INT,"        		      		
                + "业务类型      varchar(255), "
                + "序号                INT,"
                + "子业务名称  varchar(255),"
                + "优先级            varchar(255),"
                + "备注                 varchar(255),"
                + "ZCXC      REAL,"
                + "Greaty    REAL,"
                + "Diff      REAL)");
                  
        ua.exec("BEGIN TRANSACTION;");
        SQLiteStatement st1= null,st2=null;        
        Object[][] yd=f.load_sheet("移动");
        try {           
            for (int i = 1; i < yd.length; i++) {
                if(yd[i]==null||yd[i][0]==null)
                    break;
                st1 = ua.prepare("INSERT INTO cmcc VALUES (?, ?, ?, ?, ?, ?,NULL,NULL,NULL);",true);   
                st1.bind(1, i);
                st1.bind(2, yd[i][0].toString().toUpperCase());
                if(yd[i][1].equals("-"))
                    st1.bind(3, -1);
                else
                    st1.bind(3, (int)Double.parseDouble(yd[i][1].toString()));
                st1.bind(4, yd[i][2].toString().toUpperCase()).bind(5, yd[i][3]+"");
                st1.bind(6, yd[i][4]+"");
                
                st1.step();
            }           
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            st1.dispose();                  
        }
        ua.exec("COMMIT TRANSACTION;");
        ua.print_all("cmcc");
    }
}
