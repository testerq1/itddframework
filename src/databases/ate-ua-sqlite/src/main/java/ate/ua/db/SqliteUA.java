package ate.ua.db;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import ate.AbstractURIHandler;
import ate.ua.ICrudUA;
import ate.ua.RunFailureException;
import ate.ua.UAOption;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;

/**
 * not using jdbc
 * depends on sqlite4java version 0.282, 本地文件 sqlite:/./test.db#new<br>
 * 
 * URI:<br>
 * sqlite:file:///c:/sqlite.txt<br>
 * sqlite:/./test.db<br>
 * 
 * http://wiki.ci.uchicago.edu/VDS/VDSDevelopment/UsingSQLite<br>
 * https://sqlite4java.googlecode.com/svn-history/r285/trunk/ant/how-to-cross-compile_sqlite4java_r01.txt<br>
 * 
 * @author ravi huang
 * 
 */
public class SqliteUA extends AbstractURIHandler implements ICrudUA {
	private String file;
	SQLiteConnection db;
	File dbfile;
	
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option == UA_URI) {
			file = this.get_schemeSpecificPart();
			if (file.startsWith("/."))
				dbfile = new File(file.substring(1));
			else if (file.contains("://")||file.contains("file:/")) {
				try {
					dbfile = new File(new URI(file));
				} catch (URISyntaxException e) {
					throw new Error("Invalid file path" + file);
				}
			} else
				dbfile = new File(file);			
		}

		return this;
	}
	
	public void exec(String sql){
		try {
			db.exec(sql);
		} catch (SQLiteException e) {
			throw new RunFailureException("exec failure "+sql,e);
		}
	}
	
	@Override
	public String get_default_schema() {
		return "sqlite";
	}

	@Override
	public int update(String qString) {
		SQLiteConnection conn;
		try {
			conn = db.exec(qString);
			return conn.getErrorCode();
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public SQLiteStatement query(String qString) {
		return prepare(qString);
	}
	/**
	 * prepare and step
	 * @param sql
	 */
	public void prepare_now(String sql){
		SQLiteStatement st=prepare(sql,false);
		try {
			st.step();
		} catch (SQLiteException e) {
			e.printStackTrace();
		}finally{
			st.dispose();
		}
	}
	/**
	 * prepare cached=false
	 * @param sql
	 * @return
	 */
	public SQLiteStatement prepare(String sql){
		return prepare(sql,false);
	}
	
	/**
	 * prepare
	 * @param sql
	 * @param cached 
	 * @return
	 */
	public SQLiteStatement prepare(String sql,boolean cached) {
		try {
			SQLiteStatement st = db.prepare(sql,cached);
			return st;
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void print_all(String name){
        SQLiteStatement st=this.query("SELECT * FROM "+name);   
        try {
            int cnt=0;
            while (st.step()) {
                for(int i=0;i<st.columnCount();i++){
                    System.out.print(st.columnString(i)+"\t");
                }
                System.out.println();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }finally {
            st.dispose();          
        }
    }
	public void print(SQLiteStatement st){        
        try {
            int cnt=0;
            while (st.step()) {
                for(int i=0;i<st.columnCount();i++){
                    System.out.print(st.columnString(i)+"\t");
                }
                System.out.println();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }finally {
            st.dispose();          
        }
    }
	@Override
	public List<String> getDatabases() {
		
		return null;
	}

	@Override
	public List<String> getTables(String db) {
		try {
			SQLiteStatement st=this.db.prepare("tables");
			System.out.println();
		} catch (SQLiteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long countTable(String table) {
		SQLiteStatement st = null;
		try {
			st = db.prepare("SELECT count(*) FROM " + table);
			return st.step() ? st.columnInt(0) : 0;
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			st.dispose();
		}
		return -1;
	}

	@Override
	public void teardown() {
		db.dispose();
	}

	@Override
	public void startup() {
		if (dbfile.isDirectory())
			throw new Error("File path is a directory：" + path);

		db = new SQLiteConnection(dbfile);

		try {
			if (!dbfile.exists()) {
				dbfile.createNewFile();
			}
			db.open(true);
			//log.info(db.profile().printReport());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean is_ready() {
		return db != null && db.isOpen();
	}

}
