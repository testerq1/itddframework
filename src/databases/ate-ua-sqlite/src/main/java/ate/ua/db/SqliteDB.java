package ate.ua.db;

import java.io.File;

import ate.ua.AbstractCrudImp;
import ate.ua.UAOption;

/**
 * using jdbc
 * http://www.tutorialspoint.com/sqlite/sqlite_attach_database.htm
 * @author ravihuang
 *
 */
public class SqliteDB extends AbstractCrudImp {
	String dbfile;
	@Override
	public SqliteDB build(UAOption option, Object value) {
		super.build(option, value);

		if (option == UA_URI) {
			dbfile = this.get_schemeSpecificPart();
			if (dbfile.startsWith("/."))
				dbfile = new File(dbfile.substring(1)).toURI().toString();					
		}

		return this;
	}
	
	
	@Override
	protected String getDriver() {
		return "org.sqlite.JDBC";
	}

	@Override
	protected String getJDBCUri() {
		return "jdbc:sqlite:"+dbfile;
	}

	@Override
	public String get_default_schema() {		
		return "sqlitedb";
	}
	/**
	 * http://www.tutorialspoint.com/sqlite/sqlite_attach_database.htm
	 * @param file
	 * @param dbname
	 * @return
	 */
	public int attach(String file,String dbname){
		return update("attach database '"+file+"' as "+dbname);		
	}
}
