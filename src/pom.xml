<?xml version="1.0" encoding="UTF-8"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one
  or more contributor license agreements.  See the NOTICE file
  distributed with this work for additional information
  regarding copyright ownership.  The ASF licenses this file
  to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance
  with the License.  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an
  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, either express or implied.  See the License for the
  specific language governing permissions and limitations
  under the License.
--><project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <prerequisites>
    <maven>3.0.0</maven>
  </prerequisites>

  <organization>
    <name>Ravi Huang</name>
    <url>http://www.notes2000.com/</url>
  </organization>

  <groupId>ate</groupId>
  <version>1.0</version>
  <artifactId>ate</artifactId>
  <name>iTDD Framework</name>
  <packaging>pom</packaging>

  <url>http://www.notes2000.com/</url>
  <inceptionYear>2012</inceptionYear>
   <distributionManagement>
      <repository>
	       <id>release</id>
	       <url>http://localmaven:8081/nexus/content/repositories/releases</url>
      </repository>
      <snapshotRepository>
	       <id>snapshot</id>
	       <url>http://localmaven:8081/nexus/content/repositories/snapshots</url>
      </snapshotRepository>
   </distributionManagement>

   
   <scm>
    <connection>scm:git:https://ravi.huang@code.google.com/p/itddframework</connection>
    <url>scm:git:https://ravi.huang@code.google.com/p/itddframework</url>
    <developerConnection>scm:git:https://ravi.huang@code.google.com/p/itddframework</developerConnection>
    <tag>ate-1.0</tag>
  </scm>
  
  <!--distributionManagement>
    <repository>
        <id>nexus-releases</id>
        <name>Nexus Release Repository</name>
        <url>http://localmaven:8081/nexus/content/repositories/releases/</url>
    </repository>
    <snapshotRepository>
        <id>nexus-snapshots</id>
        <name>Nexus Snapshot Repository</name>
        <url>http://localmaven:8081/nexus/content/repositories/snapshots/</url>
    </snapshotRepository>
  </distributionManagement-->
  
  <!--issueManagement>
    <system>jira</system>
    <url>http://issues.apache.org/jira/browse/DIRMINA</url>
  </issueManagement>

  <ciManagement>
    <notifiers>
      <notifier>
        <type>mail</type>
        <address>dev@mina.apache.org</address>
      </notifier>
    </notifiers>
  </ciManagement-->

  <description>iTDD is an automation test platform.</description>

  <licenses>
    <license>
      <name>Apache 2.0 License</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <!-- Define the version for each of the used jar and maven plugins -->
  <properties>
  	<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>   
    <arguments/>
    <skipTests>false</skipTests>
    
    <!-- Maven Plugins -->    
    <version.assembly.plugin>2.3</version.assembly.plugin>  
    <version.bundle.plugin>2.3.7</version.bundle.plugin>  
    <version.compiler.plugin>2.5.1</version.compiler.plugin>
    <version.shade.plugin>1.7.1</version.shade.plugin>
    <version.surefire.plugin>2.12.2</version.surefire.plugin>
    <version.scm.plugin>1.8</version.scm.plugin>
    <version.release.plugin>2.3.2</version.release.plugin>
    <!--version.source.plugin>2.2</version.source.plugin-->
    <version.javadoc.plugin>2.9.1</version.javadoc.plugin>
    <version.jxr.plugin>2.3</version.jxr.plugin>
    <version.rat.maven.plugin>1.0-alpha-3</version.rat.maven.plugin>
    
    <!-- Jars -->
    <version.junit>4.11</version.junit>
    <version.log4j>1.2.17</version.log4j>
    <version.slf4j.api>1.7.5</version.slf4j.api>
    <version.slf4j.log4j12>1.7.5</version.slf4j.log4j12>
    <version.mina>2.0.9</version.mina>
    <version.mina.serial>2.0.9</version.mina.serial>
    <version.codec>1.8</version.codec>
    <version.testng>6.8.1</version.testng>
	  <version.netty>4.1.0.Beta4</version.netty>
	  <version.groovy>2.3.2</version.groovy>
	  <version.ant>1.9.2</version.ant>
	  <version.configuration>1.10</version.configuration>
	  <version.oro>2.0.8</version.oro>
	  <version.quartz>2.2.0</version.quartz>
		<version.net>3.3</version.net>
		<version.reflectutils>0.9.19</version.reflectutils>
		<version.snmp4j>2.3.0</version.snmp4j>
  </properties>

  <modules>    
  	<module>ate-main</module>
    <module>ate-core</module>    
    <module>utils</module>
    
    <module>network</module>    
    <module>databases</module>    
    <module>terminals</module>
    <module>rawsocket</module>
    <module>3g</module>    
    
    <!--module>sandbox</module>
    <module>ate-shell</module>
    <module>app</module>
    <module>ate-osgi</module>
    <module>utils</module-->    
   
  </modules>
  
  <!-- =========================================== -->
  <!-- The dependencies for all the projects       -->
  <!-- =========================================== -->
  <dependencyManagement>
    <dependencies>

	    <dependency>
	      <groupId>org.apache.ant</groupId>
	      <artifactId>ant</artifactId>
	      <version>${version.ant}</version>
	    </dependency>
	  	
      <dependency>
	      <groupId>org.codehaus.groovy</groupId>
	      <artifactId>groovy-all</artifactId>
	      <version>${version.groovy}</version>
      </dependency>
      
	    <dependency>
	      <groupId>commons-configuration</groupId>
	      <artifactId>commons-configuration</artifactId>
	      <version>${version.configuration}</version>
	    </dependency>
	    
	    <dependency>
	      <groupId>oro</groupId>
	      <artifactId>oro</artifactId>
	      <version>${version.oro}</version>
	    </dependency>
	  
	    <dependency>
	      <groupId>org.quartz-scheduler</groupId>
	      <artifactId>quartz</artifactId>
	      <version>${version.quartz}</version>
	    </dependency>
	            
      <dependency>
	      <groupId>junit</groupId>
	      <artifactId>junit</artifactId>
	      <version>${version.junit}</version>      
        <!--scope>test</scope-->
      </dependency>
      
      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>${version.slf4j.api}</version>
      </dependency>

      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-log4j12</artifactId>
        <version>${version.slf4j.log4j12}</version>
      </dependency>      
      
      <dependency>
        <groupId>log4j</groupId>
        <artifactId>log4j</artifactId>
        <version>${version.log4j}</version>
      </dependency>
      
      <dependency>
      	<groupId>org.apache.mina</groupId>
      	<artifactId>mina-core</artifactId>
        <version>${version.mina}</version>      
      </dependency>      
	    
	    <dependency>
	  		<groupId>org.apache.mina</groupId>
	  		<artifactId>mina-transport-serial</artifactId>
	  		<version>${version.mina}</version>
	  	</dependency>
	  	      
      <dependency>
        <groupId>commons-codec</groupId>
        <artifactId>commons-codec</artifactId>
        <version>${version.codec}</version>      
      </dependency>
	    
      <dependency>
        <groupId>org.testng</groupId>
        <artifactId>testng</artifactId>
        <version>${version.testng}</version>
        <scope>test</scope>
        <!--classifier>jdk15</classifier-->
      </dependency>

      <dependency>
      	<groupId>io.netty</groupId>
      	<artifactId>netty-all</artifactId>
      	<version>${version.netty}</version>
      </dependency>      
      
      <dependency>
	  		<groupId>commons-net</groupId>
	  		<artifactId>commons-net</artifactId>
	  		<version>${version.net}</version>
	  	</dependency>
    </dependencies>
    
  </dependencyManagement>

  <!-- ==================================== -->
  <!-- Common dependencies for all projects -->
  <!-- ==================================== -->
  <dependencies>
    <dependency>
      <groupId>org.testng</groupId>
      <artifactId>testng</artifactId>   
    </dependency>
  </dependencies>

  <profiles>
    <!--profile>
      <id>serial</id>
      <activation>
        <property>
          <name>with-LGPL-dependencies</name>
        </property>
      </activation>
      <modules>
        <module>mina-transport-serial</module>
      </modules>
    </profile-->

    <profile>
      <id>ate-release</id>
      <build>
        <plugins>
          <plugin>
            <artifactId>maven-javadoc-plugin</artifactId>
            <executions>
              <execution>
                <phase>install</phase>

                <goals>
                  <goal>javadoc</goal>
                </goals>
                <configuration>
                  <aggregate>true</aggregate>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <artifactId>maven-jxr-plugin</artifactId>
            <configuration>
              <aggregate>true</aggregate>
            </configuration>
            
            <executions>
              <execution>
                <phase>install</phase>

                <goals>
                  <goal>jxr</goal>
                  <goal>test-jxr</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
      
      <modules>
        <module>distribution</module>
      </modules>
    </profile>
  
    <profile>
      <id>windows</id>
      <activation>
        <os>
          <family>windows</family>
        </os>
      </activation>
      <properties>
          <envClassifier>windows</envClassifier>
          <excludesOS>linux</excludesOS>
      </properties>
    </profile>
    <profile>
      <id>linux</id>
      <activation>
        <os>
          <family>unix</family>
        </os>
      </activation>
      <properties>
          <envClassifier>linux</envClassifier>
          <excludesOS>windows</excludesOS>
      </properties>
    </profile>
  
  </profiles>
  
  <build>  	
    <pluginManagement>
    	<plugins>
      	 <plugin>
	        <artifactId>maven-clean-plugin</artifactId>
	        <version>2.5</version>
	        <configuration>
			      <filesets>
			        <fileset>
			          <directory>.</directory>
			          <includes>			       
			            <include>logs/**</include>			            
			          </includes>
			        </fileset>
			      </filesets>
			    </configuration>		        
	      </plugin> 
	      
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-assembly-plugin</artifactId>
          <version>${version.assembly.plugin}</version>	        
	        <configuration>
	        		<finalName>itdd</finalName>
	  				  <appendAssemblyId>false</appendAssemblyId>	  				  
	            <descriptor>assembly/dep.xml</descriptor>
	        </configuration>	          
	        <executions>
	        	<execution>
	            <id>make-assembly</id> 
	            <phase>package</phase> 	            
	            <goals>
	              <goal>single</goal>
	            </goals>            
	          </execution>
	        </executions>
        </plugin>    
        <plugin>
	        <artifactId>maven-compiler-plugin</artifactId>
	        <version>${version.compiler.plugin}</version>
	        <configuration>
	          <encoding>UTF-8</encoding>
	          <source>1.7</source>
	          <target>1.7</target>
	          <debug>true</debug>
	          <optimize>true</optimize>
	          <showDeprecations>true</showDeprecations>
	        </configuration>
	      </plugin> 
      </plugins>
    </pluginManagement>

    <plugins>
    	<!--plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>license-maven-plugin</artifactId>
        <version>1.5</version>
        <configuration>
          <verbose>true</verbose>          
        </configuration>
        <executions>
          <execution>
            <id>first</id>
            <goals>
              <goal>update-file-header</goal>
            </goals>
            <phase>process-sources</phase>
            <configuration>            	
              <licenseName>apache_v2</licenseName>
              <includes>
                <include>**/ate/**</include>
                <include>**/Main.java</include>
              </includes>
            </configuration>
          </execution>
        </executions>
      </plugin-->
      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
      </plugin>

      <plugin>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${version.surefire.plugin}</version>
        
        <configuration>
        	<testFailureIgnore>true</testFailureIgnore>
        	<showSuccess>true</showSuccess>
        	<aggregate>true</aggregate>
          <argLine>-Xmx1024m</argLine>
          <excludedGroups>${excludesOS}</excludedGroups>
        </configuration>
      </plugin>
        
			<plugin>
			    <artifactId>maven-scm-plugin</artifactId>
			    <version>${version.scm.plugin}</version>
			    <configuration>
			        <scmVersionType>branch</scmVersionType>
			        <scmVersion>master</scmVersion>
			    </configuration>
			</plugin>
			          
			<plugin>
			    <artifactId>maven-release-plugin</artifactId>
			    <version>${version.release.plugin}</version>
			    <configuration>
			        <autoVersionSubmodules>true</autoVersionSubmodules>
			        <scmCommentPrefix/>
			        <checkModificationExcludes>
	            	<checkModificationExclude>pom.xml</checkModificationExclude>
	            </checkModificationExcludes>
			    </configuration>
			</plugin>
			
      <!--plugin>
        <artifactId>maven-source-plugin</artifactId>
        <version>${version.source.plugin}</version>
        <executions>
          <execution>
            <id>attach-source</id>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin-->

      <plugin>
        <groupId>org.apache.felix</groupId>
        <artifactId>maven-bundle-plugin</artifactId>
        <version>${version.bundle.plugin}</version>
        <inherited>true</inherited>
        <extensions>true</extensions>
      </plugin>
    </plugins>
  </build>

  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>${version.javadoc.plugin}</version>
        <configuration>
        	<encoding>UTF-8</encoding>
          
          <breakiterator>true</breakiterator>
          <charset>UTF-8</charset>
          <docencoding>UTF-8</docencoding>
          <windowtitle>iTDD Framework ${project.version} API Documentation</windowtitle>
          <doctitle>iTDD Framework ${project.version} API Documentation</doctitle>          
          <excludePackageNames>org.**</excludePackageNames>
          <links>
            <link>http://docs.oracle.com/javase/6/docs/api/</link>
            <link>http://groovy.codehaus.org/api/</link>            
            <link>http://testng.org/javadocs/</link>
            <link>http://mina.apache.org/mina-project/apidocs/</link>
            <link>http://netty.io/4.0/api/</link>
            <link>http://logging.apache.org/log4j/1.2/apidocs/</link>
          </links>
        </configuration>
        
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jxr-plugin</artifactId>
        <version>${version.jxr.plugin}</version>
        
        <reportSets>
          <reportSet>
            <id>aggregate</id>
            <inherited>false</inherited>
            <reports>
              <report>aggregate</report>
            </reports>
          </reportSet>
        </reportSets>
        
        <configuration>
          <inputEncoding>UTF-8</inputEncoding>
          <outputEncoding>UTF-8</outputEncoding>
          <windowTitle>iTDD Framework ${project.version} Cross Reference</windowTitle>
          <docTitle>iTDD Framework ${project.version} Cross Reference</docTitle>
        </configuration>
      </plugin>
      
      <plugin>  
          <groupId>org.apache.maven.plugins</groupId>
	        <artifactId>maven-surefire-report-plugin</artifactId>
	        <version>2.16</version>
	        <configuration>
	          <aggregate>true</aggregate>
	        </configuration>
      </plugin> 
      
      <!--plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>rat-maven-plugin</artifactId>
        <version>${version.rat.maven.plugin}</version>
        <configuration>
          <excludes>
            <exclude>**/target/**/*</exclude>
            <exclude>**/.*</exclude>
            <exclude>**/NOTICE.txt</exclude>
            <exclude>**/LICENSE*.txt</exclude>
          </excludes>
          <excludeSubProjects>false</excludeSubProjects>
        </configuration>
      </plugin-->
    </plugins>
  </reporting>
</project>