package ate;

public interface IService {
	public final static int SVC_START=1;
	public final static int SVC_STOP=0;
	void start();
	void stop();
	String getName();
	String getDesc();
	boolean isStarted();
	
}
