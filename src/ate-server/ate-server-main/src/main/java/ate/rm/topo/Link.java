package ate.rm.topo;

import java.util.ArrayList;

public class Link {
	int from;
	int to;
	int type;

	public Link(int from, int to, int type) {
		this.from = from;
		this.to = to;
		this.type = type;
	}
	public static void main(String[] args){
		Topology pTopo = new Topology();
		Topology lTopo = new Topology();

		DUT rt1 = new DUT("RT1", 1);
		rt1.addLinks(2, 5,4,7,8);
		DUT rt2 = new DUT("RT2", 2);
		rt2.addLinks(1, 3, 4, 6,5);
		DUT rt3 = new DUT("RT3", 3);
		rt3.addLinks(2, 5, 6);

		DUT rt4 = new DUT("RT4", 4);
		rt4.addLinks(2, 5,1);

		DUT rt5 = new DUT("RT5", 5);
		rt5.addLinks(1, 3, 4, 6,2);

		DUT rt6 = new DUT("RT6", 6);
		rt6.addLinks(2, 3, 5);
		DUT rt7 = new DUT("RT7", 7);
		rt7.addLinks(1);
		DUT rt8 = new DUT("RT8", 8);
		rt8.addLinks(1);
		
		pTopo.addDuts(rt1, rt2, rt3, rt4, rt5, rt6,rt7,rt8);
		
		// logical topo
		DUT r1 = new DUT("R1", 1);
		r1.addLinks(2, 3);
		DUT r2 = new DUT("R2", 2);
		r2.addLinks(1, 3);
		DUT r3 = new DUT("R3", 3);
		r3.addLinks(1, 2,4);
		DUT r4 = new DUT("R4", 4);
		r4.addLinks(3);
		DUT r5 = new DUT("R5", 5);
		r5.addLinks(4,6);
		DUT r6 = new DUT("R6", 6);
		r6.addLinks(4, 5);
		lTopo.addDuts(r1, r2, r3,r4);
		
		System.out.println(pTopo.validate(lTopo));
		
		ArrayList al=new ArrayList();
		al.add(2);
		al.add(5);
//		al.add(3);
		pTopo.calcReleDegree(al);
		pTopo.printDetail();
		ArrayList al1=new ArrayList();
		al1.add(4);
		al1.add(3);
//		al1.add(6);
//		al1.add(5);		
		lTopo.calcReleDegree(al1);
		lTopo.printDetail();
	}
}