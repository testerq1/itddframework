package ate.rm.topo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Topology {
	private ArrayList<DUT> duts = new ArrayList<DUT>();
	
	private ArrayList<Link> links = new ArrayList<Link>();
	private int[] weight;
	int[] degree;
	public void build(){
		Collections.sort(duts);
		Collections.reverse(duts);
		weight=new int[duts.size()];
		
		for(int i=0;i<duts.size();i++){
			DUT dut=duts.get(i);
			weight[i]=dut.getLinks().size();
		}
	}
	public int[] getWeight(){
		return weight;
	}
	public boolean validate(Topology topo){	
		topo.build();
		this.build();
		int min=topo.getWeight()[topo.getWeight().length-1];
		boolean removed=false;
		ArrayList<DUT> removedDuts=new ArrayList<DUT>();
		for(int i=weight.length-1;i>0;i--){
			if(weight[i]<min){
				removedDuts.add(duts.remove(i));
				removed=true;				
			}
		}		
		
		if(removed){
			for(DUT rmTmp:removedDuts){
				int seq=rmTmp.getSeq();
				for(DUT tmp:duts){				
					tmp.removeLink(seq);
				}
				for(int i=0;i<links.size();i++){
					if(links.get(i).from==seq||links.get(i).to==seq)
						links.remove(i);
				}
			}
			topo.build();
			this.build();
		}
		
		if(topo.getDuts().size()>duts.size()||topo.getLinks().size()>links.size())
			return false;
		
		return true;
		
	}
	
	public int[] calcReleDegree(List posted){	
		degree=new int[duts.size()];
		for(int i=0;i<duts.size();i++){
			DUT dut=duts.get(i);
			for(int j=0;j<dut.getLinks().size();j++){
				Link link=dut.getLinks().get(j);
				if(!posted.contains(link.to))
					degree[i]++;
			}
		}
		return degree;
	}
	public void resetWeight(){
		this.weight=degree;
	}
	
	public void printDetail(){
		System.out.println("Print Topo Begin:");
		int sumD=0;
		int sumW=0;
		for(int i=0;i<duts.size();i++){
			sumD+=degree[i];
			sumW+=weight[i];
		}
		System.out.println("DV:"+(sumW-sumD));
		System.out.println("Links:"+links.size());
		
		for(int i=0;i<duts.size();i++){
			DUT dut=duts.get(i);
			System.out.println("\t DUT "+dut.getName()+": "+degree[i]+" "+weight[i]);
		}
		System.out.println("Print Topo End!");
	}
	public void addDuts(DUT... dut) {
		for (int i = 0; i < dut.length; i++) {
			this.duts.add(dut[i]);
		}
		ArrayList tmp = new ArrayList();
		for (int i = 0; i < duts.size(); i++) {
			ArrayList<Link> ls = duts.get(i).getLinks();
			for (int j = 0; j < ls.size(); j++) {
				if (tmp.contains(ls.get(j).to))
					continue;
				else
					links.add(ls.get(j));
			}
			tmp.add(duts.get(i).seq);
		}
	}

	public ArrayList<DUT> getDuts() {
		return this.duts;
	}

	public ArrayList<Link> getLinks() {
		return links;
	}
	public HashMap map(){
		HashMap hm=new HashMap();
		boolean done=false;
		for(int j=0;j<links.size();j++){
			Link link=links.get(j);
			while(done){
				for(int i=0;i<duts.size();i++){
					
				}
			}
		}
		
		return hm;
	}
}