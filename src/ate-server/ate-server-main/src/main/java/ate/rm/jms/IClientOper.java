package ate.rm.jms;

public interface IClientOper {
	boolean addClient(String name);
	boolean removeClient(String name);
}
