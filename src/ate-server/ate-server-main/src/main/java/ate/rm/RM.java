/*
 * 
 */
package ate.rm;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ate.rm.dev.Host;

/**
 * The Class Utils.
 */
public class RM {
	/** The spring. */
	private static ApplicationContext spring;

	/**
	 * Gets the spring context.
	 *
	 * @return the spring context
	 */
	public static ApplicationContext getSpringContext() {	
		if(spring==null)
			spring = new ClassPathXmlApplicationContext(
					new String[] { "file:"+Host.HOME
							+File.separator+ "conf"+File.separator+"spring_camel.xml" });
		return spring;
	}
	
	/**
	 * Gets the message queue address.
	 *
	 * @return the mQ address
	 */
	public static String getMQAddress(){
		
		String url=((ActiveMQConnectionFactory)getSpringContext().getBean("jmsConnectionFactory")).getBrokerURL();
		
		return url.substring(url.lastIndexOf("/")+1,url.lastIndexOf(":"));
	}
	
	/**
	 * Gets the message queue addr port.
	 *
	 * @return the mQ addr port
	 * @throws Exception the exception
	 */
	public static int getMQAddrPort()throws Exception{
		String url=((ActiveMQConnectionFactory)getSpringContext().getBean("jmsConnectionFactory")).getBrokerURL();
		String port=url.substring(url.lastIndexOf(":")+1);
		return Integer.parseInt(port);
		
	}
	
	/**
	 * Load_jars.
	 *
	 * @param file the file
	 * @throws Exception the exception
	 */
	static void load_jars(File file)throws Exception{		
		Method addURL = URLClassLoader.class.getDeclaredMethod("addURL",
				new Class[] { URL.class });
		addURL.setAccessible(true);
		ClassLoader sysloader = Thread.currentThread().getContextClassLoader();
		if (file.isFile() && file.getName().endsWith(".jar"))
			try {					
				addURL.invoke(sysloader, new Object[] { file.toURI().toURL() });
			} catch (Exception e) {
				e.printStackTrace();
			}
	}	
}
