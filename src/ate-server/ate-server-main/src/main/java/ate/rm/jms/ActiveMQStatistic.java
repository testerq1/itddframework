package ate.rm.jms;

import java.util.Enumeration;

import javax.jms.ConnectionFactory;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TemporaryQueue;

import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.jms.JmsConfiguration;

import ate.rm.RM;

public class ActiveMQStatistic {
	private static ActiveMQStatistic instance;
	 
	Session session;
	ConnectionFactory cf;
	private ActiveMQStatistic(){
		CamelContext context=(CamelContext)RM.getSpringContext().getBean("camel");;
		ActiveMQComponent amq=(ActiveMQComponent)context.getComponent("activemq");
		JmsConfiguration cnf= amq.getConfiguration();
		cf =((PooledConnectionFactory)cnf.getConnectionFactory());		
	}
	
	public static ActiveMQStatistic createInstance(){
		if(instance==null)
			instance=new ActiveMQStatistic();
		return instance;
	}
	
	public static void main(String[]args){
		
	}
	
	public int getTaskQueueSize(String queuename)throws Exception{
	
		session=cf.createConnection().createSession(false, Session.AUTO_ACKNOWLEDGE);
		TemporaryQueue replyTo=session.createTemporaryQueue();
		MessageConsumer consumer = session.createConsumer(replyTo);
			
		
		MessageProducer producer = session.createProducer(null);

		String queueName = "ActiveMQ.Statistics.Destination." + queuename;
		Queue query = session.createQueue(queueName);

		Message msg = session.createMessage();
		msg.setJMSReplyTo(replyTo);
		producer.send(query, msg);
		MapMessage reply = (MapMessage) consumer.receive();
		
		for (Enumeration e = reply.getMapNames();e.hasMoreElements();) {
		    String name = e.nextElement().toString();
		    System.err.println(name + "=" + reply.getObject(name));
		}
		return 0;
	}
	public int getAllRunTaskQueueSize(){
	
		return 0;
	}
	
}
