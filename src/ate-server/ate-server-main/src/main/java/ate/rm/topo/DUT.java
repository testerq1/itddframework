package ate.rm.topo;

import java.util.ArrayList;

public class DUT implements Comparable {
	String name;
	int seq;	
	ArrayList<Link> links = new ArrayList<Link>();
	public void removeLink(int to){
		for(int i=0;i<links.size();i++){
			Link tmp=links.get(i);
			if(tmp.to==to)
				links.remove(i);
		}
	}
	public int getSeq() {
		return seq;
	}
	public String getName() {
		return name;
	}	

	public DUT(String name, int seq) {
		this.name = name;
		this.seq = seq;
	}

	public void addLink(int to, int type) {
		this.links.add(new Link(seq, to, type));
	}

	public void addLink(int to) {
		this.links.add(new Link(seq, to, 1));
	}

	public void addLinks(int... to) {
		for (int i = 0; i < to.length; i++)
			this.links.add(new Link(seq, to[i], 1));
	}

	public ArrayList<Link> getLinks() {
		return this.links;
	}

	public int compareTo(Object o) {
		return this.links.size() - ((DUT) o).getLinks().size();
	}
	public void build(){
		
	}
}