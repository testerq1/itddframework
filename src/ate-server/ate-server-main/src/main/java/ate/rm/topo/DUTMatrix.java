package ate.rm.topo;

import java.util.ArrayList;
import java.util.List;

public class DUTMatrix {
	private int[][] m;
	private ArrayList<Link> links = new ArrayList<Link>();
	private int links_size;
	private ArrayList<DUT> duts = new ArrayList<DUT>();

	public DUTMatrix(Topology topo) {
		links_size = topo.getLinks().size();
		m = new int[topo.getDuts().size()][links_size+1];	
		this.duts = topo.getDuts();

		for (int i = 0; i < links_size; i++) {
			Link ll = topo.getLinks().get(i);
			m[ll.from - 1][i] = 1;
			m[ll.to - 1][i] = 1;
		}
		for(int i=0;i<m.length;i++){
			m[i][links_size]=i+1;
		}
		weight=new int[duts.size()];
		
		for(int i=0;i<duts.size();i++){
			DUT dut=duts.get(i);
			weight[i]=dut.getLinks().size();
		}
		
	}

	boolean neighborCol(MappingStatus ms,int value){
		for(int i=ms.curCol;i<links_size-ms.switchColTimes;i++){
			if(m[ms.curRow][i]==value){
				boolean preEqual=true;
				for(int i1=0;i1<ms.curRow;i1++){
					if(m[i1][i]!=m[i1][ms.curCol]){
						preEqual=false;
						break;
					}
				}
				if(preEqual){
					this.switchColumn(ms.curCol, i);
					ms.switchColTimes++;
					ms.curCol++;
					this.switchColumn(i, links_size-ms.switchColTimes);
					return true;
				}
			}
		}
		return false;
	}
	boolean neighborRow(MappingStatus ms,int value){
		for(int i=ms.curRow;i<m.length-ms.switchedRowTimes;i++){
			if(m[i][ms.curCol]==value){
				boolean preEqual=true;
				for(int i1=0;i1<ms.curCol;i1++){
					if(m[i][i1]!=m[ms.curRow][i1]){
						preEqual=false;
						break;
					}
				}
				if(preEqual){
					this.switchRow(ms.curCol, i);
					ms.switchedRowTimes++;
					ms.curRow++;
					this.switchRow(i, m.length-ms.switchedRowTimes);
					return true;
				}
			}
		}
		return false;
	}
	int getCell(int row,int col){
		return m[row][col];
	}
	int[] getRDegrees(int row) {
		int[] degree = new int[duts.size()];
		for (int i = 0; i < this.duts.size(); i++) {
			int[] tmp = plusRow(row, i);
			for (int j = 0; j < links_size; j++) {
				if (tmp[j] == 2) {
					degree[i]++;
					degree[i] += ((links_size - j) * 1000);
				}
			}
		}

		return degree;
	}

	public void matrix_sort(int[] data) {
		// 数组长度
		int len = data.length;
		for (int i = 0; i < len - 1; i++) {
			// 临时变量
			int temp = 0;
			// 交换标志,false表示未交换
			boolean isExchanged = false;
			for (int j = len - 1; j > i; j--) {
				// 如果data[j]小于data[j - 1],交换
				if (data[j]-data[j - 1] > 0) {
					temp = data[j];
					data[j] = data[j - 1];
					data[j - 1] = temp;
					this.switchRow(j,j-1);
					// 发生了交换,故将交换标志置为真
					isExchanged = true;
				}
			}
			// 本趟排序未发生交换,提前终止算法,提高效率
			if (!isExchanged) {
				break;
			} 
		}
	}

	int getWeight(int row) {
		int q_tmp = 0;
		for (int j = 0; j < m[row].length; j++)
			q_tmp += m[row][j];
		return q_tmp;
	}

	int getRows() {
		return m.length;
	}

	int getCols() {
		return links_size;
	}

	void switchRow(int l1, int l2) {
		int[] t = m[l1];
		m[l1] = m[l2];
		m[l2] = t;
		DUT d1 = duts.get(l1);
		duts.set(l1, duts.get(l2));
		duts.set(l2, d1);
	}

	void switchColumn(int col1, int col2) {
		for (int i = 0; i < m.length; i++) {
			int tmp = m[i][col1];
			m[i][col1] = m[i][col2];
			m[i][col2] = tmp;
		}
	}

	void printMatrix() {
		System.out.println("Print Matrix Begin:");
		for (int i = 0; i < m.length; i++) {
			int[] am = m[i];
			System.out.print(duts.get(i).name + " ");
			for (int j = 0; j < am.length; j++)
				System.out.print(am[j] + " ");
			System.out.println();
		}
		for (int i = 0; i < m.length; i++) {			
			System.out.print(duts.get(i).name + " ");
		}
		System.out.println();

		System.out.println("Print Matrix End!");
	}

	public static int getLevenshteinDistance(int[] s, int[] t) {
		int n = s.length; // length of s
		int m = t.length; // length of t

		if (n == 0) {
			return m;
		} else if (m == 0) {
			return n;
		}

		int p[] = new int[n + 1]; // 'previous' cost array, horizontally
		int d[] = new int[n + 1]; // cost array, horizontally
		int _d[]; // placeholder to assist in swapping p and d

		// indexes into strings s and t
		int i; // iterates through s
		int j; // iterates through t

		int t_j; // jth character of t

		int cost; // cost

		for (i = 0; i <= n; i++) {
			p[i] = i;
		}

		for (j = 1; j <= m; j++) {
			t_j = t[j - 1];
			d[0] = j;

			for (i = 1; i <= n; i++) {
				cost = s[i - 1] == t_j ? 0 : 1;
				// minimum of cell to the left+1, to the top+1, diagonally left
				// and up +cost
				d[i] = Math.min(Math.min(d[i - 1] + 1, p[i] + 1), p[i - 1]
						+ cost);
			}

			// copy current distance counts to 'previous row' distance counts
			_d = p;
			p = d;
			d = _d;
		}

		// our last action in the above loop was to switch d and p, so p now
		// actually has the most recent cost counts
		return p[n];
	}

	void optimize(boolean isFirst) {
		if(isFirst){
			int max = 0;
			int q = 0;
			for (int i = 0; i < duts.size(); i++) {
				int q_tmp = getWeight(i);
				if (q < q_tmp) {
					max = i;
					q = q_tmp;
				}
			}
			if (max != 0)
				switchRow(0, max);		
		}
		matrix_sort(getRDegrees(0));
		col_optimize(0,links_size);
		if(duts.size()>0)
			col_optimize(1,getWeight(0));
	}
	void col_optimize(int beginRow,int colSize){
		for (int i = 0; i < colSize; i++) {
			if (m[beginRow][i] == 1)
				continue;
			for (int j = colSize - 1; j > 0; j--) {
				if (j <= i){					
					return;
				}
				if (m[beginRow][j] == 1) {
					switchColumn(i, j);
					break;
				}
			}
		}
		
	}
	int[] plusRow(int r1, int r2) {
		int[] tmp = new int[m[0].length];
		for (int i = 0; i < m[0].length; i++) {
			tmp[i] = m[r1][i] + m[r2][i];
		}
		return tmp;
	}
	int[] degree;
	private int[] weight;
	public int[] calcReleDegree(List posted){	
		degree=new int[duts.size()];
		for(int i=0;i<duts.size();i++){
			DUT dut=duts.get(i);
			for(int j=0;j<dut.getLinks().size();j++){
				Link link=dut.getLinks().get(j);
				if(!posted.contains(link.to))
					degree[i]++;
			}
		}
		return degree;
	}
	public void matrixSort(int seq){
		
	}
	public static void main(String[] args){
		Topology pTopo = new Topology();
		// LogicalTopo lTopo=new LogicalTopo();
		Topology lTopo = new Topology();
		DUT rt1 = new DUT("RT1", 1);
		rt1.addLinks(2, 4, 5);
		DUT rt2 = new DUT("RT2", 2);
		rt2.addLinks(1, 3, 4, 5, 6);
		DUT rt3 = new DUT("RT3", 3);
		rt3.addLinks(2, 5, 6);

		DUT rt4 = new DUT("RT4", 4);
		rt4.addLinks(1, 2, 5);

		DUT rt5 = new DUT("RT5", 5);
		rt5.addLinks(1, 2, 3, 4, 6);

		DUT rt6 = new DUT("RT6", 6);
		rt6.addLinks(2, 3, 5);

		pTopo.addDuts(rt1, rt2, rt3, rt4, rt5, rt6);
		DUTMatrix pm = new DUTMatrix(pTopo);
		pm.printMatrix();
		
		// logical topo
		DUT r1 = new DUT("R1", 1);
		r1.addLinks(2, 3);
		DUT r2 = new DUT("R2", 2);
		r2.addLinks(1, 3);
		DUT r3 = new DUT("R3", 3);
		r3.addLinks(1, 2);
		lTopo.addDuts(r1, r2, r3);
	}

}