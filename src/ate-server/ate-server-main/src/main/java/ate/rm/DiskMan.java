package ate.rm;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.StringTokenizer;

public class DiskMan {
	/**
	 * Gets the Free Space on Linux
	 * 
	 * @param path
	 *            String
	 * @return long Value
	 * @throws Exception
	 */
	private static long getFreeSpaceOnLinux(String path) throws Exception {
		long bytesFree = -1;
		Process processOnLinux = Runtime.getRuntime().exec("df " + path);
		InputStream reader = new BufferedInputStream(
				processOnLinux.getInputStream());
		StringBuffer buffer = new StringBuffer();
		for (;;) {
			int charCount = reader.read();
			if (charCount == -1)
				break;
			buffer.append((char) charCount);
		}
		String outputText = buffer.toString();
		reader.close();

		// parse the output text for the bytes free info
		StringTokenizer tokenizer = new StringTokenizer(outputText, "\n");
		tokenizer.nextToken();
		if (tokenizer.hasMoreTokens()) {
			String line = tokenizer.nextToken();
			StringTokenizer tokenizerSecond = new StringTokenizer(line, " ");
			if (tokenizerSecond.countTokens() >= 4) {
				tokenizerSecond.nextToken();
				tokenizerSecond.nextToken();
				tokenizerSecond.nextToken();
				bytesFree = Long.parseLong(tokenizerSecond.nextToken());
				return bytesFree;
			}

			if (bytesFree > 0) {
				return bytesFree;
			} else {
				return bytesFree;
			}
		}
		throw new Exception("Can not read the free space of " + path + " path");
	}

	public static void main(String[] args) {
		File[] fss = File.listRoots();
		for (File file : fss) {
			long totalSpace = file.getTotalSpace(); // total disk space in
													// bytes.
			long usableSpace = file.getUsableSpace(); // /unallocated / free
														// disk space in bytes.
			long freeSpace = file.getFreeSpace(); // unallocated / free disk
													// space in bytes.

			System.out.println(" === Partition Detail ===");

			System.out.println(" === bytes ===");
			System.out.println("Total size : " + totalSpace + " bytes");
			System.out.println("Space free : " + usableSpace + " bytes");
			System.out.println("Space free : " + freeSpace + " bytes");

			System.out.println(" === mega bytes ===");
			System.out.println("Total size : " + totalSpace / 1024 / 1024
					+ " mb");
			System.out.println("Space free : " + usableSpace / 1024 / 1024
					+ " mb");
			System.out.println("Space free : " + freeSpace / 1024 / 1024
					+ " mb");
		}
	}

}
