package ate.rm.topo;

class MappingStatus{
	int matchedRow=0;
	int matchedCol=0;
	int switchedRowTimes=0;
	int switchColTimes=0;
	int curRow=0;
	int curCol=0;
}
public class Topo {
	Topology pTopo = new Topology();
	// LogicalTopo lTopo=new LogicalTopo();
	Topology lTopo = new Topology();

	boolean anneal(DUTMatrix pm, DUTMatrix lm) {
		MappingStatus ms=new MappingStatus();
		for (int i = 0; i < lm.getRows(); i++) {
			for(int j=0;j<lm.getCols();j++){
				if(lm.getCell(i, j)!=pm.getCell(i, j)){
					if(!pm.neighborCol(ms, lm.getCell(i, j))&&!pm.neighborRow(ms, lm.getCell(i, j))){
						return false;							
					}
				}
			}				
		}
		return true;
	}
	boolean equal(DUTMatrix pm,DUTMatrix lm){
		for (int i = 0; i < lm.getRows(); i++) {
			for(int j=0;j<lm.getCols();j++){
				if(lm.getCell(i, j)!=pm.getCell(i, j))
					return false;
			}
			
		}
		return true;
	}
	boolean map() {
		DUTMatrix pm = new DUTMatrix(pTopo);
		pm.printMatrix();
		
		DUTMatrix lm = new DUTMatrix(lTopo);
		lm.optimize(true);			
		
		for (int i = 0; i < pm.getRows()-1; i++) {
			if(pm.getWeight(0) < lm.getWeight(0)||!anneal(pm,lm))					
				pm.switchRow(i,i+1);
			else
				return true;	
		}
			
		return false;
	}

	public Topo() {
		// testbed
		DUT rt1 = new DUT("RT1", 1);
		rt1.addLinks(2, 4, 5);
		DUT rt2 = new DUT("RT2", 2);
		rt2.addLinks(1, 3, 4, 5, 5, 6);
		DUT rt3 = new DUT("RT3", 3);
		rt3.addLinks(2, 5, 6);

		DUT rt4 = new DUT("RT4", 4);
		rt4.addLinks(1, 2, 5);

		DUT rt5 = new DUT("RT5", 5);
		rt5.addLinks(1, 2, 2, 3, 4, 6);

		DUT rt6 = new DUT("RT6", 6);
		rt6.addLinks(2, 3, 5);

		pTopo.addDuts(rt1, rt2, rt3, rt4, rt5, rt6);

		// logical topo
		DUT r1 = new DUT("R1", 1);
		r1.addLinks(2, 3);
		DUT r2 = new DUT("R2", 2);
		r2.addLinks(1, 3);
		DUT r3 = new DUT("R3", 3);
		r3.addLinks(1, 2);
		lTopo.addDuts(r1, r2, r3);
		map();
	}

	public static void main(String[] args) {
		new Topo();
	}
}

