package ate;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

//import ate.actions.MibCompileCommand;
import ate.actions.RunTestScriptCommand;
import ate.rm.dev.Host;
import ate.ua.mina.UAClient;
import ate.util.MyConsole;

public class Shell implements Runnable {
	private static final String NO_CONSOLE = "Error: Console unavailable";
	private static final String GREETINGS = "Welcome to the System. Please login.%n";
	private static final String DENIED_ATTEMPT = "Wrong user name or password [%1$d]%n";
	private static final String ACCESS_DENIED = "Access denied%n";
	private static final String ACCESS_GRANTED = "Access granted%n";
	private static final String UNKNOWN_COMMAND = "Unknown command [%1$s]%n";
	private static final String COMMAND_ERROR = "Command error [%1$s]: [%2$s]%n";

	private static final String TIME_FORMAT = "%1$tH:%1$tM:%1$tS";
	private static final String PROMPT = TIME_FORMAT + " $ ";
	private static final String USER_PROMPT = TIME_FORMAT + " User: ";
	private static final String PASS_PROMPT = TIME_FORMAT
			+ " Password [%2$s]: ";

	private static final String USER = "ravi";
	private static final String PASS = "tsy";
	static MyConsole console;
	private final Method[] mthds;
	public static Shell shell; 
	Actions action=new Actions();	
	public static boolean isActive=false;
	private Shell() {
		isActive=true;
		System.out.println(Host.HOME);
		mthds=Actions.class.getMethods();
	}
	public Shell getShell(){
		if(shell==null)
			shell=new Shell();
		return shell;
	}
	class Actions{	
		UAClient client;
		public void run(String ... paras)throws Exception{
			new RunTestScriptCommand().execute(new String[]{});
		}
		public void test(String ... paras){
			console.println("i'm test!");
		}
//		public void mibc(String ... paras)throws Exception{
//			new MibCompileCommand().execute(null);
//		}
		
//		public void sendStr(String s)throws Exception{
//			client.send_string(s);
//		}
//		public void sendHex(String s)throws Exception{		
//			client.sendHex(s);
//		}
//		public void cron(String ... paras) throws Exception{
//			CronCommand.createInstance().execute(paras);		
//		}
	}
	
	public static void main(String[] args) throws Exception {
		new Thread(new Shell()).start();
	}

	@Override
	public void run() {
		try {
			if (console == null)
				console = MyConsole.getMyConsole();

			if (console != null)
				while (true) {
					String commandLine = console.readLine(PROMPT, new Date());
					Scanner scanner = new Scanner(commandLine);

					if (scanner.hasNext()) {
						final String commandName = scanner.next();

						try {
							Method m=null;
							ArrayList<String> paras=new ArrayList<String>();
							while(scanner.hasNext())
								paras.add(scanner.next());
							
							for(Method tmp:mthds)
								if(tmp.getName().equalsIgnoreCase(commandName)){
									m=tmp;
									break;
								}
							
							String[] ps=new String[paras.size()];
							paras.toArray(ps);
							
							if(m!=null){
								if(m.getParameterTypes().length==0)
									m.invoke(action, ps);
								else
									m.invoke(action, ps);
							} else
								console.println("不支持的方法");
							
						} catch (Exception e) {
							e.printStackTrace();
							console.printf(UNKNOWN_COMMAND, commandName);
						}
					}

					scanner.close();
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// private static boolean login(MyConsole console) throws Exception{
	// console.printf(GREETINGS);
	//
	// boolean accessGranted = false;
	// int attempts = 0;
	// while (!accessGranted && attempts < 3) {
	// String name = console.readLine(USER_PROMPT, new Date());
	// String passdata = console.readPassword(PASS_PROMPT, new Date(),
	// name);
	// if (USER.equals(name) && PASS.equals(passdata)) {
	// attempts = 0;
	// accessGranted = true;
	// break;
	// }
	//
	// console.printf(DENIED_ATTEMPT, ++attempts);
	// }
	//
	// if (!accessGranted) {
	// console.printf(ACCESS_DENIED);
	// return false;
	// }
	//
	// console.printf(ACCESS_GRANTED);
	// return true;
	// }


}

