package ate.comp.agent;

import java.io.IOException;

import org.apache.camel.CamelContext;
import org.snmp4j.agent.DuplicateRegistrationException;
import org.snmp4j.agent.MOGroup;
import org.snmp4j.agent.MOServer;
import org.snmp4j.agent.NotificationOriginator;
import org.snmp4j.agent.io.DefaultMOPersistenceProvider;
import org.snmp4j.agent.mo.DefaultMOFactory;
import org.snmp4j.agent.mo.MOAccessImpl;
import org.snmp4j.agent.mo.MOFactory;
import org.snmp4j.agent.mo.MOScalar;
import org.snmp4j.agent.mo.MOTableIndex;
import org.snmp4j.agent.mo.MOTableSubIndex;
import org.snmp4j.agent.util.IndexGenerator;
import org.snmp4j.log.LogAdapter;
import org.snmp4j.log.LogFactory;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.AbstractVariable;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.SMIConstants;
import org.snmp4j.smi.Variable;

import ate.rm.jms.IClientOper;

public class TtMgtMib implements MOGroup,IClientOper {
	private static final LogAdapter LOGGER = LogFactory
			.getLogger(TtMgtMib.class);
	private CamelContext camelContext;
	private MOScalar saveConfig;
	private MOScalar reboot;
	private static MOFactory moFactory = DefaultMOFactory.getInstance();
	private NotificationOriginator notificationOriginator;
	final DefaultMOPersistenceProvider defaultPersistenceProvider;
	private static final int ttIndex1_base = 2;
	
	protected static final OID oldTtSaveConfig = new OID(new int[] { 1, 3, 6,
			1, 4, 1, 65535, 1, 1, 1, 1, 0 });
	protected static final OID oldTtSysReboot = new OID(new int[] { 1, 3, 6, 
			1, 4, 1, 65535, 1, 1, 1, 2, 0 });
	protected static final OID oldTtSysReloadLogConfig=new OID(new int[] { 1, 3, 6, 
			1, 4, 1, 65535, 1, 1, 1, 3, 0 });
		
		//Log4jConfigurer cfg;

	TtMgtMibSysTaskTable sysTaskTable;
	TtMgtMibSysSvcTable sysSvcTable;
	TtMgtMibSysAllRunTaskTable sysAllRunTable;
	TtMgtMibSysClientTable sysClientTable;
	public TtMgtMib(DefaultMOPersistenceProvider provider) {
		this.defaultPersistenceProvider=provider;
		this.saveConfig = 
			new MOScalar(new OID(oldTtSaveConfig),
				MOAccessImpl.ACCESS_READ_WRITE, new Integer32(0)) {
			public Variable getValue() {
				return new Integer32(0);
			}

			public int setValue(Variable value) {
				try {
					defaultPersistenceProvider.store(defaultPersistenceProvider.getDefaultURI());
				} catch (IOException e) {					
					e.printStackTrace();
					return SnmpConstants.SNMP_ERROR_NO_CREATION;
				}
				return SnmpConstants.SNMP_ERROR_SUCCESS;
			}
		};

		this.reboot = new MOScalar(new OID(
				this.oldTtSysReboot), MOAccessImpl.ACCESS_READ_WRITE,
				new OctetString("")){
			public Variable getValue() {
				return new OctetString("");
			}

			public int setValue(Variable value) {
				String v = value.toString();
				if(v!=null&&v.length()>0){
					System.exit(99);
				}
				return SnmpConstants.SNMP_ERROR_BAD_VALUE;
			}			
		};	
		
	}
	
	public TtMgtMib(TTAgentConsumer processor,DefaultMOPersistenceProvider defaultPersistenceProvider) {
		this(defaultPersistenceProvider);
		sysTaskTable=new TtMgtMibSysTaskTable(processor,moFactory);
		sysSvcTable= TtMgtMibSysSvcTable.createInstance(moFactory);
		sysAllRunTable=new TtMgtMibSysAllRunTaskTable(processor,moFactory);
		sysClientTable=TtMgtMibSysClientTable.createInstance(moFactory);
	}	

	public void registerMOs(MOServer server, OctetString context)
			throws DuplicateRegistrationException {
		server.register(saveConfig, context);
		server.register(this.reboot, context);
		
		server.register(sysTaskTable.getTtSysTaskEntry(), context);
		server.register(sysSvcTable.getTtSysSvcEntry(), context);
		server.register(sysAllRunTable.getTtSysAllRunTaskEntry(), context);
		server.register(sysClientTable.getTtSysClientEntry(), context);
		
	}
	
	public void unregisterMOs(MOServer server, OctetString context) {
		server.unregister(saveConfig, context);
		server.unregister(this.reboot, context);
		server.unregister(sysTaskTable.getTtSysTaskEntry(), context);
		server.unregister(sysSvcTable.getTtSysSvcEntry(), context);
		server.unregister(sysAllRunTable.getTtSysAllRunTaskEntry(), context);
		server.unregister(sysClientTable.getTtSysClientEntry(), context);
	}

	/**
	 * Sets the <code>NotificationOriginator</code> to be used for sending the
	 * authenticationFailure trap.
	 * 
	 * @param notificationOriginator
	 *            a NotificationOriginator instance or <code>null</code> to
	 *            disable authenticationFailure traps (default).
	 */
	public void setNotificationOriginator(
			NotificationOriginator notificationOriginator) {
		this.notificationOriginator = notificationOriginator;
	}

	/**
	 * Gets the notification originator used for sending authenticationFailure
	 * traps.
	 * 
	 * @return a NotificationOriginator.
	 * @since 1.2
	 */
	public NotificationOriginator getNotificationOriginator() {
		return notificationOriginator;
	}

	public OID generatorIndex(MOTableIndex tid, String[] s) {
		MOTableSubIndex[] ttSysTaskEntryIndexes = new MOTableSubIndex[] {
				moFactory.createSubIndex(null, SMIConstants.SYNTAX_INTEGER, 1,
						1),
				moFactory.createSubIndex(null, SMIConstants.SYNTAX_IPADDRESS,
						4, 4), };
		// ttSysTaskEntryModel.getRow(new OID().append(1));
		// generatorIndex(this.ttSysTaskEntryIndex,null);
		tid = moFactory.createIndex(ttSysTaskEntryIndexes, false);
		for (int i = 0; i < tid.size(); i++) {
			MOTableSubIndex subid = tid.getIndex(i);
			Variable o = AbstractVariable
					.createFromSyntax(subid.getSmiSyntax());

			new IndexGenerator(o);
		}
		return null;
	}
	public boolean addClient(String name) {
		try {
			sysClientTable.addRow(name);
			return true;
		} catch (Exception e) {
			
			e.printStackTrace();
			return false;
		}		
	}
	public boolean removeClient(String name) {
		try {
			sysClientTable.deleteRow(name);
			return true;
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}		
	}
}
