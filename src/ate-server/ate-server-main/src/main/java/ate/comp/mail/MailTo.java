package ate.comp.mail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.xerces.util.URI;


public class MailTo {
	static public final String MAILTO_SCHEME = "mailto:";
    private final static int NOT_FOUND = -1;  
    private static final char[] HEX_DIGITS = "0123456789ABCDEF".toCharArray();  
	// All the parsed content is added to the headers.
	private HashMap<String, String> mHeaders;
	// Well known headers
	static private final String TO = "to";
	static private final String BODY = "body";
	static private final String CC = "cc";
	static private final String BCC = "bcc";
	static private final String SUBJECT = "subject";
	
	/***
	 * 
	 * Test to see if the given string is a mailto URL
	 * 
	 * @param url
	 *            string to be tested
	 * 
	 * @return true if the string is a mailto URL
	 */

	public static boolean isMailTo(String url) {
		if (url != null && url.startsWith(MAILTO_SCHEME)) {
			return true;
		}
		return false;
	}

	/***
	 * 
	 * Parse and decode a mailto scheme string. This parser implements
	 * 
	 * RFC 2368. The returned object can be queried for the parsed parameters.
	 * 
	 * @param url
	 *            String containing a mailto URL
	 * 
	 * @return MailTo object
	 * 
	 * @exception Exception
	 *                if the scheme is not a mailto URL
	 */

	public static MailTo parse(String url) throws Exception {
		if (url == null) {
			throw new NullPointerException();
		}
		if (!isMailTo(url)) {
			throw new Exception("Not a mailto scheme");
		}

		// Strip the scheme as the Uri parser can't cope with it.
		String noScheme = url.substring(MAILTO_SCHEME.length());
		URI email = new URI(url);
		MailTo m = new MailTo();
		
		
		// Parse out the query parameters
		String query = email.getQueryString();
		if (query != null) {
			String[] queries = query.split("&");
			for (String q : queries) {
				String[] nameval = q.split("=");
				if (nameval.length == 0) {
					continue;
				}

				// insert the headers with the name in lowercase so that
				// we can easily find common headers
				m.mHeaders.put(decode(nameval[0]).toLowerCase(),
				nameval.length > 1 ? decode(nameval[1]) : null);
			}
		}
		// Address can be specified in both the headers and just after the
		// mailto line. Join the two together.
		String address = email.getPath();
		if (address != null) {
			String addr = m.getTo();
			if (addr != null) {
				address += ", " + addr;
			}
			m.mHeaders.put(TO, address);
		}
		return m;
	}

    /***  
     * Decodes '%'-escaped octets in the given string using the UTF-8 scheme.  
     * Replaces invalid octets with the unicode replacement character  
     * ("\\uFFFD").  
     *  
     * @param s encoded string to decode  
     * @return the given string with escaped octets decoded, or null if  
     *  s is null  
     */ 

    public static String decode(String s) {  
        /**  
        Compared to java.net.URLEncoderDecoder.decode(), this method decodes a 
        chunk at a time instead of one character at a time, and it doesn't  
        throw exceptions. It also only allocates memory when necessary--if  
        there's nothing to decode, this method won't do much.  
        */ 

        if (s == null) {  
            return null;  
        }  
        StringBuilder decoded = null;  
        ByteArrayOutputStream out = null;  
        int oldLength = s.length();  
        // This loop alternates between copying over normal characters and  
        // escaping in chunks. This results in fewer method calls and  
        // allocations than decoding one character at a time.  
        int current = 0;  
        while (current < oldLength) {  
            // Start in "copying" mode where we copy over normal characters.  
            // Find the next escape sequence.  
            int nextEscape = s.indexOf('%', current);  
            if (nextEscape == NOT_FOUND) {  
                if (decoded == null) {  
                    // We didn't actually decode anything.  
                    return s;  
                } else {  
                    // Append the remainder and return the decoded string. 
                    decoded.append(s, current, oldLength);  
                    return decoded.toString();  
                }  
            }    

            // Prepare buffers.  
            if (decoded == null) {  
                // Looks like we're going to need the buffers...  
                // We know the new string will be shorter. Using the old length  
                // may overshoot a bit, but it will save us from resizing the
                // buffer.  
                decoded = new StringBuilder(oldLength); 
                out = new ByteArrayOutputStream(4);  
            } else {  
                // Clear decoding buffer.  
                out.reset();  
            }  

            // Append characters leading up to the escape.  
            if (nextEscape > current) {  
                decoded.append(s, current, nextEscape); 
                current = nextEscape;  
            } else {  
                // assert current == nextEscape  
            }  

            // Switch to "decoding" mode where we decode a string of escape  
            // sequences.    

            // Decode and append escape sequences. Escape sequences look like  
            // "%ab" where % is literal and a and b are hex digits
            try {  
                do {  
                    if (current + 2 >= oldLength) {  
                        // Truncated escape sequence. 
                        out.write(REPLACEMENT);  

                    } else {  
                        int a = Character.digit(s.charAt(current + 1), 16);  
                        int b = Character.digit(s.charAt(current + 2), 16);  
                        if (a == -1 || b == -1) {  
                            // Non hex digits.  
                            out.write(REPLACEMENT);  
                        } else {  
                            // Combine the hex digits into one byte and write.  
                            out.write((a << 4) + b);  
                        }  
                    }  
                    // Move passed the escape sequence.  
                    current += 3;  

                } while (current < oldLength && s.charAt(current) == '%');  
                decoded.append(out.toString(DEFAULT_ENCODING));  
            } catch (UnsupportedEncodingException e) {  
                throw new AssertionError(e);  
            } catch (IOException e) {  

                throw new AssertionError(e);  

            }  

        } 
        return decoded == null ? s : decoded.toString();  

    }  
    private static final String DEFAULT_ENCODING = "UTF-8"; 
    private static final byte[] REPLACEMENT = { (byte) 0xFF, (byte) 0xFD };    
	/***
	 * 
	 * Retrieve the To address line from the parsed mailto URL. This could be
	 * 
	 * several email address that are comma-space delimited.
	 * 
	 * If no To line was specified, then null is return
	 * 
	 * @return comma delimited email addresses or null
	 */

	public String getTo() {
		return mHeaders.get(TO);
	}

	/***
	 * 
	 * Retrieve the CC address line from the parsed mailto URL. This could be
	 * 
	 * several email address that are comma-space delimited.
	 * 
	 * If no CC line was specified, then null is return
	 * 
	 * @return comma delimited email addresses or null
	 */

	public String getCc() {
		return mHeaders.get(CC);
	}
	public String getBcc() {
		return mHeaders.get(BCC);
	}
	/***
	 * 
	 * Retrieve the subject line from the parsed mailto URL.
	 * 
	 * If no subject line was specified, then null is return
	 *   
	 * @return subject or null
	 */

	public String getSubject() {
		return mHeaders.get(SUBJECT);
	}

	/***
	 * 
	 * Retrieve the body line from the parsed mailto URL.
	 * 
	 * If no body line was specified, then null is return
	 * 
	 * @return body or null
	 */

	public String getBody() {
		return mHeaders.get(BODY);
	}

	/***
	 * Retrieve all the parsed email headers from the mailto URL
	 * @return map containing all parsed values
	 */

	public Map<String, String> getHeaders() {
		return mHeaders;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(MAILTO_SCHEME);
		sb.append('?');
		for (Map.Entry<String, String> header : mHeaders.entrySet()) {
			sb.append(encode(header.getKey()));
			sb.append('=');
			sb.append(encode(header.getValue()));
			sb.append('&');
		}
		return sb.toString();

	}
	public static String encode(String s) {  
        return encode(s, null);  
    }

	/***  
     * Returns true if the given character is allowed. 
     * @param c character to check  
     * @param allow characters to allow  
     * @return true if the character is allowed or false if it should be 
     *  encoded  
     */ 

    private static boolean isAllowed(char c, String allow) {  
        return (c >= 'A' && c <= 'Z')  
                || (c >= 'a' && c <= 'z')  
                || (c >= '0' && c <= '9')  
                || "_-!.~'()*".indexOf(c) != NOT_FOUND  
                || (allow != null && allow.indexOf(c) != NOT_FOUND);  
    }

    /***  

     * Encodes characters in the given string as '%'-escaped octets  
     * using the UTF-8 scheme. Leaves letters ("A-Z", "a-z"), numbers  
     * ("0-9"), and unreserved characters ("_-!.~'()*") intact. Encodes  
     * all other characters with the exception of those specified in the  
     * allow argument.  
     *  
     * @param s string to encode  
     * @param allow set of additional characters to allow in the encoded form,  
     *  null if no characters should be skipped  
     * @return an encoded version of s suitable for use as a URI component,  
     *  or null if s is null  
     */ 
    public static String encode(String s, String allow) {  
        if (s == null) {  
            return null;  
        }    
        // Lazily-initialized buffers.  
        StringBuilder encoded = null;   
        int oldLength = s.length();   
        // This loop alternates between copying over allowed characters and  
        // encoding in chunks. This results in fewer method calls and  
        // allocations than encoding one character at a time.  
        int current = 0;  
        while (current < oldLength) {  
            // Start in "copying" mode where we copy over allowed chars.    
            // Find the next character which needs to be encoded.  
            int nextToEncode = current;  
            while (nextToEncode < oldLength  
                    && isAllowed(s.charAt(nextToEncode), allow)) {  
                nextToEncode++;  
            }    
            // If there's nothing more to encode...  
            if (nextToEncode == oldLength) {  
                if (current == 0) {  
                    // We didn't need to encode anything!  
                    return s;  
                } else {  
                    // Presumably, we've already done some encoding. 
                    encoded.append(s, current, oldLength);  
                    return encoded.toString();  
                }  
            }    

            if (encoded == null) {  
                encoded = new StringBuilder();  
            }    

            if (nextToEncode > current) {  
                // Append allowed characters leading up to this point.  
                encoded.append(s, current, nextToEncode);  
            } else {  
                // assert nextToEncode == current  
            }  

            // Switch to "encoding" mode.  
            // Find the next allowed character.  
            current = nextToEncode;  
            int nextAllowed = current + 1;  
            while (nextAllowed < oldLength  
                    && !isAllowed(s.charAt(nextAllowed), allow)) {  
                nextAllowed++;  
            }  
            // Convert the substring to bytes and encode the bytes as  
            // '%'-escaped octets.  
            String toEncode = s.substring(current, nextAllowed); 
            try {  
                byte[] bytes = toEncode.getBytes(DEFAULT_ENCODING);  
                int bytesLength = bytes.length;  
                for (int i = 0; i < bytesLength; i++) {  
                    encoded.append('%');  
                    encoded.append(HEX_DIGITS[(bytes[i] & 0xf0) >> 4]);  
                    encoded.append(HEX_DIGITS[bytes[i] & 0xf]);  

                }  

            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);  
            }  
            current = nextAllowed;  

        }  
        return encoded == null ? s : encoded.toString();  

    }  

	/***
	 * 
	 * Private constructor. The only way to build a Mailto object is through
	 * 
	 * the parse() method.
	 */

	private MailTo() {
		mHeaders = new HashMap<String, String>();
	}
    public static void main(String[] args)throws Exception{
    	String mailto="mailto:astark1@unl.edu,ravi@gmail.com?subject=MailTo%20Comments&cc=ASTARK1@UNL.EDU&bcc=id@internet.node";
    	MailTo url = MailTo.parse(mailto);
    	System.out.println(url.getCc());
    	System.out.println(url.getBody());
    	System.out.println(url.getTo());
    	System.out.println(url.getSubject());
    	System.out.println(url.getBcc());
    }
}
