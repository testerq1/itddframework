package ate.comp.agent;

import org.snmp4j.agent.mo.DefaultMOMutableRow2PCFactory;
import org.snmp4j.agent.mo.DefaultMOMutableTableModel;
import org.snmp4j.agent.mo.MOAccessImpl;
import org.snmp4j.agent.mo.MOColumn;
import org.snmp4j.agent.mo.MOFactory;
import org.snmp4j.agent.mo.MOMutableRow2PC;
import org.snmp4j.agent.mo.MOMutableTableModel;
import org.snmp4j.agent.mo.MOTable;
import org.snmp4j.agent.mo.MOTableIndex;
import org.snmp4j.agent.mo.MOTableSubIndex;
import org.snmp4j.agent.mo.snmp.DisplayString;
import org.snmp4j.log.LogAdapter;
import org.snmp4j.log.LogFactory;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.SMIConstants;
import org.snmp4j.smi.Variable;

public class TtMgtMibSysClientTable {	
	private static final LogAdapter LOGGER = LogFactory
			.getLogger(TtMgtMibSysClientTable.class);
	
	private static final int colTtSysClientName = 2;
	private static final OID oidTtSysClientEntry = new OID(new int[] { 1, 3, 6, 1,	4, 1, 65535, 1, 1, 5, 1 });

	private static final int ttIndex1_base = 2;
	private static MOTableIndex TtSysClientEntryIndex;
	private static MOTableSubIndex[] TtSysClientEntryIndexes;
	private MOFactory moFactory;
	private DefaultMOMutableRow2PCFactory rowFactory;
	private MOTable TtSysClientEntry;

	private MOMutableTableModel TtSysClientEntryModel;
	private static TtMgtMibSysClientTable singletonInstance;

	private TtMgtMibSysClientTable(MOFactory moFactory) {
		this.moFactory = moFactory;

		TtSysClientEntryIndexes = new MOTableSubIndex[] { moFactory
				.createSubIndex(null, SMIConstants.SYNTAX_INTEGER, 1, 1) };
		TtSysClientEntryIndex = moFactory.createIndex(TtSysClientEntryIndexes, false);
		createSysClientEntry();
	}

	public static TtMgtMibSysClientTable createInstance(MOFactory moFactory) {
		if (singletonInstance == null)
			singletonInstance = new TtMgtMibSysClientTable( moFactory);
		return singletonInstance;
	}

	public void addRow(String name) throws Exception {
		Variable[] values = new Variable[] { new OctetString(name)};
		OID index=new OID(new int[] { TtSysClientEntryModel.getRowCount() + 1 });
		TtSysClientEntryModel.addRow(rowFactory.createRow(index, values));
	}

	private void createSysClientEntry() {
		MOColumn[] TtSysClientEntryColumns = new MOColumn[1];

		TtSysClientEntryColumns[colTtSysClientName - ttIndex1_base] = new DisplayString(
				colTtSysClientName, MOAccessImpl.ACCESS_READ_ONLY, null, false);
		
		TtSysClientEntryModel = new DefaultMOMutableTableModel();
		rowFactory=new DefaultMOMutableRow2PCFactory();
		TtSysClientEntryModel.setRowFactory(rowFactory);
		
		TtSysClientEntry = moFactory.createTable(oidTtSysClientEntry,
				TtSysClientEntryIndex, TtSysClientEntryColumns, TtSysClientEntryModel);		
	}

	public MOTable getTtSysClientEntry() {
		return TtSysClientEntry;
	}

	public void deleteRow(String name) throws Exception {
		for (int i = 0; i <= TtSysClientEntryModel.getRowCount(); i++) {
			OID index = oidTtSysClientEntry.append(1);			
			MOMutableRow2PC row = (MOMutableRow2PC) TtSysClientEntryModel
					.getRow(index.append(i));
			if (row.getValue(1).toString().equalsIgnoreCase(name)) {
				TtSysClientEntryModel.removeRow(row.getIndex());
				return;
			}
		}
		throw new Exception("Not find row:" + name);
	}
	 
}
