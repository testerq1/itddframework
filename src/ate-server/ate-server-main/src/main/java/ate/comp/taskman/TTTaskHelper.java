package ate.comp.taskman;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.StringBufferInputStream;

public class TTTaskHelper {
	public static String encode(TTTask f) throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMLEncoder xmlEncoder = new XMLEncoder(baos);
		xmlEncoder.writeObject(f);
		xmlEncoder.close();
		return baos.toString();
	}

	public static TTTask decode(String xml){
		XMLDecoder decoder = new XMLDecoder(new StringBufferInputStream(xml));
		TTTask o = (TTTask) decoder.readObject();
		decoder.close();
		return o;
	}

	public static void main(String[] args) throws Exception {
		TTTask oo = new TTTask();
		oo.setResultDesc("ravi.huang");
		oo.setSrc_ip("192.168.1.102");
		oo.setType("test");
		oo.setLocal(true);
		oo.setParas("start test");
		System.out.println("task:" + oo.toString());
		
		String out=TTTaskHelper.encode(oo);
		System.out.println("task:" + out);
		
		TTTask f2 = TTTaskHelper.decode(out);
		System.out.println("task:" + f2.toString());
		// the output : Foobar
	}

}
