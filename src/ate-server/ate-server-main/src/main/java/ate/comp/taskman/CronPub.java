package ate.comp.taskman;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Date;
import java.util.HashMap;

import org.quartz.CronTrigger;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CronPub{
	private static Logger log = LoggerFactory.getLogger(CronPub.class);
	private SchedulerFactory sf=new StdSchedulerFactory();
	private Scheduler sched;
	private HashMap<String,CronTaskDetail> jobs=new HashMap<String,CronTaskDetail>();
	private static CronPub instance;
	private TMProducer processor;
	private CronPub(TMProducer processor){
		this.processor=processor;
	}
	private CronPub(){}
	
	public void setProcessor(TMProducer processor){
		this.processor=processor;
	}
	public static CronPub createInstance(TMProducer processor){
		if(instance==null)
			instance=new CronPub(processor);
		return instance;
	}
	public static CronPub createInstance(){
		if(instance==null)
			instance=new CronPub();
		return instance;
	}

	
	public JobKey startCron(ICronRunner run) throws Exception{
		if(sched==null||sched.isShutdown()){
			sched=sf.getScheduler();
		}
		CronTrigger trigger=newTrigger()
	        .withIdentity("ClassTrigger", "ICronRunner")
	        .withSchedule(cronSchedule(run.cron()))
	        .build();
			
		CronRunnerTaskDetail job = new CronRunnerTaskDetail(run);
		Date ft=sched.scheduleJob(job,trigger);
		sched.start();
		log.debug("start task at:"+ft.toString());
		return job.getKey();
	}
	
	public void stopCron(JobKey key) throws Exception{
		sched.deleteJob(key);
	}
	
	public TTTask execute(TTTask task){		
		try {
			if(sched==null||sched.isShutdown()){
				sched=sf.getScheduler();
			}
			if(task.getSubCmd().startsWith("stop")){	
				CronTaskDetail job=jobs.remove(task.getCronTaskID());
				sched.deleteJob(job.getKey());
				task.setResultDesc("stop cron task:\n"+job.getTask());
				log.debug("stop cron task:"+job.getTask());
			}else if(task.getSubCmd().startsWith("clear")){					
				sched.clear();	
				log.debug("clear all cron task");
				task.setResultDesc("clear all cron task");
				return task;
			}else if(task.getSubCmd().startsWith("start")){					
				CronTrigger trigger=newTrigger()
		        .withIdentity("TTTaskTrigger", "CronTTTask")
		        .withSchedule(cronSchedule(task.getTime()))
		        .build();
				
				CronTaskDetail job = new CronTaskDetail(processor,task);
				Date ft=sched.scheduleJob(job,trigger);			
				jobs.put(task.getCronTaskID(), job);
				sched.start();
				log.debug("start task at:"+ft.toString());
				TTTask rtn=task.clone();
				
				rtn.setResultDesc("start cron task at "+ft.toString());
				task=rtn;
			}
			task.setExeResult(EXERESULT.SUCCESS);
			return task;
		} catch (Exception e) {
			e.printStackTrace();
			task.setExeResult(EXERESULT.FAIL);
			task.setResultDesc(e.getMessage());			
		}
		return task;
	}

}
