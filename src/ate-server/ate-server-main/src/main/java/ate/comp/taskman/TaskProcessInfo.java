package ate.comp.taskman;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskProcessInfo {
	private static Logger log = LoggerFactory.getLogger(TaskProcessInfo.class);
	TTTask task;
	String taskid;
	TASKCLASS tClass;
	HashMap loadClient=new HashMap();
	HashMap finishedClient=new HashMap();
	ArrayList<TTTask> clientRunResult=new ArrayList<TTTask>();
	
	public TaskProcessInfo(final TTTask task){
		this.taskid=task.getUniqueId();
		tClass=task.getTaskClass();
		this.task=task;
	}
	public boolean isRunOnce(){
		return tClass.equals(TASKCLASS.RUNONCE);			
	}
	public void addClientRunResult(TTTask task){
		clientRunResult.add(task);
	}
	public ArrayList<TTTask> getClientRunResult(){
		return this.clientRunResult;
	}
	public void addLoadClient(String client){	
		if(client!=null&&client.length()>0)
		loadClient.put(client, "");
		
	}
	public HashMap getLoadClients(){
		return this.loadClient;
	}
	public void addFinishedClient(String client){
		if(client!=null&&client.length()>0)
		finishedClient.put(client, "");		
	}
	public HashMap getFinishedClients(){
		return this.finishedClient;
	}

	public boolean isAllDone(){
		log.debug("finishedClient: {}  loadClient: {}",finishedClient.keySet().size(),
				loadClient.keySet().size());
		
		return finishedClient.keySet().size()>0&&
				finishedClient.keySet().containsAll(loadClient.keySet());
	}
}
