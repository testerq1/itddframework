package ate.comp.taskman;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class CronScriptJob implements Job{
	public CronScriptJob(){}
	public void execute(JobExecutionContext context)
    throws JobExecutionException {
		JobDetail job=context.getJobDetail();
		if(job instanceof CronTaskDetail){
			processTTTask((CronTaskDetail)job);
		}else if(job instanceof CronRunnerTaskDetail){
			processRunner((CronRunnerTaskDetail)job);
		}
	}
	private void processRunner(CronRunnerTaskDetail job){
		ICronRunner runner=job.getCronRunner();
		runner.run();
	}
		
	private void processTTTask(CronTaskDetail job){
		try {
			TTTask task=job.getTask();
			TTTask sub_task=task.clone();
			sub_task.setCronExp("");
			sub_task.setTaskClass(TASKCLASS.RUNALL);
			job.getRunner().processTask(sub_task);
		} catch (Exception e) {
			e.printStackTrace();
			//todo ravi.huang process cron task result
		}
	}
}