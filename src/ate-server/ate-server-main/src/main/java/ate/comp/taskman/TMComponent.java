package ate.comp.taskman;

import java.util.Map;

import org.apache.camel.Endpoint;
import org.apache.camel.impl.DefaultComponent;

public class TMComponent extends DefaultComponent {
	
	@Override
	protected Endpoint createEndpoint(String uri, String remaining,
			Map parameters) throws Exception {
		TMEndpoint endpoint;		
		endpoint = new TMEndpoint(uri, this);
		setProperties(endpoint, parameters);	
		return endpoint;
		 		
	}
}