package ate.comp.taskman;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskRegister {
	static Logger log=LoggerFactory.getLogger(TaskRegister.class);
	static TaskRegister register;
	HashMap<String,TaskProcessInfo> tasks=new HashMap<String,TaskProcessInfo>();
	private TaskRegister(){}
	
	public static TaskRegister createInstance(){
		if(register==null){
			register=new TaskRegister();
		}
		return register;
	}
	public void taskRunning(TTTask task){
		if(tasks.get(task.getUniqueId())==null){
			TaskProcessInfo info=new TaskProcessInfo(task);		
			log.debug("new task "+task.getUniqueId()+" running at:"+task.getWorkerClient());
			info.addLoadClient(task.getWorkerClient());
			tasks.put(task.getUniqueId(), info);
		}else{
			log.debug("task "+task.getUniqueId()+" already running at:"+task.getWorkerClient());
			TaskProcessInfo info=tasks.get(task.getUniqueId());
			info.addLoadClient(task.getWorkerClient());
		}
	}
	public TaskProcessInfo taskFinished(TTTask task)
	{
		TaskProcessInfo info=null;
		if(tasks.get(task.getUniqueId())==null){
			log.debug("task "+task.getUniqueId()+" has only one client and finished at:"+task.getWorkerClient());
			info=new TaskProcessInfo(task);
			info.addLoadClient(task.getWorkerClient());
			info.addFinishedClient(task.getWorkerClient());
			tasks.put(task.getUniqueId(), info);
		}else{
			log.debug("task "+task.getUniqueId()+" finished at:"+task.getWorkerClient());
			info=tasks.get(task.getUniqueId());
			info.addFinishedClient(task.getWorkerClient());
		}
		info.addClientRunResult(task);
		return info;
	}
	public TaskProcessInfo removeTask(TTTask task){
		return tasks.remove(task.getUniqueId());
	}
	public int size(){
		return tasks.size();
	}
}
