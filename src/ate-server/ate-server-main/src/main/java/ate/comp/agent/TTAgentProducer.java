package ate.comp.agent;

import java.util.HashMap;

import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.command.ActiveMQMessage;
import org.apache.camel.AsyncCallback;
import org.apache.camel.Exchange;
import org.apache.camel.component.jms.JmsMessage;
import org.apache.camel.impl.DefaultAsyncProducer;

import ate.rm.jms.IClientOper;

public class TTAgentProducer extends DefaultAsyncProducer {
	IClientOper co;
	private HashMap<String, String> client = new HashMap<String, String>();

	public TTAgentProducer(TTAgentEndpoint endpoint) {
		super(endpoint);

	}

	public boolean process(Exchange exchange, AsyncCallback callback) {
		Object obj = exchange.getIn();
		if (!(obj instanceof JmsMessage)) {
			return false;
		}
		if (co == null) {
			co = ((TTAgentEndpoint) this.getEndpoint()).getConsumer().getClientHandler();
		}
		JmsMessage tt = (JmsMessage) obj;
		ActiveMQMessage msg = (ActiveMQMessage) tt.getJmsMessage();
		String dest = msg.getDestination().getPhysicalName();
		if (dest.toLowerCase().contains("activemq.advisory.connection"))
			return processConnectionAdvisory(msg);
		callback.done(true);
		return true;
		// if(obj instanceof TTTask){
		// TTTask task = (TTTask)obj;
		// if(task.getExeResult()==EXERESULT.SUCCESS){
		// log.debug(task.getType()+" process result: "+"FINISHED");
		// }else if(task.getExeResult()==EXERESULT.FAIL){
		// log.debug(task.getType()+" process result: "+"FAILURE");
		// }else if(task.getExeResult()==EXERESULT.UNAVAILABLE){
		// log.debug(task.getType()+" process result: "+"UNAVAILABLE");
		// } else {
		// log.debug(task.getType()+" process result: "+task.getExeResult());
		// }
		// callback.done(true);
		// return true;
		// }
	}

	private boolean processConsumerTopicAdvisory(ActiveMQMessage msg) {

		return false;
	}

	private boolean processConsumerQueueAdvisory(ActiveMQMessage msg) {

		return false;

	}

	private boolean processConnectionAdvisory(ActiveMQMessage msg) {
		ActiveMQConnection conn = msg.getConnection();
		String cid = "";
		try {
			cid = conn.getClientID();
		} catch (JMSException e) {
			e.printStackTrace();
			return false;
		}
		String name = cid.substring(0, cid.indexOf("^"));
		// conn.getst
		if (conn.isStarted()) {
			if (client.get(name) != null) {
				return true;
			}
			co.addClient(name);
			client.put(name, "");
		} else if (conn.isClosed()) {
			if (client.get(name) != null) {
				co.removeClient(name);
				client.remove(name);
			} else {
				log.warn("client not added or remove already:" + name);
			}
		} else {
			log.debug("client status is {}", msg.getConnection().getStats().getDescription());
		}
		return true;
	}

}
