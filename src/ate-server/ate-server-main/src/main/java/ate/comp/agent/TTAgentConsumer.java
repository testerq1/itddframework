package ate.comp.agent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.management.ManagementFactory;

import javax.management.ObjectName;

import mx4j.tools.adaptor.http.HttpAdaptor;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Processor;
import org.apache.camel.impl.DefaultConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.PDU;
import org.snmp4j.Session;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.agent.CommandProcessor;
import org.snmp4j.agent.DefaultMOServer;
import org.snmp4j.agent.DuplicateRegistrationException;
import org.snmp4j.agent.MOGroup;
import org.snmp4j.agent.MOServer;
import org.snmp4j.agent.ManagedObject;
import org.snmp4j.agent.NotificationOriginator;
import org.snmp4j.agent.ProxyForwarder;
import org.snmp4j.agent.io.DefaultMOPersistenceProvider;
import org.snmp4j.agent.io.ImportModes;
import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.agent.mo.jmx.mibs.JvmManagementMib;
import org.snmp4j.agent.mo.jmx.mibs.JvmManagementMibInst;
import org.snmp4j.agent.mo.snmp.NotificationOriginatorImpl;
import org.snmp4j.agent.mo.snmp.ProxyForwarderImpl;
import org.snmp4j.agent.mo.snmp.RowStatus;
import org.snmp4j.agent.mo.snmp.SNMPv2MIB;
import org.snmp4j.agent.mo.snmp.SnmpCommunityMIB;
import org.snmp4j.agent.mo.snmp.SnmpFrameworkMIB;
import org.snmp4j.agent.mo.snmp.SnmpNotificationMIB;
import org.snmp4j.agent.mo.snmp.SnmpProxyMIB;
import org.snmp4j.agent.mo.snmp.SnmpTargetMIB;
import org.snmp4j.agent.mo.snmp.StorageType;
import org.snmp4j.agent.mo.snmp.TransportDomains;
import org.snmp4j.agent.mo.snmp.UsmMIB;
import org.snmp4j.agent.mo.snmp.VacmMIB;
import org.snmp4j.agent.mo.snmp4j.Snmp4jConfigMib;
import org.snmp4j.agent.mo.snmp4j.Snmp4jLogMib;
import org.snmp4j.agent.security.MutableVACM;
import org.snmp4j.mp.MPv1;
import org.snmp4j.mp.MPv2c;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.MessageProcessingModel;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.AuthMD5;
import org.snmp4j.security.AuthSHA;
import org.snmp4j.security.PrivDES;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModel;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.ThreadPool;

import ate.actions.SaveConfigCommand;
import ate.comp.taskman.TTTask;
import ate.rm.jms.IClientOper;

public class TTAgentConsumer extends DefaultConsumer {
	private static final Logger logger = LoggerFactory
			.getLogger(TTAgentConsumer.class);

	public static final int STATE_STOPPED = 30;

	protected CommandProcessor agent;

	protected String configFileURI;
	protected OctetString defaultContext;
	protected ProxyForwarder defaultProxyForwarder;

	protected MessageDispatcherImpl dispatcher;
	private JvmManagementMib jvmManagementMIB;

	protected MPv3 mpv3;
	protected NotificationOriginator notificationOriginator;
	private SaveConfigCommand saver;
	protected DefaultMOServer server;
	protected Snmp session;
	protected Snmp4jConfigMib snmp4jConfigMIB;
	protected Snmp4jLogMib snmp4jLogMIB;
	protected SnmpCommunityMIB snmpCommunityMIB;
	protected SnmpFrameworkMIB snmpFrameworkMIB;
	protected SnmpNotificationMIB snmpNotificationMIB;
	protected SnmpProxyMIB snmpProxyMIB;

	protected SnmpTargetMIB snmpTargetMIB;
	protected SNMPv2MIB snmpv2MIB;
	protected OctetString sysDescr = new OctetString("TTool-Agent - "
			+ System.getProperty("os.name", "") + " - "
			+ System.getProperty("os.arch") + " - "
			+ System.getProperty("os.version"));
	protected OID sysOID = new OID("1.3.6.1.4.1.9999");
	protected Integer32 sysServices = new Integer32(10);

	protected TransportMapping[] transportMappings;
	protected TtMgtMib ttMgtMIB;
	
	public IClientOper getClientHandler() {
		return ttMgtMIB;
	}

	protected USM usm;

	protected UsmMIB usmMIB;

	protected VacmMIB vacmMIB;
	private TTAgentEndpoint endpoint;
	private DefaultMOPersistenceProvider defaultPersistenceProvider;
	private static TTAgentConsumer instance;
	private TTAgentConsumer(TTAgentEndpoint endpoint, Processor processor) {
		super(endpoint, processor);
	
		this.agent = new CommandProcessor(new OctetString(
				MPv3.createLocalEngineID()));
//		{
//			public void processPdu(CommandResponderEvent event) {
//				super.processPdu(event);			
//
//				PDU pdu = event.getPDU();
//				// check PDU not null
//				if (pdu != null) {
//					_processPDU(pdu);
//				} else {
//					logger.debug("Received invalid PDU: " + pdu);
//				}
//			}
//		};
		
		this.endpoint = endpoint;
		this.server = new DefaultMOServer();
		
		this.defaultPersistenceProvider =
	        new DefaultMOPersistenceProvider(new MOServer[] { this.server },
	        		endpoint.getConfigURI());
	}
	public static TTAgentConsumer getInstance(TTAgentEndpoint endpoint, Processor processor){
		if(instance==null){
			instance = new TTAgentConsumer(endpoint,processor);
		}
		return instance;
	}
	public int process(TTTask task){
		log.debug(task.toSummary());
		try {			
			Exchange ex=this.endpoint.createExchange(ExchangePattern.InOut,task);
			Object o=null;
//			if(task.isLocal()){
//				this.endpoint.getCamelContext().createProducerTemplate().asyncCallback(
//						"tttask:run",ex,null);
//				return SnmpConstants.SNMP_ERROR_SUCCESS;
//			}else{
				getProcessor().process(ex);
				
			//}
			o = ex.getOut().getBody();
			if(o!=null){
				if(o.equals("ok"))
					return SnmpConstants.SNMP_ERROR_SUCCESS;
				if(o.equals("busy"))
					return SnmpConstants.SNMP_MP_NOT_IN_TIME_WINDOW;
			}
		} catch (Exception e) {
			getExceptionHandler().handleException(e);
		}
		return SnmpConstants.SNMP_ERROR_RESOURCE_UNAVAILABLE;
	}
	private void _processPDU(PDU pdu) {
		if (logger.isDebugEnabled()) {
			logger.debug("Received Message for {} : {}",
					this.endpoint.getAddress(), pdu);
		}
		try {
			getProcessor().process(this.endpoint.createExchange(pdu));
		} catch (Exception e) {
			
			getExceptionHandler().handleException(e);
		}
	}

	/**
	 * Adds community to security name mappings needed for SNMPv1 and SNMPv2c.
	 * 
	 * @param communityMIB
	 *            the SnmpCommunityMIB holding coexistence configuration for
	 *            community based security models.
	 */
	protected void addCommunities(SnmpCommunityMIB communityMIB) {
		Variable[] com2sec = new Variable[] { new OctetString("public"), // community
																			// name
				new OctetString("public"), // security name
				agent.getContextEngineID(), // local engine ID
				new OctetString(), // default context name
				new OctetString(), // transport tag
				new Integer32(StorageType.nonVolatile), // storage type
				new Integer32(RowStatus.active) // row status
		};
		MOTableRow row = communityMIB.getSnmpCommunityEntry().createRow(
				new OctetString("public").toSubIndex(true), com2sec);
		communityMIB.getSnmpCommunityEntry().addRow(row);
		// snmpCommunityMIB.setSourceAddressFiltering(true);
	}

	/**
	 * Adds initial notification targets and filters.
	 * 
	 * @param targetMIB
	 *            the SnmpTargetMIB holding the target configuration.
	 * @param notificationMIB
	 *            the SnmpNotificationMIB holding the notification (filter)
	 *            configuration.
	 */
	protected void addNotificationTargets(SnmpTargetMIB targetMIB,
			SnmpNotificationMIB notificationMIB) {
		targetMIB.addDefaultTDomains();

		targetMIB.addTargetAddress(new OctetString("notification"),
				TransportDomains.transportDomainUdpIpv4, new OctetString(
						new UdpAddress("127.0.0.1/162").getValue()), 200, 1,
				new OctetString("notify"), new OctetString("v2c"),
				StorageType.permanent);
		targetMIB.addTargetParams(new OctetString("v2c"),
				MessageProcessingModel.MPv2c,
				SecurityModel.SECURITY_MODEL_SNMPv2c,
				new OctetString("public"), SecurityLevel.NOAUTH_NOPRIV,
				StorageType.permanent);
		notificationMIB.addNotifyEntry(new OctetString("default"),
				new OctetString("notify"),
				SnmpNotificationMIB.SnmpNotifyTypeEnum.trap,
				StorageType.permanent);
	}

	/**
	 * Adds all the necessary initial users to the USM.
	 * 
	 * @param usm
	 *            the USM instance used by this agent.
	 */
	protected void addUsmUser(USM usm) {
		UsmUser user = new UsmUser(new OctetString("user4"), AuthMD5.ID,
				new OctetString("1234567890abcdef"), PrivDES.ID,
				new OctetString("1234567890abcdef"));
		usm.addUser(user.getSecurityName(), usm.getLocalEngineID(), user);
		user = new UsmUser(new OctetString("TEST"), AuthSHA.ID,
				new OctetString("maplesyrup"), PrivDES.ID, new OctetString(
						"maplesyrup"));
		usm.addUser(user.getSecurityName(), usm.getLocalEngineID(), user);
		user = new UsmUser(new OctetString("SHA"), AuthSHA.ID, new OctetString(
				"SHAAuthPassword"), null, null);
		usm.addUser(user.getSecurityName(), usm.getLocalEngineID(), user);
		user = new UsmUser(new OctetString("user1"), null,
				null, null, null);
		usm.addUser(user.getSecurityName(), usm.getLocalEngineID(), user);
	}

	/**
	 * Adds initial VACM configuration.
	 * 
	 * @param vacmMIB
	 *            the VacmMIB holding the agent's view configuration.
	 */
	protected void addViews(VacmMIB vacm) {
		vacm.addGroup(SecurityModel.SECURITY_MODEL_SNMPv1, new OctetString(
				"public"), new OctetString("v1v2group"),
				StorageType.nonVolatile);
		vacm.addGroup(SecurityModel.SECURITY_MODEL_SNMPv2c, new OctetString(
				"public"), new OctetString("v1v2group"),
				StorageType.nonVolatile);
		vacm.addGroup(SecurityModel.SECURITY_MODEL_USM, new OctetString(
				"user4"), new OctetString("v3group"), StorageType.nonVolatile);
		vacm.addGroup(SecurityModel.SECURITY_MODEL_USM,
				new OctetString("TEST"), new OctetString("v3test"),
				StorageType.nonVolatile);
		vacm.addGroup(SecurityModel.SECURITY_MODEL_USM, new OctetString("SHA"),
				new OctetString("v3restricted"), StorageType.nonVolatile);
		vacm.addGroup(SecurityModel.SECURITY_MODEL_USM, new OctetString("user1"),
				new OctetString("v3restricted"), StorageType.nonVolatile);
		
		vacm.addAccess(new OctetString("v1v2group"), new OctetString(),
				SecurityModel.SECURITY_MODEL_ANY, SecurityLevel.NOAUTH_NOPRIV,
				MutableVACM.VACM_MATCH_EXACT, new OctetString("fullReadView"),
				new OctetString("fullWriteView"), new OctetString(
						"fullNotifyView"), StorageType.nonVolatile);
		vacm.addAccess(new OctetString("v3group"), new OctetString("context4"),
				SecurityModel.SECURITY_MODEL_USM, SecurityLevel.AUTH_PRIV,
				MutableVACM.VACM_MATCH_EXACT, new OctetString("fullReadView"),
				new OctetString("fullWriteView"), new OctetString(
						"fullNotifyView"), StorageType.nonVolatile);
		vacm.addAccess(new OctetString("v3restricted"), new OctetString(""),
				SecurityModel.SECURITY_MODEL_USM, SecurityLevel.AUTH_NOPRIV,
				MutableVACM.VACM_MATCH_EXACT, new OctetString(
						"restrictedReadView"), new OctetString(
						"restrictedWriteView"), new OctetString(
						"restrictedNotifyView"), StorageType.nonVolatile);
		vacm.addAccess(new OctetString("v3test"), new OctetString(""),
				SecurityModel.SECURITY_MODEL_USM, SecurityLevel.AUTH_PRIV,
				MutableVACM.VACM_MATCH_EXACT, new OctetString("testReadView"),
				new OctetString("testWriteView"), new OctetString(
						"testNotifyView"), StorageType.nonVolatile);

		vacm.addViewTreeFamily(new OctetString("fullReadView"), new OID("1.3"),
				new OctetString(), VacmMIB.vacmViewIncluded,
				StorageType.nonVolatile);
		vacm.addViewTreeFamily(new OctetString("fullWriteView"),
				new OID("1.3"), new OctetString(), VacmMIB.vacmViewIncluded,
				StorageType.nonVolatile);
		vacm.addViewTreeFamily(new OctetString("fullNotifyView"),
				new OID("1.3"), new OctetString(), VacmMIB.vacmViewIncluded,
				StorageType.nonVolatile);

		vacm.addViewTreeFamily(new OctetString("restrictedReadView"), new OID(
				"1.3.6.1.2"), new OctetString(), VacmMIB.vacmViewIncluded,
				StorageType.nonVolatile);
		vacm.addViewTreeFamily(new OctetString("restrictedWriteView"), new OID(
				"1.3.6.1.2.1"), new OctetString(), VacmMIB.vacmViewIncluded,
				StorageType.nonVolatile);
		vacm.addViewTreeFamily(new OctetString("restrictedNotifyView"),
				new OID("1.3.6.1.2"), new OctetString(),
				VacmMIB.vacmViewIncluded, StorageType.nonVolatile);

		vacm.addViewTreeFamily(new OctetString("testReadView"), new OID(
				"1.3.6.1.2"), new OctetString(), VacmMIB.vacmViewIncluded,
				StorageType.nonVolatile);
		vacm.addViewTreeFamily(new OctetString("testReadView"), new OID(
				"1.3.6.1.2.1.1"), new OctetString(), VacmMIB.vacmViewExcluded,
				StorageType.nonVolatile);
		vacm.addViewTreeFamily(new OctetString("testWriteView"), new OID(
				"1.3.6.1.2.1"), new OctetString(), VacmMIB.vacmViewIncluded,
				StorageType.nonVolatile);
		vacm.addViewTreeFamily(new OctetString("testNotifyView"), new OID(
				"1.3.6.1.2"), new OctetString(), VacmMIB.vacmViewIncluded,
				StorageType.nonVolatile);
	}

	@Override
	protected void doStart() throws Exception {
		super.doStart();

		init();
		loadConfig(ImportModes.UPDATE_CREATE);
		finishInit();
		run();

		agent.setWorkerPool(ThreadPool.create("RequestPool", 4));
		// listen to the transport
		if (logger.isDebugEnabled()) {
			logger.debug("Starting message consumer on {} using {} protocol",
					endpoint.getAddress(), endpoint.getProtocol());
		}
		this.session.listen();
		if (logger.isInfoEnabled()) {
			logger.info("Started message consumer on {} using {} protocol",
					endpoint.getAddress(), endpoint.getProtocol());
		}
	}

	@Override
	protected void doStop() throws Exception {
		// stop listening to the transport
		if (this.session != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Stopping message consumer on {}",
						this.endpoint.getAddress());
			}
			this.session.close();
			logger.info("Stopped message consumer on "
					+ this.endpoint.getAddress());
		}

		super.doStop();
	}

	/**
	 * Finishes intialization of the agent by connecting server and command
	 * processor, setting up USM, VACM, notification targets, and finally
	 * sending a coldStart notification to configured targets.
	 */
	protected void finishInit() {		
		agent.setNotificationOriginator(notificationOriginator);
		agent.addMOServer(server);
		agent.setCoexistenceProvider(snmpCommunityMIB);
		agent.setVacm(vacmMIB);
		dispatcher.addCommandResponder(agent);
	}

	/**
	 * This method can be overwritten by a subagent to specify the contexts each
	 * MIB module (group) will be registered to.
	 * 
	 * @param mibGroup
	 *            a group of {@link ManagedObject}s (i.e., a MIB module).
	 * @return the context for which the module should be registered.
	 * @since 1.1
	 */
	protected OctetString getContext(MOGroup mibGroup) {
		return defaultContext;
	}

	/**
	 * Reads the engine boots counter from the corresponding input stream
	 * (file).
	 * 
	 * @return the boots counter value read or zero if it could not be read.
	 */
	protected int getEngineBoots() {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(new File(this.endpoint.getBootCounterURI()));
			ObjectInputStream ois = new ObjectInputStream(fis);
			int boots = ois.readInt();
			if (logger.isInfoEnabled()) {
				logger.info("Engine boots is: " + boots);
			}
			return boots;
		} catch (FileNotFoundException ex) {
			logger.warn("Could not find boot counter file: " + endpoint.getBootCounterURI());
		} catch (IOException iox) {
			if (logger.isDebugEnabled()) {
				iox.printStackTrace();
			}
			logger.error("Failed to read boot counter: " + iox.getMessage());
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException ex1) {
					logger.warn(ex1.getLocalizedMessage());
				}
			}
		}
		return 0;
	}

	public NotificationOriginator getNotificationOriginator() {
		return notificationOriginator;
	}
	/**
	 * Initialize transport mappings, message dispatcher, basic MIB modules,
	 * proxy forwarder, VACM and USM security, and custom MIB modules and
	 * objects provided by sub-classes.
	 * 
	 * @throws IOException
	 *             if initialization fails because transport initialization
	 *             fails.
	 */
	public void init() throws IOException {		
		initTransportMappings();
		initMessageDispatcher();
		server.addContext(new OctetString());
		snmpv2MIB = new SNMPv2MIB(sysDescr, sysOID, sysServices);
		ttMgtMIB = new TtMgtMib(this,this.defaultPersistenceProvider);
		// register Snmp counters for updates
		dispatcher.addCounterListener(snmpv2MIB);
		agent.addCounterListener(snmpv2MIB);
		snmpFrameworkMIB = new SnmpFrameworkMIB(
				(USM) mpv3.getSecurityModel(SecurityModel.SECURITY_MODEL_USM),
				dispatcher.getTransportMappings());
		usmMIB = new UsmMIB(usm, SecurityProtocols.getInstance());
		usm.addUsmUserListener(usmMIB);

		vacmMIB = new VacmMIB(new MOServer[] { server });
		snmpTargetMIB = new SnmpTargetMIB(dispatcher);
		snmpNotificationMIB = new SnmpNotificationMIB();
		snmpCommunityMIB = new SnmpCommunityMIB(snmpTargetMIB);
		initConfigMIB();
		snmpProxyMIB = new SnmpProxyMIB();
		notificationOriginator = new NotificationOriginatorImpl(session,
				vacmMIB, snmpv2MIB.getSysUpTime(), snmpTargetMIB,
				snmpNotificationMIB);
		snmpv2MIB.setNotificationOriginator(agent);

		setupDefaultProxyForwarder();
		// add USM users
		addUsmUser(usm);
		// add SNMPv1/v2c community to SNMPv3 security name mappings
		addCommunities(snmpCommunityMIB);
		addViews(vacmMIB);
		addNotificationTargets(snmpTargetMIB, snmpNotificationMIB);

		registerSnmpMIBs();
	}

	protected void initConfigMIB() {
		snmp4jLogMIB = new Snmp4jLogMib();
		if ((configFileURI != null)
				&& (saver.getDefaultPersistenceProvider() != null)) {
			snmp4jConfigMIB = new Snmp4jConfigMib(snmpv2MIB.getSysUpTime());
			snmp4jConfigMIB.setSnmpCommunityMIB(snmpCommunityMIB);
			snmp4jConfigMIB.setPrimaryProvider(saver
					.getDefaultPersistenceProvider());
		}
	}

	/**
	 * Initializes the message dispatcher ({@link MessageDispatcherImpl}) with
	 * the transport mappings.
	 */
	protected void initMessageDispatcher() {
		dispatcher = new MessageDispatcherImpl();
		mpv3 = new MPv3(agent.getContextEngineID().getValue());

		usm = new USM(SecurityProtocols.getInstance(),
				agent.getContextEngineID(), updateEngineBoots());
		SecurityModels.getInstance().addSecurityModel(usm);
		SecurityProtocols.getInstance().addDefaultProtocols();
		dispatcher.addMessageProcessingModel(new MPv1());
		dispatcher.addMessageProcessingModel(new MPv2c());
		dispatcher.addMessageProcessingModel(mpv3);

		initSnmpSession();
	}

	protected void initSnmpSession() {
		session = new Snmp(dispatcher);

		for (int i = 0; i < transportMappings.length; i++) {
			try {
				session.addTransportMapping(transportMappings[i]);
			} catch (Exception ex) {
				logger.warn("Failed to initialize transport mapping '"
						+ transportMappings[i] + "' with: " + ex.getMessage());
			}
		}
		updateSession(session);
	}

	/**
	 * Initializes the transport mappings (ports) to be used by the agent.
	 * 
	 * @throws IOException
	 */
	protected void initTransportMappings() throws IOException {
		Address listenGenericAddress = GenericAddress.parse(this.endpoint
				.getAddress());
		transportMappings = new TransportMapping[1];
		if ("tcp".equals(endpoint.getProtocol())) {
			transportMappings[0] = new DefaultTcpTransportMapping(
					(TcpAddress) listenGenericAddress);
		} else if ("udp".equals(endpoint.getProtocol())) {
			transportMappings[0] = new DefaultUdpTransportMapping(
					(UdpAddress) listenGenericAddress);
		} else {
			throw new IllegalArgumentException("Unknown protocol: "
					+ endpoint.getProtocol());
		}
	}

	/**
	 * Loads the configuration using the specified import mode from the set
	 * config file.
	 * 
	 * @param importMode
	 *            one of the import modes defined by {@link ImportModes}.
	 */
	public void loadConfig(int importMode) {
		try {
			if (saver != null)
				saver.getDefaultPersistenceProvider().restore(null, importMode);
		} catch (IOException ex) {
			logger.error(ex.getLocalizedMessage());
		}
	}

	/**
	 * Register additional managed objects at the agent's server.
	 */
	protected void registerManagedObjects() {
		HttpAdaptor adapter = new HttpAdaptor();
		ObjectName adapterName = null;
		ObjectName snmpv2MIBName = null;
		try {
			// snmpv2MIBName = new ObjectName("TtoolAgent:name=snmpv2MIB");
			// mbs.registerMBean(snmpv2MIB., snmpv2MIBName);

			// scalarSupport.addAll(new ObjectName(
			// "TtoolAgent:name=ClassLoading"),
			// mBeanScalarAttributeDescriptions)
			// The following code instruments a scalar with JMX:

			adapterName = new ObjectName(
					"TtoolAgent:name=htmladapter,port=9092");
			adapter.setPort(9092);
			ManagementFactory.getPlatformMBeanServer().registerMBean(adapter,
					adapterName);

			adapter.setProcessor(new mx4j.tools.adaptor.http.XSLTProcessor());
			adapter.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Register the basic MIB modules at the agent's <code>MOServer</code>.
	 */
	protected void registerSnmpMIBs() {
		try {
			snmpTargetMIB.registerMOs(server, getContext(snmpTargetMIB));
			ttMgtMIB.registerMOs(server, getContext(ttMgtMIB));
			snmpNotificationMIB.registerMOs(server,
					getContext(snmpNotificationMIB));
			vacmMIB.registerMOs(server, getContext(vacmMIB));
			usmMIB.registerMOs(server, getContext(usmMIB));
			snmpv2MIB.registerMOs(server, getContext(snmpv2MIB));
			snmpFrameworkMIB.registerMOs(server, getContext(snmpFrameworkMIB));
			snmpCommunityMIB.registerMOs(server, getContext(snmpCommunityMIB));
			snmp4jLogMIB.registerMOs(server, getContext(snmp4jLogMIB));
			if (snmp4jConfigMIB != null) {
				snmp4jConfigMIB
						.registerMOs(server, getContext(snmp4jConfigMIB));
			}
			snmpProxyMIB.registerMOs(server, getContext(snmpProxyMIB));
			registerManagedObjects();
			jvmManagementMIB = new JvmManagementMibInst(
					agent.getNotificationOriginator());
			jvmManagementMIB.registerMOs(server, null);
		} catch (DuplicateRegistrationException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Starts the agent by let it listen on the configured SNMP agent ports
	 * (transpot mappings).
	 */
	public void run() {		
		initSnmpSession();		
		try {
			session.listen();
		} catch (IOException iox) {
			logger.error(iox.getLocalizedMessage());
		}
	}

	protected void sendColdStartNotification() {
		notificationOriginator.notify(new OctetString(),
				SnmpConstants.coldStart, new VariableBinding[0]);
	}
	
	protected void setEngineBoots(int engineBoots) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(new File(this.endpoint.getBootCounterURI()));
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeInt(engineBoots);
			oos.close();
			if (logger.isInfoEnabled()) {
				logger.info("Wrote boot counter: " + engineBoots);
			}
		} catch (FileNotFoundException fnfex) {
			logger.error("Boot counter configuration file not found: "
					+ fnfex.getMessage());
		} catch (IOException iox) {
			logger.error("Failed to write boot counter: " + iox.getMessage());
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException ex1) {
					logger.warn(ex1.getLocalizedMessage());
				}
			}
		}
	}

	/**
	 * Creates and registers the default proxy forwarder application (
	 * {@link ProxyForwarderImpl}).
	 */
	protected void setupDefaultProxyForwarder() {
		defaultProxyForwarder = new ProxyForwarderImpl(session, snmpProxyMIB,
				snmpTargetMIB);
		agent.addProxyForwarder(defaultProxyForwarder, null,
				ProxyForwarder.PROXY_TYPE_ALL);
		((ProxyForwarderImpl) defaultProxyForwarder)
				.addCounterListener(snmpv2MIB);
	}

	/**
	 * Unregister additional managed objects from the agent's server.
	 */
	protected void unregisterManagedObjects() {

	}

	/**
	 * Unregister the basic MIB modules from the agent's <code>MOServer</code>.
	 */
	protected void unregisterSnmpMIBs() {
		snmpTargetMIB.unregisterMOs(server, getContext(snmpTargetMIB));
		snmpNotificationMIB.unregisterMOs(server,
				getContext(snmpNotificationMIB));
		vacmMIB.unregisterMOs(server, getContext(vacmMIB));
		usmMIB.unregisterMOs(server, getContext(usmMIB));
		ttMgtMIB.unregisterMOs(server, getContext(ttMgtMIB));
		snmpv2MIB.unregisterMOs(server, getContext(snmpv2MIB));
		snmpFrameworkMIB.unregisterMOs(server, getContext(snmpFrameworkMIB));
		snmpCommunityMIB.unregisterMOs(server, getContext(snmpCommunityMIB));
		snmp4jLogMIB.unregisterMOs(server, getContext(snmp4jLogMIB));
		if (snmp4jConfigMIB != null) {
			snmp4jConfigMIB.unregisterMOs(server, getContext(snmp4jConfigMIB));
		}
		snmpProxyMIB.unregisterMOs(server, getContext(snmpProxyMIB));
		unregisterManagedObjects();
	}

	/**
	 * Updates the engine boots counter and returns the actual value.
	 * 
	 * @return the actual boots counter value.
	 */
	protected int updateEngineBoots() {
		int boots = getEngineBoots();
		boots++;
		if (boots <= 0) {
			boots = 1;
		}
		setEngineBoots(boots);
		return boots;
	}

	/**
	 * Updates all objects with a new session instance. This method must be
	 * overwritten, if non-default SNMP MIB instances are created by a subclass.
	 * 
	 * @param session
	 *            a SNMP Session instance.
	 */
	protected void updateSession(Session session) {
		if (notificationOriginator instanceof NotificationOriginatorImpl) {
			((NotificationOriginatorImpl) notificationOriginator)
					.setSession(session);
		}
		if (defaultProxyForwarder instanceof ProxyForwarderImpl) {
			((ProxyForwarderImpl) defaultProxyForwarder).setSession(session);
		}
	}
}
