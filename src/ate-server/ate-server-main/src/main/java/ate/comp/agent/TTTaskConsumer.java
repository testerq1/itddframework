package ate.comp.agent;

import java.util.Random;

import org.apache.camel.ConsumerTemplate;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.ScheduledPollConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import ate.actions.TaskProcess;
import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;
import ate.rm.dev.Host;

/**
 * The TTTask consumer.
 */
public class TTTaskConsumer extends ScheduledPollConsumer {
	private Logger log = LoggerFactory.getLogger(TTTaskConsumer.class);
	private final TTTaskEndpoint endpoint;
	ProducerTemplate pt;
	ConsumerTemplate ct;
	Random random = new Random();
	int cnt = 0;
	TaskProcess process = TaskProcess.getInstance();

	public TTTaskConsumer(TTTaskEndpoint endpoint, Processor processor) {
		super(endpoint, processor);
		this.endpoint = endpoint;
		ct = this.getEndpoint().getCamelContext().createConsumerTemplate();
		pt = this.getEndpoint().getCamelContext().createProducerTemplate();
	}

	@Override
	protected int poll() throws Exception {
		JmsComponent oo = (JmsComponent) this.getEndpoint().getCamelContext()
				.getComponent("activemq");
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		PlatformTransactionManager transactionManager = oo.getConfiguration()
				.getTransactionManager();

		TransactionStatus status = transactionManager.getTransaction(def);

		Exchange exchange = ct.receive("activemq:queue:taskqueue");

		TTTask task = exchange.getIn().getBody(TTTask.class);

		log.debug("receive mag from taskqueue:" + exchange.getIn().getMessageId());
		log.debug("receive task from taskqueue:" + task);
		if (task == null) {
			return cnt;
		}
		task.setExeResult(EXERESULT.NEW);
		process.INFORM(task);

		if (process.isBusy()) {
			transactionManager.rollback(status);
			log.debug("processor busy rollback task {}", task);
			Thread.currentThread().sleep(3000 * random.nextInt(10));
			return cnt;
		}
		try {
			task.setExeResult(EXERESULT.RUNNING);
			task.setWorkerClient(Host.getSystemInfo("ip"));
			pt.sendBody("activemq:queue:taskstatus", task);
			// send message to next processor in the route
			process.processTask(task);

			// 这里没有想清楚，暂时都commit,将来可以按照结果处理事务
			transactionManager.commit(status);

			getProcessor().process(exchange);
		} catch (Exception e) {
			// 此处的异常
			e.printStackTrace();
		} finally {
			// log exception if an exception occurred and was not handleds
			if (exchange.getException() != null) {
				getExceptionHandler().handleException("Error processing exchange", exchange,
						exchange.getException());
			}
		}
		return cnt++;// number of messages polled
	}

}
