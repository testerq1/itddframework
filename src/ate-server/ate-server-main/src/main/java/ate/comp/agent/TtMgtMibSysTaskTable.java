package ate.comp.agent;

import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.agent.mo.DefaultMOMutableRow2PC;
import org.snmp4j.agent.mo.DefaultMOMutableRow2PCFactory;
import org.snmp4j.agent.mo.DefaultMOMutableTableModel;
import org.snmp4j.agent.mo.MOAccessImpl;
import org.snmp4j.agent.mo.MOColumn;
import org.snmp4j.agent.mo.MOFactory;
import org.snmp4j.agent.mo.MOMutableColumn;
import org.snmp4j.agent.mo.MOMutableRow2PC;
import org.snmp4j.agent.mo.MOMutableTableModel;
import org.snmp4j.agent.mo.MOTable;
import org.snmp4j.agent.mo.MOTableIndex;
import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.agent.mo.MOTableSubIndex;
import org.snmp4j.agent.mo.snmp.DisplayString;
import org.snmp4j.agent.request.SnmpSubRequest;
import org.snmp4j.agent.request.SubRequest;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.SMIConstants;
import org.snmp4j.smi.Variable;

import ate.ITaskRunner;
import ate.actions.TaskProcess;
import ate.comp.mail.MailConfiguration;
import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;
import ate.rm.RM;
import ate.util.GenData0;
public class TtMgtMibSysTaskTable {
	private static Logger log = LoggerFactory.getLogger(TtMgtMibSysTaskTable.class);
	private MOFactory moFactory;
	private static final int ttIndex1_base = 2;
	private static final OID oidTtSysTaskEntry = new OID(new int[] { 1, 3, 6,
			1, 4, 1, 65535, 1, 1, 2, 1 });

	// Column sub-identifer defintions for ttSysTaskEntry:
	private static final int colTtSysTaskType = 2;
	private static final int colTtSysTaskIsLocal = 3;
	private static final int colTtSysTaskDesc = 4;
	
	private static final int colTtSysTaskParas = 5;
	private static final int colTtSysTaskSendResultTo = 6;
	private static final int colTtSysTaskCommit = 7;
	private MOTableSubIndex[] ttSysTaskEntryIndexes;
	private MOTableIndex ttSysTaskEntryIndex;		
	private MOTable ttSysTaskEntry;
	private MOMutableTableModel ttSysTaskEntryModel;
	private DefaultMOMutableRow2PCFactory rowFactory;		
	private String mailTo="";
	private TTAgentConsumer processor;
	public TtMgtMibSysTaskTable(TTAgentConsumer processor, MOFactory moFactory) {
		this.moFactory = moFactory;
		this.processor=processor;
		ttSysTaskEntryIndexes = new MOTableSubIndex[] { moFactory
				.createSubIndex(null, SMIConstants.SYNTAX_INTEGER, 1, 1) };
		ttSysTaskEntryIndex = moFactory.createIndex(
				ttSysTaskEntryIndexes, false);	
		
		this.mailTo=((MailConfiguration)RM.getSpringContext().getBean("mailSenderConfig")).toMailTo();
		createSysTaskEntry();
		
		Lookup lkp = Lookup.getDefault();
		Object[] rst = lkp.lookupAll(ITaskRunner.class).toArray();
		for (Object tmp : rst) {
			ITaskRunner svc = (ITaskRunner) tmp;			
			this.addRow( svc.getType(), svc.getDesc());
			try {
				TaskProcess.getInstance().registerTask(svc);
			} catch (Exception e) {
				log.error(GenData0.getExceptionDesc(e));
				e.printStackTrace();
			}
			
		}
	}
	
	public MOTable getTtSysTaskEntry() {
		return ttSysTaskEntry;
	}

	public void addRow(String type, String desc) {
		Variable[] values = new Variable[] { new OctetString(type),new Integer32(0),
				new OctetString(desc), new OctetString(""),
				new OctetString(mailTo),new Integer32(0) };
		
		OID index=new OID(new int[] { ttSysTaskEntryModel.getRowCount() + 1 });
		ttSysTaskEntryModel.addRow(ttSysTaskEntryModel.createRow(index, values));
	}
	private void createSysTaskEntry() {
		MOColumn[] ttSysTaskEntryColumns = new MOColumn[6];

		ttSysTaskEntryColumns[colTtSysTaskType - ttIndex1_base] = new DisplayString(
				colTtSysTaskType, MOAccessImpl.ACCESS_READ_ONLY, null, true);
		ttSysTaskEntryColumns[colTtSysTaskDesc - ttIndex1_base] = new DisplayString(
				colTtSysTaskDesc, MOAccessImpl.ACCESS_READ_ONLY, null, true);
		ttSysTaskEntryColumns[colTtSysTaskIsLocal - ttIndex1_base] = new MOMutableColumn(
				colTtSysTaskIsLocal, SMIConstants.SYNTAX_INTEGER32,
				MOAccessImpl.ACCESS_READ_WRITE);
		
		ttSysTaskEntryColumns[colTtSysTaskParas - ttIndex1_base] = new DisplayString(
				colTtSysTaskParas, MOAccessImpl.ACCESS_READ_WRITE, null, true);
		ttSysTaskEntryColumns[colTtSysTaskSendResultTo - ttIndex1_base] = new DisplayString(
				colTtSysTaskSendResultTo, MOAccessImpl.ACCESS_READ_WRITE, null, true);
		ttSysTaskEntryColumns[colTtSysTaskCommit - ttIndex1_base] = new MOMutableColumn(
				colTtSysTaskCommit, SMIConstants.SYNTAX_INTEGER32,
				MOAccessImpl.ACCESS_READ_WRITE);
		

		ttSysTaskEntryModel = new DefaultMOMutableTableModel();
		rowFactory=new DefaultMOMutableRow2PCFactory() {
			public MOMutableRow2PC createRow(OID index, Variable[] values)
					throws UnsupportedOperationException {
				return new TtSysTaskEntryRow(index, values);
			}
		};
		ttSysTaskEntryModel.setRowFactory(rowFactory);
		ttSysTaskEntry = moFactory
				.createTable(oidTtSysTaskEntry, ttSysTaskEntryIndex,
						ttSysTaskEntryColumns, ttSysTaskEntryModel);
	}
	class TtSysTaskEntryRow extends DefaultMOMutableRow2PC {
		public TtSysTaskEntryRow(OID index, Variable[] values) {
			super(index, values);
		}
		@Override
		public void commit(SubRequest subRequest, MOTableRow changeSet,
				int column) {		
			super.commit(subRequest, changeSet, column);
			
			if(column==colTtSysTaskCommit - ttIndex1_base){	
				IpAddress adr = (IpAddress) ((SnmpSubRequest) subRequest)
								.getSnmpRequest().getInitiatingEvent().getPeerAddress();				
				TTTask task = new TTTask();	
				task.setLocal(getValue(colTtSysTaskIsLocal-ttIndex1_base).toInt()==1);
				task.setType(getValue(colTtSysTaskType-ttIndex1_base).toString());
				task.setParas(getValue(colTtSysTaskParas-ttIndex1_base).toString());
				task.setMailTo(getValue(colTtSysTaskSendResultTo-ttIndex1_base).toString().trim());
				task.setSrc_ip(adr.getInetAddress().getHostAddress());
				task.setExeResult(EXERESULT.NEW);
				int error_code=TaskProcess.getInstance().getRunnerTemplate(task.getType()).validTaskConfig(task);
				if(error_code==0){					
					error_code=processor.process(task);
					log.debug("Generate new Task "+(error_code==0?"successful":"failure")+":"+task);
					subRequest.setErrorStatus(error_code);				
				}else{
					subRequest.setErrorStatus(SnmpConstants.SNMP_ERROR_BAD_VALUE);		
				}
				setValue(colTtSysTaskCommit - ttIndex1_base,new Integer32(0));
			}
		}

//		public void commitRow(SubRequest subRequest, MOTableRow changeSet) {
//			super.commitRow(subRequest, changeSet);

//		}
	}
}
