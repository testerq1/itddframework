package ate.comp.agent;

import java.util.Map;

import org.apache.camel.Endpoint;
import org.apache.camel.impl.DefaultComponent;

public class TTAgentComponent extends DefaultComponent {
	
	@Override
	@SuppressWarnings("unchecked")
	protected Endpoint createEndpoint(String uri, String remaining,
			Map parameters) throws Exception {
		TTAgentEndpoint endpoint;		
		endpoint = TTAgentEndpoint.createEndpoint(uri, this);
		setProperties(endpoint, parameters);	
		return endpoint;
		 		
	}
}