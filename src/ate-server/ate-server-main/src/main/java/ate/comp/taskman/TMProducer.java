package ate.comp.taskman;

import java.util.Date;

import org.apache.camel.AsyncCallback;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultAsyncProducer;
import org.apache.camel.impl.DefaultMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TMProducer extends DefaultAsyncProducer {
	private static final Logger log = LoggerFactory
	.getLogger(TMProducer.class);
	
	private final CronPub cron= CronPub.createInstance(this);
	TaskRegister register=TaskRegister.createInstance();
	private final ProducerTemplate pt;
	public TMProducer(Endpoint endpoint) {
		super(endpoint);
		pt=endpoint.getCamelContext().createProducerTemplate();
	}

	@Override
	public boolean process(Exchange exchange, AsyncCallback callback) {			
		TTTask task=TTTaskHelper.decode(exchange.getIn().getBody(String.class));				
		DefaultMessage out=new DefaultMessage();
		out.setBody("ok");
		exchange.setOut(out);
		processTask(task);
		callback.done(true);
		return true;
	}
	
	public boolean processTask(TTTask task){		
		task.setUniqueId(""+new Date().getTime());
		log.debug("Receive task "+task.getUniqueId());
		switch (task.getTaskClass())
		{
			case RUNALL:
				log.debug("RunAll:"+task.getUniqueId());
				pt.sendBody("activemq:topic:allrun", ExchangePattern.InOnly, task);
				break;
			case RUNONCE:
				log.debug("RunOnce:"+task.getUniqueId());
				pt.sendBody("activemq:queue:taskqueue", ExchangePattern.InOnly, task);
				break;
			case RUNCRONALL:
				pt.sendBody("activemq:resultqueue", ExchangePattern.InOnly, cron.execute(task));				
				break;
			default:
				break;
		}
		return true;
	}
}
