package ate.comp.agent;

import java.util.Date;
import java.util.Random;

import org.apache.camel.AsyncCallback;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultAsyncProducer;
import org.apache.camel.impl.DefaultMessage;

import ate.actions.TaskProcess;
import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;
import ate.comp.taskman.TTTaskHelper;

public class TTTaskProducer extends DefaultAsyncProducer {
	ProducerTemplate pt;
	ConsumerTemplate ct;
	TaskProcess process = TaskProcess.getInstance();
	Random random = new Random();
	public TTTaskProducer(Endpoint endpoint) {
		super(endpoint);
		ct =getEndpoint().getCamelContext().createConsumerTemplate();
		pt=getEndpoint().getCamelContext().createProducerTemplate();
	}

	@Override
	public boolean process(Exchange exchange, AsyncCallback callback) {		
		TTTask task=TTTaskHelper.decode(exchange.getIn().getBody(String.class));
		task.setUniqueId(""+new Date().getTime());
		
		task.setExeResult(EXERESULT.NEW);
		process.INFORM(task);
		
		if(process.isBusy()){
			DefaultMessage out=new DefaultMessage();
			out.setBody("busy");
			exchange.setOut(out);
			callback.done(false);
			return false;
		}else
			exchange.getOut().setBody("ok");
		log.debug("task producer receive task:"+task.getUniqueId());
		//set result to not run so activemq.dlq queue can send this task back when expires 
		
		task.setResultDesc("can't run now!");
		
		process.processTask(task);
		return true;
	}
}
