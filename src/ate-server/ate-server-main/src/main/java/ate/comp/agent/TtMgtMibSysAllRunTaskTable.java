package ate.comp.agent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.agent.mo.DefaultMOMutableRow2PC;
import org.snmp4j.agent.mo.DefaultMOMutableRow2PCFactory;
import org.snmp4j.agent.mo.DefaultMOMutableTableModel;
import org.snmp4j.agent.mo.MOAccessImpl;
import org.snmp4j.agent.mo.MOColumn;
import org.snmp4j.agent.mo.MOFactory;
import org.snmp4j.agent.mo.MOMutableColumn;
import org.snmp4j.agent.mo.MOMutableRow2PC;
import org.snmp4j.agent.mo.MOMutableTableModel;
import org.snmp4j.agent.mo.MOTable;
import org.snmp4j.agent.mo.MOTableIndex;
import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.agent.mo.MOTableSubIndex;
import org.snmp4j.agent.mo.snmp.DisplayString;
import org.snmp4j.agent.request.SnmpSubRequest;
import org.snmp4j.agent.request.SubRequest;
import org.snmp4j.log.LogAdapter;
import org.snmp4j.log.LogFactory;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.SMIConstants;
import org.snmp4j.smi.Variable;

import ate.ITaskRunner;
import ate.actions.TaskProcess;
import ate.comp.mail.MailConfiguration;
import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TASKCLASS;
import ate.comp.taskman.TTTask;
import ate.rm.RM;

public class TtMgtMibSysAllRunTaskTable {
	private static Logger log = LoggerFactory.getLogger(TaskProcess.class);
	
	private MOFactory moFactory;
	private static final int ttIndex1_base = 2;
	private static final LogAdapter LOGGER = LogFactory
	.getLogger(TtMgtMibSysAllRunTaskTable.class);
	private TTAgentConsumer processor;
	private static final OID oidTtSysAllRunTaskEntry = new OID(new int[] { 1, 3, 6,
			1, 4, 1, 65535, 1, 1, 3, 1 });

	private static final int colTtSysAllRunTaskType = 2;
	private static final int colTtSysAllRunTaskParas = 3;
	private static final int colTtSysAllRunTaskCronExpression = 4;
	private static final int colTtSysAllRunTaskSendResultTo=5;
	private static final int colTtSysAllRunTaskCommit = 6;
	private MOTableSubIndex[] ttSysAllRunTaskEntryIndexes;
	private MOTableIndex ttSysAllRunTaskEntryIndex;		
	private MOTable ttSysAllRunTaskEntry;
	
	public MOTable getTtSysAllRunTaskEntry() {
		return ttSysAllRunTaskEntry;
	}
	private MOMutableTableModel ttSysAllRunTaskEntryModel;
	
	public TtMgtMibSysAllRunTaskTable(TTAgentConsumer processor, MOFactory moFactory) {		
		this.moFactory = moFactory;
		this.processor=processor;
		ttSysAllRunTaskEntryIndexes = new MOTableSubIndex[] { moFactory
				.createSubIndex(null, SMIConstants.SYNTAX_INTEGER, 1, 1) };
		ttSysAllRunTaskEntryIndex = moFactory.createIndex(
				ttSysAllRunTaskEntryIndexes, false);	
		createSysAllRunTaskEntry();
		this.mailTo=((MailConfiguration)RM.getSpringContext().getBean("mailSenderConfig")).toMailTo();
		for(ITaskRunner type:TaskProcess.getInstance().getTaskTypes()){
			if(!type.onlyCanRunOnce())
				this.addRow(type.getType());
		}
	}
	private void createSysAllRunTaskEntry() {
		MOColumn[] ttSysAllRunTaskEntryColumns = new MOColumn[5];
		
		ttSysAllRunTaskEntryColumns[colTtSysAllRunTaskType - ttIndex1_base] = new DisplayString(
				colTtSysAllRunTaskType, MOAccessImpl.ACCESS_READ_WRITE, null, true);
		ttSysAllRunTaskEntryColumns[colTtSysAllRunTaskParas - ttIndex1_base] = new DisplayString(
				colTtSysAllRunTaskParas, MOAccessImpl.ACCESS_READ_WRITE, null, true);
		ttSysAllRunTaskEntryColumns[colTtSysAllRunTaskCronExpression - ttIndex1_base] = new DisplayString(
				colTtSysAllRunTaskCronExpression, MOAccessImpl.ACCESS_READ_WRITE, null, true);
		ttSysAllRunTaskEntryColumns[colTtSysAllRunTaskSendResultTo - ttIndex1_base] = new DisplayString(
				colTtSysAllRunTaskSendResultTo, MOAccessImpl.ACCESS_READ_WRITE, null, true);		
		ttSysAllRunTaskEntryColumns[colTtSysAllRunTaskCommit - ttIndex1_base] = new MOMutableColumn(
				colTtSysAllRunTaskCommit, SMIConstants.SYNTAX_INTEGER32,
				MOAccessImpl.ACCESS_READ_WRITE);

		ttSysAllRunTaskEntryModel = new DefaultMOMutableTableModel();

		ttSysAllRunTaskEntryModel.setRowFactory(new DefaultMOMutableRow2PCFactory() {
			public MOMutableRow2PC createRow(OID index, Variable[] values)
					throws UnsupportedOperationException {
				return new TtSysAllRunTaskEntryRow(index, values);
			}
		});
		ttSysAllRunTaskEntry = moFactory
				.createTable(oidTtSysAllRunTaskEntry, ttSysAllRunTaskEntryIndex,
						ttSysAllRunTaskEntryColumns, ttSysAllRunTaskEntryModel);
	}
	private String mailTo="";
	public void addRow(String type){
		Variable[] values = new Variable[] { new OctetString(type),
				new OctetString(""), new OctetString(""),
				new OctetString(mailTo),new Integer32(0) };
		
		OID index=new OID(new int[] { ttSysAllRunTaskEntryModel.getRowCount() + 1 });
		ttSysAllRunTaskEntry.addRow(ttSysAllRunTaskEntryModel.createRow(index, values));
	}
	
	class TtSysAllRunTaskEntryRow extends DefaultMOMutableRow2PC {
		
		public TtSysAllRunTaskEntryRow(OID index, Variable[] values) {
			super(index, values);
		}
		@Override
		public void commit(SubRequest subRequest, MOTableRow changeSet,
				int column) {		
			super.commit(subRequest, changeSet, column);
			
			if(column==colTtSysAllRunTaskCommit - ttIndex1_base){
				TTTask task = new TTTask();	
				String cronExp=getValue(colTtSysAllRunTaskCronExpression-ttIndex1_base).toString();
				setValue(colTtSysAllRunTaskCommit - ttIndex1_base,new Integer32(0));				
				task.setType(getValue(colTtSysAllRunTaskType-ttIndex1_base).toString());
				task.setParas(getValue(colTtSysAllRunTaskParas-ttIndex1_base).toString());
				task.setMailTo(getValue(colTtSysAllRunTaskSendResultTo-ttIndex1_base).toString().trim());
				task.setCronExp(cronExp.trim());
				task.setExeResult(EXERESULT.NEW);
				IpAddress adr = (IpAddress) ((SnmpSubRequest) subRequest)
								.getSnmpRequest().getInitiatingEvent().getPeerAddress();
				task.setSrc_ip(adr.getInetAddress().getHostAddress());
				
				if(cronExp!=null&&cronExp.trim().length()>0){
					task.setTaskClass(TASKCLASS.RUNCRONALL);					
				}
				else 
					task.setTaskClass(TASKCLASS.RUNALL);
				
				int error_code=TaskProcess.getInstance().getRunnerTemplate(task.getType()).validTaskConfig(task);
				if(error_code==0){					
					error_code=processor.process(task);
					log.debug("Generate new Task "+(error_code==0?"successful":"failure")+":"+task);
					subRequest.setErrorStatus(error_code);				
				}else{
					subRequest.setErrorStatus(SnmpConstants.SNMP_ERROR_BAD_VALUE);		
				}
				
				setValue(colTtSysAllRunTaskCommit - ttIndex1_base,new Integer32(0));
			}
		}
//		public void commitRow(SubRequest subRequest, MOTableRow changeSet) {
//			super.commitRow(subRequest, changeSet);
//			
//		}
	}
}
