package ate.comp.agent;

import java.io.File;
import java.net.URI;

import org.apache.camel.Consumer;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.IsSingleton;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.Producer;
import org.apache.camel.component.snmp.SnmpMessage;
import org.apache.camel.impl.DefaultEndpoint;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.impl.DefaultMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.PDU;
import org.snmp4j.mp.SnmpConstants;

import ate.comp.taskman.TTTask;
import ate.rm.dev.Host;

public class TTAgentEndpoint extends DefaultEndpoint implements IsSingleton {
	private static final Logger LOG = LoggerFactory
			.getLogger(TTAgentEndpoint.class);
	public static final String DEFAULT_COMMUNITY = "public";
	public static final int DEFAULT_SNMP_VERSION = SnmpConstants.version1;
	public static final int DEFAULT_SNMP_RETRIES = 2;
	public static final int DEFAULT_SNMP_TIMEOUT = 1500;
	private String address;
	private String protocol = "udp";
	private int retries = DEFAULT_SNMP_RETRIES;
	private int timeout = DEFAULT_SNMP_TIMEOUT;
	private int snmpVersion = DEFAULT_SNMP_VERSION;
	private String snmpCommunity = DEFAULT_COMMUNITY;
	private int delay = 60;
	private String configURI="";
	private final String bootCntURI="";
	public String getConfigURI() {		
		if(configURI.length()==0)
			return Host.HOME+File.separator+"conf"+
			File.separator+"snmp_config.ini";
		return Host.HOME+File.separator+"conf"+
		File.separator+configURI;
	}
	public String getBootCounterURI() {		
		if(bootCntURI.length()==0)
			return Host.HOME+File.separator+"conf"+
			File.separator+"snmp_bootcounter.ini";
		return Host.HOME+File.separator+"conf"+
		File.separator+bootCntURI;
	}
	public void setConfigURI(String configURI) {
		this.configURI = configURI;
	}
	private static TTAgentEndpoint owner;
	private TTAgentEndpoint(String uristring, TTAgentComponent component) {
		super(uristring, component);
		URI uri = URI.create(getEndpointUri());
		String host = uri.getHost();
		
		int port = uri.getPort();
		if (host == null || host.trim().length() < 1)
			host = "127.0.0.1";
		if (port == -1)
			port = 161;
		
		if(!host.equalsIgnoreCase("admin")){
			String address = String.format("%s:%s/%d", getProtocol(), host, port);
			LOG.debug("Using snmp address {}", address);
			setAddress(address);
		}
	
	}
	public static TTAgentEndpoint createEndpoint(String uristring, TTAgentComponent component){
		if(owner==null)
			owner=new TTAgentEndpoint(uristring,component);
		return owner;
	}
	public TTAgentConsumer consumer;
	
	public TTAgentConsumer getConsumer() {
		return consumer;
	}
	
	@Override
	public Consumer createConsumer(Processor processor) throws Exception {	
		consumer=TTAgentConsumer.getInstance(this, processor);
		return consumer;
	}

	@Override
	public Producer createProducer() throws Exception {		
		return new TTAgentProducer(this);
	}

	/**
	 * creates an exchange for the given message
	 * 
	 * @param pdu
	 *            the pdu
	 * @return an exchange
	 */
	public Exchange createExchange(PDU pdu) {
		return createExchange(getExchangePattern(), pdu);
	}
	@Override
	public Exchange createExchange(){
		return new DefaultExchange(this, ExchangePattern.InOut);
	}
	/**
	 * creates an exchange for the given pattern and message
	 * 
	 * @param pattern
	 *            the message exchange pattern
	 * @param pdu
	 *            the pdu
	 * @return the exchange
	 */
	private Exchange createExchange(ExchangePattern pattern, PDU pdu) {
		Exchange exchange = new DefaultExchange(this, pattern);
		exchange.setIn(new SnmpMessage(pdu));
		return exchange;
	}
	public Exchange createExchange(ExchangePattern pattern,TTTask task) {
		Exchange exchange = new DefaultExchange(this, pattern);
		Message msg=new DefaultMessage();
		msg.setBody(task);
		exchange.setIn(msg);
		return exchange;
	}
	/**
	 * creates and configures the endpoint
	 * 
	 * @throws Exception
	 *             if unable to setup connection
	 */
	public void initiate() throws Exception {
		
	}

	public int getDelay() {
		return delay;
	}

	/**
	 * Sets update rate in seconds
	 * 
	 * @param updateEvery
	 *            the update rate in seconds
	 */
	public void setDelay(int updateEvery) {
		delay = updateEvery;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getRetries() {
		return retries;
	}

	public void setRetries(int retries) {
		this.retries = retries;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public int getSnmpVersion() {
		return snmpVersion;
	}

	public void setSnmpVersion(int snmpVersion) {
		this.snmpVersion = snmpVersion;
	}

	public String getSnmpCommunity() {
		return snmpCommunity;
	}

	public void setSnmpCommunity(String snmpCommunity) {
		this.snmpCommunity = snmpCommunity;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	@Override
	public String toString() {
		// only show address to avoid user and password details to be shown
		return "TTAgentEndpoint[" + address + "]";
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
}
