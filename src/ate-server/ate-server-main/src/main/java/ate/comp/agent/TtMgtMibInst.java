package ate.comp.agent;

import org.snmp4j.agent.DefaultMOServer;
import org.snmp4j.agent.DuplicateRegistrationException;
import org.snmp4j.agent.MOServer;
import org.snmp4j.agent.NotificationOriginator;
import org.snmp4j.agent.io.DefaultMOPersistenceProvider;
import org.snmp4j.agent.mo.MOScalar;
import org.snmp4j.agent.mo.jmx.MBeanNotificationObjectInfo;
import org.snmp4j.agent.mo.jmx.mibs.JvmManagementMib;
import org.snmp4j.agent.mo.jmx.types.InverseBooleanType;
import org.snmp4j.agent.mo.jmx.types.TypedAttribute;
import org.snmp4j.agent.mo.jmx.types.TypedCompositeDataAttribute;
import org.snmp4j.agent.mo.snmp.smi.EnumerationConstraint;
import org.snmp4j.agent.mo.snmp.smi.ValueConstraint;
import org.snmp4j.agent.mo.snmp.smi.ValueConstraintValidator;
import org.snmp4j.smi.Counter64;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;

public class TtMgtMibInst extends TtMgtMib {
	private static final Object[][] SCALAR_MBEANS_SYSTEM = {
			{ JvmManagementMib.oidJvmClassesLoadedCount, "LoadedClassCount",
					Integer.class },
			{ JvmManagementMib.oidJvmClassesTotalLoadedCount,
					"TotalLoadedClassCount", Long.class },
			{ JvmManagementMib.oidJvmClassesUnloadedCount,
					"UnloadedClassCount", Long.class },
			{ JvmManagementMib.oidJvmClassesVerboseLevel,
					new InverseBooleanType("Verbose") }, };
	private static final MBeanNotificationObjectInfo[] jvmLowMemoryPoolCollectNotif = new MBeanNotificationObjectInfo[] {
			new MBeanNotificationObjectInfo(new OID(
					JvmManagementMib.oidJvmMemPoolEntry.getValue(),
					new int[] { JvmManagementMib.colJvmMemPoolName }),
					new OctetString(), new TypedAttribute("poolName",
							String.class)),
			new MBeanNotificationObjectInfo(new OID(
					JvmManagementMib.oidJvmMemPoolEntry.getValue(),
					new int[] { JvmManagementMib.colJvmMemPoolCollectUsed }),
					new Counter64(), new TypedCompositeDataAttribute("usage",
							"used", Long.class)),
			new MBeanNotificationObjectInfo(
					new OID(
							JvmManagementMib.oidJvmMemPoolEntry.getValue(),
							new int[] { JvmManagementMib.colJvmMemPoolCollectThreshdCount }),
					new Counter64(), new TypedAttribute("count", Long.class)) };

	public TtMgtMibInst(TTAgentConsumer processor,DefaultMOPersistenceProvider defaultPersistenceProvider) {
		super(processor, defaultPersistenceProvider);
	}
	private NotificationOriginator notificationOriginator;
	public TtMgtMibInst(TTAgentConsumer processor,NotificationOriginator notificationOriginator,DefaultMOPersistenceProvider defaultPersistenceProvider) {
	    super(processor,defaultPersistenceProvider);
	    this.notificationOriginator = notificationOriginator;
	    addJvmManagementMibInstrumentaton();
	}

	public void registerMOs(MOServer server, OctetString context)
			throws DuplicateRegistrationException {
		super.registerMOs(server, context);
		if (server instanceof DefaultMOServer) {
			addJvmManagementMibConstraints((DefaultMOServer) server);
		}
	}

	private void addJvmManagementMibInstrumentaton() {
	}

	private void addJvmManagementMibConstraints(DefaultMOServer server) {
		MOScalar scalar = (MOScalar) server.getManagedObject(
				JvmManagementMib.oidJvmThreadContentionMonitoring, null);
		ValueConstraint jvmThreadContentionMonitoringVC = new EnumerationConstraint(
				new int[] {
						JvmManagementMib.JvmThreadContentionMonitoringEnum.enabled,
						JvmManagementMib.JvmThreadContentionMonitoringEnum.disabled });
		scalar.addMOValueValidationListener(new ValueConstraintValidator(
				jvmThreadContentionMonitoringVC));
		scalar = (MOScalar) server.getManagedObject(
				JvmManagementMib.oidJvmThreadCpuTimeMonitoring, null);
		ValueConstraint jvmThreadCpuTimeMonitoringVC = new EnumerationConstraint(
				new int[] {
						JvmManagementMib.JvmThreadContentionMonitoringEnum.enabled,
						JvmManagementMib.JvmThreadContentionMonitoringEnum.disabled });
		scalar.addMOValueValidationListener(new ValueConstraintValidator(
				jvmThreadCpuTimeMonitoringVC));
	}

}
