package ate.comp.agent;

import java.util.HashMap;

import org.openide.util.Lookup;
import org.snmp4j.agent.mo.DefaultMOMutableRow2PC;
import org.snmp4j.agent.mo.DefaultMOMutableRow2PCFactory;
import org.snmp4j.agent.mo.DefaultMOMutableTableModel;
import org.snmp4j.agent.mo.MOAccessImpl;
import org.snmp4j.agent.mo.MOColumn;
import org.snmp4j.agent.mo.MOFactory;
import org.snmp4j.agent.mo.MOMutableColumn;
import org.snmp4j.agent.mo.MOMutableRow2PC;
import org.snmp4j.agent.mo.MOMutableTableModel;
import org.snmp4j.agent.mo.MOTable;
import org.snmp4j.agent.mo.MOTableIndex;
import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.agent.mo.MOTableSubIndex;
import org.snmp4j.agent.mo.snmp.DisplayString;
import org.snmp4j.agent.request.SubRequest;
import org.snmp4j.log.LogAdapter;
import org.snmp4j.log.LogFactory;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.SMIConstants;
import org.snmp4j.smi.Variable;

import ate.IService;

public class TtMgtMibSysSvcTable {
	private static final int colTtSysSvcAction = 4;
	private static final int colTtSysSvcDesc = 3;
	private static final int colTtSysSvcName = 2;
	private static final LogAdapter LOGGER = LogFactory
			.getLogger(TtMgtMibSysSvcTable.class);

	private static final OID oidTtSysSvcEntry = new OID(new int[] { 1, 3, 6, 1,	4, 1, 65535, 1, 1, 4, 1 });

	private static final int ttIndex1_base = 2;
	private static MOTableIndex ttSysSvcEntryIndex;
	private static MOTableSubIndex[] ttSysSvcEntryIndexes;
	private MOFactory moFactory;
	private DefaultMOMutableRow2PCFactory rowFactory;
	private MOTable ttSysSvcEntry;

	private MOMutableTableModel ttSysSvcEntryModel;
	private static TtMgtMibSysSvcTable singletonInstance;
	private HashMap<String, IService> svcMap = new HashMap<String, IService>();

	private TtMgtMibSysSvcTable(MOFactory moFactory) {
		this.moFactory = moFactory;

		ttSysSvcEntryIndexes = new MOTableSubIndex[] { moFactory
				.createSubIndex(null, SMIConstants.SYNTAX_INTEGER, 1, 1) };
		ttSysSvcEntryIndex = moFactory.createIndex(ttSysSvcEntryIndexes, false);
		createSysSvcEntry();

		Lookup lkp = Lookup.getDefault();
		Object[] rst = lkp.lookupAll(IService.class).toArray();
		for (Object tmp : rst) {
			IService svc = (IService) tmp;
			try {
				this.addRow(svc, svc.getName(), svc.getDesc(), 0, -1);
			} catch (Exception e) {
				LOGGER.error(e.getLocalizedMessage());
				e.printStackTrace();
			}
		}

	}

	public static TtMgtMibSysSvcTable createInstance(MOFactory moFactory) {
		if (singletonInstance == null)
			singletonInstance = new TtMgtMibSysSvcTable(moFactory);
		return singletonInstance;
	}

	public void addRow(IService callback, String name, String desc, int type,
			int status) throws Exception {
		if (svcMap.get(name.toLowerCase()) != null) {
			throw new Exception("Add Duplicated Row!");
		}
		this.svcMap.put(name, callback);

		Variable[] values = new Variable[] { new OctetString(name),
				new OctetString(desc), new Integer32(type),
				new Integer32(status) };
		OID index=new OID(new int[] { ttSysSvcEntryModel.getRowCount() + 1 });
		ttSysSvcEntryModel.addRow(rowFactory.createRow(index, values));
	}

	private void createSysSvcEntry() {
		MOColumn[] ttSysSvcEntryColumns = new MOColumn[3];

		ttSysSvcEntryColumns[colTtSysSvcName - ttIndex1_base] = new DisplayString(
				colTtSysSvcName, MOAccessImpl.ACCESS_READ_ONLY, null, false);
		ttSysSvcEntryColumns[colTtSysSvcDesc - ttIndex1_base] = new DisplayString(
				colTtSysSvcDesc, MOAccessImpl.ACCESS_READ_ONLY, null, false);

		ttSysSvcEntryColumns[colTtSysSvcAction - ttIndex1_base] = new MOMutableColumn(
				colTtSysSvcAction, SMIConstants.SYNTAX_INTEGER32,
				MOAccessImpl.ACCESS_READ_WRITE);

		ttSysSvcEntryModel = new DefaultMOMutableTableModel();
		rowFactory=new DefaultMOMutableRow2PCFactory() {
			public MOMutableRow2PC createRow(OID index, Variable[] values)
					throws UnsupportedOperationException {
				return new TtSysSvcEntryRow(index, values);
			}
		};
		ttSysSvcEntryModel.setRowFactory(rowFactory);
		
		ttSysSvcEntry = moFactory.createTable(oidTtSysSvcEntry,
				ttSysSvcEntryIndex, ttSysSvcEntryColumns, ttSysSvcEntryModel);		
	}

	class TtSysSvcEntryRow extends DefaultMOMutableRow2PC {		
		@Override
		public void commit(SubRequest subRequest, MOTableRow changeSet,
				int column) {		
			super.commit(subRequest, changeSet, column);
			if(column==colTtSysSvcAction-ttIndex1_base){
				String key = getValue(colTtSysSvcName - ttIndex1_base).toString();
				int action = getValue(colTtSysSvcAction - ttIndex1_base).toInt();
				IService svc= svcMap.get(key);
				if(svc==null){
					return;
				}
				if (action == IService.SVC_STOP&&svc.isStarted()) {
					svc.stop();
				} else if(action == IService.SVC_START&&!svc.isStarted()){
					svc.start();
				}
			}
		}

		public TtSysSvcEntryRow(OID index, Variable[] values) {
			super(index, values);
		}		
	}

	public MOTable getTtSysSvcEntry() {
		return ttSysSvcEntry;
	}

	public void setSysSvcStatus(String name, int status) throws Exception {
		for (int i = 1; i <= ttSysSvcEntryModel.getRowCount(); i++) {
			OID index = oidTtSysSvcEntry.append(1);
			MOMutableRow2PC row = (MOMutableRow2PC) ttSysSvcEntryModel
					.getRow(index.append(i));
			if (row.getValue(1).toString().equalsIgnoreCase(name)) {
				row.setValue(3, new Integer32(status));
				return;
			}
		}
		throw new Exception("Not find row:" + name);
	}
	 
}
