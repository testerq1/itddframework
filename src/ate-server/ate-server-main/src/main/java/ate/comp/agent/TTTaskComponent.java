package ate.comp.agent;

import java.util.Map;

import org.apache.camel.Endpoint;
import org.apache.camel.impl.DefaultComponent;

public class TTTaskComponent extends DefaultComponent {
	
	@Override
	@SuppressWarnings("unchecked")
	protected Endpoint createEndpoint(String uri, String remaining,
			Map parameters) throws Exception {
		TTTaskEndpoint endpoint;		
		endpoint = new TTTaskEndpoint(uri, this);
		setProperties(endpoint, parameters);	
		return endpoint;
		 		
	}
}