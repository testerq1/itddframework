package ate.comp.taskman;

import org.quartz.JobKey;
import org.quartz.impl.JobDetailImpl;


public class CronTaskDetail extends JobDetailImpl {
	TMProducer runner;
	TTTask task;
	
	public CronTaskDetail(TMProducer runner,TTTask task){
		super();
		this.runner = runner;		
		this.task=task;
		setKey(new JobKey(task.getCronTaskID(), "CronTTTask"));
		setJobClass(CronScriptJob.class);
	}
	public TMProducer getRunner() {
		return runner;
	}

	public TTTask getTask() {
		return task;
	}
	
	@Override
	public String toString(){
		return super.toString()+"\n"+task.toString();
	}
}
