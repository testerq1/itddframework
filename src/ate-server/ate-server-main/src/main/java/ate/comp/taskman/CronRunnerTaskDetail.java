package ate.comp.taskman;

import org.quartz.JobKey;
import org.quartz.impl.JobDetailImpl;

public class CronRunnerTaskDetail extends JobDetailImpl {
	ICronRunner runner;
	public CronRunnerTaskDetail(ICronRunner runner){
		super();
		JobKey key = new JobKey(runner.toString(), "ICronRunner");		
		this.setKey(key);
		this.setJobClass(CronScriptJob.class);	
		this.runner=runner;
	}
	public ICronRunner getCronRunner(){
		return runner;
	}
}
