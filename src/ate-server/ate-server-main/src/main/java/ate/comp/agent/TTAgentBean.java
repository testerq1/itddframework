package ate.comp.agent;

public class TTAgentBean {
	@Override
	public String toString() {
		return ip+":"+port+"?"+"protocol="+protocol;
	}
	private String ip="0.0.0.0";
	private String port="161";
	private String protocol="udp";
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
}
