package ate.actions;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;

public class RunnerTest extends ATaskRunner {
	private static int cnt = 0;
	private int loop = 5;

	public int getLoop() {
		return loop;
	}

	public TTTask execute(TTTask task) {
		task.setResultDesc("run here " + "RunnerTest:" + cnt++);
		logger.debug("running...");
		task.setExeResult(EXERESULT.SUCCESS);
		try {
			while (loop-- > 0) {
				Thread.currentThread().sleep((int) Math.random() * 10000 + 5000);
				logger.debug(" {} in loop {}", task.getUniqueId(), loop);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return task;
	}

	public String getType() {
		return "test";
	}

	public String getDesc() {
		return "A Runner Test";
	}

	@Override
	public boolean onlyCanRunOnce() {
		return false;
	}

	public void run() {
		TTTask task = new TTTask();
		this.execute(task);
	}

	public static void main(String[] args) throws Exception {
		final RunnerTest runner = new RunnerTest();
		ExecutorService exec = Executors.newSingleThreadExecutor();
		Runnable run = new Runnable() {
			public void run() {
				runner.execute(null);
			}
		};
		Future ff = exec.submit(run);
		int loop = runner.getLoop();
		int cnt = 0;
		while (loop > 0) {
			cnt++;
			loop = runner.getLoop();
			System.out.println(cnt + ":" + runner.getLoop());
			Thread.currentThread().sleep(5000);
		}
		exec.shutdown();
	}
}
