package ate.actions;

import ate.comp.taskman.TTTask;

public class AntCommand extends ATaskRunner {

	public TTTask execute(TTTask task) {
		AntMainFacade ant = new AntMainFacade();
		ant.start(task.getParas().split(" "), null, null);
		return null;
	}

	public String getType() {		
		return "Ant";
	}

	public String getDesc() {
		return "Ant";
	}

}
