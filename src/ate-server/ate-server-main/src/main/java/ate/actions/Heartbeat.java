package ate.actions;

import javax.sip.message.Request;

import org.apache.camel.CamelContext;

import ate.comp.taskman.TTTask;
import ate.rm.RM;
import ate.rm.dev.Host;

public class Heartbeat extends ATaskRunner {

	public TTTask execute(TTTask task) {
		CamelContext ctx=(CamelContext)RM.getSpringContext().getBean("camel");
		String ip = Host.getSystemInfo("ip");
		ctx.createProducerTemplate().sendBodyAndHeader(  
			    "sip://ttoolheartbeat@localhost:5152?&eventHeaderName=reg&toHost=localhost",   
			    "EVENT_A",  
			    "REQUEST_METHOD",   
			    Request.REGISTER);
		return task;
	}
	@Override
	public boolean needReport(){
		return false;
	}
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}
	public String getDesc() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
