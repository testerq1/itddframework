package ate.actions;

import java.io.IOException;

import org.snmp4j.agent.MOServer;
import org.snmp4j.agent.io.DefaultMOPersistenceProvider;

import ate.ITaskRunner;
import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;

public class SaveConfigCommand extends ATaskRunner{
	protected DefaultMOPersistenceProvider defaultPersistenceProvider;
	public DefaultMOPersistenceProvider getDefaultPersistenceProvider() {
		return defaultPersistenceProvider;
	}
	public void setDefaultPersistenceProvider(
			DefaultMOPersistenceProvider defaultPersistenceProvider) {
		this.defaultPersistenceProvider = defaultPersistenceProvider;
	}
	private static SaveConfigCommand saver;
	private String configURI;
	
	private SaveConfigCommand (DefaultMOPersistenceProvider defaultPersistenceProvider){
		super();
		this.defaultPersistenceProvider=defaultPersistenceProvider;		
	}
	
	public static ITaskRunner createCommand(MOServer server, String configURI){
		if(saver==null) {
			saver=new SaveConfigCommand(new DefaultMOPersistenceProvider(
					new MOServer[] { server }, configURI));
		}
		return saver;
	}
	
	public TTTask execute(TTTask task) {	
		this.task=task;
		try {
			defaultPersistenceProvider.store(defaultPersistenceProvider.getDefaultURI());
			task.setExeResult(EXERESULT.SUCCESS);
		} catch (IOException e) {			
			e.printStackTrace();
			task.setExeResult(EXERESULT.FAIL);
			task.setResultDesc(e.getLocalizedMessage());
		}
		return task;
	}
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}
	public String getDesc() {
		// TODO Auto-generated method stub
		return null;
	}
}
