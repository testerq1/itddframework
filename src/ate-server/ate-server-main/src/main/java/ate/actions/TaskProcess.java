package ate.actions;

import java.util.Collection;
import java.util.HashMap;

import org.apache.camel.CamelContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.ITaskRunner;
import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;
import ate.rm.RM;
import ate.rm.dev.Host;
import ate.util.GenData0;

public class TaskProcess implements INotifier{
	public enum TASKPROCSTATUS{RUNNING,READY}
	private static Logger log = LoggerFactory.getLogger(TaskProcess.class);
	private static TaskProcess process;
	private HashMap<String,ITaskRunner> tasks=new HashMap<String,ITaskRunner>();
	private Thread worker;
	private CamelContext ccxt;
	private TaskProcess(){}
	
	public void registerTask(ITaskRunner task)throws Exception{
		if(tasks.get(task.getType())!=null){
			throw new Exception("name conflict: "+task.getClass()+" and "+tasks.get(task.getType()));
		}
		tasks.put(task.getType(), task);
	}
	public Collection<ITaskRunner> getTaskTypes(){	
		return tasks.values();
	}
	public ITaskRunner getRunnerTemplate(String type){
		return tasks.get(type);
	}
	public static TaskProcess getInstance(){
		if(process==null){
			process=new TaskProcess();
		}
		return process;
	}
	public EXERESULT processTask(TTTask task){
		log.debug("TTTask begin process...:"+task);	
		if(task==null){
			return EXERESULT.UNAVAILABLE;
		}		
		
		ITaskRunner cmd=null;
		CamelContext ctx=(CamelContext)RM.getSpringContext().getBean("camel");
		try {
			cmd = (ITaskRunner)tasks.get(task.getType()).getClass().newInstance();			
		} catch (Exception e) {
			task.setExeResult(EXERESULT.FAIL);
			task.setResultDesc(GenData0.getExceptionDesc(e));
			
			log.debug("TTTask process finished, send task info to taskstatus: "+task);
			this.INFORM(task);
			e.printStackTrace();
			return EXERESULT.UNAVAILABLE;
		}
		task.setExeDuration(System.currentTimeMillis());
		task.setWorkerClient(Host.getSystemInfo("ip"));		
		cmd.setTask(task);
		cmd.setNotifier(this);
		worker=new Thread(cmd);
		worker.start();		
		return EXERESULT.SUCCESS;		
	}
	public boolean isBusy(){
		return worker!=null&&worker.isAlive();
	}
	public void INFORM(TTTask task){
		ccxt.createProducerTemplate().sendBody("activemq:queue:taskstatus", task);
	}
	public void setContext(CamelContext ctx){
		this.ccxt=ctx;
	}
	
}
