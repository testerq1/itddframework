package ate;

import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.Route;
import org.apache.camel.ServiceStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import ate.rm.RM;
import ate.rm.dev.Host;

public class MainThread implements Runnable {
	public static boolean debug=false;	
	
	private static String HOME = Host.HOME;
	
	protected static Logger log=LoggerFactory.getLogger(MainThread.class);
	
	@Override
	public void run() {
		ApplicationContext spring = RM.getSpringContext();
		CamelContext context = (CamelContext) spring.getBean("camel");
		
		try {
//			RouteBuilder rbb=new RouteBuilder() {
//	            @Override
//	            public void configure() throws Exception {
//	                from("ttagent:0.0.0.0:6161?protocol=udp")
//	                    .choice()
//	                        .when(header("runat").contains("local"))
//	                        	.to("log:foo?level=INFO")
//	                        .otherwise()
//	                            .to("log:foo?level=ERROR");
//	            }
//	        };
	        
			context.disableJMX();
			context.start();
//			context.addRoutes(rbb);
			List<Route> o =context.getRoutes();
			for(Route rt:o)
				if(context.getRouteStatus(rt.getId())!=ServiceStatus.Started)
					context.startRoute(rt.getId());
		
			
//			ActiveMQStatistic s=ActiveMQStatistic.createInstance();
//			s.getTaskQueueSize("activemq:resultqueue");
			//ff.
			
			 //Session session = a.createSession(false , Session.AUTO_ACKNOWLEDGE);   

//			ff.setClientInternalExceptionListener(clientInternalExceptionListener);
//			ff.setExceptionListener(exceptionListener)
//			ff.set
			
//			System.out.println();
			//ActiveMQConnectionFactory df=((ActiveMQConnectionFactory)cnf.getConnectionFactory().createConnection());
			//TransportListener jj;
			
			//cnf.setSubscriptionDurable(subscriptionDurable)
//			 TTTask oo = new TTTask();
//			 oo.setName("ravi.huang");
//			 oo.setSrc_ip("192.168.1.102");
//			 oo.setType("test");
//			 oo.setParas("start test");
			 //oo.setCronExp("/5 * * ? * *");
//			 oo.setExeDuration(10000000l);
//			 oo.setExeResult(EXERESULT.SUCCESS);
//			 oo.setResultDesc("i love you!");
//			 context.createProducerTemplate().sendBody("activemq:topic:allrun",ExchangePattern.InOnly, oo);
//			 Thread.currentThread().sleep(50000);
//			 oo.setParas("stop test");
//			 context.createProducerTemplate().sendBody("activemq:topic:allrun",ExchangePattern.InOnly, oo);
			// context.createProducerTemplate().sendBody("tttask:poll",ExchangePattern.InOnly, o);	
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
