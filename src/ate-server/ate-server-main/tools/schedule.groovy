import java.util.Date;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;

import ate.*

def ttmain=new TTClassLoader();

def filename= args[args.length-1];

if(!filename.endsWith(".groovy")){
	println("请指定脚本文件...");
	return;
}
ttmain.loadGroovyScript(new File(filename));

def time="0 0 22 * * ?";
//for(int i=0;i<args.length-1;i++){
//	time+=args[i]+" ";
//}

def trigger=new CronTrigger("cron","shell",filename,"cronscript");
trigger.setCronExpression(time);
if(sched==null||sched.isShutdown()){
	sched=sf.getScheduler();
}
JobDetail job=new JobDetail(filename,"cronscript",CronScriptJob.class);

sched.addJob(job, true);

Date ft=sched.scheduleJob(trigger);
println(job.getFullName()+" should be run at "+ft+" repeated by "+trigger.getCronExpression());

sched.start();