package ate.actions;

import java.io.File;

import jfs.conf.JFSConfig;
import jfs.conf.JFSDirectoryPair;
import jfs.shell.JFSPrint;
import jfs.sync.JFSComparison;
import jfs.sync.JFSRootElement;
import jfs.sync.JFSSynchronization;
import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;
import ate.rm.RM;
import ate.rm.dev.Host;

public class JFSSyncCommand extends ATaskRunner {
	public String default_target=Host.HOME+File.separator+"scripts";
	@Override
	public TTTask execute(TTTask task) {
		String [] args=task.getParas().split(" ");
		if(args[0].endsWith("$"))
			args[0]=args[0].substring(0,args[0].length()-1)+File.separator+Host.getSystemInfo("hostname");
		
		JFSDirectoryPair pair=null;		
		if(args.length==1)
			pair=new JFSDirectoryPair(args[0],default_target);
		else
			pair=new JFSDirectoryPair(args[0],args[1]);
		//JFSConfig cfg=JFSConfig.getInstance();
//		JFSFileInfo info;
//		info = new JFSFileInfo(".", "");
//		JFSServerAccess access = JFSServerAccess.getInstance("192.168.1.100", 55201, ".");
//		info = access.getInfo(info);
//		info.print();
		JFSConfig cfg=(JFSConfig)RM.getSpringContext().getBean("jfsConfig");
		cfg.addDirectoryList(pair);
		JFSComparison comparison = JFSComparison.getInstance();
		JFSSynchronization sync=JFSSynchronization.getInstance();
		JFSRootElement root = new JFSRootElement(pair);		
		int wait=3;
		while(!root.isActive()&&wait-->0)
			root = new JFSRootElement(pair);
		if(root.isActive()){
			comparison.compare();		
			sync.computeSynchronizationLists();
			//JFSPrint.printComparisonTable();
			task.setResultDesc(JFSPrint.getCompareResult());
			System.out.println(task.getResultDesc());
			sync.synchronize();					
			task.setExeResult(EXERESULT.SUCCESS);
		}else{			
			task.setExeResult(EXERESULT.FAIL);
			task.setResultDesc("can't connect to server or file not exists!");
		}
		cfg.clean();
		return task;
	}

	@Override
	public String getType() {		
		return "JFS_Client";
	}

	@Override
	public String getDesc() {
		return "JFS Client";
	}
	public static void main(String[] args){
		JFSConfig cfg=(JFSConfig)RM.getSpringContext().getBean("jfsConfig");
		cfg.clean();
		cfg=(JFSConfig)RM.getSpringContext().getBean("jfsConfig");
		cfg=(JFSConfig)RM.getSpringContext().getBean("jfsConfig");
//		TTTask task=new TTTask();
//		task.setParas("ext://192.168.1.102/scripts ../tt");
//		new JFSSyncCommand().execute(task);
//		System.out.print("");
	}
	@Override
	public boolean onlyCanRunOnce(){
		return false;
	}
	@Override
	public int validTaskConfig(final TTTask task){
		if(task.getParas()==null||!task.getParas().toLowerCase().startsWith("ext"))
			return PARAS;
		return super.validTaskConfig(task);
	}
}
