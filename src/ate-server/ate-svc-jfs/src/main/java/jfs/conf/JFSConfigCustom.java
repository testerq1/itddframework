package jfs.conf;

import java.io.File;
import java.util.List;


public class JFSConfigCustom extends JFSConfigXML {
	public void setInclude(List<JFSFilter> includes) {
		for(JFSFilter include:includes){
			this.addInclude(include);
		}
		this.include = includes;
	}
	public List<JFSFilter> getExclude() {
		return exclude;
	}
	public void setExclude(List<JFSFilter> excludes) {
		for(JFSFilter exclude:excludes){
			this.addExclude(exclude);
		}
		this.exclude = excludes;
	}
	public List<JFSDirectoryPair> getDirPairs() {
		return dirPairs;
	}
	public void setDirPairs(List<JFSDirectoryPair> pairs) {
		for(JFSDirectoryPair pair:pairs){
			this.addDirectoryList(pair);
		}
		this.dirPairs = pairs;
	}

	public String getConfigFile() {
		return configFile;
	}
	public void setConfigFile(String configFile) {
		this.load(new File(configFile));
		this.configFile = configFile;
	}
	private List<JFSFilter> include;
	private List<JFSFilter> exclude;
	private List<JFSDirectoryPair> dirPairs;
	private String configFile;
	
}
