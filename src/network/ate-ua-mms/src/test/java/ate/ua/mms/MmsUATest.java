package ate.ua.mms;

import org.testng.annotations.Test;

import ate.testcase.CsUATestcase;

public class MmsUATest extends CsUATestcase{

    @Test
    public void basic(){
        MmsUA uas=this.create_server("mms://"+siip+"", new MmsUA());
        MmsUA uac=this.create_client("mms://"+sip+"", new MmsUA());
        uas.set_dump_file("mms.pcap");
        this.start_all_ua();
        
        assertTrue(uas.is_ready()&&uac.is_ready());
        
        uac.send_message(uac.create_pdu("InitiateRequest"));
        
        Object o=uas.recv_message();
        
        assertTrue(o!=null&&o instanceof MmsPacket);
        
    }
    public void AfterClass() {
        super.AfterClass();
        LHOST.rm("*.pcap");
    }
}
