package ate.ua.mms;

import java.io.IOException;
import java.io.InputStream;

import org.apache.mina.core.buffer.IoBuffer;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.BerIdentifier;
import org.openmuc.jasn1.ber.types.BerNull;
import org.openmuc.openiec61850.internal.mms.asn1.ConfirmedErrorPdu;
import org.openmuc.openiec61850.internal.mms.asn1.ConfirmedRequestPdu;
import org.openmuc.openiec61850.internal.mms.asn1.ConfirmedResponsePdu;
import org.openmuc.openiec61850.internal.mms.asn1.InitiateRequestPdu;
import org.openmuc.openiec61850.internal.mms.asn1.InitiateResponsePdu;
import org.openmuc.openiec61850.internal.mms.asn1.RejectPdu;
import org.openmuc.openiec61850.internal.mms.asn1.ServiceError;
import org.openmuc.openiec61850.internal.mms.asn1.UnconfirmedPdu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.ua.RunFailureException;

public class MmsPacket{
    protected static Logger log = LoggerFactory.getLogger(MmsPacket.class);
    Object pkt;    
    
    private final BerByteArrayOutputStream berOStream = new BerByteArrayOutputStream(500, true);

    private int codeLength=0;
    
    public MmsPacket(){}
    
    public MmsPacket(Object pkt){
        this.pkt=pkt;
    }
    
    public void decode(IoBuffer io){   
        InputStream iStream=io.asInputStream();
        
        try {
            BerIdentifier berIdentifier = new BerIdentifier();
            codeLength += berIdentifier.decode(iStream);        
            BerIdentifier passedIdentifier = berIdentifier;
            
            if (berIdentifier.equals(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 0)) {
                ConfirmedRequestPdu confirmedRequestPdu = new ConfirmedRequestPdu();
                codeLength += confirmedRequestPdu.decode(iStream, false);
                pkt= confirmedRequestPdu;
            }

            if (berIdentifier.equals(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 1)) {
                ConfirmedResponsePdu confirmedResponsePdu = new ConfirmedResponsePdu();
                codeLength += confirmedResponsePdu.decode(iStream, false);
                pkt= confirmedResponsePdu;
            }

            if (berIdentifier.equals(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 2)) {
                ConfirmedErrorPdu confirmedErrorPdu = new ConfirmedErrorPdu();
                codeLength += confirmedErrorPdu.decode(iStream, false);
                pkt= confirmedErrorPdu;
            }

            if (berIdentifier.equals(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 3)) {
                UnconfirmedPdu unconfirmedPdu = new UnconfirmedPdu();
                codeLength += unconfirmedPdu.decode(iStream, false);
                pkt= unconfirmedPdu;
            }

            if (berIdentifier.equals(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 4)) {
                RejectPdu rejectPdu = new RejectPdu();
                codeLength += rejectPdu.decode(iStream, false);
                pkt= rejectPdu;
            }

            if (berIdentifier.equals(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 8)) {
                InitiateRequestPdu initiateRequestPdu = new InitiateRequestPdu();
                codeLength += initiateRequestPdu.decode(iStream, false);
                pkt= initiateRequestPdu;
            }

            if (berIdentifier.equals(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 9)) {
                InitiateResponsePdu initiateResponsePdu = new InitiateResponsePdu();
                codeLength += initiateResponsePdu.decode(iStream, false);
                pkt= initiateResponsePdu;
            }

            if (berIdentifier.equals(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 10)) {
                ServiceError initiateErrorPdu = new ServiceError();
                codeLength += initiateErrorPdu.decode(iStream, false);
                pkt= initiateErrorPdu;
            }

            if (berIdentifier.equals(BerIdentifier.CONTEXT_CLASS, BerIdentifier.PRIMITIVE, 11)) {
                BerNull conclude_RequestPDU = new BerNull();
                codeLength += conclude_RequestPDU.decode(iStream, false);
                pkt= conclude_RequestPDU;
            }
        } catch (IOException e) {
            pkt=io.array();
            log.warn("wrong decoded");
        }        
    }
    
    
    public byte[] encode() {
        berOStream.reset();
        try {
            if (pkt instanceof byte[]) {
                byte[] code=(byte[])pkt;
                for (int i = code.length - 1; i >= 0; i--) {
                    berOStream.write(code[i]);
                }
                codeLength= code.length;
            }else if ( pkt instanceof BerNull) {
                codeLength += ((BerNull)pkt).encode(berOStream, false);
                codeLength += (new BerIdentifier(BerIdentifier.CONTEXT_CLASS, BerIdentifier.PRIMITIVE, 11))
                        .encode(berOStream);
            }else if (pkt instanceof ServiceError) {
                codeLength += ((ServiceError)pkt).encode(berOStream, false);
                codeLength += (new BerIdentifier(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 10))
                        .encode(berOStream);
            }else if (pkt instanceof InitiateResponsePdu) {
                codeLength += ((InitiateResponsePdu)pkt).encode(berOStream, false);
                codeLength += (new BerIdentifier(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 9))
                        .encode(berOStream);
            }else if(pkt instanceof InitiateRequestPdu) {
                codeLength += ((InitiateRequestPdu)pkt).encode(berOStream, false);
                codeLength += (new BerIdentifier(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 8))
                        .encode(berOStream);
            }else if (pkt instanceof RejectPdu) {
                codeLength += ((RejectPdu)pkt).encode(berOStream, false);
                codeLength += (new BerIdentifier(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 4))
                        .encode(berOStream);
            }else if (pkt instanceof UnconfirmedPdu) {
                codeLength += ((UnconfirmedPdu)pkt).encode(berOStream, false);
                codeLength += (new BerIdentifier(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 3))
                        .encode(berOStream);
            }else if (pkt instanceof ConfirmedErrorPdu) {
                codeLength += ((ConfirmedErrorPdu)pkt).encode(berOStream, false);
                codeLength += (new BerIdentifier(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 2))
                        .encode(berOStream);
            }else if (pkt instanceof ConfirmedResponsePdu) {
                codeLength += ((ConfirmedResponsePdu)pkt).encode(berOStream, false);
                codeLength += (new BerIdentifier(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 1))
                        .encode(berOStream);
            }else if (pkt instanceof ConfirmedRequestPdu) {
                codeLength += ((ConfirmedRequestPdu)pkt).encode(berOStream, false);
                codeLength += (new BerIdentifier(BerIdentifier.CONTEXT_CLASS, BerIdentifier.CONSTRUCTED, 0))
                        .encode(berOStream);
            }
            return berOStream.getArray();
        } catch (IOException e) {
            throw new RunFailureException(e);
        }        
    }
    
    public Object get_packet() {
        return pkt;
    }
    
    public void set_pkt(Object pkt) {
        this.pkt=pkt;
    }
}
