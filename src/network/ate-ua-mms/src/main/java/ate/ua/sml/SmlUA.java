package ate.ua.sml;

import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;

import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.AbstractCustomUA;

/**
 * SML是一种用于数据抄读和参数设置的通讯协议，它是由国际上知名的电力公司RWE、EON和EnBW，共同制定的一种用于德国智能电表的工业化标准规范
 * @author Administrator
 *
 */
public class SmlUA extends AbstractCustomUA<SmlPacket>{

    @Override
    protected void add_default_filter() {
        
    }

    @Override
    public String get_default_schema() {
        return "sml";
    }

    @Override
    public SmlPacket decodePacket(IoBuffer bs) {
        return null;
    }
}
