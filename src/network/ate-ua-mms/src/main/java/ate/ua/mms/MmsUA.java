package ate.ua.mms;

import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.openmuc.jasn1.ber.BerByteArrayOutputStream;
import org.openmuc.jasn1.ber.types.BerBitString;
import org.openmuc.jasn1.ber.types.BerInteger;
import org.openmuc.openiec61850.internal.mms.asn1.InitRequestDetail;
import org.openmuc.openiec61850.internal.mms.asn1.InitiateRequestPdu;

import ate.ua.RunFailureException;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;
import ate.util.X2X0;

/**
 * 工控Manufacturing Message Service (MMS)协议
 * http://www.openmuc.org/index.php?id=48
 * http://www.monfox.com/dosi/java-mms-sdk.html
 * 
 * @author ravi huang
 *
 */
public class MmsUA extends AMinaUAImp<MmsPacket>{
    static final int MINIMUM_MMS_PDU_SIZE = 64;
    private static final int MAXIMUM_MMS_PDU_SIZE = 65000;

    private static final byte[] DEFAULT_TSEL_LOCAL = new byte[] { 0, 0 };
    private static final byte[] DEFAULT_TSEL_REMOTE = new byte[] { 0, 1 };

    private int proposedMaxPduSize = 65000;
    private final int proposedMaxServOutstandingCalling = 5;
    private final int proposedMaxServOutstandingCalled = 5;
    private final int proposedDataStructureNestingLevel = 10;
    private byte[] servicesSupportedCalling = new byte[] { (byte) 0xee, 0x1c, 0, 0, 0x04, 0x08, 0, 0, 0x79,
            (byte) 0xef, 0x18 };

    private int messageFragmentTimeout = 10000;
    private int responseTimeout = 20000;
    private static final BerInteger version = new BerInteger(new byte[] { (byte) 0x01, (byte) 0x01 });
    private static final BerBitString proposedParameterCbbBitString = new BerBitString(new byte[] { 0x03, 0x05,
            (byte) 0xf1, 0x00 });
    
    class SocketDecoder extends CumulativeProtocolDecoder {

        @Override
        protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out)
                throws Exception {
            if (in.remaining() <= 0) 
                return false;            
            MmsPacket decodedPdu = new MmsPacket();
            decodedPdu.decode(in);
            out.write(decodedPdu);
            return true;
        }
    }
    
    class SocketEncoder extends ProtocolEncoderAdapter {
        private final BerByteArrayOutputStream berOStream = new BerByteArrayOutputStream(500, true);
        @Override
        public void encode(IoSession session, Object message, ProtocolEncoderOutput out)
                throws Exception {
            if (message instanceof MmsPacket){
                MmsPacket pkt=(MmsPacket)message;                
                out.write(IoBuffer.wrap(pkt.encode()));
            }else if (message instanceof String){
                out.write(IoBuffer.wrap(X2X0.hexs2b(message.toString())));
            }else{
                throw new RunFailureException("encode failure:"+message);
            }
        }
    }
    
    @Override
    public int get_port(){
        if(port<=0)
            port=102;
        return port;
    }

    @Override
    protected void add_default_filter() {
        add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(new SocketEncoder(),
                new SocketDecoder()));
        
    }

    @Override
    public String get_default_schema() {
        return "mms";
    }
    
    @Override
    public String get_transport(){
        if(this.transport==null){
            this.transport="tcp";
        }
        return this.transport;
    }
    
    public MmsPacket create_pdu(String type){
        if(type.equalsIgnoreCase("InitiateRequest")){
            InitRequestDetail initRequestDetail = new InitRequestDetail(version, proposedParameterCbbBitString,
                    new BerBitString(servicesSupportedCalling, 85));
    
            InitiateRequestPdu initiateRequestPdu = new InitiateRequestPdu(new BerInteger(proposedMaxPduSize),
                    new BerInteger(proposedMaxServOutstandingCalling), new BerInteger(proposedMaxServOutstandingCalled),
                    new BerInteger(proposedDataStructureNestingLevel), initRequestDetail);
    
            return new MmsPacket(initiateRequestPdu);
        }
        return null;
    }

    @Override
    public boolean message_matched(MmsPacket msg, HashMap<String, Object> hm) {
        return true;
    }
    
}
