package ate.ua.j8583;

import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import ate.ua.RunFailureException;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;

import com.solab.iso8583.*;

/**
 * 8583的一种实现
 * http://www.mirrorservice.org/sites/download.sourceforge.net/pub/sourceforge/j/project/j8/j8583/j8583/1.6.0/
 * @author Administrator
 *
 */
public class J8583UA extends AMinaUAImp<IsoMessage>{
    class SocketDecoder extends CumulativeProtocolDecoder {
        MessageFactory mf = new MessageFactory();
        @Override
        protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out)
                throws Exception {
            if (in.remaining() >= 0) {
                byte[] dst=new byte[in.remaining()];
                in.get(dst);
                out.write(mf.parseMessage(dst, 0));
                return true;
            }
            return false;
        }
    }

    class SocketEncoder extends ProtocolEncoderAdapter {
        @Override
        public void encode(IoSession session, Object message, ProtocolEncoderOutput out)
                throws Exception {
            IoBuffer buf=null;
            if (message instanceof String)
                buf=IoBuffer.wrap(message.toString().getBytes(charset));
            else if (message instanceof byte[])
                buf=IoBuffer.wrap((byte[])message);
            else if (message instanceof IsoMessage)
                buf=IoBuffer.wrap(((IsoMessage)message).writeData());            
            else if(message instanceof IoBuffer)                
                buf=(IoBuffer)buf;
            else
                throw new RunFailureException("wrong message type:"+message);            
            out.write(buf);
        }
    }

    @Override
    public boolean message_matched(IsoMessage msg, HashMap<String, Object> hm) {
        return true;
    }

    @Override
    protected void add_default_filter() {
        add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(new SocketEncoder(),
                new SocketDecoder()));
    }

    @Override
    public String get_default_schema() {
        return "8583";
    }
}
