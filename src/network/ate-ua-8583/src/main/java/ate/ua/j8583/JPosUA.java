package ate.ua.j8583;

import java.util.HashMap;

import ate.ua.mina.AMinaUAImp;

import com.solab.iso8583.IsoMessage;

/**
 * 8583的另一种实现
 * 
 * @author Administrator
 *
 */
public class JPosUA extends AMinaUAImp<Object> {

    @Override
    public boolean message_matched(Object msg, HashMap<String, Object> hm) {
        return true;
    }

    @Override
    protected void add_default_filter() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String get_default_schema() {
        return "jpos";
    }

}
