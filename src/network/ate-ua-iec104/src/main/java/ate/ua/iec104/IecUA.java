package ate.ua.iec104;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeoutException;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.openmuc.j60870.APdu;
import org.openmuc.j60870.ASdu;
import org.openmuc.j60870.ConnectionSettings;
import org.openmuc.j60870.APdu.APCI_TYPE;

import ate.ua.RunFailureException;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;


public class IecUA extends AMinaUAImp<APdu>{
    ConnectionSettings settings = new ConnectionSettings();
    PduFactory pfactory=new PduFactory(0);
    
    class SocketDecoder extends CumulativeProtocolDecoder {

        @Override
        protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out)
                throws Exception {
            if (in.remaining() <= 0) {              
                return false;
            }
            if(IecUA.this.is_fuzzy()){
                byte[] bs=new byte[in.remaining()];
                in.get(bs);
                out.write(bs);
            }else{
                if(0x68!=in.get(0))
                    throw new RunFailureException("Message does not start with 0x68");
                APdu aPdu = new APdu(new DataInputStream(in.asInputStream()), settings);
                out.write(aPdu);
            }
            return true;
        }
    }

    class SocketEncoder extends ProtocolEncoderAdapter {
        @Override
        public void encode(IoSession session, Object message, ProtocolEncoderOutput out)
                throws Exception {
            if (message instanceof APdu){
                IoBuffer buffer=IoBuffer.allocate(255);
                ((APdu)message).encode(buffer, settings);
                int len=buffer.position();
                buffer.flip();
                out.write(buffer);
            }else if (message instanceof IoBuffer)
                out.write(message);
            else if (message instanceof byte[])
                out.write(IoBuffer.wrap((byte[])message));
            else
                log.error("message type wrong: {}",message);
        }
    }
    
    public APdu create_i_pdu(ASdu requestASdu){
        return create_apdu(APCI_TYPE.I_FORMAT, requestASdu);
    }
    
    public APdu create_apdu(APCI_TYPE apciType){
        return create_apdu(apciType,null);
    }
    
    public APdu create_apdu(APCI_TYPE apciType, ASdu requestASdu) {
        return this.pfactory.create_apdu(apciType,requestASdu);
    }
    
    public PduFactory get_pdu_factory(){
        return this.pfactory;
    }
    
    public ConnectionSettings get_settings(){
        return this.settings;
    }
    
    @Override
    public String get_default_schema() {
        return "iec104";
    }   
    
    @Override
    public boolean message_matched(APdu msg, HashMap<String, Object> hm) {
        return true;
    }

    @Override
    protected void add_default_filter() {
        add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(new SocketEncoder(),
                new SocketDecoder()));
    }

}
