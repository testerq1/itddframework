package ate.ua.iec104;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.TimeoutException;

import org.openmuc.j60870.APdu;
import org.openmuc.j60870.ASdu;
import org.openmuc.j60870.CauseOfTransmission;
import org.openmuc.j60870.IeAckFileOrSectionQualifier;
import org.openmuc.j60870.IeBinaryStateInformation;
import org.openmuc.j60870.IeChecksum;
import org.openmuc.j60870.IeDoubleCommand;
import org.openmuc.j60870.IeFileReadyQualifier;
import org.openmuc.j60870.IeFileSegment;
import org.openmuc.j60870.IeFixedTestBitPattern;
import org.openmuc.j60870.IeLastSectionOrSegmentQualifier;
import org.openmuc.j60870.IeLengthOfFileOrSection;
import org.openmuc.j60870.IeNameOfFile;
import org.openmuc.j60870.IeNameOfSection;
import org.openmuc.j60870.IeNormalizedValue;
import org.openmuc.j60870.IeQualifierOfCounterInterrogation;
import org.openmuc.j60870.IeQualifierOfInterrogation;
import org.openmuc.j60870.IeQualifierOfParameterActivation;
import org.openmuc.j60870.IeQualifierOfParameterOfMeasuredValues;
import org.openmuc.j60870.IeQualifierOfResetProcessCommand;
import org.openmuc.j60870.IeQualifierOfSetPointCommand;
import org.openmuc.j60870.IeRegulatingStepCommand;
import org.openmuc.j60870.IeScaledValue;
import org.openmuc.j60870.IeSectionReadyQualifier;
import org.openmuc.j60870.IeSelectAndCallQualifier;
import org.openmuc.j60870.IeShortFloat;
import org.openmuc.j60870.IeSingleCommand;
import org.openmuc.j60870.IeTestSequenceCounter;
import org.openmuc.j60870.IeTime16;
import org.openmuc.j60870.IeTime56;
import org.openmuc.j60870.InformationElement;
import org.openmuc.j60870.InformationObject;
import org.openmuc.j60870.TypeId;
import org.openmuc.j60870.APdu.APCI_TYPE;

import ate.ua.AbstractCustomPacket;
import ate.ua.RunFailureException;

public class PduFactory {
    int originatorAddress = 0;

    public PduFactory(int originatorAddress) {
        this.originatorAddress = originatorAddress;
    }

    public APdu create_apdu(ASdu requestASdu) {
        return create_apdu(APCI_TYPE.I_FORMAT, requestASdu);
    }

    public APdu create_apdu(APCI_TYPE apciType) {
        return create_apdu(apciType, null);
    }

    public APdu create_apdu(APCI_TYPE apciType, ASdu requestASdu) {
        APdu requestAPdu = null;
        if (apciType == APCI_TYPE.STARTDT_ACT) {
            requestAPdu = new APdu(0, 0, APCI_TYPE.STARTDT_ACT, null);
        } else if (apciType == APCI_TYPE.STARTDT_CON) {
            requestAPdu = new APdu(0, 0, APCI_TYPE.STARTDT_CON, null);
        } else {
            requestAPdu = new APdu(0, 0, apciType, requestASdu);
        }
        return requestAPdu;
    }

    public ASdu create_confirmation(ASdu aSdu) throws IOException {
        CauseOfTransmission cot = aSdu.getCauseOfTransmission();
        if (cot == CauseOfTransmission.ACTIVATION) {
            cot = CauseOfTransmission.ACTIVATION_CON;
        } else if (cot == CauseOfTransmission.DEACTIVATION) {
            cot = CauseOfTransmission.DEACTIVATION_CON;
        }
        return new ASdu(aSdu.getTypeIdentification(),
                aSdu.isSequenceOfElements(), cot, aSdu.isTestFrame(),
                aSdu.isNegativeConfirm(), aSdu.getOriginatorAddress(),
                aSdu.getCommonAddress(), aSdu.getInformationObjects());
    }

    /**
     * Sends a single command (C_SC_NA_1, TI: 45) and blocks until a
     * confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param informationObjectAddress
     *            the information object address.
     * @param singleCommand
     *            the command to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_singleCommand(int commonAddress,
            int informationObjectAddress, IeSingleCommand singleCommand) {
        CauseOfTransmission cot;
        if (singleCommand.isCommandStateOn()) {
            cot = CauseOfTransmission.ACTIVATION;
        } else {
            cot = CauseOfTransmission.DEACTIVATION;
        }
        ASdu aSdu = new ASdu(TypeId.C_SC_NA_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { singleCommand } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a single command with time tag CP56Time2a (C_SC_TA_1, TI: 58) and
     * blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param informationObjectAddress
     *            the information object address.
     * @param singleCommand
     *            the command to be sent.
     * @param timeTag
     *            the time tag to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_singleCommandWithTimeTag(int commonAddress,
            int informationObjectAddress, IeSingleCommand singleCommand,
            IeTime56 timeTag) {
        CauseOfTransmission cot;
        if (singleCommand.isCommandStateOn()) {
            cot = CauseOfTransmission.ACTIVATION;
        } else {
            cot = CauseOfTransmission.DEACTIVATION;
        }
        ASdu aSdu = new ASdu(
                TypeId.C_SC_TA_1,
                false,
                cot,
                false,
                false,
                originatorAddress,
                commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { singleCommand, timeTag } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a double command (C_DC_NA_1, TI: 46) and blocks until a
     * confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param doubleCommand
     *            the command to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_doubleCommand(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeDoubleCommand doubleCommand) {

        ASdu aSdu = new ASdu(TypeId.C_DC_NA_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { doubleCommand } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a double command with time tag CP56Time2a (C_DC_TA_1, TI: 59) and
     * blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param doubleCommand
     *            the command to be sent.
     * @param timeTag
     *            the time tag to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_doubleCommandWithTimeTag(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeDoubleCommand doubleCommand, IeTime56 timeTag) {

        ASdu aSdu = new ASdu(
                TypeId.C_DC_TA_1,
                false,
                cot,
                false,
                false,
                originatorAddress,
                commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { doubleCommand, timeTag } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a regulating step command (C_RC_NA_1, TI: 47) and blocks until a
     * confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param regulatingStepCommand
     *            the command to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_regulatingStepCommand(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeRegulatingStepCommand regulatingStepCommand) {

        ASdu aSdu = new ASdu(
                TypeId.C_RC_NA_1,
                false,
                cot,
                false,
                false,
                originatorAddress,
                commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { regulatingStepCommand } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a regulating step command with time tag CP56Time2a (C_RC_TA_1, TI:
     * 60) and blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param regulatingStepCommand
     *            the command to be sent.
     * @param timeTag
     *            the time tag to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_regulatingStepCommandWithTimeTag(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeRegulatingStepCommand regulatingStepCommand, IeTime56 timeTag) {

        ASdu aSdu = new ASdu(TypeId.C_RC_TA_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { regulatingStepCommand,
                                timeTag } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a set-point command, normalized value (C_SE_NA_1, TI: 48) and
     * blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param normalizedValue
     *            the value to be sent.
     * @param qualifier
     *            the qualifier to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_setNormalizedValueCommand(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeNormalizedValue normalizedValue,
            IeQualifierOfSetPointCommand qualifier) throws IOException,
            TimeoutException {

        ASdu aSdu = new ASdu(TypeId.C_SE_NA_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { normalizedValue,
                                qualifier } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a set-point command with time tag CP56Time2a, normalized value
     * (C_SE_TA_1, TI: 61) and blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param normalizedValue
     *            the value to be sent.
     * @param qualifier
     *            the qualifier to be sent.
     * @param timeTag
     *            the time tag to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_setNormalizedValueCommandWithTimeTag(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeNormalizedValue normalizedValue,
            IeQualifierOfSetPointCommand qualifier, IeTime56 timeTag) {

        ASdu aSdu = new ASdu(TypeId.C_SE_TA_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { normalizedValue,
                                qualifier, timeTag } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a set-point command, scaled value (C_SE_NB_1, TI: 49) and blocks
     * until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param scaledValue
     *            the value to be sent.
     * @param qualifier
     *            the qualifier to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_setScaledValueCommand(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeScaledValue scaledValue, IeQualifierOfSetPointCommand qualifier) {

        ASdu aSdu = new ASdu(
                TypeId.C_SE_NB_1,
                false,
                cot,
                false,
                false,
                originatorAddress,
                commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { scaledValue, qualifier } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a set-point command with time tag CP56Time2a, scaled value
     * (C_SE_TB_1, TI: 62) and blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param scaledValue
     *            the value to be sent.
     * @param qualifier
     *            the qualifier to be sent.
     * @param timeTag
     *            the time tag to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_setScaledValueCommandWithTimeTag(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeScaledValue scaledValue, IeQualifierOfSetPointCommand qualifier,
            IeTime56 timeTag) {

        ASdu aSdu = new ASdu(TypeId.C_SE_TB_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { scaledValue, qualifier,
                                timeTag } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a set-point command, short floating point number (C_SE_NC_1, TI:
     * 50) and blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param shortFloat
     *            the value to be sent.
     * @param qualifier
     *            the qualifier to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_setShortFloatCommand(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeShortFloat shortFloat, IeQualifierOfSetPointCommand qualifier) {

        ASdu aSdu = new ASdu(
                TypeId.C_SE_NC_1,
                false,
                cot,
                false,
                false,
                originatorAddress,
                commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { shortFloat, qualifier } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a set-point command with time tag CP56Time2a, short floating point
     * number (C_SE_TC_1, TI: 63) and blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param shortFloat
     *            the value to be sent.
     * @param qualifier
     *            the qualifier to be sent.
     * @param timeTag
     *            the time tag to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_setShortFloatCommandWithTimeTag(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeShortFloat shortFloat, IeQualifierOfSetPointCommand qualifier,
            IeTime56 timeTag) {

        ASdu aSdu = new ASdu(TypeId.C_SE_TC_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { shortFloat, qualifier,
                                timeTag } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a bitstring of 32 bit (C_BO_NA_1, TI: 51) and blocks until a
     * confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param binaryStateInformation
     *            the value to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_bitStringCommand(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeBinaryStateInformation binaryStateInformation) {

        ASdu aSdu = new ASdu(
                TypeId.C_BO_NA_1,
                false,
                cot,
                false,
                false,
                originatorAddress,
                commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { binaryStateInformation } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a bitstring of 32 bit with time tag CP56Time2a (C_BO_TA_1, TI: 64)
     * and blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param binaryStateInformation
     *            the value to be sent.
     * @param timeTag
     *            the time tag to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_bitStringCommandWithTimeTag(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeBinaryStateInformation binaryStateInformation, IeTime56 timeTag) {

        ASdu aSdu = new ASdu(TypeId.C_BO_TA_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { binaryStateInformation,
                                timeTag } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends an interrogation command (C_IC_NA_1, TI: 100) and blocks until a
     * confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param qualifier
     *            the qualifier to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_interrogation(int commonAddress,
            CauseOfTransmission cot, IeQualifierOfInterrogation qualifier) {
        ASdu aSdu = new ASdu(TypeId.C_IC_NA_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(0,
                        new InformationElement[][] { { qualifier } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a counter interrogation command (C_CI_NA_1, TI: 101) and blocks
     * until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param qualifier
     *            the qualifier to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_counterInterrogation(int commonAddress,
            CauseOfTransmission cot, IeQualifierOfCounterInterrogation qualifier) {
        ASdu aSdu = new ASdu(TypeId.C_CI_NA_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(0,
                        new InformationElement[][] { { qualifier } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a read command (C_RD_NA_1, TI: 102) and blocks until a confirmation
     * is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param informationObjectAddress
     *            the information object address.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_readCommand(int commonAddress,
            int informationObjectAddress) {
        ASdu aSdu = new ASdu(
                TypeId.C_RD_NA_1,
                false,
                CauseOfTransmission.REQUEST,
                false,
                false,
                originatorAddress,
                commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress, new InformationElement[0][0]) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a clock synchronization command (C_CS_NA_1, TI: 103) and blocks
     * until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param time
     *            the time to be sent.
     * @return the time that was returned by the server in the confirmation
     *         message. Null if waiting for confirmation messages was disabled.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_synchronizeClocks(int commonAddress, IeTime56 time) {
        InformationObject io = new InformationObject(0,
                new InformationElement[][] { { time } });

        InformationObject[] ios = new InformationObject[] { io };

        ASdu aSdu = new ASdu(TypeId.C_CS_NA_1, false,
                CauseOfTransmission.ACTIVATION, false, false,
                originatorAddress, commonAddress, ios);

        return create_apdu(aSdu);

        // return (IeTime56)
        // responsePdu.getASdu().getInformationObjects()[0].getInformationElements()[0][0];
    }

    /**
     * Sends a test command (C_TS_NA_1, TI: 104) and blocks until a confirmation
     * is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_testCommand(int commonAddress) {
        ASdu aSdu = new ASdu(
                TypeId.C_TS_NA_1,
                false,
                CauseOfTransmission.ACTIVATION,
                false,
                false,
                originatorAddress,
                commonAddress,
                new InformationObject[] { new InformationObject(
                        0,
                        new InformationElement[][] { { new IeFixedTestBitPattern() } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a reset process command (C_RP_NA_1, TI: 105) and blocks until a
     * confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param qualifier
     *            the qualifier to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_resetProcessCommand(int commonAddress,
            IeQualifierOfResetProcessCommand qualifier) throws IOException,
            TimeoutException {
        ASdu aSdu = new ASdu(TypeId.C_RP_NA_1, false,
                CauseOfTransmission.ACTIVATION, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(0,
                        new InformationElement[][] { { qualifier } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a delay acquisition command (C_CD_NA_1, TI: 106) and blocks until a
     * confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            spontaneous.
     * @param time
     *            the time to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_delayAcquisitionCommand(int commonAddress,
            CauseOfTransmission cot, IeTime16 time) throws IOException,
            TimeoutException {
        ASdu aSdu = new ASdu(TypeId.C_CD_NA_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(0,
                        new InformationElement[][] { { time } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a test command with time tag CP56Time2a (C_TS_TA_1, TI: 107) and
     * blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param testSequenceCounter
     *            the value to be sent.
     * @param time
     *            the time to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_testCommandWithTimeTag(int commonAddress,
            IeTestSequenceCounter testSequenceCounter, IeTime56 time) {
        ASdu aSdu = new ASdu(TypeId.C_TS_TA_1, false,
                CauseOfTransmission.ACTIVATION, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(0,
                        new InformationElement[][] { { testSequenceCounter,
                                time } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a parameter of measured values, normalized value (P_ME_NA_1, TI:
     * 110) and blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param informationObjectAddress
     *            the information object address.
     * @param normalizedValue
     *            the value to be sent.
     * @param qualifier
     *            the qualifier to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_parameterNormalizedValueCommand(int commonAddress,
            int informationObjectAddress, IeNormalizedValue normalizedValue,
            IeQualifierOfParameterOfMeasuredValues qualifier)
            throws IOException, TimeoutException {
        ASdu aSdu = new ASdu(TypeId.P_ME_NA_1, false,
                CauseOfTransmission.ACTIVATION, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { normalizedValue,
                                qualifier } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a parameter of measured values, scaled value (P_ME_NB_1, TI: 111)
     * and blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param informationObjectAddress
     *            the information object address.
     * @param scaledValue
     *            the value to be sent.
     * @param qualifier
     *            the qualifier to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_parameterScaledValueCommand(int commonAddress,
            int informationObjectAddress, IeScaledValue scaledValue,
            IeQualifierOfParameterOfMeasuredValues qualifier) {
        ASdu aSdu = new ASdu(
                TypeId.P_ME_NB_1,
                false,
                CauseOfTransmission.ACTIVATION,
                false,
                false,
                originatorAddress,
                commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { scaledValue, qualifier } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a parameter of measured values, short floating point number
     * (P_ME_NC_1, TI: 112) and blocks until a confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param informationObjectAddress
     *            the information object address.
     * @param shortFloat
     *            the value to be sent.
     * @param qualifier
     *            the qualifier to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_parameterShortFloatCommand(int commonAddress,
            int informationObjectAddress, IeShortFloat shortFloat,
            IeQualifierOfParameterOfMeasuredValues qualifier) {
        ASdu aSdu = new ASdu(
                TypeId.P_ME_NC_1,
                false,
                CauseOfTransmission.ACTIVATION,
                false,
                false,
                originatorAddress,
                commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { shortFloat, qualifier } }) });
        return create_apdu(aSdu);
    }

    /**
     * Sends a parameter activation (P_AC_NA_1, TI: 113) and blocks until a
     * confirmation is received.
     *
     * @param commonAddress
     *            the Common ASDU Address. Valid value are 1...255 or 1...65535
     *            for field lengths 1 or 2 respectively.
     * @param cot
     *            the cause of transmission. Allowed are activation and
     *            deactivation.
     * @param informationObjectAddress
     *            the information object address.
     * @param qualifier
     *            the qualifier to be sent.
     * @throws IOException
     *             if a fatal communication error occurred.
     * @throws TimeoutException
     *             if the configured response timeout runs out before the
     *             confirmation message is received.
     */
    public APdu create_parameterActivation(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeQualifierOfParameterActivation qualifier) {
        ASdu aSdu = new ASdu(TypeId.P_AC_NA_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { qualifier } }) });
        return create_apdu(aSdu);
    }

    public APdu create_fileReady(int commonAddress,
            int informationObjectAddress, IeNameOfFile nameOfFile,
            IeLengthOfFileOrSection lengthOfFile, IeFileReadyQualifier qualifier) {
        ASdu aSdu = new ASdu(TypeId.F_FR_NA_1, false,
                CauseOfTransmission.FILE_TRANSFER, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { nameOfFile,
                                lengthOfFile, qualifier } }) });
        return create_apdu(aSdu);
    }

    public APdu create_sectionReady(int commonAddress,
            int informationObjectAddress, IeNameOfFile nameOfFile,
            IeNameOfSection nameOfSection,
            IeLengthOfFileOrSection lengthOfSection,
            IeSectionReadyQualifier qualifier) {
        ASdu aSdu = new ASdu(
                TypeId.F_SR_NA_1,
                false,
                CauseOfTransmission.FILE_TRANSFER,
                false,
                false,
                originatorAddress,
                commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { nameOfFile,
                                nameOfSection, lengthOfSection, qualifier } }) });
        return create_apdu(aSdu);
    }

    public APdu create_callOrSelectFiles(int commonAddress,
            CauseOfTransmission cot, int informationObjectAddress,
            IeNameOfFile nameOfFile, IeNameOfSection nameOfSection,
            IeSelectAndCallQualifier qualifier) {
        ASdu aSdu = new ASdu(TypeId.F_SC_NA_1, false, cot, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { nameOfFile,
                                nameOfSection, qualifier } }) });
        return create_apdu(aSdu);
    }

    public APdu create_lastSectionOrSegment(int commonAddress,
            int informationObjectAddress, IeNameOfFile nameOfFile,
            IeNameOfSection nameOfSection,
            IeLastSectionOrSegmentQualifier qualifier, IeChecksum checksum) {
        ASdu aSdu = new ASdu(TypeId.F_LS_NA_1, false,
                CauseOfTransmission.FILE_TRANSFER, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { nameOfFile,
                                nameOfSection, qualifier, checksum } }) });
        return create_apdu(aSdu);
    }

    public APdu create_ackFileOrSection(int commonAddress,
            int informationObjectAddress, IeNameOfFile nameOfFile,
            IeNameOfSection nameOfSection, IeAckFileOrSectionQualifier qualifier) {
        ASdu aSdu = new ASdu(TypeId.F_AF_NA_1, false,
                CauseOfTransmission.FILE_TRANSFER, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { nameOfFile,
                                nameOfSection, qualifier } }) });
        return create_apdu(aSdu);
    }

    public APdu create_sendSegment(int commonAddress,
            int informationObjectAddress, IeNameOfFile nameOfFile,
            IeNameOfSection nameOfSection, IeFileSegment segment) {
        ASdu aSdu = new ASdu(TypeId.F_SG_NA_1, false,
                CauseOfTransmission.FILE_TRANSFER, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { nameOfFile,
                                nameOfSection, segment } }) });
        return create_apdu(aSdu);
    }

    public APdu create_sendDirectory(int commonAddress,
            int informationObjectAddress, InformationElement[][] directory) {
        ASdu aSdu = new ASdu(TypeId.F_DR_TA_1, false,
                CauseOfTransmission.FILE_TRANSFER, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress, directory) });
        return create_apdu(aSdu);
    }

    public APdu create_queryLog(int commonAddress,
            int informationObjectAddress, IeNameOfFile nameOfFile,
            IeTime56 rangeStartTime, IeTime56 rangeEndTime) {
        ASdu aSdu = new ASdu(TypeId.F_SC_NB_1, false,
                CauseOfTransmission.FILE_TRANSFER, false, false,
                originatorAddress, commonAddress,
                new InformationObject[] { new InformationObject(
                        informationObjectAddress,
                        new InformationElement[][] { { nameOfFile,
                                rangeStartTime, rangeEndTime } }) });
        return create_apdu(aSdu);
    }

}
