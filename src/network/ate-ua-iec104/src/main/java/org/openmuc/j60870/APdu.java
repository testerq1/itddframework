/*
 * Copyright 2014 Fraunhofer ISE
 *
 * This file is part of j60870.
 * For more information visit http://www.openmuc.org
 *
 * j60870 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * j60870 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with j60870.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.openmuc.j60870;

import java.io.DataInputStream;
import java.io.IOException;

import org.apache.mina.core.buffer.IoBuffer;

public final class APdu {

	public enum APCI_TYPE {
		I_FORMAT, S_FORMAT, TESTFR_CON, TESTFR_ACT, STOPDT_CON, STOPDT_ACT, STARTDT_CON, STARTDT_ACT;
	}

	private int sendSeqNum = 0;

	private int receiveSeqNum = 0;

	private APCI_TYPE apciType;

	private ASdu aSdu = null;

	public APdu(int sendSeqNum, int receiveSeqNum, APCI_TYPE apciType, ASdu aSdu) {
		this.sendSeqNum = sendSeqNum;
		this.receiveSeqNum = receiveSeqNum;
		this.apciType = apciType;
		this.aSdu = aSdu;
	}

	public APdu(DataInputStream is, ConnectionSettings settings) throws IOException {
	    int start=is.readByte();
		int length = is.readByte() & 0xff;

		if (length < 4 || length > 253) {
			throw new IOException("APDU contain invalid length: " + length);
		}

		byte[] aPduHeader = new byte[4];
		is.readFully(aPduHeader);

		if ((aPduHeader[0] & 0x01) == 0) {
			apciType = APCI_TYPE.I_FORMAT;
			sendSeqNum = ((aPduHeader[0] & 0xfe) >> 1) + ((aPduHeader[1] & 0xff) << 7);
			receiveSeqNum = ((aPduHeader[2] & 0xfe) >> 1) + ((aPduHeader[3] & 0xff) << 7);

			aSdu = new ASdu(is, settings, length - 4);
		}
		else if ((aPduHeader[0] & 0x02) == 0) {
			apciType = APCI_TYPE.S_FORMAT;
			receiveSeqNum = ((aPduHeader[2] & 0xfe) >> 1) + ((aPduHeader[3] & 0xff) << 7);
		}
		else {
			if (aPduHeader[0] == 0x83) {
				apciType = APCI_TYPE.TESTFR_CON;
			}
			else if (aPduHeader[0] == 0x43) {
				apciType = APCI_TYPE.TESTFR_ACT;
			}
			else if (aPduHeader[0] == 0x23) {
				apciType = APCI_TYPE.STOPDT_CON;
			}
			else if (aPduHeader[0] == 0x13) {
				apciType = APCI_TYPE.STOPDT_ACT;
			}
			else if (aPduHeader[0] == 0x0B) {
				apciType = APCI_TYPE.STARTDT_CON;
			}
			else {
				apciType = APCI_TYPE.STARTDT_ACT;
			}
		}

	}

	public void encode(IoBuffer buffer, ConnectionSettings settings) throws IOException {
		buffer.put((byte)0x68);
		buffer.put((byte)0x00);
		
		if (apciType == APCI_TYPE.I_FORMAT) {
			buffer.put((byte) (sendSeqNum << 1));
			buffer.put((byte) (sendSeqNum >> 7));
			buffer.put((byte) (receiveSeqNum << 1));
			buffer.put((byte) (receiveSeqNum >> 7));
			aSdu.encode(buffer, settings);
		}
		else if (apciType == APCI_TYPE.STARTDT_ACT) {
		    buffer.put((byte) 0x07);
		    buffer.put((byte) 0x00);
		    buffer.put((byte) 0x00);
		    buffer.put((byte) 0x00);
		}
		else if (apciType == APCI_TYPE.STARTDT_CON) {
		    buffer.put((byte) 0x0b);
			buffer.put((byte) 0x00);
			buffer.put((byte) 0x00);
			buffer.put((byte) 0x00);
		}
		else if (apciType == APCI_TYPE.S_FORMAT) {
		    buffer.put((byte) 0x01);
		    buffer.put((byte) 0x00);
		    buffer.put((byte) (receiveSeqNum << 1));
		    buffer.put((byte) (receiveSeqNum >> 7));
		}
		int len=buffer.position();
		buffer.put(1,(byte) (buffer.position()-2));
	}

	public APCI_TYPE getApciType() {
		return apciType;
	}

	public int getSendSeqNumber() {
		return sendSeqNum;
	}

	public int getReceiveSeqNumber() {
		return receiveSeqNum;
	}

	public ASdu getASdu() {
		return aSdu;
	}

}
