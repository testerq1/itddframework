package ate.ua.iec;

import org.openmuc.j60870.APdu;
import org.openmuc.j60870.CauseOfTransmission;
import org.openmuc.j60870.IeQualifierOfInterrogation;
import org.openmuc.j60870.APdu.APCI_TYPE;
import org.testng.annotations.Test;

import ate.testcase.CsUATestcase;
import ate.testcase.Testcase;
import ate.testcase.UATestcase;
import ate.ua.iec104.PduFactory;
import ate.ua.iec104.IecUA;

public class IecTest extends CsUATestcase{

    @Test
    public void tt(){
        double c=1;
        double inter=0.04;
        for(int i=0;i<7;i++){
            c += (c*inter+1);
            log.info(c+"");
        }
        c=c*15368;
        log.info("total:"+c);
        log.info("8-14:");        
        for(int i=0;i<7;i++){
            c *= 1+inter;   
            log.info("total:"+c);
        }
        
        log.info("15-17:");
        for(int i=0;i<3;i++){
            c = (c-10000)*(1+inter);   
            log.info("total:"+c);
        }
        
        log.info("18-21:");
        for(int i=0;i<4;i++){
            c = (c-20000)*(1+inter);   
            log.info("total:"+c);
        }
        log.info("22-25:");
        for(int i=0;i<4;i++){
            c *= 1+inter;   
            log.info("total:"+c);
        }
        log.info("total:"+(c-50000));
    }
    
    public void iec104(){
        //ClientApp.main(new String[]{});
        IecUA svr=this.create_server("iec104://"+siip+":1234/tcp",new IecUA());
        IecUA clt=this.create_client("iec104://"+siip+":1234/tcp",new IecUA());
        this.start_all_ua();
        assertTrue(clt.is_ready()&&svr.is_ready());
        
        APdu pdu=clt.create_apdu(APCI_TYPE.STARTDT_ACT);
        clt.send_message(pdu);
        
        APdu rcv=svr.recv_message();
        assertTrue(rcv!=null&&rcv.getApciType()==APCI_TYPE.STARTDT_ACT);
        
        pdu=svr.create_apdu(APCI_TYPE.STARTDT_CON);
        svr.send_message(pdu);
        
        rcv=clt.recv_message();
        assertTrue(rcv!=null&&rcv.getApciType()==APCI_TYPE.STARTDT_CON);
        
        PduFactory pf=svr.get_pdu_factory();
        int commonAddress=0;
        pdu=pf.create_interrogation(commonAddress, CauseOfTransmission.ACTIVATION,
                new IeQualifierOfInterrogation(0));
        clt.send_message(pdu);
        
        rcv=svr.recv_message();
        assertTrue(rcv!=null&&rcv.getApciType()==APCI_TYPE.I_FORMAT);
    }

}
