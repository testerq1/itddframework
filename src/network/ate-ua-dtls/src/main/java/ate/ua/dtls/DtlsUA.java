package ate.ua.dtls;

import ate.AbstractURIHandler;

/**
 * 实现方式1(native)：
 * https://github.com/wolfSSL/wolfssljni.git
 * https://github.com/cyassl/cyassl
 * 
 * 实现方式2：
 * https://github.com/eclipse/californium.scandium
 * 
 * 实现方式3：
 * (io) https://www.bouncycastle.org/java.html
 * https://github.com/bcgit/bc-java/blob/master/core/src/test/java/org/bouncycastle/crypto/tls/test/DTLSClientTest.java
 * 
 * @author ravihuang
 *
 */
public class DtlsUA extends AbstractURIHandler{

    @Override
    public void teardown() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void startup() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String get_default_schema() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean is_ready() {
        // TODO Auto-generated method stub
        return false;
    }

}
