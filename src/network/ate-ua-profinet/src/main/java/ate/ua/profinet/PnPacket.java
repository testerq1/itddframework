package ate.ua.profinet;

import java.io.OutputStream;

import ate.ua.AbstractCustomPacket;

public class PnPacket extends AbstractCustomPacket<PnPacket>{

    @Override
    public PnPacket build(String k, Object v) {
        return this;
    }

    @Override
    public PnPacket get_packet() {

        return this;
    }

    @Override
    public void encodePacket(OutputStream out) {
        
    }

}
