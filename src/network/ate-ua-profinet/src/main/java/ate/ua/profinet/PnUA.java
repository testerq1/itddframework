package ate.ua.profinet;

import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;

import ate.ua.raw.mina.ARawCustomUAImp;

public class PnUA extends ARawCustomUAImp<PnPacket>{

    @Override
    public int get_port(){
        port=0x8892;
        return port;
    }
    
    @Override
    public boolean message_matched(PnPacket msg, HashMap<String, Object> hm) {
        return true;
    }

    @Override
    public PnPacket decodePacket(IoBuffer buf) {
        return null;
    }

    @Override
    public String get_default_schema() {
        return "profinet";
    }

}
