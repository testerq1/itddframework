package ate.test;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.text.*;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import ate.testcase.CsUATestcase;

public class SipUATest extends CsUATestcase {
	String sip_reg = "REGISTER sip:192.168.1.2 SIP/2.0\r\n" + 
			"Via: SIP/2.0/UDP 192.168.1.2:5060;branch=z9hG4bKnp104984053-44ce4a41192.168.1.2;rport\r\n" +
			"From: <sip:{0}@voip.brurjula.net>;tag=6433ef9\r\n" +
			"To: <sip:{0}@voip.brujula.net>\r\n" +
			"Call-ID: 105090259-446faf7b@192.168.1.2\r\n" +
			"CSeq: {1} REGISTER\r\n" +
			"Content-Length: 0\r\n" +
			"Max-Forwards: 70\r\n";
	
	String sip_req_cd="{0} sip:{2}@voip.brujula.net SIP/2.0\r\n"+
			"Via: SIP/2.0/UDP 192.168.1.2;branch=z9hG4bKnp104984053-44ce4a41192.168.1.2;rport\r\n"+
			"From: \"arik\" <sip:{1}@voip.brurjula.net>\r\n"+
			"To: <sip:{2}@voip.brujula.net>;tag=6433ef9\r\n"+
			"Call-ID: 105090259-446faf7a@192.168.1.2\r\n"+
			"CSeq: 1 {0}\r\n"+
			"Content-Length: 0\r\n"+
			"Max-Forwards: 70\r\n"+
			"User-Agent: Nero SIPPS IP Phone Version 2.0.51.16\r\n";
	
	String sip_req_cg="{0} sip:{2}@voip.brujula.net SIP/2.0\r\n" +
			"Via: SIP/2.0/UDP 192.168.1.2:5060;branch=z9hG4bKnp104984053-44ce4a41192.168.1.2;rport\r\n" +
			"From: \"arik\" <sip:{1}@voip.brurjula.net>;tag=6433ef9\r\n" +
			"To: <sip:{2}@voip.brujula.net>\r\n" +
			"Call-ID: 105090259-446faf7a@192.168.1.2\r\n" +
			"CSeq: 1 {0}\r\n" +
			"Content-Length: 0\r\n" +
			"Max-Forwards: 70\r\n";
	
	String sip_req_cg_o="{0} sip:{2}@voip.brujula.net SIP/2.0\r\n" +
			"Via: SIP/2.0/UDP 192.168.1.2:5060;branch=z9hG4bKnp104984053-44ce4a41192.168.1.2;rport\r\n" +
			"From: \"arik\" <sip:{1}@voip.brurjula.net>;tag=6433ef9\r\n" +
			"To: <sip:{2}@voip.brujula.net>\r\n" +
			"Call-ID: 105090259-446faf7a@192.168.1.2\r\n" +
			"CSeq: 1 {0}\r\n" +
			"User-Agent: Nero SIPPS IP Phone Version 2.0.51.16\r\n" +
			"Expires: 120\r\n" +
			"Accept: application/sdp\r\n" +
			"Content-Type: application/sdp\r\n" +
			"Content-Length: 272\r\n" +
			"Contact: <sip:{1}@192.168.1.2>\r\n" +
			"Max-Forwards: 70\r\n" +
			"Allow: INVITE, ACK, CANCEL, BYE, REFER, OPTIONS, NOTIFY, INFO\r\n\r\n" +			
			"v=0\r\n" +
			"o=SIPPS 105015165 105015162 IN IP4 192.168.1.2\r\n" +
			"s=SIP call\r\n" +
			"c=IN IP4 192.168.1.2\r\n" +
			"t=0 0\r\n" +
			"m=audio 30000 RTP/AVP 0 8 97 2 3\r\n" +
			"a=rtpmap:0 pcmu/8000\r\n" +
			"a=rtpmap:8 pcma/8000\r\n" +
			"a=rtpmap:97 iLBC/8000\r\n" +
			"a=rtpmap:2 G726-32/8000\r\n" +
			"a=rtpmap:3 GSM/8000\r\n" +
			"a=fmtp:97 mode=20\r\n" +
			"a=sendrecv\r\n";
	String sip_resp="SIP/2.0 {0} {1}r\n" +
			"Via: SIP/2.0/UDP 192.168.1.2:5060;branch=z9hG4bKnp104984053-44ce4a41192.168.1.2;rport=5060;received=80.230.219.70\r\n" +
			"From: \"arik\" <sip:{2}@voip.brurjula.net>;tag=6433ef9\r\n" +
			"To: <sip:{3}@voip.brujula.net>;tag=fhdgfhdgfhdf\r\n" +
			"Call-ID: 105090259-446faf7a@192.168.1.2\r\n" +
			"CSeq: 1 {4}\r\n" +
			"Server: Sip EXpress router (0.8.12 (i386/linux))\r\n" +
			"Content-Length: 0\r\n" +
			"Warning: 392 200.68.120.81:5060 \"Noisy feedback tells:  pid=32642 req_src_ip=80.230.219.70 req_src_port=5060 in_uri=sip:97239287044@voip.brujula.net out_uri=sip:97239287044@voip.brujula.net via_cnt==1\"\r\n";
	String sip_ack="ACK sip:{1}@voip.brujula.net SIP/2.0\r\n"+
			"Via: SIP/2.0/UDP 192.168.1.2:5060;branch=z9hG4bKnp104984053-44ce4a41192.168.1.2s;rport\r\n" +
			"From: \"arik\" <sip:{0}@voip.brurjula.net>;tag=6433ef9\r\n" +
			"To: <sip:{1}@voip.brujula.net>\r\n" +
			"Call-ID: {2}\r\n" +
			"CSeq: 1 ACK\r\n"+
			"Contact: <sip:{1}@192.168.1.2>\r\n" +
			"Content-Length: 0\r\n";
	
	@DataProvider(name = "Data_Basical_call")
	public Object[][] Data_Basical_call() {
		return [["BYE","cg"],["CANCEL","cg"],
				["BYE","cd"],["CANCEL","cd"]];
	}
	
	@Test(dataProvider = "Data_Basical_call")
	public void basical_call(String method,String handoff) throws ParseException {		
		def svr = create_server("sip://$sip:5060/udp");
		def clt = create_client("sip://$sip:5060/udp");
		this.start_all_ua();
		assertTrue(clt.is_ready());
		assertTrue(svr.is_ready());
		
		String callid="105090259-446faf78@192.168.1.2";
		def send0=clt.createSIPMessage(MessageFormat.format(sip_req_cg_o,"INVITE","816666","97239287044"));
		send0.setCallId(callid);
		clt.send_message(send0);
		
		def req = svr.recv_request("INVITE", "816666", "97239287044");
		assertTrue(req != null);

		def send1=clt.createSIPMessage(MessageFormat.format(sip_resp, "200","OK","816666", "97239287044","INVITE"));
		send1.setCallId(callid);
		
		svr.send_message(send1);
		def resp = clt.recv_response(200, "816666", "97239287044");
		assertTrue(resp != null);
		
		clt.send_string(MessageFormat.format(sip_ack,"816666", "97239287044",callid));
		def ack=svr.recv_request("ACK", "816666", "97239287044");
		assertTrue(ack != null);
		
		log.info("挂机");
		if(handoff.equals("cg")){
			send0=clt.createSIPMessage(MessageFormat.format(sip_req_cg,method,"816666","97239287044"));
			send0.setCallId(callid);
			clt.send_message(send0);
			req = svr.recv_request(method, "816666", "97239287044");
			assertTrue(req != null);
			send1=svr.createSIPMessage(MessageFormat.format(sip_resp, "200","OK","816666", "97239287044",method));
			send1.setCallId(callid);
			
			svr.send_message(send1);
			resp = clt.recv_response(200, "816666", "97239287044");
			assertTrue(resp != null);
		}else{
			send0=svr.createSIPMessage(MessageFormat.format(sip_req_cg,method,"97239287044","816666"));
			send0.setCallId(callid);
			svr.send_message(send0);
			req = clt.recv_request(method, "97239287044", "816666");
			assertTrue(req != null);
			send1=clt.createSIPMessage(MessageFormat.format(sip_resp, "200","OK","816666", "97239287044",method));
			send1.setCallId(callid);
			
			clt.send_message(send1);
			resp = svr.recv_response(200, "816666", "97239287044");
			assertTrue(resp != null);
		}
		
	}
	@DataProvider(name = "Data_TestStep")
	public Object[][] Data_TestStep() {
		return [[ "487","adsasd" ] ,[ "484","busy" ]];
	}
	@Test(dataProvider = "Data_TestStep")
	public void cancel(String code,String reason) {
		def svr =  create_server("sip://$sip:5060/udp");
		def clt = create_client("sip://$sip:5060/udp");
		this.start_all_ua();
		assertTrue(clt.is_ready());
		assertTrue(svr.is_ready());
		
		clt.send_string(MessageFormat.format(sip_req_cg_o,"INVITE","816666","97239287044"));
		
		def req = svr.recv_request("INVITE", "816666", "97239287044");
		assertTrue(req != null);

		svr.send_string(MessageFormat.format(sip_resp, code,reason,"816666", "97239287044"));
		def resp = clt.recv_response(code, "816666", "97239287044");
		assertTrue(resp != null);
	}

	@Test(dataProvider = "Data_TestStep")
	public void register(String code,String reason) {
		def svr = create_server("sip://$sip:5060/udp");
		def clt = create_client("sip://$sip:5060/udp");
		this.start_all_ua();
		assertTrue(clt.is_ready());
		assertTrue(svr.is_ready());

		clt.send_string(MessageFormat.format(sip_reg,"816666", "1"));
		
		def req = svr.recv_request("REGISTER", "816666", "816666");
		assertTrue(req != null);
		
		svr.send_string(MessageFormat.format(sip_resp, code,reason,"816666", "816666"));
		def resp = clt.recv_response(code, "816666", "816666");
		assertTrue(resp != null);
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

}
