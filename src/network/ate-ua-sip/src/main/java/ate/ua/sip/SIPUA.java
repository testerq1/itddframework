package ate.ua.sip;

/*
 * #%L
 * iTDD UA SIP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import gov.nist.javax.sip.message.SIPMessage;
import gov.nist.javax.sip.message.SIPRequest;
import gov.nist.javax.sip.message.SIPResponse;
import gov.nist.javax.sip.parser.StringMsgParser;

import java.text.ParseException;
import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import ate.ua.IAutoResponseMessage;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;

public class SIPUA extends AMinaUAImp<SIPMessage> implements
		IAutoResponseMessage<IoSession, SIPMessage> {
	protected HashMap<String, String> autoRespAttr = null;

	class SipDecoder extends CumulativeProtocolDecoder {
		@Override
		protected boolean doDecode(IoSession session, IoBuffer in,
				ProtocolDecoderOutput out) throws Exception {
			if (in.remaining() >= 0) {
				log.debug("doDecode");
				byte[] b = new byte[in.remaining()];
				in.get(b);
				SIPMessage msg = parser.parseSIPMessage(b);
				out.write(msg);
				return true;
			}
			return false;
		}
	}

	class SipEncoder extends ProtocolEncoderAdapter {
		@Override
		public void encode(IoSession session, Object message,
				ProtocolEncoderOutput out) throws Exception {
			SIPMessage pkt = null;
			if (message instanceof SIPMessage) {
				pkt = (SIPMessage) message;
			} else {
				pkt = parser.parseSIPMessage(message.toString());
			}
			out.write(IoBuffer.wrap(pkt.encodeAsBytes()));
		}

	}

	public static StringMsgParser parser = new StringMsgParser();

	@Override
	public void add_default_filter() {
		add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(
				new SipEncoder(), new SipDecoder()));
	}

	public SIPMessage createSIPMessage(String message) {
		try {
			return parser.parseSIPMessage(message);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public SIPMessage get_auto_resp_msg(IoSession session, SIPMessage req) {
		if (this.autoRespAttr == null)
			return null;
		log.debug("{} get_auto_resp_msg {}", this, req);
		if (!(req instanceof SIPRequest)) {
			return null;
		}
		// 准备处理200 invite的ACK
		// else{
		// SIPResponse resp=(SIPResponse)req;
		//
		// }
		SIPRequest msg = (SIPRequest) req;
		return msg.createResponse(200);
	}

	@Override
	public String get_default_schema() {
		return "sip";
	}

	@Override
	public boolean message_matched(SIPMessage omsg, HashMap<String, Object> hm) {
		if (hm == null || hm.size() == 0)
			return true;
		String type = hm.get("type").toString();
		if (type.equalsIgnoreCase("request")) {
			if (omsg instanceof SIPRequest) {
				SIPRequest msg = (SIPRequest) omsg;
				return hm.get("method").equals(msg.getMethod())
						&& msg.getFrom().toString()
								.contains(hm.get("from").toString())
						&& msg.getTo().toString()
								.contains(hm.get("to").toString());
			}
		} else {
			if (omsg instanceof SIPResponse) {
				SIPResponse msg = (SIPResponse) omsg;
				return hm.get("statuscode").equals(msg.getStatusCode() + "")
						&& msg.getFrom().toString()
								.contains(hm.get("from").toString())
						&& msg.getTo().toString()
								.contains(hm.get("to").toString());
			}
		}
		return false;
	}

	public SIPRequest recv_request(String method, String from, String to) {
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("type", "request");
		hm.put("method", method);
		hm.put("to", to);
		hm.put("from", from);
		return (SIPRequest) this.recv_message(hm);
	}

	public SIPResponse recv_response(int statuscode, String from, String to) {		
		return recv_response(statuscode + "",from,to);
	}
	
	public SIPResponse recv_response(String statuscode, String from, String to) {
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("type", "response");
		hm.put("statuscode", statuscode + "");
		hm.put("to", to);
		hm.put("from", from);
		return (SIPResponse) this.recv_message(hm);
	}
}
