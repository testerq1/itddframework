package ate.test;

/*
 * #%L
 * ate-ua-dhcp
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.Method;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.HashMap;

import org.dhcp4java.DHCPPacket;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

import ate.testcase.CsUATestcase;
import ate.ua.dhcp.DhcpUA;
import ate.ua.dhcp.DhcpUtils;

public class DhcpUATest extends CsUATestcase{
	
//	public static void main(String[] args){
//		try {  
//            Object mul_ip1;
//			// 组播ip1  
//            InetAddress a1 = InetAddress.getByName("239.0.0.1");  
//            // 组播ip2  
//            InetAddress a2 =InetAddress.getByName("239.0.0.2");  
//  
//            // socket组(随意绑定一个空闲udp端，这里也可以使用匿名构造函数)  
//            MulticastSocket multicastSocket = new MulticastSocket();  
//  
//            multicastSocket.joinGroup(a1);  
//            multicastSocket.joinGroup(a2);  
//  
//            String content1 = "a1:你在哪里，亲。。。？";  
//            String content2 = "a2:你死哪里去了，亲。。。？";  
//  
//            DatagramPacket packet1 = new DatagramPacket(content1.getBytes(),content1.getBytes().length,a1,67);  
//            DatagramPacket packet2 = new DatagramPacket(content2.getBytes(),content2.getBytes().length,a2,67);  
//  
//            multicastSocket.send(packet1);  
//              
//  
//            multicastSocket.send(packet2);  
//              
//        } catch (Exception e) {  
//            e.printStackTrace();  
//        }  
//	}
	
	@Test
	public void base() {
		log.info(STEP+" base:");
		log.info(STEP+".1 init:");
		DhcpUA svr = (DhcpUA)this.create_server("dhcp://0.0.0.0:67");
		DhcpUA clt = (DhcpUA)this.create_server("dhcp://127.0.0.1:68");
		
		this.start_all_ua();
		assertTrue(svr.is_ready()&&clt.is_ready());		
		assertTrue(svr.get_transport().equals("udp")&&clt.get_transport().equals("udp"));	
		assertTrue(svr.get_port()==67&&clt.get_port()==68);		
		log.info(STEP+".2 client send/recv message:");	
		Object msg =clt.create_message(new HashMap<String, String>(){{
			put("boot","request");
			put("dhcp","discover");
			put("DHO_HOST_NAME","abc");
		}});		
		clt.broadcast(msg, "255.255.255.255:67","0.0.0.0:68");
		DHCPPacket pkt=svr.recv_message();
		assertTrue(DhcpUtils.has_option(pkt, "DHO_HOST_NAME", "abc"));
		assertFalse(DhcpUtils.has_option(pkt, "DHO_HOST_NAME", "abcd"));		
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {		
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		//;
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}

}
