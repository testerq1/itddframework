package ate.ua.dhcp;

/*
 * #%L
 * ate-ua-dhcp
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.dhcp4java.DHCPConstants.DHO_ARP_CACHE_TIMEOUT;
import static org.dhcp4java.DHCPConstants.DHO_BOOTFILE;
import static org.dhcp4java.DHCPConstants.DHO_BOOT_SIZE;
import static org.dhcp4java.DHCPConstants.DHO_BROADCAST_ADDRESS;
import static org.dhcp4java.DHCPConstants.DHO_COOKIE_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_DHCP_LEASE_TIME;
import static org.dhcp4java.DHCPConstants.DHO_DHCP_MAX_MESSAGE_SIZE;
import static org.dhcp4java.DHCPConstants.DHO_DHCP_MESSAGE;
import static org.dhcp4java.DHCPConstants.DHO_DHCP_REBINDING_TIME;
import static org.dhcp4java.DHCPConstants.DHO_DHCP_RENEWAL_TIME;
import static org.dhcp4java.DHCPConstants.DHO_DHCP_REQUESTED_ADDRESS;
import static org.dhcp4java.DHCPConstants.DHO_DHCP_SERVER_IDENTIFIER;
import static org.dhcp4java.DHCPConstants.DHO_DOMAIN_NAME;
import static org.dhcp4java.DHCPConstants.DHO_DOMAIN_NAME_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_EXTENSIONS_PATH;
import static org.dhcp4java.DHCPConstants.DHO_FINGER_SERVER;
import static org.dhcp4java.DHCPConstants.DHO_FONT_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_HOST_NAME;
import static org.dhcp4java.DHCPConstants.DHO_IMPRESS_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_INTERFACE_MTU;
import static org.dhcp4java.DHCPConstants.DHO_IRC_SERVER;
import static org.dhcp4java.DHCPConstants.DHO_LOG_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_LPR_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_MAX_DGRAM_REASSEMBLY;
import static org.dhcp4java.DHCPConstants.DHO_MERIT_DUMP;
import static org.dhcp4java.DHCPConstants.DHO_MOBILE_IP_HOME_AGENT;
import static org.dhcp4java.DHCPConstants.DHO_NAME_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_NAME_SERVICE_SEARCH;
import static org.dhcp4java.DHCPConstants.DHO_NDS_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_NDS_TREE_NAME;
import static org.dhcp4java.DHCPConstants.DHO_NETBIOS_DD_SERVER;
import static org.dhcp4java.DHCPConstants.DHO_NETBIOS_NAME_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_NETBIOS_SCOPE;
import static org.dhcp4java.DHCPConstants.DHO_NISPLUS_DOMAIN;
import static org.dhcp4java.DHCPConstants.DHO_NISPLUS_SERVER;
import static org.dhcp4java.DHCPConstants.DHO_NIS_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_NNTP_SERVER;
import static org.dhcp4java.DHCPConstants.DHO_NTP_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_NWIP_DOMAIN_NAME;
import static org.dhcp4java.DHCPConstants.DHO_PATH_MTU_AGING_TIMEOUT;
import static org.dhcp4java.DHCPConstants.DHO_PATH_MTU_PLATEAU_TABLE;
import static org.dhcp4java.DHCPConstants.DHO_POLICY_FILTER;
import static org.dhcp4java.DHCPConstants.DHO_POP3_SERVER;
import static org.dhcp4java.DHCPConstants.DHO_RESOURCE_LOCATION_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_ROOT_PATH;
import static org.dhcp4java.DHCPConstants.DHO_ROUTERS;
import static org.dhcp4java.DHCPConstants.DHO_ROUTER_SOLICITATION_ADDRESS;
import static org.dhcp4java.DHCPConstants.DHO_SMTP_SERVER;
import static org.dhcp4java.DHCPConstants.DHO_STATIC_ROUTES;
import static org.dhcp4java.DHCPConstants.DHO_STDA_SERVER;
import static org.dhcp4java.DHCPConstants.DHO_STREETTALK_SERVER;
import static org.dhcp4java.DHCPConstants.DHO_SUBNET_MASK;
import static org.dhcp4java.DHCPConstants.DHO_SUBNET_SELECTION;
import static org.dhcp4java.DHCPConstants.DHO_SWAP_SERVER;
import static org.dhcp4java.DHCPConstants.DHO_TCP_KEEPALIVE_INTERVAL;
import static org.dhcp4java.DHCPConstants.DHO_TFTP_SERVER;
import static org.dhcp4java.DHCPConstants.DHO_TIME_OFFSET;
import static org.dhcp4java.DHCPConstants.DHO_TIME_SERVERS;
import static org.dhcp4java.DHCPConstants.DHO_USER_AUTHENTICATION_PROTOCOL;
import static org.dhcp4java.DHCPConstants.DHO_VENDOR_CLASS_IDENTIFIER;
import static org.dhcp4java.DHCPConstants.DHO_WWW_SERVER;
import static org.dhcp4java.DHCPConstants.DHO_X_DISPLAY_MANAGER;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.dhcp4java.DHCPConstants;
import org.dhcp4java.DHCPOption;
import org.dhcp4java.DHCPPacket;

import ate.util.X2X0;

public class DhcpUtils extends X2X0{
	public static boolean has_option(DHCPPacket pkt,String name, String value){
		if(pkt==null){
			log.warn("pkt is null");
			return false;
		}
		
		DHCPOption opt=create_option(name,value);
		if(opt==null)
			throw new Error(name + " has bad value: "+  value);
		
		return pkt.getOption(opt.getCode()).equals(opt);
		
	}
	
	public static DHCPOption create_option(String key, String value) {
		byte code = DHCPConstants.getDhoNamesReverse(key.toUpperCase());
		switch (code) {
		case DHO_BOOT_SIZE:
		case DHO_MAX_DGRAM_REASSEMBLY:
		case DHO_INTERFACE_MTU:
		case DHO_DHCP_MAX_MESSAGE_SIZE:
			return DHCPOption.newOptionAsShort(code, Short.parseShort(value));
		case DHO_NAME_SERVICE_SEARCH:
		case DHO_PATH_MTU_PLATEAU_TABLE: {
			String[] tmp = value.trim().split(": ");
			short[] ss = new short[tmp.length];
			for (int i = 0; i < tmp.length; i++)
				ss[i] = Short.parseShort(tmp[i]);
			return DHCPOption.newOptionAsShorts(code, ss);
		}
		case DHO_TIME_OFFSET:
		case DHO_PATH_MTU_AGING_TIMEOUT:
		case DHO_ARP_CACHE_TIMEOUT:
		case DHO_TCP_KEEPALIVE_INTERVAL:
		case DHO_DHCP_LEASE_TIME:
		case DHO_DHCP_RENEWAL_TIME:
		case DHO_DHCP_REBINDING_TIME:
			return DHCPOption.newOptionAsInt(code, Integer.parseInt(value));
		case DHO_SUBNET_MASK:
		case DHO_SWAP_SERVER:
		case DHO_BROADCAST_ADDRESS:
		case DHO_ROUTER_SOLICITATION_ADDRESS:
		case DHO_DHCP_REQUESTED_ADDRESS:
		case DHO_DHCP_SERVER_IDENTIFIER:
		case DHO_SUBNET_SELECTION: {
			InetAddress addr = null;
			try {
				addr = InetAddress.getByAddress(X2X0.ip2b(value));
			} catch (UnknownHostException e) {

				e.printStackTrace();
			}
			return DHCPOption.newOptionAsInetAddress(code, addr);
		}
		case DHO_ROUTERS:
		case DHO_TIME_SERVERS:
		case DHO_NAME_SERVERS:
		case DHO_DOMAIN_NAME_SERVERS:
		case DHO_LOG_SERVERS:
		case DHO_COOKIE_SERVERS:
		case DHO_LPR_SERVERS:
		case DHO_IMPRESS_SERVERS:
		case DHO_RESOURCE_LOCATION_SERVERS:
		case DHO_POLICY_FILTER:
		case DHO_STATIC_ROUTES:
		case DHO_NIS_SERVERS:
		case DHO_NTP_SERVERS:
		case DHO_NETBIOS_NAME_SERVERS:
		case DHO_NETBIOS_DD_SERVER:
		case DHO_FONT_SERVERS:
		case DHO_X_DISPLAY_MANAGER:
		case DHO_MOBILE_IP_HOME_AGENT:
		case DHO_SMTP_SERVER:
		case DHO_POP3_SERVER:
		case DHO_NNTP_SERVER:
		case DHO_WWW_SERVER:
		case DHO_FINGER_SERVER:
		case DHO_IRC_SERVER:
		case DHO_STREETTALK_SERVER:
		case DHO_STDA_SERVER:
		case DHO_NDS_SERVERS: {
			InetAddress[] addrs = null;
			String tmp[] = value.split(" ");
			for (int i = 0; i < tmp.length; i++) {
				try {
					addrs[i] = InetAddress.getByAddress(X2X0.ip2b(tmp[i]));
				} catch (UnknownHostException e) {

					e.printStackTrace();
				}
			}
			return DHCPOption.newOptionAsInetAddresses(code, addrs);
		}
		case DHO_HOST_NAME:
		case DHO_MERIT_DUMP:
		case DHO_DOMAIN_NAME:
		case DHO_ROOT_PATH:
		case DHO_EXTENSIONS_PATH:
		case DHO_NETBIOS_SCOPE:
		case DHO_DHCP_MESSAGE:
		case DHO_VENDOR_CLASS_IDENTIFIER:
		case DHO_NWIP_DOMAIN_NAME:
		case DHO_NISPLUS_DOMAIN:
		case DHO_NISPLUS_SERVER:
		case DHO_TFTP_SERVER:
		case DHO_BOOTFILE:
		case DHO_NDS_TREE_NAME:
		case DHO_USER_AUTHENTICATION_PROTOCOL:
			return DHCPOption.newOptionAsString(code, value);
		default:
			break;

		}
		return null;
	}
}
