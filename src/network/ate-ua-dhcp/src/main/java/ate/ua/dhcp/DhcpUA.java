package ate.ua.dhcp;

/*
 * #%L
 * ate-ua-dhcp
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.dhcp4java.DHCPConstants.HTYPE_ETHER;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.dhcp4java.DHCPConstants;
import org.dhcp4java.DHCPPacket;

import ate.AbstractURIHandler;
import ate.ua.AbstractPacketUAImp;
import ate.ua.AtePacket;
import ate.ua.UAOption;
import ate.ua.netty.AFilterFactory;
import ate.ua.netty.ANettyUAImp;
import ate.ua.netty.MulticastPacket;

public class DhcpUA extends ANettyUAImp<DHCPPacket> {
	class DhcpDecoder extends MessageToMessageDecoder<DatagramPacket> {
		@Override
		protected void decode(ChannelHandlerContext ctx, DatagramPacket in,
				List<Object> out) throws Exception {	
			byte[] bs = new byte[in.content().readableBytes()]; 
			in.content().readBytes(bs); 
			log.debug("{} decode byte buffer {}",DhcpUA.this, bs.length);			
			if(bs.length>0){				
				DHCPPacket msg=DHCPPacket.getPacket(bs, 0, bs.length, strict);
				log.debug("{} decode and add_packet {}",this, msg);
				
				add_packet(new AtePacket(ctx.channel(), msg));
				out.add(msg);
			}
		}
	}

	class DhcpEncoder extends MessageToMessageEncoder<Object> {		
		@Override
		protected void encode(ChannelHandlerContext ctx, Object msg,
				List<Object> out) throws Exception {			
			log.debug("{} encode {}",DhcpUA.this, msg);
			if (msg == null) 
				return;
			if(msg instanceof DHCPPacket)			
				out.add(Unpooled.copiedBuffer(((DHCPPacket)msg).serialize()));
			else if(msg instanceof MulticastPacket){
				DatagramPacket udppkt=null;
				MulticastPacket mpkt=(MulticastPacket)msg;
				ByteBuf buff=Unpooled.copiedBuffer(((DHCPPacket)mpkt.get_message()).serialize());
				String[] recipient=mpkt.get_recipient().split(":");
				InetSocketAddress rmt=new InetSocketAddress(recipient[0],Integer.parseInt(recipient[1]));
//				if(mpkt.get_sender()!=null){
//					String[] sender=mpkt.get_sender().split(":");
//					InetSocketAddress lcl=new InetSocketAddress(sender[0],Integer.parseInt(sender[1]));
//					udppkt= new DatagramPacket(buff,rmt,lcl);					
//				}else{
//					udppkt= new DatagramPacket(buff,rmt);
//				}			
				udppkt= new DatagramPacket(buff,rmt);
				out.add(udppkt);
			}
		}
	}

	private Map<String, Byte> bootParas = new LinkedHashMap<String, Byte>();
	private Map<String, Byte> dhcpCodeParas = new LinkedHashMap<String, Byte>();
	private byte hlen = 6;
	private byte hops = 0;
	private byte htype = HTYPE_ETHER;

	private Map<String, Byte> htypeParas = new LinkedHashMap<String, Byte>();

	private boolean strict = true;

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option,value);
		
		if(option==UA_TYPE){
			this.transport = "udp";
			Map<Byte, String> mm = DHCPConstants.getBootNamesMap();
			for (Byte b : mm.keySet())
				bootParas.put(mm.get(b).toLowerCase(), b);
			
			mm = DHCPConstants.getHtypesMap();
			for (Byte b : mm.keySet())
				htypeParas.put(mm.get(b).toLowerCase(), b);
			mm = DHCPConstants.getDhcpCodesMap();
			
			for (Byte b : mm.keySet())
				dhcpCodeParas.put(mm.get(b).toLowerCase(), b);
			
			if (this.port <= 0) {
				if (type == CLIENT)
					port = 68;
				else
					port = 67;
			}
			
			set_filters_factory(new AFilterFactory() {
				@Override
				public List<ChannelHandler> create_filters() {
					return new ArrayList<ChannelHandler>(Arrays.asList(
							new DhcpDecoder(), new DhcpEncoder()));
				}
			});
		}
		return this;
	}
	
	

	/**
	 * hm=["boot":"request","htype":"fddi","dhcp":"discover","dho_host_name":
	 * "abc"]
	 * 
	 * @param paras
	 * @return
	 */
	public DHCPPacket create_message(HashMap<String, String> paras) {
		log.debug("{} create_message {}",this,paras);
		
		HashMap<String, String> hm = (HashMap<String, String>) paras.clone();

		DHCPPacket pkt = new DHCPPacket();
		if (hm.get("boot") != null)
			pkt.setOp(bootParas.remove("boot" + hm.get("boot").toLowerCase()));
		if (hm.get("htype") != null)
			pkt.setHtype(htypeParas.remove("htype_" + hm.get("htype").toLowerCase()));
		if (hm.get("dhcp") != null)
			pkt.setDHCPMessageType(dhcpCodeParas.remove("dhcp" + hm.get("dhcp").toLowerCase()));

		for (String key : hm.keySet()) {
			if (key.toLowerCase().startsWith("dho")) {
				pkt.setOption(DhcpUtils.create_option(key, hm.get(key)));
			}
		}

		if (pkt.getHtype() <= 0)
			pkt.setHtype(htype);

		if (pkt.getHtype() == HTYPE_ETHER) {
			pkt.setHlen((byte) 6);
			pkt.setHops((byte) 0);
		} else {
			// TODO
		}

		return pkt;
	}

	@Override
	public String get_default_schema() {
		return "dhcp";
	}

	@Override
	public boolean message_matched(DHCPPacket msg, HashMap<String,Object> hm) {
		return true;
	}
}
