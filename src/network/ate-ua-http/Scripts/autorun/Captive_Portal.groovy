import java.lang.reflect.Method;
import org.testng.annotations.Test;
import org.testng.annotations.*;
import ate.testcase.*;
import ate.ua.http.*;

public class CPortalTest extends CsUATestcase {	
	String uri="http://127.0.0.1:80/";
	@Test
	public void captive_portal_01() {
		def svr = create_server(uri);
		def clt = create_client("http://localhost/");
		start_all_ua();
		
		assertTrue(svr.is_ready());
		assertTrue(clt.is_ready());
		log.info "${STEP}.1 client 发送Get，不带Cookie：" 
		def req = clt.create_request(
			["pkttype":"GET", 
			 "uri":"bpss/login.jsp?wlanacname=0116.0755.200.00&wlanuserip=172.24.117.147&ssid=CMCC&wlanacip=221.179.21.230",
			 "Cache-Control":"no-cache"			 
			 ]
		)
		clt.send_message(req);
		
		log.info "${STEP}.2 Server收到Get："
		def o=svr.recv_message();
		
		log.info "${STEP}.3 Server发送响应200，带cookie："
		def response=svr.create_response(o, 
			["status_code":"200",
			 "Server":"Apache-Coyote/1.1",
			 "Set-Cookie":"JSESSIONID=9BCE11A083A5363D6F587BEFEC7DEF02; Path=/bpss",
			 "Pragma":"No-cache",
			 "Cache-Control":"no-cache",
			 "Expires":"Thu, 01 Jan 1970 00:00:00 GMT",
			 "Content-Type":"text/html;charset=GBK",
			 "Date":"Thu, 15 Nov 2012 06:21:30 GMT",
			 "content":"""<HTML></HTML>"""
		]);
		svr.send_message(response);		
		
		log.info "${STEP}.4 client 收200："
		o= clt.recv_message();
		assertTrue(o.getStatusLine().getStatusCode()==200);
		
		log.info "${STEP}.5 client 重发Get，带Cookie："
		req.addHeader("Cookie","JSESSIONID=7EB114CB8DF1D9AB4756D73A614E902C")
		
		clt.send_message(req);
		
		log.info "${STEP}.6 Server收Get，带Cookie：" 
		o=svr.recv_message();
		assertTrue(o!=null&&o.getRequestLine().getMethod()=="GET");
		
		log.info "${STEP}.7 Server 重发200，带Cookie："
		svr.send_message(response);
		
		log.info "${STEP}.8 client再次收200："
		o= clt.recv_message();		
		assertTrue(o.getStatusLine().getStatusCode()==200);
				
		log.info "${STEP}.9 client 发Post:"
		req = clt.create_request(
			["pkttype":"POST",
			 "uri":"bpss/index.jsp",
			 "Referer":uri+"bpss/login.jsp?wlanacname=0116.0755.200.00&wlanuserip=172.24.117.147&ssid=CMCC&wlanacip=221.179.21.230",
			 "Cache-Control":"no-cache",
			 "Cookie":"JSESSIONID=7EB114CB8DF1D9AB4756D73A614E902C",
			 "User-Agent":"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; SE 2.X MetaSr 1.0)",
			 "Content-Type":"application/x-www-form-urlencoded",
			 "content":"wlanuserip=172.24.117.147&wlanacname=0116.0755.200.00&wlanacip=221.179.21.230"
			 ]
		)
		clt.send_message(req);
		
		log.info "${STEP}.10 Server收Post，带Cookie："
		o=svr.recv_message();
		assertTrue(o!=null&&o.getRequestLine().getMethod()=="POST");
		
		log.info "${STEP}.11 Server发送响应200，带cookie："
		response=svr.create_response(o,
			["status_code":"200",
			 "Server":"Apache-Coyote/1.1",
			 "Set-Cookie":"JSESSIONID=9BCE11A083A5363D6F587BEFEC7DEF02; Path=/bpss",
			 "Pragma":"No-cache",
			 "Cache-Control":"no-cache",
			 "Expires":"Thu, 01 Jan 1970 00:00:00 GMT",
			 "Content-Type":"text/html;charset=GBK",
			 "Date":"Thu, 15 Nov 2012 06:21:30 GMT"
			 
		]);
		svr.send_message(response);
		
		log.info "${STEP}.12 client再次收200："
		o= clt.recv_message();		
		assertTrue(o.getStatusLine().getStatusCode()==200);
	}
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		log.debug("after Method " + m);
		super.shutdown_all_ua();

		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}

}