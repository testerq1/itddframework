package ate.ua.http;

/*
 * #%L
 * ate-ua-http
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;

import ate.util.X2X0;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class HttpSim implements HttpHandler{
	private String ip;
	private int port;
	private HttpServer hs;
	private String context="/";
	private String resp="Happy Day!";
	private int resp_code=200;
	
	public HttpSim(String ip,int port){
		this.ip=ip;
		this.port=port;
	}
	
	public void listen() 
	{
		if(hs!=null)
			hs.stop(0);
		try {
			hs= HttpServer.create(new InetSocketAddress(ip,port),0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		hs.createContext(context, this);
		
		hs.setExecutor(null); // creates a default executor
		hs.start();
	}
	
	public void handle(HttpExchange t) throws IOException {
		InputStream is = t.getRequestBody();
		String from=t.getRemoteAddress().toString();
		
		String response = 		
"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"+
"<head>\n"+
"<title>"+X2X0.now()+" from:"+from+"|To:"+t.getLocalAddress()+"</title>\n"+
"<h3>"+resp+"</h3>";
		
		Headers headers = t.getResponseHeaders();
		//headers.add("Content-Type", "application/atom+xml");
		
		
		headers.add("Content-Type","text/html; charset=utf-8");
		
		t.sendResponseHeaders(resp_code, response.getBytes().length);
		
		OutputStream os = t.getResponseBody();
		os.write(response.getBytes());
		os.close();
	}
	
	public void setResponse(String resp,int code){
		this.resp=resp;
		this.resp_code=code;
	}
	
	public void stop(){
		hs.stop(0);
	}
	
	public static void main(String[] args) throws Exception{
		new HttpSim("172.16.5.112", 80).listen();
		System.out.println("asddas");
	}	
}
