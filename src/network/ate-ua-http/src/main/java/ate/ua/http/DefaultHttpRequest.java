package ate.ua.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Map;

import org.apache.mina.http.HttpRequestImpl;
import org.apache.mina.http.api.HttpMethod;
import org.apache.mina.http.api.HttpVersion;

import ate.rm.dev.Host;

public class DefaultHttpRequest extends HttpRequestImpl{
    HttpConfig config;
    public DefaultHttpRequest(HttpConfig config, HttpMethod method,
            String requestedPath, String queryString,
            Map<String, String> headers) {
        super(config.version, method, requestedPath, queryString, headers);
        this.config=config;
        this.getHeaders().put("User-Agent", config.UserAgent);
        getHeaders().put("Connection", "Keep-Alive");        
    }
    
    public DefaultHttpRequest(HttpVersion version, HttpMethod method,
            String requestedPath, String queryString,
            Map<String, String> headers) {
        super(version, method, requestedPath, queryString, headers);
    }
    ByteArrayOutputStream body;
    
    public DefaultHttpRequest set_content(byte[] bs){
        if(body==null)
            body=new ByteArrayOutputStream();
        try {
            body.write(bs);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.getHeaders().put("Content-Length", String.valueOf(body.size()));
        
        return this;
    }
    
    public byte[] get_content(){
        if(body==null)
            return null;
        return body.toByteArray();
    }
    public DefaultHttpRequest get_packet(){
        if(body!=null)
            this.getHeaders().put("Content-Length", String.valueOf(body.size()));
        return this;
    }
    public DefaultHttpRequest append(byte[] bs){
        return set_content(bs);
    }
    
    @Override
    public String toString() {
        if(config==null)
            return  toString(Host.charset);
        return toString(config.charset);
    }
    
    public String toString(Charset charset) {
        String result = super.toString();
        
        if(body!=null)
            result+=new String(body.toByteArray(),charset);
        return result;
    }
}
