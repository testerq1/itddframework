package ate.ua.http;

import java.nio.ByteBuffer;
import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.http.HttpClientEncoder;
import org.apache.mina.http.HttpServerEncoder;
import org.apache.mina.http.api.HttpMessage;
import org.apache.mina.http.api.HttpMethod;
import org.apache.mina.http.api.HttpStatus;
import org.apache.mina.http.api.HttpVersion;

import ate.AbstractURIHandler;
import ate.ua.IAutoResponseMessage;
import ate.ua.UAOption;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;
import ate.util.X2X0;

/**
 * 对chunked编码支持的不好
 * @author Administrator
 *
 */
public class HttpMinaUA extends AMinaUAImp<HttpMessage> implements
        IAutoResponseMessage {
    HttpConfig config=new HttpConfig();
    
    @Override
    public AbstractURIHandler build(UAOption option, Object value) {
        super.build(option, value);

        if (option != UA_TYPE)
            return this;

        this.transport = "tcp";

        if (this.port <= 0)
            port = 80;
        
        return this;
    }
    
    @Override
    public void add_default_filter() {
        if (this.type == SERVER)
            add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(
                    new HttpServerEncoder(), new HttpServerDecoder()));
        else
            add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(
                    new HttpClientEncoder(), new HttpClientDecoder()));
    }

    @Override
    public boolean message_matched(HttpMessage msg, HashMap<String, Object> hm) {
        return true;
    }


    @Override
    public String get_default_schema() {
        return "mhttp";
    }
    /**
     * hm=["pkttype":"Get","uri":"http://www.baidu.com"] create_request(hm)
     * 不支持2个相同的Header
     * 
     * @param hm
     *            the hm
     * @return the http request
     */
    public DefaultHttpRequest create_request(HashMap<String, String> hm) {
        HashMap<String, String> tmp = (HashMap<String, String>) hm.clone();
        try {
            String content = tmp.remove("content");
            DefaultHttpRequest req = new DefaultHttpRequest(config,
                    HttpMethod.valueOf(tmp.remove("pkttype").toUpperCase()),
                    tmp.remove("uri"),                    
                    "",
                    tmp);
            
            if (content != null) {
                req.append(content.getBytes(charset));
            }
           
            return req;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Create_response. hm keys: {status_code,content,{headers}}
     * 
     * @param msg
     *            the msg
     * @param hm
     *            the hm
     * @return the http packet
     */
    public DefaultHttpResponse create_response(Object msg, HashMap<String, String> hm) {
        HashMap<String, String> tmp = (HashMap<String, String>) hm.clone();

        String code = tmp.remove("status");
        if (code == null)
            code = "SUCCESS_OK";

        DefaultHttpResponse resp = new DefaultHttpResponse(
                config,
                HttpStatus.valueOf(code),
                tmp);
        String content = tmp.remove("content");
        if (content != null) {
            resp.set_content(content.getBytes());
        }
        return resp;
    }
    protected HashMap<String, String> autoRespAttr = null;
    
    /*
     * 如果content为null,会有一个默认值 <html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
     * "<head>\n" + "<title>" + X2X0.now() + " from:" + req.getRemoteAddress() +
     * "|To:" + req.getLocalAddress() + "</title>\n" + "<h3>" + "hello" +
     * "</h3>"
     * 
     * @see
     * ate.ua.IPacketUA#get_auto_resp_msg(org.apache.mina.core.session.IoSession
     * , java.lang.Object)
     */
    public Object get_auto_resp_msg(Object ctx, Object req) {
        IoSession session = (IoSession) ctx;
        if (autoRespAttr == null)
            return null;

        DefaultHttpResponse resp = this.create_response(req, this.autoRespAttr);
        String content = this.autoRespAttr.get("content");
        if (content == null) {
            content = "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
                    + "<head>\n" + "<title>" + X2X0.now() + " from:"
                    + session.getRemoteAddress() + "|To:"
                    + session.getLocalAddress() + "</title>\n" + "<h3>"
                    + "hello" + "</h3>";
            resp.append(content.getBytes(charset));
        }
        return resp;
    }
    @Override
    public void send_message(HttpMessage msg) {        
        byte[] content=null;
        if(msg instanceof DefaultHttpResponse){
            msg=((DefaultHttpResponse)msg).get_packet();
            content=((DefaultHttpResponse)msg).get_content();            
        }else if(msg instanceof DefaultHttpRequest){
            msg=((DefaultHttpRequest)msg).get_packet();
            content=((DefaultHttpRequest)msg).get_content();
        }else
            log.error("{} send_message {}",this,msg);
        
        super.send_message(msg);
        if(content!=null)
            current_session.write(IoBuffer.wrap(content));
    }
    /**
     * 预设值一组自动响应的参数,如果attrs为null，则不会自动响应
     * 
     * @param attrs
     */
    public void set_auto_response(HashMap<String, String> attrs) {
        this.autoRespAttr = attrs;        
    }
}
