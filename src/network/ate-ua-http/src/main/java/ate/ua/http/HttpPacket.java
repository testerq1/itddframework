/*
 * 
 */
package ate.ua.http;

/*
 * #%L
 * ate-ua-http
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.List;
import com.sun.net.httpserver.HttpExchange;


/**
 * The Class HttpPacket.
 * HttpClientUA会用到
 */
public class HttpPacket {
	
	/** The method. */
	public String method;
	
	/** The status_code. */
	public int status_code=-1;
	
	/** The headers. */
	public List headers;
	
	/** The body. */
	public String body;
	
	/** The exchange. */
	public HttpExchange exchange;
	
	/**
	 * Instantiates a new http packet.
	 *
	 * @param method the method
	 */
	HttpPacket(String method){this.method=method;}
	
	/**
	 * Instantiates a new http packet.
	 *
	 * @param scode the scode
	 */
	HttpPacket(int scode){this.status_code=scode;}
}
