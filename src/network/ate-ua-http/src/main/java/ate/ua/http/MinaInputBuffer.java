package ate.ua.http;

/*
 * #%L
 * ate-ua-http
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;

import org.apache.http.impl.io.AbstractSessionInputBuffer;
import org.apache.http.params.HttpParams;
import org.apache.mina.core.buffer.IoBuffer;

public class MinaInputBuffer extends AbstractSessionInputBuffer {
	
	MinaInputBuffer(IoBuffer io, HttpParams params){
		int size=io.limit();
		io.flip();		
		this.init(io.asInputStream(), size, params);
	}
    
	@Override
	public boolean isDataAvailable(int timeout) throws IOException {

		return hasBufferedData();
	}

}
