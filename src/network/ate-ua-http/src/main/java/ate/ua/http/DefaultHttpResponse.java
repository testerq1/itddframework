package ate.ua.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.mina.http.api.HttpResponse;
import org.apache.mina.http.api.HttpStatus;
import org.apache.mina.http.api.HttpVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.AbstractURIHandler;
import ate.Global;
import ate.rm.dev.Host;

public class DefaultHttpResponse implements HttpResponse {
    protected static Logger log = LoggerFactory.getLogger(DefaultHttpResponse.class);
    private final HttpVersion version;

    private final HttpStatus status;

    private final Map<String, String> headers;
    
    private ByteArrayOutputStream body;
    
    HttpConfig config;
    public DefaultHttpResponse(HttpConfig config, HttpStatus status, Map<String, String> headers) {
        this.config=config;
        this.version = config.version;
        this.status = status;
        this.headers = headers;
    }
    public DefaultHttpResponse(HttpVersion version, HttpStatus status, Map<String, String> headers) {
        this.version = version;
        this.status = status;
        this.headers = headers;
    }
    public HttpVersion getProtocolVersion() {
        return version;
    }

    public String getContentType() {
        return headers.get("content-type");
    }

    public boolean isKeepAlive() {
        // TODO check header and version for keep alive
        return false;
    }

    public String getHeader(String name) {
        return headers.get(name);
    }

    public boolean containsHeader(String name) {
        return headers.containsKey(name);
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public HttpStatus getStatus() {
        return status;
    }
    
    public DefaultHttpResponse set_content(byte[] bs){
        if(body==null)
            body=new ByteArrayOutputStream();
        try {
            body.write(bs);
        } catch (IOException e) {
            e.printStackTrace();
        }        
        return this;
    }
    
    public DefaultHttpResponse get_packet(){
        if(body!=null)
        this.headers.put("Content-Length", String.valueOf(body.size()));
        return this;
    }
    
    public byte[] get_content(){
        if(body==null)
            return null;
        return body.toByteArray();
    }
    
    public DefaultHttpResponse append(byte[] bs){
        if(Global.verbose){
            log.debug("append BODY: {} bytes {}", bs.length,new String(bs));
        }
        return set_content(bs);
    }
        
    public int status_code(){
        return this.getStatus().code();
    }
    
    @Override
    public String toString() {
        if(config==null)
            return  toString(Host.charset);
        return toString(config.charset);
    }
    public String toString(Charset charset) {
        String result = "HTTP RESPONSE STATUS: " + status + "\n";
        result += "VERSION: " + version + "\n";

        result += "--- HEADER --- \n";
        for (String key : headers.keySet()) {
            String value = headers.get(key);
            result += key + ":" + value + "\n";
        }
        if(body!=null)
            result+=new String(body.toByteArray(),charset);
        return result;
    }
}