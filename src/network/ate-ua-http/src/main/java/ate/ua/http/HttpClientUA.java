/*
 * 
 */
package ate.ua.http;

/*
 * #%L
 * ate-ua-http
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.mina.core.session.IoSession;

import ate.AbstractURIHandler;
import ate.ua.AbstractPacketUAImp;
import ate.ua.IAutoResponseMessage;
import ate.ua.UAOption;
import ate.util.X2X0;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

/**
 * The Class HttpClientUA.
 * 采用是apache的HttpClient包，发送请求时会建立一个线程
 *  
 * server模式采用的是JRE的默认httpserver,注释掉了，原因是不太稳定，根因不清楚，因此不支持server模式，
 * 将来可以考虑删掉server的代码，暂时保留备查
 * 
 * server采用sun jre自带的HttpServer，比较简单
 */
public class HttpClientUA extends AbstractPacketUAImp implements Runnable, HttpHandler,IAutoResponseMessage,
		ResponseHandler<String> {
	protected HashMap<String, String> autoRespAttr = null;
	
	private String context = "/";
	
	private HttpServer hs;
	/** The httpclient. */
	HttpClient httpclient;	
	
	/**
	 * Add_headers.
	 *
	 * @param msg the msg
	 * @param hm the hm
	 */
	public void add_headers(Object msg, HashMap<String, String> hm) {
		if (msg instanceof HttpRequestBase) {
			HttpRequestBase req = (HttpRequestBase) msg;
			for (String key : hm.keySet()) {
				req.addHeader(key, hm.get(key));
			}
		} else {
			HttpExchange exchange = (HttpExchange) msg;
			Headers headers = exchange.getResponseHeaders();
			for (String key : hm.keySet())
				headers.add(key, hm.get(key));
		}
	}

	/* (non-Javadoc)
	 * @see ate.ua.AUAImp#create_ua(int, java.lang.String)
	 */
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		
		if (option!=UA_TYPE)
			return this;
		
		if(type == CLIENT) {			
			httpclient = HttpClientBuilder.create().build();
			
		} else {
			throw new Error("不支持server模式");
//			try {
//				hs = HttpServer.create(new InetSocketAddress(host, port), 0);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			hs.createContext(context, this);
//			hs.setExecutor(null); // creates a default executor
			
		}		
		return this;
	}

	/**
	 * hm=["pkttype":"Get","uri":"http://www.baidu.com"] create_request(hm)
	 * 不支持2个相同的Header
	 * @param hm the hm
	 * @return the http request base
	 */
	public HttpRequestBase create_request(HashMap<String, String> hm) {
		HashMap<String, String> tmp=(HashMap<String, String>)hm.clone();
		try {
			HttpRequestBase msg = (HttpRequestBase) Class.forName(
					"org.apache.http.client.methods.Http" + tmp.remove("pkttype"))
					.newInstance();
			msg.setURI(URI.create(tmp.remove("uri")));
			
			for(String key:tmp.keySet()){
				if(msg.getHeaders(key)!=null)
					msg.setHeader(key, tmp.get(key));
				else
					msg.addHeader(key,tmp.get(key));
			}
			((HttpPost)msg).setEntity(null);
			
			return msg;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Create_response.
	 * hm keys: {status_code,content,{headers}}
	 * 如果content为null,会有一个默认值
	 * "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
	 *				+ "<head>\n" + "<title>" + X2X0.now() + " from:"
	 *				+ req.getRemoteAddress() + "|To:" + req.getLocalAddress()
	 *				+ "</title>\n" + "<h3>" + "hello" + "</h3>"
	 * 
	 * @param msg the msg
	 * @param hm the hm
	 * @return the http packet
	 */
	public HttpPacket create_response(Object msg,HashMap<String, String> hm) {
		HashMap<String, String> tmp=(HashMap<String, String>)hm.clone();
		
		HttpExchange req=((HttpPacket)msg).exchange;
		Headers headers = req.getResponseHeaders();
		String response = tmp.remove("content");
		
		int status_code = 200;
		String sc=tmp.remove("status_code");
		
		if ( sc != null)			
			status_code = Integer.parseInt(sc);
		
		for (String key : tmp.keySet()) {			
			headers.add(key, hm.get(key));			
		}
		if (response == null)
			response = "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
					+ "<head>\n" + "<title>" + X2X0.now() + " from:"
					+ req.getRemoteAddress() + "|To:" + req.getLocalAddress()
					+ "</title>\n" + "<h3>" + "hello" + "</h3>";
		
		try {
			req.sendResponseHeaders(status_code, response.getBytes().length);
		} catch (Exception e) {
			e.printStackTrace();
		}
		HttpPacket resppkt=new HttpPacket(status_code);
		resppkt.exchange=req;
		resppkt.body=response;
		return resppkt;
	}

	/* (non-Javadoc)
	 * @see ate.ua.IPacketUA#get_auto_resp_msg(org.apache.mina.core.session.IoSession, java.lang.Object)
	 */
	public Object get_auto_resp_msg(Object ctx, Object req) {
		if (this.autoRespAttr == null)
			return null;

		if (req instanceof HttpExchange) {
			HttpExchange t = (HttpExchange) req;
			Headers headers = t.getResponseHeaders();
			String response = null;
			int status_code = -1;
			for (String key : autoRespAttr.keySet()) {
				if (key.equals("content"))
					response = autoRespAttr.get(key);
				else if (key.equals("status_code")) {
					status_code = Integer.parseInt(autoRespAttr.get(key));
				} else
					headers.add(key, autoRespAttr.get(key));
				// headers.add("Content-Type", "application/atom+xml");
				// headers.add("Content-Type", "text/html; charset=utf-8");
			}
			if (response == null)
				response = "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
						+ "<head>\n" + "<title>" + X2X0.now() + " from:"
						+ t.getRemoteAddress() + "|To:" + t.getLocalAddress()
						+ "</title>\n" + "<h3>" + "hello" + "</h3>";

			if (status_code == -1)
				status_code = 200;

			try {
				t.sendResponseHeaders(200, response.getBytes().length);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return response;
		}
		return null;
	}
	/**
	 * rest_xxx封装一组rest client api
	 * @param url
	 * @return
	 */
	public String rest_get(String url){		
		return get_response(new HttpGet(url));
	}
	
	public String rest_put(String url){		
		return get_response(new HttpPut(url));
	}
	
	public String rest_post(String url){		
		return get_response(new HttpPost(url));
	}
	
	public String rest_head(String url){		
		return get_response(new HttpHead(url));
	}
	public String rest_options(String url){		
		return get_response(new HttpOptions(url));
	}
	
	public String rest_delete(String url){		
		return get_response(new HttpDelete(url));
	}
	
	public String rest_patch(String url){		
		return get_response(new HttpPatch(url));
	}
	
	public String rest_trace(String url){		
		return get_response(new HttpTrace(url));
	}
	
	private String get_response(HttpUriRequest req){
		try {
			HttpResponse httpResponse = httpclient.execute(req);			
			return X2X0.InputStreamTOString(httpResponse.getEntity().getContent(),charset);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private String get_body(Object msg) {
		if (msg instanceof HttpResponse) {
			HttpResponse resp = (HttpResponse) msg;
			HttpEntity entity = resp.getEntity();
			if (entity == null)
				return null;
			try {
				return EntityUtils.toString(entity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (msg instanceof HttpExchange) {
			HttpExchange t = (HttpExchange) msg;
			InputStream is = t.getRequestBody();
			try {
				if (is.available() <= 0)
					return null;
				byte[] bs = new byte[is.available()];

				is.read(bs);
				return new String(bs, charset);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;

	}

	/* (non-Javadoc)
	 * @see ate.ua.AUAImp#getDefaultSchema()
	 */
	@Override
	public String get_default_schema() {
		return "httpc";
	}

	/* (non-Javadoc)
	 * @see com.sun.net.httpserver.HttpHandler#handle(com.sun.net.httpserver.HttpExchange)
	 */
	@Override
	public void handle(HttpExchange t) throws IOException {
		log.debug(this+ "receive message:");
		Object o = get_auto_resp_msg(null, t);
		if (o == null) {
			HttpPacket pkt = new HttpPacket(t.getRequestMethod());
			pkt.headers = Arrays.asList(t.getRequestHeaders().values());
			pkt.body = get_body(t);
			pkt.exchange=t;
			System.out.println(pkt.method);
			for(Object h:pkt.headers){
				log.debug("{}",h);
			}
			if(this.is_record)
				add_packet(pkt);
		} else {
			OutputStream os = t.getResponseBody();
			os.write(o.toString().getBytes());
			os.flush();
		}
		this.rcvd();
		// InputStream is = t.getRequestBody();
		// String from = t.getRemoteAddress().toString();
		//
		// String response = "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
		// + "<head>\n" + "<title>" + X2X0.now() + " from:" + from
		// + "|To:" + t.getLocalAddress() + "</title>\n" + "<h3>"
		// + "hello" + "</h3>";
		//
		// Headers headers = t.getResponseHeaders();
		// // headers.add("Content-Type", "application/atom+xml");
		//
		// headers.add("Content-Type", "text/html; charset=utf-8");
		//
		// t.sendResponseHeaders(200, response.getBytes().length);
		//
		// OutputStream os = t.getResponseBody();
		// os.write(response.getBytes());
		// os.close();
	}

	/* (non-Javadoc)
	 * @see org.apache.http.client.ResponseHandler#handleResponse(org.apache.http.HttpResponse)
	 */
	public String handleResponse(HttpResponse response) {
		log.debug(this+ "receive message:");
		HttpPacket pkt = new HttpPacket(response.getStatusLine()
				.getStatusCode());
		pkt.headers = Arrays.asList(response.getAllHeaders());
		pkt.body = get_body(response);
		add_packet(pkt);
		return "httpclient_ok";
	}

	/* (non-Javadoc)
	 * @see ate.ua.AUAImp#is_ready()
	 */
	@Override
	public boolean is_ready() {
		if (type == CLIENT)
			return httpclient != null;
		else
			return hs != null;
	}

	/* 
	 * hm支持：{method,status_code,body},采用字符串equal方式进行比对
	 * 
	 * @see ate.ua.IPacketUA#message_matched(java.lang.Object, java.util.HashMap)
	 */
	@Override	
	public boolean message_matched(Object msg, HashMap hm) {
		if (!(msg instanceof HttpPacket))
			return false;
		
		if (hm == null)
			return true;
		
		HttpPacket t = (HttpPacket) msg;
		if (type == SERVER) {
			if (t.method == null
					|| (hm.get("method") != null && !t.method
							.equalsIgnoreCase(hm.get("method").toString())))
				return false;
		} else {
			if (hm.get("status_code") != null
					&& !hm.get("status_code").equals(t.status_code + ""))
				return false;

			if (hm.get("body") != null
					&& (t.body == null || !t.body.equals(hm.get("body"))))
				return false;

		}
		return true;
	}

	/**
	 * Remove_header.
	 *
	 * @param msg the msg
	 * @param header the header
	 */
	public void remove_header(Object msg, String header) {
		if (msg instanceof HttpRequestBase) {
			HttpRequestBase req = (HttpRequestBase) msg;
			req.removeHeaders(header);
		} else {
			HttpExchange exchange = (HttpExchange) msg;
			Headers headers = exchange.getResponseHeaders();
			headers.remove(header);
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		if (type == SERVER)
			hs.start();		
	}
	
	/* (non-Javadoc)
	 * @see ate.ua.IPacketUA#send_message(java.lang.Object)
	 */
	@Override
	public void send_message(Object msg) {
		log.debug(this +" send message:");
		try {
			if (type == CLIENT) {
				final HttpClientUA ua=this;
				final HttpUriRequest req=(HttpUriRequest)msg;
				new Thread("httpclient execute"){
					public void run(){
						try {
							if (!"httpclient_ok".equalsIgnoreCase(httpclient.execute(
									req, ua)))
								throw new Error("http server no response");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}.start();
			} else {
				HttpPacket pkt=(HttpPacket)msg;
				HttpExchange exchange = pkt.exchange;
				OutputStream os = exchange.getResponseBody();
				os.write(pkt.body.getBytes());
				os.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see ate.ua.IPacketUA#start_ua()
	 */
	@Override
	public void startup() {
		if (type == SERVER)
			new Thread(this).start();
	}

	/* (non-Javadoc)
	 * @see ate.ua.IPacketUA#shutdown()
	 */
	@Override
	public void teardown() {
		if (hs != null) {
			hs.stop(0);
			hs = null;
		}
	}

}
