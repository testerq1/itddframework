package ate.ua.http;

/*
 * #%L
 * ate-ua-http
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.OutputStream;

import org.apache.http.impl.io.AbstractSessionOutputBuffer;
import org.apache.http.params.HttpParams;
import org.apache.mina.core.buffer.IoBuffer;

public class MinaOutputBuffer extends AbstractSessionOutputBuffer {
	MinaOutputBuffer(OutputStream output,int buffersize, HttpParams params){
		this.init(output, buffersize, params);
	}
}
