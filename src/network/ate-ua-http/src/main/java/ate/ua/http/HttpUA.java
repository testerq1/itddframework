/*
 * Http UA 使用 apache http core 解析 http
 */
package ate.ua.http;

/*
 * #%L
 * ate-ua-http
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.HashMap;

import org.apache.http.Header;
import org.apache.http.HeaderIterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpMessage;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestFactory;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseFactory;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.DefaultHttpClientConnection;
import org.apache.http.impl.DefaultHttpRequestFactory;
import org.apache.http.impl.DefaultHttpResponseFactory;
import org.apache.http.impl.entity.EntityDeserializer;
import org.apache.http.impl.entity.EntitySerializer;
import org.apache.http.impl.entity.LaxContentLengthStrategy;
import org.apache.http.impl.entity.StrictContentLengthStrategy;
import org.apache.http.impl.io.DefaultHttpRequestParser;
import org.apache.http.impl.io.DefaultHttpResponseParser;
import org.apache.http.io.HttpMessageParser;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.message.BasicLineFormatter;
import org.apache.http.message.BasicLineParser;
import org.apache.http.message.LineFormatter;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpProcessor;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.http.protocol.ImmutableHttpProcessor;
import org.apache.http.protocol.RequestConnControl;
import org.apache.http.protocol.RequestContent;
import org.apache.http.protocol.RequestExpectContinue;
import org.apache.http.protocol.RequestTargetHost;
import org.apache.http.protocol.RequestUserAgent;
import org.apache.http.protocol.ResponseConnControl;
import org.apache.http.protocol.ResponseContent;
import org.apache.http.protocol.ResponseServer;
import org.apache.http.util.CharArrayBuffer;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.apache.mina.http.HttpClientDecoder;

import ate.AbstractURIHandler;
import ate.ua.IAutoResponseMessage;
import ate.ua.UAOption;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;
import ate.util.X2X0;

/**
 * 
 * 简单的HttpUA.采用mina实现 可以代替HttpClientUA,两种实现方式，用mina做更容易合入现有mina框架里去
 * 本类的decode实现了http消息的Cumulative,可以供以后参考,不支持Http Chunked编码
 */
public class HttpUA extends AMinaUAImp<HttpMessage> implements
		IAutoResponseMessage {
    HttpMessageParser parser;
	/**
	 * The Class HttpDecoder.
	 */
	class HttpDecoder extends CumulativeProtocolDecoder {
		ByteBuffer content;
		private final EntityDeserializer entitydeserializer = new EntityDeserializer(
				new LaxContentLengthStrategy());
		HttpMessage message;
		
		
		protected boolean canResponseHaveBody(final HttpRequest request,
				final HttpResponse response) {
			if ("HEAD".equalsIgnoreCase(request.getRequestLine().getMethod())) {
				return false;
			}
			int status = response.getStatusLine().getStatusCode();
			return status >= HttpStatus.SC_OK
					&& status != HttpStatus.SC_NO_CONTENT
					&& status != HttpStatus.SC_NOT_MODIFIED
					&& status != HttpStatus.SC_RESET_CONTENT;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.apache.mina.filter.codec.CumulativeProtocolDecoder#doDecode(org
		 * .apache.mina.core.session.IoSession,
		 * org.apache.mina.core.buffer.IoBuffer,
		 * org.apache.mina.filter.codec.ProtocolDecoderOutput)
		 */
		@Override
		protected boolean doDecode(IoSession session, IoBuffer in,
				ProtocolDecoderOutput out) throws Exception {
			if (in.remaining() <= 0)
				return false;
			
			log.debug("io remain: " + in.remaining());
			log.debug("doDecode:" + in.getString(charset.newDecoder()));
			MinaInputBuffer buf = new MinaInputBuffer(in, params);
			if (content == null) {
				HttpMessageParser parser = null;

				if (is_client())
					parser = new DefaultHttpResponseParser(buf,
							BasicLineParser.DEFAULT, respfactory, params);
				else
					parser = new DefaultHttpRequestParser(buf,
							BasicLineParser.DEFAULT, reqfactory, params);

				message = parser.parse();
				int contentlen = 0;
				if (message.getHeaders("Content-Length").length > 0) {
					contentlen = Integer.parseInt(message
							.getHeaders("Content-Length")[0].getValue());
				}
				if (contentlen <= 0) {
					out.write(message);
					contentlen = 0;
					log.debug("message complete 1");
					return true;
				}
				content = ByteBuffer.allocate(contentlen);
				byte[] rem = new byte[buf.length()];
				buf.read(rem);
				content.put(rem);
			}
			byte[] rem = new byte[in.remaining()];
			buf.read(rem);
			content.put(rem);

			if (content.hasRemaining()) {
				log.debug("message not complete 1 " + content.remaining());
				return false;
			}

			BytesSessionInputBuffer sbuf = new BytesSessionInputBuffer(
					content.array(), params);
			HttpEntity entity = this.entitydeserializer.deserialize(sbuf,
					message);
			if (message instanceof HttpResponse)
				((HttpResponse) message).setEntity(entity);
			else
				((HttpEntityEnclosingRequest) message).setEntity(entity);

			out.write(message);
			content = null;
			log.debug("message complete 2");
			return true;
		}
	}

	/**
	 * The Class HttpEncoder.
	 */
	class HttpEncoder extends ProtocolEncoderAdapter {
		private final EntitySerializer entityserializer = new EntitySerializer(
				new StrictContentLengthStrategy());
		protected final CharArrayBuffer lineBuf = new CharArrayBuffer(128);

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.apache.mina.filter.codec.ProtocolEncoder#encode(org.apache.mina
		 * .core.session.IoSession, java.lang.Object,
		 * org.apache.mina.filter.codec.ProtocolEncoderOutput)
		 */
		@Override
		public void encode(IoSession session, Object message,
				ProtocolEncoderOutput out) throws Exception {
			HttpMessage msg = (HttpMessage) message;
			ByteArrayOutputStream bos = new ByteArrayOutputStream();

			MinaOutputBuffer buf = new MinaOutputBuffer(bos, 1024, params);
			HttpEntity entity = null;
			if (msg instanceof HttpRequest) {
				lineFormatter.formatRequestLine(lineBuf,
						((HttpRequest) msg).getRequestLine());
				if (msg instanceof HttpEntityEnclosingRequest)
					entity = ((HttpEntityEnclosingRequest) msg).getEntity();
			} else {
				lineFormatter.formatStatusLine(lineBuf,
						((HttpResponse) msg).getStatusLine());
				entity = ((HttpResponse) msg).getEntity();
			}
			buf.writeLine(lineBuf);
			for (HeaderIterator it = msg.headerIterator(); it.hasNext();) {
				Header header = it.nextHeader();
				buf.writeLine(lineFormatter.formatHeader(this.lineBuf, header));
			}
			lineBuf.clear();
			buf.writeLine(lineBuf);

			if (entity != null)
				this.entityserializer.serialize(buf, msg, entity);

			buf.flush();
			out.write(IoBuffer.wrap(bos.toByteArray()));

		}
	}

	public static HttpParams params;
	private static HttpRequestFactory reqfactory = new DefaultHttpRequestFactory();
	private static HttpResponseFactory respfactory = new DefaultHttpResponseFactory();
	protected HashMap<String, String> autoRespAttr = null;
	HttpContext context;
	HttpRequestExecutor httpexecutor = new HttpRequestExecutor();
	HttpProcessor httpproc;

	protected boolean isAutoResponse = false;
	protected final LineFormatter lineFormatter = BasicLineFormatter.DEFAULT;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ate.ua.AMinaUAImp#add_default_filter()
	 */
	@Override
	public void add_default_filter() {
		add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(
				new HttpEncoder(), new HttpDecoder()));
//	      if(this.type==SERVER)
//	            add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(
//	                new HttpServerEncoder(), new HttpServerDecoder()));
//	      else{
//	            add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(
//	                    new HttpClientEncoder(), new HttpClientDecoder()));
//	      
      
          
	}

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option != UA_TYPE)
			return this;

		this.transport = "tcp";

		if (this.port <= 0)
			port = 80;
		params = new SyncBasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params, this.charset.name());
		HttpProtocolParams.setUserAgent(params, "ATE/1.1");
		HttpProtocolParams.setUseExpectContinue(params, true);

		httpproc = new ImmutableHttpProcessor(new HttpRequestInterceptor[] {
				new RequestContent(), new RequestTargetHost(),
				new RequestConnControl(), new RequestUserAgent(),
				new RequestExpectContinue() }, new HttpResponseInterceptor[] {
				new ResponseServer(), new ResponseContent(),
				new ResponseConnControl() });
		context = new BasicHttpContext(null);
		HttpHost host = new HttpHost(this.host, port);

		DefaultHttpClientConnection conn = new DefaultHttpClientConnection();
		context.setAttribute(ExecutionContext.HTTP_CONNECTION, conn);
		context.setAttribute(ExecutionContext.HTTP_TARGET_HOST, host);
		return this;
	}

	/**
	 * hm=["pkttype":"Get","uri":"http://www.baidu.com"] create_request(hm)
	 * 不支持2个相同的Header
	 * 
	 * @param hm
	 *            the hm
	 * @return the http request
	 */
	public HttpMessage create_request(HashMap<String, String> hm) {
		HashMap<String, String> tmp = (HashMap<String, String>) hm.clone();
		try {
			HttpRequest req = reqfactory.newHttpRequest(tmp.remove("pkttype")
					.toUpperCase(), tmp.remove("uri"));
			String content = tmp.remove("content");
			if (content != null) {
				if (!(req instanceof BasicHttpEntityEnclosingRequest))
					throw new Error("Get不能带content");
				else {
					try {
						((BasicHttpEntityEnclosingRequest) req)
								.setEntity(new StringEntity(content));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
						return null;
					}
				}
			}

			for (String key : tmp.keySet()) {
				if (req.getHeaders(key) != null)
					req.setHeader(key, tmp.get(key));
				else
					req.addHeader(key, tmp.get(key));
			}
			req.setParams(params);
			httpexecutor.preProcess(req, httpproc, context);
			return req;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Create_response. hm keys: {status_code,content,{headers}}
	 * 
	 * @param msg
	 *            the msg
	 * @param hm
	 *            the hm
	 * @return the http packet
	 */
	public HttpResponse create_response(Object msg, HashMap<String, String> hm) {
		HashMap<String, String> tmp = (HashMap<String, String>) hm.clone();

		// context决定local,null为默认
		int code = 200;
		String code0 = tmp.remove("status_code");
		if (code0 != null)
			code = Integer.parseInt(code0);

		HttpResponse resp = respfactory.newHttpResponse(HttpVersion.HTTP_1_1,
				code, context);
		String content = tmp.remove("content");
		if (content != null) {
			try {
				resp.setEntity(new StringEntity(content));
				for (String key : tmp.keySet()) {
					if (resp.getHeaders(key) != null)
						resp.setHeader(key, tmp.get(key));
					else
						resp.addHeader(key, tmp.get(key));
				}
				resp.setParams(params);
				httpproc.process(resp, context);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return resp;
	}

	/*
	 * 如果content为null,会有一个默认值 <html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
	 * "<head>\n" + "<title>" + X2X0.now() + " from:" + req.getRemoteAddress() +
	 * "|To:" + req.getLocalAddress() + "</title>\n" + "<h3>" + "hello" +
	 * "</h3>"
	 * 
	 * @see
	 * ate.ua.IPacketUA#get_auto_resp_msg(org.apache.mina.core.session.IoSession
	 * , java.lang.Object)
	 */
	public Object get_auto_resp_msg(Object ctx, Object req) {
		IoSession session = (IoSession) ctx;
		if (!this.isAutoResponse || autoRespAttr == null)
			return null;

		HttpResponse resp = this.create_response(req, this.autoRespAttr);
		String content = this.autoRespAttr.get("content");
		if (content == null) {
			content = "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
					+ "<head>\n" + "<title>" + X2X0.now() + " from:"
					+ session.getRemoteAddress() + "|To:"
					+ session.getLocalAddress() + "</title>\n" + "<h3>"
					+ "hello" + "</h3>";
			try {
				resp.setEntity(new StringEntity(content));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				return null;
			}
		}
		return resp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ate.ua.AUAImp#getDefaultSchema()
	 */
	@Override
	public String get_default_schema() {
		return "http";
	}

	/*
	 * 支持的属性：{method,code,version,[headers...]}
	 * 
	 * @see ate.ua.IPacketUA#message_matched(java.lang.Object,
	 * java.util.HashMap)
	 */
	@Override
	public boolean message_matched(HttpMessage msg, HashMap<String, Object> hm) {
		if (hm == null)
			return true;
		HashMap<String, Object> hm0 = (HashMap<String, Object>) hm.clone();

		Object m = hm0.remove("method");

		if (m != null) {
			if (!(msg instanceof HttpRequest))
				return false;
			if (!((HttpRequest) msg).getRequestLine().getMethod()
					.equalsIgnoreCase(m.toString()))
				return false;
		} else {
			m = hm0.remove("code");
			if (m != null) {
				if (!(msg instanceof HttpResponse)
						|| m.toString().equalsIgnoreCase(
								((HttpResponse) msg).getStatusLine()
										.getStatusCode() + ""))
					return false;
				m = hm0.remove("version");
				if (m != null
						&& !((HttpResponse) msg).getProtocolVersion()
								.toString().contains(m.toString()))
					return false;
			}
		}

		for (String key : hm.keySet()) {
			if (!msg.containsHeader(key))
				return false;

			Header[] hs = msg.getHeaders(key);
			boolean ismatch = false;
			String v = hm0.remove(key).toString();
			for (int i = 0; i < hs.length; i++) {
				if (hs[i].getValue().equals(hm.remove(v))) {
					ismatch = true;
					break;
				}
			}
			if (!ismatch)
				return false;
		}
		return true;
	}

	/**
	 * 预设值一组自动响应的参数,如果attrs为null，则不会自动响应
	 * 
	 * @param attrs
	 */
	public void set_auto_response(HashMap<String, String> attrs) {
		this.autoRespAttr = attrs;
		if (autoRespAttr == null)
			isAutoResponse = false;
		else
			isAutoResponse = true;
	}
}
