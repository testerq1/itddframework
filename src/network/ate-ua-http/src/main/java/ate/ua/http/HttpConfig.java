package ate.ua.http;

import java.nio.charset.Charset;

import org.apache.mina.http.api.*;

import ate.rm.dev.Host;

public class HttpConfig {
    public HttpVersion version=HttpVersion.HTTP_1_1;
    public Charset charset=Host.charset;
    public String UserAgent="ATE/1.1";
}
