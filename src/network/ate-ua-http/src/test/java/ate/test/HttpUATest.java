package ate.test;

/*
 * #%L
 * ate-ua-http
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.lang.reflect.Method;

import org.apache.http.HttpMessage;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.log4j.Level;
import org.apache.mina.http.api.HttpStatus;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;

import ate.Global;
import ate.testcase.CsUATestcase;
import ate.ua.http.*;

import java.util.*;

public class HttpUATest extends CsUATestcase {
	
	@Test
	public void http_client2() {
	    Global.verbose=true;
	    HttpMinaUA clt = this.create_client("http://www.baidu.com/",new HttpMinaUA());
        start_all_ua();

        // assertTrue(svr.is_ready());
        assertTrue(clt.is_ready());
        DefaultHttpRequest req = clt.create_request(new HashMap<String, String>() {
            {
                put("pkttype", "Get");
                put("uri", "/");
                put("Host", "www.baidu.com:80");
            }
        });

        // clt.set_timeout(100000);
        clt.send_message(req);
        clt.set_timeout(100);
        Object o = clt.recv_message();

        DefaultHttpResponse resp = (DefaultHttpResponse) o;
        int sc = resp.status_code();
        assertTrue(sc == 200 || sc == 300|| sc == 302,sc);
    }
	public void captive_portal_02() {
	    HttpMinaUA svr = create_server("http://"+siip,new HttpMinaUA());
	    HttpMinaUA clt = create_client("http://"+sip,new HttpMinaUA());
        start_all_ua();

        assertTrue(svr.is_ready());
        assertTrue(clt.is_ready());
        STEP("client 发送OPTIONS，不带Cookie：");
        DefaultHttpRequest req = clt.create_request(new HashMap<String, String>() {
            {
                put("pkttype", "OPTIONS");
                put("uri",
                        "bpss/login.jsp?wlanacname=0116.0755.200.00&wlanuserip=172.24.117.147&ssid=CMCC&wlanacip=221.179.21.230");
                put("Host", "crl:verisign.com");
                put("User-Agent", "Microsoft-CryptoAPI/5.131.2600.5512");
                put("Cache-Control", "no-cache");
            }
        });

        clt.send_message(req);

        STEP("Server收到Get：");
        Object o = svr.recv_message();

        STEP("Server发送响应200，带cookie：");
        DefaultHttpResponse response = svr.create_response(o,
                new HashMap<String, String>() {
                    {
                        put("status_code", "200");
                        put("Server", "Apache-Coyote/1.1");
                        put("Set-Cookie",
                                "JSESSIONID=9BCE11A083A5363D6F587BEFEC7DEF02; Path=/bpss");
                        put("Pragma", "No-cache");
                        put("Cache-Control", "no-cache");
                        put("Expires", "Thu, 01 Jan 1970 00:00:00 GMT");
                        put("Content-Type", "text/html;charset=GBK");
                        put("Date", "Thu, 15 Nov 2012 06:21:30 GMT");
                        put("content", "<HTML></HTML>");
                    }
                });
        svr.send_message(response);

        STEP("client 收200：");
        o = clt.recv_message();
        DefaultHttpResponse resp = (DefaultHttpResponse) o;
        assertTrue(o!=null&&resp.status_code()==200,"response code不正确");
    }
	
	public void captive_portal_01() {
		HttpUA svr = create_server("http://"+siip);
		HttpUA clt = create_client("http://"+sip);
		start_all_ua();

		assertTrue(svr.is_ready());
		assertTrue(clt.is_ready());
		STEP("client 发送OPTIONS，不带Cookie：");
		HttpMessage req = clt.create_request(new HashMap<String, String>() {
			{
				put("pkttype", "OPTIONS");
				put("uri",
						"bpss/login.jsp?wlanacname=0116.0755.200.00&wlanuserip=172.24.117.147&ssid=CMCC&wlanacip=221.179.21.230");
				put("Host", "crl:verisign.com");
				put("User-Agent", "Microsoft-CryptoAPI/5.131.2600.5512");
				put("Cache-Control", "no-cache");
			}
		});

		clt.send_message(req);

		STEP("Server收到Get：");
		HttpMessage o = svr.recv_message();

		STEP("Server发送响应200，带cookie：");
		HttpMessage response = svr.create_response(o,
				new HashMap<String, String>() {
					{
						put("status_code", "200");
						put("Server", "Apache-Coyote/1.1");
						put("Set-Cookie",
								"JSESSIONID=9BCE11A083A5363D6F587BEFEC7DEF02; Path=/bpss");
						put("Pragma", "No-cache");
						put("Cache-Control", "no-cache");
						put("Expires", "Thu, 01 Jan 1970 00:00:00 GMT");
						put("Content-Type", "text/html;charset=GBK");
						put("Date", "Thu, 15 Nov 2012 06:21:30 GMT");
						put("content", "<HTML></HTML>");
					}
				});
		svr.send_message(response);

		STEP("client 收200：");
		o = clt.recv_message();
		HttpResponse resp = (HttpResponse) o;
		assertTrue(resp.getStatusLine().getStatusCode() == 200,"response code不正确");
	}
	
	// @Test
	public void rest() {
		HttpClientUA clt = (HttpClientUA) create_client("httpc:local");
		log.info(clt.rest_put("http://172.16.5.118:3000/"));

	}

	//@Test
	public void http_client() {
		HttpUA clt = (HttpUA) this.create_client("http://www.baidu.com/");
		start_all_ua();

		// assertTrue(svr.is_ready());
		assertTrue(clt.is_ready());
		HttpMessage req = clt.create_request(new HashMap<String, String>() {
			{
				put("pkttype", "Get");
				put("uri", "/");
			}
		});

		// clt.set_timeout(100000);
		clt.send_message(req);

		Object o = clt.recv_message();

		assertTrue(o instanceof HttpResponse);

		HttpResponse resp = (HttpResponse) o;
		int sc = resp.getStatusLine().getStatusCode();
		assertTrue(sc == 200 || sc == 300);

		log.info("headers count: " + resp.getAllHeaders().length);
		log.info("content length: " + resp.getEntity());

	}

	String localuri = "http://127.0.0.1:80/";
	int cnt = 0;

	//@Test
	public void auto_response() {
		HttpUA svr = (HttpUA) this.create_server(localuri, new HttpUA() {
			public Object get_auto_resp_msg(Object session, Object req) {
				cnt++;
				return create_response(req, new HashMap() {
					{
						put("status_code", "200");
						put("content", "<html><title>test" + cnt
								+ "</title></html>");
					}
				});
			}
		});
		HttpUA clt = (HttpUA) this.create_client("http://localhost/");
		start_all_ua();

		HttpMessage o = clt.create_request(new HashMap<String, String>() {
			{
				put("pkttype", "GET");
				put("uri", "/");
				put("Host", "ping.ie.sogou.com");
				put("Cache-Control", "no-cache");
			}
		});
		clt.send_message(o);

		o = clt.recv_message();
		assertTrue(o instanceof HttpResponse);

		HttpResponse resp = (HttpResponse) o;

		assertTrue(resp.getStatusLine().getStatusCode() == 200);

		log.info("headers count: " + resp.getAllHeaders().length);
	}

	//@Test
	public void http_server() {
		HttpUA svr = (HttpUA) this.create_server(localuri);
		HttpUA clt = (HttpUA) this.create_client("http://localhost/");
		start_all_ua();

		assertTrue(svr.is_ready());
		assertTrue(clt.is_ready());

		HttpMessage o = clt.create_request(new HashMap<String, String>() {
			{
				put("pkttype", "GET");
				put("uri", "/");
				put("Host", "ping.ie.sogou.com");
				put("Cache-Control", "no-cache");
			}
		});
		clt.send_message(o);

		HttpRequest req = (HttpRequest) svr.recv_message();

		assertTrue(req.getRequestLine().getMethod().equals("GET"));

		HttpMessage response = svr.create_response(o,
				new HashMap<String, String>() {
					{
						put("status_code", "300");
						put("Host", "ping.ie.sogou.com");
						put("content", "no-cache");
					}
				});
		svr.send_message(response);

		o = clt.recv_message();
		assertTrue(o instanceof HttpResponse);

		HttpResponse resp = (HttpResponse) o;

		assertTrue(resp.getStatusLine().getStatusCode() == 300,"response code不正确");

		log.info("headers count: " + resp.getAllHeaders().length);
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		log.debug("after Method " + m);
		super.shutdown_all_ua();

		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}

}
