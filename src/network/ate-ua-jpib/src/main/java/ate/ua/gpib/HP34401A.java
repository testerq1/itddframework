package ate.ua.gpib;

import be.ac.ulb.gpib.*;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Enumeration;

class HP34401A {
    private GPIBDeviceIdentifier deviceIdentifier;
    private Enumeration devicesList;
    private GPIBDevice myDevice;

    // Enumerates devices on the bus and take control of device #22
    HP34401A() throws IOException {
        super();

        devicesList = GPIBDeviceIdentifier.getDevices();
        while (devicesList.hasMoreElements()) {
            deviceIdentifier = (GPIBDeviceIdentifier) devicesList.nextElement();
            if (deviceIdentifier.getAddress() == 22){
                deviceIdentifier.open();
                myDevice=deviceIdentifier;
            }
        }
    }

    // Takes a VAC measure on a basis of a 100Volts caliber and 1Volt precision
    public float getVoltage() throws RemoteException {
        float value = 0;
        try {
            value = (float) Float.parseFloat(myDevice
                    .sendCommand("MEAS:VOLT:AC? 100,1"));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RemoteException(
                    "In Proxy's HP34401A getVDC(): IOException\n"
                            + e.getMessage());
        }
        return value;
    }

    public void clear() throws RemoteException {
        try {
            myDevice.clear();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RemoteException(
                    "In Proxy's HP34401A Reset(): IOException\n"
                            + e.getMessage());
        }
    }

}
