/*
 * A GPIB driver based on NI (www.ni.com) API.
 *  Copyright (C) 2001-2003 Jean-Michel DRICOT (jdricot@ulb.ac.be)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

package be.ac.ulb.gpib;

import java.io.IOException;
import java.util.ArrayList;

/**
 * The Class LinuxGPIBDriver.
 *
 * @author Jean-Michel DRICOT
 * 
 * extended by Ralf Tralow
 */
public class LinuxGPIBDriver implements GPIBDriver
{

	/** The gpbi error. */
	private String gpbiError = ""; //$NON-NLS-1$

	/** The listener list. */
	private ArrayList<GPIBDriverListener> listenerList = new ArrayList<GPIBDriverListener>();
	
	/*
	 * initialize() will be called by the CommPortIdentifier's static initializer. The responsibility of this method is: 1) Ensure that that the hardware is
	 * present. 2) Load any required native libraries. 3) Register the port names with the CommPortIdentifier.
	 * 
	 * @author Jean-Michel DRICOT
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#initialize(boolean)
	 */
	public boolean initialize(boolean getIDN)
	{
		scanDevices();
		return true;
	}

	/**
	 * Scan devices.
	 */
	private native void scanDevices();

	/**
	 * Adds the scanned device.
	 * 
	 * @param address
	 *          the address
	 * @param name
	 *          the name
	 */
	private void addScannedDevice(int address, String name)
	{
		GPIBDeviceIdentifier.addDevice(name.trim(), address, this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#getDevice(int)
	 */
	public GPIBDevice getDevice(int address)
	{
		// System.out.println("WindowsGPIBDriver  | driver.getDevice()");
		GPIBDevice dev = new GPIBDevice(address, this);
		return (GPIBDevice) dev;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#clearDevice(int)
	 */
	public void clearDevice(int _descr) throws IOException
	{
		try
		{
			clearDeviceImpl(_descr);
		}
		catch (IOException e)
		{
			throw e;
		}
	}

	/**
	 * Open device.
	 * 
	 * @param _address
	 *          the _address
	 * @return the int
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 */
	public int openDevice(int _address) throws IOException
	{
		int res;
		if ((res = openDeviceImpl(_address)) == -1)
		{
			throw new IOException("Unable to open device");} //$NON-NLS-1$
		return res;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#sendCommand(java.lang.String, int)
	 */
	public String sendCommand(String command, int address) throws IOException
	{
		String result = ""; //$NON-NLS-1$
		try
		{
			result = sendCommandImpl(command, address);
		}
		catch (IOException e)
		{
			throw e;
		}
		return result;
	}

	/**
	 * Clear device impl.
	 * 
	 * @param descriptor
	 *          the descriptor
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 */
	private native void clearDeviceImpl(int descriptor) throws IOException;

	/**
	 * Open device impl.
	 * 
	 * @param address
	 *          the address
	 * @return the int
	 */
	private native int openDeviceImpl(int address);

	/**
	 * Send command impl.
	 * 
	 * @param command
	 *          the command
	 * @param address
	 *          the address
	 * @return the string
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 */
	private native String sendCommandImpl(String command, int address) throws IOException;

	static
	{
		System.loadLibrary("jpib"); //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#getDriverError()
	 */
	public String getDriverError()
	{
		return gpbiError;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#sendCommandBin(java.lang.String, int)
	 */
	public byte[] sendCommandBin(String _message, int _descriptor) throws IOException
	{
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#writeCommand(java.lang.String, int)
	 */
	public void writeCommand(String _message, int _descriptor) throws IOException
	{
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#writeCommand(java.lang.String, int)
	 */
	public void writeCommandData(String _message, byte[] data, int _descriptor) throws IOException
	{
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#openDevice(int, float)
	 */
	public int openDevice(int _address, float _timeout) throws IOException
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * Opens a device and returns a descriptor integer that can be used internally later.
	 *
	 * @param controllerNumber the controller number
	 * @param _address the _address
	 * @param _timeout the _timeout
	 * @return the int
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public int openDevice(int controllerNumber, int _address, float _timeout) throws IOException
	{
		return  openDevice(_address, _timeout);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#localDevice(int)
	 */
	public void localDevice(int _descriptor) throws IOException
	{
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#remoteDevice(int)
	 */
	public void remoteDevice(int _descriptor) throws IOException
	{
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#getMaxBufferLength()
	 */
	public int getMaxBufferLength() throws IOException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#getDLLVersion()
	 */
	public String getDLLVersion() throws IOException
	{
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#getControllerName()
	 */
	@Override
	public String getControllerName()
	{
		return "GPIB"; //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#addListener(be.ac.ulb.gpib.GPIBDriverListener)
	 */
	@Override
	public void addListener(GPIBDriverListener gpibDriverListener)
	{
		this.listenerList.add(gpibDriverListener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#removeListener(be.ac.ulb.gpib.GPIBDriverListener)
	 */
	@Override
	public void removeListener(GPIBDriverListener gpibDriverListener)
	{
		this.listenerList.remove(gpibDriverListener);
	}
	
	/**
	 * Fire bytes readed.
	 *
	 * @param bytesReaded the bytes readed
	 */
	protected void fireBytesReaded(int bytesReaded)
	{
		GPIBDriverListener[] listenerAr = (GPIBDriverListener[]) listenerList.toArray(new GPIBDriverListener[listenerList.size()]);

		for (int i = 0; i < listenerAr.length; i++)
		{
			listenerAr[i].bytesReaded(bytesReaded);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#isAsciiPossible()
	 */
	@Override
	public boolean isAsciiPossible()
	{
		// TODO Auto-generated method stub
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see be.ac.ulb.gpib.GPIBDriver#isBinaryPossible()
	 */
	@Override
	public boolean isBinaryPossible()
	{
		// TODO Auto-generated method stub
		return true;
	}

	/* (non-Javadoc)
	 * @see be.ac.ulb.gpib.GPIBDriver#closeDevice(int)
	 */
	@Override
	public void closeDevice(int controllerNumber)
	{
		// TODO Auto-generated method stub
		
	}

}
