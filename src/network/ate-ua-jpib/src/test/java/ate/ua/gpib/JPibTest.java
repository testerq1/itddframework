package ate.ua.gpib;

import java.io.IOException;
import java.util.Enumeration;

import org.testng.annotations.Test;

import ate.testcase.Testcase;
import be.ac.ulb.gpib.GPIBDevice;
import be.ac.ulb.gpib.GPIBDeviceIdentifier;
import be.ac.ulb.gpib.GPIBDriver;
import be.ac.ulb.tools.OSUtils;

public class JPibTest extends Testcase {
    @Test
    public void ttt() {
        GPIBDeviceIdentifier deviceIdentifier;

        // Load the OS depended Class-Driver
        switch (OSUtils.getOS()) {
        case LINUX32:
        case LINUX64:
            // TODO Driver for Linux must be developed
            GPIBDeviceIdentifier.initialize(
                    "be.ac.ulb.gpib.LinuxGPIBDriver", false); //$NON-NLS-1$
            break;

        case WINDOWS32:
        case WINDOWS64:
            GPIBDeviceIdentifier.initialize(
                    "be.ac.ulb.gpib.WindowsGPIBDriver", false); //$NON-NLS-1$
            break;

        default:
            // OS not supported
            break;
        }
        System.out.println("");

        // Get list of all found devices
        Enumeration<?> gpibDevicesList;
        gpibDevicesList = GPIBDeviceIdentifier.getDevices();
        while (gpibDevicesList.hasMoreElements()) {
            deviceIdentifier = (GPIBDeviceIdentifier) gpibDevicesList
                    .nextElement();
            int addr = deviceIdentifier.getAddress();
            if (addr > 0) {
                GPIBDriver gpibDriver = deviceIdentifier.getDriver();
                System.out.println("Found GPIB device with address '" + addr);

                // GET IDN
                GPIBDevice myDevice = new GPIBDevice(addr, gpibDriver);
                try {
                    myDevice.open(1.0f);

                    try {
                        System.out.println(myDevice.sendCommand("ID"));
                    } catch (IOException e) {
                        System.out.println("No answer for the command 'ID'");
                        // e.printStackTrace();
                    }
                    try {
                        System.out.println(myDevice.sendCommand("*IDN?"));
                    } catch (IOException e) {
                        System.out.println("No answer for the command '*IDN?'");
                        // e.printStackTrace();
                    }
                    System.out.println("");

                } catch (IOException e) {
                    System.out.println("Error on setting GPIB");
                    // e.printStackTrace();
                }
            }
        }
    }
}
