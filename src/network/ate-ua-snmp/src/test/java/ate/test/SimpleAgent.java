package ate.test;

import java.io.IOException;

import org.snmp4j.CommandResponderEvent;
import org.snmp4j.PDU;

import ate.ua.snmp.SnmpAgent;


public class SimpleAgent extends SnmpAgent{  
    public SimpleAgent(String version) {
        super(version);
    }

    public void processRequestEvent(CommandResponderEvent event){
        PDU pdu = event.getPDU();
        if(pdu==null)
            return;
        if(getOid(pdu,0).startsWith("1.3.6.1.4.1.12.2.1")){
            addVB("1.3.6.1.4.1.12.2.1.0", "gauge32", "85");
        }else if(getOid(pdu,0).startsWith("1.3.6.1.4.1.12.2.2")){
            addVB("1.3.6.1.4.1.12.2.1.0", "gauge32", "81");
        }else if(getOid(pdu,0).startsWith("1.3.6.1.2.1.1.3")){
            addVB("1.3.6.1.2.1.1.3.0", "timeticks", "285617700");            
        }else if(getOid(pdu,0).startsWith("1.3.6.1.2.1.1.1")){
            addVB("1.3.6.1.2.1.1.1.0", "octet", "abctest");            
        }else if(getOid(pdu,0).startsWith("1.3.6.1.2.1.1.2")){
            addVB("1.3.6.1.2.1.1.2.0", "object", "1.3.6.1.4.1.12.1.2");            
        }else if(getOid(pdu,0).startsWith("1.3.6.1.2.1.1.4")){
            addVB("1.3.6.1.2.1.1.4.0", "octet", "huangxy");            
        }else if(getOid(pdu,0).startsWith("1.3.6.1.2.1.1.5")){
            addVB("1.3.6.1.2.1.1.5.0", "octet", "testagent");            
        }else if(getOid(pdu,0).startsWith("1.3.6.1.2.1.1.6")){
            addVB("1.3.6.1.2.1.1.6.0", "octet", "changhong");            
        }else{
            System.out.println(getOid(pdu,0));
        }
        super.processRequestEvent(event);
    }
    
    public static void main(String args[]) throws IOException{
        SimpleAgent agt=new SimpleAgent("v1");
        agt.setLocalAddress("udp:0.0.0.0/161");
        agt.listen();
    }
    
}
