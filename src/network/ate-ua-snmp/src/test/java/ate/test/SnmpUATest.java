package ate.test;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashMap;

import org.snmp4j.PDU;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.testcase.CsUATestcase;
import ate.ua.mina.UAClient;
import ate.ua.mina.UAServer;
import ate.ua.snmp.APDU;
import ate.ua.snmp.SnmpUA2;

/**
 * SNMP暂时还不能完整使用
 * snmpgetnext -v 3 -n context4 -u user4 -A md5 -A 1234567890abcdef -x des -X
 * 1234567890abcdef -l authPriv -E 8000137001ac100570 -t 1000 172.16.5.112
 * sysUpTime nmpgetnext -v 3 -n "" -u user4 -A md5 -A 1234567890abcdef -x des -X
 * 1234567890abcdef -l authPriv -t 1000 172.16.5.112 sysUpTime
 * 
 * @author xiaoyong.huang
 * 
 */

public class SnmpUATest extends CsUATestcase {
	
	//@Test
	public void Test_api() {
		SnmpUA2 clt = this.create_client("snmp://172.16.5.194:16147/udp");
		clt.startup();
		assertTrue(clt.get_eid().equals("5105"));
		assertTrue(expect.equal(clt.get_next("1"),"1.3.6.1.4.1.5105.80.1.11.0 = 10000"));
		assertTrue(expect.equal_e(clt.get_next("1"),".*5105*"));
		
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("pdu", clt.GETNEXT);
		APDU pdu = clt.createAPDU(hm);

		pdu.addOid("1.3");
		clt.sendMessage(pdu);
		PDU resp = (PDU)clt.recv_message();
		assertTrue(resp.getErrorStatus()==0);
		assertTrue(resp.getErrorIndex()==0);
		clt.print(resp);
	}
	
	//@Test
//	public void test_lib() throws IOException {
//		String agt = "udp:172.16.5.194/161";
//		Address targetAddress = GenericAddress.parse(agt);
//		DefaultUdpTransportMapping transport = new DefaultUdpTransportMapping();
//		transport.setMaxInboundMessageSize(65520);
//		Snmp snmp = new Snmp(transport);
//		USM usm = new USM(SecurityProtocols.getInstance(), new OctetString(
//				MPv3.createLocalEngineID()), 0);
//		SecurityModels.getInstance().addSecurityModel(usm);
//		transport.listen();
//		
//		// add user to the USM
//		snmp.getUSM().addUser(
//				new OctetString("user4"),
//				new UsmUser(new OctetString("user4"), AuthMD5.ID,
//						new OctetString("1234567890abcdef"), PrivDES.ID,
//						new OctetString("1234567890abcdef")));
//		
//		// create the target
//		UserTarget target = new UserTarget();
//		target.setAddress(targetAddress);
//		//target.setRetries(1);
//		target.setTimeout(500000);
//		target.setVersion(SnmpConstants.version3);
//		target.setSecurityLevel(SecurityLevel.AUTH_PRIV);
//		target.setSecurityName(new OctetString("user4"));
//
//		// create the PDU
//		ScopedPDU pdu = new ScopedPDU();
//		pdu.add(new VariableBinding(new OID("1.3.6.1.2.1.1.3")));
//		pdu.setType(PDU.GETNEXT);
//		pdu.setContextName(new OctetString("context4"));
//		// send the PDU
//		ResponseEvent response = snmp.send(pdu, target);
//		// extract the response PDU (could be null if timed out)
//		PDU responsePDU = response.getResponse();
//		// extract the address used by the agent to send the response:
//		Address peerAddress = response.getPeerAddress();
//	}
//
//
//
	@Test
	public void get() {
		SnmpUA2 clt = create_client("snmp://172.16.5.1");
		assertTrue(clt.get_io_handler() instanceof UAClient);
		start_all_ua();
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("user", "public1");
		hm.put("pdu", clt.GET);
		APDU pdu = clt.createAPDU(hm).addOid("1.3.6.1.4.1.12.2.2.0").addOid("1.3.6.1.4.1.12.2.1.0");
		clt.sendMessage(pdu);
		
		APDU resp = clt.recv_apdu();		
		assertTrue(resp!=null&&((Integer)resp.next())>0);
		log.info(""+resp.v());
		assertTrue(resp.next()!=null&&((Integer)resp.v())>=0);
		log.info(""+resp.v());
		resp.reset();
		assertTrue(((Integer)resp.get("1.3.6.1.4.1.12.2.2.0"))>0);
		assertTrue(((Integer)resp.get("1.3.6.1.4.1.12.2.1.0"))>=0);
	}
	
	public void Test_clt_v3() {
		SnmpUA2 clt = this.create_client("snmp://172.16.5.194:16147/udp");
		assertTrue(clt.get_io_handler() instanceof UAClient);
		start_all_ua();
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("version", clt.SNMPV3);
		hm.put("user", "user4");
		hm.put("pdu", clt.GETNEXT);
		APDU pdu = clt.createAPDU(hm);

		pdu.addOid("1.3");
		clt.sendMessage(pdu);

		PDU resp = (PDU)clt.recv_message();
		clt.print(resp);
	}


	//@Test
	public void Test_svr_v2() {
		SnmpUA2 svr = this.create_server("snmp://127.0.0.1:161/udp");
		assertTrue(svr.get_io_handler() instanceof UAServer);

		SnmpUA2 clt = this.create_client("snmp://127.0.0.1:161/udp");
		assertTrue(clt.get_io_handler() instanceof UAClient);
		start_all_ua();

		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("pdu", clt.GETNEXT);
		APDU pdu = clt.createAPDU(hm);
		pdu.addOid("1.3");
		clt.sendMessage(pdu);
		PDU req = (PDU)svr.recv_message();
		assertTrue(req != null);
		clt.print(req);
		PDU sresp = svr.createResponse(req);
		pdu.pdu = sresp;
		pdu.addTimeticks("1.3.6.1.2.1.1.3.0", "1234567");
		svr.sendMessage(pdu);

		PDU resp = (PDU)clt.recv_message();
		assertTrue(resp != null);
		clt.print(resp);
	}

	@BeforeMethod
	public void beforeMethod() {
	}

	@AfterMethod
	public void afterMethod() {
		this.shutdown_all_ua();
	}

	@BeforeClass
	public void beforeClass() {
		// this.wait_yes();
	}

	@AfterClass
	public void afterClass() {

	}

}
