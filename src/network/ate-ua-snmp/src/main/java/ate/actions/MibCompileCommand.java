package ate.actions;

import net.percederberg.mibble.Mib;
import net.percederberg.mibble.MibLoaderException;
import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;
import ate.ua.snmp.MibFactory;
import ate.util.GenData0;

public class MibCompileCommand extends ATaskRunner {
	
	public TTTask execute(TTTask task) {
		this.task=task;
		this.paras=task.getParas().split(" ");		
		Mib mib;
		try {
			MibFactory mf=MibFactory.build();
			mib=mf.loadMib(paras[0]);
			if(mib!=null){
				mf.unLoadMib(mib);
				task.setExeResult(EXERESULT.SUCCESS);
			}else{
				showTips("只能指定某个的mib文件，不支持文件夹");				
				task.setResultDesc("只能指定某个的mib文件，不支持文件夹");				
			}		
			mf.destroy();
		} catch (MibLoaderException e) {
			e.getLog().printTo(output);		
			task.setResultDesc(GenData0.getExceptionDesc(e));
		}catch (Exception e) {
			showTips(e.getLocalizedMessage());		
			task.setResultDesc(GenData0.getExceptionDesc(e));
		}
		task.setExeResult(EXERESULT.FAIL);
		return task;
	}

	public String getType() {
		return "mibcheck";
	}

	public String getDesc() {
		return "Check Mib File";
	}
}
