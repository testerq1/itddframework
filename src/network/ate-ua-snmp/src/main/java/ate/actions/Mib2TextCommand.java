package ate.actions;
/*
 * MibblePrinter.java
 *
 * This work is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This work is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Copyright (c) 2004-2006 Per Cederberg. All rights reserved.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

import net.percederberg.mibble.Mib;
import net.percederberg.mibble.MibLoader;
import net.percederberg.mibble.MibLoaderException;
import net.percederberg.mibble.MibLoaderLog;
import net.percederberg.mibble.MibSymbol;
import net.percederberg.mibble.MibTypeSymbol;
import net.percederberg.mibble.MibValue;
import net.percederberg.mibble.MibValueSymbol;
import net.percederberg.mibble.MibWriter;
import net.percederberg.mibble.snmp.SnmpObjectType;
import net.percederberg.mibble.value.ObjectIdentifierValue;
import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;
import ate.rm.dev.Host;

import ate.util.GenData0;

/**
 * A program that parses and prints a MIB file. If the MIB file(s)
 * specified on the command-line uses constructs or syntax that are
 * not supported, an error message will be printed to the standard
 * output.
 *
 * @author   Per Cederberg, <per at percederberg dot net>
 * @version  2.7
 * @since    2.0
 */
public class Mib2TextCommand extends ATaskRunner{

    /**
     * The command-line help output.
     */
    private static final String COMMAND_HELP =
        "Prints the contents of an SNMP MIB file. This program comes with\n" +
        "ABSOLUTELY NO WARRANTY; for details see the LICENSE.txt file.\n" +
        "\n" +
        "Syntax: MibblePrinter [--mib|--oid|--debug] <file(s) or URL(s)>\n" +
        "\n" +
        "    --mib     Prints a formatted and indented version of the MIB.\n" +
        "              This is the default printing mode.\n" +
        "    --oid     Prints the complete OID tree, including all nodes\n" +
        "              in imported MIB files\n" +
        "    --debug   Prints the MIB contents in debug format, which will\n" +
        "              display all values completely resolved.";

    /**
     * The internal error message.
     */
    private static final String INTERNAL_ERROR =
        "INTERNAL ERROR: An internal error has been found. Please report\n" +
        "    this error to the maintainers (see the web site for\n" +
        "    instructions). Be sure to include the version number, as\n" +
        "    well as the text below:\n";

    /**
     * The MIB pretty printing mode.
     */
    private static final int MIB_PRINT_MODE = 0;

    /**
     * The MIB oid tree printing mode.
     */
    private static final int OID_PRINT_MODE = 1;

    /**
     * The MIB debug printing mode.
     */
    private static final int DEBUG_PRINT_MODE = 2;

    /**
     * The application main entry point.
     *
     * @param args           the command-line parameters
     */
    public static void main(String[] args) {
        
        // Print loaded MIBs
//        if (printMode == OID_PRINT_MODE) {
//            printOidTree(loader);
//        } else {
//            printMibs(loader, printMode);
//        }
    }

    /**
     * Prints the contents of all MIBs in a MIB loader.
     *
     * @param loader         the MIB loader
     * @param printMode      the print mode to use
     */
    private static void printMibs(MibLoader loader, int printMode) {
        Mib[]  mibs = loader.getAllMibs();

        for (Mib mib : mibs)
			if (mib.isLoaded())
				if (printMode == MIB_PRINT_MODE)
					printMib(mib);
				else
					printDebug(mib);
    }

    /**
     * Prints the contents of a single MIB in pretty printing mode.
     *
     * @param mib            the MIB to print
     */
    private static void printMib(Mib mib) {
        MibWriter  os = new MibWriter(System.out);

        os.print(mib);
        System.out.println();
        System.out.println();
    }

    /**
     * Prints the contents of a single MIB in debug mode.
     *
     * @param mib            the MIB to print
     */
    private static void printDebug(Mib mib) {
        Iterator  iter;

        iter = mib.getAllSymbols().iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }

    /**
     * Prints command-line help information.
     *
     * @param error          an optional error message, or null
     */
    private static void printHelp(String error) {
        System.err.println(COMMAND_HELP);
        System.err.println();
        if (error != null)
			printError(error);
    }

    /**
     * Prints an internal error message. This type of error should
     * only be reported when run-time exceptions occur, such as null
     * pointer and the likes. All these error should be reported as
     * bugs to the program maintainers.
     *
     * @param e              the exception to be reported
     */
    private static void printInternalError(Exception e) {
        System.err.println(INTERNAL_ERROR);
        e.printStackTrace();
    }

    /**
     * Prints an error message.
     *
     * @param message        the error message
     */
    private static void printError(String message) {
        System.err.print("Error: ");
        System.err.println(message);
    }
    
    /**
     * Prints a file not found error message.
     *
     * @param file           the file name not found
     * @param e              the detailed exception
     */
    private static void printError(String file, FileNotFoundException e) {
        StringBuffer  buffer = new StringBuffer();

        buffer.append("couldn't open file:\n    ");
        buffer.append(file);
        printError(buffer.toString());
    }

    /**
     * Prints a URL not found error message.
     *
     * @param url            the URL not found
     * @param e              the detailed exception
     */
    private static void printError(String url, IOException e) {
        StringBuffer  buffer = new StringBuffer();

        buffer.append("couldn't open URL:\n    ");
        buffer.append(url);
        printError(buffer.toString());
    }

	@Override
	public TTTask execute(TTTask task) {
		MibLoader  loader = new MibLoader();
        Mib        mib = null;
        File       file;
        URL        url;
        MibLoaderLog log = null;
        String result="";
        try {
            try {
                url = new URL(task.getParas());
            } catch (MalformedURLException e) {
                url = null;
            }
            if (url == null) {
                file = new File(task.getParas());
                loader.addDir(file.getParentFile());
                mib = loader.load(file);
            } else
				mib = loader.load(url);
            if (mib.getLog().warningCount() > 0)
				mib.getLog().printTo(System.err);
            result=getMibText(loader);   
            task.setExeResult(EXERESULT.SUCCESS);
        } catch (MibLoaderException e) {
            log=e.getLog();//.printTo(System.err);
            task.setExeResult(EXERESULT.FAIL);    
			task.setResultDesc(GenData0.getExceptionDesc(e));
        } catch (Exception e) {
        	task.setExeResult(EXERESULT.FAIL);
			task.setResultDesc(GenData0.getExceptionDesc(e));
		}
        
        String  path=Host.HOME+File.separator+"tmp";        
        try {
			if(!new File(path).exists())
				new File(path).mkdir();
			File outFile = new File(path+File.separator+task.getParas()+".txt");
			FileWriter fstream = new FileWriter(outFile);
			BufferedWriter out = new BufferedWriter(fstream);
			if(result.length()>0){				
				out.write(result);
				task.setExeResult(EXERESULT.SUCCESS);				
			}else if(log!=null){
				log.printTo(new PrintWriter(out));
				task.setExeResult(EXERESULT.FAIL);
			}			
			out.close(); 
			task.setParas(outFile.toURI().toString());
		}  catch (Exception e) {
			task.setExeResult(EXERESULT.FAIL);
			e.printStackTrace();
			task.setResultDesc(GenData0.getExceptionDesc(e));
		}
		return task;
	}
	
	String getMibText(MibLoader loader){
		Mib                    mib;
        ObjectIdentifierValue  root = null;
        Iterator               iter;
        MibSymbol              symbol;
        MibValue               value;

        if (loader.getAllMibs().length <= 0)
			return "Error:no MIB modules have been loaded";
        mib = loader.getAllMibs()[0];
        iter = mib.getAllSymbols().iterator();
        while (root == null && iter.hasNext()) {
            symbol = (MibSymbol) iter.next();
            if (symbol instanceof MibValueSymbol) {
                value = ((MibValueSymbol) symbol).getValue();
                if (value instanceof ObjectIdentifierValue)
					root = (ObjectIdentifierValue) value;
            }
        }
        if (root == null)
			return "Error:no OID value could be found in " + mib.getName();
		else {
            while (root.getParent() != null)
				root = root.getParent();
            return printOid(root);
        }
	}
	/**
     * Prints the detailed OID tree starting in the specified OID. 
     *
     * @param oid            the OID node to print
     */
    private static String printOid(ObjectIdentifierValue oid) {
    	String rst="";
    	if(oid.getSymbol().isScalar()){
	        rst+= "\n++++++++++"+oid.getName();
	        MibTypeSymbol o=((SnmpObjectType)oid.getSymbol().getType()).getSyntax().getReferenceSymbol();
	        if(o==null)
				rst+="\n----------"+((SnmpObjectType)oid.getSymbol().getType()).getSyntax().getName();
			else
				rst+="\n"+o.getName();
    	}
        for (int i = 0; i < oid.getChildCount(); i++)
			rst+=printOid(oid.getChild(i));
        return rst;
    }

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "mib2txt";
	}

	@Override
	public String getDesc() {
		// TODO Auto-generated method stub
		return "SNMP Mib to Text";
	}
}
