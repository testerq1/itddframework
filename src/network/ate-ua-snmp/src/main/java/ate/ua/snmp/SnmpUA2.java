package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.snmp4j.CommandResponderEvent;
import org.snmp4j.CommunityTarget;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.PDU;
import org.snmp4j.ScopedPDU;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.TransportStateReference;
import org.snmp4j.UserTarget;
import org.snmp4j.agent.CommandProcessor;
import org.snmp4j.mp.MPv1;
import org.snmp4j.mp.MPv2c;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.AuthMD5;
import org.snmp4j.security.AuthSHA;
import org.snmp4j.security.Priv3DES;
import org.snmp4j.security.PrivAES128;
import org.snmp4j.security.PrivAES192;
import org.snmp4j.security.PrivAES256;
import org.snmp4j.security.PrivDES;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;

import ate.AbstractURIHandler;
import ate.ua.UAOption;
import ate.ua.mina.AMinaUAImp;
import ate.util.GenData0;

/**
 * 使用SNMP4J本身的机制实现PDU的处理<br>
 * 默认v3用户: md5/des ateauth/atepriv<br>
 * 不支持trap<br>
 * @author Administrator
 * 
 */
public class SnmpUA2 extends AMinaUAImp<PDU> {
	public enum AUTH {
		MD5(AuthMD5.ID), SHA(AuthSHA.ID);
		public static String getName(OID oid) {
			if (oid.equals(SHA.ID))
				return "sha";
			if (oid.equals(MD5.ID))
				return "md5";
			return null;
		}

		private OID ID;

		private AUTH(OID oid) {
			this.ID = oid;
		}

		public String toString() {
			return ID.toString();
		}
	}

	class PendingRequest {
		PDU request;
		int requestStatus = 0;
		Target target;

		// TransportMapping transportMapping;
		PendingRequest(PDU request, Target target) {
			this.request = request;
			this.target = target;
		}
	}

	public enum PRIV {
		AES128(PrivAES128.ID), AES192(PrivAES192.ID), AES256(PrivAES256.ID), DES(
				PrivDES.ID), DESEDE(Priv3DES.ID);
		public static String getName(OID oid) {
			if (oid.equals(DES.ID))
				return "des";
			if (oid.equals(DESEDE))
				return "3des";
			if (oid.equals(AES128))
				return "aes128";
			if (oid.equals(AES192))
				return "aes192";
			if (oid.equals(AES256))
				return "aes256";
			return null;
		}

		OID ID;

		private PRIV(OID oid) {
			this.ID = oid;
		}

		public String toString() {
			return ID.toString();
		}
	}

	class Snmp4jFilter extends ProtocolCodecFilter {
		public Snmp4jFilter() {
			super(new ProtocolEncoderAdapter(){
				public void encode(IoSession session, Object message,
						ProtocolEncoderOutput out) throws Exception {
					if (message instanceof byte[]) {
						out.write(IoBuffer.wrap((byte[])message));
						return;
					}else{
						log.error("message is {}",message);
					}			
				}	
			}, new CumulativeProtocolDecoder(){
				protected boolean doDecode(IoSession session, IoBuffer in,
						ProtocolDecoderOutput out) throws Exception {
					if (in.remaining() < 0)
						return false;
					return true;
				}
			});			
		}

		@Override
		public void messageReceived(NextFilter nextFilter, IoSession session,
				Object message) throws Exception {
			IoBuffer io = (IoBuffer) message;
			TransportStateReference stateReference = new TransportStateReference(
					transportMapping, transportMapping.getListenAddress(),
					null, SecurityLevel.undefined, SecurityLevel.undefined,
					false, session.toString());

			dispatcher.processMessage(transportMapping,
					transportMapping.getListenAddress(), io.buf(),
					stateReference);
			//rcvdpdu是snmp4j本身解析出来的包，也就是说，所有snmp业务相关的处理都在上一行处理完成
			//从而避免在这里解包
			nextFilter.messageReceived(session, rcvdpdu);
		}

		@Override
		public void messageSent(NextFilter nextFilter, IoSession session,
				WriteRequest writeRequest) throws Exception {
			
		}

	}

	public static final String GET = PDU.GET + "";

	public static final String GETBULK = PDU.GETBULK + "";
	public static final String GETNEXT = PDU.GETNEXT + "";
	public static final String INFORM = PDU.INFORM + "";
	public static final String NOTIFICATION = PDU.NOTIFICATION + "";
	public static final String REPORT = PDU.REPORT + "";
	public static final String RESPONSE = PDU.RESPONSE + "";
	public static final String SET = PDU.SET + "";
	public static final String SNMPV1 = SnmpConstants.version1 + "";
	public static final String SNMPV2C = SnmpConstants.version2c + "";

	public static final String SNMPV3 = SnmpConstants.version3 + "";
	public static final String TRAP = PDU.TRAP + "";

	public static final String V1TRAP = PDU.V1TRAP + "";

	public String contextEngineID;

	private static OctetString createOctetString(String s) {
		OctetString octetString;
		if (s.startsWith("0x")) {
			octetString = OctetString.fromHexString(s.substring(2), ':');
		} else {
			octetString = new OctetString(s);
		}
		return octetString;
	}

	public void print(PDU pdu) {
		if (pdu == null) {
			log.debug("pdu is null");
			return;
		}

		log.info(pdu.getTypeString(pdu.getType()) + " PDU info:");
		log.info("---------------------------------");
		log.info("ErrorIndex=" + pdu.getErrorIndex());
		log.info("ErrorStatus=" + pdu.getErrorStatusText());

		for (int i = 0; i < pdu.size(); i++) {
			VariableBinding vb = pdu.get(i);
			log.debug(vb.toString());
		}
		log.info("---------------------------------");
	}

	// public String contextName;
	protected MessageDispatcherImpl dispatcher;
	byte[] engineid = MPv3.createLocalEngineID();
	public int maxRepetitions = 1;
	public int nonRepeaters = 1;
	PDU rcvdpdu;
	public Target readCommunity;
	PendingRequest request;
	SecurityModels securityModels = SecurityModels.getInstance();

	// v3 ，默认用户名
	public String securityName = "user4";
	SecurityProtocols securityProtocols = SecurityProtocols.getInstance();
	TransportMapping transportMapping;

	public Target userTarget;

	USM usm;
	int version = SnmpConstants.version2c;

	public Target writeCommunity;

	@Override
	public void add_default_filter() {
		add_last_iofilter(this.scheme, new Snmp4jFilter());
	}
	/**
	 * addUsmUser("SHADES",SnmpUA.AUTH.MD5,"SHADESAuthPassword",SnmpUA.PRIV.DES,
	 * "SHADESPrivPassword"); addUsmUser("SHADES",SnmpUA,null,"",null,"");
	 * 
	 * @param securityName
	 */
	public void addUsmUser(String securityName, AUTH auth, String authPasswd,
			PRIV priv, String privPasswd) {
		UsmUser user = new UsmUser(new OctetString(securityName),
				auth != null ? auth.ID : null, new OctetString(authPasswd),
				priv != null ? priv.ID : null, new OctetString(privPasswd));
		usm.addUser(user.getSecurityName(), new OctetString(), user);
	}
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if(option==UA_URI){
			if(port<=0)
				port=161;
			if(transport==null)
				transport="udp";
		}
		return this;
	}

	public APDU createAPDU(HashMap<String, String> hm) {
		int version = SnmpConstants.version2c;
		if (hm.get("version") != null)
			version = Integer.parseInt(hm.get("version"));
		int pduType = Integer.parseInt(hm.get("pdu"));

		APDU apdu = new APDU();
		apdu.version = version;
		PDU pdu = null;
		if (version == SnmpConstants.version3) {
			pdu = new ScopedPDU();
			ScopedPDU scopedPDU = (ScopedPDU) pdu;
			if (apdu.contextEngineID != null) {
				scopedPDU.setContextEngineID(new OctetString(
						apdu.contextEngineID));
			}
			if (apdu.contextName != null) {
				scopedPDU.setContextName(new OctetString(apdu.contextName));
			}
		} else {
			if (pduType != PDU.V1TRAP) {
				pdu = new PDU();
			}
		}
		pdu.setType(pduType);
		if (pdu.getType() == PDU.GETBULK) {
			pdu.setMaxRepetitions(maxRepetitions);
			pdu.setNonRepeaters(nonRepeaters);
		}

		apdu.pdu = pdu;
		return apdu;
	}

	public PDU createResponse(PDU pdu) {
		PDU resp = (PDU) pdu.clone();
		resp.clear();
		resp.setType(PDU.RESPONSE);
		resp.setRequestID(pdu.getRequestID());
		resp.setErrorIndex(0);
		resp.setErrorStatus(PDU.noError);
		return resp;
	}

	private Target createTarget(HashMap<String, String> hm) {
		Target target = null;
		if (hm.get("user") == null)
			return null;

		int version = SnmpConstants.version2c;
		if (hm.get("version") != null) {
			version = Integer.parseInt(hm.remove("version"));
		}
		if (version == SnmpConstants.version3) {
			UserTarget tmp = new UserTarget();
			int level = 0;
			if (hm.get("authpass") != null)
				level += 1;
			if (hm.get("privpass") != null)
				level += 2;
			tmp.setSecurityLevel(level);
			tmp.setSecurityName(createOctetString(hm.get("user")));
			target = tmp;
		} else {
			CommunityTarget tmp = new CommunityTarget();
			tmp.setCommunity(this.createOctetString(hm.get("user")));
			target = tmp;
		}
		
		String addr=host+"/"+port;
		target.setVersion(version);
		if (this.transport.equalsIgnoreCase("udp"))
			target.setAddress(new UdpAddress(addr));
		else
			target.setAddress(new TcpAddress(addr));
		target.setRetries(1);
		target.setTimeout(10000);
		target.setMaxSizeRequestPDU(65535);
		return target;
	}
	public String get(String oid) {
		return request_v2c(oid, GET);
	}
	
	@Override
	public String get_default_schema() {
		return "snmp";
	}

	/**
	 * 获得企业标识
	 * 
	 * @return
	 */
	public String get_eid() {
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("pdu", GETNEXT);
		APDU pdu = this.createAPDU(hm);
		pdu.addOid("1.3.6.1.4.1");
		this.sendMessage(pdu);
		PDU resp = (PDU) super.recv_message();
		return resp.get(0).getOid().get(6) + "";
	}

	public String get_next(String oid) {
		return request_v2c(oid, GETNEXT);
	}

	@Override
	public boolean message_matched(PDU msg, HashMap hm) {
		return true;
	}	

	private String request_v2c(String oid, String type) {
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("pdu", type);
		APDU pdu = this.createAPDU(hm);
		pdu.addOid(oid);
		this.sendMessage(pdu);
		PDU resp = (PDU) super.recv_message();
		if (resp == null)
			return null;
		VariableBinding vb = resp.get(0);
		if (vb != null)
			return vb.toString();
		return null;
	}

	public void sendMessage(APDU pdu) {
		Target target = this.readCommunity;
		if (pdu.version == SnmpConstants.version3)
			target = this.userTarget;
		else if (pdu.pdu.getType() == PDU.SET)
			target = this.writeCommunity;
		sendMessage(pdu.pdu, target);
	}

	public void sendMessage(PDU pdu, Target target) {
		log.debug("send pdu {} to {}", pdu, target);
		try {			
			if (pdu.getType() != PDU.RESPONSE) {
				int ir = GenData0.random(0, 2 ^ 32 - 1);
				pdu.setRequestID(new Integer32(ir));
			}
			this.dispatcher.sendPdu(target, pdu, true);
			this.request = new PendingRequest(pdu, target);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// 只支持V2C
	public boolean set(String oid, String type, String value) {
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("pdu", SET);
		APDU pdu = this.createAPDU(hm);
		VariableBinding vb0 = new VariableBinding(new OID(oid));
		vb0.setVariable(SNMPUtil.createVariable(type, value));
		pdu.pdu.add(vb0);

		this.sendMessage(pdu);
		PDU resp = super.recv_message();
		if (resp == null)
			log.warn("no response");
		return resp != null && resp.getErrorStatus() == 0;
	}
	
	@Override
	public void startup() {
		transportMapping = MinaTransportMapping.createTransportMapping(this);
		
		usm = new USM(securityProtocols, new OctetString(engineid), 0);
		securityModels.addSecurityModel(usm);
		securityProtocols.addDefaultProtocols();

		addUsmUser(securityName, SnmpUA2.AUTH.MD5, "1234567890abcdef",
				SnmpUA2.PRIV.DES, "1234567890abcdef");
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("user", "public");
		this.readCommunity = this.createTarget(hm);
		hm.put("user", "private");
		this.writeCommunity = this.createTarget(hm);
		hm.put("user", securityName);
		hm.put("version", SnmpConstants.version3 + "");
		hm.put("authpass", AUTH.MD5.toString());
		hm.put("privpass", PRIV.DES.toString());
		this.userTarget = this.createTarget(hm);

		dispatcher = new MessageDispatcherImpl();
		dispatcher.addMessageProcessingModel(new MPv1());
		dispatcher.addMessageProcessingModel(new MPv2c());
		dispatcher.addMessageProcessingModel(new MPv3(engineid));

		CommandProcessor commandResponder = new CommandProcessor(
				new OctetString(MPv3.createLocalEngineID())) {
			public void processPdu(CommandResponderEvent event) {
				rcvdpdu = event.getPDU();
			}
		};
		dispatcher.addCommandResponder(commandResponder);

		dispatcher.addTransportMapping(transportMapping);
		super.startup();
	}
	public APDU recv_apdu() {
		PDU pdu=super.recv_message(null);
		if(pdu==null)
			return null;
		return new APDU(pdu);
	}
	
	public boolean is_ok(PDU pdu){
		return pdu!=null&&0==pdu.getErrorIndex()&&pdu.getErrorStatus()==0;
	}
}
