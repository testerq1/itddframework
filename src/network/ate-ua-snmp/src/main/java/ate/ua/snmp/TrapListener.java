package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;

import org.snmp4j.CommandResponder;
import org.snmp4j.CommandResponderEvent;
import org.snmp4j.MessageDispatcher;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.MessageException;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.log.LogFactory;
import org.snmp4j.mp.MPv1;
import org.snmp4j.mp.MPv2c;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.StateReference;
import org.snmp4j.mp.StatusInformation;
import org.snmp4j.security.Priv3DES;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.transport.AbstractTransportMapping;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.MultiThreadedMessageDispatcher;
import org.snmp4j.util.ThreadPool;

public class TrapListener implements CommandResponder {
	int numDispatcherThreads = 2;
	Address address;
	OctetString localEngineID = new OctetString(MPv3.createLocalEngineID());
	
	public void setAddress(String address){
		this.address=SnmpConfig.createAddress(address);
	}
	
	public void setNumDispatcherThreads(int numDispatcherThreads) {
	    this.numDispatcherThreads = numDispatcherThreads;
	}
	
	public TrapListener(String transportAddress) {
		String transport = "udp";
		int colon = transportAddress.indexOf(':');
		if (colon > 0) {
			transport = transportAddress.substring(0, colon);
			transportAddress = transportAddress.substring(colon + 1);
		}
		// set default port
		if (transportAddress.indexOf('/') < 0) {
			transportAddress += "/161";
		}
		if (transport.equalsIgnoreCase("udp")) {
			this.address = new UdpAddress(transportAddress);
		} else if (transport.equalsIgnoreCase("tcp")) {
			this.address = new TcpAddress(transportAddress);
		}
		throw new IllegalArgumentException("Unknown transport " + transport);
	}

	public synchronized void listen() throws IOException {
		AbstractTransportMapping transport;
		if (address instanceof TcpAddress) {
			transport = new DefaultTcpTransportMapping((TcpAddress) address);
		} else {
			transport = new DefaultUdpTransportMapping((UdpAddress) address);
		}
		ThreadPool threadPool = ThreadPool.create("DispatcherPool",
				numDispatcherThreads);
		MessageDispatcher mtDispatcher = new MultiThreadedMessageDispatcher(
				threadPool, new MessageDispatcherImpl());

		// add message processing models
		mtDispatcher.addMessageProcessingModel(new MPv1());
		mtDispatcher.addMessageProcessingModel(new MPv2c());
		mtDispatcher.addMessageProcessingModel(new MPv3(localEngineID
				.getValue()));

		// add all security protocols
		SecurityProtocols.getInstance().addDefaultProtocols();
		SecurityProtocols.getInstance().addPrivacyProtocol(new Priv3DES());

		Snmp snmp = new Snmp(mtDispatcher, transport);
//		if (version == SnmpConstants.version3) {
//			USM usm = new USM(SecurityProtocols.getInstance(), localEngineID, 0);
//			SecurityModels.getInstance().addSecurityModel(usm);
//			if (authoritativeEngineID != null) {
//				snmp.setLocalEngine(authoritativeEngineID.getValue(), 0, 0);
//			}
//			// Add the configured user to the USM
//			addUsmUser(snmp);
//		} else {
//			CommunityTarget target = new CommunityTarget();
//			target.setCommunity(community);
//		}

		snmp.addCommandResponder(this);

		transport.listen();
		System.out.println("Listening on " + address);

		try {
			this.wait();
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

	public synchronized void processPdu(CommandResponderEvent e) {
		PDU command = e.getPDU();
		if (command != null) {
			System.out.println(command.toString());
			if ((command.getType() != PDU.TRAP)
					&& (command.getType() != PDU.V1TRAP)
					&& (command.getType() != PDU.REPORT)
					&& (command.getType() != PDU.RESPONSE)) {
				command.setErrorIndex(0);
				command.setErrorStatus(0);
				command.setType(PDU.RESPONSE);
				StatusInformation statusInformation = new StatusInformation();
				StateReference ref = e.getStateReference();
				try {
					e.getMessageDispatcher().returnResponsePdu(
							e.getMessageProcessingModel(),
							e.getSecurityModel(), e.getSecurityName(),
							e.getSecurityLevel(), command,
							e.getMaxSizeResponsePDU(), ref, statusInformation);
				} catch (MessageException ex) {
					System.err.println("Error while sending response: "
							+ ex.getMessage());
					LogFactory.getLogger(SnmpConfig.class).error(ex);
				}
			}
		}
	}
}
