package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.ScopedPDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.MessageProcessingModel;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.Counter32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.SMIConstants;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.util.PDUFactory;
import org.snmp4j.util.TableEvent;
import org.snmp4j.util.TableListener;
import org.snmp4j.util.TableUtils;

public class TableOperation extends SnmpConfig implements PDUFactory {
	Snmp snmp;
	public static final int DEFAULT = 0;
	public static final int WALK = 1;
	public static final int TABLE = 3;
	public static final int CVS_TABLE = 4;
	public static final int TIME_BASED_CVS_TABLE = 5;
	public static final int SNAPSHOT_CREATION = 6;
	public static final int SNAPSHOT_DUMP = 7;
	
	File snapshotFile;

	protected int operation = DEFAULT;

	int numDispatcherThreads = 2;

	boolean useDenseTableOperation = false;
	int pduType;
	// table options
	OID lowerBoundIndex, upperBoundIndex;
	Target target;
	Address address;
	OID authProtocol;
	OID privProtocol;
	OctetString privPassphrase;
	OctetString authPassphrase;
	OctetString community = new OctetString("public");
	OctetString authoritativeEngineID;
	OctetString contextEngineID;
	OctetString contextName = new OctetString();
	OctetString securityName = new OctetString();
	OctetString localEngineID = new OctetString(MPv3.createLocalEngineID());
	TimeTicks sysUpTime = new TimeTicks(0);
	OID trapOID = SnmpConstants.coldStart;

	PDUv1 v1TrapPDU = new PDUv1();

	  public void setUseDenseTableOperation(boolean useDenseTableOperation) {
	    this.useDenseTableOperation = useDenseTableOperation;
	  }
	
	private void createSnapshot(List snapshot) {
	    FileOutputStream fos = null;
	    try {
	      fos = new FileOutputStream(snapshotFile);
	      ObjectOutputStream oos = new ObjectOutputStream(fos);
	      oos.writeObject(snapshot);
	      oos.flush();
	    }
	    catch (Exception ex) {
	      ex.printStackTrace();
	    }
	    finally {
	      if (fos != null) {
	        try {
	          fos.close();
	        }
	        catch (IOException ex1) {
	        }
	      }
	    }
	  }
	public PDU createPDU(Target target) {
		PDU request;
		if (target.getVersion() == SnmpConstants.version3) {
			request = new ScopedPDU();
			ScopedPDU scopedPDU = (ScopedPDU) request;
			if (contextEngineID != null) {
				scopedPDU.setContextEngineID(contextEngineID);
			}
			if (contextName != null) {
				scopedPDU.setContextName(contextName);
			}
		} else {
			if (pduType == PDU.V1TRAP) {
				request = v1TrapPDU;
			} else {
				request = new PDU();
			}
		}
		request.setType(pduType);
		return request;
	}

	public void table(Vector<VariableBinding> vbs) throws IOException {
		TableUtils tableUtils = new TableUtils(snmp, this);
		tableUtils.setMaxNumRowsPerPDU(maxRepetitions);
		Counter32 counter = new Counter32();

		OID[] columns = new OID[vbs.size()];
		for (int i = 0; i < columns.length; i++) {
			columns[i] = ((VariableBinding) vbs.get(i)).getOid();
		}
		long startTime = System.currentTimeMillis();
		synchronized (counter) {

			TableListener listener;
			if (operation == TABLE) {
				listener = new TextTableListener();
			} else {
				listener = new CVSTableListener(System.currentTimeMillis());
			}
			if (useDenseTableOperation) {
				tableUtils.getDenseTable(target, columns, listener, counter,
						lowerBoundIndex, upperBoundIndex);
			} else {
				tableUtils.getTable(target, columns, listener, counter,
						lowerBoundIndex, upperBoundIndex);
			}
			try {
				counter.wait(timeout);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}
		System.out.println("Table received in "
				+ (System.currentTimeMillis() - startTime) + " milliseconds.");
	}

	class CVSTableListener implements TableListener {

		private long requestTime;
		boolean isfinished=false;
		public boolean isFinished() {				
			return isfinished;
		}
		public CVSTableListener(long time) {
			this.requestTime = time;
		}

		public boolean next(TableEvent event) {
			if (operation == TIME_BASED_CVS_TABLE) {
				System.out.print(requestTime);
				System.out.print(",");
			}
			System.out.print("\"" + event.getIndex() + "\",");
			for (int i = 0; i < event.getColumns().length; i++) {
				Variable v = event.getColumns()[i].getVariable();
				String value = v.toString();
				switch (v.getSyntax()) {
				case SMIConstants.SYNTAX_OCTET_STRING: {
					StringBuffer escapedString = new StringBuffer(value
							.length());
					StringTokenizer st = new StringTokenizer(value, "\"", true);
					while (st.hasMoreTokens()) {
						String token = st.nextToken();
						escapedString.append(token);
						if (token.equals("\"")) {
							escapedString.append("\"");
						}
					}
				}
				case SMIConstants.SYNTAX_IPADDRESS:
				case SMIConstants.SYNTAX_OBJECT_IDENTIFIER:
				case SMIConstants.SYNTAX_OPAQUE: {
					System.out.print("\"");
					System.out.print(value);
					System.out.print("\"");
					break;
				}
				default: {
					System.out.print(value);
				}
				}
				if (i + 1 < event.getColumns().length) {
					System.out.print(",");
				}
			}
			System.out.println();
			return true;
		}

		public void finished(TableEvent event) {
			synchronized (event.getUserObject()) {
				event.getUserObject().notify();
			}
			isfinished=true;
		}

	}

	class TextTableListener implements TableListener {
		boolean isfinished=false;
		public boolean isFinished() {				
			return isfinished;
		}
		public void finished(TableEvent event) {
			System.out.println();
			System.out.println("Table walk completed with status "
					+ event.getStatus() + ". Received " + event.getUserObject()
					+ " rows.");
			synchronized (event.getUserObject()) {
				event.getUserObject().notify();
			}
			isfinished=true;
		}

		public boolean next(TableEvent event) {
			System.out.println("Index = " + event.getIndex() + ":");
			for (int i = 0; i < event.getColumns().length; i++) {
				System.out.println(event.getColumns()[i]);
			}
			System.out.println();
			((Counter32) event.getUserObject()).increment();
			return true;
		}

	}

	private void dumpSnapshot() {
	    FileInputStream fis = null;
	    try {
	      fis = new FileInputStream(snapshotFile);
	      ObjectInputStream ois = new ObjectInputStream(fis);
	      List l = (List) ois.readObject();
	      int i=1;
	      System.out.println("Dumping snapshot file '"+snapshotFile+"':");
	      for (Iterator it = l.iterator(); it.hasNext(); i++) {
	        System.out.println(""+i+": "+it.next());
	      }
	      System.out.println();
	      System.out.println("Dumped "+l.size()+" variable bindings.");
	    }
	    catch (Exception ex) {
	      ex.printStackTrace();
	    }
	    finally {
	      if (fis != null) {
	        try {
	          fis.close();
	        }
	        catch (IOException ex1) {
	        }
	      }
	    }
	  }
	public int getPduType() {
	    return pduType;
	  }
	  public static void main(String[] args) {
	    try {
	    	TableOperation snmpRequest = new TableOperation();
	      if (snmpRequest.operation == SNAPSHOT_DUMP) {
	        snmpRequest.dumpSnapshot();
	      }
	      else {
	        try {
	          if ((snmpRequest.operation == TABLE) ||
	                   (snmpRequest.operation == CVS_TABLE) ||
	                   (snmpRequest.operation == TIME_BASED_CVS_TABLE)) {
	            snmpRequest.table(new Vector());
	          }
	          else {
//	            PDU response = snmpRequest.send(new Vector());
//	            if ((snmpRequest.getPduType() == PDU.TRAP) ||
//	                (snmpRequest.getPduType() == PDU.REPORT) ||
//	                (snmpRequest.getPduType() == PDU.V1TRAP) ||
//	                (snmpRequest.getPduType() == PDU.RESPONSE)) {
//	              System.out.println(PDU.getTypeString(snmpRequest.getPduType()) +
//	                                 " sent successfully");
//	            }
//	            else if (response == null) {
//	              if (snmpRequest.operation != WALK) {
//	                System.out.println("Request timed out.");
//	              }
//	            }
//	            else if (response.getType() == PDU.REPORT) {
//	              printReport(response);
//	            }
//	            else if (snmpRequest.operation == DEFAULT) {
//	              System.out.println("Response received with requestID=" +
//	                                 response.getRequestID() +
//	                                 ", errorIndex=" +
//	                                 response.getErrorIndex() + ", " +
//	                                 "errorStatus=" + response.getErrorStatusText() +
//	                                 "(" + response.getErrorStatus() + ")");
//	              printVariableBindings(response);
//	            }
//	            else {
//	              System.out.println("Received something strange: requestID=" +
//	                                 response.getRequestID() +
//	                                 ", errorIndex=" +
//	                                 response.getErrorIndex() + ", " +
//	                                 "errorStatus=" + response.getErrorStatusText() +
//	                                 "(" + response.getErrorStatus() + ")");
//	              printVariableBindings(response);
//	            }
	          }
	        }
	        catch (IOException ex) {
	          System.err.println("Error while trying to send request: " +
	                             ex.getMessage());
	          ex.printStackTrace();
	        }
	      }
	    }
	    catch (IllegalArgumentException iaex) {
	      System.err.print("Error: "+iaex.getMessage());
	      iaex.printStackTrace();
	    }
	  }
	  protected static void printReport(PDU response) {
		    if (response.size() < 1) {
		      System.out.println("REPORT PDU does not contain a variable binding.");
		      return;
		    }

		    VariableBinding vb = response.get(0);
		    OID oid =vb.getOid();
		    if (SnmpConstants.usmStatsUnsupportedSecLevels.equals(oid)) {
		      System.out.print("REPORT: Unsupported Security Level.");
		    }
		    else if (SnmpConstants.usmStatsNotInTimeWindows.equals(oid)) {
		      System.out.print("REPORT: Message not within time window.");
		    }
		    else if (SnmpConstants.usmStatsUnknownUserNames.equals(oid)) {
		      System.out.print("REPORT: Unknown user name.");
		    }
		    else if (SnmpConstants.usmStatsUnknownEngineIDs.equals(oid)) {
		      System.out.print("REPORT: Unknown engine id.");
		    }
		    else if (SnmpConstants.usmStatsWrongDigests.equals(oid)) {
		      System.out.print("REPORT: Wrong digest.");
		    }
		    else if (SnmpConstants.usmStatsDecryptionErrors.equals(oid)) {
		      System.out.print("REPORT: Decryption error.");
		    }
		    else if (SnmpConstants.snmpUnknownSecurityModels.equals(oid)) {
		      System.out.print("REPORT: Unknown security model.");
		    }
		    else if (SnmpConstants.snmpInvalidMsgs.equals(oid)) {
		      System.out.print("REPORT: Invalid message.");
		    }
		    else if (SnmpConstants.snmpUnknownPDUHandlers.equals(oid)) {
		      System.out.print("REPORT: Unknown PDU handler.");
		    }
		    else if (SnmpConstants.snmpUnavailableContexts.equals(oid)) {
		      System.out.print("REPORT: Unavailable context.");
		    }
		    else if (SnmpConstants.snmpUnknownContexts.equals(oid)) {
		      System.out.print("REPORT: Unknown context.");
		    }
		    else {
		      System.out.print("REPORT contains unknown OID ("
		                         + oid.toString() + ").");
		    }
		    System.out.println(" Current counter value is " +
		                       vb.getVariable().toString() + ".");
		  }

	  protected static void printVariableBindings(PDU response) {
	    for (int i=0; i<response.size(); i++) {
	      VariableBinding vb = response.get(i);
	      System.out.println(vb.toString());
	    }
	  }

	  public void setUpperBoundIndex(String upperBoundIndex) {
	    this.upperBoundIndex = new OID(upperBoundIndex);
	  }

	  public void setLowerBoundIndex(String lowerBoundIndex) {
	    this.lowerBoundIndex = new OID(lowerBoundIndex);
	  }

	@Override
	public PDU createPDU(MessageProcessingModel messageProcessingModel) {
		
		return null;
	}
}
