package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.ScopedPDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.UserTarget;
import org.snmp4j.log.LogFactory;
import org.snmp4j.log.LogLevel;
import org.snmp4j.mp.CounterSupport;
import org.snmp4j.mp.DefaultCounterListener;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.AuthMD5;
import org.snmp4j.security.AuthSHA;
import org.snmp4j.security.Priv3DES;
import org.snmp4j.security.PrivAES128;
import org.snmp4j.security.PrivAES192;
import org.snmp4j.security.PrivAES256;
import org.snmp4j.security.PrivDES;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.AbstractTransportMapping;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TreeEvent;
import org.snmp4j.util.TreeListener;
import org.snmp4j.util.TreeUtils;

/**
 * The SnmpRequest application is an example implementation of most of the
 * SNMP4J features. It can be used to send SNMP requests to a target or to
 * listen for traps/notifications and inform requests.
 *
 * @author Frank Fock
 * @version 1.9
 */
public class SnmpConfig {
	int timeout = 1000;
	int version = SnmpConstants.version3;
	CommunityTarget writeTarget;
	Address address;
	OctetString authPassphrase;
	OID authProtocol;
	// OctetString authoritativeEngineID;
	OctetString contextEngineID;
	OctetString contextName = new OctetString();
	int engineBootCount = 0;	
	String key;
	OctetString localEngineID = new OctetString(MPv3.createLocalEngineID());
	Logger log = Logger.getLogger(SnmpConfig.class);
	int maxRepetitions = 10;

	int maxSizeResponsePDU = 65535;
	int nonRepeaters = 0;
	OctetString privPassphrase;
	OID privProtocol;
	CommunityTarget readTarget;
	int retries = 1;
	OctetString securityName = new OctetString();

	Target target;

	class WalkCounts {
		public int objects;
		public int requests;
	}
	public static Address createAddress(String transportAddress) {
		Address address;
		String transport = "udp";
		int colon = transportAddress.indexOf(':');
		if (colon > 0) {
			transport = transportAddress.substring(0, colon);
			transportAddress = transportAddress.substring(colon + 1);
		}
		// set default port
		if (transportAddress.indexOf('/') < 0) {
			transportAddress += "/161";
		}
		if (transport.equalsIgnoreCase("udp")) {
			address = new UdpAddress(transportAddress);
		} else if (transport.equalsIgnoreCase("tcp")) {
			address = new TcpAddress(transportAddress);
		} else {
			throw new IllegalArgumentException("Unknown transport " + transport);
		}
		return address;
	}
	private static OctetString createOctetString(String s) {
		OctetString octetString;
		if (s.startsWith("0x")) {
			octetString = OctetString.fromHexString(s.substring(2), ':');
		} else {
			octetString = new OctetString(s);
		}
		return octetString;
	}
	//  private void checkOptions() {
	//    if ((operation == WALK) &&
	//        ((pduType != PDU.GETBULK) && (pduType != PDU.GETNEXT))) {
	//      throw new IllegalArgumentException(
	//          "Walk operation is not supported for PDU type: "+
	//          PDU.getTypeString(pduType));
	//    }
	//    else if ((operation == WALK) && (vbs.size() != 1)) {
	//      throw new IllegalArgumentException(
	//          "There must be exactly one OID supplied for walk operations");
	//    }
	//    if ((pduType == PDU.V1TRAP) && (version != SnmpConstants.version1)) {
	//      throw new IllegalArgumentException(
	//          "V1TRAP PDU type is only available for SNMP version 1");
	//    }
	//  }

	public SnmpConfig() {
		// Set the default counter listener to return proper USM and MP error
		// counters.
		CounterSupport.getInstance().addCounterListener(
				new DefaultCounterListener());
	}

	private void addUsmUser(Snmp snmp) {
		snmp.getUSM().addUser(
				securityName,
				new UsmUser(securityName, authProtocol, authPassphrase,
						privProtocol, privPassphrase));
	}

	public PDU createPDU(int pduType) {
		//Target target = this.getTarget(pduType);
		PDU request = null;
		if (target.getVersion() == SnmpConstants.version3) {
			request = new ScopedPDU();
			ScopedPDU scopedPDU = (ScopedPDU) request;
			if (contextEngineID != null) {
				scopedPDU.setContextEngineID(contextEngineID);
			}
			if (contextName != null) {
				scopedPDU.setContextName(contextName);
			}
		} else {
			if (pduType != PDU.V1TRAP) {
				request = new PDU();
			}
		}
		request.setType(pduType);
		if (request.getType() == PDU.GETBULK) {
			request.setMaxRepetitions(maxRepetitions);
			request.setNonRepeaters(nonRepeaters);
		}
		return request;
	}

	public Snmp createSnmpSession() throws IOException {
		AbstractTransportMapping transport;
		if (address instanceof TcpAddress) {
			transport = new DefaultTcpTransportMapping();
			transport.getMaxInboundMessageSize();
		} else {
			transport = new DefaultUdpTransportMapping();
		}
		// Could save some CPU cycles:
		// transport.setAsyncMsgProcessingSupported(false);
		Snmp snmp = new Snmp(transport);
		((MPv3) snmp.getMessageProcessingModel(MPv3.ID))
				.setLocalEngineID(localEngineID.getValue());

		if (version == SnmpConstants.version3) {
			USM usm = new USM(SecurityProtocols.getInstance(), localEngineID,
					engineBootCount);
			SecurityModels.getInstance().addSecurityModel(usm);
			addUsmUser(snmp);
		}
		return snmp;
	}

	public String getKey() {
		return key;
	}

	public Target getTarget(int pduType) {
		if (version == SnmpConstants.version3) {
			return this.target;
		}
		if (pduType == PDU.SET) {
			return this.writeTarget;
		}
		return this.readTarget;
	}

	public void setAddress(String transportAddress) {
		this.key = transportAddress;
		this.address = createAddress(transportAddress);
	}

	public void setAuthPassphrase(String s) {
		this.authPassphrase = createOctetString(s);
	}

	public void setAuthProtocol(String s) {
		if (s.equals("md5")) {
			authProtocol = AuthMD5.ID;
		} else if (s.equals("sha")) {
			authProtocol = AuthSHA.ID;
		} else {
			throw new IllegalArgumentException(
					"Authentication protocol unsupported: " + s);
		}
	}

	public void setContextEngineID(String s) {
		this.contextEngineID = createOctetString(s);
	}

	public void setContextName(String s) {
		this.contextName = createOctetString(s);
	}

	public void setDebugOption(String s) {
		LogFactory.getLogFactory().getRootLogger().setLogLevel(
				LogLevel.toLevel(s));
	}

	public void setEngineBootCount(int i) {
		this.engineBootCount = Math.max(i, 0);
	}

	public void setLocalEngineID(String s) {
		localEngineID = createOctetString(s);
	}

	public void setMaxRepetitions(int maxRepetitions) {
		this.maxRepetitions = maxRepetitions;
	}

	public void setMaxSizeResponsePDU(int i) {
		maxSizeResponsePDU = i;
	}

	public void setNonRepeaters(int nonRepeaters) {
		this.nonRepeaters = nonRepeaters;
	}

	public void setPrivPassphrase(String s) {
		this.privPassphrase = createOctetString(s);
	}

	public void setPrivProtocol(String s) {
		if (s.equals("des")) {
			privProtocol = PrivDES.ID;
		} else if ((s.equals("aes128")) || (s.equals("aes"))) {
			privProtocol = PrivAES128.ID;
		} else if (s.equals("aes192")) {
			privProtocol = PrivAES192.ID;
		} else if (s.equals("aes256")) {
			privProtocol = PrivAES256.ID;
		} else if ((s.equals("3des") || s.equalsIgnoreCase("desde"))) {
			privProtocol = Priv3DES.ID;
		} else {
			throw new IllegalArgumentException("Privacy protocol " + s
					+ " not supported");
		}
	}

	public void setReadTarget(String community) {
		readTarget = new CommunityTarget();
		readTarget.setCommunity(this.createOctetString(community));
		readTarget.setVersion(version);
		readTarget.setAddress(address);
		readTarget.setRetries(retries);
		readTarget.setTimeout(timeout);
		readTarget.setMaxSizeRequestPDU(maxSizeResponsePDU);
	}

	public void setRetries(int retries) {
		this.retries = retries;
	}

	public void setSecurityName(String s) {
		this.securityName = createOctetString(s);
	}

	public void setTarget(Target target) {
		this.target = target;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public void initV3Target() {
		if (version == SnmpConstants.version3) {
			UserTarget target = new UserTarget();
			if (authPassphrase != null) {
				if (privPassphrase != null) {
					target.setSecurityLevel(SecurityLevel.AUTH_PRIV);
				} else {
					log.warn("privPassphrase没有配置");
					target.setSecurityLevel(SecurityLevel.AUTH_NOPRIV);
				}
			} else {
				log.warn("authPassphrase没有配置");
				target.setSecurityLevel(SecurityLevel.NOAUTH_NOPRIV);
			}
			target.setSecurityName(securityName);
		} else {
			throw new IllegalArgumentException("版本不正确" + version);
		}
		target.setVersion(version);
		target.setAddress(address);
		target.setRetries(retries);
		target.setTimeout(timeout);
		
		target.setMaxSizeRequestPDU(maxSizeResponsePDU);
	}

	public void setVersion(String v) {
		if (v.equals("1")) {
			version = SnmpConstants.version1;
		} else if (v.equals("2c")) {
			version = SnmpConstants.version2c;
		} else if (v.equals("3")) {
			version = SnmpConstants.version3;
		} else {
			throw new IllegalArgumentException("Version " + v
					+ " not supported");
		}
	}

	public void setWriteTarget(String community) {
		this.writeTarget = new CommunityTarget();
		writeTarget.setCommunity(this.createOctetString(community));
		writeTarget.setVersion(version);
		writeTarget.setAddress(address);
		writeTarget.setRetries(retries);
		writeTarget.setTimeout(timeout);
		writeTarget.setMaxSizeRequestPDU(maxSizeResponsePDU);
	}

	public PDU walk(Snmp snmp, PDU request, Target target, final List snapshot)
			throws IOException {
		request.setNonRepeaters(0);
		OID rootOID = request.get(0).getOid();
		PDU response = null;
		final WalkCounts counts = new WalkCounts();
		final long startTime = System.currentTimeMillis();
		TreeUtils treeUtils = new TreeUtils(snmp, new DefaultPDUFactory());
		TreeListener treeListener = new TreeListener() {
			boolean isfinished=false;
			public boolean isFinished() {				
				return isfinished;
			}

			public void finished(TreeEvent e) {
				if ((e.getVariableBindings() != null)
						&& (e.getVariableBindings().length > 0)) {
					next(e);
				}
				System.out.println();
				System.out
						.println("Total requests sent:    " + counts.requests);
				System.out.println("Total objects received: " + counts.objects);
				System.out.println("Total walk time:        "
						+ (System.currentTimeMillis() - startTime)
						+ " milliseconds");
				if (e.isError()) {
					System.err
							.println("The following error occurred during walk:");
					System.err.println(e.getErrorMessage());
				}
				synchronized (this) {
					this.notify();
				}
				isfinished=true;
			}

			public boolean next(TreeEvent e) {
				counts.requests++;
				if (e.getVariableBindings() != null) {
					VariableBinding[] vbs = e.getVariableBindings();
					counts.objects += vbs.length;
					for (int i = 0; i < vbs.length; i++) {
						if (snapshot != null) {
							snapshot.add(vbs[i]);
						}
						System.out.println(vbs[i].toString());
					}
				}
				return true;
			}
			
		};
		synchronized (treeListener) {
			treeUtils.getSubtree(target, rootOID, null, treeListener);
			try {
				treeListener.wait();
			} catch (InterruptedException ex) {
				System.err.println("Tree retrieval interrupted: "
						+ ex.getMessage());
				Thread.currentThread().interrupt();
			}
		}
		return response;
	}
}
