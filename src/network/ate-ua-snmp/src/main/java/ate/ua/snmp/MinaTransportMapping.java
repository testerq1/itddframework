package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;

import org.snmp4j.TransportMapping;
import org.snmp4j.TransportStateReference;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.transport.AbstractTransportMapping;

import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.UAServer;

/**
 * 为了将SNMP4J 适配到 Mina，重写一个TransportMapping
 * @author xiaoyong.huang
 *
 */
public class MinaTransportMapping {
	
	public static TransportMapping createTransportMapping(AMinaUAImp ua){		
		final AMinaUAImp handler=ua;
		if(handler.get_transport().equalsIgnoreCase("tcp")){
			return new AbstractTransportMapping<TcpAddress>(){
				@Override
				public Class<? extends Address> getSupportedAddressClass() {
					return TcpAddress.class;
				}

				@Override
				public TcpAddress getListenAddress() {			
					return new TcpAddress(handler.get_host()+"/"+handler.get_port());
				}

				@Override
				public boolean isListening() {
					return handler.is_ready();
				}

				@Override
				public void sendMessage(TcpAddress address, byte[] message,
						TransportStateReference tmStateReference) throws IOException {
					try {
						handler.send_bytes(message);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				@Override
				public void close() throws IOException {
					handler.teardown();
				}

				@Override
				public void listen() throws IOException {
					handler.startup();
				}
			};
		}
		return new AbstractTransportMapping<UdpAddress>(){			
			@Override
			public Class<? extends Address> getSupportedAddressClass() {
				return UdpAddress.class;
			}

			@Override
			public UdpAddress getListenAddress() {			
				return new UdpAddress(handler.get_host()+"/"+handler.get_port());
			}

			@Override
			public boolean isListening() {
				Object tmp=handler.get_io_handler();
				return (tmp instanceof UAServer)&&((UAServer)tmp).is_ready();
			}
			
			@Override
			public void close() throws IOException {
				handler.teardown();
			}

			@Override
			public void listen() throws IOException {
				handler.startup();
			}

			@Override
			public void sendMessage(UdpAddress address, byte[] message,
					TransportStateReference tmStateReference) throws IOException {
				try {
					handler.send_bytes(message);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
		};
	}
	private MinaTransportMapping(){}
}
