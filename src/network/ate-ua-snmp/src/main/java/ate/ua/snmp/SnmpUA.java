package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.ScopedPDU;
import org.snmp4j.TransportMapping;
import org.snmp4j.TransportStateReference;
import org.snmp4j.asn1.BER;
import org.snmp4j.asn1.BER.MutableByte;
import org.snmp4j.asn1.BERInputStream;
import org.snmp4j.asn1.BEROutputStream;
import org.snmp4j.mp.MPv1;
import org.snmp4j.mp.MPv2c;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.MessageProcessingModel;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.mp.StateReference;
import org.snmp4j.security.AuthMD5;
import org.snmp4j.security.AuthSHA;
import org.snmp4j.security.AuthenticationProtocol;
import org.snmp4j.security.ByteArrayWindow;
import org.snmp4j.security.DecryptParams;
import org.snmp4j.security.Priv3DES;
import org.snmp4j.security.PrivAES128;
import org.snmp4j.security.PrivAES192;
import org.snmp4j.security.PrivAES256;
import org.snmp4j.security.PrivDES;
import org.snmp4j.security.PrivacyProtocol;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModel;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.security.UsmSecurityParameters;
import org.snmp4j.security.UsmUser;
import org.snmp4j.security.UsmUserEntry;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.UdpAddress;

import ate.AbstractURIHandler;
import ate.ua.AbstractPacketUAImp;
import ate.ua.UAOption;
import ate.ua.mina.AMinaUAImp;

/**
 * 使用Mina decode/encode的机制实现PDU的处理,支持C/S模式，
 * 目前对V3版本支持不太完整，主要是v3协商阶段Reporter消息的自动处理没有实现
 * 
 * 有一个默认v3用户: md5/des ateauth/atepriv
 * @author Administrator
 *
 */
public class SnmpUA extends AMinaUAImp {
	public enum AUTH {
		MD5(AuthMD5.ID), SHA(AuthSHA.ID);
		public static String getName(OID oid) {
			if (oid.equals(SHA.ID))
				return "sha";
			if (oid.equals(MD5.ID))
				return "md5";
			return null;
		}

		private OID ID;

		private AUTH(OID oid) {
			this.ID = oid;
		}
	}
	public enum PRIV {
		AES128(PrivAES128.ID), AES192(PrivAES192.ID), AES256(PrivAES256.ID), DES(
				PrivDES.ID), DESEDE(Priv3DES.ID);
		public static String getName(OID oid) {
			if (oid.equals(DES.ID))
				return "des";
			if (oid.equals(DESEDE))
				return "3des";
			if (oid.equals(AES128))
				return "aes128";
			if (oid.equals(AES192))
				return "aes192";
			if (oid.equals(AES256))
				return "aes256";
			return null;
		}

		OID ID;

		private PRIV(OID oid) {
			this.ID = oid;
		}
	}
	class Snmp4jFilter extends IoFilterAdapter {
//		@Override
//		public void messageSent(NextFilter nextFilter, IoSession session,
//				WriteRequest writeRequest) throws Exception {
//			//dispatcher
//		}

		@Override
		public void messageReceived(NextFilter nextFilter, IoSession session,
				Object message) throws Exception {
			IoBuffer in = (IoBuffer) message;

			if (transportMapping.getSupportedAddressClass().equals(
					TcpAddress.class)) {
				// dispatcher.processMessage(transportMapping, address, buf,
				// tmStateReference)
			} else {
				TransportStateReference stateReference = new TransportStateReference(
						transportMapping, transportMapping.getListenAddress(),
						null, SecurityLevel.undefined, SecurityLevel.undefined,
						false, session);
				InetSocketAddress inetAddress = (InetSocketAddress) session
						.getRemoteAddress();
				dispatcher.processMessage(transportMapping, new UdpAddress(
						inetAddress.getAddress(), inetAddress.getPort()), in
						.buf(), stateReference);
			}

//			if (in.remaining() < 0)
//				return;
//
//			BERInputStream wholeMsg = new BERInputStream(in.buf());
//			BER.MutableByte type = new BER.MutableByte();
//			int length = BER.decodeHeader(wholeMsg, type);
//			if (type.getValue() != BER.SEQUENCE) {
//				return;
//			}
//
//			long lengthOfLength = wholeMsg.getPosition();
//			wholeMsg.reset();
//			wholeMsg.mark(length);
//			if (wholeMsg.skip(lengthOfLength) != lengthOfLength) {
//				return ;
//			}
//
//			Integer32 version = new Integer32();
//			version.decodeBER(wholeMsg);
			nextFilter.messageReceived(session, message);
		}
	}

	class SnmpDecoder extends CumulativeProtocolDecoder {

		private void decodeV3Header(BERInputStream wholeMsg,
				UsmSecurityParameters usmSecurityParameters, APDU apdu)
				throws IOException {
			Integer32 msgID = new Integer32(0);
			Integer32 msgMaxSize = new Integer32(Integer.MAX_VALUE);
			OctetString msgFlags = new OctetString(new byte[1]);
			Integer32 securityModel = new Integer32();
			msgID.decodeBER(wholeMsg);
			msgMaxSize.decodeBER(wholeMsg);
			msgFlags.decodeBER(wholeMsg);
			securityModel.decodeBER(wholeMsg);

			apdu.msgID = msgID.getValue();
			apdu.msgMaxSize = msgMaxSize.getValue();
			apdu.msgFlags = (byte) (msgFlags.getValue()[0] & 0xFF);

			apdu.reportableFlag = ((apdu.msgFlags & 0x04) > 0);

			switch (apdu.msgFlags & 0x03) {
			case 3:
				apdu.securityLevel = SecurityLevel.AUTH_PRIV;
				break;
			case 0:
				apdu.securityLevel = SecurityLevel.NOAUTH_NOPRIV;
				break;
			case 1:
				apdu.securityLevel = SecurityLevel.AUTH_NOPRIV;
				break;
			default:
				apdu.securityLevel = SecurityLevel.NOAUTH_NOPRIV;
				log.debug("Invalid message (illegal msgFlags)");
			}

			int secParametersPosition = (int) wholeMsg.getPosition();
			usmSecurityParameters.decodeBER(wholeMsg);
			usmSecurityParameters
					.setSecurityParametersPosition(secParametersPosition);

			apdu.securityName = new String(usmSecurityParameters.getUserName()
					.getValue());
			apdu.securityEngineID = new String(
					usmSecurityParameters.getAuthoritativeEngineID());
			wholeMsg.reset();
		}

		private BERInputStream decrypt(BERInputStream wholeMsg,
				UsmSecurityParameters usmSecurityParameters, APDU apdu)
				throws IOException {
			AuthenticationProtocol auth = securityProtocols
					.getAuthenticationProtocol(apdu.authProtID);
			PrivacyProtocol priv = securityProtocols
					.getPrivacyProtocol(apdu.privProtID);
			UsmUserEntry user = getUser(usmSecurityParameters.getUserName());
			apdu.authProtID = user.getUsmUser().getAuthenticationProtocol();
			apdu.privProtID = user.getUsmUser().getPrivacyProtocol();
			int scopedPDUPosition = usmSecurityParameters
					.getScopedPduPosition();
			byte[] message = buildMessageBuffer(wholeMsg);
			BEROutputStream scopedPDU = new BEROutputStream();
			if ((usmSecurityParameters.getUserName().length() > 0)
					|| (apdu.securityLevel > SecurityLevel.NOAUTH_NOPRIV)) {
				if (apdu.securityLevel >= SecurityLevel.AUTH_NOPRIV) {
					int authParamsPos = usmSecurityParameters
							.getAuthParametersPosition()
							+ usmSecurityParameters
									.getSecurityParametersPosition();
					boolean authentic = auth
							.isAuthentic(
									user.getAuthenticationKey(),
									message,
									0,
									message.length,
									new ByteArrayWindow(
											message,
											authParamsPos,
											AuthenticationProtocol.MESSAGE_AUTHENTICATION_CODE_LENGTH));
					if (!authentic) {
						log.debug("Wrong digest -> authentication failure: "
								+ usmSecurityParameters
										.getAuthenticationParameters()
										.toHexString());
					}
					// check time
					// int status = timeTable.checkTime(new
					// UsmTimeEntry(securityEngineID,
					// usmSecurityParameters.getAuthoritativeEngineBoots(),
					// usmSecurityParameters.getAuthoritativeEngineTime()));
				}
				if (apdu.securityLevel >= SecurityLevel.AUTH_PRIV) {
					OctetString privParams = usmSecurityParameters
							.getPrivacyParameters();
					DecryptParams decryptParams = new DecryptParams(
							privParams.getValue(), 0, privParams.length());
					try {
						int scopedPDUHeaderLength = message.length
								- scopedPDUPosition;
						ByteBuffer bis = ByteBuffer.wrap(message,
								scopedPDUPosition, scopedPDUHeaderLength);
						BERInputStream scopedPDUHeader = new BERInputStream(bis);
						long headerStartingPosition = scopedPDUHeader
								.getPosition();
						int scopedPDULength = BER.decodeHeader(scopedPDUHeader,
								new MutableByte());
						int scopedPDUPayloadPosition = scopedPDUPosition
								+ (int) (scopedPDUHeader.getPosition() - headerStartingPosition);
						scopedPDUHeader.close();
						// early release pointer:
						scopedPDUHeader = null;
						byte[] scopedPduBytes = priv.decrypt(message,
								scopedPDUPayloadPosition, scopedPDULength, user
										.getPrivacyKey(), usmSecurityParameters
										.getAuthoritativeEngineBoots(),
								usmSecurityParameters
										.getAuthoritativeEngineTime(),
								decryptParams);
						ByteBuffer buf = ByteBuffer.wrap(scopedPduBytes);
						scopedPDU.setFilledBuffer(buf);
					} catch (Exception ex) {
						log.debug("Decryption error: " + ex.getMessage());
					}
				} else {
					int scopedPduLength = message.length - scopedPDUPosition;
					ByteBuffer buf = ByteBuffer.wrap(message,
							scopedPDUPosition, scopedPduLength);
					scopedPDU.setFilledBuffer(buf);
				}
			} else {
				int scopedPduLength = message.length - scopedPDUPosition;
				ByteBuffer buf = ByteBuffer.wrap(message, scopedPDUPosition,
						scopedPduLength);
				scopedPDU.setFilledBuffer(buf);
			}
			return new BERInputStream(scopedPDU.rewind());
		}

		@Override
		protected boolean doDecode(IoSession session, IoBuffer in,
				ProtocolDecoderOutput out) throws Exception {
			if (in.remaining() < 0)
				return false;

			BERInputStream wholeMsg = new BERInputStream(in.buf());
			BER.MutableByte type = new BER.MutableByte();
			int length = BER.decodeHeader(wholeMsg, type);
			if (type.getValue() != BER.SEQUENCE) {
				return false;
			}

			long lengthOfLength = wholeMsg.getPosition();
			wholeMsg.reset();
			wholeMsg.mark(length);
			if (wholeMsg.skip(lengthOfLength) != lengthOfLength) {
				return false;
			}

			Integer32 version = new Integer32();
			version.decodeBER(wholeMsg);

			APDU apdu = new APDU();
			apdu.version = version.getValue();
			if (version.getValue() == SnmpConstants.version1)
				apdu.pdu = new PDUv1();
			else if (version.getValue() == SnmpConstants.version2c)
				apdu.pdu = new PDU();
			else if (version.getValue() == SnmpConstants.version3)
				apdu.pdu = new ScopedPDU();
			else
				return false;

			if (version.getValue() < SnmpConstants.version3) {
				OctetString securityName = new OctetString();
				securityName.decodeBER(wholeMsg);
				apdu.community = new String(securityName.getValue());
			} else {
				UsmSecurityParameters usmSecurityParameters = (UsmSecurityParameters) usm
						.newSecurityParametersInstance();
				decodeV3Header(wholeMsg, usmSecurityParameters, apdu);
				wholeMsg = decrypt(wholeMsg, usmSecurityParameters, apdu);
			}
			apdu.pdu.decodeBER(wholeMsg);
			out.write(apdu);
			return true;
		}
	}

	class SnmpEncoder extends ProtocolEncoderAdapter {
		private void build(APDU pdu) {
			if (pdu.securityName == null) {
				if (pdu.version == SnmpConstants.version3) {
					pdu.securityName = securityName;
					return;
				}
				if (pdu.pdu.getType() == PDU.SET)
					pdu.securityName = writeCommunity;
				else
					pdu.securityName = readCommunity;
			}
		}

		@Override
		public void encode(IoSession session, Object message,
				ProtocolEncoderOutput out) throws Exception {
			if (!(message instanceof APDU)) {
				log.warn("message isn't APDU!");
				return;
			}

			APDU apdu = (APDU) message;
			build(apdu);

			MessageProcessingModel mp = dispatcher
					.getMessageProcessingModel(apdu.version);
			BEROutputStream outgoingMessage = new BEROutputStream();
			UsmSecurityParameters usmSecurityParameters = (UsmSecurityParameters) usm
					.newSecurityParametersInstance();
			StateReference stateReference = new StateReference();
			stateReference.setMsgID(apdu.msgID);

			int maxSecParamsOverhead = usmSecurityParameters
					.getBERMaxLength(apdu.securityLevel);
			int status = 0;
			if (apdu.isresponse) {
				status = mp.prepareResponseMessage(3, apdu.msgMaxSize,
						SecurityModel.SECURITY_MODEL_USM,
						apdu.securityName.getBytes(),
						getSecurityLevel(apdu.securityName), apdu.pdu,
						apdu.msgMaxSize - maxSecParamsOverhead, stateReference,
						null, outgoingMessage);

			} else {
				GenericAddress destAddress = new GenericAddress();
				status = mp.prepareOutgoingMessage(getAddress(),
						apdu.msgMaxSize, 3, SecurityModel.SECURITY_MODEL_USM,
						apdu.securityName.getBytes(),
						getSecurityLevel(apdu.securityName), apdu.pdu, false,
						null, destAddress, outgoingMessage, null);
			}
			System.out.println(status);
			ByteBuffer buffer = outgoingMessage.getBuffer();
			buffer.flip();

			out.write(IoBuffer.wrap(buffer));
		}

		private ByteBuffer encodeGlobalData(APDU apdu) throws IOException {
			Integer32 msgID = new Integer32(apdu.msgID);
			Integer32 msgMaxSize = new Integer32(apdu.msgMaxSize);
			OctetString msgFlags = new OctetString(new byte[] { apdu.msgFlags });
			Integer32 securityModel = new Integer32(apdu.securityModel);

			int length = msgID.getBERLength() + msgMaxSize.getBERLength()
					+ msgFlags.getBERLength() + securityModel.getBERLength();

			int globalLen = length + BER.getBERLengthOfLength(length) + 1;
			ByteBuffer globalDataBuffer = ByteBuffer.allocate(globalLen);
			BEROutputStream globalDataOutputStream = new BEROutputStream(
					globalDataBuffer);
			BER.encodeHeader(globalDataOutputStream, BER.SEQUENCE, length);
			msgID.encodeBER(globalDataOutputStream);
			msgMaxSize.encodeBER(globalDataOutputStream);
			msgFlags.encodeBER(globalDataOutputStream);
			securityModel.encodeBER(globalDataOutputStream);

			return globalDataBuffer;
		}
	}

	protected static byte[] buildMessageBuffer(BERInputStream scopedPDU)
			throws IOException {
		scopedPDU.mark(16);
		int readLengthBytes = (int) scopedPDU.getPosition();
		BER.MutableByte mutableByte = new BER.MutableByte();
		int length = BER.decodeHeader(scopedPDU, mutableByte);
		readLengthBytes = (int) scopedPDU.getPosition() - readLengthBytes;
		byte[] buf = new byte[length + readLengthBytes];
		scopedPDU.reset();

		int offset = 0;
		int avail = scopedPDU.available();
		while ((offset < buf.length) && (avail > 0)) {
			int read = scopedPDU.read(buf, offset, buf.length - offset);
			if (read < 0) {
				break;
			}
			offset += read;
		}
		return buf;
	}

	

	public String contextEngineID;

	public String contextName;

	protected MessageDispatcherImpl dispatcher;

	byte[] engineid = MPv3.createLocalEngineID();

	public int maxRepetitions = 1;

	public int nonRepeaters = 1;
	public String readCommunity = "public";
	SecurityModels securityModels = SecurityModels.getInstance();
	// v3 ，默认用户名
	public String securityName = "ate";
	SecurityProtocols securityProtocols = SecurityProtocols.getInstance();
	TransportMapping transportMapping;
	USM usm;
	int version = SnmpConstants.version2c;
	public String writeCommunity = "private";
	@Override
	public void add_default_filter() {
		add_last_iofilter(this.scheme,new Snmp4jFilter());
	}

	/**
	 * addUsmUser("SHADES",SnmpUA.AUTH.MD5,"SHADESAuthPassword",SnmpUA.PRIV.DES,
	 * "SHADESPrivPassword"); addUsmUser("SHADES",SnmpUA,null,"",null,"");
	 * 
	 * @param securityName
	 */
	public void addUsmUser(String securityName, AUTH auth, String authPasswd,
			PRIV priv, String privPasswd) {
		UsmUser user = new UsmUser(new OctetString(securityName),
				auth != null ? auth.ID : null, new OctetString(authPasswd),
				priv != null ? priv.ID : null, new OctetString(privPasswd));
		usm.addUser(user.getSecurityName(), usm.getLocalEngineID(), user);
	}

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option,value);
		
		if(option==UA_TYPE){
			transportMapping = MinaTransportMapping
					.createTransportMapping(this);
			add_last_iofilter("snmp", new Snmp4jFilter());
			// new ProtocolCodecFilter(new SnmpEncoder(), new SnmpDecoder()));
	
			usm = new USM(securityProtocols, new OctetString(engineid), 0);
			securityModels.addSecurityModel(usm);
			securityProtocols.addDefaultProtocols();
	
			addUsmUser(securityName, SnmpUA.AUTH.MD5, "ateauthpass",
					SnmpUA.PRIV.DES, "ateprivpass");
	
			dispatcher = new MessageDispatcherImpl();
			dispatcher.addMessageProcessingModel(new MPv1());
			dispatcher.addMessageProcessingModel(new MPv2c());
			dispatcher.addMessageProcessingModel(new MPv3(engineid));
		}
		return this;
	}

	public APDU createAPDU(int version, int pduType) {
		APDU apdu = new APDU();
		apdu.version = version;
		PDU pdu = null;
		if (version == SnmpConstants.version3) {
			pdu = new ScopedPDU();
			ScopedPDU scopedPDU = (ScopedPDU) pdu;
			if (contextEngineID != null) {
				scopedPDU.setContextEngineID(new OctetString(contextEngineID));
			}
			if (contextName != null) {
				scopedPDU.setContextName(new OctetString(contextName));
			}
		} else {
			if (pduType != PDU.V1TRAP) {
				pdu = new PDU();
			}
		}
		pdu.setType(pduType);
		if (pdu.getType() == PDU.GETBULK) {
			pdu.setMaxRepetitions(maxRepetitions);
			pdu.setNonRepeaters(nonRepeaters);
		}
		apdu.pdu = pdu;
		return apdu;
	}

	@Override
	public String get_default_schema() {		
		return "snmp1";
	}

	private Address getAddress() {
		Address address;

		String transportAddress = host+"/"+port;
		
		if (this.transport.equalsIgnoreCase("udp")) {
			address = new UdpAddress(transportAddress);
		} else
			address = new TcpAddress(transportAddress);

		return address;
	}

	private int getSecurityLevel(String securityName) {
		UsmUser user = usm.getUser(usm.getLocalEngineID(),
				new OctetString(securityName)).getUsmUser();
		int level = 0;
		if (user.getPrivacyProtocol() != null)
			level += 1;
		if (user.getAuthenticationProtocol() != null)
			level += 2;
		return level;
	}

	private UsmUserEntry getUser(OctetString securityName) {
		return usm.getUser(usm.getLocalEngineID(), securityName);
	}

	@Override
	public boolean message_matched(Object msg, HashMap hm) {
		return true;
	}
}
