package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashMap;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.PDU;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;

public class PDUUtil {
	static Logger log=LoggerFactory.getLogger(PDUUtil.class);	
	
	PDU inPdu;
	MibFactory mf;
	
	Vector<VariableBinding> vbs;
	public PDUUtil(MibFactory mf){
		this.mf=mf;
	}
	
	public PDUUtil(PDU pdu,MibFactory mf){
		this.inPdu=pdu;
		this.mf=mf;
	}
	public void setPDU(PDU pdu){
		this.inPdu=pdu;
	}
	protected void addVB(VariableBinding vb){
		
		this.vbs.add(vb);
	}
	protected void addVBs(Vector<VariableBinding> vbs){
		this.vbs.addAll(vbs);
	}
	
	public boolean containAll(HashMap<String,String> map){
		int cnt=0;
		for(String key:map.keySet()){			
			String value=map.get(key);
			if(contains(key,value))
				cnt++;
			else
				break;
		}
		return cnt==map.keySet().size();
	}
	public boolean contains(MibNode mn,String value){
		for(VariableBinding tmp:getVBs()){
			if(tmp.getOid().toString().contains(mn.getOid()+".")){
				if(value==null)
					return true;
				Variable v1=tmp.getVariable();
				Variable v2=SNMPUtil.createVariable(mn.getType(), value);
				return v1.equals(v2);
			}
		}
		return false;
	}
	public boolean contains(String name,String value){		
		MibNode mn=mf.getNode(name);		
		return contains(mn,value);
	}
	public boolean containsOid(String oid){
		for(VariableBinding tmp:getVBs()){
			if(tmp.getOid().toString().contains(oid+"."))				
				return true;			
		}
		return false;
	}
	
	public boolean errorStatusIs(String ec){	
		String _ec=null;
		if(ec.toLowerCase().startsWith("err")){
			_ec=SNMPUtil.ec_enum.getProperty(ec);
		}else{
			_ec=""+inPdu.getErrorStatus();
		}
		return ec.equalsIgnoreCase("ec");			
	}
	public boolean errorStatusIs(int ec){	
		return ec==inPdu.getErrorStatus();			
	}
	public String get1stVBOID(){		
		return getVBs().get(0).getOid().toString();			
	}
	
	public int getErrorStatus(){
		return this.inPdu.getErrorStatus();
	}
	public PDU getPDU(){
		return this.inPdu;
	}
	/*
	*get Variable value for 
	*name:Variable name
	*/
	public String getVB(String name){
		MibNode mn=mf.getNode(name);
		for(VariableBinding tmp:getVBs()){
			if(tmp.getOid().toString().startsWith(mn.getOid()+".")){
				return tmp.getVariable().toString();
			}
		}
		return null;
	}
	
	public Vector<VariableBinding> getVBs(){
		if(vbs!=null)
			return this.vbs;
		if(inPdu==null)
			return null;
		this.vbs=(Vector<VariableBinding>)inPdu.getVariableBindings();
		return vbs;
	}
	public boolean isok(){
		return inPdu!=null&&inPdu.getErrorStatus()==0&&inPdu.size()>0;
	}
	/*
	*assure whether variable bindings  equal?
	*/	
	public boolean vbsEquals(PDU pdu){
		if(pdu==null||inPdu==null||pdu.size()!=inPdu.size())
			return false;
		return this.getVBs().containsAll(pdu.getVariableBindings());
	}
	
}
