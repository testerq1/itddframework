package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.PDU;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.Null;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.SMIConstants;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.UnsignedInteger32;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;

public class APDU {
	protected static Logger log = LoggerFactory.getLogger(APDU.class);
	public static Variable createVariable(String type, String value) {
		type =type.toLowerCase();
		if(type.startsWith("int")||type.startsWith("rowstatus")){			
			return new Integer32(Integer.parseInt(value));
		}
		if(type.startsWith("counter32")||
				type.startsWith("uinteger32")||
				type.startsWith("gauge32")||
				type.startsWith("opaque")){
			return new UnsignedInteger32(Long.parseLong(value));
		}
		if(type.startsWith("octet")||
				type.startsWith("displaystring"))	
			return new OctetString(value);
		if(type.startsWith("hex"))
			return OctetString.fromString(value, ':', 16);	
//		case 'd':
//			variable = OctetString.fromString(value, '.', 10);
//			break;
		if(type.startsWith("bit"))
			return OctetString.fromString(value, ' ', 2);	
		if(type.startsWith("null"))
			return new Null();
		if(type.startsWith("object"))
			return new OID(value);
		if(type.startsWith("timeticks"))
			return new TimeTicks(Long.parseLong(value));
		if(type.startsWith("ip"))
			return new IpAddress(value);
			
		throw new IllegalArgumentException("Variable type " + type
					+ " not supported");
	}
	public static void main(String[] args){
		APDU p1=new APDU();
		p1.authProtID=new OID("1");
		p1.pdu=new PDU();
		APDU p2=p1.clone();
		System.out.println();			
	}
	public int version=SnmpConstants.version2c;

	public String community;
	public PDU pdu;
	// V3 header
	public String engineId;
	public int msgID;
	public int msgMaxSize=(1 << 16) - 1;
	public byte msgFlags;
	public String securityName;
	public String securityEngineID;
	
	public String contextEngineID;
	public String contextName;
	public int securityModel;
	public int securityLevel;
	boolean reportableFlag;
	OID authProtID;	
	OID privProtID;		
	boolean isresponse;
	
	private int index=-1;
	public APDU(){}

	public APDU(PDU pdu){
		this.pdu=pdu;
	}
	private Object _v(Variable var){
		if(var==null)
			return null;
		switch(var.getSyntax()){
			case SMIConstants.SYNTAX_COUNTER32:
			case SMIConstants.SYNTAX_INTEGER32:
			case SMIConstants.SYNTAX_TIMETICKS:	
			case SMIConstants.SYNTAX_GAUGE32:
				return var.toInt();
			case SMIConstants.SYNTAX_COUNTER64:			
				return var.toLong();		
			case SMIConstants.SYNTAX_OBJECT_IDENTIFIER:	
			case SMIConstants.SYNTAX_OCTET_STRING:
			case SMIConstants.SYNTAX_IPADDRESS:
				return var.toString();
			default:
				log.error("Syntax not support："+var.getSyntaxString());
				return null;
		}		
	}
	public APDU addBitString(String oid,String value){
		addVB(oid,"bit",value);
		return this;
	}
	public APDU addHexString(String oid,String value){
		addVB(oid,"hex",value);
		return this;
	}
	public APDU addInteger32(String oid,String value){
		addVB(oid,"int",value);
		return this;
	}
	public APDU addIPAddress(String oid,String value){
		addVB(oid,"ip",value);
		return this;
	}
	public APDU addObject(String oid,String value){
		addVB(oid,"object",value);
		return this;
	}
	
	public APDU addOctetString(String oid,String value){
		addVB(oid,"octet",value);
		return this;
	}
	
	public APDU addOid(String oid){
		addVB(oid,null,null);
		return this;
	}
	
	public APDU addTimeticks(String oid,String value){
		addVB(oid,"timeticks",value);
		return this;
	}
	
	public APDU addUInteger32(String oid,String value){
		addVB(oid,"uinteger32",value);
		return this;
	}
	
	private APDU addVB(String oid,String type,String value){
		VariableBinding vb=new VariableBinding(new OID(oid));
		if(value!=null)
			vb.setVariable(createVariable(type,value));
		pdu.add(vb);
		return this;
	}
	
	public APDU clone(){
		APDU clone=new APDU();
		
		if(this.pdu!=null)
			clone.pdu=(PDU) this.pdu.clone();
		if(authProtID!=null)
			clone.authProtID=(OID) authProtID.clone();
		if(privProtID!=null)
			clone.privProtID=(OID) privProtID.clone();
		clone.engineId         =engineId;        
		clone.msgID            =msgID;           
		clone.msgMaxSize       =msgMaxSize;      
		clone.msgFlags         =msgFlags;        
		clone.securityName     =securityName;    
		clone.securityEngineID =securityEngineID;
		clone.securityModel    =securityModel;   
		clone.securityLevel    =securityLevel;   
		clone.reportableFlag   =reportableFlag;  		
		return clone;
	}
	
	public Object get(String oid){
		return _v(pdu.getVariable(new OID(oid)));
	}
	
	public String getAuthProt() {
		return SnmpUA.AUTH.getName(authProtID);
	}
	
	public String getPrivProt() {
		return SnmpUA.PRIV.getName(privProtID);
	}
	
	public Object next(){
		index++;
		if(index>pdu.getVariableBindings().size())
			return null;
		
		return v();
	}
	public void reset(){
		index=-1;
	}
	
	public Object v(){
		return _v(pdu.get(index).getVariable());
	}
}
