package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import net.percederberg.mibble.Mib;
import net.percederberg.mibble.MibLoader;
import net.percederberg.mibble.MibLoaderException;
import net.percederberg.mibble.MibSymbol;
import net.percederberg.mibble.MibValue;
import net.percederberg.mibble.MibValueSymbol;
import net.percederberg.mibble.value.ObjectIdentifierValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.rm.dev.Host;
import ate.util.GenData0;

/**
 * 管理所有mib节点 目前没有做文件管理，每调用一次load，就重新hash一次该文件中所有leaf
 *  
 * @author ravi huang
 * 
 */
public class MibFactory {
	private final MibLoader loader = new MibLoader();	
	static MibFactory mf;
	private final HashMap<String, MibNode> leafs = new HashMap<String, MibNode>();
	private final Logger log = LoggerFactory.getLogger(MibFactory.class);
	
	private MibGroup mg;	

	private final HashMap<String, MibGroup> mgs=new HashMap<String, MibGroup>();
	
	private final ArrayList<MibNode> tableColumn=new ArrayList<MibNode>();
	private final String mib_home;
	
	public void reLoad(){
		try {
			mf.load(mib_home);		
		} catch(MibLoaderException e){
			e.getLog().printTo(System.out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args)throws Exception{
		System.setProperty("TTOOL_HOME",System.getProperty("user.dir"));
		new MibFactory();		
	} 

	/**
	 * @param filename
	 *            支持本地文件/目录，共享文件/目录，http,ftp文件
	 */
	private MibFactory() throws Exception{
		String mib_home=System.getProperty("MIB_HOME");
		if(mib_home==null)
			mib_home=Host.HOME + File.separator
			+ "Scripts"+File.separator+"mibs";
		load(mib_home);
		this.mib_home=mib_home;	
	}
	public static void destroy(){
		mf=null;
	}
	public static MibFactory build()throws Exception{
		if(mf==null)
			mf=new MibFactory();
		return mf;
	}
	public MibNode getNode(String nodeName) {
		return leafs.get(nodeName);
	}
	public Mib loadMib(String filename) throws Exception{		
		if (GenData0.isUrl(filename))
			return loader.load(new InputStreamReader(GenData0.getInputStream(filename))); 
		File file=new File(filename);
		if(!file.exists())
			throw new Exception("指定文件不存在："+filename);
		if(file.isFile())
			return loader.load(file);
		return null;		
	}
	public void unLoadMib(Mib mib) throws Exception{		
		loader.unload(mib);
		
	}
	/**
	 * @param filename
	 *            支持本地文件/目录，共享文件/目录，http,ftp文件
	 * @return
	 */
	public void load(String filename) throws Exception{		
		Mib mib = null;
		File file=new File(filename);
		if (GenData0.isUrl(filename))
			mib = loader.load(new InputStreamReader(GenData0.getInputStream(filename)));
		else if(file.isFile())
			mib = loader.load(file);
		else
			for(File tmp:file.listFiles()){		
				if(tmp.isHidden())
					continue;
				if(tmp.isFile()){
					String fname=tmp.getName();
					if(fname.toLowerCase().endsWith(".my")||fname.toLowerCase().endsWith(".mib"))
						load(tmp.getAbsolutePath());
				}
				else load(tmp.getAbsolutePath());
			}
		if(mib==null)
			return;
		if (mib.getLog().warningCount() > 0)
			mib.getLog().printTo(System.err);
		if (mib.getLog().errorCount() > 0) {
			mib.getLog().printTo(System.out);
			throw new Exception("mib 文件不合法");
		}

		if (loader.getAllMibs().length == 0) {
			log.warn("no mib loaded");
			return;
		}
		MibSymbol symbol = null;
		MibValue value = null;
		ObjectIdentifierValue root = null;
		Iterator iter = mib.getAllSymbols().iterator();
		while (iter.hasNext()) {
            symbol = (MibSymbol) iter.next();
            if (symbol instanceof MibValueSymbol) {
                value = ((MibValueSymbol) symbol).getValue();
                if (value instanceof ObjectIdentifierValue)
					root = (ObjectIdentifierValue) value;
            }
            MibValueSymbol symbol1 = root.getSymbol();
    		if (symbol1.isScalar()||symbol1.isTableColumn()) {
    			MibNode mn = new MibNode(root);
    			leafs.put(mn.getName(), mn);
    			//System.out.println(mn.getName());
    		}
		}			
	}

}
