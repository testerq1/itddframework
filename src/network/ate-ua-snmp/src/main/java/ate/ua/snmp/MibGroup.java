package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.percederberg.mibble.snmp.SnmpIndex;
import net.percederberg.mibble.snmp.SnmpObjectType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.PDU;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;

import ate.util.GenData0;


public class MibGroup{
	protected static Logger log=LoggerFactory.getLogger(MibGroup.class);
	protected ArrayList<ArrayList<String>> error_data;
	protected HashMap<String,ArrayList<String>> deft_datas;
	protected ArrayList<String> deftRow;
	protected String deftIndex;
	protected String rowstatus;
	
	protected ArrayList<String> all_nodes;
	protected ArrayList<String> id_nodes;
	protected ArrayList<String> non_id_nodes;
	protected ArrayList<String> writeable_nodes;	
	protected MibFactory mf;
	private boolean implied;
	
	public MibGroup(MibFactory mf){
		this.mf=mf;
	}
	public void presetDeftData(HashMap<String,Object> hm)throws Exception{
		all_nodes=new ArrayList<String>();
		error_data=new ArrayList<ArrayList<String>>();
		deft_datas=new HashMap<String,ArrayList<String>>();
		for(Map.Entry<String, Object> en:hm.entrySet()){
			ArrayList<ArrayList<String>> all= GenData0.expandDatas(en);
			String key=en.getKey();
			all_nodes.add(key);
			MibNode mn=mf.getNode(key);
			ArrayList<String> al=(ArrayList<String>)mn.getValues().clone();
			if(al==null)
				al=new ArrayList<String>();
			
			for(ArrayList<String> tve:all){
				if(tve.get(1)==null||tve.size()==0){
					
				}else if(tve.get(2).equals("0")){
					if(tve.get(1).equals("&min")){
						al.add(al.get(0));
					}else if(tve.get(1).equals("&max")){
						al.add(al.get(al.size()-1));
					}else{
						al.add(tve.get(1));
					}					
				}else{
					error_data.add(tve);
				}
			}		
			if(deft_datas.get(key)==null){
				deft_datas.put(key, al);				
			}			
		}
		this.initNodes();
		log.info("表初始化完成");
	}
	
	public ArrayList<String> getDeftRow(){
		return (ArrayList<String>)this.deftRow;
	}
	
	public String getRowstatus(){
		return this.rowstatus;
	}
	
	public String genIndex(){
		return this.genIndex(deftRow);		
	}
	public String genIndex(HashMap<String,String> datas){
		ArrayList<String> al= new ArrayList<String>();
		for(String tmp:this.id_nodes){
			al.add(datas.get(tmp));
		}
		return this.genIndex(al);
	}
	public String genIndex(ArrayList<String> al){
		return genIndex(al.toArray(new String[al.size()]));		
	}
	public String getKey(){
		return mf.getNode(all_nodes.get(0)).getParent().getName();
	}
	public ArrayList<String> buildRowData(ArrayList<String> al, HashMap<String,String> merge){
		for(String key:merge.keySet()){
			int i=all_nodes.indexOf(key);
			al.set(i, merge.get(key));
		}
		return al;
	}
	
	public void initNodes(){
		Collections.sort(all_nodes, new Comparator<String>(){
			public int compare(String o1, String o2){
				return mf.getNode(o1).compareTo(mf.getNode(o2));
			}
		});
		id_nodes=new ArrayList<String>();
		non_id_nodes=new ArrayList<String>();
		writeable_nodes=new ArrayList<String>();	
		this.deftRow=new ArrayList<String>();
		
		MibNode mn=mf.getNode(all_nodes.get(0));
		 Object pp=mn.getParent().getNativeOID().getSymbol().getType();
		 
		 if(pp instanceof SnmpObjectType){
			 SnmpObjectType entry=(SnmpObjectType)pp;
			 ArrayList<SnmpIndex> ids=entry.getIndex();
			 for(SnmpIndex tmp:ids){
				 this.id_nodes.add(tmp.getValue().getName());				 
			 }
			 this.implied=ids.get(ids.size()-1).isImplied();
		 }
		 
		for(int i=0;i<all_nodes.size();i++){
			String node=all_nodes.get(i);
			MibNode tmp=mf.getNode(node);
			
			ArrayList<String> al=this.deft_datas.get(node);
			this.deftRow.add(al.get(al.size()-1));
			
			if(tmp.getType().equalsIgnoreCase("rowstatus")){
				this.rowstatus=node;
				continue;
			}
			
			if(this.id_nodes.indexOf(node)<0){
				this.non_id_nodes.add(node);
			}	
			
			if(tmp.isWriteable()){
				this.writeable_nodes.add(node);
			}			
		}
		this.deftIndex=this.genIndex();
	}
	public HashMap<String,String> genWritableMap(String ... paras){
		HashMap<String,String> hm =new HashMap<String,String>();
		int len=Math.min(paras.length, this.writeable_nodes.size());
		for(int i=0;i<len;i++){
			hm.put(this.writeable_nodes.get(i), paras[i]);
		}
		return hm;		
	}
	
	public String genIndex(String ...paras){
		String index="";
		int len=Math.min(paras.length, this.id_nodes.size());
		for(int i=0;i<len;i++){
			String id=paras[i];
			if(id==null){
				continue;
			}
			MibNode tmp=mf.getNode(id_nodes.get(i));
			if(id.equalsIgnoreCase("&min")){
				ArrayList<String> al = tmp.getValues();
				id=al.get(0);
			}else if(id.equalsIgnoreCase("&max")){
				ArrayList<String> al = tmp.getValues();
				id=al.get(al.size()-1);
			}
			
			if(tmp.getType().toLowerCase().contains("string")){
				if(id==null||id.length()==0){
					index+=(".1.0");
					continue;
				}
				if(this.implied){
					if(tmp.equals(id_nodes.get(id_nodes.size()-1)))
						index+=("."+id.length());
				}
				index+=(".'"+id+"'");
			}else{
				index+=("."+id);
			}
			log.debug(tmp.getName()+"="+id);			
		}
		index=index.substring(1);
		OID oid=new OID(index);
		log.debug("生成索引："+oid.toString());
		return oid.toString();
	}
	
	public HashMap<String,String> genNonIdMap(String ... paras){
		HashMap<String,String> hm =new HashMap<String,String>();
		int len=Math.min(paras.length, this.non_id_nodes.size());
		for(int i=0;i<len;i++){
			hm.put(this.non_id_nodes.get(i), paras[i]);
		}
		return hm;		
	}
	public void printPDUInfo(PDU pdu){
		if(pdu==null){
			log.debug("pdu is null");
			return;
		}
		
		log.info(pdu.getTypeString(pdu.getType())+" PDU info:");
		log.info("---------------------------------");
		log.info("ErrorIndex="+pdu.getErrorIndex());
		log.info("ErrorStatus="+pdu.getErrorStatusText());
		if(pdu.getErrorIndex()!=0){
			String ecs[]=pdu.getErrorStatusText().split(" ");
			if(ecs.length==2){
				String ec=ecs[1].trim();
				String cn=SNMPUtil.ec_cn.getProperty(ec);
				String en=SNMPUtil.ec_en.getProperty(ec);
				String em=SNMPUtil.ec_enum.getProperty(ec);
				if(cn!=null){log.debug("cn="+cn);}
				if(en!=null){log.debug("en="+en);}
				if(em!=null){log.debug("em="+em);}
			}
		}
		for(int i=0;i<pdu.size();i++){
			VariableBinding vb=pdu.get(i);
			boolean find=false;
			if(all_nodes!=null){
				for(String tmp:all_nodes){
					MibNode mn=mf.getNode(tmp);
					if(vb.getOid().toString().contains(mn.getOid()+".")){
						log.debug(vb.toString().replace(mn.getOid(), tmp));
						find=true;
						break;
					}
				}
			}
			if(find==false){
				log.debug(vb.toString());
			}
		}
		log.info("---------------------------------");		
	}
	public HashMap<String,String> getRndWriteableParas(int cnt){
		HashMap<String,String> hm=new HashMap<String,String>();
		List<String> al = (ArrayList<String>)this.writeable_nodes.clone();
		
		Collections.shuffle(al);
		if(cnt==0)return hm;
		
		al=al.subList(0, cnt);
		for(String tmp:al){
			if(this.deftRow==null){
				log.warn("没有初始数据...");
				hm.put(tmp, null);
			}else{
				int pos=this.writeable_nodes.indexOf(tmp);
				hm.put(tmp, this.deftRow.get(pos));
			}
		}		
		return hm;
		
	}	
	
	public ArrayList<String> getwriteable_nodes(){
		return this.writeable_nodes;
	}
	public ArrayList<String> getdeftRow(){
		return this.deftRow;
	}
	public ArrayList<String> getall_nodes(){
		return this.all_nodes;
	}
	public ArrayList<String> getnon_id_nodes(){
		return this.non_id_nodes;
	}
	
	public HashMap<String,String> getRndWriteableParas(){		
		Random rnd=new Random();
		int i=rnd.nextInt()%this.writeable_nodes.size();		
		return getRndWriteableParas(i);
	}	
	
}
