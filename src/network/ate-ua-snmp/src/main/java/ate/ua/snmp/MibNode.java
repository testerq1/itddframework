package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;

import net.percederberg.mibble.MibType;
import net.percederberg.mibble.MibTypeSymbol;
import net.percederberg.mibble.MibValueSymbol;
import net.percederberg.mibble.snmp.SnmpAccess;
import net.percederberg.mibble.snmp.SnmpObjectType;
import net.percederberg.mibble.type.CompoundConstraint;
import net.percederberg.mibble.type.Constraint;
import net.percederberg.mibble.type.IntegerType;
import net.percederberg.mibble.type.SizeConstraint;
import net.percederberg.mibble.type.StringType;
import net.percederberg.mibble.type.ValueRangeConstraint;
import net.percederberg.mibble.value.ObjectIdentifierValue;

import org.snmp4j.smi.OID;

public class MibNode implements Comparable<MibNode>{
	ObjectIdentifierValue _oid;
	String deft;
	MibValueSymbol[] symbols;
	ArrayList<String> values=new ArrayList<String>();
	String mainConstrains;
	String subConstrains;
	int maxLen;
	int minLen;
	boolean implied;
	
	public int compareTo(MibNode mn){		
		return new OID(getOid()).compareTo(new OID(mn.getOid()));		
	}
	ObjectIdentifierValue getNativeOID(){
		return _oid;
	}
	public boolean isImplied(){
		return this.implied;
	}
	public void setImplied(boolean b){
		this.implied=b;
	}
	public boolean isWriteable(){
		return getAccess().equals(SnmpAccess.READ_CREATE)||getAccess().equals(SnmpAccess.READ_WRITE);
	}
	public MibNode(ObjectIdentifierValue oid) {
		this._oid = oid;
		this.initVariables();
	}
	
	public MibValueSymbol[] get_symbols(){
		return this.symbols;
	}
	
	public ArrayList<String> getValues(){
		return values;
	}
	
	private void initVariables(){
		SnmpObjectType sotype=(SnmpObjectType)_oid.getSymbol().getType();
		
		MibType type=sotype.getSyntax();		
		
		this.deft=sotype.getDefaultValue()==null?null:sotype.getDefaultValue().toString();
		
		Constraint cs=null;
		if(type instanceof StringType){
			cs=((StringType)type).getConstraint();
		}else if(type instanceof IntegerType){
			cs=((IntegerType)type).getConstraint();
			this.symbols=((IntegerType)type).getAllSymbols();
		}
		
		if(cs==null){
			return;
		}
		this.mainConstrains=cs.getClass().getSimpleName();
		if(cs instanceof CompoundConstraint){
			for(Object tmp:((CompoundConstraint)cs).getConstraintList())
				this.values.add(tmp.toString());			
		}else if(cs instanceof SizeConstraint){
			for(Object tmp:((SizeConstraint)cs).getValues()){
				if(tmp instanceof ValueRangeConstraint){
					this.subConstrains="ValueRangeConstraint";
					maxLen=Integer.parseInt(((ValueRangeConstraint)tmp).getUpperBound().toString());
					minLen=Integer.parseInt(((ValueRangeConstraint)tmp).getLowerBound().toString());
				}else{
					this.maxLen=Integer.parseInt(tmp.toString());
					this.minLen=this.maxLen;
				}
			}
		}else if(cs instanceof ValueRangeConstraint){
			this.values.add(((ValueRangeConstraint)cs).getLowerBound().toString());
			this.values.add(((ValueRangeConstraint)cs).getUpperBound().toString());
			
		}
	}
	public String getName() {
		return _oid.getName();
	}

	public String getOid() {
		return _oid.toObject().toString();
	}

	/**
	 * @return 父节点
	 */
	public MibNode getParent() {
		if (_oid.getParent() != null)
			return new MibNode(_oid.getParent());
		return null;
	}
	public SnmpAccess getAccess(){
		SnmpObjectType type=(SnmpObjectType)_oid.getSymbol().getType();
		
		return type.getAccess();
	}
	public String getType() {
		MibValueSymbol symbol = _oid.getSymbol();
		MibTypeSymbol o = ((SnmpObjectType) symbol.getType()).getSyntax()
				.getReferenceSymbol();
		if (o == null) {
			return ((SnmpObjectType) symbol.getType()).getSyntax().getName();
		}
		return o.getName();
	}
	public String getDefault(){
		SnmpObjectType type=(SnmpObjectType)_oid.getSymbol().getType();
		return type.getDefaultValue().toString();
	}
	public boolean isScalar() {
		return _oid.getSymbol().isScalar();
	}
	public boolean isTable() {
		return _oid.getSymbol().isTable();
	}
	public boolean isTableColumn() {
		return _oid.getSymbol().isTableColumn();
	}
	public boolean isTableRow() {
		return _oid.getSymbol().isTableRow();
	}
}
