package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Vector;

import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.VariableBinding;

public class TrapSender extends SnmpConfig {
	  TimeTicks sysUpTime = new TimeTicks(0);
	  OID trapOID = SnmpConstants.coldStart;

	  PDUv1 v1TrapPDU = new PDUv1();

	  public void setSysUpTime(TimeTicks sysUpTime) {
	    this.sysUpTime = sysUpTime;
	  }

	  public void setTrapOID(OID trapOID) {
	    this.trapOID = trapOID;
	  }
	  OctetString contextName = new OctetString();
	  

	  private void checkTrapVariables(Vector vbs,int pduType) {
		    if ((pduType == PDU.INFORM) ||
		        (pduType == PDU.TRAP)) {
		      if ((vbs.size() == 0) ||
		          ((vbs.size() > 1) &&
		           (!((VariableBinding) vbs.get(0)).getOid().equals(SnmpConstants.
		          sysUpTime)))) {
		        vbs.add(0, new VariableBinding(SnmpConstants.sysUpTime, sysUpTime));
		      }
		      if ((vbs.size() == 1) ||
		          ((vbs.size() > 2) &&
		           (!((VariableBinding) vbs.get(1)).getOid().equals(SnmpConstants.
		          snmpTrapOID)))) {
		        vbs.add(1, new VariableBinding(SnmpConstants.snmpTrapOID, trapOID));
		      }
		    }
		  }
}
