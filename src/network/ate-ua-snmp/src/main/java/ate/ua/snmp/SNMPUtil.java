package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashMap;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.Null;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.UnsignedInteger32;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;

import ate.util.GenData0;

public class SNMPUtil {	
	static Properties ec_cn = new Properties();
	static Properties ec_en = new Properties();
	static Properties ec_enum = new Properties();
	private static HashMap<String, SNMPUtil> ins = new HashMap<String, SNMPUtil>();
	private static Logger log = LoggerFactory.getLogger(SNMPUtil.class);
	private MibFactory mf;
    
    public static String snmp_str2index(String s){      
        OID oid=new OID(".'"+s+"'");
        return oid.toString();      
    }
    
	public MibFactory getMf() {
		return mf;
	}
	public void setMf(MibFactory mf) {
		this.mf = mf;
	}

	private SnmpConfig config;
	private Snmp snmp;
	
	public static SNMPUtil createInstance(SnmpConfig config)
			throws Exception {
		if(ins.get(config.getKey())!=null)
			return ins.get(config.getKey());
		
		SNMPUtil imp = new SNMPUtil(config);
		ins.put(config.getKey(), imp);
		return imp;
	}
	public static Variable createVariable(String type, String value) {
		type =type.toLowerCase();
		if(type.startsWith("int")||type.startsWith("rowstatus")){			
			return new Integer32(Integer.parseInt(value));
		}
		if(type.startsWith("counter32")||
				type.startsWith("uinteger32")||
				type.startsWith("gauge32")||
				type.startsWith("opaque")){
			return new UnsignedInteger32(Long.parseLong(value));
		}
		if(type.startsWith("octet")||
				type.startsWith("displaystring"))	
			return new OctetString(value);
		if(type.startsWith("hex"))
			return OctetString.fromString(value, ':', 16);	
//		case 'd':
//			variable = OctetString.fromString(value, '.', 10);
//			break;
		if(type.startsWith("bit"))
			return OctetString.fromString(value, ' ', 2);	
		if(type.startsWith("null"))
			return new Null();
		if(type.startsWith("object"))
			return new OID(value);
		if(type.startsWith("timeticks"))
			return new TimeTicks(Long.parseLong(value));
		if(type.startsWith("ip"))
			return new IpAddress(value);
			
		throw new IllegalArgumentException("Variable type " + type
					+ " not supported");
	}

	public VariableBinding createVB1(String node, String... paras)
			throws Exception {
		if (node.startsWith("1.")) {
			return new VariableBinding(new OID(node));
		}
		MibNode mn = mf.getNode(node);

		if (mn == null)
			throw new Exception("指定的mib不存在：" + node);
		if (mn.isScalar())
			return new VariableBinding(new OID(mn.getOid() + ".0"));

		if (paras == null || paras.length == 0)
			throw new Exception("请指定索引：" + node);

		return new VariableBinding(new OID(mn.getOid() + "." + paras[0]));
	}

	public VariableBinding createVB2(String node, String... paras)
			throws Exception {
		String type = null;
		String value = null;
		VariableBinding vb = createVB1(node, paras);
		if (node.startsWith("1.")) {
			try {
				type = paras[0];
				value = paras[1];
			} catch (Exception e) {
				throw new Exception("请指定类型和值");
			}
		} else {
			MibNode mn = mf.getNode(node);
			try {
				type = mn.getType();
				if (mn.isScalar()) {
					value = paras[0];
				} else {
					value = paras[1];
				}
			} catch (Exception e) {
				throw new Exception("请指定索引和值");
			}
		}
		vb.setVariable(createVariable(type, value));
		return vb;
	}

	public static void loadec_cn(String filename) throws Exception {
		ec_cn.load(GenData0.getInputStream(filename));
	}

	public static void loadec_en(String filename) throws Exception {
		ec_en.load(GenData0.getInputStream(filename));
	}

	public static void loadec_enum(String filename) throws Exception {
		ec_enum.load(GenData0.getInputStream(filename));
	}

	private SNMPUtil(SnmpConfig config) throws Exception{
		this.config = config;
		this.mf=MibFactory.build();
		
		snmp = config.createSnmpSession();
		snmp.listen();
		
	}

	public void close() throws Exception {
		ins.remove(config.getKey());
		snmp.close();
	}

	public PDU createPDU(int type) {
		return config.createPDU(type);
	}

	public PDU sendPDU(PDU pdu) throws Exception {
		return snmp.send(pdu, config.getTarget(pdu.getType())).getResponse();
	}

	public void setReadComunity(String readComunity) {
		config.setReadTarget(readComunity);
	}

	public void setWriteComunity(String writeComunity) {
		config.setWriteTarget(writeComunity);
	}

	public PDUUtil walk(MibNode mn) throws Exception {
		PDUUtil pu = new PDUUtil(mf);
		if (mn == null)
			return pu;
		PDU getn = this.createPDU(PDU.GETNEXT);
		getn.add(new VariableBinding(new OID(mn.getOid())));
		log.debug("walk " + mn.getName());

		if (mn.isScalar()) {
			pu.inPdu = this.sendPDU(getn);
			return pu;
		}
		PDUUtil tmp = new PDUUtil(mf);
		tmp = new PDUUtil(this.sendPDU(getn),mf);
		while (tmp.isok()) {
			if (tmp.contains(mn, null)) {
				if(pu.inPdu==null){
					pu=tmp;
				}else{
					pu.addVBs(tmp.getVBs());
				}
			} else {
				break;
			}
			getn.clear();
			getn.add(new VariableBinding(new OID(tmp.inPdu.get(0).getOid())));
			tmp = new PDUUtil(this.sendPDU(getn),mf);
		}
		return pu;
	}
}
