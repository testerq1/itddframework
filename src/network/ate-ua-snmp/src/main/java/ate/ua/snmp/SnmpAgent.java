package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.CommandResponderEvent;
import org.snmp4j.CommunityTarget;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.PDU;
import org.snmp4j.ScopedPDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.UserTarget;
import org.snmp4j.agent.CommandProcessor;
import org.snmp4j.agent.DefaultMOServer;
import org.snmp4j.log.Log4jLogFactory;
import org.snmp4j.log.LogFactory;
import org.snmp4j.log.LogLevel;
import org.snmp4j.mp.MPv1;
import org.snmp4j.mp.MPv2c;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.ThreadPool;

/**
 * community: public
 * @author Administrator
 *
 */
public class SnmpAgent {
	 static {
		    LogFactory.setLogFactory(new Log4jLogFactory());
		    org.apache.log4j.BasicConfigurator.configure();
		    LogFactory.getLogFactory().getRootLogger().setLogLevel(LogLevel.OFF);
		  }
	protected static final Logger log = LoggerFactory
			.getLogger(SnmpAgent.class);
	protected MessageDispatcherImpl dispatcher;	
	protected Snmp session;
	protected CommandProcessor agent;
	protected DefaultMOServer server;
	protected TransportMapping transportMapping;
	private int version;
	private PDU resp;
	protected OctetString defaultContext;
	protected OctetString engineId=new OctetString(MPv3.createLocalEngineID());
	
	public SnmpAgent(String version){
		if(version.equals("v1"))
			this.version=SnmpConstants.version1;
		else if(version.equals("v2c"))
			this.version=SnmpConstants.version2c;
		else if(version.equals("v3"))
			this.version=SnmpConstants.version3;
		
		initResponse();
		this.agent = new CommandProcessor(new OctetString(
				MPv3.createLocalEngineID()))
		{
			public void processPdu(CommandResponderEvent event) {
				processRequestEvent(event);				
			}
		};
		this.server = new DefaultMOServer();		
	}	
	
	public String getOid(PDU pdu,int index){
		return pdu.get(index).getOid().toString();
	}
	
	public void processRequestEvent(CommandResponderEvent event){
		PDU pdu = event.getPDU();
		if(pdu==null)
			return;		
		try {
		    readCommunity.setAddress(event.getPeerAddress());
            resp.setRequestID(pdu.getRequestID());
			dispatcher.sendPdu(this.transportMapping,readCommunity, resp, false);
			//dispatcher.sendPdu(dispatcher.get, resp, false);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		this.resp.clear();
	}
	
	public void setLogLevelAll(){
		LogFactory.getLogFactory().getRootLogger().setLogLevel(LogLevel.ALL);
	}
	/**
	 * 
	 * @param oid
	 * @param type int
					rowstatus
					counter32||uinteger32||gauge32||opaque
					octet||displaystring
					hex
					bit
					null
					object
					timeticks
					ip
	 * @param value
	 */
	public void addVB(String oid,String type,String value){
		VariableBinding vb=new VariableBinding(new OID(oid));
		vb.setVariable(SNMPUtil.createVariable(type, value));
		resp.add(vb);
	}
	
	private void initResponse() {
		 this.resp = null;
		if (version == SnmpConstants.version3) {
			resp = new ScopedPDU();
			ScopedPDU scopedPDU = (ScopedPDU) resp;
			if (engineId != null) {
				scopedPDU.setContextEngineID(engineId);
			}
			if (defaultContext != null) {
				scopedPDU.setContextName(defaultContext);
			}
		} else {			
			resp = new PDU();			
		}		
		resp.setType(PDU.RESPONSE);				
	}	
	
	public void listen(){
		initMessageDispatcher();				
		agent.setWorkerPool(ThreadPool.create("RequestPool", 4));
		this.dispatcher.addCommandResponder(agent);
		try {
			this.session.listen();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	CommunityTarget readCommunity;
	
	public void setLocalAddress(String uri) throws IOException {
		Address listenGenericAddress = GenericAddress.parse(uri);
		
		if (uri.toLowerCase().startsWith("tcp")) {
			transportMapping = new DefaultTcpTransportMapping(
					(TcpAddress) listenGenericAddress);
		} else if (uri.toLowerCase().startsWith("udp")) {
			transportMapping = new DefaultUdpTransportMapping(
					(UdpAddress) listenGenericAddress);
		} else {
			throw new IllegalArgumentException("Unknown protocol: "+ uri);
		}
		 readCommunity = new CommunityTarget();
		 readCommunity.setCommunity(new OctetString("public"));
		 readCommunity.setVersion(this.version);
         
	}
	
	private void initMessageDispatcher() {
		dispatcher = new MessageDispatcherImpl();
		MPv3 mpv3 = new MPv3(agent.getContextEngineID().getValue());

		USM usm = new USM(SecurityProtocols.getInstance(),
				agent.getContextEngineID(), 0);
		SecurityModels.getInstance().addSecurityModel(usm);
		SecurityProtocols.getInstance().addDefaultProtocols();
		dispatcher.addMessageProcessingModel(new MPv1());
		dispatcher.addMessageProcessingModel(new MPv2c());
		dispatcher.addMessageProcessingModel(mpv3);
		//dispatcher.add
		session = new Snmp(dispatcher);		
		
		try {
			session.addTransportMapping(transportMapping);
		} catch (Exception ex) {
			log.warn("Failed to initialize transport mapping '"
					+ transportMapping + "' with: " + ex.getMessage());
		}
	}
	public static void main(String[] args) throws IOException{
		SnmpAgent agt=new SnmpAgent("v2c");
		agt.setLocalAddress("udp:172.16.5.111/161");
		agt.listen();
		agt.addVB("1.3.6.1.4.1.12.1.1.0", "uinteger32", "1000");
		System.out.println("tere");
	} 
}
