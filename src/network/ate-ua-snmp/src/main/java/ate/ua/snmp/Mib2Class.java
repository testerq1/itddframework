package ate.ua.snmp;

/*
 * #%L
 * iTDD UA SNMP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Iterator;

import net.percederberg.mibble.Mib;
import net.percederberg.mibble.MibLoader;
import net.percederberg.mibble.MibSymbol;
import net.percederberg.mibble.MibValue;
import net.percederberg.mibble.MibValueSymbol;
import net.percederberg.mibble.MibWriter;
import net.percederberg.mibble.value.ObjectIdentifierValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Mib2Class {
	static Logger log= LoggerFactory.getLogger(Mib2Class.class);
    private static void printMibs(MibLoader loader, int printMode) {
        Mib[]  mibs = loader.getAllMibs();

        for (int i = 0; i < mibs.length; i++) {
            if (mibs[i].isLoaded()) {
            	printMib(mibs[i]);
            }
        }
    }

    /**
     * Prints the contents of a single MIB in pretty printing mode.
     *
     * @param mib            the MIB to print
     */
    private static void printMib(Mib mib) {
        MibWriter  os = new MibWriter(System.out);

        os.print(mib);

    }

    /**
     * Prints the contents of a single MIB in debug mode.
     *
     * @param mib            the MIB to print
     */
    private static void printDebug(Mib mib) {
        Iterator  iter;

        iter = mib.getAllSymbols().iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
            System.out.println();
        }
        System.out.println();
        System.out.println();
    }

    /**
     * Prints the complete OID tree. All MIB modules loaded with the
     * specified MIB loader will be printed.
     *
     * @param loader            the MIB loader
     */
    private static void printOidTree(MibLoader loader) {
        Mib                    mib;
        ObjectIdentifierValue  root = null;
        Iterator               iter;
        MibSymbol              symbol;
        MibValue               value;

        if (loader.getAllMibs().length <= 0) {
        	log.error("no MIB modules have been loaded");
            return;
        }
        mib = loader.getAllMibs()[0];
        iter = mib.getAllSymbols().iterator();
        while (root == null && iter.hasNext()) {
            symbol = (MibSymbol) iter.next();
            if (symbol instanceof MibValueSymbol) {
                value = ((MibValueSymbol) symbol).getValue();
                if (value instanceof ObjectIdentifierValue) {
                    root = (ObjectIdentifierValue) value;
                }
            }
        }
        if (root == null) {
            log.error("no OID value could be found in " + mib.getName());
        } else {
            while (root.getParent() != null) {
                root = root.getParent();
            }
            printOid(root);
        }
    }

    /**
     * Prints the detailed OID tree starting in the specified OID. 
     *
     * @param oid            the OID node to print
     */
    private static void printOid(ObjectIdentifierValue oid) {
        System.out.println(oid.toDetailString());
        for (int i = 0; i < oid.getChildCount(); i++) {
            printOid(oid.getChild(i));
        }
    }

}
