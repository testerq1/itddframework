package org.snmp4j.tools.console;

import java.util.HashMap;

import org.azeckoski.reflectutils.ReflectUtils;
import org.azeckoski.reflectutils.exceptions.FieldnameNotFoundException;

import org.snmp4j.smi.OctetString;
import org.snmp4j.tools.console.SnmpRequest;

import ate.AbstractURIHandler;
import ate.ua.UAOption;
import ate.ua.mina.AMinaUAImp;

/**
 * 使用封装SnmpRequest类的方式实现snmp command功能,还没有完成
 * @author Administrator
 * 
 */
public class SnmpUA3 extends AMinaUAImp {
	SnmpRequest req;

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			boolean istrap="trap".equalsIgnoreCase(get_query_para("p"));
			if (port <= 0){
				if(istrap)
					port = 162;
				else
					port = 161;
			}			
			req=new SnmpRequest(host+"/"+port,this.get_query_arameters());			
			
			return this;
		}
		return this;
	}
	
	public void help(){
		req.printUsage();
	}
	
	public void set_paras(HashMap<String, Object> map) {
		ReflectUtils setter = ReflectUtils.getInstance();		
		for (String key : map.keySet()) {
			Object o = map.get(key);
			try {
				setter.setFieldValue(req, key, map.get(key));
				log.debug("set SnmpRequest's {} to {}", key, o);
			} catch (FieldnameNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String get_default_schema() {
		return "snmp";
	}

	private static OctetString createOctetString(String s) {
		OctetString octetString;
		if (s.startsWith("0x")) {
			octetString = OctetString.fromHexString(s.substring(2), ':');
		} else {
			octetString = new OctetString(s);
		}
		return octetString;
	}

	@Override
	public boolean message_matched(Object msg, HashMap hm) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected void add_default_filter() {
		// TODO Auto-generated method stub
		
	}

	
}
