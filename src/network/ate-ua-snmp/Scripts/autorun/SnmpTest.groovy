import java.lang.reflect.Method;

import org.testng.annotations.*
import ate.testcase.*
import org.testng.annotations.Test;
public class SnmpTest extends CsUATestcase {
	
	@DataProvider(name="Data_TestStep")
    public Object[][] Data_TestStep(){		
        return [["udp"],["tcp"]];
    }
	
	//@Test(dataProvider="Data_TestStep")
	@Test
	public void get(){
		log.info(STEP+" get:");
		log.info(STEP+".1 init: $siip $sip");
		def clt = create_client("snmp://172.16.5.1");
		start_all_ua();
		
		def pdu = clt.createAPDU(["user":"public1","pdu":clt.GET])
					 .addOid("1.3.6.1.4.1.12.2.2.0")
		             .addOid("1.3.6.1.4.1.12.2.1.0");
		clt.sendMessage(pdu);
		
		def resp = clt.recv_apdu();
		assertTrue(resp!=null&&resp.next()>0);
		log.info(""+resp.v());
		assertTrue(resp.next()!=null&&resp.v()>=0);
		log.info(""+resp.v());
		resp.reset();
		assertTrue(resp.get("1.3.6.1.4.1.12.2.2.0")>0)
		assertTrue(resp.get("1.3.6.1.4.1.12.2.1.0")>=0)
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {		
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		//;
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
