package ate.test;

/*
 * #%L
 * iTDD UA CMCC-Portal
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.testcase.CsUATestcase;
import ate.ua.portal.CMCCPortalPacket;
import ate.ua.portal.CMCCPortalUA;

public class PortalUATest extends CsUATestcase{
	@Test
	public void AutoResponse() {
		  CMCCPortalUA svr=create_server("portal://127.0.0.1:2000/udp");	   
		  CMCCPortalUA clt=create_client("portal://127.0.0.1:2000/udp");	  
		  assertTrue(!clt.is_ready());
		  assertTrue(!svr.is_ready());	  
		  this.start_all_ua();	  
		  assertTrue(clt.is_ready());
		  assertTrue(svr.is_ready());
		  svr.set_auto_response(new HashMap());
		  
		  CMCCPortalPacket pkt=clt.create_packet("REQ_AUTH");
		  pkt.setUserip("11.0.0.1");
		  pkt.reqid=pkt.newReqID();
		  clt.send_message(pkt);
		  
		  pkt = clt.recv_message();
		  assertTrue(pkt!=null);
		  assertTrue(pkt.isType("ACK_AUTH"));
		  assertTrue(pkt.reqid!=0);
		  assertTrue(pkt.getAttr("chappassword")==null);
		  
		  svr.clear_q();
		  clt.send_message(pkt.createResponse(null, null));
		  
		  pkt = svr.recv_message();
		  assertTrue(pkt!=null);
		  assertTrue(pkt.isType("AFF_ACK_AUTH"));
		  assertTrue(pkt.reqid!=0);
		  assertTrue(pkt.serialno!=0);
	  }
	
  @Test
  public void REQ_CHALLENGE() {
	  CMCCPortalUA svr=create_server("portal://127.0.0.1:2000/udp");	   
	  CMCCPortalUA clt=create_client("portal://127.0.0.1:2000/udp");	  
	  assertTrue(!clt.is_ready());
	  assertTrue(!svr.is_ready());	  
	  this.start_all_ua();	  
	  assertTrue(clt.is_ready());
	  assertTrue(svr.is_ready());
	  
	  CMCCPortalPacket pkt=clt.create_packet("REQ_CHALLENGE");
	  pkt.setUserip("11.0.0.1");
	  clt.send_message(pkt);
	  
	  CMCCPortalPacket req = svr.rcv_message("REQ_CHALLENGE",""+pkt.serialno,""+pkt.reqid);
	  assertTrue(req!=null);
	  assertTrue(req.serialno!=0);
	  
	  HashMap<String, String> hm = new HashMap<String, String>();
	  hm.put("pkttype", "ACK_CHALLENGE");
	  hm.put("chappassword", "abcdef");
	  svr.send_message(svr.create_packet(hm,req));	 
	  
	  pkt = clt.recv_message();
	  assertTrue(pkt!=null);
	  assertTrue(pkt.isType("ACK_CHALLENGE"));
	  assertTrue(pkt.reqid!=0);
	  assertTrue(pkt.getAttr("chappassword")==null);
	  //assertTrue(resp.getAttribute("Framed-MTU").getAttributeValue().equals("1500"));
  }
  @Test
  public void REQ_AUTH() {
	  CMCCPortalUA svr=create_server("portal://127.0.0.1:2000/udp");	   
	  CMCCPortalUA clt=create_client("portal://127.0.0.1:2000/udp");	  
	  assertTrue(!clt.is_ready());
	  assertTrue(!svr.is_ready());	  
	  this.start_all_ua();	  
	  assertTrue(clt.is_ready());
	  assertTrue(svr.is_ready());
	  
	  CMCCPortalPacket pkt=clt.create_packet("REQ_AUTH");
	  pkt.setUserip("11.0.0.1");
	  pkt.reqid=pkt.newReqID();
	  clt.send_message(pkt);
	  
	  CMCCPortalPacket req = svr.rcv_message("REQ_AUTH",""+pkt.serialno,""+pkt.reqid);
	  assertTrue(req!=null);
	  assertTrue(req.serialno!=0);
	  
	  HashMap<String, String> hm = new HashMap<String, String>();
	  hm.put("pkttype", "ACK_AUTH");
	  hm.put("chappassword", "abcdef");
	  //svr.send_message(svr.create_packet(hm,req));	 
	  svr.send_message(req.createResponse(null, null));
	  
	  
	  pkt = clt.recv_message();
	  assertTrue(pkt!=null);
	  assertTrue(pkt.isType("ACK_AUTH"));
	  assertTrue(pkt.reqid!=0);
	  assertTrue(pkt.getAttr("chappassword")==null);
	  
	  svr.clear_q();
	  clt.send_message(pkt.createResponse(null, null));
	  
	  pkt = svr.recv_message();
	  assertTrue(pkt!=null);
	  assertTrue(pkt.isType("AFF_ACK_AUTH"));
	  assertTrue(pkt.reqid!=0);
	  assertTrue(req.serialno!=0);
  }
  
  @BeforeMethod
  public void beforeMethod(Method m) {	  
	  super.BeforeMethod(m);
  }

  @AfterMethod
  public void afterMethod(Method m) {
	  super.shutdown_all_ua();
	  super.AfterMethod(m);
  }
}
