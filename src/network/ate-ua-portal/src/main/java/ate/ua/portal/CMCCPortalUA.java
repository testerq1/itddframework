package ate.ua.portal;

/*
 * #%L
 * iTDD UA CMCC-Portal
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import ate.ua.IAutoResponseMessage;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.AbstractCustomUA;
import ate.ua.mina.NoExceptionProtocolCodecFilter;
import ate.ua.portal.CMCCPortalPacket.PT;

/**
 * 中国移动Portal UA
 */
public class CMCCPortalUA extends AbstractCustomUA<CMCCPortalPacket> implements
		IAutoResponseMessage<IoSession, CMCCPortalPacket> {
	protected HashMap<String, String> autoRespAttr = null;

	/**
	 * 创建一个packet<br>
	 * pkttype,包类型<br>
	 * errcode,错误码<br>
	 * username,password,challenge,chappassword.<br>
	 * 
	 * @param paras
	 *            the paras
	 * @param sended_msg
	 *            the sended_msg
	 * @return the cMCC portal packet
	 */
	public CMCCPortalPacket create_packet(HashMap<String, String> paras,
			CMCCPortalPacket sended_msg) {
		CMCCPortalPacket pkt = new CMCCPortalPacket();
		pkt.type = CMCCPortalPacket.PT.valueOf(paras.remove("pkttype"))
				.getValue();
		pkt.errcode = 0;
		if (sended_msg != null) {
			pkt.pap = sended_msg.pap;
			pkt.rsv = sended_msg.rsv;
			pkt.serialno = sended_msg.serialno;
			pkt.usrip = sended_msg.usrip;
			pkt.usrport = sended_msg.usrport;
			pkt.ver = sended_msg.ver;
			pkt.reqid = sended_msg.reqid;
		}

		if (pkt.type == PT.REQ_CHALLENGE.getValue()
				|| pkt.type == PT.REQ_AUTH.getValue()
				|| pkt.type == PT.REQ_LOGOUT.getValue()) {
			pkt.serialno = pkt.newSerialNo();
		}

		if (PT.ACK_CHALLENGE.getValue() == pkt.type)
			pkt.reqid = pkt.newReqID();

		if (paras.get("errcode") != null)
			pkt.errcode = pkt.getErrCode(paras.remove("errcode"), pkt.type);

		for (String key : paras.keySet()) {
			pkt.addAttr(sended_msg.createAttr(key, paras.remove(key)));
		}

		pkt.attrnum = (byte) pkt.attrs.size();

		return pkt;
	}

	/**
	 * 根据指定类型创建一个CMCCPortalPacket.
	 * 
	 * @param type
	 *            one string of
	 *            {REQ_CHALLENGE,ACK_CHALLENGE,REQ_AUTH,ACK_AUTH,REQ_LOGOUT,
	 *            ACK_LOGOUT,AFF_ACK_AUTH,NTF_LOGOUT,REQ_INFO,ACK_INFO}
	 * @return the cMCC portal packet
	 */
	public CMCCPortalPacket create_packet(String type) {
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("pkttype", type);
		return this.create_packet(hm, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ate.ua.IPacketUA#get_auto_resp_msg(org.apache.mina.core.session.IoSession
	 * , java.lang.Object)
	 */
	public CMCCPortalPacket get_auto_resp_msg(IoSession session,
			CMCCPortalPacket req) {
		if (autoRespAttr == null) {
			return null;
		}

		return req.createResponse(autoRespAttr, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ate.ua.AUAImp#getDefaultSchema()
	 */
	@Override
	public String get_default_schema() {
		return "portal";
	}

	/**
	 * rcvMessage的时候调用<br>
	 * 检查了ErrCode,pkttype,serialno,reqid是不是符合预期.<br>
	 * 
	 * @param omsg
	 *            the omsg
	 * @param hm
	 *            the hm
	 * @return true, if successful
	 */
	@Override
	public boolean message_matched(CMCCPortalPacket msg,
			HashMap<String, Object> hm) {
		if (hm == null)
			return true;

		Object reqType = hm.get("pkttype");

		if (reqType != null) {
			CMCCPortalPacket.PT rt = CMCCPortalPacket.PT
					.valueOf(reqType.toString());
			if (rt == null) {
				log.error("pkttype is wrong " + reqType);
				return false;
			}
			if (rt.getValue() != msg.type)
				return false;
		}
		Object id = hm.get("serialno");
		if (id != null) {
			short serialno = Short.parseShort(id.toString());
			if (serialno != msg.serialno)
				return false;
		}
		id = hm.get("reqid");
		if (id != null) {
			short reqid = Short.parseShort(id.toString());
			if (reqid != msg.reqid)
				return false;
		}
		return true;
	}

	/**
	 * Rcv_message.
	 * 
	 * @param reqType
	 *            the req type
	 * @param serialno
	 *            the serialno
	 * @param reqid
	 *            the reqid
	 * @return the cMCC portal packet
	 */
	public CMCCPortalPacket rcv_message(String reqType, String serialno,
			String reqid) {
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("pkttype", reqType);
		hm.put("serialno", serialno);
		hm.put("reqid", reqid);
		return (CMCCPortalPacket) this.recv_message(hm);
	}

	/**
	 * 预设值一组自动响应的参数,如果attrs为null，则不会自动响应
	 * 
	 * @param attrs
	 */
	public void set_auto_response(HashMap<String, String> attrs) {
		this.autoRespAttr = attrs;
	}

    @Override
    public CMCCPortalPacket decodePacket(IoBuffer in) {
        if(needDecode)
            return  CMCCPortalPacket.create_packet(in);
        return new CMCCPortalPacket(in.array());
    }
}
