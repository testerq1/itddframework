package ate.ua.portal;

/*
 * #%L
 * iTDD UA CMCC-Portal
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;

import ate.ua.RunFailureException;
import ate.ua.AbstractCustomPacket;
import ate.util.GenData0;
import ate.util.X2X0;

/**
 * The Class CMCCPortalPacket.
 */
public class CMCCPortalPacket extends AbstractCustomPacket<CMCCPortalPacket> {

    /** The Constant HEADER_LENGTH. */
    public static final int HEADER_LENGTH = 16;

    /**
     * The Class AttrTLV.
     */
    public class AttrTLV implements Cloneable {

        /** The type. */
        public byte type;

        /** The len. */
        public byte len;

        /** The value. */
        public byte[] value;

        /**
         * Builds the.
         */
        public void build() {
            if (value.length > Byte.MAX_VALUE)
                log.warn("length not match!");
            this.len = (byte) value.length;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#clone()
         */
        @Override
        public AttrTLV clone() {
            AttrTLV tlv = new AttrTLV();
            tlv.type = type;
            tlv.len = len;
            tlv.value = value.clone();
            return null;
        }
    }

    /**
     * Checks if is request.
     *
     * @return true, if is request
     */
    public boolean isRequest() {
        return type % 2 == 1;
    }

    /**
     * Checks if is ack auth.
     *
     * @return true, if is ack auth
     */
    public boolean isAckAuth() {
        return type == 4;
    }

    /**
     * The Enum PKTTYPE.
     */
    public static enum PT {
        /** The RE q_ challenge. */
        REQ_CHALLENGE((byte) 0x01),
        /** The AC k_ challenge. */
        ACK_CHALLENGE((byte) 0x02),
        /** The RE q_ auth. */
        REQ_AUTH((byte) 0x03),
        /** The AC k_ auth. */
        ACK_AUTH((byte) 0x04),
        /** The RE q_ logout. */
        REQ_LOGOUT((byte) 0x05),
        /** The AC k_ logout. */
        ACK_LOGOUT((byte) 0x06),
        /** The AF f_ ac k_ auth. */
        AFF_ACK_AUTH((byte) 0x07),
        /** The NT f_ logout. */
        NTF_LOGOUT((byte) 0x08),
        /** The RE q_ info. */
        REQ_INFO((byte) 0x09),
        /** The AC k_ info. */
        ACK_INFO((byte) 0x0a);
        private byte value;

        private PT(byte value) {
            this.value = value;
        }

        /**
         * Gets the value.
         *
         * @return the value
         */
        public byte getValue() {
            return value;
        }

        /**
         * Gets the name.
         *
         * @param value
         *            the value
         * @return the name
         */
        public static String getName(int value) {
            if (value >= PT.values().length)
                log.debug("");
            return PT.values()[value].name();
        }
    }

    /**
     * The Enum ATTRTYPE.
     */
    enum ATTRTYPE {

        /** The username. */
        username((byte) 0x01),
        /** The password. */
        password((byte) 0x02),
        /** The challenge. */
        challenge((byte) 0x03),
        /** The chappassword. */
        chappassword((byte) 0x04);
        private byte value;

        private ATTRTYPE(byte value) {
            this.value = value;
        }

        /**
         * Gets the value.
         *
         * @return the value
         */
        public byte getValue() {
            return value;
        }
    }

    /** The ver. */
    public byte ver = 1;

    // PKTTYPE,默认为-1，方便断言
    /** The type. */
    public byte type = -1;

    /** 只对Type值为 0x03 的认证请求报文有意义 Chap：0x00 Pap: 0x01. */
    public byte pap = 0;

    /** The rsv. */
    public byte rsv = 0;

    /** serial no */
    public short serialno;

    /** reqid */
    public short reqid = 0;

    /**
     * UserIP字段为Portal用户的IP地址，长度为 4 字节，其值由Portal Server根据其获得的IP地址填写，
     * 在所有的报文中此字段都要有具体的值；.
     */
    public int usrip = -1;
    // UserPort字段目前没有用到，长度为 2 字节，在所有报文中其值为0
    /** The usrport. */
    public short usrport = 0;

    /**
     * ErrCode字段和Type字段一起表示一定的意义，长度为 1字节，具体如下：
     * 1)对于Type值为1、3、7的报文，ErrCode字段无意义，其值为0； 2)当Type值为 2 时：
     * ErrCode＝0，表示AC设备告诉Portal Server请求Challenge成功； ErrCode＝1，表示AC设备告诉Portal
     * Server请求Challenge被拒绝； ErrCode＝2，表示AC设备告诉Portal Server此链接已建立；
     * ErrCode＝3，表示AC设备告诉Portal Server有一个用户正在认证过程中，请稍后再试；
     * ErrCode＝4，则表示AC设备告诉Portal Server此用户请求Challenge失败（发生错误）； 3)当Type值为 4 时：
     * ErrCode＝0，表示AC设备告诉Portal Server此用户认证成功； ErrCode＝1，表示AC设备告诉Portal
     * Server此用户认证请求被拒绝； ErrCode＝2，表示AC设备告诉Portal Server此链接已建立；
     * ErrCode＝3，表示AC设备告诉Portal Server有一个用户正在认证过程中，请稍后再试； ErrCode＝4
     * ，表示AC设备告诉Portal Server此用户认证失败（发生错误）； 4)当Type值为 5 时：
     * ErrCode＝0，表示此报文是Portal发给AC设备的请求下线报文；
     * ErrCode＝1，表示此报文是在Portal没有收到AC设备发来的对各种请求的响应报文， 而定时器超时时由Portal发给AC设备的报文；
     * 5)当Type值为 6 时： ErrCode＝0，表示AC设备告诉Portal Server此用户下线成功；
     * ErrCode＝1，表示AC设备告诉Portal Server此用户下线被拒绝； ErrCode＝2, 表示AC设备告诉Portal
     * Server此用户下线失败（发生错误）； 6)对Type为REQ_INFO时，ErrCode无意义，其值为0；
     * 7)对Type为NTF_LOGOUT时，ErrCode含义如下： ErrCode＝0,下线
     * 8)对Type为ACK_INFO时，ErrCode含义如下：
     * ErrCode＝0,处理成功，但不表示全部消息都被获取了，有多少信息被获得应通过属性来判断 ErrCode＝1,功能不支持，表示设备不支持这一功能
     * ErrCode＝2,消息处理失败，由于某种不可知原因，使处理失败，例如询问消息格式错误等。.
     */
    public byte errcode;

    /**
     * AttrNum字段表示其后边可变长度的属性字段属性的个数，长度为 1 字节（表示属性字段最多可有255个属性），
     * 其值在所有的报文中都要根据具体情况赋值；.
     */
    public byte attrnum;
    // attr list
    /** The attrs. */
    public ArrayList<AttrTLV> attrs = new ArrayList<AttrTLV>();

    public CMCCPortalPacket(){}
    public CMCCPortalPacket(byte[] data){super(data);}
    
    public void setType(PT type) {
        this.type = type.getValue();
    }

    /**
     * Sets the userip.
     *
     * @param ip
     *            the new userip
     */
    public void setUserip(String ip) {
        this.usrip = X2X0.ip2i(ip);
    }

    /**
     * Gets the userip.
     *
     * @return the userip
     */
    public String getUserip() {
        return X2X0.int2ip(this.usrip);
    }

    /**
     * Checks if is type.
     *
     * @param name
     *            the name
     * @return true, if is type
     */
    public boolean isType(String name) {
        return PT.valueOf(name).getValue() == type;
    }

    /**
     * Gets the err code.
     *
     * @param ec
     *            the ec
     * @param type
     *            the type
     * @return the err code
     */
    public byte getErrCode(String ec, byte type) {
        if (ec.equalsIgnoreCase("random")) {
            switch (type) {
            case 2: // ACK_CHALLENGE
            case 4: // ACK_AUTH
                return (byte) GenData0.random(0, 4);
            case 5: // REQ_LOGOUT
                return (byte) GenData0.random(0, 1);
            case 6: // ACK_LOGOUT
            case 10:// ACK_INFO
                return (byte) GenData0.random(0, 2);
            default:
                break;
            }
            return 0;
        }
        return Byte.parseByte(ec);
    }

    /**
     * 该方法会被get_auto_resp_msg调用，因此需要考虑自动回响应时的一些处理(TODO) paras.put("errcode",1)
     * paras.put("errcode","random") paras.put("chappassword"，"123456")
     *
     * @param paras
     *            the paras
     * @param sendedMsg
     *            the sended msg
     * @return the cMCC portal packet
     */
    public CMCCPortalPacket createResponse(HashMap<String, String> paras,
            CMCCPortalPacket sendedMsg) {
        // AFF_ACK_AUTH是终结信令
        if (type == PT.AFF_ACK_AUTH.getValue()) {
            log.debug(this + "packet is AFF_ACK_AUTH");
            return null;
        }
        CMCCPortalPacket resp = new CMCCPortalPacket();

        if (type == PT.ACK_AUTH.getValue())
            resp.type = PT.AFF_ACK_AUTH.getValue();
        else if (type % 2 == 1)
            resp.type = (byte) (type + 1);
        else {
            log.debug(this + "null");
            return null;
        }

        resp.ver = ver;
        resp.pap = pap;
        resp.rsv = rsv;
        resp.serialno = this.serialno;
        resp.usrip = this.usrip;
        resp.usrport = this.usrport;
        resp.errcode = 0;
        resp.attrnum = 0;

        if (PT.ACK_CHALLENGE.getValue() == resp.type)
            resp.reqid = this.newReqID();
        else if (sendedMsg != null)
            resp.reqid = sendedMsg.reqid;
        else
            resp.reqid = this.reqid;

        if (paras == null) {
            // TODO 处理自动回相应的场景
            return resp;
        }

        if (paras.get("errcode") != null)
            resp.errcode = this.getErrCode(paras.remove("errcode"), resp.type);

        for (String key : paras.keySet()) {
            resp.addAttr(createAttr(key, paras.remove(key)));
        }

        resp.attrnum = (byte) resp.attrs.size();
        log.debug(this + " return " + resp);
        return resp;
    }

    /**
     * 生成一个新的Serial No. 1)由Portal Server随机生成，Portal
     * Server必须尽量保证不同认证流程的SerialNo在一定时间内不得重复， 在同一个认证流程中所有报文的SerialNo相同；<br>
     * 2)由Portal发出的Type值为1、3的请求报文其SerialNo都是随机生成数； <br>
     * 3)由Portal发出的Type值为5的报文其SerialNo值分两种情况：<br>
     * 当ErrCode为0 时，SerialNo值为一个随机生成数；<br>
     * 当ErrCode为1时，SerialNo值可能和Type值为1或3的报文相同，具体要看是请求Challenge超时还是请求认证超时； <br>
     * 4)由Portal发出的Type值为7的报文SerialNo和其发出的相应请求报文的SerrialNo相同；
     * 比如对于Type值为7的报文其SerialNo值和Type值为3的请求认证报文相同；<br>
     * 5)由AC发给Portal的响应报文的SerialNo必须和Portal发送的相应请求报文的SerialNo一样，否则Portal
     * Server会丢 掉从AC设备发来的响应报文； <br>
     * 比如Type值为2的报文其SerialNo值必须和Type值为1的报文相同; <br>
     * Type值为4的报文其 SerialNo值必须和Type值为3的报文相同; <br>
     * Type值为6的报文其SerialNo值必须和Type值为5的报文相同。<br>
     * 
     * @return the short
     */
    public static short newSerialNo() {
        return (short) GenData0.random(0, 2 ^ 16 - 1);
    }

    /**
     * 生成一个新的req id. 1)ReqID字段长度为 2 个字节，由AC设备随机生成，尽量使得在一定时间内ReqID不重复。<br>
     * 2)在Chap认证方式中：<br>
     * a、AC设备在Type为2的请求Challenge响应报文中把该ReqID的值告诉Portal Server；<br>
     * b、在Type值为3、4、7的报文中ReqID字段的值都和Type值为2的报文中此字段的值相同； <br>
     * c、在Type值为5的报文中，若报文表示请求Challenge 超时则此字段值为0 ；<br>
     * 若报文表示请求认证超时则此字段值和Type值为 2(ACK-CHALLENGE)的报文中此字段的值相同；<br>
     * 3)在Pap认证方式中，此字段无意义，其值为0； <br>
     * 4)在Type值为 5的报文中，若报文表示请求下线时则此字段值为0 ；<br>
     * 5)在Type值为1、6的报文中，该字段均无意义，值都为 0；<br>
     * 
     * @return the short
     */
    public static short newReqID() {
        return (short) GenData0.random(0, 2 ^ 16 - 1);
    }

    /**
     * 创建一个attr.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     * @return the attr tlv
     */
    public AttrTLV createAttr(String name, Object value) {
        AttrTLV attr = new AttrTLV();
        attr.type = ATTRTYPE.valueOf(name).getValue();
        attr.value = this.get_bytes(value);
        attr.build();
        return attr;
    }

    /**
     * 新增一个attr.
     *
     * @param tlv
     *            the tlv
     */

    public void addAttr(AttrTLV tlv) {
        if (tlv == null)
            log.warn("attr is null");
        this.attrs.add(tlv);
        this.attrnum++;
    }

    /**
     * Gets the attr.
     *
     * @param name
     *            the name
     * @return the attr
     */
    public AttrTLV getAttr(String name) {
        for (AttrTLV tmp : attrs) {
            if (tmp.type == ATTRTYPE.valueOf(name).getValue())
                return tmp;
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see ate.ua.CustomPacket#encodePacket(java.io.OutputStream)
     */
    @Override
    public void encodePacket(OutputStream out) {
        DataOutputStream dos = new DataOutputStream(out);
        try {
            dos.writeByte(ver);
            dos.writeByte(type);
            dos.writeByte(pap);
            dos.writeByte(rsv);
            dos.writeShort(this.serialno);
            dos.writeShort(this.reqid);
            dos.writeInt(this.usrip);
            dos.writeShort(this.usrport);
            dos.writeByte(this.errcode);
            dos.writeByte(this.attrnum);
            for (int i = 0; i < this.attrs.size(); i++) {
                AttrTLV tlv = attrs.get(i);
                if (tlv == null)
                    continue;
                dos.write(tlv.type);
                dos.write(tlv.len + 2);
                if (tlv.value != null)
                    dos.write(tlv.value);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static CMCCPortalPacket create_packet(IoBuffer in) {
        CMCCPortalPacket pkt=new CMCCPortalPacket();
        DataInputStream din = new DataInputStream(in.asInputStream());
        try {
            if (din.available() < HEADER_LENGTH) {
                log.warn("bad packet: packet too short (" + din.available()
                        + " bytes)");
                return null;
            }
            pkt.ver = din.readByte();
            pkt.type = din.readByte();
            pkt.pap = din.readByte();
            pkt.rsv = din.readByte();
            pkt.serialno = din.readShort();
            pkt.reqid = din.readShort();
            pkt.usrip = din.readInt();
            pkt.usrport = din.readShort();
            pkt.errcode = din.readByte();
            pkt.attrnum = din.readByte();
            for (int i = 0; i < pkt.attrnum; i++) {
                AttrTLV tlv = pkt.new AttrTLV();
                tlv.type = din.readByte();
                tlv.len = din.readByte();
                tlv.value = new byte[tlv.len - 2];
                din.read(tlv.value);
            }
        } catch (IOException e) {
            throw new RunFailureException(e);
        }
        return pkt;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        if (type < 1)
            return null;
        return PT.getName(type - 1);
    }

    @Override
    public CMCCPortalPacket build(String k, Object v) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CMCCPortalPacket get_packet() {
        return this;
    }
}
