package ate.ua.syslog;

/*
 * #%L
 * iTDD UA Syslog
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.net.SocketAddress;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.productivity.java.syslog4j.Syslog;
import org.productivity.java.syslog4j.SyslogConstants;
import org.productivity.java.syslog4j.SyslogIF;
import org.productivity.java.syslog4j.server.SyslogServer;
import org.productivity.java.syslog4j.server.SyslogServerConfigIF;
import org.productivity.java.syslog4j.server.SyslogServerEventIF;
import org.productivity.java.syslog4j.server.SyslogServerIF;
import org.productivity.java.syslog4j.server.SyslogServerSessionEventHandlerIF;

import ate.AbstractURIHandler;
import ate.ua.AbstractPacketUAImp;
import ate.ua.UAOption;

public class SyslogUA extends AbstractPacketUAImp implements
		SyslogServerSessionEventHandlerIF {
	private SyslogIF syslog;
	private SyslogServerIF syslogd;
	private String transport = "udp";

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option != UA_TYPE)
			return this;

		if (this.path != null && this.path.length() > 2)
			this.transport = this.path.substring(1);

		if (type == CLIENT) {
			syslog = Syslog.getInstance(this.transport);
			syslog.getConfig().setHost(host);
			syslog.getConfig().setPort(port);
		} else {
			syslogd = SyslogServer.getInstance(this.transport);

			SyslogServerConfigIF syslogServerConfig = syslogd.getConfig();
			syslogServerConfig.setHost(host);
			syslogServerConfig.setPort(port);
			syslogServerConfig.addEventHandler(this);

			// if (syslogServerConfig instanceof TCPNetSyslogServerConfigIF) {
			// ((TCPNetSyslogServerConfigIF)
			// syslogServerConfig).setTimeout(Integer.parseInt(options.timeout));
			// if (!options.quiet) {
			// System.out.println("Timeout: " + options.timeout);
			// }
			//
			// } else {
			// System.err.println("Timeout not supported for protocol \"" +
			// options.protocol + "\" (ignored)");
			// }
		}

		return this;
	}

	@Override
	public void destroy(SyslogServerIF syslogServer) {
	}

	@Override
	public void event(Object session, SyslogServerIF syslogServer,
			SocketAddress socketAddress, SyslogServerEventIF event) {
		if (is_record)
			this.add_packet(event.getMessage());
		this.rcvd();
	}

	@Override
	public void exception(Object session, SyslogServerIF syslogServer,
			SocketAddress socketAddress, Exception exception) {
	}

	@Override
	public String get_default_schema() {
		return "syslog";
	}

	private int get_log_level(String level) {
		if (level.equalsIgnoreCase("debug"))
			return SyslogConstants.LEVEL_DEBUG;
		else if (level.equalsIgnoreCase("info"))
			return SyslogConstants.LEVEL_INFO;
		else if (level.equalsIgnoreCase("notice"))
			return SyslogConstants.LEVEL_NOTICE;
		else if (level.equalsIgnoreCase("warn"))
			return SyslogConstants.LEVEL_WARN;
		else if (level.equalsIgnoreCase("error"))
			return SyslogConstants.LEVEL_ERROR;
		else if (level.equalsIgnoreCase("critical"))
			return SyslogConstants.LEVEL_CRITICAL;
		else if (level.equalsIgnoreCase("alert"))
			return SyslogConstants.LEVEL_ALERT;
		else if (level.equalsIgnoreCase("emergency"))
			return SyslogConstants.LEVEL_EMERGENCY;
		else
			log.error("wrong syslog level " + level);
		return -1;
	}

	@Override
	public void initialize(SyslogServerIF syslogServer) {
	}

	@Override
	public boolean is_ready() {
		if (type == CLIENT) {
			return syslog != null;
		} else
			return syslogd != null && syslogd.getThread().isAlive();
	}

	@Override
	public boolean message_matched(Object msg, HashMap hm) {
		return true;
	}

	/**
	 * 发送debug级别syslog： clt.send_message(new String[]{"debug","message"});
	 * 如果报GStringImpl cannot be cast to java.lang.String异常，需要把message转化成String
	 * 级别包括：{debug,info,notice,warn,error,critical,alert,emergency}
	 */
	@Override
	public void send_message(Object msg) {
		if (type == SERVER) {
			log.error("syslog server cant't send message!");
			return;
		}

		List<String> ps = null;
		if (msg instanceof Object[])
			ps = (List<String>) Arrays.asList((String[]) msg);
		else
			ps = (List<String>) msg;

		if (ps.get(0).equalsIgnoreCase("backlog")) {
			syslog.backLog(get_log_level(ps.get(1)), ps.get(2), ps.get(3));
			return;
		}

		int level = get_log_level(ps.get(0));
		if (level != -1)
			syslog.log(level, ps.get(1));
		else
			throw new Error("Wrong syslog level " + ps.get(0));
	}

	@Override
	public void sessionClosed(Object session, SyslogServerIF syslogServer,
			SocketAddress socketAddress, boolean timeout) {
	}

	@Override
	public Object sessionOpened(SyslogServerIF syslogServer,
			SocketAddress socketAddress) {
		return null;
	}

	public void set_dateformat(String pattern) {
		syslog.getMessageProcessor().setDateFormatPattern(pattern.trim() + " ");
	}

	@Override
	public void startup() {
		if (type == SERVER) {
			SyslogServer.getThreadedInstance(this.transport);
		}
	}

	@Override
	public void teardown() {
		if (type == CLIENT) {
			syslog.shutdown();
		} else {
			syslogd.shutdown();
		}
	}
}
