package ate.test;

/*
 * #%L
 * iTDD UA Syslog
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.testcase.CsUATestcase;
import ate.ua.syslog.SyslogUA;

public class SyslogUATest extends CsUATestcase {
	@Test
	public void base() {
		SyslogUA svr = (SyslogUA) this
				.create_server("syslog://127.0.0.1:5000/udp");
		SyslogUA clt = (SyslogUA) this
				.create_client("syslog://127.0.0.1:5000/");
		start_all_ua();
		// clt.set_dateformat("MMM dd HH:mm:ss yyyy");
		clt.set_dateformat("yyyy-MM-dd HH:mm:ss");

		assertTrue(svr.is_ready());
		assertTrue(clt.is_ready());

		clt.send_message(new String[] { "debug", "hhhhhhhhhhh" });

		Object msg = svr.recv_message();

		assertTrue(msg instanceof String);
		svr.clear_q();

		for (int i = 0; i < 2; i++) {
			log.info("loop " + i);
			for (int p = 0; p < 100; p++) {
				String tmp = "loop " + i + " test " + p;
				clt.send_message(new String[] { "emergency", tmp });
				// log.info("send "+tmp);
				sleep(50);
			}
			assertTrue(svr.expect_rcvd_count(100, 10000));
			svr.clear_q();
		}
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		log.debug("after Method " + m);
		super.shutdown_all_ua();

		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
