import java.lang.reflect.Method;
import org.testng.annotations.*
import ate.testcase.CsUATestcase;
import ate.ua.syslog.SyslogUA;
import common.*
import org.testng.annotations.Test;

public class SyslogUATest extends CsUATestcase {
	@Test
	public void base() {
		log.info "$STEP base:"
		def svr = create_server("syslog://127.0.0.1:5000/udp");
		def clt = create_client("syslog://127.0.0.1:5000/");
		start_all_ua();
		clt.set_dateformat("yyyy-MM-dd HH:mm:ss");

		assertTrue(svr.is_ready());
		assertTrue(clt.is_ready());

		def sipnum=1
		def natnum=1
		def dipnum=1 
		def ipsrc=GenData.incrIp("11.0.0.0",1,sipnum)
		def ipnat=GenData.incrIp("110.0.0.1",1,natnum)
		def ipdip=GenData.incrIp("11.250.0.0",1,dipnum)
		def endport=1025+10
		for(int i=0;i<sipnum;i++) {
			log.info "loop $i"
			for(int p=1025;p<endport;p++) {
				def msg="%%01SEC/4/SESSION(l): -DevIP=172.16.5.168; Protocol:tcp; "+
						"${ipsrc[i]}:$p; ${ipnat[i]}:$p -->${ipdip[i]}:80; [2013/8/8 18:6:51 - 2013/8/8 18:6:52] Src VPN ID:0 Dst VPN ID:0; status:86"
				clt.send_message(["emergency", msg]);		
				sleep(10)		
			}
			assertTrue(svr.expect_rcvd_count(endport-1025, 10000));
			
			svr.clear_q();
		}

		
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		log.debug("after Method " + m);
		super.shutdown_all_ua();

		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
