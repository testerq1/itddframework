package ate.ua.modbus;

import java.util.Arrays;
import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import ate.ua.RunFailureException;
import ate.ua.UAOption;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;
import ate.util.X2X0;

import com.serotonin.modbus4j.base.BaseMessageParser;
import com.serotonin.modbus4j.ip.encap.EncapMessageParser;
import com.serotonin.modbus4j.ip.encap.EncapMessageRequest;
import com.serotonin.modbus4j.ip.encap.EncapMessageResponse;
import com.serotonin.modbus4j.ip.xa.XaMessageParser;
import com.serotonin.modbus4j.ip.xa.XaMessageRequest;
import com.serotonin.modbus4j.ip.xa.XaMessageResponse;
import com.serotonin.modbus4j.msg.ModbusRequest;
import com.serotonin.modbus4j.msg.ModbusResponse;
import com.serotonin.util.queue.ByteQueue;

public class ModbusUA extends AMinaUAImp{
    BaseMessageParser parser;
    private short nextTransactionId = 0;
    boolean encap=false;
    protected short getNextTransactionId() {
        return nextTransactionId++;
    }
    class UADecoder extends CumulativeProtocolDecoder {
        @Override
        protected boolean doDecode(IoSession session, IoBuffer msg,
                ProtocolDecoderOutput out) throws Exception {
            if(msg.remaining()<=0){
                return false;
            }
            byte[] bs=new byte[msg.remaining()];        
            msg.get(bs);
            out.write(parser.parseMessage(new ByteQueue(bs)));            
            return true;
        }
    }
    class UAEncoder extends ProtocolEncoderAdapter {
        @Override
        public void encode(IoSession session, Object message,
                ProtocolEncoderOutput out) throws Exception {
            byte[] bs=null;
            if(message instanceof ModbusRequest){
                ModbusRequest ipRequest = (ModbusRequest) message;                
                if (encap)
                    bs = new EncapMessageRequest(ipRequest).getMessageData();
                else
                    bs = new XaMessageRequest(ipRequest, getNextTransactionId()).getMessageData();                
            }else if(message instanceof ModbusResponse){
                ModbusResponse ipResponse = (ModbusResponse) message;                
                if (encap)
                    bs = new EncapMessageResponse(ipResponse).getMessageData();
                else
                    bs = new XaMessageResponse(ipResponse, getNextTransactionId()).getMessageData();                
            }else if(message instanceof String){
                bs=X2X0.hexs2b((String)message);
            }else if(message instanceof byte[]){
                bs=(byte[])message;
            }else
                throw new RunFailureException("type wrong:"+bs);
            
            log.debug("encode:{} {}",bs.length,Arrays.toString(bs));
            out.write(IoBuffer.wrap(bs));            
        }
    }
    @Override
    public ModbusUA build(UAOption option, Object value) {
        super.build(option, value);
        
        if (option == UA_URI) {
            encap="true".equalsIgnoreCase(this.get_query_para("encap"));
        }
        return this;
    }
    @Override
    public int get_port(){
        if(port<0)
            port=502;
        return port;
    }
    @Override
    public String get_transport(){
        if(transport==null||transport.length()<2)
            transport="tcp";
        return transport;
    }
    @Override
    protected void add_default_filter() {
        add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(new UAEncoder(),
                new UADecoder()));        
    }
    
    @Override
    public void startup(){
        super.startup();
        //Master是client
        if (encap)
            parser = new EncapMessageParser(type!=SERVER);
        else
            parser = new XaMessageParser(type!=SERVER); 
    }

    @Override
    public String get_default_schema() {
        return "modbus";
    }
    @Override
    public boolean message_matched(Object msg, HashMap hm) {
        return true;
    }

}
