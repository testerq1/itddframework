package ate.ua.modbus;

import org.testng.annotations.*;

import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.encap.EncapMessageRequest;
import com.serotonin.modbus4j.ip.encap.EncapMessageResponse;
import com.serotonin.modbus4j.msg.ReadCoilsRequest;
import com.serotonin.modbus4j.msg.ReadCoilsResponse;
import com.serotonin.util.queue.ByteQueue;

import ate.testcase.CsUATestcase;
import ate.util.X2X0;

public class ModbusUATest extends CsUATestcase{
    @Test
    public void test() throws ModbusTransportException{
        ModbusUA uas=this.create_server("modbus://"+siip+"/?encap=true",new ModbusUA());
        ModbusUA uac=this.create_client("modbus://"+sip+"/?encap=true",new ModbusUA());
        this.start_all_ua();
        assertTrue(uac.is_ready()&&uas.is_ready());
        
        STEP("Send request:");
        ReadCoilsRequest request = new ReadCoilsRequest(241, 98, 200);        
        uac.send_message(request);
        
        STEP("Recv request:");
        Object o= uas.recv_message();
        assertTrue(o instanceof EncapMessageRequest);
        ReadCoilsRequest req=(ReadCoilsRequest)((EncapMessageRequest)o).getModbusMessage();
        
        STEP("Send Response:");
        ReadCoilsResponse response=(ReadCoilsResponse)req.getResponseInstance(241);
        response.setData(X2X0.hexs2b("0a0b0c0d"));
        uas.send_message(response);
        
        STEP("Recv Response:");
        o= uac.recv_message();
        assertTrue(o instanceof EncapMessageResponse);        
        assertTrue(((EncapMessageResponse)o).getModbusMessage() instanceof ReadCoilsResponse);
        
        
    }

}
