package ate.test;

/*
 * #%L
 * iTDD UA Radius
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.tinyradius.packet.RadiusPacket;

import ate.testcase.CsUATestcase;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.UAClient;
import ate.ua.mina.UAServer;
import ate.ua.radius.RadiusUA;

public class RadiusUATest extends CsUATestcase {
	Object[][] SAMPLES = {
			{ "SVT_RADIUS_PARSER_FUNC_001_0", new HashMap<String, String>() {
				{
					put("User-Name", "13760439582");
					put("CHAP-Password", "07c42a8ac9bb36d2bcdc4ae8e05fcb8e8b");
					put("Calling-Station-Id", "00:26:82:B7:F5:BF");
					put("NAS-IP-Address", "192.168.1.88");
					put("Called-Station-Id", "00-17-7B-2B-D3-0C:zteopen");

				}
			} },
			{ "SVT_RADIUS_PARSER_FUNC_001_1", new HashMap<String, String>() {
				{
					put("User-Name", "13760439583");
					put("CHAP-Password", "07c42a8ac9bb36d2bcdc4ae8e05fcb8e8b");
					put("Calling-Station-Id", "00:26:82:B7:F5:BF");
					put("NAS-IP-Address", "192.168.1.88");
					put("Called-Station-Id", "00-17-7B-2B-D3-0C:zteopen");
				}
			} } };

	@Test(dataProvider="Data_TestStep",groups={"FUNC"})
	public void TestStep2(String name, HashMap<String, String> hm) {
		RadiusUA svrAcct = (RadiusUA) this
				.create_server("radius://"+siip+":1813/udp?type=acct");
		RadiusUA cltAcct = (RadiusUA) this
				.create_client("radius://"+sip+":1813/udp?type=acct");
		start_all_ua();

		hm.put("Acct-Status-Type", "Start");
		cltAcct.send_message(cltAcct.create_request(hm));

		RadiusPacket req = (RadiusPacket) svrAcct.recv_message("User-Name",
				hm.get("User-Name"));
		assertTrue(req != null);
		HashMap<String, String> hm1 = new HashMap<String, String>();
		hm1.put("type-code", svrAcct.ACCOUNTING_RESPONSE);
		svrAcct.send_message(svrAcct.create_response(hm1, req));
		RadiusPacket resp = (RadiusPacket) cltAcct.recv_message();
		assertTrue(resp != null);
		assertTrue(cltAcct.is_packet_type(resp, cltAcct.ACCOUNTING_RESPONSE));
		assertTrue(cltAcct.is_response(req, resp));
	}

	@DataProvider(name = "Data_TestStep")
	public Object[][] Data_TestStep() {
		return SAMPLES;
	}

	@Test(dataProvider="Data_TestStep",groups={"FUNC"})
	public void TestStep(String name, HashMap<String, String> hm) {
		RadiusUA svrAuth = (RadiusUA) this
				.create_server("radius://"+siip+":1812/udp");
		RadiusUA cltAuth = (RadiusUA) this
				.create_client("radius://"+sip+":1812/udp");
		start_all_ua();

		assertTrue(svrAuth.is_ready());
		assertTrue(cltAuth.is_ready());
		cltAuth.send_message(cltAuth.create_request(hm));

		Object req = svrAuth.recv_message("User-Name", hm.get("User-Name"));
		assertTrue(req != null);

		HashMap<String, String> hm1 = new HashMap<String, String>();
		hm1.put("type-code", svrAuth.ACCESS_ACCEPT);
		hm1.put("Framed-MTU", "1500");

		svrAuth.send_message(cltAuth.create_response(hm1, (RadiusPacket) req));

		RadiusPacket resp = (RadiusPacket) cltAuth.recv_message();
		assertTrue(resp != null);
		assertTrue(cltAuth.is_packet_type(resp, cltAuth.ACCESS_ACCEPT));
		assertTrue(cltAuth.has_attrs(resp, "Framed-MTU", "1500"));
	}

	@Test
	public void Test_AutoResp() {
		log.info(STEP+" Test_AutoResp");
		log.info(STEP+".1 init");
		RadiusUA svrAuth = (RadiusUA) this
				.create_server("radius://"+siip+":1812/udp");
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("type-code", svrAuth.ACCESS_ACCEPT);
		hm.put("Framed-MTU", "1500");
		svrAuth.set_auto_response(hm);
		
		RadiusUA cltAuth = (RadiusUA) this
				.create_client("radius://"+sip+":1812/udp");
		start_all_ua();		
		assertTrue(svrAuth.is_ready());
		assertTrue(cltAuth.is_ready());
		
		log.info(STEP+".2 clt send 1000 Msg");
		hm = new HashMap<String, String>();
		hm.put("User-Name", "13760439583");
		hm.put("CHAP-Password", "07c42a8ac9bb36d2bcdc4ae8e05fcb8e8b");
		hm.put("Calling-Station-Id", "00:26:82:B7:F5:BF");
		hm.put("NAS-IP-Address", "192.168.1.88");
		hm.put("Called-Station-Id", "00-17-7B-2B-D3-0C:zteopen");
		cltAuth.set_is_record(false);
		svrAuth.set_is_record(false);
		for (int i = 0; i < 1000; i++) {
			long name = 13760439583L + i;
			hm.put("User-Name", name + "");
			cltAuth.send_message(cltAuth.create_request(hm));
			//sleep(2);
		}
		
		log.info(STEP+".2 clt recv 1000 response");

		assertTrue("预期Client收到1000个响应",cltAuth.expect_rcvd_count(1000,20000));
	}

	@Test
	public void Test_Auth() {
		RadiusUA svr = (RadiusUA) this
				.create_server("radius://"+siip+":1812/udp");
		RadiusUA clt = (RadiusUA) this
				.create_client("radius://"+sip+":1812/udp");
		assertTrue(!clt.is_ready());
		assertTrue(!svr.is_ready());
		assertTrue(svr.get_io_handler() instanceof UAServer);
		assertTrue(clt.get_io_handler() instanceof UAClient);
		this.start_all_ua();
		assertTrue(clt.is_ready());
		assertTrue(svr.is_ready());

		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("User-Name", "13760439582");
		hm.put("& User-Name", "13760439583");
		hm.put("&1 User-Name", "13760439584");
		hm.put("CHAP-Password", "07c42a8ac9bb36d2bcdc4ae8e05fcb8e8b");
		hm.put("Calling-Station-Id", "00:26:82:B7:F5:BF");
		hm.put("NAS-IP-Address", "192.168.1.88");
		hm.put("Called-Station-Id", "00-17-7B-2B-D3-0C:zteopen");
		clt.send_message(clt.create_request(hm));
		RadiusPacket req = svr.recv_message("User-Name", "13760439582");
		assertTrue(req != null);

		hm.clear();
		hm.put("type-code", svr.ACCESS_ACCEPT);
		hm.put("Framed-MTU", "1500");
		svr.send_message(svr.create_response(hm, req));
		RadiusPacket resp = (RadiusPacket) clt.recv_message();
		assertTrue(resp != null);
		assertTrue(resp.getPacketType() == RadiusPacket.ACCESS_ACCEPT);
		assertTrue(resp.getAttribute("Framed-MTU").getAttributeValue()
				.equals("1500"));
	}

	@Test
	public void Test_Acct() {
		AMinaUAImp tmp = (AMinaUAImp) this
				.create_server("radius://"+siip+":1813/udp");
		assertTrue(tmp instanceof RadiusUA);
		RadiusUA svr = (RadiusUA) tmp;
		assertTrue(svr.get_io_handler() instanceof UAServer);

		svr.change_type(RadiusUA.UA_TYPE_ACCT);
		tmp = (AMinaUAImp) this.create_client("radius://"+sip+":1813/udp");
		assertTrue(tmp instanceof RadiusUA);
		RadiusUA clt = (RadiusUA) tmp;
		assertTrue(clt.get_io_handler() instanceof UAClient);
		this.start_all_ua();

		clt.change_type(RadiusUA.UA_TYPE_ACCT);
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("Acct-Status-Type", "Start");
		hm.put("User-Name", "13760439582");
		hm.put("Calling-Station-Id", "00:26:82:B7:F5:BF");
		hm.put("NAS-IP-Address", "192.168.1.88");
		hm.put("Called-Station-Id", "00-17-7B-2B-D3-0C:zteopen");
		clt.send_message(clt.create_request(hm));
		RadiusPacket req = svr.recv_message("User-Name", "13760439582");
		assertTrue(req != null);

		hm.clear();
		hm.put("type-code", svr.ACCOUNTING_RESPONSE);
		svr.send_message(svr.create_response(hm, req));
		RadiusPacket resp = (RadiusPacket) clt.recv_message();
		assertTrue(resp != null);
		assertTrue(resp.getPacketType() == RadiusPacket.ACCOUNTING_RESPONSE);
		assertTrue(resp.getPacketIdentifier() == req.getPacketIdentifier());
		log.debug("done");
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();

		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}

}
