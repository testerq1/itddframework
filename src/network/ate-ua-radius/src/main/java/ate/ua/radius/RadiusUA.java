package ate.ua.radius;

/*
 * #%L
 * iTDD UA Radius
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.tinyradius.attribute.RadiusAttribute;
import org.tinyradius.attribute.VendorSpecificAttribute;
import org.tinyradius.dictionary.AttributeType;
import org.tinyradius.dictionary.DefaultDictionary;
import org.tinyradius.dictionary.Dictionary;
import org.tinyradius.dictionary.DictionaryParser;
import org.tinyradius.dictionary.MemoryDictionary;
import org.tinyradius.dictionary.WritableDictionary;
import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.AccountingRequest;
import org.tinyradius.packet.RadiusPacket;
import org.tinyradius.util.RadiusException;

import ate.AbstractURIHandler;
import ate.ua.IAutoResponseMessage;
import ate.ua.UAOption;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;
import ate.util.GenData0;
import ate.util.X2X0;

/**
 * 支持多个相同属性
 * 
 * @author xiaoyong.huang
 * 
 */
public class RadiusUA extends AMinaUAImp<RadiusPacket> implements
		IAutoResponseMessage<IoSession, RadiusPacket> {
	public enum ACCTTYPE {
		OFF(8), ON(7), START(1), STOP(2), UPDATE(3);
		private int id;

		ACCTTYPE(int id) {
			this.id = id;
		};

		public int getID() {
			return id;
		}
	}

	class RadiusDecoder extends CumulativeProtocolDecoder {
		@Override
		protected boolean doDecode(IoSession session, IoBuffer in,
				ProtocolDecoderOutput out) throws Exception {
			if (in.remaining() >= 0) {
				RadiusPacket pkt = RadiusPacket.decodePacket(dict,
						in.asInputStream(), sharedSecret, null);
				out.write(pkt);
				return true;
			}
			return false;
		}
	}

	class RadiusEncoder extends ProtocolEncoderAdapter {
		@Override
		public void encode(IoSession session, Object message,
				ProtocolEncoderOutput out) throws Exception {
			if (message instanceof RadiusPacket) {
				RadiusPacket pkt = (RadiusPacket) message;
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				pkt.encodePacket(bos, sharedSecret, null);
				out.write(IoBuffer.wrap(bos.toByteArray()));
			}
		}
	}

	public static final String ACCESS_ACCEPT = "2";
	public static final int ACCESS_CHALLENGE = 11;
	public static final String ACCESS_REJECT = "3";
	public static final String ACCESS_REQUEST = "1";
	public static final int ACCOUNTING_MESSAGE = 10;
	public static final String ACCOUNTING_REQUEST = "4";
	public static final String ACCOUNTING_RESPONSE = "5";
	public static final String ACCOUNTING_STATUS = "6";
	public static final int COA_ACK = 44;
	public static final int COA_NAK = 45;
	public static final int COA_REQUEST = 43;
	public static final int DISCONNECT_ACK = 41;
	public static final int DISCONNECT_NAK = 42;
	public static final int DISCONNECT_REQUEST = 40; // RFC 2882
	public static final int PASSWORD_ACCEPT = 8;
	public static final int PASSWORD_REJECT = 9;
	public static final int PASSWORD_REQUEST = 7;
	public static final int RESERVED = 255;
	public static final int STATUS_ACCEPT = 47;
	public static final int STATUS_CLIENT = 13;

	public static final int STATUS_REJECT = 48;
	public static final int STATUS_REQUEST = 46;
	public static final int STATUS_SERVER = 12;
	public static final int UA_TYPE_ACCT = 2;
	public static final int UA_TYPE_AUTH = 1;;
	private String auth_prot = "chap";

	protected HashMap<String, String> autoRespAttr = null;

	private Dictionary dict = DefaultDictionary.getDefaultDictionary();
	private String sharedSecret = "testing123";

	// 在自动响应的时候，需要保存这个值
	private int type_code;
	private int ua_type = UA_TYPE_AUTH;

	@Override
	public void add_default_filter() {
		add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(
				new RadiusEncoder(), new RadiusDecoder()));
	}

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option != UA_TYPE)
			return this;

		List<String> list = get_query_paras("type");
		if (list == null || list.size() == 0)
			this.ua_type = UA_TYPE_AUTH;
		else {
			String s = list.get(0);
			if (s.toLowerCase().startsWith("acct"))
				ua_type = UA_TYPE_ACCT;
			else
				ua_type = UA_TYPE_AUTH;
		}
		return this;
	}

	/**
	 * 变化auth协议{chap, pap}
	 * 
	 * @param auth_protocol
	 */
	public void change_auth(String auth_protocol) {
		this.auth_prot = auth_protocol;
	}

	public WritableDictionary change_dictionary(String name) throws IOException {
		WritableDictionary instance = new MemoryDictionary();
		InputStream source = new FileInputStream(conf_path + name);
		DictionaryParser.parseDictionary(source, instance);
		dict=instance;
		
		return instance;
	}

	/**
	 * radius ua的类型{UA_TYPE_AUTH,UA_TYPE_ACCT}
	 * 
	 * @param ua_type
	 */
	public void change_type(int ua_type) {
		this.ua_type = ua_type;
	}

	/**
	 * 创建一个Radius报文，参数必须以属性对的形式指定<br>
	 * 如果是认证报文，必须指定Acct-Status-Type参数(start,stop,update,on,off)<br>
	 * 如果是计费报文，必须指定User-Name和(User-Password||CHAP-Password)参数<br>
	 * 默认校验方法是pap<br>
	 * 如果要添加2个相同的属性，用"&0 "开头<br>
	 * 
	 * @param paras
	 *            属性形式的报文
	 * @return 一个Radius报文
	 */
	public RadiusPacket create_request(HashMap<String, String> paras) {
		RadiusPacket pkt = null;
		String user = null;
		switch (this.ua_type) {
		case UA_TYPE_ACCT:
			user = paras.get("User-Name");
			String status_type = paras.get("Acct-Status-Type");
			pkt = new AccountingRequest(user, get_acct_status_type(status_type));
			break;
		case UA_TYPE_AUTH:
			user = paras.get("User-Name");
			String passwd = paras.get("User-Password");
			if (auth_prot.equalsIgnoreCase("chap")) {
				passwd = paras.get("CHAP-Password");
			}
			AccessRequest ar = new AccessRequest(user, passwd);
			ar.setAuthProtocol(auth_prot);
			pkt = ar;
			break;
		default:
			log.warn("ua type is not in (acct,auth).");
			break;
		}
		addAttribute(pkt,paras);
		
		return pkt;
	}
	
	private void addAttribute(RadiusPacket pkt,HashMap<String, String> paras){
		for (String key : paras.keySet()) {
			String tmp = key;
			if (!key.equals("User-Name") && !key.equals("Acct-Status-Type")
					&& !key.contains("Password")) {
				if (key.startsWith("&"))
					key = key.split(" ")[1];
				if(key.equalsIgnoreCase("Vendor-Specific")){
					VendorSpecificAttribute att=new VendorSpecificAttribute();
					byte[] bs=X2X0.hexs2b(paras.get(tmp));
					try {
						att.readAttribute(bs, 0, bs.length);
						pkt.addAttribute(att);
					} catch (RadiusException e) {
						e.printStackTrace();
					}
				}else 					
					pkt.addAttribute(key, paras.get(tmp));
			}
		}
		
	}
	
	/**
	 * Acct-Terminate-Cause={1,4,5,6},<br>
	 * 1：用户请求下线(User Request)<br>
	 * 4:空闲超时(Idle Timeout)<br>
	 * 5：会话超时(Session Timeout)<br>
	 * 6：DM下线(Admin Reset)<br>
	 * 
	 * @param paras
	 * @param req
	 * @return RadiusPacket
	 */
	public RadiusPacket create_response(HashMap<String, String> paras,
			RadiusPacket req) {
		String tc = paras.remove("type-code");
		if (tc != null)
			type_code = Integer.parseInt(tc);

		RadiusPacket pkt = new RadiusPacket(type_code,
				req.getPacketIdentifier());
		;
		for (String key : paras.keySet()) {
			if (key.equalsIgnoreCase("Acct-Terminate-Cause")
					&& paras.get("Acct-Terminate-Cause").equalsIgnoreCase(
							"random")) {
				String[] reasons = new String[] { "1", "4", "5", "6" };
				pkt.addAttribute(key,
						reasons[GenData0.random(0, reasons.length - 1)]);
			} else
				addAttribute(pkt,paras);
		}
		return pkt;
	}

	private int get_acct_status_type(String type) {
		type=type.toLowerCase();
		if (type.endsWith("start"))
			return ACCTTYPE.START.getID();
		else if (type.endsWith("stop"))
			return ACCTTYPE.STOP.getID();
		else if (type.endsWith("update"))
			return ACCTTYPE.UPDATE.getID();
		else if (type.endsWith("on"))
			return ACCTTYPE.ON.getID();
		else if (type.endsWith("off"))
			return ACCTTYPE.OFF.getID();
		else
			log.error("Acct-Status-Type should be one of (start,stop,update,on,off)");		
		
		return -1;
	}

	public RadiusPacket get_auto_resp_msg(IoSession session, RadiusPacket req) {
		if (this.autoRespAttr == null)
			return null;

		return this.create_response(this.autoRespAttr, req);
	}

	@Override
	public String get_default_schema() {
		return "radius";
	}

	/**
	 * 判断pkt里是否具有属性
	 * 
	 * @param pkt
	 * @param name
	 *            属性名
	 * @param value
	 *            属性值
	 * @return boolean
	 */
	public boolean has_attrs(RadiusPacket pkt, String name, String value) {
		return pkt.getAttribute(name).getAttributeValue().equals(value);
	}

	/**
	 * 是不是pkt类型
	 * 
	 * @param pkt
	 * @param pkt_type
	 *            {ACCESS_REQUEST,...,ACCOUNTING_STATUS}
	 * @return
	 */
	public boolean is_packet_type(RadiusPacket pkt, String pkt_type) {
		return pkt.getPacketType() == Integer.parseInt(pkt_type);
	}

	/**
	 * req和resp的ID是不是一致
	 * 
	 * @param req
	 *            请求包
	 * @param resp
	 *            响应包
	 * @return
	 */
	public boolean is_response(RadiusPacket req, RadiusPacket resp) {
		return req.getPacketIdentifier() == resp.getPacketIdentifier();
	}

	@Override
	public boolean message_matched(RadiusPacket msg, HashMap<String, Object> hm) {
		if ((this.ua_type == this.UA_TYPE_AUTH && msg.getPacketType() > 3)
				|| this.ua_type == this.UA_TYPE_ACCT
				&& msg.getPacketType() <= 3) {
			log.debug("消息类型不对：" + ua_type + " " + msg.getPacketType());
			return false;
		}
		if (hm == null || hm.size() == 0)
			return true;

		Iterator keys = hm.keySet().iterator();
		while (keys.hasNext()) {
			String typeName = keys.next().toString();
			String value = hm.get(typeName).toString();

			AttributeType type = dict.getAttributeTypeByName(typeName);
			if (type == null) {
				log.warn("unknown attribute type '" + typeName + "'");
				return false;
			}

			// 可能会带多个相同的属性，任一相等，即返回
			List rcvList = msg.getAttributes(type.getTypeCode());
			if (rcvList.size() == 0) {
				log.debug("收到的消息没有带属性" + typeName + " " + type.getTypeCode());
				return false;
			}
			boolean matched = true;
			for (Object tmp : rcvList) {
				RadiusAttribute rcvattr = (RadiusAttribute) tmp;
				if (rcvattr.getAttributeValue().equals(value)) {
					matched = true;
					break;
				} else
					matched = false;
			}

			if (!matched) {
				log.debug("收到的消息中属性" + typeName + "的值不符合预期：" + value);
				return false;
			}
		}

		return true;
	}

	public RadiusPacket recv_message(String type, String value) {
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put(type, value);
		return (RadiusPacket) this.recv_message(hm);
	}

	/**
	 * 预设值一组自动响应的参数,如果attrs为null，则不会自动响应
	 * 
	 * @param attrs
	 */
	public void set_auto_response(HashMap<String, String> attrs) {
		this.autoRespAttr = attrs;
	}

	public void set_shared_secret(String shared_secret) {
		this.sharedSecret = shared_secret;
	}
}