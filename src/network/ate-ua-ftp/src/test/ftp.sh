#!/bin/bash

ftp_host="172.16.5.111"
username="root"
password="admin"

ftp_connection_string="ftp://$username:$password@$ftp_host"

local_dir="/root/file/"
remote_dir="/file"

pid=`ps ax | grep lftp | grep -v grep | cut -d " " -f 1`

if [ x"$pid" != "x" ];then
        exit 0
fi

lftp -c "set ftp:list-options -a;
set net:timeout 5;
set net:max-retries 2;
set net:reconnect-interval-base 5;
set ftp:sync-mode on
set ftp:use-size  true;
open '$ftp_connection_string';
lcd $local_dir;
cd $remote_dir
mirror --verbose --reverse --verbose --only-missing;
"