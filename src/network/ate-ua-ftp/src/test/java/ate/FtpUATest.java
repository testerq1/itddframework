package ate;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPCmd;
import org.apache.commons.net.ftp.FTPFile;
import org.testng.annotations.*;

import ate.testcase.*;
import ate.ua.ftp.FTPUA;
import ate.ua.ftp.SMBUtil;

/**
 * Unit test for simple App.
 * 需要在localhost搭建ftp服务器
 */
public class FtpUATest extends CsUATestcase
{
    @Test
    public void smbtest(){
    	SMBUtil fh = new SMBUtil("C:\\Windows");    	
		assertTrue(fh.getNewest(SMBUtil.DIR_TYPE)!=null);
		assertTrue(fh.getNewestName(SMBUtil.DIR_TYPE, "2014-04-19")!=null);
    }
    
    @Test
    public void getNewest(){    	
    	FTPUA clt=create_client("ftp://root:admin@localhost");
    	this.start_all_ua();

    	String path=clt.getNewest(clt.DIR_TYPE);
    	ArrayList<String> ar=clt.walk(path);
    	assertTrue(null!=path);
    	assertTrue(ar.size()>0);    	
    }
    
    @Test
    public void basic(){    	
    	FTPUA svr=create_server("ftp://root:passwd@"+siip+":1234/.");
    	FTPUA clt=create_client("ftp://root:passwd@"+sip+":1234/.");
    	this.start_all_ua();

    	FTPClient ua=clt.get_client();
    	try {
    		FTPFile[] fs=ua.listFiles();
    		for(FTPFile tmp:fs){
    			System.out.print(tmp.getName()+"\t");
    		}
    		System.out.println();
    		assertTrue(fs!=null&&fs.length>4);
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    }
    
}
