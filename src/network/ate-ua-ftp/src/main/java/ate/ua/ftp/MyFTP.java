package ate.ua.ftp;

import java.io.IOException;

import ate.rm.dev.Host;

public class MyFTP {
	
	public static void main(String[] args) throws Exception {
		System.out.println(Host.HOME);
		args=new String[]{"c:\\"};
		
//		for(int i=0;i<args.length;i++)
//			System.out.println(i+" : "+args[i]);
		FTPUA ua = new FTPUA();
		ua.build(ua.UA_URI,
				"ftp://root:passwd@127.0.0.1:1234/.").build(ua.UA_TYPE, ua.SERVER);
		System.out.println("ftp server listen on port 1234, u/p:root:passwd");
		if(args.length>0)
			ua.set_home(args[0]);
		
		ua.startup();
		Thread.currentThread().join();

	}
}
