package ate.ua.ftp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;

import ate.AbstractURIHandler;
import ate.ua.RunFailureException;
import ate.ua.UAOption;

public class FTPUA extends AbstractURIHandler {
	public final static int FILE_TYPE = FTPFile.FILE_TYPE;
	public final static int DIR_TYPE = FTPFile.DIRECTORY_TYPE;
	
	private FtpServer server;
	private FTPClient client;
	private boolean isstarting = false;
	private String SYS_T = FTPClientConfig.SYST_UNIX;
	private StringPairUserManager um = StringPairUserManager
			.newInstance("root");

	/**
	 * ftpserver.user.admin.homedirectory=/ftproot
	 * ftpserver.user.admin.userpassword=admin
	 * ftpserver.user.admin.enableflag=true
	 * ftpserver.user.admin.writepermission=true ftpserver.user.admin.idletime=0
	 * ftpserver.user.admin.maxloginnumber=0
	 * ftpserver.user.admin.maxloginperip=0 ftpserver.user.admin.uploadrate=0
	 * ftpserver.user.admin.downloadrate=0
	 */
	@Override
	public FTPUA build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			if (port <= 0)
				port = 21;
			if (path.startsWith("/."))
				path = path.substring(1);
		}else if(option == UA_TYPE){			
			if (type == SERVER) {
				FtpServerFactory serverFactory = new FtpServerFactory();
				ListenerFactory factory = new ListenerFactory();
				factory.setPort(port);

				// replace the default listener
				serverFactory.addListener("default", factory.createListener());
				PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
				serverFactory.setUserManager(um);
				um.setUserAttribute(username, um.ATTR_PASSWORD,
						um.encryptPasswd(passwd));
				um.setUserAttribute(username, um.ATTR_HOME, path);

				um.setUserAttribute(username, um.ATTR_WRITE_PERM, "true");
				server = serverFactory.createServer();
			} else {
				client = new FTPClient();
				FTPClientConfig config = new FTPClientConfig();
				// config.setXXX(YYY); // change required options
				
				client.configure(config);
			}
		}

		return this;

	}
	
	public FTPClient get_client(){		
		return this.client;
	}
	
	@Override
	public void teardown() {
		if(type==SERVER){
			isstarting = false;
			server.stop();
		}else{
			try {				
				client.disconnect();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void startup() {
		try {
			if (type == SERVER) {
				server.start();
				isstarting = true;
			} else {
				client.connect(host, port);
				log.debug("Connected to " + server + ".");
				log.debug(client.getReplyString());
				client.login(username, passwd);
				// After connection attempt, verify success.
				int reply = client.getReplyCode();
				if (!FTPReply.isPositiveCompletion(reply)) {					
					throw new RunFailureException("FTP server refused connection.");					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public void set_home(String path){
		um.setUserAttribute(username, um.ATTR_HOME, path);
	}
	/**
	 * WFTPD时需要设置为FTPClientConfig.SYST_UNIX
	 * 
	 * @param syst
	 */

	public void setFTPSystype(String syst) {
		SYS_T = syst;		
	}
	public ArrayList<String> walk(String path){
		ArrayList<String> al=new ArrayList<String>();

		FTPFile[] ftpFiles=null;
		try {
			ftpFiles = client.listFiles(path);
		} catch (IOException e) {
			throw new RunFailureException(e);
		}

		for (FTPFile ftpFile : ftpFiles) {		
			if(ftpFile.getName().startsWith("."))
				continue;

			if (ftpFile.getType()==FTPFile.FILE_TYPE)
				al.add(path+File.separator+ftpFile.getName());
			else if (ftpFile.getType()==FTPFile.DIRECTORY_TYPE)
				al.addAll(walk(path+File.separator+ftpFile.getName()));			
		}		
		return al;
	}
	/**
	 * 获得最新的一个文件，按照给定的cal进行比较
	 * 只比较根目录
	 * @param type
	 *            文件或者文件夹 @see FTPHandler.FILE_TYPE FTPHandler.DIR_TYPE
	 * @param cal
	 *            可以指定一个供比较的日期
	 * @return 最新的文件名，如果没有则返回null,如果cal参数为null则返回比较的结果
	 * @throws Exception
	 */
	public String getNewest(int type, Calendar cal) {
		FTPFile[] ftpFiles=null;
		try {
			ftpFiles = client.listFiles();
		} catch (IOException e) {
			throw new RunFailureException(e);
		}
		String newest = "";

		for (FTPFile ftpFile : ftpFiles) {			
			if (ftpFile.getType() != type || ftpFile.getName().startsWith("."))
				continue;

			if (newest == "" || ftpFile.getTimestamp().after(cal)){
				newest = ftpFile.getName();
				cal=ftpFile.getTimestamp();
			}

			// FileUtils.byteCountToDisplaySize(ftpFile.getSize()));
		}
		return newest;
		// client.logout();
	}
	/**
	 * 获得最新的一个文件，只比较根目录
	 * @param type @see this.getNewest(int type, Calendar cal)
	 * @return   文件名
	 * @throws Exception
	 */
	public String getNewest(int type) {
		return getNewest(type, null);
	}
	/**
	 * 按名字比较，获得比指定的name字符串大的文件，只返回最大的一个
	 * @param type
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public String getNewestName(int type, String name) {
		FTPFile[] ftpFiles=null;
		try {
			ftpFiles = client.listFiles();
		} catch (IOException e) {
			throw new RunFailureException(e);
		}
		String newest = name;
		for (FTPFile ftpFile : ftpFiles) {
			if (ftpFile.getType() != type || ftpFile.getName().startsWith("."))
				continue;

			if (ftpFile.getName().compareTo(newest) > 0)
				newest = ftpFile.getName();

			// FileUtils.byteCountToDisplaySize(ftpFile.getSize()));
		}
		return newest;
	}
	/**
	 * 按名字比较，获得最老的一个
	 * @param type
	 * @return
	 * @throws Exception
	 */

	public String getNewestName(int type) {
		return getNewestName(type, "");
	}
	@Override
	public String get_default_schema() {
		return "ftp";
	}

	@Override
	public boolean is_ready() {
		if(type==SERVER)			
			return server != null && isstarting&&!server.isStopped()&&!server.isSuspended();
		else
			return client != null && client.isConnected()&&client.isAvailable();
	}
}
