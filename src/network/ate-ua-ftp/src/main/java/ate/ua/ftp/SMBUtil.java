package ate.ua.ftp;

import java.io.File;
import java.util.Calendar;

import org.apache.commons.net.ftp.FTPFile;

/** 
 * //Todo 支持JCIFS @see http://jcifs.samba.org/
 *                       http://www.muneebahmad.com/index.php/archives/48
 * @author ravi huang
 */
public class SMBUtil {
	public final static int FILE_TYPE = FTPFile.FILE_TYPE;
	public final static int DIR_TYPE = FTPFile.DIRECTORY_TYPE;
	private final File client;

	public SMBUtil(String file) {
		client = new File(file);
	}

	public boolean typeIsEqual(File file,int type){
		if(type==FILE_TYPE)
			return file.isFile();
		if(type==DIR_TYPE)
			return file.isDirectory();
		return false;
	}

	/**
	 * 获得最新的一个文件，按照给定的Calendar进行比较
	 * 只比较根目录
	 * @param type
	 *            文件或者文件夹 @see FTPHandler.FILE_TYPE FTPHandler.DIR_TYPE
	 * @param cal
	 *            可以指定一个供比较的日期
	 * @return 最新的文件名，如果没有则返回null,如果cal参数为null则返回比较的结果
	 * @throws Exception
	 */
	public String getNewest(int type, Calendar cal) {
		if(!client.exists())
			return null;

		File[] files = client.listFiles();
		File tmp = null;
		long max=(cal==null?0:cal.getTimeInMillis());

		for (File file : files) {
			if (!typeIsEqual(file,type) || file.getName().startsWith("."))
				continue;

			if (file.lastModified()>max){
				tmp = file;
				//System.out.println(max+" "+file.lastModified());
				max=file.lastModified();

			}
		}
		if(tmp==null)
			return null;

		return tmp.getName();
	}
	/**
	 * 获得最新的一个文件，只比较根目录
	 * @param type @see this.getNewest(int type, Calendar cal)
	 * @return   文件名
	 * @throws Exception
	 */
	public String getNewest(int type) {
		return getNewest(type, null);
	}
	/**
	 * 按名字比较，获得比指定的name字符串大的文件，只返回最大的一个
	 * @param type
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public String getNewestName(int type, String name) {	
		String newest = null;

		if(!client.exists())
			return newest;

		File[] files = client.listFiles();
		String max=name;

		for (File file : files) {
			if (!typeIsEqual(file,type) || file.getName().startsWith("."))
				continue;

			if (file.getName().compareTo(max) > 0){
				newest = file.getName();
				max=newest;
			}
		}
		return newest;
	}
	/**
	 * 按名字比较，获得最老的一个
	 * @param type
	 * @return
	 * @throws Exception
	 */

	public String getNewestName(int type) {
		return getNewestName(type, "");
	}

	public static void main(String[] args) throws Exception {
		SMBUtil fh = new SMBUtil("G:\\downloads");	
		System.out.println(1322833068000l>1337322638773l);
		System.out.println(fh.getNewest(SMBUtil.DIR_TYPE));
		System.out.println(fh.getNewestName(SMBUtil.DIR_TYPE, "我-04-19"));

	}
}