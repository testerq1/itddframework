package ate.ua.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import org.apache.ftpserver.FtpServerConfigurationException;
import org.apache.ftpserver.ftplet.Authentication;
import org.apache.ftpserver.ftplet.AuthenticationFailedException;
import org.apache.ftpserver.ftplet.Authority;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.User;
import org.apache.ftpserver.usermanager.AnonymousAuthentication;
import org.apache.ftpserver.usermanager.Md5PasswordEncryptor;
import org.apache.ftpserver.usermanager.PasswordEncryptor;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.ftpserver.usermanager.UsernamePasswordAuthentication;
import org.apache.ftpserver.usermanager.impl.AbstractUserManager;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.ConcurrentLoginPermission;
import org.apache.ftpserver.usermanager.impl.ConcurrentLoginRequest;
import org.apache.ftpserver.usermanager.impl.PropertiesUserManager;
import org.apache.ftpserver.usermanager.impl.TransferRatePermission;
import org.apache.ftpserver.usermanager.impl.TransferRateRequest;
import org.apache.ftpserver.usermanager.impl.WritePermission;
import org.apache.ftpserver.usermanager.impl.WriteRequest;
import org.apache.ftpserver.util.BaseProperties;
import org.apache.ftpserver.util.IoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringPairUserManager extends AbstractUserManager {
	 private final Logger LOG = LoggerFactory
	            .getLogger(PropertiesUserManager.class);

	    private final static String PREFIX = "ftpserver.user.";

	    private BaseProperties userDataProp=new BaseProperties();

	    private static PasswordEncryptor passwordEncryptor = new Md5PasswordEncryptor();

	    /**
	     * Internal constructor, do not use directly. Use {@link PropertiesUserManagerFactory} instead.
	     */
	    private StringPairUserManager(String adminName,PasswordEncryptor passwordEncryptor) {
	        super(adminName, passwordEncryptor);	        
	    }

	    public static StringPairUserManager newInstance(String adminName){
	    	return new StringPairUserManager(adminName,passwordEncryptor); 
	    }
	    
	   /* Delete an user. Removes all this user entries from the properties. After
	     * removing the corresponding from the properties, save the data.
	     */
	    public void delete(String usrName) throws FtpException {
	        // remove entries from properties
	        String thisPrefix = PREFIX + usrName + '.';
	        Enumeration<?> propNames = userDataProp.propertyNames();
	        ArrayList<String> remKeys = new ArrayList<String>();
	        while (propNames.hasMoreElements()) {
	            String thisKey = propNames.nextElement().toString();
	            if (thisKey.startsWith(thisPrefix)) {
	                remKeys.add(thisKey);
	            }
	        }
	        Iterator<String> remKeysIt = remKeys.iterator();
	        while (remKeysIt.hasNext()) {
	            userDataProp.remove(remKeysIt.next());
	        }	       
	    }
	    
	    public String encryptPasswd(String passwd){
	    	return this.passwordEncryptor.encrypt(passwd);
	    }

	    /**
	     * Get user password. Returns the encrypted value.
	     * 
	     * <pre>
	     * If the password value is not null
	     *    password = new password 
	     * else 
	     *   if user does exist
	     *     password = old password
	     *   else 
	     *     password = &quot;&quot;
	     * </pre>
	     */
	    private String getPassword(User usr) {
	        String name = usr.getName();
	        String password = usr.getPassword();

	        if (password != null) {
	            password = getPasswordEncryptor().encrypt(password);
	        } else {
	            String blankPassword = getPasswordEncryptor().encrypt("");

	            if (doesExist(name)) {
	                String key = PREFIX + name + '.' + ATTR_PASSWORD;
	                password = userDataProp.getProperty(key, blankPassword);
	            } else {
	                password = blankPassword;
	            }
	        }
	        return password;
	    }

	    /**
	     * Get all user names.
	     */
	    public String[] getAllUserNames() {
	        // get all user names
	        String suffix = '.' + ATTR_HOME;
	        ArrayList<String> ulst = new ArrayList<String>();
	        Enumeration<?> allKeys = userDataProp.propertyNames();
	        int prefixlen = PREFIX.length();
	        int suffixlen = suffix.length();
	        while (allKeys.hasMoreElements()) {
	            String key = (String) allKeys.nextElement();
	            if (key.endsWith(suffix)) {
	                String name = key.substring(prefixlen);
	                int endIndex = name.length() - suffixlen;
	                name = name.substring(0, endIndex);
	                ulst.add(name);
	            }
	        }

	        Collections.sort(ulst);
	        return ulst.toArray(new String[0]);
	    }

	    /**
	     * Load user data.
	     */
	    public User getUserByName(String userName) {
	        if (!doesExist(userName)) {
	            return null;
	        }

	        String baseKey = PREFIX + userName + '.';
	        BaseUser user = new BaseUser();
	        user.setName(userName);
	        user.setEnabled(userDataProp.getBoolean(baseKey + ATTR_ENABLE, true));
	        user.setHomeDirectory(userDataProp
	                .getProperty(baseKey + ATTR_HOME, "/"));

	        List<Authority> authorities = new ArrayList<Authority>();

	        if (userDataProp.getBoolean(baseKey + ATTR_WRITE_PERM, false)) {
	            authorities.add(new WritePermission());
	        }

	        int maxLogin = userDataProp.getInteger(baseKey + ATTR_MAX_LOGIN_NUMBER,
	                0);
	        int maxLoginPerIP = userDataProp.getInteger(baseKey
	                + ATTR_MAX_LOGIN_PER_IP, 0);

	        authorities.add(new ConcurrentLoginPermission(maxLogin, maxLoginPerIP));

	        int uploadRate = userDataProp.getInteger(
	                baseKey + ATTR_MAX_UPLOAD_RATE, 0);
	        int downloadRate = userDataProp.getInteger(baseKey
	                + ATTR_MAX_DOWNLOAD_RATE, 0);

	        authorities.add(new TransferRatePermission(downloadRate, uploadRate));

	        user.setAuthorities(authorities);

	        user.setMaxIdleTime(userDataProp.getInteger(baseKey
	                + ATTR_MAX_IDLE_TIME, 0));

	        return user;
	    }

	    /**
	     * User existance check
	     */
	    public boolean doesExist(String name) {
	        String key = PREFIX + name + '.' + ATTR_HOME;
	        return userDataProp.containsKey(key);
	    }

	    /**
	     * User authenticate method
	     */
	    public User authenticate(Authentication authentication)
	            throws AuthenticationFailedException {
	        if (authentication instanceof UsernamePasswordAuthentication) {
	            UsernamePasswordAuthentication upauth = (UsernamePasswordAuthentication) authentication;

	            String user = upauth.getUsername();
	            String password = upauth.getPassword();

	            if (user == null) {
	                throw new AuthenticationFailedException("Authentication failed");
	            }

	            if (password == null) {
	                password = "";
	            }

	            String storedPassword = userDataProp.getProperty(PREFIX + user
	                    + '.' + ATTR_PASSWORD);

	            if (storedPassword == null) {
	                // user does not exist
	                throw new AuthenticationFailedException("Authentication failed");
	            }

	            if (getPasswordEncryptor().matches(password, storedPassword)) {
	                return getUserByName(user);
	            } else {
	                throw new AuthenticationFailedException("Authentication failed");
	            }

	        } else if (authentication instanceof AnonymousAuthentication) {
	            if (doesExist("anonymous")) {
	                return getUserByName("anonymous");
	            } else {
	                throw new AuthenticationFailedException("Authentication failed");
	            }
	        } else {
	            throw new IllegalArgumentException(
	                    "Authentication not supported by this user manager");
	        }
	    }

	    /**
	     * Close the user manager - remove existing entries.
	     */
	    public synchronized void dispose() {
	        if (userDataProp != null) {
	            userDataProp.clear();
	            userDataProp = null;
	        }
	    }
	    
	    public void setUserAttribute(String uname,String key,String value){
	    	userDataProp.put(PREFIX+uname+"."+key, value);
	    }
	    
		@Override
		public void save(User arg0) throws FtpException {
			// TODO Auto-generated method stub
			
		}
}
