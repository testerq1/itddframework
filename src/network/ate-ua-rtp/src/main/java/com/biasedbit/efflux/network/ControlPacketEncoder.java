/*
 * Copyright 2010 Bruno de Carvalho
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.biasedbit.efflux.network;

import com.biasedbit.efflux.logging.Logger;
import com.biasedbit.efflux.packet.CompoundControlPacket;
import com.biasedbit.efflux.packet.ControlPacket;
import io.netty.buffer.*;
import io.netty.channel.*;
import io.netty.channel.ChannelHandler.Sharable;

import java.util.List;
import io.netty.handler.codec.MessageToMessageEncoder;

/**
 * @author <a href="http://bruno.biasedbit.com/">Bruno de Carvalho</a>
 */
@Sharable
public class ControlPacketEncoder extends MessageToMessageEncoder<Object> {

    // constants ------------------------------------------------------------------------------------------------------

    protected static final Logger LOG = Logger.getLogger(ControlPacketEncoder.class);

    // constructors ---------------------------------------------------------------------------------------------------

    private ControlPacketEncoder() {
    }

    // public static methods ------------------------------------------------------------------------------------------

    public static ControlPacketEncoder getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
	protected void encode(ChannelHandlerContext ctx, Object msg, List<Object> out) throws Exception {		
        try {
            if (msg instanceof ControlPacket) {
            	out.add(((ControlPacket) msg).encode());
            } else if (msg instanceof CompoundControlPacket) {            	
                List<ControlPacket> packets = ((CompoundControlPacket) msg).getControlPackets();
                ByteBuf[] buffers = new ByteBuf[packets.size()];
                for (int i = 0; i < buffers.length; i++) {
                    buffers[i] = packets.get(i).encode();
                }
                out.add(Unpooled.wrappedBuffer(buffers));
            }
        } catch (Exception e1) {
            LOG.error("Failed to encode compound RTCP packet to send.", e1);
        }

        // Otherwise do nothing.
    }

    // private classes ------------------------------------------------------------------------------------------------

    private static final class InstanceHolder {
        private static final ControlPacketEncoder INSTANCE = new ControlPacketEncoder();
    }
}
