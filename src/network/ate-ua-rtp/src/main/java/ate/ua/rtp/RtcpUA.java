package ate.ua.rtp;

/*
 * #%L
 * iTDD UA RTP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import ate.AbstractURIHandler;
import ate.ua.UAOption;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;

import com.biasedbit.efflux.packet.CompoundControlPacket;
import com.biasedbit.efflux.packet.ControlPacket;

public class RtcpUA extends AMinaUAImp<List<ControlPacket>> {

	class RtcpDecoder extends CumulativeProtocolDecoder {

		@Override
		protected boolean doDecode(IoSession session, IoBuffer in,
				ProtocolDecoderOutput out) throws Exception {
			log.debug("{} doDecode", this);
			if (in.remaining() >= 0) {
				byte[] bs = new byte[in.remaining()];
				in.get(bs);

				ByteBuf buffer = Unpooled.wrappedBuffer(bs);
				if ((buffer.readableBytes() % 4) > 0) {
					throw new IllegalArgumentException(
							"Invalid RTCP packet length: expecting multiple of 4 and got "
									+ buffer.readableBytes());
				}
				List<ControlPacket> controlPacketList = new ArrayList<ControlPacket>(
						2);

				while (buffer.readableBytes() > 0) {
					try {
						controlPacketList.add(ControlPacket.decode(buffer));
					} catch (Exception e1) {
						log.debug(
								"Exception caught while decoding RTCP packet.",
								e1);
					}
				}

				out.write(controlPacketList);
				return true;
			}
			return false;
		}
	}

	class RtcpEncoder extends ProtocolEncoderAdapter {
		@Override
		public void encode(IoSession session, Object msg,
				ProtocolEncoderOutput out) throws Exception {
			log.debug("{} encode", this);

			if (msg instanceof ControlPacket) {
				IoBuffer buf = IoBuffer.wrap(((ControlPacket) msg).encode()
						.array());
				out.write(buf);

			} else if (msg instanceof CompoundControlPacket) {
				List<ControlPacket> packets = ((CompoundControlPacket) msg)
						.getControlPackets();

				ByteArrayOutputStream bsout = new ByteArrayOutputStream();
				for (int i = 0; i < packets.size(); i++) {
					bsout.write(packets.get(i).encode().array());
				}

				out.write(IoBuffer.wrap(bsout.toByteArray()));
			}

		}
	}

	@Override
	protected void add_default_filter() {
		add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(
				new RtcpEncoder(), new RtcpDecoder()));

	}

	// @Override
	// public void initChannel(Channel ch) {
	// ChannelPipeline pipeline = ch.pipeline();
	// pipeline.addLast("decoder", new ControlPacketDecoder());
	// pipeline.addLast("encoder", ControlPacketEncoder.getInstance());
	// }

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option == UA_TYPE)
			this.transport = "udp";
		// set_filters_factory(new AFilterFactory() {
		// @Override
		// public List<ChannelHandler> create_filters() {
		// return new ArrayList<ChannelHandler>(Arrays.asList(
		// new ControlPacketDecoder(),
		// ControlPacketEncoder.getInstance()));
		// }
		// });
		return this;
	}

	@Override
	public String get_default_schema() {
		return "rtcp";
	}

	@Override
	public boolean message_matched(List<ControlPacket> msg,
			HashMap<String, Object> hm) {
		return true;
	}

}
