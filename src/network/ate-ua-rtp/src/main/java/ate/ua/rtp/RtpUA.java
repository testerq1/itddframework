package ate.ua.rtp;

/*
 * #%L
 * iTDD UA RTP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import ate.AbstractURIHandler;
import ate.ua.UAOption;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;

import com.biasedbit.efflux.packet.DataPacket;

//public class RtpUA extends ANettyUAImp<DataPacket> {
public class RtpUA extends AMinaUAImp<DataPacket> {
	// comment for netty
	// class RtpDecoder extends SimpleChannelInboundHandler<DatagramPacket> {
	// @Override
	// public void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) {
	// log.debug("{} decode DatagramPacket {}", RtpUA.this, msg);
	// DataPacket pkt = DataPacket.decode(msg.content());
	// add_packet(new AtePacket(ctx.channel(), pkt));
	// }
	// }
	//
	// class RtpEncoder extends MessageToMessageEncoder<DataPacket> {
	// @Override
	// protected void encode(ChannelHandlerContext ctx, DataPacket msg,
	// List<Object> out) throws Exception {
	// log.debug("{} encode {}", RtpUA.this, msg);
	// out.add(msg.encode());
	// }
	// }
	class RtpDecoder extends CumulativeProtocolDecoder {

		@Override
		protected boolean doDecode(IoSession session, IoBuffer in,
				ProtocolDecoderOutput out) throws Exception {
			log.debug("{} doDecode", this);
			if (in.remaining() >= 0) {
				byte[] bs = new byte[in.remaining()];
				in.get(bs);
				out.write(DataPacket.decode(bs));
				return true;
			}
			return false;
		}
	}

	class RtpEncoder extends ProtocolEncoderAdapter {
		@Override
		public void encode(IoSession session, Object message,
				ProtocolEncoderOutput out) throws Exception {
			log.debug("{} encode", this);
			if (message instanceof DataPacket) {
				byte[] bs = ((DataPacket) message).encode().array();
				IoBuffer buf = IoBuffer.wrap(bs);
				out.write(buf);
			}
		}
	}

	private int seq = 0;

	@Override
	protected void add_default_filter() {
		add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(
				new RtpEncoder(), new RtpDecoder()));

	}

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option == UA_TYPE)
			this.transport = "udp";
		// comment for netty
		// set_filters_factory(new AFilterFactory() {
		// @Override
		// public List<ChannelHandler> create_filters() {
		// return new ArrayList<ChannelHandler>(Arrays.asList(
		// new RtpDecoder(),
		// new RtpEncoder()));
		// }
		// });
		return this;
	}

	/**
	 * hm=["seq":1,"pt":8,"ssrc":69,"data":"abc"]
	 * 
	 * @param paras
	 * @return
	 */
	public DataPacket create_message(HashMap<String, Object> hm) {
		DataPacket packet = new DataPacket();
		if (hm.get("seq") != null)
			packet.setSequenceNumber((Integer) hm.get("seq"));
		else
			packet.setSequenceNumber(seq++);
		if (hm.get("pt") != null)
			packet.setPayloadType((Integer) hm.get("pt"));
		else
			packet.setPayloadType(8);

		if (hm.get("ssrc") != null)
			packet.setSsrc((Integer) hm.get("ssrc"));
		else
			packet.setSsrc(69);

		Object data = hm.get("data");
		if (data == null)
			return packet;

		if (data instanceof byte[])
			packet.setData((byte[]) data);
		else
			packet.setData(data.toString().getBytes(charset));

		return packet;
	}

	@Override
	public String get_default_schema() {
		return "rtp";
	}

	@Override
	public boolean message_matched(DataPacket msg, HashMap<String, Object> hm) {
		return true;
	}

}
