package ate.test;

/*
 * #%L
 * iTDD UA RTP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.biasedbit.efflux.packet.DataPacket;

import ate.testcase.CsUATestcase;
import ate.ua.mina.*;
import ate.ua.rtp.RtpUA;

public class RtpUATest extends CsUATestcase {
	@Test
	public void send_rcv() {
		log.info(STEP+" send_rcv");
		log.info(STEP+".1 send_rcv: ");
		RtpUA svr = (RtpUA)this.create_server("rtp://"+sip+":1234/");
		assertTrue(svr.get_io_handler() instanceof UAServer);
		
		RtpUA clt = (RtpUA) this.create_client("rtp://"+sip+":1234/");
		assertTrue(clt.get_io_handler() instanceof UAClient);
		this.start_all_ua();
		
		//svr.connect(clt);
		
		assertTrue(svr.is_ready()&&clt.is_ready());
		
		log.info(STEP+".2 client send message:");
		HashMap<String,Object> hm= new HashMap<String,Object>(){
			{
				put("seq",1);
				put("pt", 126);
				put("ssrc",70);
				put("data","12345");
			}
		};
		clt.send_message(clt.create_message(hm));
		
		DataPacket req = svr.recv_message();
		assertTrue(req!=null && req.getSequenceNumber()==1&&req.getPayloadType()==126
				&&req.getSsrc()==70&&req.getData().toString(charset).equals("12345"));
		
		log.info(STEP+".3 server send 1234567:");
		hm.put("seq", 2);
		hm.put("data","abcdef");
		svr.send_message(svr.create_message(hm));
		
		req=clt.recv_message();
		assertTrue(req!=null && req.getSequenceNumber()==2&&req.getPayloadType()==126
				&&req.getSsrc()==70&&req.getData().toString(charset).equals("abcdef"));
		//isIterativeMode=true;
		wait_yes();
		
//		log.info(STEP+".4 reconnect all ua:");
//		this.reconnect_all_ua();	
//		//svr.connect(clt);
//		assertTrue(svr.is_ready());
//		assertTrue(clt.is_ready());
//		
//		hm.put("seq", 3);
//		hm.put("data","1234567");
//		clt.send_message(clt.create_message(hm));
//		
//		req = svr.recv_message();
//		assertTrue(req!=null && req.getSequenceNumber()==3&&req.getPayloadType()==126
//				&&req.getSsrc()==70&&req.getData().toString(charset).equals("1234567"));
//		
//		log.info(STEP+".5 server send 1234567h:");
//		hm.put("seq", 4);
//		hm.put("data","abcdefg");
//		svr.send_message(svr.create_message(hm));
//		
//		req=clt.recv_message();
//		assertTrue(req!=null && req.getSequenceNumber()==4&&req.getPayloadType()==126
//				&&req.getSsrc()==70&&req.getData().toString(charset).equals("abcdefg"));
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
