package ate.ua.websocket;

import org.testng.annotations.Test;

import ate.testcase.CsUATestcase;

public class WSTest extends CsUATestcase{
  @Test
  public void f() {
      WebsocketUA svr = create_server("ws://"+siip+":5000/events",new WebsocketUA());        
      WebsocketUA clt = create_client("ws://"+sip+":5000/events",new WebsocketUA());
      svr.startup();
      
      sleep(5000);
      
      clt.startup();
      clt.send_message("123456");
      
      Object s=svr.recv_message();
      assertTrue("123456".equals(s),s);
      
      svr.send_message("abcdefgh");
      s=clt.recv_message();
      assertTrue("abcdefgh".equals(s),s);
  }
}
