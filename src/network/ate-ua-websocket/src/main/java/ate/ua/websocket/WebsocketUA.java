package ate.ua.websocket;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.concurrent.Future;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

import ate.ua.AbstractPacketUAImp;
import ate.ua.AtePacket;
import ate.ua.RunFailureException;

/**
 * https://blog.openshift.com/how-to-build-java-websocket-applications-using-the-jsr-356-api/
 * @author Ravi Huang
 *
 */
public class WebsocketUA extends AbstractPacketUAImp{
    WebSocketClient client;
    Server server;
    Session session;
    
    @Override
    public void teardown() {
        try {
            if(this.type==SERVER){
                server.stop();
            }else{
                session.close();
                client.stop();           
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public int get_port(){
        if(port<=0)
            port=80;
        return port;
    }
    @Override
    public String get_path(){
        if(path==null||path.length()==0)
            path="/";
        return path;
    }
    @Override
    public void startup() {
        if(this.type==SERVER){
            server = new Server();
            ServerConnector connector = new ServerConnector(server);
            connector.setHost(host);
            connector.setPort(this.get_port());
            server.addConnector(connector);

            // Setup the basic application "context" for this application at "/"
            // This is also known as the handler tree (in jetty speak)
            ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            context.setContextPath("/");
            server.setHandler(context);
            
            // Add a websocket to a specific path spec
            ServletHolder holderEvents = new ServletHolder("ws-events", EventServlet.class);
            context.addServlet(holderEvents, this.get_path()+"/*");

            try
            {
                server.start();
               // server.dump(System.err);
                
            }
            catch (Throwable t)
            {
                t.printStackTrace();
            }  
        }else{
            URI uri = URI.create("ws://"+host+":"+this.get_port()+this.get_path());

            client = new WebSocketClient();
            try
            {                
                client.start();
                // The socket that receives events
                EventSocket socket = new EventSocket();
                // Attempt Connect
                session = client.connect(socket,uri).get();
                // Wait for Connect
                log.info(session+"");
            }
            catch (Throwable t)
            {
                t.printStackTrace();
            }
        }
        
    }
    
//    @Override
//    public T get_message() {
//        AtePacket<Session> pkt=(AtePacket<Session>)super.get_message();
//        if(pkt!=null)
//            session=pkt.getSession();
//        else
//            return null;
//        
//        return (T)pkt.getPacket();
//    }
    
    @Override
    public String get_default_schema() {
        return "ws";
    }

    @Override
    public boolean is_ready() {        
        return (server!=null&&server.isStarted())||client!=null;
    }

 
    @Override
    public boolean message_matched(Object msg, HashMap hm) {
        return true;
    }

    @Override
    public void send_message(Object msg) {
        try {
            session.getRemote().sendString(msg.toString());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    @SuppressWarnings("serial")
    class EventServlet extends WebSocketServlet
    {
        @Override
        public void configure(WebSocketServletFactory factory)
        {
            factory.register(EventSocket.class);
        }
    }
    
    class EventSocket extends WebSocketAdapter
    {
        @Override
        public void onWebSocketConnect(Session sess)
        {
            super.onWebSocketConnect(sess);
            System.out.println("Socket Connected: " + sess);
        }
        
        @Override
        public void onWebSocketText(String message)
        {
            super.onWebSocketText(message);
            WebsocketUA.this.add_packet(message);
        }
        
        @Override
        public void onWebSocketClose(int statusCode, String reason)
        {
            super.onWebSocketClose(statusCode,reason);
            System.out.println("Socket Closed: [" + statusCode + "] " + reason);
        }
        
        @Override
        public void onWebSocketError(Throwable cause)
        {
            super.onWebSocketError(cause);
            cause.printStackTrace(System.err);
        }
    }
}
