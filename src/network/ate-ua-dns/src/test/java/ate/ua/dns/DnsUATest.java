package ate.ua.dns;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.testng.annotations.Test;
import org.xbill.DNS.*;
import org.xbill.DNS.utils.base64;

import com.google.common.net.HostAndPort;
import com.spotify.dns.DnsException;
import com.spotify.dns.DnsSrvResolver;
import com.spotify.dns.DnsSrvResolvers;

import ate.testcase.*;

public class DnsUATest extends UATestcase {
	@Test
	public void update() throws IOException{
		Name zone = Name.fromString("ravihuang.net.");
		Name host = Name.fromString("ppp", zone);
		Update update = new Update(zone);
		update.replace(host, Type.A, 5, "172.16.5.133");
		

		Resolver res = new SimpleResolver("172.16.5.114");
		String s=new String(base64.fromString("uYR98pFTGsm67NAkGrcvrg=="));
		
		res.setTSIGKey(new TSIG(Name.fromString("DDNS_UPDATED."), base64.fromString("uYR98pFTGsm67NAkGrcvrg==")));
		res.setTCP(false);
		

		Message response = res.send(update);
		
	}
	
	public void mydig(){
		log.info("mydig");
		DnsUA ua=this.create_ua("dns://8.8.8.8/udp?tsig=name:z0pll56C4cwLXYd2HG6WsQ==", DnsUA.class);
		ua.startup();
		Message msg=ua.dig("www.baidu.com");
		log.info(msg+"");
	}
	@Test
	public void dig(){
		log.info("dig");
		String[] argv=new String[]{"@172.16.5.114","pxe.ravihuang.net"};
		try {
			Dig.main(argv);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void ttt() {
		DnsSrvResolver resolver = DnsSrvResolvers.newBuilder()
				.cachingLookups(true).retainingDataOnFailures(true)
				.dnsLookupTimeoutMillis(1000).build();

		boolean quit = false;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		try {
			while (!quit) {
				System.out.print("Enter a SRV name: ");
				String line = in.readLine();

				if (line == null) {
					quit = true;
				} else {

					List<HostAndPort> nodes = resolver.resolve(line);

					for (HostAndPort node : nodes) {
						System.out.println(node);
					}

				}
			}
		} catch (DnsException e) {
			e.printStackTrace(System.out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
