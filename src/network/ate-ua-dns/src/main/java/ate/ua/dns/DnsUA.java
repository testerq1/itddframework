package ate.ua.dns;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.xbill.DNS.*;

import ate.AbstractURIHandler;
import ate.ua.RunFailureException;
import ate.ua.UAOption;

public class DnsUA extends AbstractURIHandler {
	boolean tcp = false;
	SimpleResolver resolver;
	
	@Override
	public DnsUA build(UAOption option, Object value) {
		super.build(option, value);

		if (option == UA_URI) {
			if (host != null && host.equals("0.0.0.0"))
				host = null;
			if (port <= 0)
				port = 53;

			if (path.contains("tcp"))
				tcp = true;
		}
		return this;
	}

	public Message dig(String cmd) {
		String[] argv = cmd.split(" ");
		int arg = 0;
		Name name=null;
		int type = Type.A, dclass = DClass.IN;

		if (argv.length < 1) {
			throw new RunFailureException(dig_usage());
		}

		try {
			arg = 0;
			String nameString = argv[arg++];
			if (nameString.equals("-x")) {
				name = ReverseMap.fromAddress(argv[arg++]);
				type = Type.PTR;
				dclass = DClass.IN;
			} else {
				name = Name.fromString(nameString, Name.root);
				type = Type.value(argv[arg]);
				if (type < 0)
					type = Type.A;
				else
					arg++;

				dclass = DClass.value(argv[arg]);
				if (dclass < 0)
					dclass = DClass.IN;
				else
					arg++;
			}
		} catch(ArrayIndexOutOfBoundsException e){
			if (name == null)
				throw new RunFailureException(dig_usage());
		} catch (Exception e) {
			throw new RunFailureException(e);
		}

		try {
			
			while (argv.length>arg && argv[arg].startsWith("-")
					&& argv[arg].length() > 1) {
				switch (argv[arg].charAt(1)) {
				case 'b':
					String addrStr;
					if (argv[arg].length() > 2)
						addrStr = argv[arg].substring(2);
					else
						addrStr = argv[++arg];
					try {
						resolver.setLocalAddress(InetAddress.getByName(addrStr));
					} catch (Exception e) {
						throw new RunFailureException("Invalid address");
					}
					break;

				case 'k':
					String key;
					if (argv[arg].length() > 2)
						key = argv[arg].substring(2);
					else
						key = argv[++arg];
					resolver.setTSIGKey(TSIG.fromString(key));
					break;

				case 't':
					resolver.setTCP(true);
					break;

				case 'i':
					resolver.setIgnoreTruncation(true);
					break;

				case 'e':
					String ednsStr;
					int edns;
					if (argv[arg].length() > 2)
						ednsStr = argv[arg].substring(2);
					else
						ednsStr = argv[++arg];
					edns = Integer.parseInt(ednsStr);
					if (edns < 0 || edns > 1) {
						throw new RunFailureException("Unsupported "
								+ "EDNS level: " + edns);
					}
					resolver.setEDNS(edns);
					break;

				case 'd':
					resolver.setEDNS(0, 0, ExtendedFlags.DO, null);
					break;

				default:
					throw new RunFailureException("Invalid option: "
							+ argv[arg]);
				}
				arg++;
			}
			Record rec = Record.newRecord(name, type, dclass);
			Message query = Message.newQuery(rec);
			log.debug(query + "");

			return resolver.send(query);

		} catch (Exception e) {
			throw new RunFailureException(e);
		}
	}
	
	public String update(String hostname,String domain,String ip,String tsig){
		Name zoneName = null; 
		int type = Type.A;
		
		try {
			Lookup lookup = new Lookup(Name.fromString(domain));
			Record [] records = lookup.run();
			if(records != null) {
			   zoneName = records[0].getName();
			}
			if(zoneName != null) {
			    Name hostName = Name.fromString(hostname, zoneName);
			    Update update = new Update(zoneName);
			    update.add(hostName, type, 600, ip);
			    
			    resolver.setTSIGKey(new TSIG(domain,tsig));

			    Message response1 = resolver.send(update);
			    return response1.getHeader().toString();
			}
		} catch (TextParseException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	String dig_usage() {
		return "Usage: dig name [<type>] [<class>] [options]";
	}

	@Override
	public void teardown() {
		

	}

	@Override
	public void startup() {
		try {
			if (host != null)
				resolver = new SimpleResolver(host);
			else
				resolver = new SimpleResolver();
			
			if(this.get_query_para("tsig")!=null)
				resolver.setTSIGKey(TSIG.fromString(get_query_para("tsig")));
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	    resolver.setPort(port);
	    resolver.setTCP(tcp);
	}

	@Override
	public String get_default_schema() {
		return "dns";
	}

	@Override
	public boolean is_ready() {
		return true;
	}

}
