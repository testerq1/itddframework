import java.lang.reflect.Method;
import java.util.HashMap;

import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.core.JIVariant;
import org.testng.annotations.*
import org.testng.annotations.Test

import ate.testcase.*
import common.*

public class OpcTest extends UATestcase{
    @Test
    public void matrikon() throws JIException{
             
        def ua=create_ua("utgard://administrator:123qwe%21%40%23@172.16.5.111/");
        ua.build(ua.PROG_ID, "Matrikon.OPC.Simulation.1");
        ua.startup();
        assertTrue(ua.is_ready());
        
        ua.set("Bucket Brigade.Int1", new JIVariant((byte)179));
        log.info(""+ua.get("Bucket Brigade.Int1"));
        
        sleep(1000);
        log.info(""+ua.get("Bucket Brigade.Int1"));
        //ua.browse();
    }

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {		
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		//;
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
