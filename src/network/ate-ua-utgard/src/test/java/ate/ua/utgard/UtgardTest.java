package ate.ua.utgard;

import java.net.UnknownHostException;

import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.core.*;
import org.jinterop.dcom.impls.JIObjectFactory;
import org.jinterop.dcom.impls.automation.IJIDispatch;
import org.jinterop.dcom.impls.automation.JIExcepInfo;
import org.testng.annotations.Test;

import ate.testcase.Testcase;
import ate.testcase.UATestcase;
import ate.util.X2X0;

/**
 * 
 * @author ravi huang
 *
 */
public class UtgardTest extends UATestcase{
    @Test
    public void matrikon() throws JIException{
        String uri=X2X0.encode_url("!@#");        
        UtgardUA ua=this.create_ua("utgard://administrator:123qwe%21%40%23@172.16.5.111/",new UtgardUA());
        ua.build(ua.PROG_ID, "Matrikon.OPC.Simulation.1");
        ua.startup();
        assertTrue(ua.is_ready());
        
        ua.set("Bucket Brigade.Int1", new JIVariant((byte)179));
        log.info(""+ua.get("Bucket Brigade.Int1"));
        
        sleep(1000);
        log.info(""+ua.get("Bucket Brigade.Int1"));
        //ua.browse();
    }
    
    @Test
    public void dcom_excel() throws UnknownHostException, JIException{
        JISession session = JISession.createSession("172.16.5.111","administrator","123qwe!@#");
        JIComServer comServer = new JIComServer(JIProgId.valueOf("Excel.Application"),"172.16.5.111",session);
      //Instantiate the COM Server
        IJIComObject excelObject = comServer.createInstance();

        //Get a handle to it's IDispatch
        IJIDispatch dispatch = (IJIDispatch)JIObjectFactory.narrowObject((IJIComObject)excelObject.queryInterface(IJIDispatch.IID));

        //call an API
        JIVariant variant = new JIVariant(Boolean.TRUE);

        //This will show excel(if yours is not visible then follow FAQ A12).
        int i=dispatch.getIDsOfNames("Visible");
        dispatch.put("Visible",variant);
        JIExcepInfo  e=dispatch.getLastExcepInfo();
        
        log.info(e.getExcepDesc());
        //...

        //when your work is done
        //JISession.destroySession(session); 
    }
}
