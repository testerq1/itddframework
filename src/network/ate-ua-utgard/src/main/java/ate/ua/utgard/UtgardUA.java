package ate.ua.utgard;

import java.net.UnknownHostException;
import java.util.concurrent.Executors;

import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.common.JISystem;
import org.jinterop.dcom.core.JIVariant;
import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.da.AccessBase;
import org.openscada.opc.lib.da.AutoReconnectController;
import org.openscada.opc.lib.da.DataCallback;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.ItemState;
import org.openscada.opc.lib.da.Server;
import org.openscada.opc.lib.da.SyncAccess;
import org.openscada.opc.lib.da.browser.BaseBrowser;
import org.openscada.opc.lib.da.browser.Branch;
import org.openscada.opc.lib.da.browser.Leaf;
import org.openscada.opc.lib.da.browser.TreeBrowser;

import ate.AbstractURIHandler;
import ate.Global;
import ate.ua.RunFailureException;
import ate.ua.UAOption;

/**
 * https://github.com/ctron/org.openscada.utgard
 * 
 * Utgard只支持client的仿真，可以配合MatrikonOPC OPCSim一起使用
 * 
 * winxp下配置COM服务：http://j-integra.intrinsyc.com/support/com/doc/#remoteaccess.html#winxpsp2
 * 1,If the computer belongs to a workgroup instead of a domain, make sure that it does not use simple file sharing. Open Windows Explorer or double click My Computer, click Tools, then go to Folder Options, click View and uncheck Use simple file sharing (Recommended) in Advanced settings.
 * 2,Click Start, click Programs, click Administrative Tools, click Component Services.
 * 3,Expand Component Services, expand Computers, and right-click My Computer. Select Properties.
 * 4,Click Default COM Security.
 * 5,Under Default Access Permissions click Edit Default. Make sure SYSTEM, INTERACTIVE, NETWORK, and the user whose authentication credentials will be used to access the COM application all have Local and Remote Access permissions.
 * 6,Under Default Access Permissions click Edit Limits. Service Pack 2 comes with the following default values: ANONYMOUS LOGON (Local Access) and Everyone (Local and Remote Access). Make sure these values are listed, and then add the user whose authentication credentials will be used to access the COM application. Allow this user to have Local and Remote Access permissions.
 * 7,Under Default Launch Permissions click Edit Default. Make sure SYSTEM, INTERACTIVE, NETWORK, and the user whose authentication credentials will be used to access the COM application all have Local and Remote Launch permissions, as well as Local and Remote Activation permissions.
 * 8,Under Default Launch Permissions click Edit Limits. Service Pack 2 comes with the following default values: MACHINE\Administrators (Local and Remote Launch, Local and Remote Activation) and Everyone (Local Launch and Local Activation). Make sure these values are listed, and then add the user whose authentication credentials will be used to access the COM application. Allow this user to have Local and Remote Launch permissions, as well as Local and Remote Activation permissions.
 * 9,Service Pack 2 comes with a built-in Windows Firewall. If the firewall is turned on, you will have to allow your COM application network access to your machine. You can do this by opening Windows Firewall and adding your COM application to the list of programs under the Exceptions tab. If Display a notification when Windows Firewall blocks a program is selected, then you will be prompted to unblock the COM application when you run your J-Integra? application the first time. Select Unblock when prompted.
 * 10,If you still get an "access denied" or "permission denied" error after configuring your DCOM settings, try rebooting your machine to allow the new settings to take effect.
 * 11,启动Server和Remote Registry服务
 * 
 * @author ravi huang
 *
 */
public class UtgardUA extends AbstractURIHandler implements DataCallback{
    Server server;
    ConnectionInformation ci;
    String progid;
    String clsid;
    String domain;
    AccessBase access;
    JIVariant value;
    Item item;
    
    public static final UAOption<String> DOMAIN_NAME =
            new UAOption<String>("DOMAIN_NAME");
    public static final UAOption<String> PROG_ID =
            new UAOption<String>("PROG_ID");
    public static final UAOption<String> CLS_ID =
            new UAOption<String>("CLS_ID");
    @Override
    public AbstractURIHandler build(UAOption option, Object value) {
        super.build(option, value);
        
        if (option == DOMAIN_NAME) {
            domain=value.toString();
        } else if (option == PROG_ID) {
            progid=value.toString();
        }else if (option == CLS_ID) {
            clsid=value.toString();
        }
        return this;
    }  
    
    @Override
    public void changed(Item item, ItemState itemState) {
        value=itemState.getValue();  
        this.item=item;
    }
    
    public void browse(){
        BaseBrowser flatBrowser = server.getFlatBrowser ();
        try {
            if ( flatBrowser != null )
            {
                for ( final String item : server.getFlatBrowser ().browse ( "" ) )
                {
                    System.out.println ( item );
                }
            }

            // browse tree
            final TreeBrowser treeBrowser = server.getTreeBrowser ();
            if ( treeBrowser != null )
            {
                dumpTree ( treeBrowser.browse (), 0 );
            }

            // browse tree manually
            browseTree ( "", treeBrowser, new Branch () );
        } catch (Exception e) {
            throw new RunFailureException(e);
        }
    }
    
    private static void browseTree ( final String prefix, final TreeBrowser treeBrowser, final Branch branch ) throws IllegalArgumentException, UnknownHostException, JIException
    {
        treeBrowser.fillLeaves ( branch );
        treeBrowser.fillBranches ( branch );

        for ( final Leaf leaf : branch.getLeaves () )
        {
            dumpLeaf ( "M - " + prefix + " ", leaf );
        }
        for ( final Branch subBranch : branch.getBranches () )
        {
            dumpBranch ( "M - " + prefix + " ", subBranch );
            browseTree ( prefix + " ", treeBrowser, subBranch );
        }
    }
    private static void dumpLeaf ( final String prefix, final Leaf leaf )
    {
        System.out.println ( prefix + "Leaf: " + leaf.getName () + " [" + leaf.getItemId () + "]" );
    }

    private static void dumpBranch ( final String prefix, final Branch branch )
    {
        System.out.println ( prefix + "Branch: " + branch.getName () );
    }

    public static void dumpTree ( final Branch branch, final int level )
    {
        final StringBuilder sb = new StringBuilder ();
        for ( int i = 0; i < level; i++ )
        {
            sb.append ( "  " );
        }
        final String indent = sb.toString ();

        for ( final Leaf leaf : branch.getLeaves () )
        {
            dumpLeaf ( indent, leaf );
        }
        for ( final Branch subBranch : branch.getBranches () )
        {
            dumpBranch ( indent, subBranch );
            dumpTree ( subBranch, level + 1 );
        }
    }
    /**
     * 由于默认period为500ms，因此两次get间应该大于500ms
     * @param itemid
     * @return
     */
    public JIVariant get(String itemid){
        try {            
            value=null;
            access.addItem(itemid, this);
            access.bind();
            int loop=10000;
            while(value==null&&loop-->0)
                try {
                    Thread.currentThread().sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            access.unbind();
            access.removeItem(itemid);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return value;
    }
    
    public void set(String itemid,JIVariant value ){        
        try {
            this.item=null;
            access.addItem(itemid, this);
            access.bind();
            int loop=10000;
            while(item==null&&loop-->0)
                try {
                    Thread.currentThread().sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            //log.info(this.value.toString());
            item.write ( value );
            access.unbind();
            access.removeItem(itemid);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        
    @Override
    public void teardown() {        
        server.disconnect();
    }

    @Override
    public void startup() {
        try {
            JISystem.setInBuiltLogHandler(Global.verbose);
        } catch (Exception e1) {
           e1.printStackTrace();
        }
        
        ci = new ConnectionInformation ();
        ci.setHost ( host );
        if(domain!=null)
            ci.setDomain ( domain );
        ci.setUser ( username );
        ci.setPassword ( passwd );
        if(clsid!=null)
            ci.setClsid ( clsid );
        else if(progid!=null)
            ci.setProgId("Matrikon.OPC.Simulation.1");
        else
            throw new RunFailureException("progid or clsid should be set.");
        
        server = new Server(ci, Executors.newSingleThreadScheduledExecutor());
        AutoReconnectController autoReconnectController = new AutoReconnectController ( server );
        try {
            //支持自动重连
            autoReconnectController.connect();
            
            //不支持自动重连
            //server.connect();
            access = new SyncAccess(server, 500);
            
        } catch (Exception e) {
            throw new RunFailureException(e);
        }
        
    }

    @Override
    public String get_default_schema() {
        return "utgard";
    }

    @Override
    public boolean is_ready() {
        return server!=null;
    }

}
