package ate.ua.opensaml;

import java.util.HashMap;
import java.util.Map;

import org.apache.wss4j.common.ext.WSSecurityException;
import org.apache.wss4j.common.saml.SamlAssertionWrapper;
import org.testng.annotations.*;

import ate.testcase.Testcase;

public class SamlTest extends Testcase {
    SAMLInputContainer input = new SAMLInputContainer();

    @BeforeClass
    public void beforeClass() {
        input.setStrIssuer("http://synesty.com");
        input.setStrNameID("UserJohnSmith");
        input.setStrNameQualifier("My Website");
        input.setSessionId("abcdedf1234567");
        input.setStrAuthMethod("SunAccessManager");
        Map customAttributes = new HashMap();
        customAttributes.put("FirstName", "John");
        customAttributes.put("LastName", "Smith");
        input.setAttributes(customAttributes);
    }

    @Test
    public void assertion() {
        Assertion1 ass=new Assertion1();
        log.info("Assertion String: "
                + ass.marshaller(ass.buildDefaultAssertion(input)));
    
        Assertion2 ass2 = new Assertion2();                
        log.info("Assertion String: "
                + ass2.marshaller(ass2.buildDefaultAssertion(input)));        
        // TODO: now you can also add encryption....
    }
    
    @Test
    public void wss4j_wrapper() throws WSSecurityException{
        SamlAssertionWrapper w1=new SamlAssertionWrapper(new Assertion1().buildDefaultAssertion(input));
        log.info("Assertion String: "+w1.assertionToString());
        SamlAssertionWrapper w2=new SamlAssertionWrapper(new Assertion2().buildDefaultAssertion(input));
        log.info("Assertion String: "+w2.assertionToString());
    }
}