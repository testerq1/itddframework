package ate.ua.opensaml;

import org.joda.time.DateTime;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.SAMLVersion;
import org.opensaml.saml1.core.Assertion;
import org.opensaml.saml1.core.Attribute;
import org.opensaml.saml1.core.AttributeStatement;
import org.opensaml.saml1.core.AttributeValue;
import org.opensaml.saml1.core.AuthenticationStatement;
import org.opensaml.saml1.core.Conditions;
import org.opensaml.saml1.core.ConfirmationMethod;
import org.opensaml.saml1.core.DoNotCacheCondition;
import org.opensaml.saml1.core.NameIdentifier;
import org.opensaml.saml1.core.Subject;
import org.opensaml.saml1.core.SubjectConfirmation;
import org.opensaml.saml1.core.impl.AssertionMarshaller;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObjectBuilder;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.util.XMLHelper;
import org.w3c.dom.Element;

import ate.ua.RunFailureException;

public class Assertion1 {

    public Assertion1() {
        try {
            DefaultBootstrap.bootstrap();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public Assertion buildDefaultAssertion(SAMLInputContainer input) {
        try {

            XMLObjectBuilderFactory builderFactory = Configuration
                    .getBuilderFactory();

            // Create the NameIdentifier
            SAMLObjectBuilder<NameIdentifier> nameIdBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(NameIdentifier.DEFAULT_ELEMENT_NAME);
            NameIdentifier nameId = nameIdBuilder.buildObject();
            nameId.setNameIdentifier(input.getStrNameID());
            nameId.setNameQualifier(input.getStrNameQualifier());
            nameId.setFormat(NameIdentifier.UNSPECIFIED);

            // Create the SubjectConfirmation
            SAMLObjectBuilder<ConfirmationMethod> confirmationMethodBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(ConfirmationMethod.DEFAULT_ELEMENT_NAME);
            ConfirmationMethod confirmationMethod = confirmationMethodBuilder
                    .buildObject();
            confirmationMethod
                    .setConfirmationMethod("urn:oasis:names:tc:SAML:1.0:cm:sender-vouches");

            SAMLObjectBuilder<SubjectConfirmation> subjectConfirmationBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(SubjectConfirmation.DEFAULT_ELEMENT_NAME);
            SubjectConfirmation subjectConfirmation = subjectConfirmationBuilder
                    .buildObject();
            subjectConfirmation.getConfirmationMethods()
                    .add(confirmationMethod);
            // Create the Subject
            SAMLObjectBuilder<Subject> subjectBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(Subject.DEFAULT_ELEMENT_NAME);
            Subject subject = subjectBuilder.buildObject();

            subject.setNameIdentifier(nameId);
            subject.setSubjectConfirmation(subjectConfirmation);

            // Create Authentication Statement
            SAMLObjectBuilder<AuthenticationStatement> authStatementBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(AuthenticationStatement.DEFAULT_ELEMENT_NAME);
            AuthenticationStatement authnStatement = authStatementBuilder
                    .buildObject();
            authnStatement.setSubject(subject);
            authnStatement.setAuthenticationMethod(input.getStrAuthMethod());
            authnStatement.setAuthenticationInstant(new DateTime());

            // Create the attribute statement
            SAMLObjectBuilder<Attribute> attrBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(Attribute.DEFAULT_ELEMENT_NAME);
            Attribute attrGroups = attrBuilder.buildObject();
            attrGroups.setAttributeName("Groups");

            XMLObjectBuilder stringBuilder = builderFactory
                    .getBuilder(XSString.TYPE_NAME);
            XSString attrNewValue = (XSString) stringBuilder.buildObject(
                    AttributeValue.DEFAULT_ELEMENT_NAME, XSString.TYPE_NAME);
            attrNewValue.setValue("AssetManager");

            attrGroups.getAttributeValues().add(attrNewValue);

            SAMLObjectBuilder<AttributeStatement> attrStatementBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(AttributeStatement.DEFAULT_ELEMENT_NAME);
            AttributeStatement attrStatement = attrStatementBuilder
                    .buildObject();
            attrStatement.getAttributes().add(attrGroups);
            // attrStatement.setSubject(subject);

            // Create the do-not-cache condition
            SAMLObjectBuilder<DoNotCacheCondition> doNotCacheConditionBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(DoNotCacheCondition.DEFAULT_ELEMENT_NAME);
            DoNotCacheCondition condition = doNotCacheConditionBuilder
                    .buildObject();

            SAMLObjectBuilder<Conditions> conditionsBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(Conditions.DEFAULT_ELEMENT_NAME);
            Conditions conditions = conditionsBuilder.buildObject();
            conditions.getConditions().add(condition);

            // Create the assertion
            SAMLObjectBuilder<Assertion> assertionBuilder = (SAMLObjectBuilder) builderFactory
                    .getBuilder(Assertion.DEFAULT_ELEMENT_NAME);
            Assertion assertion = assertionBuilder.buildObject();
            assertion.setIssuer(input.getStrIssuer());
            assertion.setIssueInstant(new DateTime());
            assertion.setVersion(SAMLVersion.VERSION_10);

            assertion.getAuthenticationStatements().add(authnStatement);
            assertion.getAttributeStatements().add(attrStatement);
            assertion.setConditions(conditions);

            return assertion;
        } catch (Exception e) {
            throw new RunFailureException(e);
        }

    }

    public String marshaller(Assertion assertion) {
        try {
            AssertionMarshaller marshaller = new AssertionMarshaller();
            Element element = marshaller.marshall(assertion);

            return XMLHelper.prettyPrintXML(element);
        } catch (MarshallingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
