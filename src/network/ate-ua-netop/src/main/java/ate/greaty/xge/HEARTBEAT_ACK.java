package ate.greaty.xge;


import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.joou.UByte;

import ate.ua.RunFailureException;



/**
 * 8.4.2.3  心跳消息响应
 */
@SuppressWarnings("unused")
public class HEARTBEAT_ACK extends CmccDpiPolicyPacket {
    public UByte Result=ubyte(0);  //心跳回应结果
    
    
    public HEARTBEAT_ACK(){
        MessageType=PT.HEARTBEAT_ACK.getValue();
        }
    
    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din) throws IOException {
        Result=ubyte(din.readByte());
        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if("Result".equalsIgnoreCase(k))
            Result=ubyte((Integer)v);
        else
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {        
        MessageLength=uint(1+HEADER_LENGTH);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
        out.writeByte(Result.byteValue());
       
        return this;
    }

}
