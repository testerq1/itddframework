package ate.greaty.xge;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.joou.UByte;
import org.joou.UInteger;

import ate.util.X2X0;
import static org.joou.Unsigned.*;

/**
 * 8.4.2.7.5    流量分析结果上报策略
 * @author dongdong.dai
 *
 */
public class REPORTER_POLICY extends CmccDpiPolicyPacket {
    public UInteger PolicyID=uint(0);
    public UByte PacketType=ubyte(0) ; //标识是要上报会话级数据，或者统计级数据
    public UByte PacketSubtype=ubyte(0) ; //针对选定的上报数据类型，选定指定报表类型
    public UInteger R_StartTime=uint(0);//分析结果上报开始时间    4bytes
    public UInteger R_EndTime=uint(0); //分析结果上报结束时间      4bytes
    public UByte R_Freq=ubyte(0); //分析结果上报周期
    public UByte R_Temp_Name_Len=ubyte(0); //XDR字段选择模板的名称长度
    public String R_Temp_Name="";//XDR字段选择模板的名称
    public UByte R_Interface =ubyte(0); //标识会话级数据是用文件格式上报，还是实时接口上报
    public UByte R_DestIPLength=ubyte(0); //上报FTP服务器的IP地址长度
    public String R_DestIP="" ; //上报FTP服务器的IP地址
    public String UserName="" ;//登录FTP服务器的用户名    8bytes
    public String  Password="" ;//登录FTP服务器密码           8bytes
    public UByte SDTPIPLength=ubyte(0);//上报SDTP IP地址长度
    public String  SDTP_IP="";//SDTP IP地址
    
    public REPORTER_POLICY(){
        MessageType = PT.REPORTER_POLICY.getValue();
        MessageVersion=uint(1);
    }

    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        PolicyID=uint(din.readInt());
        PacketType=ubyte(din.readByte());
        PacketSubtype=ubyte(din.readByte());
        R_StartTime=uint(din.readInt());
        R_EndTime=uint(din.readInt());
        R_Freq=ubyte(din.readByte());
        R_Temp_Name_Len=ubyte(din.readByte());
        R_Temp_Name=this.readString(din, R_Temp_Name_Len.intValue());
        R_Interface=ubyte(din.readByte());
        R_DestIPLength=ubyte(din.readByte());
        
        byte[] ip=new byte[R_DestIPLength.byteValue()];
        din.read(ip);
        R_DestIP =X2X0.bs2ip(ip);
          
    
        UserName=readString(din,8);
        Password=readString(din,8);
        
        SDTPIPLength=ubyte(din.readByte());
        
        
        byte[] ip1=new byte[SDTPIPLength.byteValue()];
        din.read(ip1);
        SDTP_IP =X2X0.bs2ip(ip1);
        
      
        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if("PolicyID".equalsIgnoreCase(k))
            PolicyID=uint((Integer)v);
        else if("PacketType".equalsIgnoreCase(k))
            PacketType=ubyte((Integer)v);
        else if("PacketSubtype".equalsIgnoreCase(k))
            PacketSubtype=ubyte((Integer)v);
        else if("R_StartTime".equalsIgnoreCase(k))
            R_StartTime=uint((Integer)v);
        else if("R_EndTime".equalsIgnoreCase(k))
            R_EndTime=uint((Integer)v);
        else if("R_Freq".equalsIgnoreCase(k))
            R_Freq=ubyte((Integer)v);
        else if("R_Temp_Name_Len".equalsIgnoreCase(k))
            R_Temp_Name_Len=ubyte((Integer)v);
        else if("R_Temp_Name".equalsIgnoreCase(k))
            R_Temp_Name=(String)v;
        else if("R_Interface".equalsIgnoreCase(k))
            R_Interface=ubyte((Integer)v);
        else if("R_DestIPLength".equalsIgnoreCase(k))
            R_DestIPLength=ubyte((Integer)v);
        else if("R_DestIP".equalsIgnoreCase(k))
            R_DestIP=(String)v;
        else if("UserName".equalsIgnoreCase(k))     //8bytes
            UserName=(String)v;
        else if("Password".equalsIgnoreCase(k))     //8bytes
            Password=(String)v;
        else if("SDTPIPLength".equalsIgnoreCase(k))
            SDTPIPLength=ubyte((Integer)v);
        else if("SDTP_IP".equalsIgnoreCase(k))
            SDTP_IP=(String)v;
        else
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {
        R_Temp_Name_Len=ubyte(R_Temp_Name.getBytes(charset).length);
     //   R_Temp_Name_Len=ubyte(120);
        R_DestIPLength=ubyte(4);
        SDTPIPLength=ubyte(4);
     //   this.MessageLength=uint(140);
        this.MessageLength=uint(this.HEADER_LENGTH+1*7+4*3+8*2+R_Temp_Name_Len.shortValue()+R_DestIPLength.shortValue()+SDTPIPLength.shortValue());
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
        byte[] uname=Arrays.copyOf(this.UserName.getBytes(charset), 8);
        byte[] passwd=Arrays.copyOf(this.Password.getBytes(charset), 8);
        out.writeInt(PolicyID.intValue());
        out.writeByte(PacketType.byteValue());
        out.writeByte(PacketSubtype.byteValue());
        out.writeInt(R_StartTime.intValue());
        out.writeInt(R_EndTime.intValue());
        out.writeByte(R_Freq.byteValue());
        out.writeByte(R_Temp_Name_Len.byteValue());
        out.write(R_Temp_Name.getBytes(charset));
        out.writeByte(R_Interface.byteValue());
        out.writeByte(R_DestIPLength.byteValue());
        out.write(X2X0.ip2b(R_DestIP));
        out.write(uname);        
        out.write(passwd);        
        out.writeByte(SDTPIPLength.byteValue());
        out.write(X2X0.ip2b(SDTP_IP));
        return this;
    }

}
