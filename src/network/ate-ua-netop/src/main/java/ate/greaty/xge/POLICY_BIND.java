package ate.greaty.xge;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.joou.UByte;
import org.joou.UInteger;

/**
 * 8.4.2.8  策略与对象绑定
 * @author dongdong.dai
 *
 */
public class POLICY_BIND extends CmccDpiPolicyPacket {
    public UByte ServerNameNum=ubyte(0);//策略服务器代号，标识发送绑定消息的策略服务器
    public UByte Bind_Action=ubyte(0);//表示对后续操作方式
    public UByte UserType=ubyte(0);//用户类型
    public UByte UserNameLength =ubyte(0);//用户帐号长度
    public String UserName ="";//用户帐号
       public UByte BandPlolicyCount=ubyte(0);//关联策略包数
       
    public List<Band> band=new ArrayList<Band>();
    class Band{
        
  
    public UByte BandPolicyMessageType=ubyte(0);//关联策略包的类型
    public UInteger BandPlolicyMessageID=uint(0);//关联策略包ID   4bytes
    
      
    
    }
    
    public POLICY_BIND add_band(HashMap<String, Object> hm){
        Band pd =new Band();
       
        pd.BandPolicyMessageType=ubyte((Integer)hm.get("BandPolicyMessageType"));
        pd.BandPlolicyMessageID=uint((Integer)hm.get("BandPlolicyMessageID"));
        this.band.add(pd);
        return this;
    }
     
    public POLICY_BIND(){
        MessageType=PT.POLICY_BIND.getValue();
        MessageVersion=uint(1);
    }

    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        ServerNameNum=ubyte(din.readByte());
        Bind_Action=ubyte(din.readByte());
        UserType=ubyte(din.readByte());
        UserNameLength=ubyte(din.readByte());
        UserName=this.readString(din, UserNameLength.intValue());
        BandPlolicyCount=ubyte(din.readByte());
        
        for(int i=0;i<BandPlolicyCount.intValue();i++){
            Band pd =new Band();
            pd.BandPolicyMessageType=ubyte(din.readByte());
            pd.BandPlolicyMessageID=uint(din.readInt());
        }
        
        
        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if("ServerNameNum".equalsIgnoreCase(k))
            ServerNameNum=ubyte((Integer)v);
        else if("Bind_Action".equalsIgnoreCase(k))
            Bind_Action=ubyte((Integer)v);
        else if("UserType".equalsIgnoreCase(k))
            UserType=ubyte((Integer)v);
        else if("UserNameLength".equalsIgnoreCase(k))
            UserNameLength=ubyte((Integer)v);
        else if("UserName".equalsIgnoreCase(k))
            UserName=(String)v;
        else if("BandPlolicyCount".equalsIgnoreCase(k))
            BandPlolicyCount=ubyte((Integer)v);
        
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {
        BandPlolicyCount=ubyte(this.band.size());
        
        UserNameLength=ubyte(UserName.getBytes(charset).length);
        MessageLength=uint(5+UserNameLength.intValue()+this.HEADER_LENGTH+5*BandPlolicyCount.intValue());
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
        out.writeByte(ServerNameNum.byteValue());
        out.writeByte(Bind_Action.byteValue());
        out.writeByte(UserType.byteValue());
        out.writeByte(UserNameLength.byteValue());
        out.write(UserName.getBytes(charset));
        out.writeByte(BandPlolicyCount.byteValue());
        for(Band ba : this.band){
            out.writeByte(ba.BandPolicyMessageType.byteValue());
            out.writeInt(ba.BandPlolicyMessageID.intValue());
        }
       
        return this;
    }

}
