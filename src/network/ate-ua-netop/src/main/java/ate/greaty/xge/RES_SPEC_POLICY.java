package ate.greaty.xge;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.joou.UByte;
import org.joou.UShort;

import ate.util.X2X0;
import static org.joou.Unsigned.*;

/**
 * 8.4.2.6.3    资源特征库下发消息
 * @author dongdong.dai
 *
 */
public class RES_SPEC_POLICY extends CmccDpiPolicyPacket {
    public UByte Resoure_Type=ubyte(0);//资源特征库的具体类型
    public UByte Protocol_Version=ubyte(0);//资源特征库的版本号
    public UByte Server_IP_Length=ubyte(0);//资源特征库所在服务器IP地址长度
    public  String  Server_IP="";//资源特征库对应表所在服务器IP地址
    public UShort Server_Port=ushort(0);//资源特征库对应表所在服务器侦听端口
    public String UserName="";//登录FTP服务器的用户名   8bytes
    public String Password="";//登录FTP服务器的密码   8bytes
    public UShort FileName_Length=ushort(0);//资源特征库对应表文件名长度
    public String FileName="";//资源特征-库对应表文件名
    
    public RES_SPEC_POLICY() {
        MessageType = PT.RES_SPEC_POLICY.getValue();
        MessageVersion=uint(1);
    }
    
    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        Resoure_Type=ubyte(din.readByte());
        Protocol_Version=ubyte(din.readByte());
        Server_IP_Length=ubyte(din.readByte());        
      //  Server_IP=this.readString(din, Server_IP_Length.intValue());
        
        byte[] ip=new byte[Server_IP_Length.byteValue()];
        din.read(ip);
        Server_IP =X2X0.bs2ip(ip);
        
        Server_Port=ushort(din.readShort());
        UserName=readString(din,8);   //8bytes
        Password=readString(din,8);     //8bytes
        FileName_Length=ushort(din.readShort());
        FileName=this.readString(din, FileName_Length.intValue());
        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if("Resoure_Type".equalsIgnoreCase(k))
            Resoure_Type=ubyte((Integer)v);
        else if("Protocol_Version".equalsIgnoreCase(k))
            Protocol_Version=ubyte((Integer)v);
        else if("Server_IP_Length".equalsIgnoreCase(k))
            Server_IP_Length=ubyte((Integer)v);
        else if("Server_IP".equalsIgnoreCase(k))
            Server_IP=(String)v;
        else if("Server_Port".equalsIgnoreCase(k))
            Server_Port=ushort((Integer)v);
        else if("UserName".equalsIgnoreCase(k))     //8bytes
            UserName=(String)v;                 
        else if("Password".equalsIgnoreCase(k))     //8bytes
            Password=(String)v;
        else if("FileName_Length".equalsIgnoreCase(k))
            FileName_Length=ushort((Integer)v);
        else if("FileName".equalsIgnoreCase(k))
            FileName=(String)v;
        else
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {
        Server_IP_Length=ubyte(4);   
        FileName_Length=ushort(this.FileName.getBytes(charset).length);
        MessageLength=uint(23+Server_IP_Length.intValue()+FileName_Length.intValue()+this.HEADER_LENGTH);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
       out.writeByte(Resoure_Type.byteValue());
       out.writeByte(Protocol_Version.byteValue());
       out.writeByte(Server_IP_Length.byteValue());
      //out.write(Server_IP.getBytes(charset));
       out.write(X2X0.ip2b(Server_IP));
       out.writeShort(Server_Port.shortValue());
       out.write(Arrays.copyOf(this.UserName.getBytes(charset), 8));
       out.write(Arrays.copyOf(this.Password.getBytes(charset), 8));
       out.writeShort(FileName_Length.shortValue());
       out.write(FileName.getBytes(charset));
       return this;
    }

}
