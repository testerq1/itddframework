package ate.greaty.xge;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.joou.UByte;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;

/**
 * DPI设备状态查询
 * @author dongdong.dai
 *
 */
public class Dev_Status extends CmccDpiPolicyPacket {
    public UByte InfoReportFreq=ubyte(0); //设备信息上报频率
    
    public Dev_Status(){
        MessageType=PT.Dev_Status.getValue();
    }

    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        InfoReportFreq=ubyte(din.readByte());
        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if("InfoReportFreq".equalsIgnoreCase(k))
            InfoReportFreq=ubyte((Integer)v);
        else
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {
        MessageLength=uint(1);
        return this;
    }
    
    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
        out.writeByte(InfoReportFreq.byteValue());
        return this;
    }

}
