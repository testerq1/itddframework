package ate.greaty.xge;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.joou.UByte;
import org.joou.UInteger;
import org.joou.UShort;

import static org.joou.Unsigned.*;

/**
 * 8.4.2.11 通用ACK报文字段
 * @author dongdong.dai
 *
 */
public class ACK extends CmccDpiPolicyPacket {
    public UByte MessageType1=ubyte(0);//DPI设备所收到的消息类型 
    public UInteger  MessageID1=uint(0) ;//DPI设备所收到的消息序号  4bytes
    public UByte PacketErrorStatus=ubyte(0);//错误类型代码
    public UShort PacketErrorInfoLength =ushort(0);//错误信息描述长度
    public String PacketErrorInfo="" ;//错误信息详细描述
    
    public ACK() {
        MessageType = PT.ACK.getValue();
    }
    
    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        MessageType1=ubyte(din.readByte());
        MessageID1=uint(din.readInt());
        PacketErrorStatus=ubyte(din.readByte());
        PacketErrorInfoLength=ushort(din.readShort());
        PacketErrorInfo=this.readString(din, PacketErrorInfoLength.intValue());
        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if("MessageType1".equalsIgnoreCase(k))
            MessageType1=ubyte((Integer)v);
        else if("MessageID1".equalsIgnoreCase(k))
            MessageID1=uint((Integer)v);
        else if("PacketErrorStatus".equalsIgnoreCase(k))
            PacketErrorStatus=ubyte((Integer)v);
        else if("PacketErrorInfoLength".equalsIgnoreCase(k))
            PacketErrorInfoLength=ushort((Integer)v);
        else if("PacketErrorInfo".equalsIgnoreCase(k))
            PacketErrorInfo=(String)v;
        else 
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {
        PacketErrorInfoLength=ushort(PacketErrorInfo.getBytes(charset).length);
        MessageLength=uint(8+PacketErrorInfoLength.intValue());
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
       out.writeByte(MessageType1.byteValue());
       out.writeInt(MessageID1.intValue());
       out.writeByte(PacketErrorStatus.byteValue());
       out.writeShort(PacketErrorInfoLength.shortValue());
       out.write(PacketErrorInfo.getBytes(charset));
        return this;
    }


}
