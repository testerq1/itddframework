package ate.greaty.xge;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.joou.UByte;
import org.joou.UInteger;
import org.joou.UShort;

import static org.joou.Unsigned.*;
import ate.greaty.xge.Dev_Status_ACK.PortDesc;
import ate.greaty.xge.Dev_Status_ACK.PortType;
import ate.greaty.xge.STATIC_IP_SYNC.User_IP;
import ate.ua.RunFailureException;
import ate.util.X2X0;

/**
 * 8.4.2.6.2 用户组归属分配消息
 * 
 * @author dongdong.dai
 * @param <uInteger>
 *
 */
public class UG_HOME extends CmccDpiPolicyPacket {
    public UShort UserGroupNum = ushort(0); // 用户组数量
    public List<UserGroup> usrgroups=new ArrayList<UserGroup>();
    
    public class UserGroup{
        public UByte UserGroupNameLength=ubyte(0);
        public String UserGroupName="";
        public UByte Action=ubyte(0);
        public UInteger UserNum = uint(0);// 本用户组所含用户数 4bytes
        public List<User> usrs=new ArrayList<User>();
        
        public UserGroup add_user(HashMap<String, Object> hm){
            User pd=new User();
            if (hm.get("UserType") != null)
                pd.UserType = ubyte((Integer) hm.get("UserType"));
            
            if(hm.get("UserNameLength") != null){
                pd.UserNameLength = ubyte((Integer) hm.get("UserNameLength"));
            }            
            
            int ut=pd.UserType.intValue();            
            if(ut==0x04)
                pd.UserName=uint(X2X0.ip2i((String)hm.get("UserName")));
            else if(ut==3){
                if(hm.get("UserName")!=null)
                    pd.UserName=ubyte((Integer)hm.get("UserName"));
                else 
                    pd.UserName=null;
            }else{
                pd.UserName=(String)hm.get("UserName");
            }
            
            usrs.add(pd);
            return this;
        }
        
        public int len_1(){
            UserGroupNameLength= ubyte(UserGroupName.toString().getBytes(charset).length);
            
            return 6+UserGroupNameLength.intValue();
          
            
        }
        public String toString(){
            User pd=new User();
            return "UserGroupNameLength="+UserGroupNameLength+
                    "\nUserGroupName="+UserGroupName+
                    "\nAction="+Action+
                    "\nUserNum="+UserNum+
                    "\nusrs="+pd.User_toString();
        }
    }
    
    public class User {
        public UByte UserType ;// 用户类型
        public UByte UserNameLength = ubyte(0);// 用户名长度
        public Object UserName;// 用户名

        public int len() {
            byte ut=UserType.byteValue();
            if(ut==0x04)
               // UserNameLength=ubyte(0);
               UserNameLength=ubyte(4);
            else if(ut==3){
                if(UserName==null)
                    UserNameLength=ubyte(0);
                else
                    UserNameLength=ubyte(1);
            }else{
                UserNameLength = ubyte(UserName.toString().getBytes(charset).length);
              //  UserNameLength=ubyte(0);
            }            
            
            return 2 + UserNameLength.intValue();
        }
        public String User_toString(){
            return "UserType="+UserType+
                    "\nUserNameLength="+UserNameLength+
                    "\nUserName="+UserName;
        }
    }
    
    public UG_HOME() {
        MessageType = PT.UG_HOME.getValue();
        this.MessageVersion=uint(1);
    }

    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        UserGroupNum = ushort(din.readShort());
        for(int i=0;i<UserGroupNum.intValue();i++){
            UserGroup ug=new UserGroup();
            ug.UserGroupNameLength = ubyte(din.readByte());
            ug.UserGroupName = this.readString(din, ug.UserGroupNameLength.intValue());
            ug.Action = ubyte(din.readByte());
            ug.UserNum = uint(din.readInt()); // 4 bytes      
            for(int j=0;j<ug.UserNum.intValue();j++){
                User usr=new User();
                usr.UserType=ubyte(din.readByte());
                usr.UserNameLength=ubyte(din.readByte());
                usr.UserName=this.readString(din, usr.UserNameLength.shortValue());
                ug.usrs.add(usr);                
            }
            this.usrgroups.add(ug);
        }
        return this;
    }
    
    public UserGroup add_usergroup( HashMap<String, Object> hm){
        UserGroup pd = new UserGroup();
        
        if (hm.get("UserGroupNameLength") != null)
            pd.UserGroupNameLength = ubyte((Integer) hm.get("UserGroupNameLength"));
        
        if(hm.get("UserGroupName") != null){
            pd.UserGroupName = (String) hm.get("UserGroupName");
        } 
        
        if(hm.get("Action") != null){
            pd.Action = ubyte((Integer)hm.get("Action"));
        }
        
        if(hm.get("UserNum") != null){
            pd.UserNum = uint((long)hm.get("UserNum"));                    
        }
        this.usrgroups.add(pd);
        return pd;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if ("UserGroupNum".equalsIgnoreCase(k))
            UserGroupNum = ushort((Integer)v);
        else
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {
        this.UserGroupNum=ushort(this.usrgroups.size());
      //   this.UserGroupNum=ushort(3);
        int pd_len=0;
        int pd_len_1=0;
        for(UserGroup ug:this.usrgroups){
            
           
            ug.UserNum=uint(ug.usrs.size());
            pd_len_1+=ug.len_1();
            for(User u:ug.usrs){
                pd_len+= u.len();
            }
        }
        MessageLength=uint(2+this.HEADER_LENGTH+pd_len+pd_len_1);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
        out.writeShort(UserGroupNum.shortValue());
        for(UserGroup ug:this.usrgroups){
            out.writeByte(ug.UserGroupNameLength.byteValue());
            out.write(ug.UserGroupName.getBytes(charset));
            out.writeByte(ug.Action.byteValue());
            out.writeInt(ug.UserNum.intValue());
            for(User u:ug.usrs){
                out.writeByte(u.UserType.byteValue());
                out.writeByte(u.UserNameLength.byteValue());
                
                byte ut=u.UserType.byteValue();
                if(ut==0x04)
                    out.writeInt(((UInteger)u.UserName).intValue());
                else if(ut==3){
                    if(u.UserName!=null)
                        out.writeByte(((UByte)u.UserName).byteValue());
                }else{
                    out.write(u.UserName.toString().getBytes(charset));
                }   
            }
        }
        return this;
    }
    
    
    public String toString(){
        return super.toString()+
                "\nUserGroupNum="+this.UserGroupNum+
                "\nuser_group="+user_group();
                
              
    }
    private String user_group(){
        String rtn="";
        UserGroup ug=new UserGroup();
        for(UserGroup tmp:this.usrgroups){
            rtn+=tmp.toString()+"\n";
            for(User tmp1:ug.usrs){
                rtn+=tmp1.User_toString()+"\n";
            }
        }
       
        return rtn;
    }
}
