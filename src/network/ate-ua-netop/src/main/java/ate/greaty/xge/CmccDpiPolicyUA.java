package ate.greaty.xge;

import java.util.Arrays;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;

import ate.Global;
import ate.ua.IAutoResponseMessage;
import ate.ua.mina.AbstractCustomUA;

/**
 * 中移动dpi策略下发协议
 * 
 * @author Administrator
 *
 */
public class CmccDpiPolicyUA extends AbstractCustomUA<CmccDpiPolicyPacket>
        implements IAutoResponseMessage<IoSession, CmccDpiPolicyPacket> {
    public int heartbeat = 0;
    private int HEADER_LENGTH=14; 
    
    public void enable_heatbeat() {
        this.register_auto_response("heartbeat", this);
    }

    @Override
    public CmccDpiPolicyPacket decodePacket(IoBuffer buf) { 
        if(Global.verbose)
            log.debug("decode packet:{}",buf.remaining());
        
        if(buf.remaining()<=this.HEADER_LENGTH)
            return null;
        
        CmccDpiPolicyPacket pkt=CmccDpiPolicyPacket.decode(buf);
        //buf.position(buf.capacity());
        return pkt;
    }

    @Override
    public String get_default_schema() {
        return "cdp";
    }

    @Override
    public CmccDpiPolicyPacket get_auto_resp_msg(IoSession session,
            CmccDpiPolicyPacket msg) {
        if (msg.MessageType.equals(PT.HEARTBEAT.getValue())){
            this.heartbeat++;
            log.info("自动响应HEARTBEAT_ACK");
            return ((HEARTBEAT)msg).create_response();
        }

        return null;
    }
    
    public int heartbeat(){
        return this.heartbeat;
    }
    
    public CmccDpiPolicyPacket create_packet(String name) {
        return create_packet(PT.get_pt(name));
    }

    public CmccDpiPolicyPacket create_packet(int type) {
        return create_packet(PT.get_pt(type));
    }

    public CmccDpiPolicyPacket create_packet(PT type) {        
        return CmccDpiPolicyPacket.create_packet(type);
    }
    
}
