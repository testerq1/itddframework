package ate.greaty.xge;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;
import static org.joou.Unsigned.ushort;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.joou.UByte;
import org.joou.UShort;

import ate.util.X2X0;



/**
 * 8.4.2.6.1 静态IP用户同步消息
 * 
 * @author dongdong.dai
 *
 */
public class STATIC_IP_SYNC extends CmccDpiPolicyPacket {    

    public UByte Bind_Action=ubyte(0); //
    public UByte UserNameLength=ubyte(0); // IP地址段用户帐号长度
    public String UserName="";// IP地址段用户帐号
    public UShort IP_SegmentNum=ushort(0);// 本用户所拥有的地址段数
    public List<User_IP> usreip;
    
    class User_IP{    
    public UByte UserIPLength=ubyte(0);// 用户IP地址长度
    public String UserIP="";// 用户IP地址
    public UByte UserIP_Prefix=ubyte(0);// 用户地址前缀长度
    
      public int len(){
          UserIPLength=ubyte(X2X0.ip2b(UserIP).length);  
          return 2+UserIPLength.intValue();
      }
    }   
   

    public STATIC_IP_SYNC() {
        MessageType = PT.STATIC_IP_SYNC.getValue();
        MessageVersion=uint(1);
    }

    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {      

        Bind_Action = ubyte(din.readByte());
        UserNameLength = ubyte(din.readByte());
        UserName = this.readString(din, UserNameLength.intValue());
        IP_SegmentNum = ushort(din.readShort());
        this.usreip = new ArrayList<>();
        int pos=this.HEADER_LENGTH+4+UserNameLength.intValue();
        
        for(int i=0;i<IP_SegmentNum.intValue();i++){
            if(pos>=this.MessageLength.intValue()){
                log.warn("IP_SegmentNum长度大于实际IP地址段信息长度: {}",IP_SegmentNum);
                break;
            }
            User_IP pd = new User_IP();
            pd.UserIPLength = ubyte(din.readByte());
            
            byte[] ip=new byte[pd.UserIPLength.byteValue()];
            din.read(ip);
            pd.UserIP = X2X0.bs2ip(ip);
            
            pd.UserIP_Prefix = ubyte(din.readByte());
            usreip.add(pd);
            pos+=(2+pd.UserIPLength.intValue());
        }


        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {

               if ("Bind_Action".equalsIgnoreCase(k))
            Bind_Action = ubyte((Integer) v);
        else if ("UserNameLength".equalsIgnoreCase(k))
            UserNameLength = ubyte((Integer) v);
        else if ("UserName".equalsIgnoreCase(k))
            UserName = (String) v;
        else if ("IP_SegmentNum".equalsIgnoreCase(k))
            IP_SegmentNum = ushort((Integer) v);
        else if ("userip".equalsIgnoreCase(k)) {
            usreip = new ArrayList<>();
            HashMap<String, Object> hm = (HashMap<String, Object>) v;
            for (int i = 0; i < hm.keySet().size(); i += 3) {
                int seq = i / 3;
                User_IP pd = new User_IP();
                if (hm.get("PortDescLength" + seq) != null)
                    pd.UserIPLength = ubyte((Integer) hm.get("UserIPLength"
                            + seq));

                pd.UserIP = (String) hm.get("UserIP" + seq);
                pd.UserIP_Prefix = ubyte((Integer) hm.get("UserIP_Prefix" + seq));
                usreip.add(pd);
            }
        } else

            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {       
        
        MessageLength=uint(4+UserNameLength.intValue()+2*usreip.size());

        byte[] tmp1 = UserName.getBytes(charset);
        if (tmp1.length > 255) {
            log.warn("username too long:" + UserName);
        }

        UserNameLength = ubyte(UserName.getBytes(charset).length);
        int pd_len = 0;
        for (User_IP tmp : this.usreip) {
            pd_len += tmp.len();
        }
        IP_SegmentNum=ushort(usreip.size());
        MessageLength = uint(this.HEADER_LENGTH+4 + UserNameLength.intValue() + pd_len);

        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
        out.writeByte(Bind_Action.byteValue());
        out.writeByte(UserNameLength.byteValue());
        out.write(UserName.getBytes(charset));
        out.writeShort(IP_SegmentNum.shortValue());
        for(User_IP tmp:usreip){
            out.writeByte(tmp.UserIPLength.byteValue());
            out.write(X2X0.ip2b(tmp.UserIP));        
            out.writeByte(tmp.UserIP_Prefix.byteValue());   
    }
        return this;
    }
}
