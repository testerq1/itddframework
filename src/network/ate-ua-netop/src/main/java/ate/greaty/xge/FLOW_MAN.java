package ate.greaty.xge;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;
import static org.joou.Unsigned.ushort;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.joou.UByte;
import org.joou.UInteger;
import org.joou.UShort;

/**
 * 8.4.2.7.2 流量管理策略
 * 
 * @author dongdong.dai
 *
 */

public class FLOW_MAN extends CmccDpiPolicyPacket {
    public UInteger PolicyID = uint(0);// 策略ID
    public UByte ProtocolType = ubyte(0); // 被管理的协议类型
    public UByte AppType = ubyte(0); // 被管理的应用类型
    public UShort AppID; // 被管理的应用编号
    public UByte AppName_Length = ubyte(0); // 被管理的应用名称长度
    public String AppName = ""; // 被管理的应用名称
    public UInteger AppTraffic_Up_ABS = uint(0); // 上行APP流量管理门限值(绝对值) 4bytes
    public UInteger AppTraffic_Dn_ABS = uint(0); // 下行APP流量管理门限值(绝对值) 4bytes
    public UInteger AppSession_Up_ABS = uint(0); // 上行APP连接数管理门限值（绝对值）4bytes
    public UInteger AppSession_Dn_ABS = uint(0); // 下行APP连接数管理门限值（绝对值）4bytes
    public UByte QoS_Label_Up = ubyte(0); // 针对本应用上行流量的差异化标记值
    public UByte QoS_Label_Dn = ubyte(0); // 针对本应用下行流量的差异化标记值
    public UByte TimeSlot = ubyte(0); // 管理时间段数
    public List<Time> timeslots=new ArrayList<>();

    class Time {
        public UInteger Starttime = uint(0); // 管理起始时间 4bytes
        public UInteger Stoptime = uint(0); // 管理终止时间 4bytes

    }

    public FLOW_MAN() {
        MessageType = PT.FLOW_MAN.getValue();
        MessageVersion = uint(1);
    }
    public FLOW_MAN add_time( HashMap<String, Object> hm){
        Time pd = new Time();
        
        if(hm.get("Starttime") != null){
            pd.Starttime = uint((long)hm.get("Starttime"));                    
        }
        if(hm.get("Stoptime") != null){
            pd.Stoptime = uint((long)hm.get("Stoptime"));                    
        }
        
        this.timeslots.add(pd);
        return this;
    }
    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        PolicyID = uint(din.readInt());
        ProtocolType = ubyte(din.readByte());
        AppType = ubyte(din.readByte());
        AppID = ushort(din.readShort());
        AppName_Length = ubyte(din.readByte());
        AppName = this.readString(din, AppName_Length.intValue());
        AppTraffic_Up_ABS = uint(din.readInt());
        AppTraffic_Dn_ABS = uint(din.readInt());
        AppSession_Up_ABS = uint(din.readInt());
        AppSession_Dn_ABS = uint(din.readInt());
        QoS_Label_Up = ubyte(din.readByte());
        QoS_Label_Dn = ubyte(din.readByte());
        TimeSlot = ubyte(din.readByte());

        for (int i = 0; i < TimeSlot.intValue(); i++) {

            Time pd = new Time();
            pd.Starttime = uint(din.readInt());
            pd.Stoptime = uint(din.readInt());

            timeslots.add(pd);

        }

        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if ("PolicyID".equalsIgnoreCase(k))
            PolicyID = uint((Integer) v);
        else if ("ProtocolType".equalsIgnoreCase(k))
            ProtocolType = ubyte((Integer) v);
        else if ("AppType".equalsIgnoreCase(k))
            AppType = ubyte((Integer) v);
        else if ("AppID".equalsIgnoreCase(k))
            AppID = ushort((Integer) v);

        else if ("AppName_Length".equalsIgnoreCase(k))
            AppName_Length = ubyte((Integer) v);
        else if ("AppName".equalsIgnoreCase(k))
            AppName = (String) v;
        else if ("AppTraffic_Up_ABS".equalsIgnoreCase(k))
            AppTraffic_Up_ABS = uint((Integer) v);
        else if ("AppTraffic_Dn_ABS".equalsIgnoreCase(k))
            AppTraffic_Dn_ABS = uint((Integer) v);
        else if ("AppSession_Up_ABS".equalsIgnoreCase(k))
            AppSession_Up_ABS = uint((Integer) v);
        else if ("AppSession_Dn_ABS".equalsIgnoreCase(k))
            AppSession_Dn_ABS = uint((Integer) v);
        else if ("QoS_Label_Up".equalsIgnoreCase(k))
            QoS_Label_Up = ubyte((Integer) v);
        else if ("QoS_Label_Dn".equalsIgnoreCase(k))
            QoS_Label_Dn = ubyte((Integer) v);
        else if ("TimeSlot".equalsIgnoreCase(k))
            TimeSlot = ubyte((Integer) v);
        else
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {
        TimeSlot=ubyte(this.timeslots.size());
       
        int pd_len = this.timeslots.size();
       

        AppName_Length = ubyte(this.AppName.getBytes(charset).length);
        MessageLength = uint(28 + (this.AppName.getBytes(charset).length)+ this.HEADER_LENGTH +8*pd_len);

        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
        out.writeInt(PolicyID.intValue());
        out.writeByte(ProtocolType.byteValue());
        out.writeByte(AppType.byteValue());
        out.writeShort(AppID.shortValue());
        out.writeByte(AppName_Length.byteValue());
        out.write(AppName.getBytes(charset));
        out.writeInt(AppTraffic_Up_ABS.intValue());
        out.writeInt(AppTraffic_Dn_ABS.intValue());
        out.writeInt(AppSession_Up_ABS.intValue());
        out.writeInt(AppSession_Dn_ABS.intValue());
        out.writeByte(QoS_Label_Up.byteValue());
        out.writeByte(QoS_Label_Dn.byteValue());
        out.writeByte(TimeSlot.byteValue());
        for (Time tmp : timeslots) {
            out.writeInt(tmp.Starttime.intValue());
            out.writeInt(tmp.Stoptime.intValue());
        }

        return this;
    }

}
