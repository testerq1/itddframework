package ate.greaty.xge;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.mina.core.buffer.IoBuffer;
import org.joou.UByte;
import org.joou.UInteger;

import ate.Global;
import ate.ua.RunFailureException;
import ate.ua.AbstractCustomPacket;
import ate.util.X2X0;

public abstract class CmccDpiPolicyPacket extends
        AbstractCustomPacket<CmccDpiPolicyPacket> {    
    protected int HEADER_LENGTH=18;
//    public static CmccDpiPolicyPacket decode_packet(InputStream in) {
//        
//        return decode(new DataInputStream(in));
//    }
    
    public static CmccDpiPolicyPacket decode(byte[] bs) {
        return decode(new DataInputStream(new ByteArrayInputStream(bs)));
    }    
    
    public static CmccDpiPolicyPacket decode(IoBuffer buf) {
        return decode(new DataInputStream(buf.asInputStream()));
    }
    public static CmccDpiPolicyPacket decode(String hexstring) {
        return decode(IoBuffer.wrap(X2X0.hexs2b(hexstring)));
    }
    
    protected String readString(DataInputStream din,int len){
        if(Global.verbose)
            log.debug("read string {}",len);
        if(len==0)
            return "";
        
        byte[] bs = new byte[len];
        try {            
            din.read(bs);
            return new String(bs, charset);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;        
    }
    
    protected int readInt(DataInputStream din){
        byte[] bs=new byte[4];
        try {
            din.read(bs);
            return X2X0.bs2i(bs); 
        } catch (IOException e) {
            throw new RunFailureException(e);
        }     
    }
    
    static CmccDpiPolicyPacket create_packet(PT type) {
        switch (type) {
        case HEARTBEAT:
            return new HEARTBEAT();
        case HEARTBEAT_ACK:
            return new HEARTBEAT_ACK();
        case STATIC_IP_SYNC:
            return new STATIC_IP_SYNC();
        case UG_HOME:
            return new UG_HOME();
        case RES_SPEC_POLICY:
            return new RES_SPEC_POLICY();
        case FLOW_MIRROR:
            return new FLOW_MIRROR();
        case FLOW_MAN:
            return new FLOW_MAN();
        case BLOCK_POLICY:
            return new BLOCK_POLICY();
        case SITE_ACCESS:
            return new SITE_ACCESS();
        case REPORTER_POLICY:
            return new REPORTER_POLICY();
        case POLICY_BIND:
            return new POLICY_BIND();
        case POLICY_SYNC:
            return new POLICY_SYNC();
        case POLICY_SYNC_ACK:
            return new POLICY_SYNC_ACK();
        case ACK:
            return new ACK();
        case Dev_Status:
            return new Dev_Status();
        case Dev_Status_ACK:
            return new Dev_Status_ACK();
        }
        return null;
    }
    
    private static CmccDpiPolicyPacket decode(DataInputStream din) {
        CmccDpiPolicyPacket pkt = null;
        try {            
            byte version = din.readByte();
            byte MessageType = din.readByte();

            pkt=create_packet(PT.get_pt(MessageType));
            
            pkt.version = ubyte(version);            
            pkt.MessageID=uint(din.readInt());
            pkt.Timestamp=uint(din.readInt());
            pkt.MessageVersion=uint(din.readInt());
            pkt.MessageLength=uint(din.readInt());             
            if(Global.verbose)
                log.debug("din available: {} {}",din.available(),pkt.MessageLength.intValue());
            
            if(din.available()<pkt.MessageLength.intValue()-pkt.HEADER_LENGTH)
                return null;
            CmccDpiPolicyPacket tmp=pkt.decode_packet(din);
            
            if(Global.verbose)
                log.debug(tmp.toString());
            
            return tmp;
            
        } catch (EOFException e) {
            // 解析包失败
            if(pkt!=null)
                pkt.decode_status = 1;
            e.printStackTrace();
            System.exit(0);
            return null;
        }catch (IOException e) {
            throw new RunFailureException(e);
        }
    }

    public UByte version = ubyte(1);

    public UByte MessageType= ubyte(0);

    public UInteger MessageID=uint(0);

    public UInteger Timestamp=uint(0);

    public UInteger MessageLength=uint(0);
    
    public UInteger MessageVersion=uint(0);
    
    public static AtomicLong msgid=new AtomicLong(0);
    
    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if("version".equalsIgnoreCase(k))
            version=ubyte((Integer)v);
        else if("MessageType".equalsIgnoreCase(k))
            MessageType=((PT)v).getValue();
        else if("MessageID".equalsIgnoreCase(k))
            MessageID=uint((Integer)v);
        else if("Timestamp".equalsIgnoreCase(k))
            Timestamp=uint((Integer)v);
        else if("MessageLength".equalsIgnoreCase(k))
            MessageLength=uint((Integer)v);
        else if("MessageVersion".equalsIgnoreCase(k))
            MessageVersion=uint((Integer)v);
        else
            log.warn("key not exist: \"{}\"",k);
        return this;
    }
    
    public abstract CmccDpiPolicyPacket decode_packet(DataInputStream din)throws IOException ;
    public abstract CmccDpiPolicyPacket encode_packet(DataOutputStream out)throws IOException ;
    
    @Override
    public boolean is_ok(){
        return super.is_ok()&&
                Timestamp.longValue()>0&&
                version.intValue()==1&&
                MessageType.byteValue()<PT.max().byteValue();
    }
    
    public CmccDpiPolicyPacket get_packet(){
        this.Timestamp=uint(new Date().getTime()/1000);
        this.MessageID=uint(msgid.addAndGet(1));
        return this;
    }
    
    public static int  get_utc_time(String time){
        //Date 类型时间
        DateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String strDate = time;
        Date d=null ;
        try {
            d=sdf.parse(strDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }        
        Calendar cal =  new GregorianCalendar();
        cal.setTime(d);       
       
        // 2、取得时间偏移量：  
        int zoneOffset = cal.get(java.util.Calendar.ZONE_OFFSET);  
        // 3、取得夏令时差：  
        int dstOffset = cal.get(java.util.Calendar.DST_OFFSET);
        
        // 4、从本地时间里扣除这些差量，即可以取得UTC时间：  
        cal.add(java.util.Calendar.MILLISECOND, -(zoneOffset + dstOffset));
        
        return (int)cal.getTimeInMillis()/1000;
    }
    
    @Override
    public void encodePacket(OutputStream out) {
        DataOutputStream dos = new DataOutputStream(out);
        try {
            if (payload != null) {
                dos.write(X2X0.hexs2b(payload));
                return;
            }
            dos.writeByte(version.byteValue());
            dos.writeByte(this.MessageType.byteValue());
            dos.writeInt(this.MessageID.intValue());
            dos.writeInt(this.Timestamp.intValue());            
            dos.writeInt(this.MessageVersion.intValue());
            dos.writeInt(this.MessageLength.intValue());
            encode_packet(dos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public String toString(){
        return "version="+this.version+
                "\nMessageType="+PT.get_name(MessageType.intValue())+
                "\nMessageID="+this.MessageID+
                "\nTimestamp="+this.Timestamp+
                "\nMessageVersion="+this.MessageVersion+
                "\nMessageLength="+this.MessageLength;
    }
}
