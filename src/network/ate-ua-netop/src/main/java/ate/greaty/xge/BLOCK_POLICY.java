package ate.greaty.xge;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;
import static org.joou.Unsigned.ushort;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.joou.UByte;
import org.joou.UInteger;
import org.joou.UShort;

import ate.util.X2X0;

/**
 * 8.4.2.7.3 封堵URL/IP/域名策略
 * 
 * @author dongdong.dai
 *
 */
public class BLOCK_POLICY extends CmccDpiPolicyPacket {
    public UInteger PolicyID = uint(0); // 策略ID
    public UByte Action = ubyte(0); // 动作类型
    public UByte Block_Type = ubyte(0); // 被封堵的对象类型
    public UByte Block_Num = ubyte(0); // 被封堵的对象个数
    public List<Block> block = new ArrayList<Block>();

    public class Block {
        public UShort Block_Length = ushort(0); // 被封堵的名称长度
        public Object Block_Name; // 被封堵的名称

        public int len() {
            byte bl = Block_Type.byteValue();
            if (bl == 2)
                Block_Length = ushort(4);
            else if (bl == 1 || bl == 3) {
                Block_Length = ushort(this.Block_Name.toString().getBytes(
                        charset).length);
            }

            return 2 + Block_Length.intValue();
        }

    }

    public boolean has_block(HashMap<String, Object> hm) {
        String bname = (String) hm.get("Block_Name");
        if(bname==null)
            return false;
        
        for (int i = 0; i < this.block.size(); i++) {
            Block block = this.block.get(i);
            if (Block_Type.intValue() == 2
                    && block.Block_Name.equals(uint(X2X0.ip2i(bname)))) {
                return true;
            } else if ((Block_Type.intValue() == 1 || Block_Type.intValue() == 3)
                    && block.Block_Name.equals(bname)) {
                return true;
            }
        }
        return false;
    }

    public BLOCK_POLICY add_block(HashMap<String, Object> hm) {
        Block pd = new Block();

        if (Block_Type.intValue() == 2) {
            pd.Block_Name = uint(X2X0.ip2i((String) hm.get("Block_Name")));
        } else if (Block_Type.intValue() == 1 || Block_Type.intValue() == 3) {
            pd.Block_Name = (String) hm.get("Block_Name");
        }

        this.block.add(pd);
        log.debug("pd.Block_Name==" + pd.Block_Name);
        return this;

    }

    public BLOCK_POLICY() {
        MessageType = PT.BLOCK_POLICY.getValue();
        MessageVersion = uint(1);
    }

    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        PolicyID = uint(din.readInt());
        Action = ubyte(din.readByte());
        Block_Type = ubyte(din.readByte());
        Block_Num = ubyte(din.readByte());
        for (int i = 0; i < Block_Num.intValue(); i++) {
            if(din.available()<=0)
                break;
            Block pd = new Block();
            pd.Block_Length = ushort(din.readShort());
            pd.Block_Name = this.readString(din, pd.Block_Length.intValue());
            block.add(pd);
        }

        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if ("PolicyID".equalsIgnoreCase(k))
            PolicyID = uint((Integer) v);
        else if ("Action".equalsIgnoreCase(k))
            Action = ubyte((Integer) v);
        else if ("Block_Type".equalsIgnoreCase(k))
            Block_Type = ubyte((Integer) v);
        else if ("Block_Num".equalsIgnoreCase(k))
            Block_Num = ubyte((Integer) v);

        else
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {        
        Block_Num = ubyte(this.block.size());
        
        int pd_len = 0;
        for (Block tmp : this.block) {
            pd_len += tmp.len();
        }

        MessageLength = uint(7 + pd_len + this.HEADER_LENGTH);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
        out.writeInt(PolicyID.intValue());
        out.writeByte(Action.byteValue());
        out.writeByte(Block_Type.byteValue());
        out.writeByte(Block_Num.byteValue());
        for (Block b : this.block) {
            out.writeShort(b.Block_Length.shortValue());
            byte b1 = this.Block_Type.byteValue();
            if (b1 == 2)
                out.writeInt(((UInteger) b.Block_Name).intValue());
            else if (b1 == 1 || b1 == 3)
                out.write(b.Block_Name.toString().getBytes(charset));

        }

        return this;
    }

}
