package ate.greaty.nfe;

import java.io.IOException;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.apache.mina.core.buffer.IoBuffer;

import ate.Global;
import ate.rm.dev.Host;
import ate.ua.AbstractCustomPacket;
import ate.ua.RunFailureException;
import ate.util.X2X0;

public class NFECPacket extends AbstractCustomPacket<NFECPacket>{
    public static Charset charset=Global.charset;
	private String version;
	private static long seq=0;
	
	/**
	 * The main method.
	 *  
	 * @param args
	 *            the arguments
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public static void main(String[] args) throws UnknownHostException {
		NFECPacket pn = new NFECPacket();
		pn.add_usergroup("ap_a", "01-01-01-01-01-01");
		pn.add_usergroup("ap_b", "01-01-01-01-01-02");
		pn.add_user("192.168.3.2", "01-01-01-01-01-01", "CC1",
				"01-02-02-02-02-02");
		pn.add_user("192.168.3.3", null, "CC2",
				"01-02-02-02-02-03");
		System.out.println(pn.get_message());

	}

	/** The content. */
	private Vector<String> content = new Vector<String>();

	/** 目前支持的类型. */
	public static final String[] types = new String[] { "ap_mac", "sta_name",
			"sta_mac", "sta_ap_mac", "sta_ip", "ap_name" };

	/** T类型所对应的标签 tags. */
	public static final String[] tags = new String[] { "01", "01", "02", "03",
			"04", "06" };

	/** The Constant ADD. */
	public final static int ADD = 0;

	/** The Constant DELETE. */
	public final static int DELETE = 1;

	public NFECPacket() {
	    super();
	}
	
	public NFECPacket set_version(String v){
		this.version=v;
		return this;
	}
	
	public NFECPacket(String msg) {
		super(msg);
	}

	public String toString() {
		if(payload==null)
		    payload=get_message();
		return payload;
		
	}

	/**
	 * Instantiates a new plugin2 nfe. [aug_1:"ap_1",aug_2:"ap_2"] [dug:"ap_1"]
	 * [aug:[name:"ap_1",id:1] [dug:[name:"ap_1",id:100] [au:[login_name:"sta1",
	 * mac:"00-00-00-00-01-01",ap_name:"ap_p", ip:"1.1.1.1"]
	 * [au:[login_name:"sta1", mac:"00-00-00-00-01-01",ap_name:"ap_p",
	 * ip:"1.1.1.1",id:1]
	 * 
	 * @param hm
	 *            the hm
	 */
	public NFECPacket(Map<String, Object> hm) {
		Iterator it = hm.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next().toString();
			Object value = hm.get(key);
			if (key.startsWith("aug")) {
				content.add(
						_pnGetSize(value, "id"),
						new_usergroup(_pnGetString(value, "ap_name"),
								_pnGetString(value, "ap_mac"),ADD));
			} else if (key.startsWith("au")) {
				content.add(_pnGetSize(value, "id"), new_user((Map) value, ADD));
			}
		}
	}

	/**
	 * Adds the paras.
	 * 
	 * @param index
	 *            the index
	 * @param info
	 *            the info
	 */
	public NFECPacket add(int index, String info) {
		if (index == 0)
			this.content.add(0, info);
		else if (index >= content.size())
			this.content.add(content.size(), info);
		else
			this.content.add(index, info);
		return this;
	}

	/**
	 * Generate the new user. 除了支持types中定义的类型外，还支持len,指定长度 长度默认为消息长度+6
	 * paras中的value会严格的加到用包里面去，末尾的空格需要用户自己带上 Usage：
	 * add_user([ip:"1.1.1.1","login_name":"asdasdsa","len":0],10)
	 * 
	 * @param paras
	 *            the paras
	 * @param index
	 *            the index
	 * @return the string
	 * @see types
	 */
	public NFECPacket add_user(Map paras, int index) {
		this.content.add(index, new_user(paras, ADD));
		return this;

	}
	public NFECPacket del_user(Map paras, int index) {
		this.content.add(index, new_user(paras, DELETE));
		return this;

	}
	/**
	 * Adds the new user. mac地址会根据IP自动生成 如果login_name为null,则用MAC代替，参考Plugin的实现
	 * 
	 * @param IP
	 *            the iP
	 * @param AP_NAME
	 *            the a p_ name
	 * @param login_name
	 *            the login_name
	 * @return the string
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public NFECPacket add_user(String IP, String AP_MAC, String login_name,
			String STA_MAC) {
		try {
			content.add(new_user(IP, AP_MAC, login_name, STA_MAC, ADD));
		} catch (UnknownHostException e) {
			e.printStackTrace();
			throw new Error("packet wrong");
		}
		return this;
	}
	public NFECPacket del_user(String IP, String AP_MAC, String login_name,
			String STA_MAC) {
		try {
			content.add(new_user(IP, AP_MAC, login_name, STA_MAC, DELETE));
		} catch (UnknownHostException e) {
			e.printStackTrace();
			throw new Error("packet wrong");
		}
		return this;
	}
	/**
	 * Adds the new ug.
	 * 
	 * @param name
	 *            the name
	 * @return the string
	 */
	public NFECPacket add_usergroup(String name, String mac) {
		content.add(new_usergroup(name, mac,ADD));
		return this;
	}
	
	public NFECPacket del_usergroup(String name, String mac) {
		content.add(new_usergroup(name, mac,DELETE));
		return this;
	}
	
	private String _pnGetString(Object o, String key) {
		if (o instanceof Map) {
			Object v = ((Map) o).get(key);
			if (v != null)
				return ((Map) o).get(key).toString();
		}
		return o.toString();
	}

	private int _pnGetSize(Object o, String key) {
		if (o instanceof Map) {
			Object v = ((Map) o).get(key);
			if (v instanceof Integer)
				return ((Integer) v).intValue();
			else if (v != null)
				return Integer.parseInt(v.toString());
		}
		return content.size();
	}

	/**
	 * Clear paras content.
	 */
	public void clear() {
		content.clear();
	}
	
	public NFECPacket set_seq(long seq){
		this.seq=seq;
		return this;
	}
	
	/**
	 * Gets the message.
	 * 
	 * @return the message
	 */
	public synchronized String get_message() {
		if (payload != null)
			return payload;
		
		String msg = "";
		for (int i = 0; i < content.size(); i++) {
			msg += content.get(i);
		}
		
		if(version!=null){
			seq++;
			String tmp=""+seq;
			msg="51"+X2X0.lPad((6+tmp.length())+"", "0", 4)+tmp+msg;
			msg="500008"+version+msg;
		}
		
		String len = X2X0.lPad((msg.getBytes(Host.charset).length)
				+ "", "0", 4);
		payload=len + msg;
		
		return payload;
	}

	/**
	 * Gets the message.
	 * 
	 * @param len
	 *            the len
	 * @return the message
	 */
	public String get_message(int len) {		
		String msg = "";
		for (int i = 0; i < content.size(); i++) {
			msg += content.get(i);
		}
		if(version!=null){		
			seq++;
			String tmp=""+seq;
			msg="51"+X2X0.lPad((6+tmp.length())+"", "0", 4)+tmp+msg;
			msg="500008"+version+msg;
		}
		payload=X2X0.lPad(len + "", "0", 4) + msg;
		
		return payload;
	}

	/**
	 * Gets the length.
	 * 
	 * @return the length
	 */
	public int length() {
		int len = 0;
		for (int i = 0; i < content.size(); i++) {
			len += content.get(i).length();
		}
		return len;
	}

	/**
	 * New_user.
	 * 
	 * @param paras
	 *            the paras
	 * @param type
	 *            the type
	 * @return the string
	 */
	public String new_user(Map paras, int type) {
		String tmp = "";
		for (int i = 0; i < types.length; i++) {
			Object value = paras.get(types[i]);
			if (value == null)
				continue;
			tmp += (tags[i] + value.toString() + " ");
		}
		int len = 0;
		if (paras.get("len") != null)
			len = ((Integer) paras.get("len")).intValue();
		else
			len = tmp.getBytes().length+6;//加上tag
		
		if (type == ADD)
			return "10" + X2X0.lPad(len + "", "0", 4) + tmp;
		
		return "11" + X2X0.lPad(len + "", "0", 4) + tmp;
	}

	/**
	 * New_user.
	 * 
	 * @param IP
	 *            the iP
	 * @param AP_NAME
	 *            the a p_ name
	 * @param login_name
	 *            the login_name
	 * @param type
	 *            the type
	 * @return the string
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public String new_user(String IP, String AP_MAC, String login_name,
			String sta_mac, int type) throws UnknownHostException {
		HashMap<String, String> hm = new HashMap<String, String>();
		if (login_name != null && login_name.length() > 0)
			hm.put("sta_name", login_name);
		if (sta_mac != null && sta_mac.length() > 0)
			hm.put("sta_mac", sta_mac);
		if (AP_MAC != null && AP_MAC.length() > 0)
			hm.put("sta_ap_mac", AP_MAC);
		if (IP != null && IP.length() > 0)
			hm.put("sta_ip", IP);

		return new_user(hm, type);
	}

	/**
	 * New_usergroup.
	 * 
	 * @param name
	 *            the name
	 * @param type
	 *            the type
	 * @return the string
	 */
	public String new_usergroup(String name, String mac,int type) {
		String tmp = "";
		if(mac!=null&&mac.length()>0){
			tmp += tags[0] + mac;
			tmp+=" ";
		}
		if(name!=null&&name.length()>0){
			tmp += tags[5] + name;
			tmp+=" ";
		}
		tmp=tmp.substring(0,tmp.length()-1);
		String len = X2X0.lPad((tmp.getBytes(Host.charset).length + 4) + "",
				"0", 4);
		
		if(type==ADD)
			return "01" + len + tmp;
		else
			return "00" + len + tmp;
	}

    @Override
    public void encodePacket(OutputStream out) {
        try {
            out.write(this.get_message().getBytes(charset));
        } catch (IOException e) {
            throw new RunFailureException(e);
        }
        
    }

    public static NFECPacket decode_packet(IoBuffer buf){
        NFECPacket pkt=new NFECPacket();
        try {
            pkt.payload=buf.getString(charset.newDecoder());
        } catch (CharacterCodingException e) {
            throw new RunFailureException(e);
        }
        return pkt;
    }
    
    @Override
    public NFECPacket build(String k, Object v) {
        if(k.equals("version"))
            this.version=v.toString();
        else if(k.equals("payload"))
            this.payload=v.toString();
        return null;
    }

    @Override
    public NFECPacket get_packet() {
        return this;
    }

}
