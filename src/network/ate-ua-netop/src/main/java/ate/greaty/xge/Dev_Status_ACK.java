package ate.greaty.xge;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.joou.UByte;
import org.joou.UInteger;

import ate.greaty.xge.BLOCK_POLICY.Block;
import ate.util.X2X0;
import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;

/**
 * 设备状态查询响应
 * 
 * @author dongdong.dai
 *
 */
public class Dev_Status_ACK extends CmccDpiPolicyPacket {
    public UInteger SoftwareVersion = uint(0);// 设备软件版本号
    public UByte DeploySiteNameLength = ubyte(0);// 设备部署站点名长度
    public String DeploySiteName = "";// 设备部署站点名
    public UByte PortsTypeNum = ubyte(0);// 设备所配备的端口类型数
    public List<PortType> porttype = new ArrayList<>();;
    public List<PortDesc> portdesc = new ArrayList<>();;

    public class PortType {
        public UByte PortType = ubyte(0);// 设备现有端口类型
        public UByte PortsNum = ubyte(0);// 属于本类型的端口数
        public String toString(){
            return "PortType="+PortType+"\nPortsNum="+PortsNum;
        }
    }

    public class PortDesc {
        public UByte PortNo = ubyte(0);// 本端口的编号
        public UByte PortDescLength = ubyte(0);// 本端口描述信息长度
        public String PortDescription = "";// 本端口描述信息
        public UByte M_LinkID = ubyte(0);// 本端口监控链路编号
        public UByte M_LinkDescLength = ubyte(0);// 本端口监控链路描述信息长度
        public String M_LinkDesc = "";// 监控链路描述信息

        public int get_len() {
            PortDescLength = ubyte(PortDescription.getBytes(charset).length);
            M_LinkDescLength = ubyte(M_LinkDesc.getBytes(charset).length);
            return 4 + PortDescLength.intValue() + M_LinkDescLength.intValue();
        }
        public String toString(){
            return "PortNo="+PortNo+
                    "\nPortDescLength="+PortDescLength+
                    "\nPortDescription="+PortDescription+
                    "\nM_LinkID="+M_LinkID
                    +"\nM_LinkDescLength="+M_LinkDescLength+
                    "\nM_LinkDesc="+M_LinkDesc;
        }
    }

    public Dev_Status_ACK add_port_type(HashMap<String, Object> hm) {
        PortType pt = new PortType();
        pt.PortType = ubyte((Integer) hm.get("PortType"));
        pt.PortsNum = ubyte((Integer) hm.get("PortsNum"));
        porttype.add(pt);
        return this;
    }
    public boolean has_port_type(HashMap<String, Object> hm) {
        for (int i = 0; i < this.porttype.size(); i++) {
            PortType pt = this.porttype.get(i);
            
            if ((hm.get("PortsNum")==null||(int)hm.get("PortsNum")==pt.PortsNum.intValue())
                    && (hm.get("PortType")==null||(int)hm.get("PortType")==pt.PortType.intValue())) {
                return true;
            } 
        }
        return false;
    }
    
    public Dev_Status_ACK add_port_desc(HashMap<String, Object> hm) {
        PortDesc pd = new PortDesc();
        pd.PortNo = ubyte((Integer) hm.get("PortNo"));
        
        if (hm.get("PortDescLength") != null)
            pd.PortDescLength = ubyte((Integer) hm.get("PortDescLength"));
        
        pd.PortDescription = (String) hm.get("PortDescription");
        
        pd.M_LinkID = ubyte((Integer) hm.get("M_LinkID"));
        
        if (hm.get("M_LinkDescLength") != null)
            pd.M_LinkDescLength = ubyte((Integer) hm.get("M_LinkDescLength"));
        
        pd.M_LinkDesc = (String) hm.get("M_LinkDesc");
        
        portdesc.add(pd);
        return this;
    }
    
    public boolean has_port_desc(HashMap<String, Object> hm) {
        for (int i = 0; i < this.portdesc.size(); i++) {
            PortDesc pt = this.portdesc.get(i);
            Object o=hm.get("PortNo");
            if ((hm.get("PortNo")==null||(int)hm.get("PortNo")==pt.PortNo.intValue())
                && (hm.get("PortDescLength")==null||(int)hm.get("PortDescLength")==pt.PortDescLength.intValue())
                && (hm.get("PortDescription")==null||hm.get("PortDescription").equals(pt.PortDescription))
                && (hm.get("M_LinkID")==null||(int)hm.get("M_LinkID")==pt.M_LinkID.intValue())
                && (hm.get("M_LinkDescLength")==null||(int)hm.get("M_LinkDescLength")==pt.M_LinkDescLength.intValue())
                && (hm.get("M_LinkDesc")==null||hm.get("M_LinkDesc").equals(pt.M_LinkDesc))
             ) {
                return true;
            } 
        }
        return false;
    }
    public Dev_Status_ACK() {
        MessageType = PT.Dev_Status_ACK.getValue();
    }

    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        SoftwareVersion = uint(din.readInt());
        DeploySiteNameLength = ubyte(din.readByte());
        DeploySiteName = this.readString(din, DeploySiteNameLength.intValue());
        PortsTypeNum = ubyte(din.readByte());
        this.porttype = new ArrayList<>(PortsTypeNum.intValue());
        for (int i = 0; i < this.PortsTypeNum.intValue(); i++) {
            PortType pt = new PortType();
            if(din.available()<=0)
                break;
            pt.PortType = ubyte(din.readByte());
            pt.PortsNum = ubyte(din.readByte());
            porttype.add(pt);
        }
        
        this.portdesc = new ArrayList<>();
        int pos = this.HEADER_LENGTH + 5 + DeploySiteNameLength.intValue() + 1
                + 2 * porttype.size();
        
        log.debug("MessageLength is {}",MessageLength.intValue());
        
        while (pos < this.MessageLength.intValue()) {
            PortDesc pd = new PortDesc();
            pd.PortNo = ubyte(din.readByte());
            pd.PortDescLength = ubyte(din.readByte());
            pd.PortDescription = readString(din, pd.PortDescLength.intValue());
            pd.M_LinkID = ubyte(din.readByte());
            pd.M_LinkDescLength = ubyte(din.readByte());
            pd.M_LinkDesc = this
                    .readString(din, pd.M_LinkDescLength.intValue());
            portdesc.add(pd);

            pos += 4 + pd.PortDescLength.intValue()
                    + pd.M_LinkDescLength.intValue();
        }
        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if ("SoftwareVersion".equalsIgnoreCase(k))
            SoftwareVersion = uint((long) v);
        else if ("DeploySiteNameLength".equalsIgnoreCase(k))
            DeploySiteNameLength = ubyte((Integer) v);
        else if ("DeploySiteName".equalsIgnoreCase(k))
            DeploySiteName = (String) v;
        else if ("PortsTypeNum".equalsIgnoreCase(k))
            PortsTypeNum = ubyte((Integer) v);
        else
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {
        DeploySiteNameLength = ubyte(DeploySiteName.getBytes(charset).length);
        PortsTypeNum = ubyte(porttype.size());
        int pd_len = 0;
        for (PortDesc tmp : this.portdesc) {
            pd_len += tmp.get_len();
        }
        MessageLength = uint(this.HEADER_LENGTH + 10 + pd_len + porttype.size()
                * 2);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
        out.writeInt(SoftwareVersion.intValue());
        out.writeByte(DeploySiteNameLength.byteValue());
        out.write(DeploySiteName.getBytes(charset));
        out.writeByte(PortsTypeNum.byteValue());
        for (PortType tmp : porttype) {
            out.writeByte(tmp.PortType.byteValue());
            out.writeByte(tmp.PortsNum.byteValue());
        }
        for (PortDesc tmp : this.portdesc) {
            out.writeByte(tmp.PortNo.byteValue());
            out.writeByte(tmp.PortDescLength.byteValue());
            out.write(tmp.PortDescription.getBytes(charset));
            out.writeByte(tmp.M_LinkID.byteValue());
            out.writeByte(tmp.M_LinkDescLength.byteValue());
            out.write(tmp.M_LinkDesc.getBytes(charset));
        }
        return this;
    }
    public String toString(){
        return super.toString()+
                "\nSoftwareVersion="+this.SoftwareVersion+
                "\nDeploySiteNameLength"+this.DeploySiteNameLength+
                "\nDeploySiteName="+this.DeploySiteName+
                "\nPortsTypeNum="+PortsTypeNum+
                "\nPorts="+print_ports();
    }
    private String print_ports(){
        String rtn="";
        for(PortType tmp:this.porttype){
            rtn+=tmp.toString()+"\n";
        }
        for(PortDesc tmp:this.portdesc){
            rtn+=tmp.toString()+"\n";
        }
        return rtn;
    }

}
