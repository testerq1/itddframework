package ate.greaty.xge;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.joou.UByte;
import org.joou.UInteger;

/**
 * 8.4.2.9  策略同步消息
 * @author dongdong.dai
 *
 */
public class POLICY_SYNC extends CmccDpiPolicyPacket {
    public UByte MessageTypeNum=ubyte(0) ;//准备同步的策略类型数 
    public UByte MessageType1=ubyte(0);//策略类型
    public UInteger MessageSerialNo=uint(0);//DPI设备拥有的，当前策略类型的策略序列号
    
    public POLICY_SYNC(){
        MessageType=PT.POLICY_SYNC.getValue();
    }

    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        MessageTypeNum=ubyte(din.readByte());
        MessageType1=ubyte(din.readByte());
        MessageSerialNo=uint(din.readInt());
        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if("MessageTypeNum".equalsIgnoreCase(k))
            MessageTypeNum=ubyte((Integer)v);
        else if("MessageType1".equalsIgnoreCase(k))
            MessageType1=ubyte((Integer)v);
        else if("MessageSerialNo".equalsIgnoreCase(k))
            MessageSerialNo=uint((Integer)v);
        else
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {
        MessageLength=uint(6);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
        out.writeByte(MessageTypeNum.byteValue());
        out.writeByte(MessageType1.byteValue());
        out.writeInt(MessageSerialNo.intValue());        
        return this;
    }

}
