package ate.greaty.xge;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;
import static org.joou.Unsigned.ushort;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.joou.UByte;
import org.joou.UInteger;
import org.joou.UShort;

import ate.util.X2X0;
/**
 * 8.4.2.10 策略同步请求回应
 * @author dongdong.dai
 *
 */
public class POLICY_SYNC_ACK extends CmccDpiPolicyPacket {
    public UByte MessageType2=ubyte(0) ;//策略类型
    public UInteger MessageSerialNo=uint(0) ;//策略服务器拥有的，当前策略类型最新的序列号
    public UShort MessageNum ;//策略数
    public List<Message> mes = new ArrayList<Message>();
    
    public class Message{
        public UInteger MessageContentLength=uint(0) ;//策略内容长度
        public byte[] MessageContents =new byte[0] ;//策略内容应只包含各策略的主体内容(不含配置接口报文头)
        //MessageContents doc文档没有定义长度，类型。故类型，长度是自己设置
                                                      
        public int len(){
            MessageContentLength=uint(this.MessageContents.length);
            return 4+MessageContentLength.intValue();
        }
        public String toString(){
            return X2X0.bs2chs(MessageContents,"");
        }
 
    } 
    public POLICY_SYNC_ACK add_msgcontent(String hexstring){
        Message pd =new Message();
        pd.MessageContentLength=uint(pd.MessageContents.length);
        pd.MessageContents=X2X0.hexs2b(hexstring);       
        this.mes.add(pd);
        return this;
        
    }
    public POLICY_SYNC_ACK(){
        MessageType=PT.POLICY_SYNC_ACK.getValue();
        MessageVersion=uint(1);
    }
    

    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        MessageType2=ubyte(din.readByte());
        MessageSerialNo=uint(din.readInt());
        MessageNum=ushort(din.readShort());
        this.mes=new ArrayList<>(MessageNum.shortValue());
        for(int i=0;i<MessageNum.intValue();i++){
            Message pd =new Message(); 
            pd.MessageContentLength=uint(din.readInt());
            pd.MessageContents=new byte[pd.MessageContentLength.intValue()];
            din.read(pd.MessageContents);
            mes.add(pd);
        }
        
        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if("MessageType2".equalsIgnoreCase(k))
            MessageType2=ubyte((Integer)v);
        else if("MessageSerialNo".equalsIgnoreCase(k))
            MessageSerialNo=uint((Integer)v);
        else if("MessageNum".equalsIgnoreCase(k))
            MessageNum=ushort(((Integer)v).shortValue());        
        else
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {
        if(MessageNum==null)
         MessageNum=ushort(this.mes.size());
        int pd_len = 0;
        for (Message tmp : this.mes) {
            pd_len += tmp.len();
        }
        MessageLength=uint(7+this.HEADER_LENGTH+pd_len);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
       out.writeByte(MessageType2.byteValue());
       
       out.writeInt(MessageSerialNo.intValue());
       out.writeShort(MessageNum.shortValue());
       
       for(Message mes : this.mes){
           out.writeInt(mes.MessageContentLength.intValue());
           out.write(mes.MessageContents);
       }
       
       
        return this;
    }

}
