package ate.greaty.nfe;

import org.apache.mina.core.buffer.IoBuffer;

import ate.ua.mina.AbstractCustomUA;

public class ControlUA extends AbstractCustomUA<NFECPacket>{
    
    @Override
    public int get_port(){
        if(port<=0)
            port=50002;
        return port;
    }
    
	public NFECPacket create_packet(){
		return new NFECPacket();
	}
    
    @Override
    public String get_default_schema() {
        return "nfeci";
    }
	//v2版本的包
	public NFECPacket create_packet(String v){
        NFECPacket pkt=new NFECPacket();
        pkt.set_version(v);
        return pkt;
    }

    @Override
    public NFECPacket decodePacket(IoBuffer in) {        
        return NFECPacket.decode_packet(in);        
    }
}
