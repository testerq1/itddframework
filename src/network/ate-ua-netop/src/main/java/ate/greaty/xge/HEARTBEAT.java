package ate.greaty.xge;

import static org.joou.Unsigned.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Date;

import org.joou.UByte;

import ate.ua.RunFailureException;

public class HEARTBEAT extends CmccDpiPolicyPacket {
    public UByte Dev_Name_Length=ubyte(0);
    public String Dev_Name="";

    public HEARTBEAT() {
        MessageType = PT.HEARTBEAT.getValue();
    }
    
    public CmccDpiPolicyPacket create_response(){
        HEARTBEAT_ACK resp=(HEARTBEAT_ACK)create_packet(PT.HEARTBEAT_ACK);       
        resp.version=version;
        resp.MessageID=MessageID;       
        resp.Timestamp=uint(new Date().getTime()/1000);
        resp.MessageVersion=MessageVersion;
        resp.Result=ubyte(1);
        return resp;
       
    }
    
    public CmccDpiPolicyPacket decode_packet(DataInputStream din) {
        try {
            Dev_Name_Length = ubyte(din.readByte());            
            Dev_Name = readString(din,Dev_Name_Length.intValue());
            
        }  catch (IOException e) {
            throw new RunFailureException(e);
        }
        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if("Dev_Name".equalsIgnoreCase(k))
            Dev_Name=v.toString();
        else if("Dev_Name_Length".equalsIgnoreCase(k)){
            Dev_Name_Length=ubyte((Integer)v);
        }else
            super.build(k, v);
        
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {   
        super.get_packet();
        Dev_Name_Length=ubyte(Dev_Name.getBytes(charset).length);        
        MessageLength=uint(HEADER_LENGTH+1+(Dev_Name_Length.intValue()));
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {     
        out.writeByte(Dev_Name_Length.byteValue());
        out.write(Dev_Name.getBytes(charset));        
        return this;
    }
}
