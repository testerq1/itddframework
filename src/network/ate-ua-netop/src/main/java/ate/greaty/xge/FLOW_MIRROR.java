package ate.greaty.xge;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;
import static org.joou.Unsigned.ushort;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.joou.UByte;
import org.joou.UInteger;
import org.joou.UShort;

import ate.util.X2X0;

/**
 * 流量镜像策略
 * 
 * @author dongdong.dai
 *
 */
public class FLOW_MIRROR extends CmccDpiPolicyPacket {
    public class MDstIp {

        public UByte MDstIpAddrLen = ubyte(0); // IP地址长度
        public String MDstIpAddr = ""; // IP地址
        public UByte MDstIpPrefixLen = ubyte(0); // IP地址前缀长度

        public int len() {
            MDstIpAddrLen = ubyte(X2X0.ip2b(MDstIpAddr).length);
            return 2 + MDstIpAddrLen.intValue();
        }
    }

    public class MDstPort {
        public UShort MDstPort = ushort(0); // 目的端口
    }

    public class MSrcPort {
        public UShort MSrcPort = ushort(0); // 源端口

    }

    public UInteger PolicyID = uint(0);
    public UByte MProtocalType = ubyte(0); // 被镜像的协议类型
    public UByte MAppType = ubyte(0); // 被镜像的应用类型

    public UShort MAppID = ushort(0); // 被镜像的应用编号
    public UByte MDstIpSegmentNum = ubyte(0); // 目的IP段个数
    public List<MDstIp> mdstIp = new ArrayList<MDstIp>();;
    public UByte MSrcPortNum = ubyte(0); // 源端口个数

    public List<MSrcPort> msrc = new ArrayList<MSrcPort>();

    public UByte MDstPortNum = ubyte(0); // 目的端口个数

    public List<MDstPort> mdst = new ArrayList<MDstPort>();
    public UShort MDataMatchOffset = ushort(0); // 需要匹配的数据地址偏移大小

    public UByte MDataMatchLen = ubyte(0); // 需要匹配的数据内容长度

    public String MDataMatchContent = ""; // 需要匹配的字串

    public UInteger M_StartTime = uint(0); // 镜像起始时间 4bytes
    public UInteger M_EndTime = uint(0); // 镜像终止时间 4bytes
    public UByte M_GroupNo = ubyte(0); // 镜像端口组
    public UByte M_Direction = ubyte(0); // 镜像方向
    public UByte MFlowAdd = ubyte(0); // 是否需要转发整条流
    public UByte MType = ubyte(0); // 转发方式

    public FLOW_MIRROR() {
        MessageType = PT.FLOW_MIRROR.getValue();
        MessageVersion = uint(1);
    }

    public FLOW_MIRROR add_mdst(HashMap<String, Object> hm) {
        MDstPort pd = new MDstPort();
        pd.MDstPort = ushort((Integer) hm.get("MDstPort"));
        this.mdst.add(pd);
        return this;
    }

    public FLOW_MIRROR add_mdstip(HashMap<String, Object> hm) {
        MDstIp pd = new MDstIp();
        if (null != hm.get("MDstIpAddrLen"))
            pd.MDstIpAddrLen = ubyte((int) hm.get("MDstIpAddrLen"));
        else
            pd.MDstIpAddrLen = ubyte(32);

        pd.MDstIpAddr = (String) hm.get("MDstIpAddr");

        if (null != hm.get("MDstIpPrefixLen"))
            pd.MDstIpPrefixLen = ubyte((int) hm.get("MDstIpPrefixLen"));
        else
            pd.MDstIpAddrLen = ubyte(24);
        this.mdstIp.add(pd);

        return this;
    }

    public FLOW_MIRROR add_msrc(HashMap<String, Object> hm) {
        MSrcPort pd = new MSrcPort();
        pd.MSrcPort = ushort((Integer) hm.get("MSrcPort"));
        this.msrc.add(pd);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if ("PolicyID".equalsIgnoreCase(k))
            PolicyID = uint((Integer) v);
        else if ("MProtocalType".equalsIgnoreCase(k))
            MProtocalType = ubyte((Integer) v);
        else if ("MAppType".equalsIgnoreCase(k))
            MAppType = ubyte((Integer) v);
        else if ("MAppID".equalsIgnoreCase(k))
            MAppID = ushort((Integer) v);
        else if ("MDstIpSegmentNum".equalsIgnoreCase(k))
            MDstIpSegmentNum = ubyte((Integer) v);

        else if ("mdstIp".equalsIgnoreCase(k)) {
            mdstIp = new ArrayList<>();
            HashMap<String, Object> hm = (HashMap<String, Object>) v;
            for (int i = 0; i < hm.keySet().size(); i += 3) {
                int seq = i / 3;
                MDstIp pd = new MDstIp();
                if (hm.get("MDstIpAddrLen" + seq) != null)
                    pd.MDstIpAddrLen = ubyte((Integer) hm.get("MDstIpAddrLen"
                            + seq));

                pd.MDstIpAddr = (String) hm.get("MDstIpAddr" + seq);
                pd.MDstIpPrefixLen = ubyte((Integer) hm.get("MDstIpPrefixLen"
                        + seq));
                mdstIp.add(pd);
            }
        }

        else if ("MSrcPortNum ".equalsIgnoreCase(k))
            MSrcPortNum = ubyte((Integer) v);

        else if ("MDstPortNum".equalsIgnoreCase(k))
            MDstPortNum = ubyte((Integer) v);

        else if ("MDataMatchOffset".equalsIgnoreCase(k))
            MDataMatchOffset = ushort((Integer) v);
        else if ("MDataMatchLen".equalsIgnoreCase(k))
            MDataMatchLen = ubyte((Integer) v);
        else if ("MDataMatchContent".equalsIgnoreCase(k))
            MDataMatchContent = (String) v;
        else if ("M_StartTime".equalsIgnoreCase(k))
            M_StartTime = uint((Integer) v);
        else if ("M_EndTime".equalsIgnoreCase(k))
            M_EndTime = uint((Integer) v);
        else if ("M_GroupNo".equalsIgnoreCase(k))
            M_GroupNo = ubyte((Integer) v);
        else if ("M_Direction".equalsIgnoreCase(k))
            M_Direction = ubyte((Integer) v);
        else if ("MFlowAdd".equalsIgnoreCase(k))
            MFlowAdd = ubyte((Integer) v);
        else if ("MType".equalsIgnoreCase(k))
            MType = ubyte((Integer) v);
        else
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        PolicyID = uint(din.readInt());
        MProtocalType = ubyte(din.readByte());
        MAppType = ubyte(din.readByte());
        MAppID = ushort(din.readShort());
        MDstIpSegmentNum = ubyte(din.readByte());

        int pos = this.HEADER_LENGTH + 9;

        for (int i = 0; i < MDstIpSegmentNum.intValue(); i++) {
            if (pos >= this.MessageLength.intValue()) {
                log.warn("MDstIpSegmentNum长度大于实际IP地址段信息长度: {}",
                        MDstIpSegmentNum);
                break;
            }
            MDstIp pd = new MDstIp();
            pd.MDstIpAddrLen = ubyte(din.readByte());

            byte[] ip = new byte[pd.MDstIpAddrLen.byteValue()];
            din.read(ip);
            pd.MDstIpAddr = X2X0.bs2ip(ip);

            pd.MDstIpPrefixLen = ubyte(din.readByte());
            mdstIp.add(pd);
            pos += (2 + pd.MDstIpAddrLen.intValue());
        }
        MSrcPortNum = ubyte(din.readByte());

        for (int i = 0; i < MSrcPortNum.intValue(); i++) {
            MSrcPort pd = new MSrcPort();
            pd.MSrcPort = ushort(din.readShort());
        }

        MDstPortNum = ubyte(din.readByte());

        for (int i = 0; i < MSrcPortNum.intValue(); i++) {
            MDstPort pd = new MDstPort();
            pd.MDstPort = ushort(din.readShort());
        }

        MDataMatchOffset = ushort(din.readShort());
        MDataMatchLen = ubyte(din.readByte());
        MDataMatchContent = this.readString(din, MDataMatchLen.intValue());
        M_StartTime = uint(din.readInt());
        M_EndTime = uint(din.readInt());
        M_GroupNo = ubyte(din.readByte());
        M_Direction = ubyte(din.readByte());
        MFlowAdd = ubyte(din.readByte());
        MType = ubyte(din.readByte());
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
        out.writeInt(PolicyID.intValue());
        out.writeByte(MProtocalType.byteValue());
        out.writeByte(MAppType.byteValue());
        out.writeShort(MAppID.shortValue());
        out.writeByte(MDstIpSegmentNum.byteValue());

        for (MDstIp tmp : mdstIp) {
            out.writeByte(tmp.MDstIpAddrLen.byteValue());
            out.write(X2X0.ip2b(tmp.MDstIpAddr));
            out.writeByte(tmp.MDstIpPrefixLen.byteValue());
        }

        out.writeByte(MSrcPortNum.byteValue());
        for (MSrcPort ms : this.msrc) {
            out.writeShort(ms.MSrcPort.shortValue());
        }
        out.writeByte(MDstPortNum.byteValue());
        for (MDstPort md : this.mdst) {
            out.writeShort(md.MDstPort.shortValue());
        }
        out.writeShort(MDataMatchOffset.shortValue());
        out.writeByte(MDataMatchLen.byteValue());
        out.write(MDataMatchContent.getBytes(charset));
        out.writeInt(M_StartTime.intValue());
        out.writeInt(M_EndTime.intValue());
        out.writeByte(M_GroupNo.byteValue());
        out.writeByte(M_Direction.byteValue());
        out.writeByte(MFlowAdd.byteValue());
        out.writeByte(MType.byteValue());
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {
        int pd_len = 0;
        
        MDstIpSegmentNum = ubyte(this.mdstIp.size());
        for (MDstIp tmp : this.mdstIp) {
            pd_len += tmp.len();
        }
        log.debug("pd_len==" + pd_len);
        
        MSrcPortNum = ubyte(this.msrc.size());
        MDstPortNum = ubyte(this.mdst.size());
        
        MDataMatchLen = ubyte(MDataMatchContent.getBytes(charset).length);

        MessageLength = uint(26 + pd_len + MDataMatchLen.intValue()
                + this.HEADER_LENGTH + 2 * MSrcPortNum.intValue() + 2
                * MDstPortNum.intValue());
        System.out.println("MessageLength==" + MessageLength);
        return this;
    }

}
