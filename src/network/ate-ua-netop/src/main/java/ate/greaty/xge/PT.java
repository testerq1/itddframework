package ate.greaty.xge;

import org.joou.UByte;
import static org.joou.Unsigned.*;

public enum PT{
    HEARTBEAT(0x00, "心跳消息"),
    HEARTBEAT_ACK(0x01, "心跳消息响应"),
    STATIC_IP_SYNC(0x02, "静态IP用户同步消息"),
    UG_HOME(0x03, "用户组归属分配消息"),
    RES_SPEC_POLICY(0x04, "资源特征库下发策略"),
    FLOW_MIRROR(0x05, "流量镜像策略"),
    FLOW_MAN(0x06, "流量管理策略"),
    BLOCK_POLICY(0x07, "封堵URL/IP/域名同步策略"),
    SITE_ACCESS(0x08, "网站访问分析策略"),
    REPORTER_POLICY(0x09, "流量分析结果上报策略"),
    POLICY_BIND(0x0a, "策略与对象绑定"),
    POLICY_SYNC(0x0b, "策略同步消息"),
    POLICY_SYNC_ACK(0x0c, "策略同步请求回应"),
    ACK(0x0d, "通用Ack"),
    Dev_Status(0x0e,"DPI设备状态查询"),
    Dev_Status_ACK(0x0f,"设备状态查询响应");    
    public static String get_name(int value) {
        if (value >= PT.values().length)
            return null;
        return PT.values()[value].name();
    }
    public static PT get_pt(int value) {
        if (value >= PT.values().length)
            return null;
        PT[] tmp=PT.values();
        return PT.values()[value];
    }
    
    public static PT get_pt(String name) {
        if(PT.valueOf(name)!=null)
            return PT.valueOf(name);
        else{
            for(PT tmp:PT.values()){
                if(tmp.name.equals(name))
                    return tmp;
            }                
        }
        return null;
    }

    private UByte value;

    String name;

    private PT(int value, String name) {
        this.value = ubyte(value);
        this.name = name;
    }

    public UByte getValue() {
        return value;
    }
    public static UByte max(){
        return ACK.getValue();
    }
    
    public String toString() {
        return PT.values()[value.byteValue()].name() + "(" + name + "," + value + ")";
    }
}