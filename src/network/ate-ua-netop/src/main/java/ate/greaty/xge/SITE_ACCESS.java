package ate.greaty.xge;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.uint;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.joou.UByte;
import org.joou.UInteger;

/**
 * 8.4.2.7.4 网站访问分析策略
 * 
 * @author dongdong.dai
 *
 */
public class SITE_ACCESS extends CmccDpiPolicyPacket {
    public UInteger PolicyID = uint(0); // 策略ID
    public UByte Action = ubyte(0); // 是否开启网站访问分析
    public UByte SRC_AREAGROUP_ID = ubyte(0); // 源区域组ID
    public UByte SRC_Area_Num = ubyte(0); // SRC_Area区域个数
    public List<SRC_Area> src_area = new ArrayList<SRC_Area>();
    public List<DEST_Area> dest_area = new ArrayList<DEST_Area>();

    public class SRC_Area {
        public UByte SRC_Area_Name_Length; // SRC_Area_Name长度
        public Object SRC_Area_Name; // SRC_Area名称 如果SRC_Area_Num=0，则无该字段

        public int len() {
            if (SRC_Area_Num.intValue() != 0)
                SRC_Area_Name_Length = ubyte(this.SRC_Area_Name.toString()
                        .getBytes(charset).length);
            return 1 + SRC_Area_Name_Length.intValue();   
            
              
            

        }
    }

    public SITE_ACCESS add_src(HashMap<String, Object> hm) {
        SRC_Area pd = new SRC_Area();
        pd.SRC_Area_Name = (String) hm.get("SRC_Area_Name");
        this.src_area.add(pd);
        return this;
    }

    public UByte DEST_AREAGROUP_ID = ubyte(0); // 目的区域组ID
    public UByte DEST_Area_Num = ubyte(0); // DEST_Area区域个数

    public class DEST_Area {
        public UByte DEST_Area_Name_Length; // DEST_Area_Name长度
        public Object DEST_Area_Name = ""; // DEST_Area名称
        // 如果DEST_Area_Num=0，则无该字段

        public int len_1() {
            if (DEST_Area_Num.intValue() != 0)

                DEST_Area_Name_Length = ubyte(this.DEST_Area_Name.toString()
                        .getBytes(charset).length);
            return 1 + DEST_Area_Name_Length.intValue();

        }
    }

    public SITE_ACCESS add_dest(HashMap<String, Object> hm) {

        DEST_Area pd = new DEST_Area();
        pd.DEST_Area_Name = (String) hm.get("DEST_Area_Name");
        this.dest_area.add(pd);
        return this;

    }

    public SITE_ACCESS() {
        MessageType = PT.SITE_ACCESS.getValue();
        MessageVersion = uint(2);
    }

    @Override
    public CmccDpiPolicyPacket decode_packet(DataInputStream din)
            throws IOException {
        PolicyID = uint(din.readInt());
        Action = ubyte(din.readByte());
        SRC_AREAGROUP_ID = ubyte(din.readByte());
        SRC_Area_Num = ubyte(din.readByte());

        for (int i = 0; i < SRC_Area_Num.intValue(); i++) {
            SRC_Area pd = new SRC_Area();
            pd.SRC_Area_Name_Length = ubyte(din.readByte());
            pd.SRC_Area_Name = this.readString(din,
                    pd.SRC_Area_Name_Length.intValue());
        }

        DEST_AREAGROUP_ID = ubyte(din.readByte());
        DEST_Area_Num = ubyte(din.readByte());

        for (int i = 0; i < DEST_Area_Num.intValue(); i++) {
            DEST_Area pd = new DEST_Area();
            pd.DEST_Area_Name_Length = ubyte(din.readByte());
            pd.DEST_Area_Name = this.readString(din,
                    pd.DEST_Area_Name_Length.intValue());
        }

        return this;
    }

    @Override
    public CmccDpiPolicyPacket build(String k, Object v) {
        if ("PolicyID".equalsIgnoreCase(k))
            PolicyID = uint((Integer) v);
        else if ("Action".equalsIgnoreCase(k))
            Action = ubyte((Integer) v);
        else if ("SRC_AREAGROUP_ID".equalsIgnoreCase(k))
            SRC_AREAGROUP_ID = ubyte((Integer) v);
        else if ("SRC_Area_Num".equalsIgnoreCase(k))
            SRC_Area_Num = ubyte((Integer) v);

        else if ("DEST_AREAGROUP_ID".equalsIgnoreCase(k))
            DEST_AREAGROUP_ID = ubyte((Integer) v);
        else if ("DEST_Area_Num".equalsIgnoreCase(k))
            DEST_Area_Num = ubyte((Integer) v);

        else
            super.build(k, v);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket get_packet() {
        SRC_Area_Num=ubyte(this.src_area.size());
        DEST_Area_Num=ubyte(this.dest_area.size());

        int pd_len = 0;
        for (SRC_Area tmp : this.src_area) {
            pd_len += tmp.len();
        }

        int pd_len1 = 0;
        for (DEST_Area tmp : this.dest_area) {
            pd_len1 += tmp.len_1();
        }

        MessageLength = uint(9 + pd_len + pd_len1 + this.HEADER_LENGTH);
        return this;
    }

    @Override
    public CmccDpiPolicyPacket encode_packet(DataOutputStream out)
            throws IOException {
        out.writeInt(PolicyID.intValue());
        out.writeByte(Action.byteValue());
        out.writeByte(SRC_AREAGROUP_ID.byteValue());
        out.writeByte(SRC_Area_Num.byteValue());
        for (SRC_Area src : this.src_area) {
            if (SRC_Area_Num.intValue() != 0) 
                out.writeByte(src.SRC_Area_Name_Length.byteValue());
                out.write(src.SRC_Area_Name.toString().getBytes(charset));
            
        }

        out.writeByte(DEST_AREAGROUP_ID.byteValue());
        out.writeByte(DEST_Area_Num.byteValue());

        for (DEST_Area des : this.dest_area) {
            if (DEST_Area_Num.intValue() != 0) 
                out.writeByte(des.DEST_Area_Name_Length.byteValue());
                out.write(des.DEST_Area_Name.toString().getBytes(charset));
            
        }

        return this;
    }

}
