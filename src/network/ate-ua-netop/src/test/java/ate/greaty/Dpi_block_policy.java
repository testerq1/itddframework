package ate.greaty;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.Global;
import ate.greaty.xge.BLOCK_POLICY;
import ate.greaty.xge.CmccDpiPolicyPacket;
import ate.greaty.xge.CmccDpiPolicyUA;
import ate.greaty.xge.POLICY_BIND;
import ate.greaty.xge.PT;
import ate.testcase.CsUATestcase;

public class Dpi_block_policy extends CsUATestcase {
    CmccDpiPolicyUA svr;

    @BeforeClass
    public void BeforeClass() {
        super.BeforeClass();
        Global.verbose = true;
        svr = create_server("cdp://" + siip + ":5555/tcp",
                new CmccDpiPolicyUA());
        this.start_all_ua();
        assertTrue(svr.is_ready());
        // this.shutdown_all_ua();
    }

   
    /**
     * 封堵URL/IP/域名同步策略 0x07 域名
     */
  //   @Test
    public void BLOCK_POLICY_1() {
        STEP(PT.BLOCK_POLICY.toString());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);
        for(int i=1;i<2;i++){
            
        int PolicyID = i ;
        int Action = 1;
        int Block_Type = 3; //1 URL;2, IP;3, 域名
       
        String Block_Name1 = "www.tian.com";
        String Block_Name = "www.tiantian.com";
        BLOCK_POLICY pn = (BLOCK_POLICY) svr.create_packet(PT.BLOCK_POLICY)
                .build("PolicyID", PolicyID).build("Action", Action)
                .build("Block_Type", Block_Type);
    

        HashMap<String, Object> block = new HashMap<String, Object>();
        block.put("Block_Name", Block_Name);

        HashMap<String, Object> block1 = new HashMap<String, Object>();
        block1.put("Block_Name", "www.tt.com");

        HashMap<String, Object> block2 = new HashMap<String, Object>();
        block2.put("Block_Name", "www.dd.com");

        HashMap<String, Object> block3 = new HashMap<String, Object>();
        block3.put("Block_Name", "www.ff.com");

        HashMap<String, Object> block4 = new HashMap<String, Object>();
        block4.put("Block_Name", Block_Name1);

        pn.add_block(block).add_block(block1).add_block(block2)
                .add_block(block3).add_block(block4);

        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        
        
        
    STEP(PT.POLICY_BIND.toString());
       
        
        int ServerNameNum = 1;
        int Bind_Action = 1;
        int UserType;
        String UserName;
        if(i<=100){
             UserType = 1;     //0 所有用户;1 静态用户;2 用户组
             UserName = "556"; 
        }
        else if(i>100 && i<=200){
             UserType = 0;     //0 所有用户;1 静态用户;2 用户组
             UserName = "173"; 
        }else if (i>200 && i<=300){
            UserType = 1;     //0 所有用户;1 静态用户;2 用户组
            UserName = "557"; 
        } else if(i>300 && i<=400){
            UserType = 0;     //0 所有用户;1 静态用户;2 用户组
            UserName = "173"; 
       }else{
           UserType = 1;     //0 所有用户;1 静态用户;2 用户组
           UserName = "556"; 
       }
              
  
        int BandPlolicyMessageID = PolicyID;
        
        
        POLICY_BIND pn1 = (POLICY_BIND)svr.create_packet(PT.POLICY_BIND)
                .build("ServerNameNum", ServerNameNum)
                .build("Bind_Action", Bind_Action).build("UserType", UserType)
                .build("UserName", UserName);
            
        
    HashMap<String, Object> band = new HashMap<String, Object>();
        band.put("BandPolicyMessageType", 7);
        band.put("BandPlolicyMessageID", BandPlolicyMessageID);

                
     pn1.add_band(band);
        svr.send_message(pn1.get_packet());

        CmccDpiPolicyPacket req2 = svr.recv_message();
        assertTrue(req2 != null);
       
        }
    }

    /**
     * URL
     */
 //    @Test
    public void BLOCK_POLICY_2() {
        STEP(PT.BLOCK_POLICY.toString());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);
        for(int i=2;i<3;i++){
        int PolicyID = i;
        int Action = 1;
        int Block_Type = 1;  //1 URL;2, IP;3, 域名
        
        String Block_Name1 = "172.16.5.1/index";
        String Block_Name = "http://172.16.5.2";
        BLOCK_POLICY pn = (BLOCK_POLICY) svr.create_packet(PT.BLOCK_POLICY)
                .build("PolicyID", PolicyID).build("Action", Action)
                .build("Block_Type", Block_Type);
        

        HashMap<String, Object> block = new HashMap<String, Object>();
        block.put("Block_Name", Block_Name);

        HashMap<String, Object> block1 = new HashMap<String, Object>();
        block1.put("Block_Name", "http://172.16.5.3");

        HashMap<String, Object> block2 = new HashMap<String, Object>();
        block2.put("Block_Name", "http://172.16.5.4");

        HashMap<String, Object> block3 = new HashMap<String, Object>();
        block3.put("Block_Name", "http://172.16.5.5");

        HashMap<String, Object> block4 = new HashMap<String, Object>();
        block4.put("Block_Name", Block_Name1);

        pn.add_block(block).add_block(block1).add_block(block2)
                .add_block(block3).add_block(block4);

        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        
 STEP(PT.POLICY_BIND.toString());
       
        
        int ServerNameNum = 1;
        int Bind_Action = 1;
       
       
        int    UserType = 1;     //0 所有用户;1 静态用户;2 用户组
          String   UserName = "557"; 
              
  
        int BandPlolicyMessageID = PolicyID;
        
        
        POLICY_BIND pn1 = (POLICY_BIND)svr.create_packet(PT.POLICY_BIND)
                .build("ServerNameNum", ServerNameNum)
                .build("Bind_Action", Bind_Action).build("UserType", UserType)
                .build("UserName", UserName);
            
        
    HashMap<String, Object> band = new HashMap<String, Object>();
        band.put("BandPolicyMessageType", 7);
        band.put("BandPlolicyMessageID", BandPlolicyMessageID);

                
     pn1.add_band(band);
        svr.send_message(pn1.get_packet());

        CmccDpiPolicyPacket req2 = svr.recv_message();
        assertTrue(req2 != null);
        }

    }

    /**
     * IP
     */
    @Test
    public void BLOCK_POLICY() {
        STEP(PT.BLOCK_POLICY.toString());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);
        for(int i=3;i<4;i++){
        int PolicyID = i;
        int Action = 1;
        int Block_Type = 2;
        // int Block_Num=1;
        String Block_Name1 = "172.16.5.1";
        String Block_Name = "172.16.5.2";
        BLOCK_POLICY pn = (BLOCK_POLICY) svr.create_packet(PT.BLOCK_POLICY)
                .build("PolicyID", PolicyID).build("Action", Action)
                .build("Block_Type", Block_Type);
        // .build("Block_Num", Block_Num);

        HashMap<String, Object> block = new HashMap<String, Object>();
        block.put("Block_Name", Block_Name);

        HashMap<String, Object> block1 = new HashMap<String, Object>();
        block1.put("Block_Name", "172.16.5.3");

        HashMap<String, Object> block2 = new HashMap<String, Object>();
        block2.put("Block_Name", "172.16.5.4");

        HashMap<String, Object> block3 = new HashMap<String, Object>();
        block3.put("Block_Name", "172.16.5.5");

        HashMap<String, Object> block4 = new HashMap<String, Object>();
        block4.put("Block_Name", Block_Name1);

        pn.add_block(block).add_block(block1).add_block(block2)
                .add_block(block3).add_block(block4);

        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
     
       STEP(PT.POLICY_BIND.toString());
          
        
        int ServerNameNum = 1;
        int Bind_Action = 1;
       
       
        int    UserType = 1;     //0 所有用户;1 静态用户;2 用户组
          String   UserName = "558"; 
              
  
        int BandPlolicyMessageID = PolicyID;
        
        
        POLICY_BIND pn1 = (POLICY_BIND)svr.create_packet(PT.POLICY_BIND)
                .build("ServerNameNum", ServerNameNum)
                .build("Bind_Action", Bind_Action).build("UserType", UserType)
                .build("UserName", UserName);
            
        
    HashMap<String, Object> band = new HashMap<String, Object>();
        band.put("BandPolicyMessageType", 7);
        band.put("BandPlolicyMessageID", BandPlolicyMessageID);

                
     pn1.add_band(band);
        svr.send_message(pn1.get_packet());

        CmccDpiPolicyPacket req2 = svr.recv_message();
        assertTrue(req2 != null);
        
    }
    }
}
