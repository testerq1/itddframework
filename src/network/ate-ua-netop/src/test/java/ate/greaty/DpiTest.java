package ate.greaty;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.Global;
import ate.greaty.xge.*;
import ate.testcase.CsUATestcase;
import static org.joou.Unsigned.*;

public class DpiTest extends CsUATestcase {
    CmccDpiPolicyUA svr;
    CmccDpiPolicyUA ua;

    /**
     * 通用ACK 0x0d
     */
    @Test
    public void ACK() {
        STEP(PT.ACK.toString());
        int MessageType1 = 127;
        int MessageID1 = 1111;
        int PacketErrorStatus = 1;
        String PacketErrorInfo = "www.jd.com";
        CmccDpiPolicyPacket pn = ua.create_packet(PT.ACK)
                .build("MessageType1", MessageType1)
                .build("MessageID1", MessageID1)
                .build("PacketErrorStatus", PacketErrorStatus)
                .build("PacketErrorInfo", PacketErrorInfo).get_packet();

        ua.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

        assertTrue(((ACK) req).MessageType1.byteValue() == (byte) MessageType1);
        assertTrue(((ACK) req).MessageID1.intValue() == (int) MessageID1);
        assertTrue(((ACK) req).PacketErrorStatus.byteValue() == (byte) PacketErrorStatus);
        assertTrue(((ACK) req).PacketErrorInfo.equals(PacketErrorInfo));

    }

    @BeforeClass
    public void BeforeClass() {
        super.BeforeClass();
        Global.verbose = true;
        svr = create_server("cdp://127.0.0.1:55555/tcp", new CmccDpiPolicyUA());
        ua = create_client("cdp://127.0.0.1:55555/tcp", new CmccDpiPolicyUA());
        this.start_all_ua();
        assertTrue(svr.is_ready());
        assertTrue(ua.is_ready());
    }

    /**
     * 封堵URL/IP/域名同步策略 0x07
     */
    @Test
    public void BLOCK_POLICY() {
        STEP(PT.BLOCK_POLICY.toString());
        int PolicyID = 111;
        int Action = 1;
        int Block_Type = 3;
        int Block_Num = 2;

        HashMap block1 = new HashMap() {
            {
                put("Block_Name", "www.jd.com");
            }
        };

        CmccDpiPolicyPacket pn = ua.create_packet(PT.BLOCK_POLICY)
                .build("PolicyID", PolicyID).build("Action", Action)
                .build("Block_Type", Block_Type);

        ((BLOCK_POLICY) pn).add_block(block1).get_packet();

        pn.build("Block_Num", Block_Num);
        ua.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

        System.out.println("((BLOCK_POLICY)req).PolicyID.intValue()=="
                + ((BLOCK_POLICY) req).PolicyID.intValue());
        System.out.println("(int)Action==" + (int) Action);

        assertTrue(((BLOCK_POLICY) req).PolicyID.intValue() == (int) PolicyID);
        assertTrue(((BLOCK_POLICY) req).Action.byteValue() == (byte) Action);
        assertTrue(((BLOCK_POLICY) req).Block_Type.byteValue() == (byte) Block_Type);
        assertTrue(((BLOCK_POLICY) req).Block_Num.byteValue() == (byte) Block_Num);
        assertTrue(((BLOCK_POLICY) req).has_block(block1));
    }

    /**
     * DPI设备状态查询 0x0e
     */
    @Test
    public void Dev_Status() {
        STEP(PT.Dev_Status.toString());
        int InfoReportFreq = 1;
        CmccDpiPolicyPacket pn = ua.create_packet(PT.Dev_Status)
                .build("InfoReportFreq", InfoReportFreq).get_packet();

        ua.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        assertTrue(((Dev_Status) req).InfoReportFreq.byteValue() == (byte) InfoReportFreq);

    }

    /**
     * 设备状态查询响应 0x0f
     */
    @Test
    public void Dev_Status_ACK() {
        STEP(PT.Dev_Status_ACK.toString());
        long SoftwareVersion = 4541;
        String DeploySiteName = "rrrrr";

        CmccDpiPolicyPacket pn = ua.create_packet(PT.Dev_Status_ACK)
                .build("SoftwareVersion", SoftwareVersion)
                .build("DeploySiteName", DeploySiteName);
        
        HashMap pt1=new HashMap() {
            {
                put("PortType", 2);
                put("PortsNum", 10);
            }
        };
        HashMap pt2=new HashMap() {
            {
                put("PortType", 20);
                put("PortsNum", 4);
            }
        };
        ((Dev_Status_ACK) pn).add_port_type(pt1);
        ((Dev_Status_ACK) pn).add_port_type(pt2);
        
        HashMap pd=new HashMap() {
            int PortNo = 255;
            String PortDescription = "AAAAAAAA";
            int M_LinkID = 255;
            String M_LinkDesc = "dddd";
            {
                put("PortNo", PortNo);
                put("PortDescription", PortDescription);
                put("M_LinkID", M_LinkID);
                put("M_LinkDesc", M_LinkDesc);
            }
        };
        ((Dev_Status_ACK) pn).add_port_desc(pd);

        ua.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        System.out.println("((Dev_Status_ACK)req).SoftwareVersion=="
                + ((Dev_Status_ACK) req).SoftwareVersion);
        System.out.println("SoftwareVersion==" + SoftwareVersion);

        assertTrue(((Dev_Status_ACK) req).SoftwareVersion.longValue() == SoftwareVersion);
        assertTrue(((Dev_Status_ACK) req).DeploySiteName.equals(DeploySiteName));
        assertTrue(((Dev_Status_ACK) req).PortsTypeNum.intValue() == 2);
        assertTrue(((Dev_Status_ACK) req).porttype.size() == 2);
        assertTrue(((Dev_Status_ACK) req).portdesc.size() == 1);
        assertTrue(((Dev_Status_ACK) req).has_port_type(pt1));
        assertTrue(((Dev_Status_ACK) req).has_port_type(pt2));
        pt2.put("PortType", 123);
        assertFalse(((Dev_Status_ACK) req).has_port_type(pt2));
        assertTrue(((Dev_Status_ACK) req).has_port_desc(pd));
        pd.put("PortNo", 2);
        assertFalse(((Dev_Status_ACK) req).has_port_desc(pd));
        
    }

    /**
     * 流量管理策略 0x06
     */
    @Test
    public void FLOW_MAN() {
        STEP(PT.FLOW_MAN.toString());
        int PolicyID = 12;
        int ProtocolType = 13;
        int AppType = 13;
        int AppID = 0;
        String AppName = "酷我音乐";
        CmccDpiPolicyPacket pn = ua.create_packet(PT.FLOW_MAN)
                .build("PolicyID", PolicyID)
                .build("ProtocolType", ProtocolType).build("AppType", AppType)
                .build("AppID", AppID).build("AppName", AppName).get_packet();

        ua.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        assertTrue(((FLOW_MAN) req).PolicyID.intValue() == (int) PolicyID);
        assertTrue(((FLOW_MAN) req).ProtocolType.byteValue() == (byte) ProtocolType);
        assertTrue(((FLOW_MAN) req).AppType.byteValue() == (byte) AppType);
        assertTrue(((FLOW_MAN) req).AppID.shortValue() == (short) AppID);
        assertTrue(((FLOW_MAN) req).AppName.equals(AppName));
    }

    /**
     * 流量镜像策略 0x05
     */
    @Test
    public void FLOW_MIRROR() {
        STEP(PT.FLOW_MIRROR.toString());
        int PolicyID = 123;

        CmccDpiPolicyPacket pn = ua.create_packet(PT.FLOW_MIRROR)
                .build("PolicyID", PolicyID);
        
        HashMap md=new HashMap(){{
            put("MDstPort",12345);
        }};
        HashMap ms=new HashMap(){{
            put("MSrcPort",23456);
        }};
        Global.verbose=true;
        
        ((FLOW_MIRROR) pn).add_mdst(md);
        ((FLOW_MIRROR) pn).add_msrc(ms);
        ua.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

        System.out.println("((FLOW_MIRROR)req).PolicyID.intValue()=="
                + ((FLOW_MIRROR) req).PolicyID.intValue());
        System.out.println("PolicyID==" + PolicyID);

        assertTrue(((FLOW_MIRROR) req).PolicyID.intValue() == PolicyID);

    }

    /**
     * 心跳消息 0x00
     */
    @Test
    public void HEARTBEAT() {
        STEP(PT.HEARTBEAT.toString());
        String Dev_Name = "小鸡炖蘑菇";
        CmccDpiPolicyPacket pn = ua.create_packet(PT.HEARTBEAT)
                .build("Dev_Name", Dev_Name).get_packet();

        ua.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        assertTrue(((HEARTBEAT) req).version.equals(ubyte(1)));
        assertTrue(((HEARTBEAT) req).Dev_Name.equals(Dev_Name));

    }

    /**
     * 心跳消息响应 0x01
     */
    @Test
    public void HEARTBEAT_ACK() {
        STEP(PT.HEARTBEAT_ACK.toString());
        int Result = 255;
        CmccDpiPolicyPacket pn = ua.create_packet(PT.HEARTBEAT_ACK)
                .build("Result", Result).get_packet();

        ua.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        assertTrue(((HEARTBEAT_ACK) req).Result.byteValue() == (byte) Result);

    }

    /**
     * 策略与对象绑定 0x0a
     */
    @Test
    public void POLICY_BIND() {
        STEP(PT.POLICY_BIND.toString());
        int ServerNameNum = 1;
        int Bind_Action = 1;
        int UserType = 1;
        String UserName = "root";
        int BandPlolicyCount = 123;
        int BandPolicyMessageType = 255;
        int BandPlolicyMessageID = 123;
        CmccDpiPolicyPacket pn = ua.create_packet(PT.POLICY_BIND)
                .build("ServerNameNum", ServerNameNum)
                .build("Bind_Action", Bind_Action).build("UserType", UserType)
                .build("UserName", UserName)
                .build("BandPlolicyCount", BandPlolicyCount)
                .build("BandPolicyMessageType", BandPolicyMessageType)
                .build("BandPlolicyMessageID", BandPlolicyMessageID)
                .get_packet();

        ua.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        assertTrue(((POLICY_BIND) req).ServerNameNum.byteValue() == (byte) ServerNameNum);
        assertTrue(((POLICY_BIND) req).Bind_Action.byteValue() == (byte) Bind_Action);
        assertTrue(((POLICY_BIND) req).UserType.byteValue() == (byte) UserType);
        assertTrue(((POLICY_BIND) req).UserName.equals(UserName));
        assertTrue(((POLICY_BIND) req).BandPlolicyCount.byteValue() == (byte) BandPlolicyCount);
        // assertTrue(((POLICY_BIND)req).BandPolicyMessageType.byteValue()==(byte)BandPolicyMessageType);
        // assertTrue(((POLICY_BIND)req).BandPlolicyMessageID.intValue()==(int)BandPlolicyMessageID);
    }

    /**
     * 策略同步消息 0x0b
     */
    @Test
    public void POLICY_SYNC() {
        STEP(PT.POLICY_SYNC.toString());
        int MessageTypeNum = 1;
        int MessageType1 = 12;
        int MessageSerialNo = 1112;
        CmccDpiPolicyPacket pn = ua.create_packet(PT.POLICY_SYNC)
                .build("MessageTypeNum", MessageTypeNum)
                .build("MessageType1", MessageType1)
                .build("MessageSerialNo", MessageSerialNo).get_packet();

        ua.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        assertTrue(((POLICY_SYNC) req).MessageTypeNum.byteValue() == (byte) MessageTypeNum);
        assertTrue(((POLICY_SYNC) req).MessageType1.byteValue() == (byte) MessageType1);
        assertTrue(((POLICY_SYNC) req).MessageSerialNo.intValue() == MessageSerialNo);

    }

    /**
     * 策略同步请求回应 0x0c
     */
    @Test
    public void POLICY_SYNC_ACK() {
        STEP(PT.POLICY_SYNC_ACK.toString());
        int MessageType2 = 1;
        int MessageSerialNo = 123456;
        // int MessageNum=111;

        String MessageContents = "01 03 35 35 36 00 03 04 01 10 05 00 18 04 02 10 05 00 18 04 06 10 05 00 18";
        POLICY_SYNC_ACK pn = (POLICY_SYNC_ACK) ua
                .create_packet(PT.POLICY_SYNC_ACK)
                .build("MessageType2", MessageType2)
                .build("MessageSerialNo", MessageSerialNo)
        // .build("MessageNum",MessageNum )
        ;
        pn.add_msgcontent(MessageContents);

        ua.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        assertTrue(((POLICY_SYNC_ACK) req).MessageType2.byteValue() == (byte) MessageType2);

        assertTrue(((POLICY_SYNC_ACK) req).MessageSerialNo.intValue() == MessageSerialNo);

        // System.out.println("((POLICY_SYNC_ACK)req).MessageNum.shortValue()=="+((POLICY_SYNC_ACK)req).MessageNum.shortValue());
        // System.out.println("(short)MessageNum=="+req.MessageNum);

        assertTrue(((POLICY_SYNC_ACK) req).MessageNum.shortValue() == 1);

        assertTrue(MessageContents.replace(" ", "").equals(((POLICY_SYNC_ACK) req).mes.get(0)
                .toString()), ((POLICY_SYNC_ACK) req).mes.get(0).len());

    }

    /**
     * 流量分析结果上报策略 0x09
     */
    @Test
    public void REPORTER_POLICY() {
        STEP(PT.REPORTER_POLICY.toString());
        int PolicyID = 222;
        int PacketType = 1;
        int PacketSubtype = 111;
        int R_StartTime = 111111;
        int R_EndTime = 21212;
        int R_Freq = 1;
        String R_Temp_Name = "ddddd";
        int R_Interface = 1;
        String R_DestIP = "5.5.5.5";
        String UserName = "fdfdfdfd";
        String Password = "password";
        String SDTP_IP = "2.2.2.2";
        CmccDpiPolicyPacket pn = ua.create_packet(PT.REPORTER_POLICY)
                .build("PolicyID", PolicyID).build("PacketType", PacketType)
                .build("PacketSubtype", PacketSubtype)
                .build("R_StartTime", R_StartTime)
                .build("R_EndTime", R_EndTime).build("R_Freq", R_Freq)
                .build("R_Temp_Name", R_Temp_Name)
                .build("R_Interface", R_Interface).build("R_DestIP", R_DestIP)
                .build("UserName", UserName).build("Password", Password)
                .build("SDTP_IP", SDTP_IP).get_packet();

        ua.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        assertTrue(((REPORTER_POLICY) req).PolicyID.intValue() == (int) PolicyID);
        assertTrue(((REPORTER_POLICY) req).PacketType.byteValue() == (byte) PacketType);
        assertTrue(((REPORTER_POLICY) req).PacketSubtype.byteValue() == (byte) PacketSubtype);
        assertTrue(((REPORTER_POLICY) req).R_StartTime.intValue() == (int) R_StartTime);
        assertTrue(((REPORTER_POLICY) req).R_EndTime.intValue() == (int) R_EndTime);
        assertTrue(((REPORTER_POLICY) req).R_Freq.byteValue() == (byte) R_Freq);
        assertTrue(((REPORTER_POLICY) req).R_Temp_Name.equals(R_Temp_Name));
        assertTrue(((REPORTER_POLICY) req).R_Interface.byteValue() == (byte) R_Interface);
        assertTrue(((REPORTER_POLICY) req).R_DestIP.equals(R_DestIP));
        assertTrue(((REPORTER_POLICY) req).UserName.equals(UserName));
        assertTrue(((REPORTER_POLICY) req).Password.equals(Password));

        System.out.println("((REPORTER_POLICY)req).SDTP_IP=="
                + ((REPORTER_POLICY) req).SDTP_IP);
        System.out.println("SDTP_IP==" + SDTP_IP);

        assertTrue(((REPORTER_POLICY) req).SDTP_IP.equals(SDTP_IP));
    }

    /**
     * 资源特征库下发策略 0x04
     */
    @Test
    public void RES_SPEC_POLICY() {
        STEP(PT.RES_SPEC_POLICY.toString());
        int Resoure_Type = 13;
        int Protocol_Version = 255;
        String Server_IP = "10.10.10.10";
        int Server_Port = 22;
        String UserName = "rootffff";
        String Password = "password";
        String FileName = "dsg.xml";
        CmccDpiPolicyPacket pn = ua.create_packet(PT.RES_SPEC_POLICY)
                .build("Resoure_Type", Resoure_Type)
                .build("Protocol_Version", Protocol_Version)
                .build("Server_IP", Server_IP)
                .build("Server_Port", Server_Port).build("UserName", UserName)
                .build("Password", Password).build("FileName", FileName)
                .get_packet();

        ua.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

        assertTrue(((RES_SPEC_POLICY) req).Resoure_Type.byteValue() == (byte) Resoure_Type);

        assertTrue(((RES_SPEC_POLICY) req).Protocol_Version.byteValue() == (byte) Protocol_Version);
        assertTrue(((RES_SPEC_POLICY) req).Server_IP.equals(Server_IP));
        assertTrue(((RES_SPEC_POLICY) req).Server_Port.shortValue() == (short) Server_Port);

        assertTrue(((RES_SPEC_POLICY) req).UserName.equals(UserName));
        assertTrue(((RES_SPEC_POLICY) req).Password.equals(Password));
        assertTrue(((RES_SPEC_POLICY) req).FileName.equals(FileName));
    }

    /**
     * 网站访问分析策略 0x08
     */
    @Test
    public void SITE_ACCESS() {
        STEP(PT.SITE_ACCESS.toString());
        int PolicyID = 22;
        int Action = 1;
        int SRC_AREAGROUP_ID = 8;
        int SRC_Area_Num = 9;
        String SRC_Area_Name = "SSSS1111";
        int DEST_AREAGROUP_ID = 54;
        int DEST_Area_Num = 255;
        String DEST_Area_Name = "hjhk454";
        CmccDpiPolicyPacket pn = ua.create_packet(PT.SITE_ACCESS)
                .build("PolicyID", PolicyID).build("Action", Action)
                .build("SRC_AREAGROUP_ID", SRC_AREAGROUP_ID)
                .build("SRC_Area_Num", SRC_Area_Num)
                .build("SRC_Area_Name", SRC_Area_Name)
                .build("DEST_AREAGROUP_ID", DEST_AREAGROUP_ID)
                .build("DEST_Area_Num", DEST_Area_Num)
                .build("DEST_Area_Name", DEST_Area_Name).get_packet();

        ua.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        assertTrue(((SITE_ACCESS) req).PolicyID.intValue() == PolicyID);
        assertTrue(((SITE_ACCESS) req).Action.byteValue() == (byte) Action);

        System.out.println("((SITE_ACCESS)req).SRC_AREAGROUP_ID.byteValue()=="
                + ((SITE_ACCESS) req).SRC_AREAGROUP_ID.byteValue());
        System.out
                .println("(byte)SRC_AREAGROUP_ID==" + (byte) SRC_AREAGROUP_ID);

        assertTrue(((SITE_ACCESS) req).SRC_AREAGROUP_ID.byteValue() == (byte) SRC_AREAGROUP_ID);
        assertTrue(((SITE_ACCESS) req).SRC_Area_Num.byteValue() == (byte) SRC_Area_Num);
        // assertTrue(((SITE_ACCESS)req).SRC_Area_Name.equals(SRC_Area_Name));
        assertTrue(((SITE_ACCESS) req).DEST_AREAGROUP_ID.byteValue() == (byte) DEST_AREAGROUP_ID);
        assertTrue(((SITE_ACCESS) req).DEST_Area_Num.byteValue() == (byte) DEST_Area_Num);
        // assertTrue(((SITE_ACCESS)req).DEST_Area_Name.equals(DEST_Area_Name));
    }

    /**
     * 静态IP用户同步消息 0x02
     */
    @Test
    public void STATIC_IP_SYNC() {
        STEP(PT.STATIC_IP_SYNC.getValue());
        int Bind_Action = 1;
        String UserName = "123456";
        int IP_SegmentNum = 123;
        HashMap<String, Object> usreip = new HashMap<String, Object>();
        usreip.put("UserIP0", "172.16.5.6");
        usreip.put("UserIP_Prefix0", 10);
        usreip.put("UserIP1", "172.16.5.7");
        usreip.put("UserIP_Prefix1", 4);

        CmccDpiPolicyPacket pn = ua.create_packet(PT.STATIC_IP_SYNC)
                .build("Bind_Action", Bind_Action).build("UserName", UserName)
                .build("IP_SegmentNum", IP_SegmentNum).build("userip", usreip)

                .get_packet();

        ua.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
    }

    /**
     * 用户组归属分配消息 0x03
     */
    @Test
    public void UG_HOME() {
        STEP(PT.UG_HOME.getValue());

        int UserGroupNum = 1;
        String UserGroupName = "天山雪莲";
        String UserGroupName1 = "天山雪莲_1";
        int Action = 1;
        // int UserNum=562;
        int UserType = 2;
        String UserName = "TYS1";
        int UserType1 = 2;
        String UserName1 = "TYS1";
        int UserType2 = 2;
        String UserName2 = "TYS1";
        UG_HOME pn = (UG_HOME) svr.create_packet(PT.UG_HOME).build(
                "UserGroupNum", UserGroupNum);
        HashMap<String, Object> ug1 = new HashMap<String, Object>();
        ug1.put("UserGroupName", UserGroupName);
        ug1.put("Action", Action);

        HashMap<String, Object> ug1_usr1 = new HashMap<String, Object>();
        ug1_usr1.put("UserType", UserType);
        ug1_usr1.put("UserName", UserName);

        HashMap<String, Object> ug1_usr2 = new HashMap<String, Object>();
        ug1_usr1.put("UserType", UserType1);
        ug1_usr1.put("UserName", UserName1);

        pn.add_usergroup(ug1).add_user(ug1_usr1).add_user(ug1_usr2);

        HashMap<String, Object> ug2 = new HashMap<String, Object>();
        ug2.put("UserGroupName", UserGroupName1);
        ug2.put("Action", Action);

        HashMap<String, Object> ug2_usr1 = new HashMap<String, Object>();
        ug2_usr1.put("UserType", UserType2);
        ug2_usr1.put("UserName", UserName2);

        pn.add_usergroup(ug2).add_user(ug2_usr1);

        svr.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

    }

}
