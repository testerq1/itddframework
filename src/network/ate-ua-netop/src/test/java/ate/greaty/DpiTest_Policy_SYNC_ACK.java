package ate.greaty;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.Global;
import ate.greaty.xge.CmccDpiPolicyPacket;
import ate.greaty.xge.CmccDpiPolicyUA;
import ate.greaty.xge.HEARTBEAT;
import ate.greaty.xge.HEARTBEAT_ACK;
import ate.greaty.xge.POLICY_SYNC_ACK;
import ate.greaty.xge.PT;
import ate.testcase.CsUATestcase;

public class DpiTest_Policy_SYNC_ACK extends CsUATestcase {
    CmccDpiPolicyUA svr;

    @BeforeClass
    public void BeforeClass() {
        super.BeforeClass();
        Global.verbose = true;
        svr = create_server("cdp://" + siip + ":5555/tcp",
                new CmccDpiPolicyUA());
        this.start_all_ua();
        assertTrue(svr.is_ready());
        // this.shutdown_all_ua();
    }

    /**
     * 心跳消息响应 0x01
     */
    // @Test
    public void HEARTBEAT_ACK() {
        STEP(PT.HEARTBEAT_ACK.toString());
        int Result = 1;

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        // svr.register_auto_response("dft", svr);

        assertTrue(req instanceof HEARTBEAT);

        HEARTBEAT hb = (HEARTBEAT) req;

        System.out.println("hb==" + hb);
        assertTrue(hb.Dev_Name != null && hb.Dev_Name.length() != 0);

        System.out.println("hb.version==" + hb.version);

        assertTrue(hb.Dev_Name_Length.intValue() == hb.Dev_Name.length());
        assertTrue(expect.ucompare(hb.version, 1) == 0);

        // System.out.println("hb.MessageLength=="+hb.MessageLength);
        // System.out.println("uint(15+hb.Dev_Name_Length.intValue()=="+uint(15+hb.Dev_Name_Length.intValue()));

        assertTrue(expect.ucompare(hb.MessageLength,
                19 + hb.Dev_Name_Length.intValue()) == 0);
        assertTrue(hb.Timestamp.longValue() > (now() / 1000 - 10), "now:"
                + now() + " ts:" + hb.Timestamp);
        assertTrue(hb.MessageVersion.intValue() >= 0);
        assertTrue(hb.MessageID.intValue() >= 0);

        HEARTBEAT_ACK resp = (HEARTBEAT_ACK) hb.create_response()
                .build("Result", Result).get_packet();

        svr.send_message(resp);

    }

    /**
     * 策略同步请求回应 -流量管理0x0c
     */
  //  @Test
    public void POLICY_SYNC_ACK() {
        STEP(PT.POLICY_SYNC_ACK.toString());
        
        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
         svr.register_auto_response("dft", svr);
        
        int MessageType2 = 6;
        int MessageSerialNo = 7;
        //一个时间段
        String MessageContents = " 00 00 00 02 01 0c 00 01 00 00 00 03 e8 00 00 03 e8 00 00 00 14 00 00 00 14 02 03 01 53 f6 f6 28 53 f6 f8 80 ";
        //4个时间段
        String MessageContents1="00 00 00 01 ff 0c 00 03 00 00 00 03 e8 00 00 03 e8 00 00 00 14 00 00 00 14 02 02 02 53 f6 f1 78 53 f6 f2 a4 53 f6 f3 d0 53 f6 f4 fc";
        
        //
        String    MessageContents2  =" 00 00 00 03 ff 0c 00 03 00 00 0f 42 40 00 00 13 88 00 00 00 14 00 00 00 14 03 02 01 53 f6 f6 28 53 f7 14 a0";
        POLICY_SYNC_ACK pn = (POLICY_SYNC_ACK) svr
                .create_packet(PT.POLICY_SYNC_ACK)
                .build("MessageType2", MessageType2)
                .build("MessageSerialNo", MessageSerialNo);

        HashMap<String, Object> mes = new HashMap<String, Object>();
        mes.put("MessageContents", MessageContents);
        
        pn.add_msgcontent(MessageContents2);

        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);

    }

    /**
     * 策略同步请求回应 -流量镜像0x0c -PORT镜像
     */
  // @Test
    public void POLICY_SYNC_ACK_1() {
        STEP(PT.POLICY_SYNC_ACK.toString());
        
        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
         svr.register_auto_response("dft", svr);
             
        
        
         int MessageType2 = 5;
         int MessageSerialNo = 2;
         //流量镜像-port镜像
         String MessageContents = "00 00 00 04 ff ff 00 ff 00 01 00 50 02 00 51 00 52 00 00 00 53 f5 5c 00 53 f7 14 a0 07 01 01 01";
         
         //流量镜像-port镜像-stoptime==0
         String MessageContents1 ="00 00 00 01 ff 0c 00 03 00 01 00 50 01 00 87 00 00 00 53 f5 5c 00 54 05 a3 30 07 01 01 01";
        
         //流量镜像-特征字镜像
         String MessageContents2 ="00 00 00 03 ff ff 00 ff 00 01 00 eb 01 00 87 00 0f 05 66 74 65 77 66 53 f5 5c 00 53 f7 14 a0 07 01 01 01";
         
         //流量镜像-服务
         String MessageContents3=" 00 00 00 02 01 0c 00 03 00 01 00 eb 01 00 87 00 00 00 53 f5 5c 00 53 f7 14 a0 07 01 01 01";
         
         //流量镜像-目的IP地址段不为0
         String MessageContents4=" 00 00 00 02 01 0c 00 03 02 04 0a 10 05 00 18 04 0b 10 05 00 17 01 00 eb 01 00 87 00 00 00 53 f5 5c 00 53 f7 14 a0 07 01 01 01";
         
         POLICY_SYNC_ACK pn = (POLICY_SYNC_ACK) svr
                 .create_packet(PT.POLICY_SYNC_ACK)
                 .build("MessageType2", MessageType2)
                 .build("MessageSerialNo", MessageSerialNo);

         
         pn.add_msgcontent(MessageContents1)
           ;

         svr.send_message(pn.get_packet());

         CmccDpiPolicyPacket req1 = svr.recv_message();
         assertTrue(req1 != null);

    }
/**
 * 策略同步请求回应 -静态用户0x0c 
 */
 //   @Test
    public void POLICY_SYNC_ACK_12() {
        STEP(PT.POLICY_SYNC_ACK.toString());
        
        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
         svr.register_auto_response("dft", svr);         
       
       
        int MessageType2 = 2;
        int MessageSerialNo = 2;
        String MessageContents ="01 03 35 35 36 00 03 04 01 10 05 00 18 04 02 10 05 00 18 04 31 10 05 00 10";
        String MessageContents1 ="01 03 35 35 37 00 03 04 17 10 05 00 18 04 18 10 05 00 18 04 31 10 05 00 10";
        POLICY_SYNC_ACK pn = (POLICY_SYNC_ACK) svr
                .create_packet(PT.POLICY_SYNC_ACK)
                .build("MessageType2", MessageType2)
                .build("MessageSerialNo", MessageSerialNo);

        HashMap<String, Object> mes = new HashMap<String, Object>();
        mes.put("MessageContents", MessageContents);
        
        HashMap<String, Object> mes1 = new HashMap<String, Object>();
        mes1.put("MessageContents", MessageContents1);
        
        pn.add_msgcontent(MessageContents).add_msgcontent(MessageContents1);

        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        
     

    }


    /**
     * 策略同步请求回应 -用户组0x0c 
     */
   //     @Test
        public void POLICY_SYNC_ACK_13() {
            STEP(PT.POLICY_SYNC_ACK.toString());
            
            CmccDpiPolicyPacket req = svr.recv_message();
            assertTrue(req != null);
             svr.register_auto_response("dft", svr);
            
            int MessageType2 = 3;
            int MessageSerialNo = 2;
                    
            String MessageContents = "00 01 03 74 74 74 01 00 00 00 02 02 03 35 35 37 02 03 35 35 36";
            String MessageContents1 ="";
            POLICY_SYNC_ACK pn = (POLICY_SYNC_ACK) svr
                    .create_packet(PT.POLICY_SYNC_ACK)
                    .build("MessageType2", MessageType2)
                    .build("MessageSerialNo", MessageSerialNo);

            HashMap<String, Object> mes = new HashMap<String, Object>();
            mes.put("MessageContents", MessageContents);
            
            HashMap<String, Object> mes1 = new HashMap<String, Object>();
            mes1.put("MessageContents", MessageContents1);

            pn.add_msgcontent(MessageContents);
            svr.send_message(pn.get_packet());

            CmccDpiPolicyPacket req1 = svr.recv_message();
            assertTrue(req1 != null);

        }


        /**
         * 策略同步请求回应 -网站访问分析
         */
     //   @Test
        public void POLICY_SYNC_ACK_14() {
            STEP(PT.POLICY_SYNC_ACK.toString());
            
            CmccDpiPolicyPacket req = svr.recv_message();
            assertTrue(req != null);
             svr.register_auto_response("dft", svr);
            
            int MessageType2 = 8;
            int MessageSerialNo = 2;
            String MessageContents = "00 00 00 05 01 01 00 02 00";
            String MessageContents2 = "00 00 00 06 01 01 00 03 00";
            String MessageContents1 = "00 00 00 07 01 04 02 02 61 31 02 61 32 05 02 02 61 32 02 61 33";
            POLICY_SYNC_ACK pn = (POLICY_SYNC_ACK) svr
                    .create_packet(PT.POLICY_SYNC_ACK)
                    .build("MessageType2", MessageType2)
                    .build("MessageSerialNo", MessageSerialNo);

           
            
            pn.add_msgcontent(MessageContents).add_msgcontent(MessageContents1).add_msgcontent(MessageContents2);

            svr.send_message(pn.get_packet());

            CmccDpiPolicyPacket req1 = svr.recv_message();
            assertTrue(req1 != null);

        }

        /**
         * 策略同步请求回应 -流量分析结果上报
         */
        @Test
        public void POLICY_SYNC_ACK_15() {
            STEP(PT.POLICY_SYNC_ACK.toString());
            
            CmccDpiPolicyPacket req = svr.recv_message();
            assertTrue(req != null);
             svr.register_auto_response("dft", svr);
            
            int MessageType2 = 9;
            int MessageSerialNo = 2;
            String MessageContents = "00 00 00 05 01 c9 53 f9 b9 f8 00 00 00 00 02 10 58 44 52 46 69 65 6c 64 73 5f 30 31 2e 63 73 76 01 04 ac 10 05 01 74 61 72 67 65 74 00 00 74 61 72 67 65 74 65 74 04 02 02 02 02";
            String MessageContents1 = "00 00 00 06 01 cb 53 f9 b9 f8 53 fb 5d 80 01 10 58 44 52 46 69 65 6c 64 73 5f 30 32 2e 63 73 76 02 04 ac 10 05 02 74 61 72 67 65 74 00 00 74 61 72 67 65 74 65 74 04 03 02 02 02";
            String MessageContents2 ="";
            String MessageContents3 ="";
            String MessageContents4 ="";
            POLICY_SYNC_ACK pn = (POLICY_SYNC_ACK) svr
                    .create_packet(PT.POLICY_SYNC_ACK)
                    .build("MessageType2", MessageType2)
                    .build("MessageSerialNo", MessageSerialNo);

           
            
            pn.add_msgcontent(MessageContents).add_msgcontent(MessageContents1);

            svr.send_message(pn.get_packet());

            CmccDpiPolicyPacket req1 = svr.recv_message();
            assertTrue(req1 != null);

        }

        /**
         * 策略同步请求回应 -封堵IP上报
         */
     //   @Test
        public void POLICY_SYNC_ACK_16() {
            STEP(PT.POLICY_SYNC_ACK.toString());
            
            CmccDpiPolicyPacket req = svr.recv_message();
            assertTrue(req != null);
             svr.register_auto_response("dft", svr);
            
            int MessageType2 = 7;
            int MessageSerialNo = 3;
            //IP
            String MessageContents = "00 00 00 07 01 02 05 00 04 ac 10 05 01 00 04 ac 10 05 03 00 04 ac 10 05 04 00 04 ac 10 05 05 00 04 ac 10 05 06";
            //URL
            String MessageContents1 = "00 00 00 08 01 01 05 00 10 31 37 32 2e 31 36 2e 35 2e 31 2f 69 6e 64 65 78 00 12 68 74 74 70 73 3a 2f 2f 31 37 32 2e 31 36 2e 35 2e 33 00 14 68 74 74 70 3a 2f 2f 77 77 77 2e 62 61 69 64 75 2e 63 6f 6d 00 11 68 74 74 70 3a 2f 2f 77 77 77 2e 6a 64 2e 63 6f 6d 00 15 68 74 74 70 3a 2f 2f 77 77 77 2e 73 75 6e 69 6e 67 2e 63 6f 6d";
            //域名
            String MessageContents2 = "00 00 00 09 01 03 05 00 0a 77 77 77 2e 6a 64 2e 63 6f 6d 00 0f 77 77 77 2e 73 69 6e 61 2e 63 6f 6d 2e 63 6e 00 0e 77 77 77 2e 73 75 6e 69 6e 67 2e 63 6f 6d 00 0d 77 77 77 2e 64 75 6f 74 65 2e 63 6f 6d 00 0b 77 77 77 2e 70 6f 63 6f 2e 63 6e";
            POLICY_SYNC_ACK pn = (POLICY_SYNC_ACK) svr
                    .create_packet(PT.POLICY_SYNC_ACK)
                    .build("MessageType2", MessageType2)
                    .build("MessageSerialNo", MessageSerialNo);

           
            
            pn.add_msgcontent(MessageContents).add_msgcontent(MessageContents1).add_msgcontent(MessageContents2);

            svr.send_message(pn.get_packet());

            CmccDpiPolicyPacket req1 = svr.recv_message();
            assertTrue(req1 != null);

        }


































}
