package ate.greaty;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.Global;
import ate.greaty.xge.CmccDpiPolicyPacket;
import ate.greaty.xge.CmccDpiPolicyUA;
import ate.greaty.xge.FLOW_MAN;
import ate.greaty.xge.POLICY_BIND;
import ate.greaty.xge.PT;
import ate.greaty.xge.STATIC_IP_SYNC;
import ate.testcase.CsUATestcase;

public class Dpi_Flow_MAN extends CsUATestcase {
    CmccDpiPolicyUA svr;
    DpiTest_Client dpiclient =new DpiTest_Client();

    @BeforeClass
    public void BeforeClass() {
        super.BeforeClass();
        Global.verbose = true;
        svr = create_server("cdp://" + siip + ":5555/tcp",
                new CmccDpiPolicyUA());
        this.start_all_ua();
        assertTrue(svr.is_ready());
        // this.shutdown_all_ua();
    }

    /**
     * 静态IP用户同步消息 0x02
     */

  //  @Test
    public void STATIC_IP_SYNC() {
        STEP(PT.STATIC_IP_SYNC.getValue());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);

        int Bind_Action = 1;
        String UserName = "173";
        // byte UserName=5;
       // int IP_SegmentNum = 123;
        HashMap<String, Object> usreip = new HashMap<String, Object>();
        usreip.put("UserIP0", "172.16.5.0");
        usreip.put("UserIP_Prefix0", 24);
        usreip.put("UserIP1", "192.168.0.0");
        usreip.put("UserIP_Prefix1", 16);

        CmccDpiPolicyPacket pn = svr.create_packet(PT.STATIC_IP_SYNC)
                .build("Bind_Action", Bind_Action)
                .build("UserName", UserName)
             //   .build("IP_SegmentNum", IP_SegmentNum)
                .build("userip", usreip)

                .get_packet();

        svr.send_message(pn);

        STATIC_IP_SYNC hb = (STATIC_IP_SYNC) pn;
        System.out.println("hb==" + hb.usreip);
        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        sleep(10000);

    }

    /**
     * 流量镜像策略 0x06
     */
     @Test
    public void Flow_MAN() {
         STEP(PT.FLOW_MAN.toString());
         CmccDpiPolicyPacket req1 = svr.recv_message();
         assertTrue(req1 != null);
         svr.register_auto_response("dft", svr);
         for(int i=1;i<5;i++){
             
         int PolicyID = i;
         int ProtocolType = 255;
         int AppType = 255;
         int AppID = 255;
         int AppTraffic_Up_ABS = 1000;
         int AppTraffic_Dn_ABS = 1000;
         int AppSession_Up_ABS = 20;
         int AppSession_Dn_ABS = 20;
         int QoS_Label_Up = 4;
         int QoS_Label_Dn = 2;

         // 时间段1
         String strDate = "2014-8-25 15:15:00";
         String strDate1 = "2014-9-11 20:20:00";
         // 时间段2
         String strDate2 =" 2014-8-25 15:25:00";
         String strDate3 = "2014-8-25 15:30:00";
         // 时间段3
         String strDate4 = "2014-8-25 15:35:00";
         String strDate5 = "2014-8-25 15:40:00";
         // 时间段4
         String strDate6 = "2014-8-25 15:45:00";
         String strDate7 = "2014-8-25 15:50:00";

         FLOW_MAN pn = (FLOW_MAN) svr.create_packet(PT.FLOW_MAN)
                 .build("PolicyID", PolicyID)
                 .build("ProtocolType", ProtocolType).build("AppType", AppType)
                 .build("AppID", AppID)
                 .build("AppTraffic_Up_ABS", AppTraffic_Up_ABS)
                 .build("AppTraffic_Dn_ABS", AppTraffic_Dn_ABS)
                 .build("AppSession_Up_ABS", AppSession_Up_ABS)
                 .build("AppSession_Dn_ABS", AppSession_Dn_ABS)
                 .build("QoS_Label_Up", QoS_Label_Up)
                 .build("QoS_Label_Dn", QoS_Label_Dn);

         HashMap<String, Object> time = new HashMap<String, Object>();
         time.put("Starttime",(long)(dpiclient.get_utc_time(strDate)));
         time.put("Stoptime", (long) (dpiclient.get_utc_time(strDate1)));

         HashMap<String, Object> time1 = new HashMap<String, Object>();
         time1.put("Starttime", (long) (dpiclient.get_utc_time(strDate2)));
         time1.put("Stoptime", (long) (dpiclient.get_utc_time(strDate3)));

         HashMap<String, Object> time2 = new HashMap<String, Object>();
         time2.put("Starttime", (long) (dpiclient.get_utc_time(strDate4)));
         time2.put("Stoptime", (long) (dpiclient.get_utc_time(strDate5)));

         HashMap<String, Object> time3 = new HashMap<String, Object>();
         time3.put("Starttime", (long) (dpiclient.get_utc_time(strDate6)));
         time3.put("Stoptime", (long) (dpiclient.get_utc_time(strDate7)));

         pn.add_time(time);

         svr.send_message(pn.get_packet());

         CmccDpiPolicyPacket req = svr.recv_message();
         assertTrue(req != null);

      
        
/**
 * 对象和策略的绑定
 */
  
        STEP(PT.POLICY_BIND.toString());
       
        
        int ServerNameNum = 1;
        int Bind_Action = 1;
        int UserType;
        String UserName;
      
       if(i<=255){
            UserType = 1;     //0 所有用户;1 静态用户;2 用户组
            UserName = "DM"+i;
       }
       else if (i>255 && i<=510)
       {   
            UserType = 2;     //0 所有用户;1 静态用户;2 用户组
            UserName = "ddd"+(i-255);
       }else{
           UserType = 0;     //0 所有用户;1 静态用户;2 用户组
           UserName = "ddd";
       }
           
                   
      
        
  
        int BandPlolicyMessageID = PolicyID;
        
        
        POLICY_BIND pn1 = (POLICY_BIND)svr.create_packet(PT.POLICY_BIND)
                .build("ServerNameNum", ServerNameNum)
                .build("Bind_Action", Bind_Action).build("UserType", UserType)
                .build("UserName", UserName);
            
        
    HashMap<String, Object> band = new HashMap<String, Object>();
        band.put("BandPolicyMessageType", 6);
        band.put("BandPlolicyMessageID", BandPlolicyMessageID);

                
     pn1.add_band(band);
        svr.send_message(pn1.get_packet());

        CmccDpiPolicyPacket req2 = svr.recv_message();
        assertTrue(req2 != null);
        
    }
     }
}
