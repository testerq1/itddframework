package ate.greaty;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.Global;
import ate.greaty.xge.CmccDpiPolicyPacket;
import ate.greaty.xge.CmccDpiPolicyUA;
import ate.greaty.xge.FLOW_MIRROR;
import ate.greaty.xge.POLICY_BIND;
import ate.greaty.xge.PT;
import ate.testcase.CsUATestcase;

public class Dpi_Flow_Mirror_Port extends CsUATestcase {
    CmccDpiPolicyUA svr;
    DpiTest_Client dpiclient =new DpiTest_Client();

    @BeforeClass
    public void BeforeClass() {
        super.BeforeClass();
        Global.verbose = true;
        svr = create_server("cdp://" + siip + ":5555/tcp",
                new CmccDpiPolicyUA());
        this.start_all_ua();
        assertTrue(svr.is_ready());
        // this.shutdown_all_ua();
    }

    /**
     * 流量镜像策略 0x05
     */
     @Test
    public void Flow_Mirror_Port() {
         STEP(PT.FLOW_MIRROR.toString());
         CmccDpiPolicyPacket req1 = svr.recv_message();
         assertTrue(req1 != null);
         svr.register_auto_response("dft", svr);
         for(int i=1;i<3;i++){
             
         int PolicyID = i;
         int MProtocalType = 255;
         int MAppType = 255;
         int MAppID = 255;
         int MSrcPort=0,MSrcPort1=0;
         int MDstPort=0,MDstPort1=0;
         int M_GroupNo = 4;
         int M_Direction = 3;
         if (i<=100){
             MSrcPort=135;
             MSrcPort1=145;
             MDstPort=235;
             MDstPort1=245;
         }
         else if (i>100 && i<=200){
             MSrcPort=165;
             MSrcPort1=166;
             MDstPort=167;
             MDstPort1=168;
             
         }else if (i>200 && i<=300){
             MSrcPort=175;
             MSrcPort1=176;
             MDstPort=177;
             MDstPort1=178;
             
         }else if (i>300 && i<=400){
             MSrcPort=185;
             MSrcPort1=186;
             MDstPort=187;
             MDstPort1=188;
             
         }else if (i>400 && i<=500){
             MSrcPort=195;
             MSrcPort1=196;
             MDstPort=197;
             MDstPort1=198;
             
         }else if (i>500 && i<=600){
             MSrcPort=205;
             MSrcPort1=206;
             MDstPort=207;
             MDstPort1=208;
             
         }else if (i>600 && i<=700){
             MSrcPort=215;
             MSrcPort1=216;
             MDstPort=217;
             MDstPort1=218;
             
         }else if (i>700 && i<=800){
             MSrcPort=225;
             MSrcPort1=226;
             MDstPort=227;
             MDstPort1=228;
             
         }else if (i>800 && i<=900){
             MSrcPort=236;
             MSrcPort1=237;
             MDstPort=238;
             MDstPort1=239;
             
         }else{
             MSrcPort=245;
             MSrcPort1=246;
             MDstPort=247;
             MDstPort1=248;
             
         }
         
         
         

        

         int MDataMatchOffset = 0;

         String MDataMatchContent = "";

         String strDate = "2014-8-26 10:40:00";
         String strDate1 = "2014-9-4 20:40:00";

         int M_StartTime = (int) dpiclient.get_utc_time(strDate);
         int M_EndTime = (int) dpiclient.get_utc_time(strDate1);
        
         
         int MFlowAdd = 1;
         int MType = 1;

         FLOW_MIRROR pn = (FLOW_MIRROR) svr
                 .create_packet(PT.FLOW_MIRROR)
                 .build("PolicyID", PolicyID)
                 .build("MProtocalType", MProtocalType)
                 .build("MAppType", MAppType)
                 .build("MAppID", MAppID)
               //  .build("mdstIp", mdstIp)                
                 .build("MDataMatchOffset", MDataMatchOffset)
                 .build("MDataMatchContent", MDataMatchContent)
                 .build("M_StartTime", M_StartTime)
                 .build("M_EndTime", M_EndTime)
                 .build("M_GroupNo", M_GroupNo)
                 .build("M_Direction", M_Direction)
                 .build("MFlowAdd", MFlowAdd)
                 .build("MType", MType);
                 

         HashMap<String, Object> ms = new HashMap<String, Object>();
         ms.put("MSrcPort", MSrcPort);
         
         HashMap<String, Object> ms1 = new HashMap<String, Object>();
         ms1.put("MSrcPort", MSrcPort1);
         
        
          
         pn.add_msrc(ms).add_msrc(ms1);
         
         HashMap<String, Object> md = new HashMap<String, Object>();
         md.put("MDstPort", MDstPort);
         
         HashMap<String, Object> md1 = new HashMap<String, Object>();
         md1.put("MDstPort", MDstPort1);
         
         
         
         pn.add_mdst(md).add_mdst(md1);
         
         svr.send_message(pn.get_packet());

         CmccDpiPolicyPacket req = svr.recv_message();
         assertTrue(req != null);

     
        
/**
 * 对象和策略的绑定
 */
  
        STEP(PT.POLICY_BIND.toString());
       
        
        int ServerNameNum = 1;
        int Bind_Action = 1;
        int UserType= 0;;
        String UserName= "ddd"+1;;
     
      
  
        int BandPlolicyMessageID = PolicyID;
        
        
        POLICY_BIND pn1 = (POLICY_BIND)svr.create_packet(PT.POLICY_BIND)
                .build("ServerNameNum", ServerNameNum)
                .build("Bind_Action", Bind_Action).build("UserType", UserType)
                .build("UserName", UserName);
             
        
    HashMap<String, Object> band = new HashMap<String, Object>();
        band.put("BandPolicyMessageType", 5);
        band.put("BandPlolicyMessageID", BandPlolicyMessageID);

                
     pn1.add_band(band);
        svr.send_message(pn1.get_packet());

        CmccDpiPolicyPacket req2 = svr.recv_message();
        assertTrue(req2 != null);
        
    }
     } 
}
