package ate.greaty;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.Global;
import ate.greaty.xge.CmccDpiPolicyPacket;
import ate.greaty.xge.CmccDpiPolicyUA;
import ate.greaty.xge.PT;
import ate.testcase.CsUATestcase;

public class Dpi_REPORTER_POLICY extends CsUATestcase {
    CmccDpiPolicyUA svr;
    DpiTest_Client dpiclient=new DpiTest_Client();

    @BeforeClass
    public void BeforeClass() {
        super.BeforeClass();
        Global.verbose = true;
        svr = create_server("cdp://" + siip + ":5555/tcp",
                new CmccDpiPolicyUA());
        this.start_all_ua();
        assertTrue(svr.is_ready());
        // this.shutdown_all_ua();
    }
    
    /**
     * 流量分析结果上报策略 0x09
     */
     @Test
    public void REPORTER_POLICY() {
        STEP(PT.REPORTER_POLICY.toString());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);
        
      //  sleep(10000);
        for(int i=1;i<520;i++){
            int PolicyID = i;
            int PacketType;
            int PacketSubtype;
            String strDate="";
            String strDate1="";       
            int R_Freq =0;
            String R_Temp_Name ="";
            int R_Interface =0;
            String R_DestIP="";
            String UserName="";
            String Password ="";
            String SDTP_IP ="";
            
        if(i<=50){
            PacketType = 1;   //1 统计级；2 会话级
            PacketSubtype = 201;
            strDate = "2014-8-25 18:10:00";
            strDate1 = "2014-8-26 20:00:00";           
            R_Freq = 2;
            R_Temp_Name = "XDRFields_01.csv";
            R_Interface = 2;
            R_DestIP = "172.16.5.215";
            UserName = "target";
            Password = "targetet";
            SDTP_IP = "2.2.2.2";
        }else if (i>50 && i<=100){
            PacketType = 1;
            PacketSubtype = 202;
            strDate = "2014-8-25 18:10:00";
            strDate1 = "2014-8-27 20:00:00";           
            R_Freq = 1;
            R_Temp_Name = "XDRFields_02.csv";
            R_Interface = 2;
            R_DestIP = "172.16.5.216";
            UserName = "target";
            Password = "targetet";
            SDTP_IP = "3.3.3.3";
        }else if(i>100 && i<=150){
            PacketType = 1;
            PacketSubtype = 203;
            strDate = "2014-8-25 18:10:00";
            strDate1 = "2014-8-28 20:00:00";           
            R_Freq = 2;
            R_Temp_Name = "XDRFields_01.csv";
            R_Interface = 1;
            R_DestIP = "172.16.5.217";
            UserName = "target";
            Password = "target";
            SDTP_IP = "4.4.4.4";
        }else if (i>150 && i<=200){
            PacketType = 2;
            PacketSubtype = 100;
            strDate = "2014-8-25 18:10:00";
            strDate1 = "2014-8-29 20:00:00";           
            R_Freq = 2;
            R_Temp_Name = "XDRFields_01.csv";
            R_Interface = 1;
            R_DestIP = "172.16.5.218";
            UserName = "target";
            Password = "targetet";
            SDTP_IP = "5.5.5.5";
        }else if (i>200 && i<=250){
            PacketType = 2;
            PacketSubtype = 101;
            strDate = "2014-8-25 18:10:00";
            strDate1 = "2014-9-1 20:00:00";           
            R_Freq = 3;
            R_Temp_Name = "XDRFields_01.csv";
            R_Interface = 2;
            R_DestIP = "172.16.5.219";
            UserName = "target";
            Password = "targetet";
            SDTP_IP = "6.6.6.6";
        }else if (i>250 && i<=300){
            PacketType = 2;
            PacketSubtype = 102;
            strDate = "2014-8-25 18:10:00";
            strDate1 = "2014-9-2 20:00:00";           
            R_Freq = 2;
            R_Temp_Name = "XDRFields_01.csv";
            R_Interface = 1;
            R_DestIP = "172.16.5.210";
            UserName = "target";
            Password = "targetet";
            SDTP_IP = "7.7.7.7";
        }else if (i>300 && i<=350){
            PacketType = 2;
            PacketSubtype = 103; 
            strDate = "2014-8-25 18:10:00";
            strDate1 = "2014-9-3 20:00:00";           
            R_Freq = 1;
            R_Temp_Name = "XDRFields_02.csv";
            R_Interface = 2;
            R_DestIP = "172.16.5.212";
            UserName = "target";
            Password = "targetet";
            SDTP_IP = "8.8.8.8";
        }else if (i>350 && i<=400){
            PacketType = 2;
            PacketSubtype = 104; 
            strDate = "2014-8-25 18:10:00";
            strDate1 = "2014-9-4 20:00:00";           
            R_Freq = 2;
            R_Temp_Name = "XDRFields_01.csv";
            R_Interface = 1;
            R_DestIP = "12.45.15.2";
            UserName = "target";
            Password = "targetet";
            SDTP_IP = "15.45.46.4";
        }else if (i>400 && i<=450){
            PacketType = 2;
            PacketSubtype = 105; 
            strDate = "2014-8-25 18:10:00";
            strDate1 = "2014-9-5 20:00:00";           
            R_Freq = 3;
            R_Temp_Name = "XDRFields_01.csv";
            R_Interface = 1;
            R_DestIP = "45.45.1.5";
            UserName = "target";
            Password = "targetet";
            SDTP_IP = "48.146.46.13";
        }else if (i>450 && i<=500){
            PacketType = 2;
            PacketSubtype = 106; 
            strDate = "2014-8-25 18:10:00";
            strDate1 = "2014-9-6 20:00:00";           
            R_Freq = 4;
            R_Temp_Name = "XDRFields_02.csv";
            R_Interface = 1;
            R_DestIP = "211.46.135.15";
            UserName = "target";
            Password = "targetet";
            SDTP_IP = "66.15.43.5";
        }
        else{
            PacketType = 2;
            PacketSubtype = 107;
            strDate = "2014-8-25 18:10:00";
            strDate1 = "2014-9-7 00:00:00";           
            R_Freq = 2;
            R_Temp_Name = "XDRFields_01.csv";
            R_Interface = 2;
            R_DestIP = "77.45.16.4";
            UserName = "target";
            Password = "targetet";
            SDTP_IP = "84.4.16.1";
        }
        
       

        int R_StartTime = (int) dpiclient.get_utc_time(strDate);
        int R_EndTime = (int) dpiclient.get_utc_time(strDate1);
        

        CmccDpiPolicyPacket pn = svr.create_packet(PT.REPORTER_POLICY)
                .build("PolicyID", PolicyID).build("PacketType", PacketType)
                .build("PacketSubtype", PacketSubtype)
                .build("R_StartTime", R_StartTime)
                .build("R_EndTime", R_EndTime).build("R_Freq", R_Freq)
                .build("R_Temp_Name", R_Temp_Name)
                .build("R_Interface", R_Interface).build("R_DestIP", R_DestIP)
                .build("UserName", UserName).build("Password", Password)
                .build("SDTP_IP", SDTP_IP).get_packet();

        svr.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

        // sleep(50000);

    }
    }   
}
