package ate.greaty;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.Global;
import ate.greaty.xge.*;

import ate.testcase.CsUATestcase;

public class DpiTest_Client extends CsUATestcase {
    CmccDpiPolicyUA svr;

    @BeforeClass
    public void BeforeClass() {
        super.BeforeClass();
        Global.verbose = true;
        svr = create_server("cdp://" + siip + ":5555/tcp",
                new CmccDpiPolicyUA());
        this.start_all_ua();
        assertTrue(svr.is_ready());
        // this.shutdown_all_ua();
    }

    /**
     * 心跳消息响应 0x01
     */
    // @Test
    public void HEARTBEAT_ACK() {
        STEP(PT.HEARTBEAT_ACK.toString());
        int Result = 1;

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        // svr.register_auto_response("dft", svr);

        assertTrue(req instanceof HEARTBEAT);

        HEARTBEAT hb = (HEARTBEAT) req;

        System.out.println("hb==" + hb);
        assertTrue(hb.Dev_Name != null && hb.Dev_Name.length() != 0);

        System.out.println("hb.version==" + hb.version);

        assertTrue(hb.Dev_Name_Length.intValue() == hb.Dev_Name.length());
        assertTrue(expect.ucompare(hb.version, 1) == 0);

        // System.out.println("hb.MessageLength=="+hb.MessageLength);
        // System.out.println("uint(15+hb.Dev_Name_Length.intValue()=="+uint(15+hb.Dev_Name_Length.intValue()));

        assertTrue(expect.ucompare(hb.MessageLength,
                19 + hb.Dev_Name_Length.intValue()) == 0);
        assertTrue(hb.Timestamp.longValue() > (now() / 1000 - 10), "now:"
                + now() + " ts:" + hb.Timestamp);
        assertTrue(hb.MessageVersion.intValue() >= 0);
        assertTrue(hb.MessageID.intValue() >= 0);

        HEARTBEAT_ACK resp = (HEARTBEAT_ACK) hb.create_response()
                .build("Result", Result).get_packet();

        svr.send_message(resp);

    }
    
  //  @Test
    public void test(){
        double i=Double.MAX_VALUE;
        while(i==i+1){
            sleep(5000);
        CmccDpiPolicyPacket req1 = svr.recv_message();
        sleep(5000);
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);
        
        }
    }

    /**
     * DPI设备状态查询 0x0e
     */
    // @Test
    public void Dev_Status() {
        STEP(PT.Dev_Status.toString());
        int InfoReportFreq = 1;

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);

        CmccDpiPolicyPacket pn = svr.create_packet(PT.Dev_Status)
                .build("InfoReportFreq", InfoReportFreq).get_packet();

        svr.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

        // assertTrue(req instanceof Dev_Status_ACK);

        // Dev_Status_ACK hb1=(Dev_Status_ACK)req;

        // System.out.println("hb.DeploySiteName=="+hb1.DeploySiteName);
        // System.out.println("hb.DeploySiteName=="+hb1.DeploySiteNameLength.byteValue());
        // System.out.println("hb.DeploySiteName=="+hb1.PortsTypeNum.byteValue());

        sleep(600000);

    }

    /**
     * 静态IP用户同步消息 0x02
     */

    @Test
    public void STATIC_IP_SYNC() {
        STEP(PT.STATIC_IP_SYNC.getValue());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);
         for(int i=1;i<256;i++){
             log.info(i+"");
             
        int Bind_Action = 1;
        String UserName = "test_"+i;
     
      
        HashMap<String, Object> usreip = new HashMap<String, Object>();
        usreip.put("UserIP0", "3.16.5.0");
        usreip.put("UserIP_Prefix0", 24);
        usreip.put("UserIP1", "4.16.5.0");
        usreip.put("UserIP_Prefix1", 24);

        CmccDpiPolicyPacket pn = svr.create_packet(PT.STATIC_IP_SYNC)
                .build("Bind_Action", Bind_Action)
                .build("UserName", UserName)
                
                .build("userip", usreip)

                .get_packet();

        svr.send_message(pn);

        STATIC_IP_SYNC hb = (STATIC_IP_SYNC) pn;
        System.out.println("hb==" + hb.usreip);
        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
       
         }
    }
 //  @Test
    public void STATIC_IP_SYNC_1() {
        STEP(PT.STATIC_IP_SYNC.getValue());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);

        int Bind_Action = 1;
        String UserName = "556";
        
        HashMap<String, Object> usreip = new HashMap<String, Object>();
        usreip.put("UserIP0", "12.16.5.0");
        usreip.put("UserIP_Prefix0", 24);
        usreip.put("UserIP1", "13.16.5.0");
        usreip.put("UserIP_Prefix1", 24);

        CmccDpiPolicyPacket pn = svr.create_packet(PT.STATIC_IP_SYNC)
                .build("Bind_Action", Bind_Action).build("UserName", UserName)
                // .build("IP_SegmentNum", IP_SegmentNum)
                .build("userip", usreip)

                .get_packet();

        svr.send_message(pn);

        STATIC_IP_SYNC hb = (STATIC_IP_SYNC) pn;
        System.out.println("hb==" + hb.usreip);
        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        

    }

    /**
     * 用户组归属分配消息 0x03
     */
    @Test
    public void UG_HOME() {
        STEP(PT.UG_HOME.toString());
        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);

       for(int i=1;i<256;i++){
        String UserGroupName = "ddd"+i;
        String UserGroupName1 = "ttt";
        int Action = 1;
        int UserType = 2;
        String UserName = "test_"+i;

        String UserName1 = "test_2";

        String UserName2 = "556";
        
        UG_HOME pn = (UG_HOME) svr.create_packet(PT.UG_HOME);
        
        HashMap<String, Object> ug1 = new HashMap<String, Object>();
        ug1.put("UserGroupName", UserGroupName);
        ug1.put("Action", Action);

        HashMap<String, Object> ug1_usr1 = new HashMap<String, Object>();
        ug1_usr1.put("UserType", UserType);
        ug1_usr1.put("UserName", UserName);

        HashMap<String, Object> ug1_usr2 = new HashMap<String, Object>();
        ug1_usr2.put("UserType", UserType);
        ug1_usr2.put("UserName", UserName1);

        pn.add_usergroup(ug1).add_user(ug1_usr1);

        HashMap<String, Object> ug2 = new HashMap<String, Object>();
        ug2.put("UserGroupName", UserGroupName1);
        ug2.put("Action", Action);

        HashMap<String, Object> ug2_usr1 = new HashMap<String, Object>();
        ug2_usr1.put("UserType", UserType);
        ug2_usr1.put("UserName", UserName2);

      //   pn.add_usergroup(ug2).add_user(ug2_usr1);

        STEP("Send Message");
        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        System.out.println("req.toString()==" + req.toString());

       }
    }

    /**
     * 资源特征库下发策略 0x04
     */
  //   @Test
    public void RES_SPEC_POLICY() {
        STEP(PT.RES_SPEC_POLICY.toString());
        
        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);
        
        
        int Resoure_Type = 4;
        int Protocol_Version = 255;
        String Server_IP = "172.16.5.211";
        int Server_Port = 21;
        String UserName = "target";
        String Password = "targetet";
        // String FileName = "ipat_01_XXX.csv";   //1
      //   String FileName = "TrafficDirection_01_XXX.csv";   //2
         String FileName = "XDRFields_02.csv";  //4
     //    String FileName = "CustomizedSignature_1_XXX.csv";   //3
      //  String FileName = "QQ_172.16.5.217_p.pcap";   //5

        CmccDpiPolicyPacket pn = svr.create_packet(PT.RES_SPEC_POLICY)
                .build("Resoure_Type", Resoure_Type)
                .build("Protocol_Version", Protocol_Version)
                .build("Server_IP", Server_IP)
                .build("Server_Port", Server_Port).build("UserName", UserName)
                .build("Password", Password).build("FileName", FileName)
                .get_packet();

        svr.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

    }

    /**
     * 流量镜像策略 0x05
     */
 //  @Test
    public void FLOW_MIRROR() {
        STEP(PT.FLOW_MIRROR.toString());
        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);

        int PolicyID = 1;
        int MProtocalType = 255;
        int MAppType = 12;
        int MAppID = 3;

        HashMap<String, Object> mdstIp = new HashMap<String, Object>();
        mdstIp.put("MDstIpAddr0", "172.16.5.0");
        mdstIp.put("MDstIpPrefixLen0", 25);
        mdstIp.put("MDstIpAddr1", "1.1.0.0");
        mdstIp.put("MDstIpPrefixLen1", 16);

        int MDataMatchOffset = 0;

        String MDataMatchContent = "";

        String strDate = "2014-8-21 10:40:00";
        String strDate1 = "2014-9-2 18:45:00";

        int M_StartTime = (int) this.get_utc_time(strDate);
        int M_EndTime = (int) this.get_utc_time(strDate1);
        int M_GroupNo = 7;
        int M_Direction = 1;
        int MFlowAdd = 1;
        int MType = 1;

        FLOW_MIRROR pn = (FLOW_MIRROR) svr
                .create_packet(PT.FLOW_MIRROR)
                .build("PolicyID", PolicyID)
                .build("MProtocalType", MProtocalType)
                .build("MAppType", MAppType)
                .build("MAppID", MAppID)
                 .build("mdstIp", mdstIp)

               
                .build("MDataMatchOffset", MDataMatchOffset)
                .build("MDataMatchContent", MDataMatchContent)
                .build("M_StartTime", M_StartTime)
                .build("M_EndTime", M_EndTime)
                .build("M_GroupNo", M_GroupNo)
                .build("M_Direction", M_Direction)
                .build("MFlowAdd", MFlowAdd)
                .build("MType", MType);
                //.get_packet();

        HashMap<String, Object> ms = new HashMap<String, Object>();
        ms.put("MSrcPort", 235);
        
        HashMap<String, Object> ms1 = new HashMap<String, Object>();
        ms1.put("MSrcPort", 245);
        
        HashMap<String, Object> ms2 = new HashMap<String, Object>();
        ms2.put("MSrcPort", 255);
        
        HashMap<String, Object> ms3 = new HashMap<String, Object>();
        ms3.put("MSrcPort", 265);
         
        pn.add_msrc(ms);
        
        HashMap<String, Object> md = new HashMap<String, Object>();
        md.put("MDstPort", 135);
        
        HashMap<String, Object> md1 = new HashMap<String, Object>();
        md1.put("MDstPort", 145);
        
        HashMap<String, Object> md2 = new HashMap<String, Object>();
        md2.put("MDstPort", 155);
        
        HashMap<String, Object> md3 = new HashMap<String, Object>();
        md3.put("MDstPort", 165);
        
        pn.add_mdst(md);
        
        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        // sleep(10000);

    }

    /**
     * 流量管理策略 0x06
     */
 //   @Test
    public void FLOW_MAN() {
        STEP(PT.FLOW_MAN.toString());
        int PolicyID = 3;
        int ProtocolType = 255;
        int AppType = 12;
        int AppID = 0;
        int AppTraffic_Up_ABS = 1000;
        int AppTraffic_Dn_ABS = 1000;
        int AppSession_Up_ABS = 20;
        int AppSession_Dn_ABS = 20;
        int QoS_Label_Up = 4;
        int QoS_Label_Dn = 2;

        // 时间段1
        String strDate = "2014-9-1 14:50:00";
        String strDate1 = "2014-9-2 19:00:00";
        // 时间段2
        String strDate2 =" 2014-8-25 15:25:00";
        String strDate3 = "2014-8-25 15:30:00";
        // 时间段3
        String strDate4 = "2014-8-25 15:35:00";
        String strDate5 = "2014-8-25 15:40:00";
        // 时间段4
        String strDate6 = "2014-8-25 15:45:00";
        String strDate7 = "2014-8-25 15:50:00";

        FLOW_MAN pn = (FLOW_MAN) svr.create_packet(PT.FLOW_MAN)
                .build("PolicyID", PolicyID)
                .build("ProtocolType", ProtocolType).build("AppType", AppType)
                .build("AppID", AppID)
                .build("AppTraffic_Up_ABS", AppTraffic_Up_ABS)
                .build("AppTraffic_Dn_ABS", AppTraffic_Dn_ABS)
                .build("AppSession_Up_ABS", AppSession_Up_ABS)
                .build("AppSession_Dn_ABS", AppSession_Dn_ABS)
                .build("QoS_Label_Up", QoS_Label_Up)
                .build("QoS_Label_Dn", QoS_Label_Dn);

        HashMap<String, Object> time = new HashMap<String, Object>();
        time.put("Starttime",(long)(this.get_utc_time(strDate)));
        time.put("Stoptime", (long) (this.get_utc_time(strDate1)));

        HashMap<String, Object> time1 = new HashMap<String, Object>();
        time1.put("Starttime", (long) (this.get_utc_time(strDate2)));
        time1.put("Stoptime", (long) (this.get_utc_time(strDate3)));

        HashMap<String, Object> time2 = new HashMap<String, Object>();
        time2.put("Starttime", (long) (this.get_utc_time(strDate4)));
        time2.put("Stoptime", (long) (this.get_utc_time(strDate5)));

        HashMap<String, Object> time3 = new HashMap<String, Object>();
        time3.put("Starttime", (long) (this.get_utc_time(strDate6)));
        time3.put("Stoptime", (long) (this.get_utc_time(strDate7)));

        pn.add_time(time);

        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
    }

    /**
     * 封堵URL/IP/域名同步策略 0x07
     */
   //  @Test
    public void BLOCK_POLICY() {
        STEP(PT.BLOCK_POLICY.toString());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);

        int PolicyID = 1;
        int Action = 1;
        int Block_Type = 3;     //1 URL;2 IP;3 域名
       
        
        BLOCK_POLICY pn = (BLOCK_POLICY) svr.create_packet(PT.BLOCK_POLICY)
                .build("PolicyID", PolicyID).build("Action", Action)
                .build("Block_Type", Block_Type);
        

        HashMap<String, Object> block = new HashMap<String, Object>();
        block.put("Block_Name", "www.poco.cn");

        HashMap<String, Object> block1 = new HashMap<String, Object>();
        block1.put("Block_Name", "www.ouou.com");

        HashMap<String, Object> block2 = new HashMap<String, Object>();
        block2.put("Block_Name", "www.live.com");

        HashMap<String, Object> block3 = new HashMap<String, Object>();
        block3.put("Block_Name", "www.google.cn");

        HashMap<String, Object> block4 = new HashMap<String, Object>();
        block4.put("Block_Name", "www.taobao.com");

        pn.add_block(block).add_block(block1).add_block(block2)
                .add_block(block3).add_block(block4);

        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

    }

    /**
     * 网站访问分析策略 0x08
     */
  //   @Test
    public void SITE_ACCESS() {
        STEP(PT.SITE_ACCESS.toString());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);

        int PolicyID = 6;
        int Action = 1;
        int SRC_AREAGROUP_ID = 61;
        
        String SRC_Area_Name = "network_in1";
        int DEST_AREAGROUP_ID = 62;
       
        String DEST_Area_Name = "network_out1";

        SITE_ACCESS pn = (SITE_ACCESS) svr.create_packet(PT.SITE_ACCESS)
                .build("PolicyID", PolicyID).build("Action", Action)
                .build("SRC_AREAGROUP_ID", SRC_AREAGROUP_ID)
               
                .build("DEST_AREAGROUP_ID", DEST_AREAGROUP_ID);
        

        HashMap<String, Object> src = new HashMap<String, Object>();
        src.put("SRC_Area_Name", SRC_Area_Name);

        HashMap<String, Object> src1 = new HashMap<String, Object>();
        src1.put("SRC_Area_Name", "rrrrrrr");

        HashMap<String, Object> src2 = new HashMap<String, Object>();
        src2.put("SRC_Area_Name", "ddddddd");
        pn.add_src(src);

        HashMap<String, Object> des = new HashMap<String, Object>();
        des.put("DEST_Area_Name", DEST_Area_Name);

        HashMap<String, Object> des1 = new HashMap<String, Object>();
        des1.put("DEST_Area_Name", "eeeeeee");

        HashMap<String, Object> des2 = new HashMap<String, Object>();
        des2.put("DEST_Area_Name", "ttttttt");

        pn.add_dest(des);

        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

    }

    // SRC_Area_Num==0 或DEST_Area_Num==0

  //   @Test
    public void SITE_ACCESS_1() {
        STEP(PT.SITE_ACCESS.toString());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);

        int PolicyID = 5;
        int Action = 1;
        int SRC_AREAGROUP_ID = 55;
        int SRC_Area_Num = 0;

        int DEST_AREAGROUP_ID = 56;
        int DEST_Area_Num = 0;

        SITE_ACCESS pn = (SITE_ACCESS) svr.create_packet(PT.SITE_ACCESS)
                .build("PolicyID", PolicyID).build("Action", Action)
                .build("SRC_AREAGROUP_ID", SRC_AREAGROUP_ID)
                .build("SRC_Area_Num", SRC_Area_Num)
                .build("DEST_AREAGROUP_ID", DEST_AREAGROUP_ID)
                .build("DEST_Area_Num", DEST_Area_Num);

        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

    }

    /**
     * 流量分析结果上报策略 0x09
     */
   //  @Test
    public void REPORTER_POLICY() {
        STEP(PT.REPORTER_POLICY.toString());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);

        int PolicyID = 13;
        int PacketType = 1;
        int PacketSubtype = 201;
        String strDate = "2014-8-25 18:10:00";
        String strDate1 = "2014-8-26 00:00:00";

        int R_StartTime = (int) this.get_utc_time(strDate);
        int R_EndTime = (int) this.get_utc_time(strDate1);
        int R_Freq = 1;
        String R_Temp_Name = "XDRFields_01.csv";
        int R_Interface = 1;
        String R_DestIP = "172.16.5.217";
        String UserName = "target";
        String Password = "targetet";
        String SDTP_IP = "2.2.2.2";

        CmccDpiPolicyPacket pn = svr.create_packet(PT.REPORTER_POLICY)
                .build("PolicyID", PolicyID).build("PacketType", PacketType)
                .build("PacketSubtype", PacketSubtype)
                .build("R_StartTime", R_StartTime)
                .build("R_EndTime", R_EndTime).build("R_Freq", R_Freq)
                .build("R_Temp_Name", R_Temp_Name)
                .build("R_Interface", R_Interface).build("R_DestIP", R_DestIP)
                .build("UserName", UserName).build("Password", Password)
                .build("SDTP_IP", SDTP_IP).get_packet();

        svr.send_message(pn);

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

         sleep(50000);

    }

    public long get_utc_time(String time) {
        // Date 类型时间
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String strDate = time;
        Date d = null;
        try {
            d = sdf.parse(strDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        log.info(d.getTime() / 1000 + "");
        return d.getTime() / 1000;
    }

    public int get_utc_time_H(String time) {
        // Date 类型时间
        DateFormat sdf = new SimpleDateFormat("hh:mm");
        String strDate = time;
        Date d = null;
        try {
            d = sdf.parse(strDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        log.info(cal.getTimeInMillis() + "");
        // 2、取得时间偏移量：
        int zoneOffset = cal.get(java.util.Calendar.ZONE_OFFSET);
        // 3、取得夏令时差：
        int dstOffset = cal.get(java.util.Calendar.DST_OFFSET);

        // 4、从本地时间里扣除这些差量，即可以取得UTC时间：
        cal.add(java.util.Calendar.MILLISECOND, -(zoneOffset + dstOffset));
        log.info(cal.getTimeInMillis() + "");

        return (int) (cal.getTimeInMillis() / 1000);
    }

    /**
     * 策略与对象绑定 0x0a
     */
  // @Test
    public void POLICY_BIND() {
        STEP(PT.POLICY_BIND.toString());
        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);

        for(int i=6;i<7;i++){
        int ServerNameNum = 1;
        int Bind_Action = 1;
        int UserType = 1;
        String UserName = "55"+i;
        int BandPlolicyCount = 5;

        POLICY_BIND pn = (POLICY_BIND) svr.create_packet(PT.POLICY_BIND)
                .build("ServerNameNum", ServerNameNum)
                .build("Bind_Action", Bind_Action).build("UserType", UserType)
                .build("UserName", UserName)
                .build("BandPlolicyCount", BandPlolicyCount);

        HashMap<String, Object> band = new HashMap<String, Object>();
        band.put("BandPolicyMessageType", 7);
        band.put("BandPlolicyMessageID", 9);

        HashMap<String, Object> band1 = new HashMap<String, Object>();
        band1.put("BandPolicyMessageType", 7);
        band1.put("BandPlolicyMessageID", 3);

        HashMap<String, Object> band2 = new HashMap<String, Object>();
        band2.put("BandPolicyMessageType", 7);
        band2.put("BandPlolicyMessageID", 4);

        pn.add_band(band);

        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
        }
    }
     /**
      * 策略与对象绑定 0x0a
      */
  //    @Test
     public void POLICY_BIND_1() {
         STEP(PT.POLICY_BIND.toString());
         CmccDpiPolicyPacket req1 = svr.recv_message();
         assertTrue(req1 != null);
         svr.register_auto_response("dft", svr);

         int ServerNameNum = 1;
         int Bind_Action = 1;
         int UserType =1 ;
         String UserName = "test_1";
         int BandPlolicyCount = 5;

         POLICY_BIND pn = (POLICY_BIND) svr.create_packet(PT.POLICY_BIND)
                 .build("ServerNameNum", ServerNameNum)
                 .build("Bind_Action", Bind_Action).build("UserType", UserType)
                 .build("UserName", UserName)
                 .build("BandPlolicyCount", BandPlolicyCount);

         HashMap<String, Object> band = new HashMap<String, Object>();
         band.put("BandPolicyMessageType", 6);
         band.put("BandPlolicyMessageID", 1);

         HashMap<String, Object> band1 = new HashMap<String, Object>();
         band1.put("BandPolicyMessageType", 5);
         band1.put("BandPlolicyMessageID", 1);

         HashMap<String, Object> band2 = new HashMap<String, Object>();
         band2.put("BandPolicyMessageType", 6);
         band2.put("BandPlolicyMessageID", 2);

         pn.add_band(band1).add_band(band);

         svr.send_message(pn.get_packet());

         CmccDpiPolicyPacket req = svr.recv_message();
         assertTrue(req != null);

     }
  

}
