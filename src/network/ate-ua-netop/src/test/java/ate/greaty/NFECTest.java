package ate.greaty;

import java.lang.reflect.Method;

import org.testng.annotations.*;

import ate.greaty.nfe.ControlUA;
import ate.greaty.nfe.NFECPacket;
import ate.testcase.CsUATestcase;

public class NFECTest extends CsUATestcase {

    @Test    
    public void basic2() {
        ControlUA svr = create_server("nfeci://" + siip + ":50002/tcp",
                new ControlUA());
        ControlUA ua = create_client("nfeci://" + sip + ":50002/tcp",
                new ControlUA());
        svr.startup();
        ua.startup();
        assertTrue(ua.is_ready());
        NFECPacket pn = ua.create_packet("10");
        pn.add_usergroup("ap_a", "01-01-01-01-01-01");
        pn.add_usergroup("ap_b", "01-01-01-01-01-02");
        pn.add_user("192.168.3.2", "01-01-01-01-01-01", "CC1",
                "01-02-02-02-02-02");
        pn.add_user("192.168.3.3", "01-01-01-01-01-02", "CC2",
                "01-02-02-02-02-03");
        String msg=
        "02115000081051000710100300101-01-01-01-01-01 06ap_a0100300101-01-01-01-01-02 06ap_b10006601CC1 0201-02-02-02-02-02 0301-01-01-01-01-01 04192.168.3.2 10006601CC2 0201-02-02-02-02-03 0301-01-01-01-01-02 04192.168.3.3 ";
        assertTrue(msg.equals(pn.get_message()),pn.get_message());
        ua.send_message(pn);
        svr.clear_q();
        NFECPacket req = svr.recv_message();
        assertTrue(req != null);
        log.info(req.toString());
        log.info(msg);
        assertTrue(msg.equals(req.toString()),req.toString());

    }

    @Test
    public void basic() {
        ControlUA svr = create_server("nfeci://" + siip + ":50002/tcp",
                new ControlUA());
        ControlUA ua = create_client("nfeci://" + sip + ":50002/tcp",
                new ControlUA());
        svr.startup();
        ua.startup();
        assertTrue(ua.is_ready());
        NFECPacket pn = ua.create_packet();
        pn.add_usergroup("ap_a", "01-01-01-01-01-01");
        pn.add_usergroup("ap_b", "01-01-01-01-01-02");
        pn.add_user("192.168.3.2", "01-01-01-01-01-01", "CC1",
                "01-02-02-02-02-02");
        pn.add_user("192.168.3.3", "01-01-01-01-01-02", "CC2",
                "01-02-02-02-02-03");
        String msg="01960100300101-01-01-01-01-01 06ap_a0100300101-01-01-01-01-02 06ap_b10006601CC1 0201-02-02-02-02-02 0301-01-01-01-01-01 04192.168.3.2 10006601CC2 0201-02-02-02-02-03 0301-01-01-01-01-02 04192.168.3.3 ";
                  //
        assertTrue(msg.equals(pn.get_message()),pn.get_message());

        ua.send_message(pn);

        NFECPacket req = svr.recv_message();
        assertTrue(req != null);
        assertTrue(msg.equals(req.toString()),req.toString());

    }

    @AfterMethod
    public void afterMethod(Method m) {
        super.shutdown_all_ua();
        super.AfterMethod(m);
    }
}
