package ate.greaty;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.Global;
import ate.greaty.xge.CmccDpiPolicyPacket;
import ate.greaty.xge.CmccDpiPolicyUA;
import ate.greaty.xge.FLOW_MIRROR;
import ate.greaty.xge.POLICY_BIND;
import ate.greaty.xge.PT;
import ate.greaty.xge.STATIC_IP_SYNC;
import ate.testcase.CsUATestcase;

public class Dpi_Flow_Mirror extends CsUATestcase {
    CmccDpiPolicyUA svr;
    DpiTest_Client dpiclient =new DpiTest_Client();

    @BeforeClass
    public void BeforeClass() {
        super.BeforeClass();
        Global.verbose = true;
        svr = create_server("cdp://" + siip + ":5555/tcp",
                new CmccDpiPolicyUA());
        this.start_all_ua();
        assertTrue(svr.is_ready());
        // this.shutdown_all_ua();
    }

    /**
     * 流量镜像策略 0x05
     */
     @Test
    public void FLOW_MIRROR() {
         STEP(PT.FLOW_MIRROR.toString());
         CmccDpiPolicyPacket req1 = svr.recv_message();
         assertTrue(req1 != null);
         svr.register_auto_response("dft", svr);
         for(int i=1;i<2;i++){
             
         int PolicyID = i;
         int MProtocalType = 255;
         int MAppType = 255;
         int MAppID = 255;
         String MDstIpAddr="",MDstIpAddr_1="";
         int MDstIpPrefixLen=0,MDstIpPrefixLen_1=0;
         int M_GroupNo = 4;
         int M_Direction = 3;
        if(i<=100){
            MDstIpAddr="1."+i+".0.0";
            MDstIpPrefixLen=16;
            MDstIpAddr_1="2."+i+".0.0";
            MDstIpPrefixLen_1=16;
        }
        else if(i>100 && i<=200){
            MDstIpAddr="3."+i+".0.0";
            MDstIpPrefixLen=16;
             MDstIpAddr_1="4."+i+".0.0";
             MDstIpPrefixLen_1=25;
             
             M_GroupNo = 4;
             M_Direction = 1;
        }
        else if (i>200 && i<=300){
            MDstIpAddr="5."+(i-200)+".0.0";
            MDstIpPrefixLen=27;
            MDstIpAddr_1="6."+(i-200)+".0.0";
            MDstIpPrefixLen_1=26;
            
            M_GroupNo = 0;
            M_Direction = 2;
        }
        else if (i>300 && i<=400){
            MDstIpAddr="7."+(i-300)+".0.0";
        MDstIpPrefixLen=25;
        MDstIpAddr_1="8."+(i-300)+".0.0";
        MDstIpPrefixLen_1=16;
        
        M_GroupNo = 0;
        M_Direction = 3;
        }
        else{
            MDstIpAddr="9."+(i-400)+".0.0";
            MDstIpPrefixLen=25;
            MDstIpAddr_1="10."+(i-400)+".0.0";
            MDstIpPrefixLen_1=16;
            
            M_GroupNo = 4;
            M_Direction = 3;
        }
       
       
        
      

         HashMap<String, Object> mdstIp = new HashMap<String, Object>();
         mdstIp.put("MDstIpAddr0", MDstIpAddr);
         mdstIp.put("MDstIpPrefixLen0", MDstIpPrefixLen);
         mdstIp.put("MDstIpAddr1", MDstIpAddr_1);
         mdstIp.put("MDstIpPrefixLen1", MDstIpPrefixLen_1);

         int MDataMatchOffset = 0;

         String MDataMatchContent = "";

         String strDate = "2014-8-26 10:40:00";
         String strDate1 = "2014-9-9 20:40:00";

         int M_StartTime = (int) dpiclient.get_utc_time(strDate);
         int M_EndTime = (int) dpiclient.get_utc_time(strDate1);
        
         
         int MFlowAdd = 1;
         int MType = 1;

         FLOW_MIRROR pn = (FLOW_MIRROR) svr
                 .create_packet(PT.FLOW_MIRROR)
                 .build("PolicyID", PolicyID)
                 .build("MProtocalType", MProtocalType)
                 .build("MAppType", MAppType)
                 .build("MAppID", MAppID)
                 .build("mdstIp", mdstIp)                
                 .build("MDataMatchOffset", MDataMatchOffset)
                 .build("MDataMatchContent", MDataMatchContent)
                 .build("M_StartTime", M_StartTime)
                 .build("M_EndTime", M_EndTime)
                 .build("M_GroupNo", M_GroupNo)
                 .build("M_Direction", M_Direction)
                 .build("MFlowAdd", MFlowAdd)
                 .build("MType", MType);
                 

         HashMap<String, Object> ms = new HashMap<String, Object>();
         ms.put("MSrcPort", 235);
         
         HashMap<String, Object> ms1 = new HashMap<String, Object>();
         ms1.put("MSrcPort", 245);
         
         HashMap<String, Object> ms2 = new HashMap<String, Object>();
         ms2.put("MSrcPort", 255);
         
         HashMap<String, Object> ms3 = new HashMap<String, Object>();
         ms3.put("MSrcPort", 265);
          
   //      pn.add_msrc(ms).add_msrc(ms1);
         
         HashMap<String, Object> md = new HashMap<String, Object>();
         md.put("MDstPort", 135);
         
         HashMap<String, Object> md1 = new HashMap<String, Object>();
         md1.put("MDstPort", 145);
         
         HashMap<String, Object> md2 = new HashMap<String, Object>();
         md2.put("MDstPort", 155);
         
         HashMap<String, Object> md3 = new HashMap<String, Object>();
         md3.put("MDstPort", 165);
         
       //  pn.add_mdst(md).add_mdst(md1);
         
         svr.send_message(pn.get_packet());

         CmccDpiPolicyPacket req = svr.recv_message();
         assertTrue(req != null);

       // sleep(20000);
        
/**
 * 对象和策略的绑定
 */
  
        STEP(PT.POLICY_BIND.toString());
       
        
        int ServerNameNum = 1;
        int Bind_Action = 1;
        int UserType= 0;;
        String UserName= "ddd"+1;;
     
      
  
        int BandPlolicyMessageID = PolicyID;
        
        
        POLICY_BIND pn1 = (POLICY_BIND)svr.create_packet(PT.POLICY_BIND)
                .build("ServerNameNum", ServerNameNum)
                .build("Bind_Action", Bind_Action).build("UserType", UserType)
                .build("UserName", UserName);
             
        
    HashMap<String, Object> band = new HashMap<String, Object>();
        band.put("BandPolicyMessageType", 5);
        band.put("BandPlolicyMessageID", BandPlolicyMessageID);

                
     pn1.add_band(band);
        svr.send_message(pn1.get_packet());

        CmccDpiPolicyPacket req2 = svr.recv_message();
        assertTrue(req2 != null);
        
    }
     } 
}
