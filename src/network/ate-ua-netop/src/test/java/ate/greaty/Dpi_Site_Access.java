package ate.greaty;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.Global;
import ate.greaty.xge.CmccDpiPolicyPacket;
import ate.greaty.xge.CmccDpiPolicyUA;
import ate.greaty.xge.PT;
import ate.greaty.xge.SITE_ACCESS;
import ate.testcase.CsUATestcase;

public class Dpi_Site_Access extends CsUATestcase {
    CmccDpiPolicyUA svr;

    @BeforeClass
    public void BeforeClass() {
        super.BeforeClass();
        Global.verbose = true;
        svr = create_server("cdp://" + siip + ":5555/tcp",
                new CmccDpiPolicyUA());
        this.start_all_ua();
        assertTrue(svr.is_ready());
        // this.shutdown_all_ua();
    }

    
 //   @Test
    public void SITE_ACCESS() {
        STEP(PT.SITE_ACCESS.toString());
        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);
    for(int i=1;i<200;i++){
        int PolicyID = i;
        int Action = 1;
        int SRC_AREAGROUP_ID = i;
        int DEST_AREAGROUP_ID = i+2;
        String SRC_Area_Name;
        String DEST_Area_Name;
        if(i<=50){
         SRC_Area_Name = "network_in1";       
       
         DEST_Area_Name = "network_out1";}
        else if (i>50 && i<=150){
            SRC_Area_Name = "network_in";       
            
            DEST_Area_Name = "network_out";
        }
        else{
      SRC_Area_Name = "network_in2";       
            
            DEST_Area_Name = "network_out2";
        }
            

        SITE_ACCESS pn = (SITE_ACCESS) svr.create_packet(PT.SITE_ACCESS)
                .build("PolicyID", PolicyID).build("Action", Action)
                .build("SRC_AREAGROUP_ID", SRC_AREAGROUP_ID)
               
                .build("DEST_AREAGROUP_ID", DEST_AREAGROUP_ID);
        

        HashMap<String, Object> src = new HashMap<String, Object>();
        src.put("SRC_Area_Name", SRC_Area_Name);

        HashMap<String, Object> src1 = new HashMap<String, Object>();
        src1.put("SRC_Area_Name", "rrrrrrr");

        HashMap<String, Object> src2 = new HashMap<String, Object>();
        src2.put("SRC_Area_Name", "ddddddd");
        pn.add_src(src);

        HashMap<String, Object> des = new HashMap<String, Object>();
        des.put("DEST_Area_Name", DEST_Area_Name);

        HashMap<String, Object> des1 = new HashMap<String, Object>();
        des1.put("DEST_Area_Name", "eeeeeee");

        HashMap<String, Object> des2 = new HashMap<String, Object>();
        des2.put("DEST_Area_Name", "ttttttt");

        pn.add_dest(des);

        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);
    }
    }
    
    @Test
    public void SITE_ACCESS_1() {
        STEP(PT.SITE_ACCESS.toString());

        CmccDpiPolicyPacket req1 = svr.recv_message();
        assertTrue(req1 != null);
        svr.register_auto_response("dft", svr);
        sleep(100000);
        
      for(int i=515;i<520;i++){
        int PolicyID = i;
        int Action = 1;
        int SRC_AREAGROUP_ID = i-514;
        int SRC_Area_Num = 0;

        int DEST_AREAGROUP_ID = i-513;
        int DEST_Area_Num = 0;

        SITE_ACCESS pn = (SITE_ACCESS) svr.create_packet(PT.SITE_ACCESS)
                .build("PolicyID", PolicyID).build("Action", Action)
                .build("SRC_AREAGROUP_ID", SRC_AREAGROUP_ID)
                .build("SRC_Area_Num", SRC_Area_Num)
                .build("DEST_AREAGROUP_ID", DEST_AREAGROUP_ID)
                .build("DEST_Area_Num", DEST_Area_Num);

        svr.send_message(pn.get_packet());

        CmccDpiPolicyPacket req = svr.recv_message();
        assertTrue(req != null);

    }

    }
    
    
    
}
