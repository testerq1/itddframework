import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.annotations.*
import ate.testcase.*
import common.*

public class NFECTest extends CsUATestcase{
	@Test
	public void basic2(){
		def svr=create_server("nfeci://"+siip+":50002/tcp");
		def ua=create_client("nfeci://"+sip+":50002/tcp");
		svr.startup();
		ua.startup();
		assertTrue(ua.is_ready());
		def pn=ua.create_packet("10");
		pn.add_usergroup("ap_a","01-01-01-01-01-01");
		pn.add_usergroup("ap_b","01-01-01-01-01-02");
		pn.add_user("192.168.3.2", "01-01-01-01-01-01","CC1", "01-02-02-02-02-02");
		pn.add_user("192.168.3.3", "01-01-01-01-01-02","CC2", "01-02-02-02-02-03");
		log.info(pn.get_message());
		//assertTrue("01930100310101-01-01-01-01-01 06ap_a 0100310101-01-01-01-01-02 06ap_b 10006601CC1 0201-02-02-02-02-02 0301-01-01-01-01-01 04192.168.3.2 10006601CC2 0201-02-02-02-02-03 0301-01-01-01-01-02 04192.168.3.3 ".equals(pn.get_message()));
		
		ua.send_message(pn);
		
		def req=svr.recv_message();
		assertTrue(req!=null);
		log.info(req.toString());
		//assertTrue("01930100310101-01-01-01-01-01 06ap_a 0100310101-01-01-01-01-02 06ap_b 10006601CC1 0201-02-02-02-02-02 0301-01-01-01-01-01 04192.168.3.2 10006601CC2 0201-02-02-02-02-03 0301-01-01-01-01-02 04192.168.3.3 ".equals(req.toString()));
				 
	}
	public void basic(){
		def svr=create_server("nfeci://"+siip+":50002/tcp");
		def clt=create_client("nfeci://"+sip+":50002/tcp");
		start_all_ua();
		
		def pn=clt.create_packet();		
		pn.add_usergroup("ap_a","01-01-01-01-01-01");
		pn.add_usergroup("ap_b","01-01-01-01-01-02");
		//String IP, String AP_MAC, String login_name, String STA_MAC         
		pn.add_user("192.168.3.2", "01-01-01-01-01-01","CC1", "01-02-02-02-02-02");
		pn.add_user("192.168.3.3", null,"CC2", "01-02-02-02-02-03");
		clt.send_message(pn);
		
		log.info(pn.toString());
		
		def req=svr.recv_message();
		assertTrue(req!=null);
		log.info(req.toString());
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {		
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
