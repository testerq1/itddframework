package ate.ua.openflow;

/*
 * #%L
 * ate-ua-openflow
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.nio.ByteBuffer;
import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.openflow.protocol.OFMessage;
import org.openflow.protocol.OFType;
import org.openflow.protocol.factory.BasicFactory;

import ate.AbstractURIHandler;
import ate.ua.UAOption;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;

/**
 * 基本框架完成
 * 
 * @author ravi huang
 * 
 */
public class OpenflowUA extends AMinaUAImp<OFMessage> {
	class OpenflowDecoder extends CumulativeProtocolDecoder {
		@Override
		protected boolean doDecode(IoSession session, IoBuffer in,
				ProtocolDecoderOutput out) throws Exception {
			if (in.remaining() >= 0) {
				out.write(factory.parseMessages(in.buf()));
				return true;
			}
			return false;
		}
	}

	class OpenflowEncoder extends ProtocolEncoderAdapter {
		@Override
		public void encode(IoSession session, Object message,
				ProtocolEncoderOutput out) throws Exception {
			if (message instanceof OFMessage) {
				OFMessage tmp = (OFMessage) message;
				ByteBuffer bf = ByteBuffer.allocate(tmp.getLength());
				tmp.writeTo(bf);
				bf.flip();
				out.write(IoBuffer.wrap(bf));
			} else if (message instanceof IoBuffer)
				out.write(message);
		}
	}

	BasicFactory factory = new BasicFactory();

	@Override
	protected void add_default_filter() {
		add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(
				new OpenflowEncoder(), new OpenflowDecoder()));
	}

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option == UA_TYPE)
			this.transport = "tcp";

		return this;
	}

	public OFMessage create_message(String name) {
		OFType oft = OFType.valueOf(name);
		return factory.getMessage(oft);
	}

	public OFMessage create_message(String name, OFMessage req) {
		OFMessage resp = create_message(name);
		resp.setXid(req.getXid());
		return resp;
	}

	public OFMessage create_response(OFMessage req) {
		OFType type = req.getType();
		OFMessage resp = null;
		switch (type) {
		case HELLO:
			resp = factory.getMessage(OFType.HELLO);
			break;
		case ECHO_REQUEST:
			resp = factory.getMessage(OFType.ECHO_REPLY);
			break;
		case FEATURES_REQUEST:
			resp = factory.getMessage(OFType.FEATURES_REPLY);
			break;
		case GET_CONFIG_REQUEST:
			resp = factory.getMessage(OFType.GET_CONFIG_REPLY);
			break;
		case STATS_REQUEST:
			resp = factory.getMessage(OFType.STATS_REPLY);
			break;
		case BARRIER_REQUEST:
			resp = factory.getMessage(OFType.BARRIER_REPLY);
			break;
		case QUEUE_CONFIG_REQUEST:
			resp = factory.getMessage(OFType.QUEUE_CONFIG_REPLY);
			break;
		default:
			break;
		}
		if (resp != null)
			resp.setXid(req.getXid());
		return resp;
	}

	@Override
	public String get_default_schema() {
		return "openflow";
	}

	@Override
	public boolean message_matched(OFMessage msg, HashMap hm) {
		return true;
	}
}
