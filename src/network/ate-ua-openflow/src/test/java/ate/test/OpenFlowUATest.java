package ate.test;

/*
 * #%L
 * ate-ua-openflow
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.Method;
import java.util.List;

import org.openflow.protocol.OFHello;
import org.openflow.protocol.OFMessage;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

import ate.testcase.CsUATestcase;
import ate.ua.openflow.OpenflowUA;

public class OpenFlowUATest extends CsUATestcase {
	@Test
	public void hello_openflow() {
		log.info(STEP+" hello_openflow:");
		log.info(STEP+".1 init:");
		OpenflowUA svr = (OpenflowUA)this.create_server("openflow://127.0.0.1:5000/");
		OpenflowUA clt1=(OpenflowUA)create_client("openflow://127.0.0.1:5000/");		
		this.start_all_ua();
		log.info(STEP+". client send hello:");
		OFMessage o=clt1.create_message("HELLO");
		clt1.send_message(o);
		
		Object tmp = svr.recv_message();
		assertTrue(tmp !=null );
		OFMessage req = (OFMessage)(((List)tmp).get(0));
		
		assertTrue(req !=null && (req instanceof OFHello) );
		
		svr.send_message(svr.create_response(req));
		
		tmp = clt1.recv_message();
		assertTrue(tmp !=null );
		OFMessage resp = (OFMessage)(((List)tmp).get(0));
		assertTrue(req !=null && (resp instanceof OFHello) );
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}

}
