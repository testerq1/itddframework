package ate.ua.cs;

/*
 * #%L
 * iTDD UA C/S
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.HashMap;

import org.apache.commons.codec.DecoderException;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import ate.ua.AtePacket;
import ate.ua.IAutoResponseMessage;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;
import ate.util.X2X0;

/**
 * c/s模型的UA，可以用于自定义协议，协议fuzzer测试等场景
 * 
 * @author xiaoyong.huang
 * 
 */
public class CsUA extends AMinaUAImp<String> implements IAutoResponseMessage<IoSession, String> {
	class SocketDecoder extends CumulativeProtocolDecoder {

		@Override
		protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out)
				throws Exception {
			if (in.remaining() <= 0) {				
				return false;
			}
			
			if(CsUA.this.is_fuzzy()){
			    byte[] bs=new byte[in.remaining()];
                in.get(bs); 
			    out.write(bs);
			}else
			    out.write(in.getString(charset.newDecoder()));
			return true;
		}
	}

	class SocketEncoder extends ProtocolEncoderAdapter {
		@Override
		public void encode(IoSession session, Object message, ProtocolEncoderOutput out)
				throws Exception {		    
		    
			if (message instanceof String){
			    byte[] bs=message.toString().getBytes(charset);
				out.write(IoBuffer.wrap(bs));				
			}else if (message instanceof IoBuffer){
			    out.write((IoBuffer)message);		    
			}else if (message instanceof byte[]){
                out.write(IoBuffer.wrap((byte[])message));                
			}else
			    log.error("message type wrong: {}",message);
		}
	}

	/** The buffer. */
	private IoBuffer buffer;

	public CsUA() {
		buffer = IoBuffer.allocate(1000);
		buffer.setAutoExpand(true);
		buffer.setAutoShrink(true);
	}

	@Override
	protected void add_default_filter() {
		add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(new SocketEncoder(),
				new SocketDecoder()));
	}

	/**
	 * Clear buffer.
	 */
	public void clear_buffer() {
		buffer.clear();
		buffer.setAutoExpand(true);
		buffer.setAutoShrink(true);
	}

	public String get_auto_resp_msg(IoSession session, String req) {
		String s = session.getService().getTransportMetadata().getName();
		this.add_packet(new AtePacket(session, req));
		return transport + " from:" + session.getLocalAddress() + " to:"
				+ session.getRemoteAddress();
		// IoBuffer.wrap(tmp.getBytes());
	}

	@Override
	public String get_default_schema() {
		return "socket";
	}

	@Override
	public boolean message_matched(String msg, HashMap hm) {
		return true;
	}

	/**
	 * 将一个16进制字符串放入buffer.
	 * 
	 * @param hex
	 *            the hex
	 * @throws DecoderException
	 *             the decoder exception
	 * @see hexs2b()
	 */
	void put_hexstring(String hex) throws DecoderException {
		log.debug(hex);
		buffer.put(X2X0.hexs2b(hex));
	}

	/**
	 * 将一个整数按照指定字节数放入buffer中 putInt(20,4) -->00000014.
	 * 
	 * @param v
	 *            整数值
	 * @param length
	 *            放入buffer中时占据的字节数
	 * @throws DecoderException
	 *             the decoder exception
	 */
	public void put_int(int v, int length) throws DecoderException {
		buffer.put(X2X0.int2b(v, length));
	}

	/**
	 * 将IP地址字符串转换为4字节byte，put到buffer中.
	 * 
	 * @param ip
	 *            the ip
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public void put_ip(String ip) throws UnknownHostException {
		buffer.put(X2X0.ip2b(ip));
	}

	/**
	 * 将一个字符串转化为byte，放入buffer中.
	 * 
	 * @param s
	 *            the s
	 * @throws UnsupportedEncodingException
	 */
	void put_string(String s) throws UnsupportedEncodingException {
		buffer.put(s.getBytes(charset));
	}

	/**
	 * 将一个字符串转化为byte，放入buffer中.
	 * 
	 * @param s
	 *            the s
	 * @throws UnsupportedEncodingException
	 */
	void put_string(String s, String charset) throws UnsupportedEncodingException {
		buffer.put(s.getBytes(charset));
	}

	public void send_buffer() {
		this.send_buffer(this.buffer);
	}

	/**
	 * 每次都会清掉message queue
	 * 
	 * @param autoResp
	 */
	public void set_auto_response(boolean autoResp) {
	    if(autoResp)
	        register_auto_response("dft", this);
	    else
	        this.remove_auto_response("dft");
		this.clear_q();
	}
}
