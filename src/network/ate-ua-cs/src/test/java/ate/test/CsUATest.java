package ate.test;

/*
 * #%L
 * iTDD UA C/S
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.Method;
import java.util.Arrays;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import ate.Global;
import ate.testcase.CsUATestcase;
import ate.ua.cs.CsUA;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.JitterIoFilter;
import ate.ua.mina.SessionLogFilter;
import ate.ua.mina.UAClient;
import ate.ua.mina.UAServer;

public class CsUATest extends CsUATestcase {
	@DataProvider(name = "Data_TestStep")
	public Object[][] Data_TestStep() {
		return new String[][] {
				//{ "udp" },
				{ "tcp" } 
		};
	}
	@Test(dataProvider = "Data_TestStep")
	public void fuzzy(String transport){
	    STEP("fuzzy test: " + transport);
        CsUA svr = create_server("socket://"+siip+":5000/" + transport);        
        CsUA clt = create_client("socket://"+sip+":5000/" + transport);        
        start_all_ua();
        
        STEP("enable fuzzy:");
        svr.fuzzy(true);
        clt.fuzzy(true);
        
        STEP("send bytes:");
        clt.send_bytes(new byte[]{1,2,3});
        
        STEP("recv:");
        Object req = svr.recv_message();
        assertTrue(req instanceof byte[]);
        assertTrue(Arrays.equals((byte[])req,new byte[]{1,2,3}));      
        
        STEP("send string:");
        clt.send_message("abcd");
        
        STEP("recv:");
        req = svr.recv_message();
        assertTrue(req instanceof byte[]);
        assertTrue(Arrays.equals((byte[])req,new byte[]{'a','b','c','d'}));  
	}
	
	@Test(dataProvider = "Data_TestStep")
	public void executors (String transport) {
		STEP("init:"+transport);
		CsUA svr = (CsUA) this.create_server("socket://"+siip+":5000/" + transport);
		svr.set_auto_response(true);
		this.start_all_ua();
		assertTrue(svr.is_ready());
		STEP("create:");
		executor_create(5);
		
		for(int i=0;i<5;i++){
			executor.execute(new Worker("socket://"+sip+":5000/" + transport,"clt"+i,10));  
		}
		
		assertTrue(executor_await_termination(1000));				
	}
	
	class Worker implements Runnable{
		String name;
		int loop;
		CsUA clt;
		Worker(String uri, String name, int loop){
			this.name=name;
			this.loop=loop;
			this.clt=(CsUA) create_client(uri);			
		}		
		
		public void run(){
			clt.startup();
			for(int i=0;i<loop;i++){
				clt.send_message("hello im "+name+" loop:"+i);
				String resp = clt.recv_message();
				assertTrue(resp!=null);
			}
			log.info(name+" done!");
		}
		
	}
	@Test(dataProvider = "Data_TestStep")
	public void worker(String transport) {		
		STEP("init:");
		CsUA svr = (CsUA) this.create_server("socket://"+sip+":5000/" + transport);
		svr.set_auto_response(true);
		svr.startup();
		Thread[] ww=new Thread[100];
		STEP("loop:");
		for(int i=0;i<ww.length;i++){
			ww[i]=new Thread(new Worker("socket://"+sip+":5000/" + transport,"clt"+i,10));
		}
		
		STEP("loop2:");
		//start_all_ua();
		for(int i=0;i<ww.length;i++){			
			ww[i].start();
			try {
				ww[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}		
	}
	@Test(dataProvider = "Data_TestStep")
	public void multi_clt_auto_response(String transport) {
		STEP("init:");
		CsUA svr = (CsUA) this.create_server("socket://"+siip+":5000/" + transport);
		CsUA clt1 = (CsUA) create_client("socket://"+sip+":5000/" + transport);
		CsUA clt2 = (CsUA) create_client("socket://"+sip+":5000/" + transport);
		svr.set_auto_response(true);
		start_all_ua();

		STEP("client send/recv message:");
		clt1.send_message("hello im clt1");
		clt2.send_message("hello im clt2");
		String resp1 = clt1.recv_message();
		assertTrue(resp1.startsWith(transport + " from:/"+siip+":5000 to:/"));
		log.info("clt1 recv:" + resp1);
		String resp2 = clt2.recv_message();
		assertTrue(resp2.startsWith(transport + " from:/"+sip+":5000 to:/"));
		log.info("clt2 recv:" + resp2);

		STEP("assert 2 resps isn't equal:");
		assertFalse(resp1.equals(resp2));

	}

	@Test(dataProvider = "Data_TestStep")
	public void multi_client(String transport) {
	    STEP("init: " + transport);
		CsUA svr = (CsUA) create_server("socket://"+siip+":5000/" + transport);
		CsUA clt1 = (CsUA) create_client("socket://"+sip+":5000/" + transport);
		CsUA clt2 = (CsUA) create_client("socket://"+sip+":5000/" + transport);
		svr.set_dump_file(transport+".pcap"); 

		start_all_ua();
		assertTrue(svr.is_ready() && clt1.is_ready() && clt2.is_ready());

		STEP("client1 send message:");
		clt1.send_message("clt1");
		String req = svr.recv_message();
		assertTrue(req.equals("clt1"));

		svr.add_last_iofilter("log", new SessionLogFilter(svr));
		STEP("server send response to clt1:");
		svr.send_message("hello clt1");
		req = clt1.recv_message();
		assertTrue(req.equals("hello clt1"));

		STEP("client2 send message:");
		clt2.send_message("clt2");
		req = svr.recv_message();
		assertTrue(req.equals("clt2"));

		svr.remove_iofilter("log");
		STEP("server send response to clt2:");
		svr.send_message("hello clt2");
		req = clt2.recv_message();
		assertTrue(req.equals("hello clt2"));

		STEP("reconnect all ua:");
		reconnect_all_ua();

		STEP("client1 send message:");
		clt1.send_message("clt1 loop");
		req = svr.recv_message();
		assertTrue(req.equals("clt1 loop"));

		STEP("server send response to clt1:");
		svr.send_message("hello clt1 loop");
		req = clt1.recv_message();
		assertTrue(req.equals("hello clt1 loop"));

		svr.add_last_iofilter("log", new SessionLogFilter(svr));
		STEP("client2 send message:");
		clt2.send_message("clt2 loop");
		req = svr.recv_message();
		assertTrue(req.equals("clt2 loop"));

		STEP("server send response to clt2:");
		svr.send_message("hello clt2 loop");
		req = clt2.recv_message();
		assertTrue(req.equals("hello clt2 loop"));
	}

	@Test(dataProvider = "Data_TestStep",invocationCount=1)
	public void send_rcv(String transport) {
	    STEP("send_rcv: " + transport);
		CsUA svr = create_server("socket://"+siip+":5000/" + transport);		
		CsUA clt = create_client("socket://"+sip+":1234/" + transport,"socket://"+sip+":5000/" + transport);
		//CsUA clt = create_client("socket://"+sip+":1234/" + transport,"socket://"+sip+":5000/" + transport);
        start_all_ua();
		assertTrue(svr.get_io_handler() instanceof UAServer);
        assertTrue(clt.get_io_handler() instanceof UAClient);
        
		clt.add_last_iofilter("jitter", new JitterIoFilter());
		svr.add_last_iofilter("jitter", new JitterIoFilter());
		STEP("client send message:");
		clt.send_message("abcdefg");
		String req = svr.recv_message();
		assertTrue(req.equals("abcdefg"));

		STEP("server send 1234567:");
		svr.send_message("1234567");
		req = clt.recv_message();
		assertTrue(req.equals("1234567"));
		
		STEP("redo clt send 11111111:");
        clt.send_message("11111111");

        req = svr.recv_message();
        assertTrue(req != null && req.equals("11111111"));

        STEP("server send 22222222:");
        svr.send_message("22222222");
        req = clt.recv_message();
        assertTrue(req.equals("22222222"));
        
		STEP("reconnect all ua:");
		reconnect_all_ua();
		clt.send_message("abcdefgh");

		req = svr.recv_message();
		assertTrue(req != null && req.equals("abcdefgh"));

		STEP("server send 1234567h:");
		svr.send_message("1234567h");
		req = clt.recv_message();
		assertTrue(req.equals("1234567h"));

	}

	@Test(dataProvider = "Data_TestStep")
	public void autosend(String transport) {
	    STEP("init:");		
		CsUA svr = this.create_server("socket://"+siip+":5000/" + transport);
		CsUA clt = this.create_client("socket://"+sip+":5000/" + transport);		
		start_all_ua();

		STEP("client send/recv message:");
		svr.set_auto_response(true);
		clt.send_message("abcdefg");
		String req = clt.recv_message();
		log.debug("recved!");
		assertTrue(req.startsWith(transport + " from:/"+sip+":5000 to:/"));

		STEP("client auto response:");
		clt.set_auto_response(true);
		svr.set_auto_response(false);

		svr.send_message("1234567");
		req = svr.recv_message();
		assertTrue(req.endsWith("to:/"+sip+":5000"));

		STEP("server auto response:");
		svr.set_auto_response(true);
		clt.set_auto_response(false);
		reconnect_all_ua();

		clt.send_message("abcdefgh");
		req = clt.recv_message();
		assertTrue(req.startsWith(transport + " from:/"+siip));

		STEP("client auto response 2:");
		svr.set_auto_response(false);
		clt.set_auto_response(true);
		svr.send_message("1234567h");
		req = svr.recv_message();
		assertTrue(req.endsWith("to:/"+sip+":5000"),req);

	}
	
	@Test(invocationCount=5)
	public void ssl(){
	    String transport="tcp";
	    STEP("send_rcv: " + transport);
        CsUA svr = create_server("socket://"+siip+":5000/" + transport);
        CsUA clt = create_client("socket://"+sip+":5000/" + transport);
        svr.use_bogus_ssl();
//        svr.startup();
//        sleep(100000);
        clt.use_bogus_ssl();
        start_all_ua();
        
        STEP("client send message:");
        clt.send_message("abcdefg");
        String req = svr.recv_message();
        assertTrue(req.equals("abcdefg"));

        STEP("server send 1234567:");
        svr.send_message("1234567");
        req = clt.recv_message();
        assertTrue(req.equals("1234567"));
        
        STEP("reconnect all ua:");
        reconnect_all_ua();
        clt.send_message("abcdefgh");

        req = svr.recv_message();
        assertTrue(req != null && req.equals("abcdefgh"));

        STEP("server send 1234567h:");
        svr.send_message("1234567h");
        req = clt.recv_message();
        assertTrue(req.equals("1234567h"));
	}
	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@Override
	@BeforeClass
	public void BeforeClass() {
	    //Global.verbose=true;
		super.BeforeClass();
	}
	
	@Override
	@AfterClass
	public void AfterClass() {
		super.AfterClass();
		LHOST.rm("*.pcap");
	}
}
