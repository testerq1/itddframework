package ate.test;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.testcase.CsUATestcase;

public class RtspUATest extends CsUATestcase {
	
	@Test
	public void base() {
		STEP("init:");
		def svr = this.create_server("rtsp://$sip");
		def clt = this.create_client("rtsp://$cip:3937","rtsp://$sip");
		start_all_ua();
	
		STEP("client send message:");
		
		def msg =clt.create_request(
            ["pkttype":"Describe",
                "uri":"/",
                "content":"12345678"]);
		clt.send_message(msg);
		
		STEP("server recv/send message:");
		//netty 会解析成两个包，一个Content，一个Request，这里需要等2个包收全
        def pkt=svr.recv_message();
        assertTrue(pkt!=null&&pkt.method().name()=="Describe");
        
		pkt=svr.recv_message(["pkttype":"content"]);		
		assertTrue(pkt=="12345678");
		
		msg =svr.create_response(
            ["status_code":"501",
                "reason":"xxxxxxx",
                "content":"1234567"]);
		svr.send_message(msg);
		
		STEP("client recv message:");
		pkt=clt.recv_message();
        assertTrue(pkt!=null && pkt.status().code()==501);
        
		pkt=clt.recv_message(["pkttype":"content"]);
		assertTrue(pkt=="1234567");
	}
	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@Override
	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
