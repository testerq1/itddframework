package ate.ua.rtsp;

/*
 * #%L
 * iTDD UA RTSP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.handler.codec.http.DefaultFullHttpRequest;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.rtsp.RtspMethods;
import io.netty.handler.codec.rtsp.RtspRequestDecoder;
import io.netty.handler.codec.rtsp.RtspRequestEncoder;
import io.netty.handler.codec.rtsp.RtspResponseDecoder;
import io.netty.handler.codec.rtsp.RtspResponseEncoder;
import io.netty.handler.codec.rtsp.RtspVersions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.seleniumhq.jetty7.http.HttpHeaders;

import ate.AbstractURIHandler;
import ate.ua.AtePacket;
import ate.ua.UAOption;
import ate.ua.netty.AFilterFactory;
import ate.ua.netty.ANettyUAImp;
public class RtspUA extends ANettyUAImp<Object> {
	HttpVersion version = RtspVersions.RTSP_1_0;
	
	@Override
	public void add_packet(Object msg) {
	    AtePacket pkt=(AtePacket)msg;
		log.debug("{} add packet {}", this, pkt.getPacket());
		if(pkt.getPacket() instanceof HttpContent)
			pkt=new AtePacket(pkt.getSession(),((HttpContent)pkt.getPacket()).content().toString(charset)); 
		msgQue.add(pkt);
	}
	
	@Override
	public void send_message(Object msg) {
		if(msg instanceof List){
			List<Object> oo=(List<Object>)msg;
			for(Object tmp:oo)
				super.send_message(tmp);
		}else
			super.send_message(msg);
			
	}
	
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option == UA_URI && port <= 0)
			port = 554;
		else if (option == UA_TYPE) {
			transport = "tcp";
			
			if (type == CLIENT)
				set_filters_factory(new AFilterFactory() {
					@Override
					public List<ChannelHandler> create_filters() {
						return new ArrayList<ChannelHandler>(Arrays.asList(
								new RtspResponseDecoder(), new RtspRequestEncoder()));
					}
				});
			else
				set_filters_factory(new AFilterFactory() {
					@Override
					public List<ChannelHandler> create_filters() {
						return new ArrayList<ChannelHandler>(Arrays.asList(
								new RtspRequestDecoder(), new RtspResponseEncoder()));
					}
				});

		}
		return this;
	}

	@Override
	public boolean message_matched(Object msg, HashMap<String, Object> hm) {
		log.debug("{} message_matched: {}",this,msg);
		if(hm==null){
			if(msg instanceof String)
				return false;
			return true;
		}
		Object type=hm.get("pkttype");
		if(type==null)
			return true;
		
		if(type.toString().equalsIgnoreCase("content"))
			return msg instanceof String;
		
		return true;
	}

	@Override
	public String get_default_schema() {
		return "rtsp";
	}

	public DefaultFullHttpResponse create_response(HashMap<String, String> paras) {
		new ArrayList<Object>();
		HashMap<String, String> tmp=(HashMap<String, String>)paras.clone();
		String v = tmp.remove("version");
		String content = tmp.remove("content");
		
		ByteBuf cbuf=null;
		HttpVersion cVersion=version;
		
		if(v!=null)
			cVersion=RtspVersions.valueOf(v);
		if(content!=null)
			cbuf=Unpooled.wrappedBuffer(content.getBytes(charset));
		
		DefaultFullHttpResponse resp = null;
		if (cbuf == null)
			resp = new DefaultFullHttpResponse(cVersion, new HttpResponseStatus(Integer.valueOf(tmp
					.remove("status_code")), tmp.remove("reason")));
		else{
			resp = new DefaultFullHttpResponse(cVersion, new HttpResponseStatus(
					Integer.valueOf(tmp.remove("status_code")), tmp.remove("reason")),cbuf);
			resp.headers().set(HttpHeaders.CONTENT_LENGTH,cbuf.capacity());
		}
		for(String h:tmp.keySet())
			resp.headers().set(h, tmp.get("h"));
		
		return resp;
	}

	public DefaultFullHttpRequest create_request(HashMap<String, String> paras) {
		new ArrayList<Object>();
		HashMap<String, String> tmp=(HashMap<String, String>)paras.clone();
		String v = tmp.remove("version");
		String content = tmp.remove("content");
		
		ByteBuf cbuf=null;
		HttpVersion cVersion=version;
		
		if(v!=null)
			cVersion=RtspVersions.valueOf(v);
		if(content!=null)
			cbuf=Unpooled.wrappedBuffer(content.getBytes(charset));
		
		DefaultFullHttpRequest req = null;
		if (cbuf == null)
			req = new DefaultFullHttpRequest(cVersion, RtspMethods.valueOf(tmp.remove("pkttype")),
					tmp.remove("uri"));
		else{
			req = new DefaultFullHttpRequest(cVersion, RtspMethods.valueOf(tmp
					.remove("pkttype")), tmp.remove("uri"),cbuf);
			req.headers().set(HttpHeaders.CONTENT_LENGTH,cbuf.capacity());
		}
		
		for(String h:tmp.keySet())
			req.headers().set(h, tmp.get("h"));
		
		return req;
	}
}
