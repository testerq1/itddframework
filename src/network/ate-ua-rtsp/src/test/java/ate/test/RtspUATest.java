package ate.test;

/*
 * #%L
 * iTDD UA RTSP
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.Global;
import ate.testcase.CsUATestcase;
import ate.ua.rtsp.RtspUA;

public class RtspUATest extends CsUATestcase {
	
	@Test
	public void base() {
		STEP("init:");
		RtspUA svr = this.create_server("rtsp://127.0.0.1");
		RtspUA clt = this.create_client("rtsp://127.0.0.1:1234","rtsp://127.0.0.1");
		start_all_ua();
		
		assertTrue(svr.is_ready()&&clt.is_ready());		
		assertTrue(svr.get_transport().equals("tcp")&&clt.get_transport().equals("tcp"));	
		assertTrue(svr.get_port()==554);
	
		STEP("client send message:");	
		
		Object msg =clt.create_request(new HashMap<String, String>(){{
			put("pkttype","Describe");
			put("uri","/");
			put("content","12345678");
			
		}});		
		clt.send_message(msg);
		
		STEP("server recv/send message:");	
		//netty 会解析成两个包，一个Content，一个Request，这里需要等2个包收全
		assertTrue(svr.expect_rcvd_count(2, 5000) );
		
		Object pkt=svr.recv_message();
        assertTrue(pkt!=null && pkt instanceof HttpRequest );
        assertTrue(((HttpRequest)pkt).method().name().equalsIgnoreCase("Describe")); 
        
		pkt=svr.recv_message(new HashMap<String, Object>(){{
			put("pkttype","content");			
		}});
		
		assertTrue(pkt!=null && pkt instanceof String );
		assertTrue(((String)pkt).equalsIgnoreCase("12345678"));	
		
		msg =svr.create_response(new HashMap<String, String>(){{
			put("status_code","501");
			put("reason","xxxxxxx");
			put("content","1234567");
		}});
		svr.send_message(msg);
		
		STEP("client recv message:");	
		assertTrue(clt.expect_rcvd_count(2, 5000) );
		
		pkt=clt.recv_message();
        
        assertTrue(pkt!=null && pkt instanceof HttpResponse );
        assertTrue(((HttpResponse)pkt).status().code()==501);
        
		pkt=clt.recv_message(new HashMap<String, Object>(){{
			put("pkttype","content");			
		}});
		
		assertTrue(pkt!=null && pkt instanceof String );
		assertTrue(((String)pkt).equalsIgnoreCase("1234567"));
	}
	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {		
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
	    Global.verbose=true;
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
