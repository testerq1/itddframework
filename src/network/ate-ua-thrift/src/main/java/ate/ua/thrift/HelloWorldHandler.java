package ate.ua.thrift;

import org.apache.thrift.TException;

import ate.ua.thrift.HelloWorld.Iface;

/**
 * 单独实现
 * @author ravi huang
 *
 */
public class HelloWorldHandler implements Iface {

	@Override
	public void ping(int length) throws TException {
		System.out.println("calling ping ,length=" + length);
	}

}
