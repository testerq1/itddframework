package ate.ua.thrift;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Date;

import org.apache.thrift.TProcessor;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.server.TNonblockingServer;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.server.THsHaServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.apache.thrift.transport.TNonblockingSocket;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

import ate.AbstractURIHandler;
import ate.ua.UAOption;

public class ThriftUA extends AbstractURIHandler {
	TTransport clientTransport;
	TServer server;
	Object args_svr;
	TProcessor processor;
	TProtocol protocol;
	String transport = "bin";

	// server join
	boolean join = false;

	public static final UAOption<TProcessor> SPROCESSOR = new UAOption<TProcessor>("SPROCESSOR");

	private TTransport getTransport() {
		try {
			if (path.startsWith("nonblock")) {
				return new TNonblockingSocket(host, port);
			}  else
				return new TSocket(host, port);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void initServerTransport() throws TTransportException {
		InetSocketAddress addr = new InetSocketAddress(host, port);
		if (path.contains("hsha")) {
			TNonblockingServerTransport transport = new TNonblockingServerSocket(addr);
			args_svr= new THsHaServer.Args(transport);
		} else if (path.contains("nonblock")) {
			TNonblockingServerTransport transport = new TNonblockingServerSocket(addr);
			args_svr = new TNonblockingServer.Args(transport);
		} else if (path.contains("threadselector")) {
			TNonblockingServerTransport transport = new TNonblockingServerSocket(addr);
			args_svr = new TThreadedSelectorServer.Args(transport);
		} else if (path.contains("threadpool")) {
			TServerTransport transport = new TServerSocket(addr);
			args_svr = new TThreadPoolServer.Args(transport);
		} else {
			TServerTransport transport = new TServerSocket(addr);
			args_svr = new TServer.Args(transport);
		}
	}
	
	private TServer createServer(TProcessor processor) {		
		if (path.contains("hsha")) {
			return new THsHaServer(
					((THsHaServer.Args)args_svr).processor(processor));
		} else if (path.contains("nonblock")) {
			return new TNonblockingServer(
					((TNonblockingServer.Args)args_svr).processor(processor));
		} else if (path.contains("threadselector")) {
			return new TThreadedSelectorServer(
					((TThreadedSelectorServer.Args)args_svr).processor(processor));
		} else if (path.contains("threadpool")) {				
			return new TThreadPoolServer(
					((TThreadPoolServer.Args)args_svr).processor(processor));
		} else {				
			return new TSimpleServer(((TServer.Args)args_svr).processor(processor));
		}
	}
	private TServer createServer() {		
		if (path.contains("hsha")) {
			return new THsHaServer((THsHaServer.Args)args_svr);
		} else if (path.contains("nonblock")) {
			return new TNonblockingServer((TNonblockingServer.Args)args_svr);
		} else if (path.contains("threadselector")) {
			return new TThreadedSelectorServer((TThreadedSelectorServer.Args)args_svr);
		} else if (path.contains("threadpool")) {				
			return new TThreadPoolServer((TThreadPoolServer.Args)args_svr);
		} else {				
			return new TSimpleServer((TServer.Args)args_svr);
		}
	}
	
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option == UA_URI) {
			if (path != null && path.length() > 3)
				transport = path.substring(1).toLowerCase();
			else
				path = "";

		} else if (option == UA_TYPE) {
			type = (Integer) value;
			join = "join".equalsIgnoreCase(this.get_fragment());
			if (type != SERVER) {
				this.clientTransport = getTransport();
				protocol = new TBinaryProtocol(clientTransport);
			}else{
				try {
					initServerTransport();
				} catch (TTransportException e) {
					e.printStackTrace();
				}
			}
		} else if (option == SPROCESSOR) {
			server = createServer((TProcessor) value);
		}

		return this;
	}

	public TProtocol get_client_protocol() {
		return protocol;
	}

	@Override
	public void teardown() {
		if (server != null)
			server.stop();
		else if (clientTransport != null)
			clientTransport.close();

	}

	@Override
	public void startup() {
		if (args_svr != null) {
			if(server==null)
				server=this.createServer();
			
			if (join)
				server.serve();
			else
				new Thread() {
					public void run() {
						server.serve();
					}
				}.start();
		} else if (clientTransport != null)
			try {
				clientTransport.open();
			} catch (TTransportException e) {
				e.printStackTrace();
			}
	}

	@Override
	public String get_default_schema() {
		return "thrift";
	}

	@Override
	public boolean is_ready() {
		if (server != null) {
			long deadline = new Date().getTime() + 30 * 1000;
			while (!server.isServing() && new Date().getTime() < deadline) {
				log.debug("{} not ready!", this);
				sleep(1000);
			}
			return server.isServing();
		}
		if (clientTransport != null)
			return clientTransport.isOpen();

		return false;
	}

}
