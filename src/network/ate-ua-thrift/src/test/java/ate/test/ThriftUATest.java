package ate.test;

import java.lang.reflect.Method;

import org.apache.thrift.TException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.AbstractURIHandler;
import ate.testcase.CsUATestcase;

import ate.ua.AbstractPacketUAImp;
import ate.ua.thrift.HelloWorldHandler;
import ate.ua.thrift.ThriftUA;
import ate.ua.thrift.HelloWorld;

public class ThriftUATest extends CsUATestcase {

	/**
	 * 
	 * namespace java ate.ua.thrift
     * service HelloWorld{
     * void ping(1: i32 length) 
     * } 
     * 
     * > thrift --gen java helloworld.thrift
	 */
	@Test 
	public void base_sync(){
		log.info(STEP + " base_sync:");
		log.info(STEP + ".1 init:");
		
		ThriftUA svr = new ThriftUA();
		svr.build(AbstractURIHandler.UA_URI, "thrift://"+sip+":5005/")
		   .build(AbstractPacketUAImp.UA_TYPE, AbstractPacketUAImp.SERVER)	
		   .build(ThriftUA.SPROCESSOR, new HelloWorld.Processor(new HelloWorldHandler()));
		svr.startup();
		assertTrue(svr.is_ready());
		
		ThriftUA clt = new ThriftUA();
		clt.build(AbstractURIHandler.UA_URI, "thrift://"+sip+":5005/")
		   .build(AbstractPacketUAImp.UA_TYPE, AbstractPacketUAImp.CLIENT);
		clt.startup();		
		
		HelloWorld.Client helloworld= new HelloWorld.Client(clt.get_client_protocol());
		try {
			helloworld.ping(1234);
		} catch (TException e) {
			e.printStackTrace();
		}
	}
	@Test 
	public void base_async(){
		log.info(STEP + " base_async:");
		log.info(STEP + ".1 init:");
		
		ThriftUA svr = new ThriftUA();
		svr.build(AbstractURIHandler.UA_URI, "thrift://"+sip+":5005/")
		   .build(AbstractPacketUAImp.UA_TYPE, AbstractPacketUAImp.SERVER)	
		   .build(ThriftUA.SPROCESSOR, new HelloWorld.Processor(new HelloWorldHandler()));
		svr.startup();
		assertTrue(svr.is_ready());
		
		ThriftUA clt = new ThriftUA();
		clt.build(AbstractURIHandler.UA_URI, "thrift://"+sip+":5005/")
		   .build(AbstractPacketUAImp.UA_TYPE, AbstractPacketUAImp.CLIENT);
		clt.startup();		
		
		HelloWorld.Client helloworld= new HelloWorld.Client(clt.get_client_protocol());
		try {
			helloworld.ping(1234);
		} catch (TException e) {
			e.printStackTrace();
		}
	}
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
