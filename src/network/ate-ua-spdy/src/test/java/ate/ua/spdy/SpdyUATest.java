package ate.ua.spdy;

import java.nio.ByteBuffer;
import java.util.Random;

import io.netty.buffer.*;
import io.netty.handler.codec.spdy.*;
import io.netty.handler.codec.spdy.SpdyFrame;

import org.testng.annotations.Test;

import ate.testcase.CsUATestcase;
import ate.ua.netty.*;


public class SpdyUATest extends CsUATestcase{
    @Test
    public void send_rcv() {
        SpdyUA svr = create_server("spdy://"+siip,new SpdyUA());
        assertTrue(svr.get_io_handler() instanceof UAServer);

        SpdyUA clt = create_client("spdy://"+sip,new SpdyUA());
        assertTrue(clt.get_io_handler() instanceof UAClient);
        start_all_ua();

        STEP("client send message:");
        clt.send_message(this.createFrames(3));
        SpdyFrame req = svr.recv_message();
        log.info(req.toString());
        assertTrue(req instanceof DefaultSpdyDataFrame);
        req = svr.recv_message();
        log.info(req.toString());
        assertTrue(req instanceof DefaultSpdySynStreamFrame);
        req = svr.recv_message();
        log.info(req.toString());
        assertTrue(req instanceof DefaultSpdySynReplyFrame);
        req = svr.recv_message();
        log.info(req.toString());
        assertTrue(req instanceof DefaultSpdyRstStreamFrame);
        req = svr.recv_message();
        log.info(req.toString());
        assertTrue(req instanceof DefaultSpdySettingsFrame);
        req = svr.recv_message();
        log.info(req.toString());
        assertTrue(req instanceof DefaultSpdyPingFrame);
        req = svr.recv_message();
        log.info(req.toString());
        assertTrue(req instanceof DefaultSpdyGoAwayFrame);
        req = svr.recv_message();
        log.info(req.toString());
        assertTrue(req instanceof DefaultSpdyHeadersFrame);
        req = svr.recv_message();
        log.info(req.toString());
        assertTrue(req instanceof DefaultSpdyWindowUpdateFrame);

    }
    private static CompositeByteBuf createFrames(int version) {
        int length = version < 3 ? 1176 : 1174;
        CompositeByteBuf frames = Unpooled.compositeBuffer(length);
        Random random = new Random();
        DefaultSpdyDataFrame ff;
        // SPDY UNKNOWN Frame
        frames.writeByte(0x80);
        frames.writeByte(version);
        frames.writeShort(0xFFFF);
        frames.writeByte(0xFF);
        frames.writeMedium(4);
        frames.writeInt(random.nextInt());

        // SPDY NOOP Frame
        frames.writeByte(0x80);
        frames.writeByte(version);
        frames.writeShort(5);
        frames.writeInt(0);

        // SPDY Data Frame
        frames.writeInt(random.nextInt() & 0x7FFFFFFF | 0x01);
        frames.writeByte(0x01);
        frames.writeMedium(1024);
        for (int i = 0; i < 256; i ++) {
            frames.writeInt(random.nextInt());
        }

        // SPDY SYN_STREAM Frame
        frames.writeByte(0x80);
        frames.writeByte(version);
        frames.writeShort(1);
        frames.writeByte(0x03);
        if (version < 3) {
            frames.writeMedium(12);
        } else {
            frames.writeMedium(10);
        }
        frames.writeInt(random.nextInt() & 0x7FFFFFFF | 0x01);
        frames.writeInt(random.nextInt() & 0x7FFFFFFF);
        frames.writeShort(0x8000);
        if (version < 3) {
            frames.writeShort(0);
        }

        // SPDY SYN_REPLY Frame
        frames.writeByte(0x80);
        frames.writeByte(version);
        frames.writeShort(2);
        frames.writeByte(0x01);
        if (version < 3) {
            frames.writeMedium(8);
        } else {
            frames.writeMedium(4);
        }
        frames.writeInt(random.nextInt() & 0x7FFFFFFF | 0x01);
        if (version < 3) {
            frames.writeInt(0);
        }

        // SPDY RST_STREAM Frame
        frames.writeByte(0x80);
        frames.writeByte(version);
        frames.writeShort(3);
        frames.writeInt(8);
        frames.writeInt(random.nextInt() & 0x7FFFFFFF | 0x01);
        frames.writeInt(random.nextInt() | 0x01);

        // SPDY SETTINGS Frame
        frames.writeByte(0x80);
        frames.writeByte(version);
        frames.writeShort(4);
        frames.writeByte(0x01);
        frames.writeMedium(12);
        frames.writeInt(1);
        if (version < 3) {
            frames.writeMedium(random.nextInt());
            frames.writeByte(0x03);
        } else {
            frames.writeByte(0x03);
            frames.writeMedium(random.nextInt());
        }
        frames.writeInt(random.nextInt());

        // SPDY PING Frame
        frames.writeByte(0x80);
        frames.writeByte(version);
        frames.writeShort(6);
        frames.writeInt(4);
        frames.writeInt(random.nextInt());

        // SPDY GOAWAY Frame
        frames.writeByte(0x80);
        frames.writeByte(version);
        frames.writeShort(7);
        if (version < 3) {
            frames.writeInt(4);
        } else {
            frames.writeInt(8);
        }
        frames.writeInt(random.nextInt() & 0x7FFFFFFF);
        if (version >= 3) {
            frames.writeInt(random.nextInt() | 0x01);
        }

        // SPDY HEADERS Frame
        frames.writeByte(0x80);
        frames.writeByte(version);
        frames.writeShort(8);
        frames.writeByte(0x01);
        frames.writeMedium(4);
        frames.writeInt(random.nextInt() & 0x7FFFFFFF | 0x01);

        // SPDY WINDOW_UPDATE Frame
        frames.writeByte(0x80);
        frames.writeByte(version);
        frames.writeShort(9);
        frames.writeInt(8);
        frames.writeInt(random.nextInt() & 0x7FFFFFFF | 0x01);
        frames.writeInt(random.nextInt() & 0x7FFFFFFF | 0x01);

        return frames;
    }
}
