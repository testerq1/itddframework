package ate.ua.spdy;

import io.netty.channel.ChannelHandler;
import io.netty.handler.codec.spdy.SpdyFrame;
import io.netty.handler.codec.spdy.SpdyFrameDecoder;
import io.netty.handler.codec.spdy.SpdyFrameEncoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import ate.AbstractURIHandler;
import ate.ua.UAOption;
import ate.ua.netty.AFilterFactory;
import ate.ua.netty.ANettyUAImp;

public class SpdyUA extends ANettyUAImp<SpdyFrame> {
    public static final UAOption<Integer> SPDY_VER = new UAOption<Integer>(
            "SPDY_VER");
    // public static final UAOption<Integer> SPDY_GZIP = new UAOption<Integer>(
    // "SPDY_GZIP");
    private int version=3;
    private boolean gzip;

    // private int maxHeaderSize=8192;

    @Override
    public AbstractURIHandler build(UAOption option, Object value) {
        super.build(option, value);

        if (option == UA_TYPE) {
            this.transport = "tcp";
            if (this.port <= 0) {
                port = 80;
            }
        } else if (option == SPDY_VER) {
            version = (Integer) value;
        }

        return this;
    }

    @Override
    public boolean message_matched(SpdyFrame msg, HashMap hm) {
        return true;
    }

    @Override
    public String get_default_schema() {
        return "spdy";
    }

    @Override
    public void startup() {
        AFilterFactory factory = null;

        factory = new AFilterFactory() {
            @Override
            public List<ChannelHandler> create_filters() {
                return new ArrayList<ChannelHandler>(Arrays.asList(
                        new SpdyFrameDecoder(version), new SpdyFrameEncoder(
                                version)));
            }
        };

        set_filters_factory(factory);
        super.startup();
    }
}
