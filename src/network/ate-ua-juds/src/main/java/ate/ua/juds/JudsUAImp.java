package ate.ua.juds;

import java.io.IOException;
import java.util.HashMap;

import com.etsy.net.JUDS;
import com.etsy.net.UnixDomainSocket;
import com.etsy.net.UnixDomainSocketClient;
import com.etsy.net.UnixDomainSocketServer;

import ate.AbstractURIHandler;
import ate.ua.AbstractPacketUAImp;
import ate.ua.AtePacket;
import ate.ua.RunFailureException;
import ate.ua.UAOption;
import ate.ua.mina.UAClient;
import ate.ua.mina.UAServer;

/**
 * Unix Domain Socket Wrapper，只能在linux下使用
 * https://github.com/mcfunley/juds/blob/master/test/TestUnixDomainSocketServer.java
 * 
 * 20140829：未完成，需要扩展UAServer和UAClient
 *  
 * @author ravi huang
 *
 * @param <T>
 */
public class JudsUAImp<T> extends AbstractPacketUAImp<T,AtePacket>{
    UnixDomainSocket io;
    int socket_type;
    
    @Override
    public AbstractURIHandler build(UAOption option, Object value) {
        super.build(option, value);
        
        if (option == UA_URI) {
            if (path != null && path.length() > 3)
                transport = path.substring(1);
            else
                transport="udp";
            socket_type=transport.equalsIgnoreCase("tcp")?JUDS.SOCK_STREAM:JUDS.SOCK_DGRAM;

        } else if (option == UA_TYPE) {
            try {
                if (type == SERVER)
                    this.io = new UnixDomainSocketServer("server", socket_type,3 );
                else
                    this.io = new UnixDomainSocketClient("client",socket_type);
                
            } catch (IOException e) {
                throw new RunFailureException(e);
            }            
        }
        return this;
    }
    
    @Override
    public void send_message(T msg) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean message_matched(T msg, HashMap<String, Object> hm) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void teardown() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void startup() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public String get_default_schema() {
         return "juds";
    }

    @Override
    public boolean is_ready() {
        return false;
    }

}
