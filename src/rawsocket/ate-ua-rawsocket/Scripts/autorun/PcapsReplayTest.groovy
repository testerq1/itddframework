import java.lang.reflect.Method;
import org.testng.annotations.*
import org.testng.annotations.Test
import ate.testcase.*
import ate.ua.raw.RawSocketUA;
import ate.ua.ssh.SSHUA;
import common.*

public class PcapsReplayTest extends RawUATestcase {
	def DUT_IP=  arg("DUT.SSH_IP")
	def DUT_PORT=arg("DUT.SSH_PORT")
	def SFLAG=   arg("DUT.SUCCESS")
	def PROMPT=  arg("DUT.PROMPT")
	def AUTH=    arg("DUT.AUTH")
	def map;

	def findPcapFiles(path) {
		def result=new ArrayList();
		new File(path).eachFileMatch(~/.*\.pcap/) {
			def tmp=it.name.split("_")
			if(map.get(tmp[0])!=null)
				result.add([it] as File[])
		}
		//log.info "Total Pcap Files:"+result.size()
		return result.toArray();
	}

	@DataProvider(name="Data_TestStep")
	public Object[][] Data_TestStep(){
		log.info "Data_TestStep:"
		def path="pcap"

		map=get_map("$path/map.properties")
		return findPcapFiles(path)
	}

	@Test
	public void test(){
		STEP("Expect Test:")
		def rtn="""
Hardcode version: 13.05.27.13.05.27.50.05.50.05
Hardcode Internal Version: 
        chip 1 - A.F.M.I = 50.08.05.05, Y.M.D.H = 13.05.27.15
        chip 2 - A.F.M.I = 50.00.05.05, Y.M.D.H = 13.05.27.15
        chip 3 - A.F.M.I = 50.00.05.05, Y.M.D.H = 13.05.27.15
Firmware Version: 1.3.0.4------Oct 16 2013 14:46:40 
"""
		println expect.start(rtn,"Hardcode version")         //true
		println expect.start(rtn,"HardcodeVersion")          //true
		println expect.end(rtn,"14:46:40")                   //true

		println expect.contains(rtn,"Firmware Version:")     //true
		println expect.contains(rtn,"Firmware Version1:")    //false
		def gs=expect.groups(rtn, "Hardcode\\sversion:\\s(\\S+)");
		println "Hardcode:"+gs[1]                            //13.05.27.13.05.27.50.05.50.05
		gs=expect.groups(rtn, "Firmware\\sVersion:\\s(\\d.*\\d)-");
		println "Firmware:"+gs[1]                            //1.3.0.4

		gs = expect.groups(rtn, "Hardcode\\sversion:\\s(\\S+)\\n(.*\\n){4}Firmware\\sVersion:\\s(\\d.*\\d)-.*");
		println "HardcodeVersion:${gs[1]}"                   //13.05.27.13.05.27.50.05.50.05
		println "FirmwareVersion:${gs[3]}"                   //1.3.0.4
	}

	//@Test(dataProvider="Data_TestStep")
	public void replay_by_ip(file) {
		STEP("初始化UA");
		def uac = create_ua("raw://$ciip");
		def uas = create_ua("raw://$siip");
		//def dut = create_ua("ssh://$AUTH@$DUT_IP:$DUT_PORT/?line_mode=r");
		uas.enable_capture(true)

		STEP("创建一个过滤器jitter,模拟网络抖动");
		def filter = create_io_filter(RawSocketUA.FILTER.Jitter);
		filter.set_min(20);
		filter.set_max(50);

		uac.add_first_io_filter(filter);
		uas.add_first_io_filter(filter);

		this.start_all_ua();
		//dut.login(PROMPT,SFLAG)

		//dut.send_line("show version")
		//def rtn=dut.recv_message();
		//println rtn

		STEP("创建一个回放器，按照IP地址运行");
		def replay = create_replayer(uac, uas);
		def handler = create_handler(replay,RawUATestcase.REPLAY.DIRECTIP4);

		STEP("回放文件:$file");
		def tmp=file.name.split("_")
		def app_name=tmp[0]
		def pcap_sip=tmp[1]
		def app_id=map.getProperty(app_name)

		handler.set_sip(pcap_sip);
		replay.replay(file.absolutePath, handler);
		assertTrue(handler.get_seq()> 0);


		/*STEP("查询DUT，确认识别出了该APP：$app_name");
		 STEP("DUT清除所有Session，配置阻挡该应用:$app_name");
		 STEP(" 重新回放文件:");
		 replay.reset()
		 replay.replay(file.absolutePath, handler);
		 assertTrue(handler.get_seq() > 0);
		 STEP(" 确认该app Session被阻挡：");
		 println "uas rcvd: "+uas.get_rcvd_count()
		 println "uas snd: "+uas.get_snd_count()
		 println "uac snd: "+uac.get_snd_count()
		 assertTrue("没有阻挡该应用的Session： ${uac.get_snd_count()} ${uas.get_rcvd_count()}",
		 (uas.get_rcvd_count()-uas.get_snd_count()) < uac.get_snd_count()/2);
		 STEP("DUT上恢复配置，清除Session：");*/
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		// ;
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}