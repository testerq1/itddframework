import java.lang.reflect.Method;

import org.testng.annotations.*

import ate.testcase.*
import ate.ua.raw.HA;
import ate.ua.raw.PcapReader;
import ate.ua.raw.RawSocketUA;
import ate.ua.raw.UAPcapPacket;
import ate.util.X2X0;
import common.*

public class ReplayTest extends RawUATestcase {
    @Test
    public void read(){
        log.info("dump");
        def ua = create_ua("raw://$IP0");        
        def pr=ua.create_pcap_reader("smtp_172.16.5.211_p.pcap");
        pr.set_matcher(HA.SIP,"172.16.5.211");
        pr.start_read();
        
        UAPcapPacket o=null;
        while((o=pr.get_packet())!=null){
            println o.is_uac()?"uac":"uas";
            println o.toString();
            println X2X0.bs2chs(o.get_payload(2),"");
            println X2X0.bs2chs(o.get_payload(3),"");
            System.out.println(X2X0.bs2chs(o.get_payload(4),""));
            o.build(HA.SIP, "1.1.1.1");
            o.build(HA.DIP, "2.2.2.2");
            o.build(HA.SMAC, "01.01.01.01.01.01");
            o.build(HA.DMAC, "02.02.02.02.02.02");
            System.out.println(o.toString());
            System.out.println(X2X0.bs2chs(o.get_payload(2),""));
            System.out.println(X2X0.bs2chs(o.get_payload(3),""));
            System.out.println(X2X0.bs2chs(o.get_payload(4),""));
            break;
        }
        
        pr.close();
        System.out.println("closed");
    }
	@Test
	void dump() {
		log.info("dump");
		def ua = create_ua("raw://$IP0");
		ua.set_is_record(false);
		ua.set_file_dumper("test.cap");
		ua.enable_capture(true);
		this.start_all_ua();

		sleep(5000);

		assertTrue(ua.rcvd() > 10);		
	}

	@Test
	public void replay() {
		STEP("初始化UA");
		def uac = create_ua("raw://$IP1");
		def uas = create_ua("raw://$IP2");
		
		STEP("设置dump文件(可选)");
		uac.set_file_dumper("test2.cap");
		uas.set_file_dumper("test1.cap");
		//使能抓包功能
		uac.enable_capture(true);
		uas.enable_capture(true);
		
		STEP("创建一个jitter过滤器，模拟网络抖动");
		def filter = create_io_filter(RawSocketUA.FILTER.Jitter);
		filter.set_min(200);
		filter.set_max(500);

		uac.add_first_io_filter(filter);
		uas.add_first_io_filter(filter);
		
		STEP("启动所有UA");
		this.start_all_ua();
		
		STEP("创建一个回放器，按照MAC地址运行");
		def replay = create_replayer(uac, uas);
		def handler = create_handler(replay,RawUATestcase.REPLAY.DIRECTMAC);
		
		//指定uac的mac地址，pcap文件中该mac地址的抓包都会从uac发送
		handler.set_smac("e005c575d40e");

		replay.replay("smtp_172.16.5.211_p.pcap", handler);
		
		assertTrue(handler.get_seq() > 0);
	}
	@Test
	public void replay_by_ip() {
		STEP("初始化UA");
		def uac = create_ua("raw://$IP1");
		def uas = create_ua("raw://$IP2");
		
		STEP("创建一个jitter过滤器，模拟网络抖动");
		def filter = create_io_filter(RawSocketUA.FILTER.Jitter);
		filter.set_min(200);
		filter.set_max(500);

		uac.add_first_io_filter(filter);
		uas.add_first_io_filter(filter);
		
		STEP("启动所有UA");
		this.start_all_ua();
		
		STEP("创建一个回放器，按照MAC地址运行");
		def replay = create_replayer(uac, uas);
		def handler = create_handler(replay,RawUATestcase.REPLAY.DIRECTIP4);
		
		STEP("回放文件");
		def filepath="smtp_172.16.5.211_p.pcap";
		def pcap=new File(filepath);
		if(pcap.isFile()){
			handler.set_sip(pcap.getName().split("_")[1]);
			replay.replay(filepath, handler);
			assertTrue(handler.get_seq() > 0);
		}
	}
	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		// ;
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();		
		LHOST.rm("test.cap");
		LHOST.rm("test1.cap");
		LHOST.rm("test2.cap");
	}
}
