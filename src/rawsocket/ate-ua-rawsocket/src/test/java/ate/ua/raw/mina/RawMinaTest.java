package ate.ua.raw.mina;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;

import org.apache.mina.transport.rawsocket.DefaultRawSessionConfig;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.Global;
import ate.testcase.RawUATestcase;
import ate.ua.mina.JitterIoFilter;
import ate.ua.mina.UAClient;
import ate.ua.mina.UAServer;
import ate.ua.raw.RawCsUA;

public class RawMinaTest extends RawUATestcase{
    
    //不支持并行
    @Test(invocationCount=1,singleThreaded=true)
    public void send_rcv() {
        StringBuffer  sb;
        String transport="raw";
        STEP("create ua: " + transport);
        
        RawCsUA svr = create_server("rawm://"+IP2+":35000/" + transport,new RawCsUA());        
        RawCsUA clt = create_client("rawm://"+IP1+":35000/" + transport,"rawm://"+IP2+":35000/" + transport,new RawCsUA());
        svr.set_dump_file("sr.cap");
        start_all_ua();        
        
        clt.add_last_iofilter("jitter", new JitterIoFilter());
        svr.add_last_iofilter("jitter", new JitterIoFilter());
        STEP("client send message:");
        clt.send_message("abcdefg".getBytes());
        String req = new String (svr.recv_message()).trim();
        assertTrue(req.equals("abcdefg"),req);

        STEP("server send 1234567:");
        svr.send_message("1234567".getBytes());
        req = new String (clt.recv_message()).trim();
        assertTrue(req.equals("1234567"));

        STEP("reconnect all ua:");
        reconnect_all_ua();
        clt.send_message("hhhhhhh".getBytes());

        req = new String (svr.recv_message()).trim();
        assertTrue(req != null && req.equals("hhhhhhh"));

        STEP("server send 1111111:");
        svr.send_message("1111111".getBytes());
        req = new String (clt.recv_message()).trim();
        assertTrue(req.equals("1111111"));
    }  
    
    @Test(invocationCount=1,singleThreaded=true)
    public void vlan() {
        //DefaultRawSessionConfig.verbose=true;
        String transport="raw";
        STEP("create ua: " + transport);
        RawCsUA svr = create_server("rawm://"+IP2+":35001/" + transport,new RawCsUA());        
        RawCsUA clt = create_client("rawm://"+IP1+":35001/" + transport,"rawm://"+IP2+":35001/" + transport,new RawCsUA());
        svr.set_dump_file("vlan.cap");
        svr.add_vlan(100).add_vlan(1000);
        clt.add_vlan(100).add_vlan(1000);
        start_all_ua();
        
        clt.add_last_iofilter("jitter", new JitterIoFilter());
        svr.add_last_iofilter("jitter", new JitterIoFilter());   
        
        STEP("client send message:");
        clt.send_message("abcdefg".getBytes());
        String req = new String (svr.recv_message()).trim();
        assertTrue(req.equals("abcdefg"),req);

        STEP("server send 1234567:");
        svr.send_message("1234567".getBytes());
        req = new String (clt.recv_message()).trim();
        assertTrue(req.equals("1234567"));

        STEP("reconnect all ua:");
        reconnect_all_ua();
        clt.send_message("hhhhhhh".getBytes());

        req = new String (svr.recv_message()).trim();
        assertTrue(req != null && req.equals("hhhhhhh"));

        STEP("server send 1111111:");
        svr.send_message("1111111".getBytes());
        req = new String (clt.recv_message()).trim();
        assertTrue(req.equals("1111111"));

    }  
    @Test(invocationCount=1,singleThreaded=true)
    public void port_is_ethType() {
        String transport="raw";
        STEP("create ua: " + transport);
        RawCsUA svr = create_server("rawm://"+IP2+":35001/" + transport,new RawCsUA());        
        RawCsUA clt = create_client("rawm://"+IP1+":35001/" + transport,"rawm://"+IP2+":35001/" + transport,new RawCsUA());
        svr.set_dump_file("port.cap");
        start_all_ua();        
        
        clt.add_last_iofilter("jitter", new JitterIoFilter());
        svr.add_last_iofilter("jitter", new JitterIoFilter());
        STEP("client send message:");
        clt.send_message("abcdefg".getBytes());
        String req = new String (svr.recv_message()).trim();
        assertTrue(req.equals("abcdefg"),req);

        STEP("server send 1234567:");
        svr.send_message("1234567".getBytes());
        req = new String (clt.recv_message()).trim();
        assertTrue(req.equals("1234567"));

        STEP("reconnect all ua:");
        reconnect_all_ua();
        clt.send_message("hhhhhhh".getBytes());

        req = new String (svr.recv_message()).trim();
        assertTrue(req != null && req.equals("hhhhhhh"));

        STEP("server send 1111111:");
        svr.send_message("1111111".getBytes());
        req = new String (clt.recv_message()).trim();
        assertTrue(req.equals("1111111"));

    } 
    @BeforeMethod
    public void beforeMethod(Method m) {
        super.BeforeMethod(m);
    }
    
    @AfterMethod
    public void afterMethod(Method m) {
        super.shutdown_all_ua();
        super.AfterMethod(m);
    }
}
