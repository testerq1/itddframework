package ate.test;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.File;
import java.lang.reflect.Method;
import java.util.Hashtable;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.testcase.RawUATestcase;
import ate.ua.raw.HA;
import ate.ua.raw.IRawIoFilter;
import ate.ua.raw.PcapReader;
import ate.ua.raw.ReplayerFactory;
import ate.ua.raw.ReplayerFactory.DirectIp4Replay;
import ate.ua.raw.ReplayerFactory.DirectMacReplay;
import ate.ua.raw.RawSocketUA;
import ate.ua.raw.JitterIoFilter;
import ate.ua.raw.UAPcapPacket;
import ate.util.X2X0;

public class RawSocketUATest extends RawUATestcase {
	@Test
	public void read(){
		PcapReader pr=new PcapReader("smtp_172.16.5.211_p.pcap");
		pr.set_matcher(HA.SIP,"172.16.5.211");
		pr.start_read();
		Hashtable ht;
		UAPcapPacket o=null;
		while((o=pr.get_packet())!=null){			
			System.out.println(o.is_uac()?"uac":"uas");
			System.out.println(o.toString());				
			System.out.println(X2X0.bs2chs(o.get_payload(2),""));
			System.out.println(X2X0.bs2chs(o.get_payload(3),""));
			System.out.println(X2X0.bs2chs(o.get_payload(4),""));
			o.build(HA.SIP, "1.1.1.1");			
            o.build(HA.DIP, "2.2.2.2");
            o.build(HA.SMAC, "01.01.01.01.01.01");         
            o.build(HA.DMAC, "02.02.02.02.02.02");
            System.out.println(o.toString());   
            System.out.println(X2X0.bs2chs(o.get_payload(2),""));
            System.out.println(X2X0.bs2chs(o.get_bytes(),""));
            System.out.println(X2X0.bs2chs(o.get_payload(3),""));
            System.out.println(X2X0.bs2chs(o.get_payload(4),""));            
			break;
		}
		
		pr.close();
		System.out.println("closed");		
	}
	
	@Test
	public void dump() {
		log.info("dump");
		RawSocketUA ua = (RawSocketUA) create_ua("raw://"+IP0);
		ua.set_is_record(false);
		ua.set_file_dumper("test.cap");
		ua.enable_capture(true);
		this.start_all_ua();

		sleep(5000);

		assertTrue(ua.rcvd() > 10);
		assertTrue(LHOST.rm("test.cap"));
	}

	@Test
	public void replay() {
		log.info("replay");
		log.info(STEP+".1 初始化UA");
		RawSocketUA uac = create_ua("raw://"+IP1);
		RawSocketUA uas = create_ua("raw://"+IP2);
		
		log.info(STEP+".2 设置dump文件(可选)");
		uac.set_file_dumper("test2.cap");
		uas.set_file_dumper("test1.cap");
		//使能抓包功能
		uac.enable_capture(true);
		uas.enable_capture(true);
		
		log.info(STEP+".3 创建一个jitter过滤器，模拟网络抖动");
		IRawIoFilter filter = create_io_filter(RawSocketUA.FILTER.Jitter);
		((JitterIoFilter) filter).set_min(200);
		((JitterIoFilter) filter).set_max(500);

		uac.add_first_io_filter(filter);
		uas.add_first_io_filter(filter);
		
		log.info(STEP+".4 启动所有UA");
		this.start_all_ua();
		
		log.info(STEP+".5 创建一个回放器，按照MAC地址运行");
		ReplayerFactory replay = create_replayer(uac, uas);
		DirectMacReplay handler = (DirectMacReplay) create_handler(replay,
				REPLAY.DIRECTMAC);
		
		//指定uac的mac地址，pcap文件中该mac地址的抓包都会从uac发送
		handler.set_smac("8ca0480002a1");

		replay.replay("smtp_172.16.5.211_p.pcap", handler);
		assertTrue(handler.get_seq() > 0);
	}
	@Test
	public void replay_by_ip() {
		log.info("replay_by_ip");
		log.info(STEP+".1 初始化UA");
		RawSocketUA uac = (RawSocketUA) create_ua("raw://"+IP1);
		RawSocketUA uas = (RawSocketUA) create_ua("raw://"+IP2);
		
		log.info(STEP+".2 创建一个jitter过滤器，模拟网络抖动");
		IRawIoFilter filter = create_io_filter(RawSocketUA.FILTER.Jitter);
		((JitterIoFilter) filter).set_min(200);
		((JitterIoFilter) filter).set_max(500);

		uac.add_first_io_filter(filter);
		uas.add_first_io_filter(filter);
		
		log.info(STEP+".3 启动所有UA");
		this.start_all_ua();
		
		log.info(STEP+".4 创建一个回放器，按照MAC地址运行");
		ReplayerFactory replay = create_replayer(uac, uas);
		DirectIp4Replay handler = (DirectIp4Replay) create_handler(replay,
				REPLAY.DIRECTIP4);
		
		log.info(STEP+".5 回放文件");
		String filepath="smtp_172.16.5.211_p.pcap";
		File pcap=new File(filepath);
		if(pcap.isFile()){
			handler.set_sip(pcap.getName().split("_")[1]);
			replay.replay(filepath, handler);
			assertTrue(handler.get_seq() > 0);
		}		
	}
	// void replay(){
	// log.info("replay");
	// log.info(System.getProperty("java.library.path"));
	// List<String> l=RawSocketUA.getRealNicIP();
	// assertTrue(l.size()==2);
	//
	// uac=(RawSocketUA)create_client("raw://"+l.get(1));
	// uas=(RawSocketUA)this.create_server("raw://"+l.get(0));
	// source_mac_filte(false);
	// enable_record(false);
	// this.start_all_ua();
	//
	// assertTrue(replay_file("d:/replay_1.pcap")>100);
	// System.out.println(uac.get_sent_count());
	// System.out.println(uas.get_sent_count());
	// // assertTrue(uac.get_rcvd_count()==0);
	// // assertTrue(uas.get_rcvd_count()==0);
	// }

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
		LHOST.rm("test.cap");
		LHOST.rm("test1.cap");
		LHOST.rm("test2.cap");
	}
}
