package ate.test;

import org.jnetpcap.packet.JPacket;
import org.jnetpcap.packet.JRegistry;
import org.jnetpcap.packet.RegistryHeaderErrors;
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.lan.IEEE802dot1q;
import org.jnetpcap.protocol.network.GRE;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.testng.annotations.*;

import ate.testcase.RawUATestcase;
import ate.ua.raw.RawSocketUA;

public class IpAttactTest extends RawUATestcase {
    @Test
    public void tcp_sync(){
        RawSocketUA uac = create_ua("raw://"+IP1);
        RawSocketUA uas = create_ua("raw://"+IP2);
        this.start_all_ua();
        
        JPacket pkt=uac.create_packet("01a0f4082f7700a0f4082f7788b80001009100000000618186801a4745446576696365463635302f4c4c4e3024474f2467636230318103009c4082184745446576696365463635302f4c4c4e3024474f4f534531830b463635305f474f4f5345318408386ebbf34217280a85010186010a8701008801018901008a0108ab208301008403030000830100840303000083010084030300008301008403030000");
        uas.send_message(pkt);
        STEP("done");
        //pkt.setByte(index, value);
    }
    @Test
    public void ip(){
        Ethernet h=new Ethernet();
        h.destination(new byte[]{1,1,1,1,1,1});
        h.source(new byte[]{1,1,1,1,1,2});
        h.type(0x8100);
        log.info(h.getHeader().length+"");
    }
    
    @Test
    public void gre(){
        try {
            JRegistry.register(GRE.class);
        } catch (RegistryHeaderErrors e) {
            e.printStackTrace();
        }
        RawSocketUA uac = create_ua("raw://"+IP1);
        JPacket pkt=uac.create_packet("c20537c9f102c2023797000008004500007c001c0000ff2f3b12ac101902ac100f020000080045000064001e0000ff013a24c0a80002c0a80004080003d4000600000000000000187a58abcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcdabcd");
        Ip4 ip=pkt.getHeader(new Ip4());
        GRE gre=pkt.getHeader(new GRE());

        assertTrue(gre.type()==0x0800);

        for(int i=0;i<pkt.getHeaderCount();i++)
            log.info("{}",pkt.getHeaderIdByIndex(i));
    }
    
    @Test
    public void qinq(){
        RawSocketUA uac = create_ua("raw://"+IP1);
        JPacket pkt=uac.create_packet("ffffffffffffca030db4001c81000064810000c808060001080006040001ca030db4001cc0a802c8000000000000c0a802fe0000000000000000000000000000");
        IEEE802dot1q qq=pkt.getHeaderByIndex(1, new IEEE802dot1q());
        
        IEEE802dot1q qq2=pkt.getHeaderByIndex(2, new IEEE802dot1q());
        log.info("{} {}",qq,qq2);
//        for(int i=0;i<pkt.getHeaderCount();i++)
//            log.info("{}",pkt.getHeaderByIndex(i, header));
    }

}
