package ate.test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.channels.spi.SelectorProvider;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.testng.annotations.Test;

import ate.testcase.Testcase;

public class NioTest extends Testcase {
    int port = 11111;
    String host = "127.0.0.1";

    @Test
    public void basic() {
        Selector selector = null;
        ServerSocketChannel server = null;
        try {
            selector = Selector.open();
            selector = Selector.open();
            selector = Selector.open();
            selector = Selector.open();
            server = ServerSocketChannel.open();
            server.socket().bind(new InetSocketAddress(port));
            server.configureBlocking(false);
            server.register(selector, SelectionKey.OP_ACCEPT);
            while (true) {
                selector.select();
                for (Iterator<SelectionKey> i = selector.selectedKeys()
                        .iterator(); i.hasNext();) {
                    SelectionKey key = i.next();
                    i.remove();
                    if (key.isConnectable()) {
                        ((SocketChannel) key.channel()).finishConnect();
                    }
                    if (key.isAcceptable()) {
                        // accept connection
                        SocketChannel client = server.accept();
                        client.configureBlocking(false);
                        client.socket().setTcpNoDelay(true);
                        client.register(selector, SelectionKey.OP_READ);
                    }
                    if (key.isReadable()) {
                        SocketChannel nextReady = (SocketChannel) key.channel();
                        // Accept the date request and send back the date string
                        Socket s = nextReady.socket();
                        // Write the current time to the socket
                        int len = s.getInputStream().available();
                        byte[] bs = new byte[len];
                        s.getInputStream().read(bs);
                        log.info(new String(bs));

                    }
                }
            }
        } catch (Throwable e) {
            throw new RuntimeException("Server failure: " + e.getMessage());
        } finally {
            try {
                selector.close();
                server.socket().close();
                server.close();
            } catch (Exception e) {
                // do nothing - server failed
            }
        }
    }

    @Test
    public void acceptConnections() throws Exception {
        // Selector for incoming time requests
        Selector acceptSelector = SelectorProvider.provider().openSelector();

        // Create a new server socket and set to non blocking mode
        ServerSocketChannel ssc = ServerSocketChannel.open();
        ssc.configureBlocking(false);

        // Bind the server socket to the local host and port

        InetAddress lh = InetAddress.getLocalHost();
        InetSocketAddress isa = new InetSocketAddress("127.0.0.1", port);
        ssc.socket().bind(isa);

        // Register accepts on the server socket with the selector. This
        // step tells the selector that the socket wants to be put on the
        // ready list when accept operations occur, so allowing multiplexed
        // non-blocking I/O to take place.
        SelectionKey acceptKey = ssc.register(acceptSelector,
                SelectionKey.OP_ACCEPT);

        int keysAdded = 0;

        // Here's where everything happens. The select method will
        // return when any operations registered above have occurred, the
        // thread has been interrupted, etc.
        while ((keysAdded = acceptSelector.select()) > 0) {
            // Someone is ready for I/O, get the ready keys
            Set readyKeys = acceptSelector.selectedKeys();
            Iterator i = readyKeys.iterator();

            // Walk through the ready keys collection and process date requests.
            while (i.hasNext()) {
                SelectionKey sk = (SelectionKey) i.next();
                i.remove();
                // The key indexes into the selector so you
                // can retrieve the socket that's ready for I/O
                ServerSocketChannel nextReady = (ServerSocketChannel) sk
                        .channel();
                // Accept the date request and send back the date string
                Socket s = nextReady.accept().socket();
                // Write the current time to the socket
                PrintWriter out = new PrintWriter(s.getOutputStream(), true);
                Date now = new Date();
                out.println(now);
                out.close();
            }
        }
    }
    @Test
    void talkToServer() {

        try {
            SocketChannel mySocket = SocketChannel.open();

            // non blocking
            mySocket.configureBlocking(false);

            // connect to a running server
            mySocket.connect(new InetSocketAddress(InetAddress.getLocalHost(),
                    port));

            // get a selector
            Selector selector = Selector.open();

            // register the client socket with "connect operation" to the
            // selector
            mySocket.register(selector, SelectionKey.OP_CONNECT);

            // select() blocks until something happens on the underlying socket
            while (selector.select() > 0) {

                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> it = keys.iterator();

                while (it.hasNext()) {

                    SelectionKey key = it.next();

                    SocketChannel myChannel = (SocketChannel) key.channel();

                    it.remove();

                    if (key.isConnectable()) {
                        if (myChannel.isConnectionPending()) {
                            myChannel.finishConnect();
                            System.out
                                    .println("Connection was pending but now is finiehed connecting.");
                        }

                        ByteBuffer bb = null;

                        while (true) {
                            bb = ByteBuffer.wrap(new String("I am Client : ")
                                    .getBytes());
                            myChannel.write(bb);
                            bb.clear();
                            synchronized (this) {
                                sleep(3000);
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
