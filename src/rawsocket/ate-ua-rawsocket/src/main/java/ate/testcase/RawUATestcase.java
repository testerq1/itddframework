package ate.testcase;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.mina.transport.rawsocket.DefaultRawSessionConfig;
import org.apache.mina.transport.rawsocket.EthNE;
import org.jnetpcap.packet.PcapPacketHandler;

import ate.ua.raw.CountIoFilter;
import ate.ua.raw.IRawIoFilter;
import ate.ua.raw.JitterIoFilter;
import ate.ua.raw.ReplayerFactory;
import ate.ua.raw.ReplayerFactory.DestAddrReplay;
import ate.ua.raw.ReplayerFactory.DirectIp4Replay;
import ate.ua.raw.ReplayerFactory.DirectMacReplay;
import ate.ua.raw.ReplayerFactory.Ip4ExReplay;
import ate.ua.raw.RawSocketUA;
import ate.ua.raw.RawSocketUA.FILTER;
import ate.util.GenData0;

/**
 * The Class RawSocketTestcase.
 */
public class RawUATestcase extends CsUATestcase {
	//管理IP
	protected String IP0=arg("HOST.NIC0_IP");
	//iptables 内网 IP
	protected String IP1=arg("HOST.NIC1_IP");
	//iptables 内网 IP
	protected String IP2=arg("HOST.NIC2_IP");
    
	//层
	final public static int L2=2;
    final public static int L3=3;
    final public static int L4=4;
    
	public enum REPLAY {
		DIRECTIP4, DIRECTMAC, IP4EX, DEST
	}

	/** The curr packet. */
	private HierarchicalINIConfiguration pktTemplate;

	public PcapPacketHandler create_handler(ReplayerFactory replayer, REPLAY mode) {
		
		switch (mode) {
		case DIRECTMAC:
			return new DirectMacReplay(replayer);
		case DIRECTIP4:
			return new DirectIp4Replay(replayer);
		case IP4EX:
			return new Ip4ExReplay(replayer);
		case DEST:
			return new DestAddrReplay(replayer);
		}
		return null;
	}

	public IRawIoFilter create_io_filter(FILTER type) {
		switch (type) {
		case Jitter:
			return new JitterIoFilter();
		case Counter:
			return new CountIoFilter();
		}
		return null;
	}

	public ReplayerFactory create_replayer(RawSocketUA uac, RawSocketUA uas) {
		return new ReplayerFactory(uac, uas);
	}

	// public void send_bytes(byte[] msg) {
	// this.io.send_bytes(msg);
	// }

	public static EthNE get_ne_by_ip(String ip) {
		return RawSocketUA.get_ne_by_ip(ip);
	}
	
	public static DefaultRawSessionConfig new_config(){
	    return new DefaultRawSessionConfig();
	}
}
