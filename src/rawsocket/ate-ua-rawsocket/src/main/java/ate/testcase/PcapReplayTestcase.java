package ate.testcase;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Arrays;
import java.util.HashMap;

import org.jnetpcap.Pcap;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.network.Arp;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;

import ate.ua.raw.PEth;
import ate.ua.raw.RawSocketUA;
import ate.ua.raw.jnetpcap.PcapFileParser;
import ate.util.X2X0;

/**
 *  暂时不要使用，用RawUATestcase代替
 * @author ravi huang
 *
 */
public class PcapReplayTestcase extends CsUATestcase implements PcapPacketHandler {
	
	protected static RawSocketUA uac;
	protected static RawSocketUA uas;
	protected StringBuilder errors = new StringBuilder();
	protected PcapFileParser p;
	
	// 包遍历序数
	protected int seq = 0;	
	
	public void enable_record(boolean b) {
		uac.set_is_record(b);
		uas.set_is_record(b);
	}

	public String get_errors() {
		return errors.toString();
	}

	@Override
	public void nextPacket(PcapPacket packet, Object user) {
		seq++;
		
	}	
	
	public int replay_file(String filename) {
		log.debug("Start...");
		StringBuilder errbuf = new StringBuilder();
		Pcap pcap = Pcap.openOffline(filename, errbuf);
		if (pcap == null) {
			log.error("Error while opening device for capture: "
					+ errbuf.toString());
			return -1;
		}
		try {
			pcap.loop(-1, this, "File parser");
			return seq;
		} finally {
			log.info("\r\nseq:{} \r\n error: {}", seq,errbuf.toString());
			pcap.close();
		}
	}

	
}
