package ate.ua.raw.jnetpcap;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;

import org.jnetpcap.Pcap;
import org.jnetpcap.packet.JHeader;
import org.jnetpcap.packet.JHeaderPool;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.packet.UnregisteredHeaderException;
import org.jnetpcap.protocol.JProtocol;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;

/**
 * A little test program to see what it would take to use jNetPcap to decode
 * jNetStream read packets. We take a pcap capture file, open it for read-write
 * with memory mapping so that we can peer natively with the underlying data.
 * 
 * @author Mark Bednarczyk
 * @author Sly Technologies, Inc.
 */
public class PcapFileParser {
	ArrayList<PcapPacket> al=new ArrayList<PcapPacket>();	
	JHeaderPool headers = new JHeaderPool();
	
	public PcapFileParser(String filename,int cnt) throws Exception{
		String file = filename;//"e:/a1.pcap";
		StringBuilder errbuf = new StringBuilder();
		Pcap pcap = Pcap.openOffline(file, errbuf);
		if (pcap == null) {
			throw new Exception("Error while opening device for capture: "+ errbuf.toString());			
		}
		PcapPacketHandler<String> jpacketHandler = new PcapPacketHandler<String>() {
			public void nextPacket(PcapPacket packet, String user) {
				al.add(packet);				
			}
		};
		try {			
			pcap.loop(cnt, jpacketHandler, "File parser");
			
		} finally {
			pcap.close();
		}
	}
	public PcapPacket getPacket(int index){
		return al.get(index);
	}
	public JHeader getPayload(int index){
		return getHeader(index,JProtocol.PAYLOAD_ID);		
	}
	public Ip4 getIp4Header(int index){		
		return (Ip4)getHeader(index,JProtocol.IP4_ID);
	}
	public Udp getUdpHeader(int index){
		return (Udp)getHeader(index,JProtocol.UDP_ID);
	}
	public Tcp getTcpHeader(int index){
		return (Tcp)getHeader(index,JProtocol.TCP_ID);
	}
	public JHeader getHeader(int index,int protocol_id){
		if(index>=al.size())
			return null;
		//System.out.println(packet);
		
		PcapPacket packet=al.get(index);
		int count = packet.getHeaderCount();
		
		for (int i = 0; i < count; i++) {

			final int pid = packet.getHeaderIdByIndex(i);
			if (pid != protocol_id) {
				continue;
			}			
			try {
				final JHeader header = headers.getHeader(pid);
				packet.getHeaderByIndex(i, header);							
				//System.out.println(header);				
				return header;
			} catch (UnregisteredHeaderException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	public static void main(String[] args) throws Exception {
		PcapFileParser p =new PcapFileParser("e:/a1.pcap",1);
		Ip4 ip=p.getIp4Header(0);
		Tcp tcp=p.getTcpHeader(0);
		
		System.out.println(ip);
//		System.out.println(udp.destination());
		
		Udp udp=p.getUdpHeader(0);
		System.out.println(udp.destination());
		System.out.println(udp.source());
		byte[] bs=p.getPayload(0).getHeader();
		System.out.println();
	}

}
