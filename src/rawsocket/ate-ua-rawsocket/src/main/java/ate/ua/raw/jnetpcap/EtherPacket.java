package ate.ua.raw.jnetpcap;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.jnetpcap.packet.JHeader;
import org.jnetpcap.packet.JMemoryPacket;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.protocol.JProtocol;
import org.jnetpcap.protocol.lan.Ethernet;

import ate.rm.dev.NetParas;
import ate.util.GenData0;

public class EtherPacket implements IPacket{
	private final static Ethernet type=new Ethernet();
	protected JPacket packet;
	public Ethernet eth; 
	public final static int HLEN=14;
	protected NetParas paras;
	
	EtherPacket(String pkt){
		packet = new JMemoryPacket(JProtocol.ETHERNET_ID,pkt);
		build();
	}
	EtherPacket(JPacket pkt){
		this.packet=pkt;
		build();
	}
	public JPacket build(){		
		eth= packet.getHeader(new Ethernet());	
		if(eth==null)
			throw new IllegalArgumentException("Not Ethernet packet!");
		paras.src_mac=eth.source();
		paras.dst_mac=eth.destination();
		return packet;
	}
	public JPacket setParas(NetParas paras){
		this.paras=paras;
		eth.source(paras.src_mac);
		eth.destination(paras.dst_mac);
		return packet;
	}
	public JPacket payload(byte[] payload){		
		JMemoryPacket bigPacket= new JMemoryPacket(packet.size()+payload.length);
		bigPacket.transferFrom(packet.getByteArray(0,packet.size()));	
		bigPacket.setByteArray(packet.size(), payload);	
		
		packet=bigPacket;
		packet.scan(JProtocol.ETHERNET_ID);
		return build();
	}
	
	public JPacket payload(int len){
		return payload(GenData0.randomBytes(len));
	}
	
	@Override
	public boolean hasResponse(JPacket packet) {		
		packet.scan(JProtocol.ETHERNET_ID);
		Ethernet eth= packet.getHeader(new Ethernet());
		
		return eth!=null&&
				isEqual(eth.source(),this.eth.destination())&&
				isEqual(eth.destination(),this.eth.source());
	}
	protected boolean isEqual(byte[] a,byte[] b){
		if(a.length!=b.length)
			return false;
		for(int i=0;i<a.length;i++){
			if(a[i]!=b[i])
				return false;
		}
		return true;
	}
	@Override
	public JHeader type() {		
		return EtherPacket.type;
	}

	@Override
	public byte[] getBytes() {
		return packet.getByteArray(0, packet.size());
	}
	@Override
	public String toString(){
		if(packet==null)
			return null;
		return packet.toString();
	}
}
