package ate.ua.raw;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ate.util.GenData0;

public abstract class AbatractRawIoFilter implements IRawIoFilter{
	public enum TYPE{
		Jitter
	} 
	
	public static IRawIoFilter create_io_filter(TYPE type){
		switch(type){
		case Jitter:
			return new JitterFilter();
		}
		return null;
	}
	
	public static class JitterFilter extends AbatractRawIoFilter{
		private int max=10;
		private int min=0;
		
		@Override
		public void packet_send(Object packet) {
			try {
				Thread.currentThread().sleep(GenData0.random(min, max));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
		}		
	}

	@Override
	public void packet_send(Object packet) {
		
	}

	@Override
	public void packet_rcvd(Object packet) {
		
	}
}
