package ate.ua.raw;

import ate.util.GenData0;

public class JitterIoFilter extends AbatractRawIoFilter {
	private int max = 10;
	private int min = 0;

	@Override
	public void packet_send(Object packet) {
		try {
			Thread.currentThread().sleep(GenData0.random(min, max));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void set_max(int max) {
		this.min = min;
	}

	public void set_min(int min) {
		this.min = min;
	}

}
