package ate.ua.raw.mina;

import java.io.ByteArrayOutputStream;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.apache.mina.transport.rawsocket.DefaultRawSessionConfig;

import ate.ua.AbstractCustomPacket;
import ate.ua.mina.NoExceptionProtocolCodecFilter;

public abstract class ARawCustomUAImp<T extends AbstractCustomPacket<T>> extends ARawMinaUAImp<T> {
    protected DefaultRawSessionConfig config=new DefaultRawSessionConfig();    
    int frameType=config.DEFAULT_FRAME_TYPE;
    class RawSocketDecoder extends CumulativeProtocolDecoder {
        
        @Override
        protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out)
                throws Exception {
            if (in.remaining() <= 0) {              
                return false;
            }
            log.debug("{} in size = {} ",ARawCustomUAImp.this,in.remaining());           
            if(is_fuzzy()){
                byte[] bs=new byte[in.remaining()];
                in.get(bs); 
                out.write(bs);
            }else{
                T s=decodePacket(in);                                        
                out.write(s);                
            }
            return true;
        }
    }
    
    class RawSocketEncoder extends ProtocolEncoderAdapter {
        @Override
        public void encode(IoSession session, Object message, ProtocolEncoderOutput out)
                throws Exception {          
            
            if (message instanceof AbstractCustomPacket){
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ((T)message).get_packet().encodePacket(bos);                
                out.write(IoBuffer.wrap(bos.toByteArray()));               
            }else if (message instanceof IoBuffer){
                out.write((IoBuffer)message);           
            }else if (message instanceof byte[]){
                out.write(IoBuffer.wrap((byte[])message));                
            }else
                log.error("message type wrong: {}",message);
        }
    }  
    
    public abstract T decodePacket(IoBuffer buf);
    
    @Override
    protected void add_default_filter() {        
        add_last_iofilter(this.get_default_schema(), new NoExceptionProtocolCodecFilter(new RawSocketEncoder(),
                new RawSocketDecoder()));
        
    }
}
