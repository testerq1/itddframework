package ate.ua.raw;

import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.testcase.Testcase;

public abstract class AbstractReplayer implements PcapPacketHandler{	
	protected static Logger log = LoggerFactory.getLogger(AbstractReplayer.class);
	protected final static int BOTH_SIDE=0;
	protected final static int SOURCE_SIDE=1;
	protected final static int DEST_SIDE=2;
	
	/**回放模式 */
	protected int replay_mode=BOTH_SIDE;
	
	protected boolean debug=false;
	
	private int barrier=0;
	protected int seq = 0;
	
	protected boolean packet_by_packet=false;
	protected boolean pv_packet=false;
	
	public void set_packet_by_packet(boolean b){
		this.packet_by_packet=b;
	}
	
	private void enable_debug(boolean b){
		this.debug=b;
	}
	
	public int get_seq() {
		return seq;
	}
	
	@Override
	public void nextPacket(PcapPacket packet, Object user) {
		if(debug&&(barrier--)==0){
			Testcase.wait_yes();
			enable_debug(false);
		}
		seq++;
	}
	
	public void wait_barrier(int cnt) {
		enable_debug(true);
		barrier=cnt;		
	}
	
	public void reset(){
		this.seq=0;
		enable_debug(false);
		barrier=0;
	}
	
	public void set_replay_mode(int mode){
		this.replay_mode=mode;
	}
	
	

}
