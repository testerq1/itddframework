package ate.ua.raw.mina;

import java.nio.ByteBuffer;

import org.apache.mina.transport.rawsocket.ARawLayer;
import org.apache.mina.transport.rawsocket.EthAddress;
import org.apache.mina.transport.rawsocket.IRawLayer;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.network.Ip6;

public class Ip6Layer extends ARawLayer<Ip6>{

    @Override
    public Ip6 getHeader(JPacket pkt, int id) {
        return pkt.getHeaderByIndex(id, new Ip6());
    }

    @Override
    public int length() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getType() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void build(IRawLayer upLayer) {
        // TODO Auto-generated method stub
        
    }

}
