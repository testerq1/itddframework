package ate.ua.raw.mina;

import org.apache.mina.transport.rawsocket.ARawLayer;
import org.apache.mina.transport.rawsocket.IRawLayer;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.protocol.network.Ip4;
import org.joou.*;
import static org.joou.Unsigned.*;

import ate.util.X2X0;

public class Ip4Layer extends ARawLayer<Ip4>{
    byte version=4;
    UByte hlength=ubyte(20);
    byte dscp=0;
    UShort tlength=ushort(0);
    UShort id=ushort(0);
    byte flags=0;
    short fragoffset=0;
    UByte ttl=ubyte(255);
    
    byte[] sip;
    byte[] dip;
    byte protocol;
    
    public Ip4Layer(String sip,String dip,int protocol){
        this.sip=X2X0.ip2b(sip);
        this.dip=X2X0.ip2b(dip);
        this.protocol=(byte)protocol;
    }
    
    @Override
    public Ip4 getHeader(JPacket pkt, int id) {
        return pkt.getHeaderByIndex(id, new Ip4());
    }

    @Override
    public int length() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getType() {
        return 0x0800;
    }

    @Override
    public void build(IRawLayer upLayer) {
        // TODO Auto-generated method stub
        
    }

}
