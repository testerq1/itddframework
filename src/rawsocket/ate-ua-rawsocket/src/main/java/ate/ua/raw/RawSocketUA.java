package ate.ua.raw;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.mina.transport.rawsocket.EthNE;
import org.apache.mina.transport.rawsocket.EthAddress;
import org.jnetpcap.Pcap;
import org.jnetpcap.PcapAddr;
import org.jnetpcap.PcapIf;
import org.jnetpcap.nio.JBuffer;
import org.jnetpcap.packet.JMemoryPacket;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.protocol.JProtocol;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;

import ate.AbstractURIHandler;
import ate.Global;
import ate.ua.AbstractPacketUAImp;
import ate.ua.UAOption;
import ate.ua.raw.jnetpcap.IPacket;
import ate.util.X2X0;

/**
 * Raw Socket封装类，实现抓包及响应功能 RawSocketUA ua = new RawSocketUA();
 * ua.init(raw://172.16.5.1/gateway);//172.16.5.1为Gateway
 * 
 * ua.init(raw://172.16.5.1/);//172.16.5.1为本地IP地址
 * 
 * ua.set_filter("");//see docs/ethereal-tcpdump.pdf
 * 
 */
public class RawSocketUA extends AbstractPacketUAImp<JPacket, JPacket> {
	public enum FILTER {
		Jitter,Counter
	}

	
	public static final UAOption<byte[]> DMAC = new UAOption<byte[]>("DMAC");
	public static final UAOption<Short> ETYPE = new UAOption<Short>("ETYPE");

	public static final UAOption<byte[]> SMAC = new UAOption<byte[]>("SMAC");
	
	/**
	 * 根据一个hex字符串创建包
	 * create_packet("01a0f4082f7700a0f4082f7788b80001009100000000618186801a4745446576696365463635302f4c4c4e3024474f2467636230318103009c4082184745446576696365463635302f4c4c4e3024474f4f534531830b463635305f474f4f5345318408386ebbf34217280a85010186010a8701008801018901008a0108ab208301008403030000830100840303000083010084030300008301008403030000")
	 * @param pkt
	 * @return
	 */
	public static JPacket create_packet(String pkt) {
		try {
			return new JMemoryPacket(JProtocol.ETHERNET_ID, pkt);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 根据一个byte数组创建包
	 * @param pkt
	 * @return
	 */
	public static JPacket create_packet(byte[] pkt) {
        try {
            return new JMemoryPacket(JProtocol.ETHERNET_ID, pkt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
	
	/**
	 * 根据一个java.nio.ByteBuffer创建包
	 * @param pkt
	 * @return
	 */
    public static JPacket create_packet(ByteBuffer pkt) {
        try {
            return new JMemoryPacket(JProtocol.ETHERNET_ID, pkt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
	
	public static PcapReader create_pcap_reader(String fname){
	    
	    return new PcapReader(fname);
	}
	
	public static JPacket create_tcp_packet(int sourceIP, int sourcePort,
			int destIP, int destPort, byte[] payload) {

		JPacket packet = new JMemoryPacket(JProtocol.ETHERNET_ID,
				" 001801bf 6adc0025 4bb7afec 08004500 "
						+ " 0041a983 40004006 d69ac0a8 00342f8c "
						+ " ca30c3ef 008f2e80 11f52ea8 4b578018 "
						+ " ffffa6ea 00000101 080a152e ef03002a "
						+ " 2c943538 322e3430 204e4f4f 500d0a");

		packet.getHeader(new Ip4());
		Tcp tcp = packet.getHeader(new Tcp());
		tcp.source(sourcePort);
		tcp.destination(destPort);

		int payloadStartOffset = tcp.getGapOffset();

		ByteBuffer buf = ByteBuffer.allocate(payloadStartOffset
				+ payload.length);

		byte[] bytes = buf.array();

		for (int i = payloadStartOffset; i < payloadStartOffset
				+ payload.length; ++i)
			bytes[i] = payload[i - payloadStartOffset];

		packet.transferFrom(bytes);

		return packet;
	}

	public static EthNE get_ne_by_ip(String ip) {
		return EthAddress.get_ne_by_ip(ip);
	}

	RawIOAdapter io;

	private final Map<UAOption<?>, Object> options = new LinkedHashMap<UAOption<?>, Object>();

	public void add_first_io_filter(IRawIoFilter filter) {
		if (io.filters.contains(filter))
			throw new Error("已经存在Filter:" + filter);
		io.filters.addFirst(filter);
	}

	// public void send_bytes(byte[] msg) {
	// this.io.send_bytes(msg);
	// }

	/**
	 * add IoFilter to last of every session
	 * 
	 * @param filter
	 */
	public void add_last_iofilter(IRawIoFilter filter) {
		if (io.filters.contains(filter))
			throw new Error("已经存在Filter:" + filter);
		io.filters.addLast(filter);
	}

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option == UA_URI) {
			io = new RawIOAdapter(this, get_ne_by_ip(host));
			type = UA;
		}

		return this;
	}

	/**
	 * 过滤器，在发包之前，收包之后会调用
	 * @param type
	 * @return
	 * @see RawIOAdapter#filters
	 * @see JitterIoFilter
	 * @see CountIoFilter
	 */
	public IRawIoFilter create_io_filter(FILTER type) {
		switch (type) {
		case Jitter:
			return new JitterIoFilter();
		case Counter:
			return new CountIoFilter();
		}
		return null;
	}

	public void enable_capture(boolean b) {
		io.enable_capture(b);
	}

	@Override
	public String get_default_schema() {
		return "raw";
	}

	@Override
	public boolean is_ready() {
		return io != null && io.is_ready();
	}

	@Override
	public boolean message_matched(JPacket o, HashMap<String, Object> hm) {
		Object pkt = hm.get("request");
		if (pkt == null)
			return true;

		if (!(pkt instanceof IPacket)) {
			log.error("request is not IPacket type!" + pkt);
			return false;
		}
		return ((IPacket) pkt).hasResponse(o);
	}

	public <B> RawSocketUA option(UAOption<B> option, B value) {
		if (option == null)
			throw new NullPointerException("option");
		if (value == null)
			synchronized (options) {
				options.remove(option);
			}
		else
			synchronized (options) {
				options.put(option, value);
			}
		return this;
	}

	public void remove_all_io_filter(IRawIoFilter filter) {
		io.filters.clear();
	}

	public void remove_io_filter(IRawIoFilter filter) {
		if (!io.filters.remove(filter))
			throw new Error("filter instance not exist " + filter);
	}

	public void send_bytes(byte[] msg) {
		io.send_bytes(msg);	
		super.snd();
	}

	public void send_jbuffer(JBuffer buffer) {
		io.send_jbuffer(buffer);
		super.snd();
	}
	
	@Override
	public void send_message(JPacket msg) {
	    if(Global.verbose)
	        log.debug("{} send message {}",this,msg.size());
		io.send_packet(msg);	
		super.snd();
	}
	
	public void set_file_dumper(String filename) {
		io.set_file_dumper(filename);
	}

	public void set_filter(String expression) {
		io.set_filter(expression);
	}

	@Override
	public void startup() {
		io.startup();
	}

	/**
	 * Close.
	 */
	@Override
	public void teardown() {
		io.teardown();
	}

	@Override
	public String toString() {
		return super.toString() + " " + this.host;
	}
}
