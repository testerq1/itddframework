package ate.ua.raw;

/**
 * 在收到包而又不想记录到队列或者dump的时候可以使用该过滤器
 * @author ravi huang
 *
 */
public class CountIoFilter extends AbatractRawIoFilter {
	private int rcvd = 0;
	
	public int get_rcvd() {
		return rcvd;
	}
	
	public int get_send() {
		return snd;
	}

	private int snd = 0;

	@Override
	public void packet_send(Object packet) {
		snd++;
	}
	@Override
	public void packet_rcvd(Object packet) {
		rcvd++;
	}
	
	public void reset(){
		this.rcvd=0;
		this.snd=0;
	}
	
}