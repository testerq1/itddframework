package ate.ua.raw;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import io.netty.util.internal.PlatformDependent;

import java.nio.charset.Charset;
import java.util.concurrent.ConcurrentMap;

import org.jnetpcap.packet.JHeader;
import org.jnetpcap.packet.JMemoryPacket;

import ate.UniqueID;
import ate.rm.dev.Host;
import ate.util.Expect;

/**
 * 
 * @author Administrator
 *
 * @param <T>
 * @param <M>
 */
public abstract class AbstractHeaderAttribution<T> extends UniqueID{
	private static final ConcurrentMap<Integer, Boolean> names = PlatformDependent.newConcurrentHashMap();
	private T def_data;
	protected Charset charset=Host.charset;
	
	public AbstractHeaderAttribution(){	
		super(names);
	}
	
	public abstract T string_to_data(String data);
	
	public abstract void modify_header(JHeader header, T value);
	
	public void payload(PEth packet, JHeader header,String value){
		byte[] payload=(value).getBytes(charset);		
		JMemoryPacket newPacket= new JMemoryPacket(header.getGapOffset()+payload.length);
		newPacket.transferFrom(packet.packet.getByteArray(0,header.getGapOffset()));	
		newPacket.setByteArray(header.getGapOffset(), payload);
		packet.packet=newPacket;	
	}
	
	public void payload(PEth packet, JHeader header,byte[] payload){
		JMemoryPacket newPacket= new JMemoryPacket(header.getGapOffset()+payload.length);
		newPacket.transferFrom(packet.packet.getByteArray(0,header.getGapOffset()));	
		newPacket.setByteArray(header.getGapOffset(), payload);
		packet.packet=newPacket;	
	}
}
