package ate.ua.raw;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.nio.charset.Charset;
import java.util.Arrays;

import org.jnetpcap.packet.JHeader;
import org.jnetpcap.packet.JMemoryPacket;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.protocol.JProtocol;
import org.jnetpcap.protocol.lan.Ethernet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.rm.dev.Host;
import ate.util.Expect;

/**
 * Ethernet Packet
 * 
 * @author ravi huang
 * 
 */
public class PEth {
	protected static Logger log = LoggerFactory.getLogger(PEth.class);
	public final static AbstractHeaderAttribution<byte[]> DMAC = new AbstractHAMac() {
		@Override
		public void modify_header(JHeader header, byte[] value) {
			((Ethernet) header).destination(value);
		}
	};

	public final static AbstractHeaderAttribution<Integer> ETYPE = new AbstractHAInteger() {
		@Override
		public void modify_header(JHeader header, Integer value) {
			((Ethernet) header).type(value);
		}
	};

	public final static AbstractHeaderAttribution<byte[]> SMAC = new AbstractHAMac() {
		@Override
		public void modify_header(JHeader header, byte[] value) {
			((Ethernet) header).source(value);
		}
	};

	private static int[] l2_header = { DMAC.id, ETYPE.id, SMAC.id };
	protected Charset charset = Host.charset;
	Ethernet eth;

	protected JPacket packet;

	public PEth(String s) {
		packet = new JMemoryPacket(JProtocol.ETHERNET_ID, s);
		eth = packet.getHeader(new Ethernet());
	}
	
	public PEth(JPacket pkt) {
		packet = pkt;
		eth = packet.getHeader(new Ethernet());		
	}
	
	public <T> PEth build(AbstractHeaderAttribution<T> header, String value) {
		if (Arrays.binarySearch(l2_header, header.id) > -1)
			header.modify_header(eth, header.string_to_data(value));
		return this;
	}

	/**
	 * build完成后complete收尾
	 * 
	 * @return
	 */
	public JPacket complete() {
		eth.checksum(eth.calculateChecksum());
		packet.scan(JProtocol.ETHERNET_ID);
		return packet;
	}

	public JPacket get_packet() {
		return packet;
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public JHeader get_header() {
		return eth;
	}
	
	public byte[] get_payload() {	    
        return eth.getPayload();
    }
	
	public PEth payload(byte[] payload) {
		JHeader header = get_header();
		JMemoryPacket newPacket = new JMemoryPacket(header.getGapOffset()
				+ payload.length);
		newPacket.transferFrom(packet.getByteArray(0, header.getGapOffset()));
		newPacket.setByteArray(header.getGapOffset(), payload);
		packet = newPacket;
		return this;
	}

	public PEth payload(String value) {
		byte[] payload = (value).getBytes(charset);
		JHeader header = get_header();
		JMemoryPacket newPacket = new JMemoryPacket(header.getGapOffset()
				+ payload.length);
		newPacket.transferFrom(packet.getByteArray(0, header.getGapOffset()));
		newPacket.setByteArray(header.getGapOffset(), payload);
		packet = newPacket;
		return this;
	}

	@Override
	public String toString() {
		return packet.toString();
	}
}
