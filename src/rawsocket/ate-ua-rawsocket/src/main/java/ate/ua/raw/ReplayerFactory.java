package ate.ua.raw;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jnetpcap.Pcap;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.network.Ip4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.util.X2X0;

/**
 * 
 * @author ravi huang
 * 
 */
public final class ReplayerFactory {
	protected static Logger log = LoggerFactory.getLogger(ReplayerFactory.class);
	public static RawSocketUA uac;
	public static RawSocketUA uas;
	StringBuilder errbuf = new StringBuilder();
	AbstractReplayer handler;

	public ReplayerFactory(RawSocketUA uac, RawSocketUA uas) {
		this.uac = uac;
		this.uas = uas;
	}

	/**
	 * 指定源ip,抓包文件中所有源ip为指定ip的包都修改MAC和Dst IP后通过uac发出 所有源ip不是指定ip的包都忽略
	 * 可以用于攻击回放
	 * 
	 * @author ravihuang
	 * 
	 */
	public static class DestAddrReplay extends AbstractReplayer {
		byte[] dip;
		byte[] mdmac;
		byte[] mdip;
		protected ReplayerFactory replay;

		public DestAddrReplay(ReplayerFactory replay) {
			this.replay = replay;
		}

		public void set_mdmac(String mdmac) {
			this.mdmac = X2X0.mac2bs(mdmac);
		}

		public void set_mdip(String mdip) {
			this.mdip = X2X0.ip2b(mdip);
		}

		public void set_dip(String dip) {
			this.mdip = X2X0.ip2b(dip);
		}

		@Override
		public void nextPacket(PcapPacket packet, Object user) {
			super.nextPacket(packet, user);
			Ethernet eth = packet.getHeader(new Ethernet());
			Ip4 iph = packet.getHeader(new Ip4());
			if (iph == null)
				return;
			if (Arrays.equals(iph.destination(), dip)) {
				iph.destination(mdip);
				eth.destination(mdmac);
				iph.checksum(iph.calculateChecksum());
				eth.checksum(eth.calculateChecksum());
				replay.uac.send_message(packet);
			} else{
				log.warn("packet not matched: dst/{} dip/{}",iph.destination(),dip);
				return;
			}
		}
	}

	/**
	 * 指定源mac,抓包文件中所有源mac为指定mac的包都通过uac发出 所有目的mac为指定mac的包都通过uas发出
	 * 
	 * @author ravihuang
	 * 
	 */
	public static class DirectMacReplay extends AbstractReplayer {
		byte[] smac;
		protected ReplayerFactory replay;

		public DirectMacReplay(ReplayerFactory replay) {
			this.replay = replay;
		}

		public void set_smac(String smac) {
			this.smac = X2X0.mac2bs(smac);
		}

		@Override
		public void nextPacket(PcapPacket packet, Object user) {
			super.nextPacket(packet, user);
			Ethernet eth = packet.getHeader(new Ethernet());
			if (Arrays.equals(eth.source(), smac)) {
				replay.uac.send_message(packet);
			} else if (Arrays.equals(eth.destination(), smac)) {
				replay.uas.send_message(packet);
			} else{
				log.warn("packet not matched:src/{} dst/{} smac/{}",eth.source(),eth.destination(),smac);
				return;
			}
		}
	}

	/**
	 * 指定源ip,抓包文件中所有源ip为指定ip的包都通过uac发出 所有目的ip为指定ip的包都通过uas发出
	 * 
	 * @author ravihuang
	 * 
	 */
	public static class DirectIp4Replay extends AbstractReplayer {
		byte[] sip;
		protected ReplayerFactory replay;

		public DirectIp4Replay(ReplayerFactory replay) {
			this.replay = replay;
		}

		public void set_sip(String sip) {
			this.sip = X2X0.ip2b(sip);
		}

		@Override
		public void nextPacket(PcapPacket packet, Object user) {
			super.nextPacket(packet, user);
			Ip4 iph = packet.getHeader(new Ip4());
			if (iph == null)
				return;
			if (Arrays.equals(iph.source(), sip)) {
				replay.uac.send_message(packet);
			} else if (Arrays.equals(iph.destination(), sip)) {
				replay.uas.send_message(packet);
			} else{
				log.warn("packet not matched:src/{} dst/{} sip/{}",iph.source(),iph.destination(),sip);
				return;
			}
		}
	}

	/**
	 * 指定源ip和一组ip地址,抓包文件中所有源ip为指定ip的包都通会过uac修改源IP后发出一组包，
	 * 所有目的ip为指定ip的包都通会过uas修改目的IP后发出一组包，
	 * 
	 * @author ravihuang
	 * 
	 */
	public static class Ip4ExReplay extends DirectIp4Replay {
		List<byte[]> msips = new ArrayList<byte[]>();

		public Ip4ExReplay(ReplayerFactory replay) {
			super(replay);
		}

		public void set_sips(Object sips) {
			if (sips instanceof String[]) {
				for (String tmp : (String[]) sips)
					this.msips.add(X2X0.ip2b(tmp));
			} else if (sips instanceof List) {
				for (String tmp : (List<String>) sips)
					this.msips.add(X2X0.ip2b(tmp));
			} else
				throw new Error("object type is wrong: " + sips);
		}

		@Override
		public void nextPacket(PcapPacket packet, Object user) {
			super.nextPacket(packet, user);
			Ip4 iph = packet.getHeader(new Ip4());
			if (iph == null)
				return;
			if (Arrays.equals(iph.source(), sip)) {
				for (byte[] ip : msips) {
					iph.source(ip);
					iph.checksum(iph.calculateChecksum());
					replay.uac.send_message(packet);
				}

			} else if (Arrays.equals(iph.destination(), sip)) {
				for (byte[] ip : msips) {
					iph.destination(ip);
					iph.checksum(iph.calculateChecksum());
					replay.uas.send_message(packet);
				}
			} else{
				log.warn("packet not matched:src/{} dst/{} sip/{}",iph.source(),iph.destination(),sip);
				return;
			}
		}
	}

	public int replay(String filename, AbstractReplayer handler) {
		log.debug("Start Replay...");		
		this.handler=handler;
		StringBuilder errbuf = new StringBuilder();
		Pcap pcap = Pcap.openOffline(filename, errbuf);
		if (pcap == null) {
			log.error("Error while opening device for capture: "
					+ errbuf.toString());
			return -1;
		}
		try {
			pcap.loop(-1, handler, "File parser");
			return handler.get_seq();
		} finally {
			log.info("seq:{} error: {}", handler.get_seq(), errbuf.toString());
			pcap.close();
		}
	}
	public void reset(){
		handler.reset();
		uac.reset_counter();
		uas.reset_counter();
	}

}
