package ate.ua.raw;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.LinkedList;

import org.apache.mina.transport.rawsocket.EthNE;
import org.apache.mina.transport.rawsocket.RawFileDumper;
import org.jnetpcap.Pcap;
import org.jnetpcap.PcapBpfProgram;
import org.jnetpcap.nio.JBuffer;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.packet.JRegistry;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.Global;
import ate.ua.IUA;
import ate.util.X2X0;

/**
 * 
 * @author ravih
 * 
 */
public class RawIOAdapter implements PcapPacketHandler, Runnable, IUA {
	protected static Logger log = LoggerFactory.getLogger(RawIOAdapter.class);
	public String id;
	private EthNE device = null;

	private RawFileDumper dumper;
	private String filter;
	private int ID_DATALINK;

	private boolean needCapture = false;
	private Pcap pcap;
	private final int promisc = Pcap.MODE_PROMISCUOUS;
	private final int snaplen = 2000;
	private Thread thread;
	protected RawSocketUA ua;
	protected LinkedList<IRawIoFilter> filters = new LinkedList<IRawIoFilter>();
	
	private final int wait = 1;

	RawIOAdapter(RawSocketUA ua, EthNE ne) {
		this.ua = ua;
		device = ne;
		id = X2X0.bs2ip(device.get_ips().get(0));

	}

	public void enable_capture(boolean b) {
		needCapture = b;
	}
	
	@Override
	public boolean is_ready() {
		return pcap != null && (!needCapture || thread != null);
	}

	/*
	 * 
	 * @see
	 * org.jnetpcap.packet.PcapPacketHandler#nextPacket(org.jnetpcap.packet.
	 * PcapPacket, java.lang.Object)
	 */
	@Override
	public void nextPacket(PcapPacket packet, Object user) {
	    if(Global.verbose)
	        log.debug("{} nextPacket size: {}", this, packet.size());
		
		ua.rcvd();
		for (IRawIoFilter filter : filters)
			filter.packet_rcvd(packet);

		if (ua.need_record()) {
			packet.scan(ID_DATALINK);
			ua.add_packet(packet);
		}
		if (dumper != null)
			dumper.dump(packet);
	}

	@Override
	public void run() {
		pcap.loop(-1, this, "captor");
		log.debug("run exit");
	}

	public void send_bytes(byte[] msg) {
		for (IRawIoFilter filter : filters)
			filter.packet_send(msg);
		pcap.sendPacket(msg);
	}

	public void send_jbuffer(JBuffer buffer) {
		for (IRawIoFilter filter : filters)
			filter.packet_send(buffer);
		pcap.sendPacket(buffer);
		
	}

	public void send_packet(JPacket msg) {
		for (IRawIoFilter filter : filters)
			filter.packet_send(msg);
		pcap.sendPacket(msg);
	}

	public void set_file_dumper(String filename) {
		dumper = new RawFileDumper(filename);
	}

	/**
	 * //expression = "tcp dst port 7777 or tcp src port 7777".
	 * 
	 * @param expression
	 *            the new filter
	 */
	public void set_filter(String expression) {
		filter = expression;

	}

	@Override
	public void startup() {
		if (thread != null && thread.isAlive()) {
			log.warn("this capture already start!");
			return;
		}
		StringBuilder errbuf = new StringBuilder();
		pcap = Pcap.openLive(device.get_name(), snaplen, promisc, wait, errbuf);
		ID_DATALINK = JRegistry.mapDLTToId(pcap.datalink());

		if (dumper != null)
			dumper.setup(pcap);

		if (!needCapture)
			return;

		if (filter != null) {
			PcapBpfProgram fp = new PcapBpfProgram();
			int optimize = 0;
			int netmask = 0xFFFFFF00;

			int q = pcap.compile(fp, filter, optimize, netmask);
			if (q != Pcap.OK)
				log.warn("Filter error: " + pcap.getErr());
			log.debug("Applying pcap filter");

			if (pcap.setFilter(fp) != Pcap.OK)
				log.warn("Error durin applying pcap filter");
		}
		thread = new Thread(this);
		thread.start();

	}

	@Override
	public void teardown() {
		if (dumper != null)
			dumper.teardown();
		try {
			pcap.breakloop();
			pcap.close();
		} catch (Exception e) {

		}
		pcap = null;
		thread = null;
	}

	@Override
	public String toString() {
		return id;
	}
}
