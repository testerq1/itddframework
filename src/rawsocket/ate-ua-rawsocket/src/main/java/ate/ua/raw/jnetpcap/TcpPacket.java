package ate.ua.raw.jnetpcap;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.net.InetAddress;
import java.net.UnknownHostException;

import org.jnetpcap.packet.JHeader;
import org.jnetpcap.packet.JMemoryPacket;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.protocol.JProtocol;
import org.jnetpcap.protocol.tcpip.Tcp;
import ate.rm.dev.NetParas;

public class TcpPacket extends Ip4Packet{
	public final static int HLEN_NOOPS=20;	
	public Tcp tcp;	
	private final static Tcp type=new Tcp();	
	
	TcpPacket(String pkt){
		super(pkt);	
	}
	TcpPacket(JPacket pkt){
		super(pkt);
	}
	private byte[] prun2long(byte[] bs){
		byte[] tmp=new byte[(int)Math.ceil(bs.length/4.0)*4];
		for(int i=0;i<tmp.length;i++)
			tmp[i]=0;
		System.arraycopy(bs, 0, tmp, 0, bs.length);
		return tmp;
	}
	
	public JPacket options(byte[] options){
		options=prun2long(options);
		
		int hlen=tcp.hlen()*4;
		int len=packet.size();
		int offset=EtherPacket.HLEN+Ip4Packet.HLEN+hlen;
		byte[] payload=new byte[len-offset];
		System.arraycopy(packet.getByteArray(0,len), offset, payload, 0, payload.length);
		tcp.hlen((HLEN_NOOPS+options.length)/4);
		
		ip.length(ip.length()-hlen+options.length+HLEN_NOOPS);
		
		JMemoryPacket bigPacket= new JMemoryPacket(len-hlen+HLEN_NOOPS+options.length);
		bigPacket.transferFrom(packet.getByteArray(0,packet.size()));	
		bigPacket.setByteArray(EtherPacket.HLEN+Ip4Packet.HLEN+HLEN_NOOPS, options);
		bigPacket.setByteArray(EtherPacket.HLEN+Ip4Packet.HLEN+HLEN_NOOPS+options.length, payload);
		
		packet=bigPacket;
		packet.scan(JProtocol.ETHERNET_ID);		
		return build();
	}
	
	public JPacket build(){
		super.build();
		tcp = packet.getHeader(new Tcp());
		if(tcp==null)
			throw new IllegalArgumentException("Not Tcp packet!");
		tcp.checksum(tcp.calculateChecksum());
		paras.src_port=(short)tcp.source();
		paras.dst_port=(short)tcp.destination();
		paras.ack=tcp.ack();
		paras.seq=tcp.seq();
		return packet;
	}
	
	public void srcIp(String host){
		InetAddress addr;
		try {
			addr = InetAddress.getByName(host);
			ip.source(addr.getAddress());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void srcPort(int port){
		tcp.source(port);		
	}
	public void ack(long l){
		tcp.ack(l);
	}
	public void seq(long l){
		tcp.seq(l);
	}
	public JPacket setParas(NetParas paras){
		super.setParas(paras);		
		tcp.source(paras.src_port);
		tcp.destination(paras.dst_port);
		return packet;
	}
	
	public TcpPacket genAck(String pkt){
		TcpPacket resp =newPacket(pkt);
		resp.setParas(this.paras.swap());
		resp.ack(tcp.seq()+packet.size()-34-tcp.hlen()*4);
		resp.seq(tcp.ack());
		return resp;
	}
	
	public static TcpPacket newPacket(String pkt) {		
		return new TcpPacket(pkt);
	}
	
	public JPacket payload(byte[] payload){		
		tcp.checksum(tcp.calculateChecksum());
		return super.payload(payload);
	}

	public static void main(String[] args){
		TcpPacket tcp=TcpPacket.newPacket("40169ff3 61b58ca0 480011e0 08004500 002c9fa8 00004006 576ec0a8 0101c0a8 016422b8 3039ac87 fbaa0000 00016000 2000f93b 00000204 05b4");

		System.out.println(tcp.build());
		System.out.println(tcp.options(new byte[]{02,04,05,(byte)0xb4,01,01,04,02}));
//		System.out.println(tcp.build(new NetParas()));		
//		System.out.println(tcp.payload(20));
	}

	@Override
	public boolean hasResponse(JPacket packet) {
		if(!super.hasResponse(packet))
			return false;
		Tcp tcp = packet.getHeader(new Tcp());
		
		return tcp!=null&&
				tcp.source()==this.tcp.destination()&&
				tcp.destination()==this.tcp.source();
	}
	
	
	@Override
	public JHeader type() {
		return TcpPacket.type;
	}	
}
