package ate.ua.raw.jnetpcap;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.jnetpcap.packet.JHeader;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.protocol.network.Ip4;

import ate.rm.dev.NetParas;

public class Ip4Packet extends EtherPacket{
	private final static Ip4 type=new Ip4();	
	public final static int HLEN=20;
	public Ip4 ip;
	
	Ip4Packet(String pkt){
		super(pkt);	
	}
	Ip4Packet(JPacket pkt){
		super(pkt);
	}
	public JPacket build(){
		super.build();
		ip = packet.getHeader(new Ip4());	
		if(ip==null)
			throw new IllegalArgumentException("Not Ip packet!");
		ip.checksum(ip.calculateChecksum());
		paras.src_ip=ip.source();
		paras.dst_ip=ip.destination();
		return packet;
	}
	public JPacket payload(byte[] payload){
		int offset=ip.length();
		ip.length(offset+payload.length);
		ip.checksum(ip.calculateChecksum());		
		return super.payload(payload);
	}
	public JPacket setParas(NetParas paras){
		super.setParas(paras);		
		ip.source(paras.src_ip);
		ip.destination(paras.dst_ip);
		return packet;
	}
	@Override
	public boolean hasResponse(JPacket packet) {
		if(!super.hasResponse(packet))
			return false;
		
		Ip4 ip = packet.getHeader(new Ip4());		
		return ip!=null&&
				isEqual(ip.source(),this.ip.destination())&&
				isEqual(ip.destination(),this.ip.source());
	}

	@Override
	public JHeader type() {
		return Ip4Packet.type;
	}

}
