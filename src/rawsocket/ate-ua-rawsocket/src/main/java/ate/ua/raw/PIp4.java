package ate.ua.raw;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Arrays;

import org.jnetpcap.packet.JHeader;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.protocol.network.Ip4;

/**
 * Ip Packet
 * 
 * @author ravi huang
 * 
 */
public class PIp4 extends PEth {
	public final static AbstractHeaderAttribution<byte[]> DIP = new AbstractHAIp() {
		@Override
		public void modify_header(JHeader header, byte[] value) {
			Ip4 ip4 = (Ip4) header;
			ip4.destination(value);
		}
	};

	public final static AbstractHeaderAttribution<byte[]> SIP = new AbstractHAIp() {
		@Override
		public void modify_header(JHeader header, byte[] value) {
			Ip4 ip4 = (Ip4) header;
			ip4.source(value);
		}
	};
	public final static AbstractHeaderAttribution<Integer> PROTOCOL = new AbstractHAInteger() {
		@Override
		public void modify_header(JHeader header, Integer value) {
			Ip4 ip4 = (Ip4) header;
			ip4.type(value);
		}
	};

	Ip4 ip4;
	private static int[] ip4_header = { DIP.id, SIP.id };

	public PIp4(String s) {
		super(s);
		ip4 = packet.getHeader(new Ip4());
	}
	public PIp4(JPacket pkt) {
		super(pkt);
		ip4 = packet.getHeader(new Ip4());
	}
	@Override
	public <T> PEth build(AbstractHeaderAttribution<T> header, String value) {
		if (Arrays.binarySearch(ip4_header, header.id) > -1)			
			header.modify_header(ip4, header.string_to_data(value));
		else
			super.build(header, value);
		return this;
	}

	@Override
	public JPacket complete() {
		ip4.checksum(ip4.calculateChecksum());
		return super.complete();
	}

	@Override
	public JHeader get_header() {
		return ip4;
	}
	
	public byte[] get_payload() {
        return ip4.getPayload();
    }
	
	public JHeader get_eth_header() {
		return this.eth;
	}
}
