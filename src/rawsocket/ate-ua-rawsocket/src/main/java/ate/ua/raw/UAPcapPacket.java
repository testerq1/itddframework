package ate.ua.raw;

import java.nio.ByteBuffer;
import java.util.Arrays;

import org.jnetpcap.packet.PcapPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.util.X2X0;

public class UAPcapPacket {
    protected static Logger log = LoggerFactory.getLogger(UAPcapPacket.class);
   
    private PcapPacket packet;
    private boolean is_uac;
    private int mtu = 1514;

    public UAPcapPacket(PcapPacket packet) {
        this.packet = packet;
    }

    public PcapPacket get_packet() {
        return this.packet;
    }

    public void set_packet(PcapPacket packet) {
        this.packet = packet;
    }

    public boolean is_uac() {
        return is_uac;
    }

    public void set_is_uac(boolean b) {
        this.is_uac = b;
    }

    public void set_mtu(int len) {
        this.mtu = len;
    }

    public byte[] get_bytes(){
        return packet.getByteArray(0, packet.size());
    }
    
    /**
     * 将指定type层及以上层当作payload返回，mtu采用指定值
     * 
     * @param type
     * @return
     * @deprecated
     */
    public byte[] get_payload(HA type) {
        switch (type) {
        case SIP:
        case DIP:
            PIp4 pkt = new PIp4(packet);
            ByteBuffer bf = ByteBuffer.allocate(Math.min(packet.size()
                    - pkt.get_eth_header().size(), mtu));
            packet.transferTo(bf, pkt.get_eth_header().getGapOffset(),
                    bf.capacity());
            return bf.array();
        case SMAC:
        case DMAC:
            break;
        default:
            break;
        }
        return null;
    }
    /**
     * 替换type的值为paras后，返回包的byte数组
     * 
     * @param type HA.SIP，HA.DIP等
     * @param paras 要替换的值
     * @return
     * @deprecated
     * @see #build(HA, String)
     */
    public byte[] get_payload(HA type, String paras) {
        switch (type) {
        case SIP: {
            PIp4 pkt = new PIp4(packet);
            pkt.build(PIp4.SIP, paras).complete();

            ByteBuffer bf = ByteBuffer.allocate(Math.min(packet.size()
                    - pkt.get_eth_header().size(), mtu));
            packet.transferTo(bf, pkt.get_eth_header().getGapOffset(),
                    bf.capacity());
            return bf.array();
        }
        case DIP: {
            PIp4 pkt = new PIp4(packet);
            pkt.build(PIp4.DIP, paras).complete();

            ByteBuffer bf = ByteBuffer.allocate(Math.min(packet.size()
                    - pkt.get_eth_header().size(), mtu));
            packet.transferTo(bf, pkt.get_eth_header().getGapOffset(),
                    bf.capacity());
            return bf.array();
        }
        case SMAC:

            break;
        default:
            break;
        }
        return null;
    }
    /**
     * 获得某层的payload，目前支持2-4层
     * 
     * @param layer 对应网络层数
     * @return
     */
    public byte[] get_payload(int layer){
        PEth pkt = null;
        int size;
        switch (layer) {
        case 2:
            return new PEth(packet).get_payload();            
        case 3:
            return new PIp4(packet).get_payload();
        case 4:
            return new PL44(packet).get_payload();
        default:
            break;
        }
        return null;
    }
    public UAPcapPacket set_payload(int layer,byte[] payload){
        switch (layer) {
        case 2:
            new PEth(packet).payload(payload);            
        case 3:
            new PIp4(packet).payload(payload);
        case 4:
            new PL44(packet).payload(payload);
        default:
            break;
        }
        return this;
    }    
    public byte[] get_payload(int layer,int len){
        byte[] bs=get_payload(layer);
        return Arrays.copyOf(bs, len);
    }
    /**
     * 支持修改8元组织
     * @param type
     * @param paras
     * @return
     */
    public UAPcapPacket build(HA type, String paras) {
        PEth pkt = null;
        switch (type) {
        case SIP:
        case DIP:
        case PROTOCOL:
            pkt = new PIp4(packet);            
            break;
        case ETYPE:    
        case SMAC:
        case DMAC:
            pkt = new PEth(packet);
            break;
        case SPORT:
        case DPORT:
            pkt = new PL44(packet);
            break;
        default:
            break;
        }
        pkt.build(type.header, paras);
        return this;
    }
    
    public String toString(){
        return packet.toHexdump();
    }
}
