package ate.ua.raw.mina;

import java.util.ArrayList;
import java.util.List;

import org.apache.mina.transport.rawsocket.DefaultRawSessionConfig;
import org.apache.mina.transport.rawsocket.EthAddress;
import org.apache.mina.transport.rawsocket.IRawLayer;
import org.apache.mina.transport.rawsocket.LVlan;
import org.jnetpcap.protocol.JProtocol;

import ate.ua.UAOption;
import ate.ua.mina.AMinaIOAdapter;
import ate.ua.mina.AMinaUAImp;


public abstract class ARawMinaUAImp<T> extends AMinaUAImp<T> {
    protected DefaultRawSessionConfig config=new DefaultRawSessionConfig();    
    int frameType=config.DEFAULT_FRAME_TYPE;
    List<IRawLayer> tunnels=new ArrayList();
    
    @Override
    public ARawMinaUAImp build(UAOption option, Object value) {
        super.build(option, value);
        
        if (option == UA_URI) {
            String frame=this.get_query_para("ft");
            if(frame==null){                
               ;
            }else if(frame.equalsIgnoreCase("snap")){
                frameType=JProtocol.IEEE_SNAP_ID;
            }else if(frame.equalsIgnoreCase("802.1q")){
                frameType=JProtocol.IEEE_802DOT1Q_ID;
            }else if(frame.equalsIgnoreCase("802.2")){
                frameType=JProtocol.IEEE_802DOT2_ID;
            }else if(frame.equalsIgnoreCase("802.3")){
                frameType=JProtocol.IEEE_802DOT3_ID;
            }           
        } 
        
        return this;
    }
    
    /**
     * add a vlan to stack
     * 
     * @param p
     * @param cfi
     * @param id
     */
    public ARawMinaUAImp add_vlan(int p,int cfi,int id,boolean islength){
        return add_layer(new LVlan((byte)p,(byte)cfi,id));                     
    }
    
    public ARawMinaUAImp add_vlan(int id){
        return this.add_vlan(id,false);
    }
    
    public ARawMinaUAImp add_vlan(int id,boolean islength){
        return this.add_vlan(0, 0, id,islength);
    }
    
    public ARawMinaUAImp add_layer(IRawLayer layer){
        tunnels.add(layer);
        return this;
    }
    
    public DefaultRawSessionConfig get_config(){
        return config;
    }
    
    @Override
    public EthAddress getLocalBindAddress() {   
        if(this.type==SERVER)
            return getTargetAddress();
            
        EthAddress tmp=EthAddress.get_addr_by_ip(cltLocalUri.getHost());
        if(cltLocalUri.getPort()==-1)
            tmp.setEthType(get_port());
        else
            tmp.setEthType(cltLocalUri.getPort());
        
        tmp.setFrameType(frameType);
        return tmp;
    }
    
    @Override
    public EthAddress getTargetAddress() {
        EthAddress tmp=EthAddress.get_addr_by_ip(this.get_host());
        tmp.setEthType(this.get_port());        
        tmp.setFrameType(frameType);
        return tmp;
    }
    
    public List<IRawLayer> getTunnels(){
        return this.tunnels;
    }
    
    public ARawMinaUAImp<T> set_config(DefaultRawSessionConfig cfg){
        this.config=cfg;
        return this;
    }
    
    @Override
    /**
     * just dump matched packets
     */
    public void set_dump_file(String filename) {        
        config.setDumperFilename(filename);
    }

    @Override
    protected AMinaIOAdapter<T> create_io(int type){        
        if (type == SERVER){            
            return new RawUAServer(this);
        }else{
            return new RawUAClient(this);
        }
    }
}
