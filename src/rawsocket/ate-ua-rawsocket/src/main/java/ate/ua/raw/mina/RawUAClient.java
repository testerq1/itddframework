package ate.ua.raw.mina;

import java.util.concurrent.Executors;

import org.apache.mina.core.service.IoConnector;
import org.apache.mina.transport.rawsocket.DefaultRawSessionConfig;
import org.apache.mina.transport.rawsocket.EthAddress;
import org.apache.mina.transport.rawsocket.RawIoConnector;
import org.apache.mina.transport.rawsocket.RawProcessor;

import ate.ua.RunFailureException;
import ate.ua.mina.UAClient;

public class RawUAClient extends UAClient{
    RawProcessor processor;
    /**
     * 需要指定 config.setLocalMacAddr()
     * @param ua
     */
    public RawUAClient(ARawMinaUAImp ua) {
        super(ua);   
    }
    
    @Override
    protected IoConnector create_connector(String transport){
        if(transport.equalsIgnoreCase("raw")){    
            processor=new RawProcessor(Executors.newCachedThreadPool()); 
            DefaultRawSessionConfig config=((ARawMinaUAImp)ua).get_config();
            config.setLocalEthAddr((EthAddress)ua.getLocalBindAddress());
            
            RawIoConnector connector=new RawIoConnector(((ARawMinaUAImp)ua).get_config(),processor);
            connector.setTunnels(((ARawMinaUAImp)ua).getTunnels());
            return connector;     
        }
        return super.create_connector(transport);
    } 
    
    @Override
    public void reconnect() {
        if(ua.get_transport().equalsIgnoreCase("raw")){
            //do nothing
            return;
        }
        super.reconnect();
    }
    
    @Override
    public String toString() {
        return "RawUAClient";
    }
    
    @Override
    public void teardown() {
        this.processor.dispose();
        super.teardown();
    }
}
