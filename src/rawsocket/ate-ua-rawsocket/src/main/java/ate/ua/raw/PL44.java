package ate.ua.raw;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Arrays;

import org.jnetpcap.packet.JHeader;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;

/**
 * Udp or Tcp packet
 * 
 * @author ravi huang
 * 
 */
public class PL44 extends PIp4 {

	public final static AbstractHeaderAttribution<Integer> DPORT = new AbstractHAInteger() {
		@Override
		public void modify_header(JHeader header, Integer value) {
			if (header instanceof Tcp)
				((Tcp) header).destination(value);
			else
				((Udp) header).destination(value);
		}
	};
	public final static AbstractHeaderAttribution<Integer> SPORT = new AbstractHAInteger() {
		@Override
		public void modify_header(JHeader header, Integer value) {
			if (header instanceof Tcp)
				((Tcp) header).source(value);
			else
				((Udp) header).source(value);
		}
	};
	JHeader l4h;
	private static int[] l4_header = { DPORT.id, SPORT.id };

	public PL44(String s) {
		super(s);
		if (ip4.type() == 6) {
			l4h = packet.getHeader(new Tcp());
		} else if (ip4.type() == 17) {
			l4h = packet.getHeader(new Udp());
		}
	}
	
	public PL44(JPacket pkt) {
        super(pkt);
        if (ip4.type() == 6) {
            l4h = packet.getHeader(new Tcp());
        } else if (ip4.type() == 17) {
            l4h = packet.getHeader(new Udp());
        }
    }
	
	@Override
	public <T> PEth build(AbstractHeaderAttribution<T> header, String value) {
		if (Arrays.binarySearch(l4_header, header.id) > -1)
			header.modify_header(l4h, header.string_to_data(value));
		else
			super.build(header, value);
		return this;
	}

	@Override
	public JHeader get_header() {
		return l4h;
	}
	public JHeader get_ip_header() {		
		return this.ip4;
	}
	@Override
    public byte[] get_payload() {
        return l4h.getPayload();
    }
}
