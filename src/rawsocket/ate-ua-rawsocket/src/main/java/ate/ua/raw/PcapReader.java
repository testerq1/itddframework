package ate.ua.raw;

import java.util.Arrays;

import org.jnetpcap.Pcap;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.network.Ip4;

import ate.ua.RunFailureException;
import ate.util.X2X0;

/**
 * 读Pcap文件
 * @author ravi huang
 *
 */
public class PcapReader extends AbstractReplayer implements Runnable {	
	/**
	 * 指定源ip,抓包文件中所有源ip为指定ip的包都通过uac发出 所有目的ip为指定ip的包都通过uas发出
	 * 
	 * @author ravihuang
	 * 
	 */
	public static class MatchIp4 implements PacketMatched {
		byte[] sip;
		
		public MatchIp4(String ip){
			set_sip(ip);
		}
		@Override
		public  int matched(PcapPacket packet) {
			Ip4 iph = packet.getHeader(new Ip4());
			if (iph == null)
				return -1;
			if (Arrays.equals(iph.source(), sip)) {
				log.debug("{} src matched",this);
				return 0;
			} else if (Arrays.equals(iph.destination(), sip)) {
				log.debug("{} dst matched",this);
				return 1;
			}
			log.debug("{} not matched",this);
			return -1;
		}

		public void set_sip(String sip) {
			this.sip = X2X0.ip2b(sip);
		}
	}
	/**
	 * 指定源mac,抓包文件中所有源mac为指定mac的包都通过uac发出 所有目的mac为指定mac的包都通过uas发出
	 * 
	 * @author ravihuang
	 * 
	 */
	public static class MatchSMac implements PacketMatched {
		byte[] smac;
		
		public MatchSMac(String mac){
			set_smac(mac);
		}
		
		@Override
		public int matched(PcapPacket packet) {
			Ethernet eth = packet.getHeader(new Ethernet());
			if (Arrays.equals(eth.source(), smac)) {
				log.debug("{} src matched",this);
				return 0;				
			} else if (Arrays.equals(eth.destination(), smac)) {
				log.debug("{} dst matche",this);
				return 1;				
			} else{
				log.debug("{} not matched",this);
				return -1;				
			}			
		}

		public void set_smac(String smac) {
			this.smac = X2X0.mac2bs(smac);
		}
	}
	public interface PacketMatched{
		int matched(PcapPacket packet);
	}
	
	public String filename;
	
	PacketMatched matcher;
	
	private boolean next=true;
	UAPcapPacket packet;
	
	Pcap pcap;
	
	private boolean pnext;
	
	public PcapReader(String filename){
		this.filename=filename;
		StringBuilder errbuf = new StringBuilder();
		pcap = Pcap.openOffline(filename, errbuf);
		if (pcap == null) {
			throw new Error("Error while opening device for capture: "
					+ errbuf.toString());			
		}
	}
	public void close(){
		if(pcap!=null){
			pcap.breakloop();
			pnext=false;
			pcap.close();
		}
	}
	
	public PacketMatched get_matcher(){
		return this.matcher;
	}
	
	public UAPcapPacket get_packet(){
		next=false;		
		UAPcapPacket tmp=null;
		while(packet==null&&pcap!=null){
			try {
				Thread.currentThread().sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
		}
		tmp=packet;
		packet=null;
		return tmp;
	}
	
	@Override
	public void nextPacket(PcapPacket pkt, Object user) {
		int m=matcher.matched(pkt);
		if(m>-1){
			this.packet=new UAPcapPacket(pkt);
			this.packet.set_is_uac(m==0);
			next=true;
		}
		while(next&&pnext){
			//log.debug("next {}, pnext {}",next,pnext);
			try {
				Thread.currentThread().sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}		
	}
	
	@Override
	public void run() {
		try {
			pcap.loop(-1, this, "File parser");			
		} finally {
			pcap.close();
			pcap=null;
			pnext=false;
		}
	}
	
	public void set_matcher(HA type,String para){
		switch(type){
		case SMAC:
		case DMAC:
			matcher= new MatchSMac(para);
			break;
		case SIP:
		case DIP:
			matcher= new MatchIp4(para);
			break;
		default:
			throw new RunFailureException("type not support now"+type);
		}		
	}
	
	public void start_read(){		
		if(matcher==null)
			throw new Error("matcher not set.");
		pnext=true;
		new Thread(this).start();
		int cnt=30;
		while(packet==null&&pcap!=null&&(cnt--)>0){
			try {
				Thread.currentThread().sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
		}
	}
}
