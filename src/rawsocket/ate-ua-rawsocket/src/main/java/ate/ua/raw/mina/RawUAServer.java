package ate.ua.raw.mina;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.transport.rawsocket.DefaultRawSessionConfig;
import org.apache.mina.transport.rawsocket.EthAddress;
import org.apache.mina.transport.rawsocket.RawIoAcceptor;
import org.apache.mina.transport.rawsocket.RawIoConnector;

import ate.ua.mina.UAServer;


public class RawUAServer extends UAServer {
    /**
     * 需要指定 config.setLocalMacAddr()
     * @param ua
     */    
    public RawUAServer(ARawMinaUAImp csUA) {
        super(csUA);
    }
    @Override
    protected IoAcceptor create_acceptor(String transport){
        if(transport.equalsIgnoreCase("raw")){
            DefaultRawSessionConfig config=((ARawMinaUAImp)ua).get_config();
            config.setLocalEthAddr((EthAddress)ua.getLocalBindAddress());
            
            RawIoAcceptor svc=new RawIoAcceptor(config);
            svc.setTunnels(((ARawMinaUAImp)ua).getTunnels());
            //svc.setLocalAddr((EthAddress)ua.getTargetAddress());
            //svc.setRemoteAddr((EthAddress)ua.getTargetAddress());            
            return svc;
        }
        return super.create_acceptor(transport);
    }  
    @Override
    public String toString() {
        return "RawUAServer";
    }
    @Override
    public void teardown() {
        super.teardown();  
        
        
    }
}
