package ate.ua.raw.jnetpcap;

/*
 * #%L
 * iTDD UA Raw Socket
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.jnetpcap.packet.JBinding;
import org.jnetpcap.packet.JHeader;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.packet.JRegistry;
import org.jnetpcap.packet.annotate.Bind;
import org.jnetpcap.packet.annotate.Dynamic;
import org.jnetpcap.packet.annotate.Field;
import org.jnetpcap.packet.annotate.Header;
import org.jnetpcap.protocol.tcpip.Udp;

/**
 * 
 * @author ravi haung
 *
 */
@Header(length = 12,name="NFE_TO_Reporter",nicname = "W_TCP")
public class NFE2ReportT1 extends JHeader {
	public static final int ID = 999;

	@Bind(to = Udp.class)
	public static boolean bindToUdp(JPacket packet, Udp udp) {
		return udp.source() == 180 && udp.destination() == 13362;
	}

	@Field(offset = 0, length = 1, format = "%d")
	public int f_flag() {		
		return (getUByte(0) & 0x80)>>7;
	}

	@Field(offset = 1, length = 1, format = "%d")
	public int d_flag() {
		return (getUByte(0) & 0x40) >> 6;
	}
	@Dynamic(Field.Property.DESCRIPTION)
	public String d_flagDescription() {
		int pf = d_flag();
		if (pf == 0) {
			return "和会话建立方向相同";
		} 
		if(pf==1){
			return "和会话建立方向相反";
		}
		return null;
	}
	@Field(offset = 2, length = 2, format = "%d")
	public int p_flag() {
		return (getUByte(0) & 0x30) >> 4;
	}
	@Dynamic(Field.Property.DESCRIPTION)
	public String p_flagDescription() {
		int pf = p_flag();
		if (pf == 2) {
			return "udp";
		} 
		if(pf==3){
			return "tcp";
		}
		return null;
	}
	@Field(offset = 4, length = 4, format = "%d")
	public int version() {
		return (getUByte(0) & 0x0f);
	}

	@Field(offset = 10, length = 22, format = "%x")
	public int session_id_index() {
		return (this.getUByte(1) & 0x3f) << 16 + this.getUShort(1);
	}

	@Field(offset = 4 * BYTE, length = 4, format = "%d")
	public int thl() {
		return (getUByte(4) & 0xf0) >> 4;
	}

	@Field(offset = 5 * BYTE, length = 6, format = "%d")
	public int tcp_flag() {
		return (getUByte(5) & 0x3f);
	}

	@Field(offset = 6 * BYTE, length = 16, format = "%x")
	public int tcp_win_size() {
		return getUShort(6);
	}

	@Field(offset = 8 * BYTE, length = 32, format = "%x")
	public long tcp_seq_number() {
		return this.getUInt(8);
	}

	public static void main(String[] args) throws Exception {
		
		final int myId = JRegistry.register(NFE2ReportT1.class);
		JRegistry.addBindings(new JBinding[] { new JBinding.DefaultJBinding(
				NFE2ReportT1.ID, myId) {

			public int getSourceId() {
				return getId();
			}

			private final NFE2ReportT1 my = new NFE2ReportT1();

			public boolean isBound(JPacket packet, int offset) {
				System.out.println();
				return true;
			}

		} });
		PcapFileParser p =new PcapFileParser("e:/a1.pcap",1);
		System.out.println(p.getPacket(0));
	}
}
