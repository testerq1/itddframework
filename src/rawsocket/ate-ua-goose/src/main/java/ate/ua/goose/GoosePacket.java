package ate.ua.goose;

import java.io.OutputStream;

import org.apache.mina.core.buffer.IoBuffer;
import org.hxzon.asn1.core.type.base.BerNode;
import org.hxzon.asn1.goose.GoosePduParser;

import ate.ua.AbstractCustomPacket;

public class GoosePacket extends AbstractCustomPacket<GoosePacket>{
    private static GoosePduParser parser=GoosePduParser.parser;
    BerNode node;
    @Override
    public GoosePacket build(String k, Object v) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GoosePacket get_packet() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void encodePacket(OutputStream out) {
        // TODO Auto-generated method stub
        
    }
    
    public GoosePacket decode_packet(IoBuffer buf){
         node=parser.parseGoose(buf.array(), 0);
         return this;
    }

}
