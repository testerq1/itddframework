package ate.ua.goose;

import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.hxzon.asn1.core.type.base.BerNode;
import org.hxzon.asn1.goose.GoosePduParser;

import ate.ua.AbstractCustomPacket;
import ate.ua.mina.NoExceptionProtocolCodecFilter;
import ate.ua.raw.mina.ARawCustomUAImp;
import ate.ua.raw.mina.ARawMinaUAImp;

public class GooseUA extends ARawCustomUAImp<GoosePacket>{
    
    @Override
    public boolean message_matched(GoosePacket msg, HashMap hm) {
        return true;
    }

    @Override
    public String get_default_schema() {
        return "goose";
    }

    @Override
    public GoosePacket decodePacket(IoBuffer buf) {
        return new GoosePacket().decode_packet(buf);
    }
    
}
