package ate.ua.arp;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.ushort;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.mina.core.buffer.IoBuffer;
import org.joou.UByte;
import org.joou.UShort;

import ate.ua.AbstractCustomPacket;
import ate.util.GenData0;
import ate.util.X2X0;

public class ArpPacket extends AbstractCustomPacket<ArpPacket>{
    UShort hardware_type=ushort(1);//ethernet
    UShort protocol_type=ushort(0x0800);//ip
    UByte hardware_size=ubyte(6);
    UByte protocol_size=ubyte(4);
    UShort opcode=ushort(1);//request
    byte[] sender_mac=new byte[]{1,1,1,1,1,1};
    byte[] sender_ip=new byte[]{1,1,1,1};
    byte[] target_mac=new byte[]{2,2,2,2,2,2};
    byte[] target_ip=new byte[]{2,2,2,2};
    byte[] padd=GenData0.randomBytes(18);
    private static int HEADER_LENGTH=44;    
    
    public static ArpPacket decode_packet(IoBuffer buf){
        byte[] bs=new byte[buf.remaining()];        
        buf.get(bs);
        if(bs.length<=HEADER_LENGTH)
            return null;
        
        ArpPacket pkt=new ArpPacket();
        DataInputStream di=new DataInputStream(new ByteArrayInputStream(bs));
        try {
            pkt.hardware_type=ushort(di.readShort());
            pkt.protocol_type=ushort(di.readShort());
            pkt.hardware_size=ubyte(di.readByte());
            pkt.protocol_size=ubyte(di.readByte());
            pkt.opcode=ushort(di.readShort());
            pkt.sender_mac=new byte[pkt.hardware_size.byteValue()];
            di.read(pkt.sender_mac);
            pkt.sender_ip=new byte[pkt.protocol_size.byteValue()];
            di.read(pkt.sender_ip);
            pkt.target_mac=new byte[pkt.hardware_size.byteValue()];
            di.read(pkt.target_mac);
            pkt.target_ip=new byte[pkt.protocol_size.byteValue()];
            di.read(pkt.target_ip);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pkt;
    }

    @Override
    public ArpPacket build(String k, Object v) {
        if("hardware_type".equalsIgnoreCase(k))
            hardware_type=ushort((Integer)v);
        else if("protocol_type".equalsIgnoreCase(k))
            protocol_type=ushort((Integer)v);
        else if("hardware_size".equalsIgnoreCase(k))
            hardware_size=ubyte((Integer)v);
        else if("protocol_size".equalsIgnoreCase(k))
            protocol_size=ubyte((Integer)v);
        else if("opcode".equalsIgnoreCase(k))
            opcode=ushort((Integer)v);//request
        else if("sender_mac".equalsIgnoreCase(k))
            sender_mac=X2X0.mac2bs((String)v);
        else if("sender_ip".equalsIgnoreCase(k))
            sender_ip=X2X0.ip2b((String)v);
        else if("target_mac".equalsIgnoreCase(k))
            target_mac=X2X0.mac2bs((String)v);
        else if("target_ip".equalsIgnoreCase(k))
            target_ip=X2X0.ip2b((String)v);
        else if("padd".equalsIgnoreCase(k))
            padd=X2X0.chs2b((String)v);
        else
            log.warn("not exist:{}",k);
        return this;
    }

    @Override
    public ArpPacket get_packet() {        
        return this;
    }

    @Override
    public void encodePacket(OutputStream out) {
        DataOutputStream dos = new DataOutputStream(out);
        try {
            if (payload != null) {
                dos.write(X2X0.hexs2b(payload));
                return;
            }
            dos.writeShort(hardware_type.shortValue());
            dos.writeShort(protocol_type.shortValue());
            dos.writeByte(hardware_size.byteValue());
            dos.writeByte(protocol_size.byteValue());
            dos.writeShort(opcode.shortValue());
            dos.write(sender_mac);
            dos.write(sender_ip);
            dos.write(target_mac);
            dos.write(target_ip);
            dos.write(padd);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
}
