package ate.ua.arp;

/*
 * #%L
 * ate-ua-arp
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;

import ate.ua.raw.mina.ARawCustomUAImp;

public class ArpUA extends ARawCustomUAImp<ArpPacket>{	
    @Override
	public int get_port(){
	    port=0x0806;
	    return port;
	}
	
	@Override
	public String get_transport(){
	    transport="raw";
	    return transport;
	}
	
	@Override
	public String get_default_schema() {
		return "arp";
	}

    @Override
    public boolean message_matched(ArpPacket msg, HashMap<String, Object> hm) {
        return true;
    }

    @Override
    public ArpPacket decodePacket(IoBuffer buf) {
        return ArpPacket.decode_packet(buf);
    }    
}
