package ate.test;

/*
 * #%L
 * ate-ua-arp
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.testcase.RawUATestcase;
import ate.ua.arp.ArpPacket;
import ate.ua.arp.ArpUA;
import ate.ua.raw.RawCsUA;

public class ArpUATest extends RawUATestcase {

	@Test
	public void snd_rcv() {
		log.info("dump");
		ArpUA uas = create_server("arp://"+IP1);
		ArpUA uac = create_client("arp://"+IP2,"arp://"+IP1);
		RawCsUA svr = create_server("socket://"+IP2+"/raw",new RawCsUA());        
        RawCsUA clt = create_client("socket://"+IP1+"/raw","socket://"+IP2+"/raw",new RawCsUA());        
       		
		uas.set_dump_file("test1.cap");
		uac.set_dump_file("test.cap");
		
		this.start_all_ua();
		
		STEP("UAC Send Request");
		ArpPacket pkt=new ArpPacket();
		//pkt.build(PArp.SENDERIP, "1.2.3.4").payload("sssssssssssssssssss")
		//		.complete();
		uac.send_message(pkt.get_packet());		
		ArpPacket tmp=uas.recv_message();		
		assertTrue(tmp!=null);
		
		STEP("UAS Send Reply");
        pkt=new ArpPacket();
        pkt.build("opcode", 2);
        uas.send_message(pkt.get_packet());     
        tmp=uac.recv_message();       
        assertTrue(tmp!=null);
		
		STEP("client send message:");
        clt.send_message("abcdefg".getBytes());
        byte[] req = svr.recv_message();
        assertTrue(req!=null&&req.length>0);

        STEP("server send 1234567:");
        svr.send_message("1234567".getBytes());
        req = clt.recv_message();
        assertTrue(req!=null&&req.length>0);
        
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
