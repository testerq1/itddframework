import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.testcase.RawUATestcase;
import ate.ua.arp.ArpPacket;
import ate.ua.arp.ArpUA;
import ate.ua.raw.RawCsUA;

public class ArpTest extends RawUATestcase {

    @Test
    public void snd_rcv() {
        log.info("dump");
        def uas = create_server("arp://"+IP1);
        def uac = create_client("arp://"+IP2,"arp://"+IP1);
        def svr = create_server("rawsocket://"+IP2+"/raw");        
        def clt = create_client("rawsocket://"+IP1+"/raw","rawsocket://"+IP2+"/raw");        
            
        uas.set_dump_file("test1.cap");
        uac.set_dump_file("test.cap");
        
        this.start_all_ua();
        
        STEP("UAC Send Request");
        ArpPacket pkt=new ArpPacket();
        uac.send_message(pkt.get_packet());     
        ArpPacket tmp=uas.recv_message();       
        assertTrue(tmp!=null);
        
        STEP("UAS Send Reply");
        pkt=new ArpPacket();
        pkt.build("opcode", 2);
        uas.send_message(pkt.get_packet());     
        tmp=uac.recv_message();       
        assertTrue(tmp!=null);
        
        STEP("client send message:");
        clt.send_message("abcdefg".getBytes());
        byte[] req = svr.recv_message();
        assertTrue(req!=null&&req.length>0);

        STEP("server send 1234567:");
        svr.send_message("1234567".getBytes());
        req = clt.recv_message();
        assertTrue(req!=null&&req.length>0);        
    }

    @BeforeMethod
    public void beforeMethod(Method m) {
        super.BeforeMethod(m);
    }

    @AfterMethod
    public void afterMethod(Method m) {
        super.shutdown_all_ua();
        super.AfterMethod(m);
    }

    @BeforeClass
    public void beforeClass() {        
        super.BeforeClass();
    }

    @AfterClass
    public void afterClass() {
        super.AfterClass();
    }
}
