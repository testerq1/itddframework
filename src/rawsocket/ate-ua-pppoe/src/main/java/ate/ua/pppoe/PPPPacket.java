package ate.ua.pppoe;

import static org.joou.Unsigned.ubyte;
import static org.joou.Unsigned.ushort;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.mina.core.buffer.IoBuffer;
import org.joou.*;

import ate.ua.AbstractCustomPacket;
import ate.util.X2X0;

public class PPPPacket extends AbstractCustomPacket<PPPPacket>{
    private static int HEADER_LENGTH=44;
    byte version = 1;           
    byte type = 1;           
    UByte code = ubyte(0);           
    UShort sessionId = ushort(1);         
    UShort length = ushort(1);       
    byte[] pt;          
    
    public static PPPPacket decode_packet(IoBuffer buf){
        byte[] bs=new byte[buf.remaining()];        
        buf.get(bs);
        if(bs.length<=HEADER_LENGTH)
            return null;
        
        PPPPacket pkt=new PPPPacket();
        DataInputStream di=new DataInputStream(buf.asInputStream());
        try {
            byte b=di.readByte();
            pkt.version=(byte)(b>>>4);
            pkt.type=(byte)(b&0x0f);
            pkt.code=ubyte(di.readByte());
            pkt.sessionId=ushort(di.readShort());            
            pkt.length=ushort(di.readShort());            
            pkt.pt=new byte[pkt.length.intValue()];
            di.read(pkt.pt);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pkt;
    }
    @Override
    public PPPPacket build(String k, Object v) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PPPPacket get_packet() {
        return this;
    }

    @Override
    public void encodePacket(OutputStream out) {
        DataOutputStream dos = new DataOutputStream(out);
        try {
            if (payload != null) {
                dos.write(X2X0.hexs2b(payload));
                return;
            }
            dos.writeByte((version<<4)+type);
            dos.writeByte(code.byteValue());
            dos.writeShort(sessionId.shortValue());
            dos.writeShort(length.shortValue());
            
            dos.write(pt);    
            if(pt.length<40)////以太包最少60字节
                dos.write(new byte[40-pt.length]);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

}
