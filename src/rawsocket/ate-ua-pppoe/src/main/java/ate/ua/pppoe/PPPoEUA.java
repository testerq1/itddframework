package ate.ua.pppoe;

import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;

import ate.rm.dev.Host;
import ate.ua.raw.ARawCustomUAImp;

public class PPPoEUA extends ARawCustomUAImp<PPPPacket>{
    
    @Override
    public int get_port(){
        port=0x8864;
        return port;
    }
    
    public static void dial(String uname,String passwd,String options){
        if(Host.is_win())
            Host.localhost().exec0("rasdial.exe "+uname+" "+uname+" "+passwd+" "+options);
        else
            ;
    }
    
    public void disconnect(){
        if(Host.is_win())
            Host.localhost().exec0("rasdial.exe /DISCONNECT");
    }

    @Override
    public boolean message_matched(PPPPacket msg, HashMap<String, Object> hm) {
        return true;
    }

    @Override
    public String get_default_schema() {
        return "pppoe";
    }

    @Override
    public PPPPacket decodePacket(IoBuffer buf) {
        // TODO Auto-generated method stub
        return null;
    }
}
