package ate.util;

import org.testng.annotations.Test;

import ate.testcase.Testcase;

public class BoilerpipeTest extends Testcase{
    @Test
    public void get_title(){
        log.info(new BoilerPipe().get_title("http://news.sina.com.cn/c/2014-09-09/030030810871.shtml"));
    }
    
    public void get_input_source(){
        new BoilerPipe().get_input_source("http://news.sina.com.cn/c/2014-09-09/030030810871.shtml");
    }
    
    public void get_images(){
        new BoilerPipe().get_images("http://www.spiegel.de/wissenschaft/natur/0,1518,789176,00.html");
    }
    
    public void highlight(){
        new BoilerPipe().highlight("http://news.sina.com.cn/c/2014-09-09/030030810871.shtml","hl.html");
    }
    
    public void test(){        
        log.info(new BoilerPipe().get_text("http://news.sina.com.cn/c/2014-09-09/030030810871.shtml"));        
    }

}
