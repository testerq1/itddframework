package ate.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ate.rm.dev.Host;
import de.l3s.boilerpipe.BoilerpipeExtractor;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.document.Image;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import de.l3s.boilerpipe.extractors.CommonExtractors;
import de.l3s.boilerpipe.sax.BoilerpipeSAXInput;
import de.l3s.boilerpipe.sax.HTMLFetcher;
import de.l3s.boilerpipe.sax.HTMLHighlighter;
import de.l3s.boilerpipe.sax.ImageExtractor;

/**
 * Boilerplate Removal and Fulltext Extraction from HTML pages
 * https://code.google.com/p/boilerpipe/
 * 
 * 
 * @author ravi huang
 *
 */
public class BoilerPipe extends AbstractTool {
    public Charset charset = Host.charset;
    
    public String get_title(String from){
        try {
            URL url = new URL(from);

            final InputSource is = HTMLFetcher.fetch(url).toInputSource();
            
            final BoilerpipeSAXInput in = new BoilerpipeSAXInput(is);
            final TextDocument doc = in.getTextDocument();            
            
            return doc.getTitle();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;        
    }
    /**
     * demo
     * @param from
     * @return
     */
    public String get_input_source(String from){
        try {
            URL url = new URL(from);

            final InputSource is = HTMLFetcher.fetch(url).toInputSource();
            
            final BoilerpipeSAXInput in = new BoilerpipeSAXInput(is);
            final TextDocument doc = in.getTextDocument();
            Object o=doc.getTextBlocks();
            
            log.info(doc.getTitle());
            
            // You have the choice between different Extractors
            // System.out.println(DefaultExtractor.INSTANCE.getText(doc));
            
            return ArticleExtractor.INSTANCE.getText(doc);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public void get_images(String from) {
        try {
            URL url = new URL(from);
            
            // choose from a set of useful BoilerpipeExtractors...
            final BoilerpipeExtractor extractor = CommonExtractors.ARTICLE_EXTRACTOR;
            // final BoilerpipeExtractor extractor =
            // CommonExtractors.DEFAULT_EXTRACTOR;
            // final BoilerpipeExtractor extractor =
            // CommonExtractors.CANOLA_EXTRACTOR;
            // final BoilerpipeExtractor extractor =
            // CommonExtractors.LARGEST_CONTENT_EXTRACTOR;

            final ImageExtractor ie = ImageExtractor.INSTANCE;

            List<Image> imgUrls = ie.process(url, extractor);

            // automatically sorts them by decreasing area, i.e. most probable true
            // positives come first
            Collections.sort(imgUrls);

            for (Image img : imgUrls) {
                System.out.println("* " + img);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 生成一个正文高亮的新网页 有bug
     * 
     * @param from
     *            源地址
     * @param to
     *            生成文件
     */
    public void highlight(String from, String to) {
        try {
            URL url = new URL(from);

            // choose from a set of useful BoilerpipeExtractors...
            final BoilerpipeExtractor extractor = CommonExtractors.ARTICLE_EXTRACTOR;
            // final BoilerpipeExtractor extractor =
            // CommonExtractors.DEFAULT_EXTRACTOR;
            // final BoilerpipeExtractor extractor =
            // CommonExtractors.CANOLA_EXTRACTOR;
            // final BoilerpipeExtractor extractor =
            // CommonExtractors.LARGEST_CONTENT_EXTRACTOR;

            // choose the operation mode (i.e., highlighting or extraction)
            final HTMLHighlighter hh = HTMLHighlighter
                    .newHighlightingInstance();
            // final HTMLHighlighter hh =
            // HTMLHighlighter.newExtractingInstance();

            PrintWriter out = new PrintWriter(to, charset.name());
            out.println("<base href=\"" + url + "\" >");
            out.println("<meta http-equiv=\"Content-Type\" content=\"text-html; charset=utf-8\" />");
            out.println(hh.process(url, extractor));
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 抓取网页正文
     * 
     * @param urlpath
     * @return
     */
    public String get_text(String from) {
        try {
            URL url = new URL(from);
            return ArticleExtractor.INSTANCE.getText(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
