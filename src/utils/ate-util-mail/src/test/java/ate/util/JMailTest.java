package ate.util;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.*;
import org.testng.annotations.Test;

import ate.testcase.UATestcase;

public class JMailTest extends UATestcase {
    String host = "mail.greatytech.com";
    String from = "[持续集成]<web.manager@greatytech.com>";
    String to = "xiaoyong.huang@greatytech.com";
    String dear="黄先生";
    String content="你好";
    //@Test
    public void sendMail() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setUsername("web.manager@greatytech.com");
        mailSender.setPassword("GreatyTech123");
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.debug", "true");
        props.put("mail.smtp.auth", true);
        mailSender.setJavaMailProperties(props);
        
        MimeMessage message = mailSender.createMimeMessage();
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(from);
        simpleMailMessage.setTo(to);
        simpleMailMessage.setSubject("我爱你中国");
        simpleMailMessage.setText("Dear %s,"
                                  +"Mail Content : %s");
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setFrom(new InternetAddress("web.manager@greatytech.com","[持续集成]"));
            helper.setTo(simpleMailMessage.getTo());
            helper.setSubject(simpleMailMessage.getSubject());
            helper.setText(String.format(simpleMailMessage.getText(), dear,
                    content));

            // FileSystemResource file = new FileSystemResource("C:\\log.txt");
            // helper.addAttachment(file.getFilename(), file);

        } catch (MessagingException e) {
            throw new MailParseException(e);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mailSender.send(message);
    }

    public void parser() {
        JMailUtil mail = new JMailUtil();
        assertTrue(null != mail
                .parser_address("[持续集成](194)<web.manager@greatytech.com>"));

    }

    @Test
    public void sendmail() throws UnsupportedEncodingException {
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.debug", "true");
        props.put("mail.smtp.auth", true);
        Session session = Session.getInstance(props);

        try {
            MimeMessage msg = new MimeMessage(session);
            // Set the FROM message
            msg.setFrom(new InternetAddress("web.manager@greatytech.com","[持续集成]"));

            InternetAddress[] address = { new InternetAddress(to) };
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject("我爱你中国");
            msg.setSentDate(new Date());
            msg.setText("This is the text for this simple demo using JavaMail.");
            Transport.send(msg, "web.manager@greatytech.com", "GreatyTech123");
        }

        catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }
}
