package ate.util;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message.*;
import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.*;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

public class JMailUtil {
    String host = "mail.greatytech.com";
    String from = "web.manager@greatytech.com";
    String from_name="[持续测试]";
    String to = "xiaoyong.huang@greatytech.com";
    String login = "web.manager@greatytech.com";
    String password = "GreatyTech123";
    String subject="我爱你中国";
    String dear="黄先生";    
    String content="你好";
    
    public JMailUtil() {

    }

    public InternetAddress parser_address(String sender) {
        try {
            return new InternetAddress(sender);
        } catch (AddressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    /**
     * spring mail
     */
    public void sendsmail() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setUsername(login);
        mailSender.setPassword(password);
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.debug", "true");
        props.put("mail.smtp.auth", true);
        mailSender.setJavaMailProperties(props);
        
        MimeMessage message = mailSender.createMimeMessage();
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(from);
        simpleMailMessage.setTo(to);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText("Dear %s,"
                                  +"Mail Content : %s");
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setFrom(new InternetAddress("web.manager@greatytech.com","[持续集成]"));
            helper.setTo(simpleMailMessage.getTo());
            helper.setSubject(simpleMailMessage.getSubject());
            helper.setText(String.format(simpleMailMessage.getText(), dear,
                    content));
            
            //附件
            // FileSystemResource file = new FileSystemResource("C:\\log.txt");
            // helper.addAttachment(file.getFilename(), file);

        } catch (MessagingException e) {
            throw new MailParseException(e);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mailSender.send(message);
    }
    /**
     * java mail
     * @throws UnsupportedEncodingException
     */
    public void sendjmail() throws UnsupportedEncodingException {
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.debug", "true");
        props.put("mail.smtp.auth", true);
        Session session = Session.getInstance(props);

        try {
            MimeMessage msg = new MimeMessage(session);
            // Set the FROM message
            msg.setFrom(new InternetAddress("web.manager@greatytech.com","[持续集成]"));

            InternetAddress[] address = { new InternetAddress(to) };
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject("我爱你中国");
            msg.setSentDate(new Date());
            msg.setText("This is the text for this simple demo using JavaMail.");
            Transport.send(msg, "web.manager@greatytech.com", "GreatyTech123");
        }

        catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }
    /**
     * java mail
     * @param body
     * @param subject
     * @param recipient
     * @throws MessagingException
     * @throws UnsupportedEncodingException
     */
    public void sendEmail_2(String body, String subject, String recipient)
            throws MessagingException, UnsupportedEncodingException {
        Properties mailProps = new Properties();
        mailProps.put("mail.smtp.from", from);
        mailProps.put("mail.smtp.host", host);
        // mailProps.put("mail.smtp.port", port);
        mailProps.put("mail.smtp.auth", true);
        // mailProps.put("mail.smtp.socketFactory.port", port);
        // mailProps.put("mail.smtp.socketFactory.class",
        // "javax.net.ssl.SSLSocketFactory");
        // mailProps.put("mail.smtp.socketFactory.fallback", "false");
        // mailProps.put("mail.smtp.starttls.enable", "true");

        Session mailSession = Session.getDefaultInstance(mailProps,
                new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(login, password);
                    }

                });

        MimeMessage message = new MimeMessage(mailSession);
        message.setFrom(new InternetAddress(from,from_name));
        String[] emails = { recipient };
        InternetAddress dests[] = new InternetAddress[emails.length];
        for (int i = 0; i < emails.length; i++) {
            dests[i] = new InternetAddress(emails[i].trim().toLowerCase());
        }
        message.setRecipients(Message.RecipientType.TO, dests);
        message.setSubject(subject, "UTF-8");
        Multipart mp = new MimeMultipart();
        MimeBodyPart mbp = new MimeBodyPart();
        mbp.setContent(body, "text/html;charset=utf-8");
        mp.addBodyPart(mbp);
        message.setContent(mp);
        message.setSentDate(new java.util.Date());

        Transport.send(message);
    }
}
