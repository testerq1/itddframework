package ate.util;

import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import ate.rm.dev.Host;
import ate.rm.file.ReadWriteTextFileWithEncoding;

import com.panayotis.gnuplot.GNUPlotException;
import com.panayotis.gnuplot.JavaPlot;
import com.panayotis.gnuplot.swing.JPlot;
import com.panayotis.gnuplot.terminal.ImageTerminal;
import com.panayotis.gnuplot.terminal.PostscriptTerminal;
import com.panayotis.gnuplot.terminal.SVGTerminal;
import com.panayotis.gnuplot.utils.Debug;

public class JGPlot extends JavaPlot{
	public static String gnuplotpath;
	
	/**
	 * quiet日志级别
	 */
	public static void quiet() {
		JavaPlot.getDebugger().setLevel(Debug.QUIET);
	}
	
	/**
	 * verbose日志级别
	 */
	public static void verbose() {
		JavaPlot.getDebugger().setLevel(Debug.VERBOSE);
	}
	
	private String cmds;
	
	private long time;
	
	public JGPlot() {		
		this(gnuplotpath,false);
	}
	/**
	 * 
	 * @param isGraph3D plot or splot
	 */
	public JGPlot(boolean isGraph3D) {		
		this(gnuplotpath,isGraph3D);		
	}
	public JGPlot(String gnuplotpath,boolean isGraph3D) {
		super(gnuplotpath, isGraph3D);
		this.gnuplotpath=gnuplotpath;
	}
	
	/**
	 * plot创建一个eps文件 
	 * @param fName
	 * @return
	 */
	public File create_eps(String fName) {
		time=new Date().getTime();
		PostscriptTerminal epsf = new PostscriptTerminal(fName);
		epsf.setColor(true);
		setTerminal(epsf);
		setPersist(false);
		plot(cmds);
		File f = new File(fName);
		if (f.exists())
			return f;
		return null;
	}

	/**
	 * plot 创建一个png文件
	 * @param imgName
	 * @param imgType
	 * @return
	 */
	public File create_image(String imgName, String imgType) {
		time=new Date().getTime();
		ImageTerminal png = new ImageTerminal();
		File file = new File(imgName);
		try {
			file.createNewFile();
			png.processOutput(new FileInputStream(file));
			setPersist(false);
			setTerminal(png);
			plot(cmds);
			ImageIO.write(png.getImage(), imgType, file);
			png.getImage().flush();
			png=null;			
			return file;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (GNUPlotException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * plot，返回一个SVG字符串
	 * @return
	 */
	public String get_svg() {
		SVGTerminal svg = new SVGTerminal();
		setTerminal(svg);
		setPersist(false);
		plot();
		return svg.getTextOutput();
	}

	/**
	 * plot开始时间
	 * @return
	 * @see #create_eps(String)
	 * @see #create_image(String, String)
	 */
	public long time(){
		return time;
	}
	/**
	 * 自定义plot，匹配字符串脚本
	 * @param cmd
	 */
	void plot(String cmd){
		if(cmd==null){
			plot();
			return;
		}
		try {
			exec.plot_quiet(cmd, term);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 在脚本前加一个reset
	 */
	public void reset(){
		getParameters().getPreInit().add("reset");
	}

	/**
	 * 设置一个字符串形式的命令<br>
	 * set_cmds
	 * @param cmds
	 * @see GPlotTest#plot_quiet()
	 */
	public void set_cmds(String cmds){
		this.cmds=cmds;
	}
	/**
	 * 传入一个脚本文件
	 * @param fName
	 * @see GPlotTest#gpl_file()
	 */
	public void set_gpl_file(String fName){
		ReadWriteTextFileWithEncoding rw=new ReadWriteTextFileWithEncoding(fName,Host.charset);		
		this.cmds=rw.read_all_lines();
	}
	
	/**
	 * 显示到JFrame里
	 */
	public void show() {
		JPlot plot = new JPlot();
		plot.setJavaPlot(this);
		setPersist(false);
		plot(cmds);
		JFrame f = new JFrame();
		f.getContentPane().add(plot);
		Dimension dim = new Dimension(800, 600);
		f.setPreferredSize(dim);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);

	}
	/**
	 * 显示SVG
	 */
	public void show_svg() {
		SVGTerminal svg = new SVGTerminal();
		setTerminal(svg);
		setPersist(false);
		plot(cmds);
		System.out.println(svg.getTextOutput());
		JFrame f = new JFrame();
		try {
			f.getContentPane().add(svg.getPanel());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Dimension dim = new Dimension(800, 600);
		f.setPreferredSize(dim);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}

}
