package ate.util;

import java.io.File;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.testcase.Testcase;

public class GPlotTest2 extends Testcase {

	@AfterClass
	public void after() {
		JGPlot.verbose();		
	}
	
	@BeforeClass
	public void before() {
		JGPlot.verbose();
		LHOST.rm("target/*.png");
		LHOST.rm("target/*.eps");
	}	
	
	@Test	
	public void rowstack(){
		JGPlot p = new JGPlot();
		p.set_cmds("reset\r\n"
				+"set key off\r\n" 
				+"set xtics rotate out\r\n"				
				+"set title 'Rowstacked Histogram'\r\n"				
				+"set boxwidth 0.6 relative\r\n"
				+"set style data histogram\r\n"
				+"set style histogram rowstacked\r\n"
				+"set style fill solid 1.0 border -1\r\n"	
				+"set grid ytics linestyle 1\r\n"
				+"set term png\r\n"				
				+"plot for [COL=2:4] 'src/test/data/date_mins.tsv' using COL:xticlabels(1) title columnheader\r\n"				  
				//+"quit\r\n"
				);
		File f = p.create_image("target/rowstack.png", "png");
		assertTrue(f != null&&f.lastModified()>p.time());	
	}
	
	public void clustered(){
		JGPlot p = new JGPlot();
		p.set_cmds("reset\r\n"
				//+"set key off\r\n" 
				+"set xtics rotate out\r\n"
				+"set title 'Clustered Histogram'\r\n"
				+"set style data histogram\r\n"				
				+"set style fill solid border\r\n"
				+"set term png\r\n"				
				+"plot for [COL=2:4] 'src/test/data/date_mins.tsv' using COL:xticlabels(1) title columnheader\r\n"
				//+"quit\r\n"
				);
		File f = p.create_image("target/clustered.png", "png");
		assertTrue(f != null&&f.lastModified()>p.time());	
	}
	
	public void boxes_spectrum (){
		JGPlot p = new JGPlot();
		p.set_cmds("reset\r\n"
				+"set key off\r\n" 
				+"set style data boxes\r\n"
				+"set title 'Frequency spectrum'\r\n"
				+"set boxwidth 0.8 relative\r\n"				
				+"set style fill solid 1.0\r\n"
				+"set term png\r\n"
				+"plot 'src/test/data/spectrum.tsv' using 1:2 with boxes\r\n"
				//+"quit\r\n"
				);
		File f = p.create_image("target/boxes_Spectrum.png", "png");
		assertTrue(f != null&&f.lastModified()>p.time());	
	}
	
	public void impulses(){
		JGPlot p = new JGPlot();
		p.set_gpl_file("src/test/impulses.dem");
		File f = p.create_image("target/impulses.png", "png");
		assertTrue(f != null&&f.lastModified()>p.time());
	}
	
	public void boxes_cn(){
		JGPlot p = new JGPlot();
		p.set_cmds("reset\r\n"
				+"set boxwidth 0.9 relative\r\n"
				+"set style fill solid 1.0\r\n"				
				+"set title '北京月平均降水量 (mm)'\r\n"
				+"set xlabel '月份'\r\n"
				+"set ylabel '降水量'\r\n"
				+"set xtics 1,1,12\r\n"
				//+"set key box\r\n"
				+"unset key\r\n" 
				+"set term png font 'simfang.ttf,14'\r\n"
				+"plot 'src/test/data/datafile.dat' with boxes\r\n"
				//+"quit\r\n"
				);
		File f = p.create_image("target/boxes_cn.png", "png");
		assertTrue(f != null&&f.lastModified()>p.time());	
	}

}
