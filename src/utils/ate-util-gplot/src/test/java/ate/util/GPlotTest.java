package ate.util;

import java.io.File;

import org.testng.annotations.*;

import com.panayotis.gnuplot.JavaPlot;
import com.panayotis.gnuplot.JavaPlot.Key;
import com.panayotis.gnuplot.dataset.FileDataSet;
import com.panayotis.gnuplot.layout.StripeLayout;
import com.panayotis.gnuplot.plot.AbstractPlot;
import com.panayotis.gnuplot.plot.DataSetPlot;
import com.panayotis.gnuplot.style.NamedPlotColor;
import com.panayotis.gnuplot.style.PlotStyle;
import com.panayotis.gnuplot.style.Style;
import com.panayotis.gnuplot.utils.Debug;

import ate.testcase.Testcase;

public class GPlotTest extends Testcase {

	@AfterClass
	public void after() {
		JGPlot.verbose();		
	}
	
	@BeforeClass
	public void before() {
		JGPlot.verbose();
		LHOST.rm("target/*.png");
		LHOST.rm("target/*.eps");
	}	
	@Test
	public void gpl_file(){
		JGPlot p = new JGPlot();
		p.set_gpl_file("src/test/borders.dem");
		File f = p.create_image("target/gpl.png", "png");
		assertTrue(f != null&&f.lastModified()>p.time());
	}
	
	@Test
	public void biniary(){
		JGPlot p = new JGPlot(true);
		p.getParameters().getPreInit().add("reset");
		
		p.addPlot("\"src/test/data/binary1\" binary");
		
		p.setTitle("Hidden line removal of explicit binary surfaces");
		p.set("ticslevel","0");
		p.set("key","box");
		p.set("style", "data lines");
		p.set("xrange","[-3:3]");
		p.set("yrange", "[-2:2]");
		p.set("hidden3d","");
		File f = p.create_image("target/3d.png", "png");
		assertTrue(f != null&&f.lastModified()>p.time());
	}	
	
	@Test
	public void defaultTerminal() {
		JGPlot p = new JGPlot();

		p.setTitle("Default Terminal Title");
		p.getAxis("x").setLabel("X axis", "Arial", 20);
		p.getAxis("y").setLabel("Y axis");

		p.getAxis("x").setBoundaries(-30, 20);
		p.setKey(JavaPlot.Key.TOP_RIGHT);

		double[][] plot = { { 1, 1.1 }, { 2, 2.2 }, { 3, 3.3 }, { 4, 4.3 } };
		DataSetPlot s = new DataSetPlot(plot);
		p.addPlot(s);
		p.addPlot("besj0(x)*0.12e1");
		PlotStyle stl = ((AbstractPlot) p.getPlots().get(1)).getPlotStyle();
		stl.setStyle(Style.POINTS);
		stl.setLineType(NamedPlotColor.GOLDENROD);
		stl.setPointType(5);
		stl.setPointSize(8);
		p.addPlot("sin(x)");

		p.newGraph();
		p.addPlot("sin(x)");

		p.newGraph3D();
		double[][] plot3d = { { 1, 1.1, 3 }, { 2, 2.2, 3 }, { 3, 3.3, 3.4 },
				{ 4, 4.3, 5 } };
		p.addPlot(plot3d);

		p.newGraph3D();
		p.addPlot("sin(x)*sin(y)");

		p.setMultiTitle("Global test title");
		StripeLayout lo = new StripeLayout();
		lo.setColumns(9999);
		p.getPage().setLayout(lo);

		File f = p.create_image( "target/defaultTerminal.png", "png");
		assertTrue(f != null&&f.lastModified()>p.time());
	}
	
	@Test
	public void EPSTerminal() {
		JGPlot p = new JGPlot();
		p.setTitle("Postscript Terminal Title");
		p.addPlot("sin (x)");
		p.addPlot("sin(x)*cos(x)");
		p.newGraph();
		p.addPlot("cos(x)");
		p.setTitle("Trigonometric functions -1");
		p.setMultiTitle("Trigonometric functions");
		File f=p.create_eps("target/EPSTerminal.eps");
		assertTrue(f != null);
	}
	public void file(){
		JGPlot p = new JGPlot();
		//p.addPlot(new FileDataSet(new File("lala")));
		String content=p.get_svg();
		assertTrue(content!=null&&content.length()>100);
	}
	
	@Test
	public void get_svg() {
		JGPlot p = new JGPlot();
		p.addPlot("x+3");
		String content=p.get_svg();
		assertTrue(content!=null&&content.length()>100);
		log.info(content);
	}	
	
	@Test
	public void plot_quiet(){
		JGPlot p = new JGPlot(true);
		p.set_cmds("reset\r\n"
				+"set hidden3d\r\n"
				+"set yrange [-2:2]\r\n"
				+"set ticslevel 0\r\n"
				+"set title 'Hidden line removal of explicit binary surfaces'\r\n"
				+"set style data lines\r\n"
				+"set xrange [-3:3]\r\n"
				//+"set key box\r\n"
				+"unset key\r\n" 
				+"set term png\r\n"
				+"splot \"src/test/data/binary1\" binary\r\n"
				//+"splot \"src/test/binary1\" binary title 'binary'\r\n"
				//+"quit\r\n"
				);
		File f = p.create_image("target/plot_quiet1.png", "png");
		assertTrue(f != null&&f.lastModified()>p.time());	
		
		p.set_cmds( "reset\r\n"+
					"set term png\r\n"+
					"splot sin(x)*y\r\n"
					//+"quit\r\n"
					);
		f = p.create_image("target/plot_quiet2.png", "png");
		assertTrue(f != null&&f.lastModified()>p.time());
	}
	
	public void show() {
		JGPlot p = new JGPlot();
		p.addPlot("sqrt(x)/x");
		p.addPlot("x*sin(x)");
		p.show();
		sleep(1000);
	}
	public void show_svg() {
		JGPlot p = new JGPlot();
		//p.setTitle("SVG Terminal Title");
		p.addPlot("x+3");
		p.show_svg();
		sleep(50000);
	}
	@Test
	public void simple() {
		JGPlot p = new JGPlot();
		p.addPlot("sin(x)");
		File f = p.create_image("target/simple.png", "png");
		assertTrue(f != null&&f.lastModified()>p.time());
	}
	@Test
	public void simple3D() {
		JGPlot p = new JGPlot(true);
		p.addPlot("sin(x)*y");
		File f = p.create_image("target/simple3D.png", "png");
		assertTrue(f != null&&f.lastModified()>p.time());
	}
	

}
