package ate.util;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;
import static org.testng.AssertJUnit.assertTrue;

import java.io.File;
import java.io.FileOutputStream;

import jcifs.UniAddress;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbSession;

import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.testng.PowerMockTestCase;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.rm.dev.Host;

/**
 * 
 * @author Administrator
 *
 */
@PrepareForTest({SmbSession.class,CifsUtil.class})
public class CifsUtilTest extends PowerMockTestCase{
    @Mock SmbFile smb2;
    @Mock File dstfile,dstdir;
    @Mock FileOutputStream fos;
    @Mock SmbFileInputStream sis;
    Host LHOST;
    CifsUtil ut;
    
    @BeforeClass
    public void beforeclss(){
        MockitoAnnotations.initMocks(this);
        
        LHOST=Host.localhost();
    }
    
	@BeforeMethod
	public void before() throws Exception{
		//LHOST.rm("111.txt");	
		when(smb2.getName()).thenReturn("smbfilename");
        when(smb2.isHidden()).thenReturn(false);
        when(smb2.isFile()).thenReturn(true);
        when(smb2.getPath()).thenReturn("smb://1.1.1.1/smbfilename");         
             
	}	
	@AfterMethod
    public void after() throws Exception{
	    //ut.teardown();
	}
	
	//@Test
	public void ut_private_method() throws Exception{
	    ut=spy(new CifsUtil("smb://a:b@1.1.1.1"));
	    doNothing().when(ut,"logon"); 
	    ut.startup();	    
	    verifyPrivate(ut).invoke("logon"); 
	    
	    when(ut.get_authority()).thenAnswer(new Answer() {
	        public Object answer(InvocationOnMock invocation) {
	            Object[] args = invocation.getArguments();
	            Object mock = invocation.getMock();
	            return "get_authority called with arguments length: " + args.length;
	         }
	     }); 
	    System.out.println(ut.get_authority()); 
	    
	    doAnswer(new Answer() {
	        public Object answer(InvocationOnMock invocation) {
	            Object[] args = invocation.getArguments();
	            Object mock = invocation.getMock();
	            System.out.println("startup called with arguments length: " + args.length); 
	            return null;
	        }
	    }).when(ut).startup();
	    ut.startup();
	}
	
	//@Test
	public void ut_copy()throws Exception{	    
	    SmbFile smb=mock(SmbFile.class);
        when(smb.getName()).thenReturn("smbdirname");
        when(smb.isDirectory()).thenReturn(true);       
        when(smb.listFiles()).thenReturn(new SmbFile[]{smb2});        
        when(dstdir.exists()).thenReturn(false);
        when(dstdir.mkdirs()).thenReturn(true);
        when(sis.read(isA(byte[].class))).thenReturn(-1);
        
        whenNew(File.class).withArguments(contains("dstdir")).thenReturn(dstdir);
        whenNew(File.class).withArguments(contains("smbfilename")).thenReturn(dstfile);
        whenNew(FileOutputStream.class).withArguments(dstfile).thenReturn(fos);
        whenNew(SmbFileInputStream.class).withArguments(smb2).thenReturn(sis);
        
        ut=new CifsUtil("smb://xiaoyong.huang:H%40Xyong@1.1.1.1");
        ut.copy(smb, "dstdir/");
        
        verify(sis,times(1)).read(isA(byte[].class));
        //verify(sis,times(1)).read(Matchers.<byte[]>anyObject());
        verifyNew(File.class).withArguments(contains("smbfilename"));
        verifyNew(File.class,times(2)).withArguments(contains("dstdir/"));
        verifyNew(File.class).withArguments("dstdir/");
        verify(dstdir).mkdirs();
        verify(dstfile,never()).mkdirs();
	}
	
	//@Test
	public void ut_logon() throws Exception{
	    
	    ut=new CifsUtil("smb://xiaoyong.huang:H%401111@1.1.1.1");
	    mockStatic(SmbSession.class);
	    doNothing().when(SmbSession.class);
        SmbSession.logon(any(UniAddress.class), any(NtlmPasswordAuthentication.class));
        
        //default
	    ut.startup();        
	    verifyStatic(times(1)); 
	    SmbSession.logon(UniAddress.getByName("1.1.1.1"), 
	            new NtlmPasswordAuthentication("","xiaoyong.huang","H@1111"));
	    
	    //with domain
	    ut.set_domain("domain");	    
	    ut.startup();
	    verifyStatic(times(1)); 
        SmbSession.logon(UniAddress.getByName("1.1.1.1"), 
                new NtlmPasswordAuthentication("domain","xiaoyong.huang","H@1111"));
        
	}
	@Test
	public void copy() throws SmbException{
        CifsUtil ut=new CifsUtil("smb://xiaoyong.huang:H%40Xyong@172.16.161.243");
        ut.startup();
        
        ut.copy(ut.get_smb_file("smb://172.16.161.243/Test_Version/reporter_for_wlan/64bit/4.5.6.7/NETOP/Reporter-4.5.6.7-20140708.bin"), "./tmp/");

        File tmp=new File("./tmp/Reporter-4.5.6.7-20140708.bin");
        assertTrue(tmp.exists());       
    }   
	
	//@Test
	public void logon() throws SmbException{
		CifsUtil ut=new CifsUtil("smb://172.16.161.243");
		ut.logon("","xiaoyong.huang", "H@Xyong");
		SmbFile fi=ut.get_smb_file("/Pub_swap/xiaoyong.huang/");
		SmbFile[] fis=fi.listFiles();
//		for(SmbFile tmp:fis){
//			if(tmp.isFile())
//				log.info(tmp.getPath()+"\t"+tmp.getDate()+"\t"+tmp.getLastModified());			
//		}
		
		ut.copy(ut.get_smb_file("/Pub_swap/111.txt"), ".");
		File tmp=new File("111.txt");
		assertTrue(tmp.exists());		
	}	

}
