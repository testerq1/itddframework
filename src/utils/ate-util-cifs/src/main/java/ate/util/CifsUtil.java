package ate.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.UnknownHostException;

import jcifs.UniAddress;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbSession;
import ate.AbstractURIHandler;
import ate.ua.RunFailureException;

/**
 * CifsUtil cifs=new CifsUtil("smb://192.168.1.1");<br> 
 * cifs=new CifsUtil("smb://root:passwd@192.168.1.1");<br>
 * cifs.set_domain("xxx");
 * cifs.startup()
 * 
 * @author Administrator
 *
 */
public class CifsUtil extends AbstractURIHandler {
	String domain="";	
	NtlmPasswordAuthentication authentication;
	boolean isstartup=false;
	
	public CifsUtil(){}
	
    public CifsUtil(String uri){        
        this.build(UA_URI, uri);              
    }
    
	private void logon(){
	    if(!isstartup){
	        System.setProperty("jcifs.smb.client.dfs.disabled", "true");
	        //http://stackoverflow.com/questions/18119489/very-slow-file-copying-to-windows-network-using-jcif
	        System.setProperty("jcifs.resolveOrder", "DNS");	        
	    }
            
		try {
			UniAddress dc = UniAddress.getByName(host);
			authentication = new NtlmPasswordAuthentication(
					domain, username, passwd);
			SmbSession.logon(dc, authentication);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (SmbException e) {
			e.printStackTrace();
		}
	}
	
	public void logon(String domain,String name,String passwd){
		this.domain=domain;
		this.username=name;
		this.passwd=passwd;
		logon();
	}
	
	public SmbFile get_smb_file(String filepath) {
		SmbFile remoteFile;
		try {
			String smb_path=uri.toString();
			
			if(filepath.startsWith("smb"))
				smb_path="";
			
			if(authentication!=null)
				remoteFile = new SmbFile(smb_path + filepath,authentication);
			else{
				remoteFile = new SmbFile(smb_path + filepath);
				remoteFile.connect();
			}
			return remoteFile;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * copy hidden以外的 smb file,
	 * copy(file,file) 
	 * copy(dir,*)  认为*为路径，如果不存在会创建
	 * copy(file,dir) dir必须以/结尾,如果不存在会创建
	 * @param remoteFile 被copy文件
	 * @param dst_path 本地路径或文件名
	 */
	public void copy(SmbFile remoteFile, String dst_path) {
		InputStream in = null;
		OutputStream out = null;
		try {
			if (dst_path == null)
				dst_path = remoteFile.getName();
			
			if (remoteFile.isDirectory()) {
			    log.debug("copy remote dir {}", remoteFile.getPath());
				File dir = new File(dst_path);
				if (!dir.exists()) {
					log.debug("mkdir {}", dst_path);
					dir.mkdirs();
				}
				SmbFile[] fs = remoteFile.listFiles();
				for (SmbFile tmp : fs) {
					copy(tmp, dst_path + File.separator + tmp.getName());
				}
			} else if(remoteFile.isFile()&&!remoteFile.isHidden()){
				log.debug("copy {} to {}", remoteFile.getPath(), dst_path);
				File localFile = new File(dst_path);
				if(dst_path.endsWith("/")&&!localFile.exists()){
				    log.debug("create folder:{}",dst_path);
				    localFile.mkdirs();
				}
				
				if(localFile.isDirectory()){				    
					localFile=new File(dst_path+File.separator+remoteFile.getName());
					log.debug("copy to:{}",localFile.getAbsolutePath());
				}
				
				int noOfBytes = 0;
				in = new BufferedInputStream(new SmbFileInputStream(remoteFile));
				out = new FileOutputStream(localFile);
				byte[] buffer = new byte[1024];
				while ((noOfBytes = in.read(buffer)) != -1) {
					out.write(buffer,0,noOfBytes);					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					out.close();
					in.close();
				}else{
				    new RunFailureException("remote smb file nit exist:"+remoteFile.getPath());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 设置domain，缺省为""
	 * @param domain
	 */
	public void set_domain(String domain){
	    this.domain=domain;
	}
	
	@Override
	public void teardown() {		
		
	}

	@Override
	public void startup() {
	    isstartup=true;
		System.setProperty("jcifs.smb.client.dfs.disabled", "true");
        if(this.get_uname()!=null)
            logon();    
	}

	@Override
	public String get_default_schema() {
		return "cifs";
	}

	@Override
	public boolean is_ready() {
		return host!=null;
	}

}
