import java.io.File;
import org.testng.annotations.*
import org.testng.annotations.Test
import ate.testcase.*
import ate.util.*;
import common.*

public class CifsTest extends CsUATestcase {

	@BeforeClass
	public void before(){
		LHOST.rm("111.txt");
		log.info("");
	}

	@Test
	public void test(){
		STEP("测试访问SMB文件系统")
		def ut=new CifsUtil("smb://172.16.161.243");
		ut.logon("","root", "admin");
		def fi=ut.get_smb_file("/Pub_swap/xiaoyong.huang/");

		fi.listFiles().each{tmp->
			if(tmp.isFile())
				log.info(tmp.getPath()+"\t"+tmp.getDate()+"\t"+tmp.getLastModified());
		}

		ut.copy(ut.get_smb_file("/Pub_swap/111.txt"), ".");
		def tmp=new File("111.txt");
		assertTrue(tmp.exists());
	}
	
	//need ssh
	@Test
	public void download(){
		STEP("测试访问SMB文件系统")
		def ut=new CifsUtil("smb://172.16.141.243");
		ut.logon("","xiaoyong.huang", "H@Xyong");
		def fi=ut.get_smb_file("/Test_Version/reporter_for_wlan/64bit/");

		def src=null;
		fi.listFiles().each{tmp->
			if(src==null ||src.getLastModified()<tmp.getLastModified()) {
				src=tmp;
			}
		}
		ut.copy(ut.get_smb_file(src.getPath()), "./"+src.getName());
		def srcfile=new File(src.getName());
		assertTrue(srcfile.exists());
		
		def srcfile2=null
		def srcfile3=null
		srcfile.listFiles().each{tmp1->
			if(tmp1.isFile()) {
				if((tmp1.getName()).matches("(\\w*)-(\\d.*\\d)-\\d*.bin")) {
					srcfile2=tmp1;
				}
			}
			else {
				tmp1.listFiles().each{tmp2->
					if((tmp2.getName()).matches("NETOP")) {
						srcfile3=tmp2;
					}
				}
				srcfile3.listFiles().each{tmp3->
					if((tmp3.getName()).matches("(\\w*)-(\\d.*\\d)-\\d*.bin")) {
						srcfile2=tmp3;
					}
				}
			}
		}
		def ssh = create_ua("ssh://root:NTc%402010@172.16.5.197:22/?line_mode=r");
		this.start_all_ua();		
		assertTrue(ssh.is_ready());
		ssh.login(/(]$ |: |]# |->|])$/, "root]");
		
		def old=ssh.send_and_expect("cat /usr/private/Firmware_version");
		assertTrue(old!=null);
		
		if(old.compareTo("Reporter-"+src.getName())<0) {
			def word="scp root@172.16.5.194:"+srcfile2.getAbsolutePath()+" /usr/download/"+srcfile2.getName();
			ssh.set_timeout(60)
			assertTrue(ssh.send_and_expect(word)!=null);			
			ssh.send_line("NTc@2010");			
			ssh.send_line("cd /usr/download");
			assertTrue(ssh.send_and_expect("ace_upsys "+srcfile2.getName())!=null);
			
			if(expect.contains(ssh.send_and_expect("cat /usr/private/Firmware_version"),"Reporter-"+src.getName()))
				println "update success"			
			else 
				println "update failed"			
		}else
			println "没有新版本，不需要升级"
	}
}
