package ate.utils.sigar;

import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarProxy;
import org.hyperic.sigar.SigarProxyCache;

public class SigarUA {
	private Sigar sigar ;
	   
    private SigarProxy proxy;
   
    private StringBuilder info = new StringBuilder();

	
	private void sigarInit(boolean isProxy) {
        sigar = new Sigar();
        if(isProxy)
            proxy = SigarProxyCache.newInstance(this.sigar);
    }
   
}
