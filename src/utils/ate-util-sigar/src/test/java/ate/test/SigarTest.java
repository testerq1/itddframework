package ate.test;
import org.hyperic.sigar.shell.ShellCommandExecException;
import org.hyperic.sigar.shell.ShellCommandUsageException;
import org.testng.annotations.Test;

import ate.testcase.Testcase;
import ate.utils.sigar.*;

public class SigarTest extends Testcase{
	@Test
	public void who() throws ShellCommandUsageException, ShellCommandExecException{
		Who who=new Who();
		who.processCommand(new String[0]);
		log.info(who.get_return());
		
	}
}
