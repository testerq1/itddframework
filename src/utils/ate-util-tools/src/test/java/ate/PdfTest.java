package ate;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import ate.testcase.Testcase;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfAnnotation;
import com.itextpdf.text.pdf.PdfAppearance;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfIndirectObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfStream;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Unit test for simple App.
 */
public class PdfTest  extends Testcase{
	protected static final String RESULT = "hello.pdf";
	File fs;
	//@Test
	public void createPdf() throws DocumentException,
			IOException {
		// step 1
		Document document = new Document();
		// step 2
		PdfWriter.getInstance(document, new FileOutputStream(RESULT));
		// step 3
		document.open();
		// step 4
		document.add(new Paragraph("Hello World!"));
		// step 5
		document.close();
		AssertJUnit.assertTrue((fs=new File(RESULT)).exists());
		
	}	
	@Test
	 public void createPdf3D() throws IOException, DocumentException {
        // step 1
    	Document document = new Document();
    	// step 2
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(RESULT));
        // step 3
        document.open();
        // step 4
        Rectangle rect = new Rectangle(100, 400, 500, 800);
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(0.5f);
        rect.setBorderColor(new BaseColor(0xFF, 0x00, 0x00));
        document.add(rect);
            
        PdfStream stream3D = new PdfStream(new FileInputStream("src/test/resources/teapot.u3d"), writer);
        stream3D.put(PdfName.TYPE, new PdfName("3D"));
        stream3D.put(PdfName.SUBTYPE, new PdfName("U3D"));
        stream3D.flateCompress();
        PdfIndirectObject streamObject = writer.addToBody(stream3D);
        stream3D.writeLength();
            
        PdfDictionary dict3D = new PdfDictionary();
        dict3D.put(PdfName.TYPE, new PdfName("3DView"));
        dict3D.put(new PdfName("XN"), new PdfString("Default"));
        dict3D.put(new PdfName("IN"), new PdfString("Unnamed"));
        dict3D.put(new PdfName("MS"), PdfName.M);
        dict3D.put(new PdfName("C2W"),
            new PdfArray(new float[] { 1, 0, 0, 0, 0, -1, 0, 1, 0, 3, -235, 28 } ));
        dict3D.put(PdfName.CO, new PdfNumber(235));

        PdfIndirectObject dictObject = writer.addToBody(dict3D); 
            
        PdfAnnotation annot = new PdfAnnotation(writer, rect);
        annot.put(PdfName.CONTENTS, new PdfString("3D Model"));
        annot.put(PdfName.SUBTYPE, new PdfName("3D"));
        annot.put(PdfName.TYPE, PdfName.ANNOT);
        annot.put(new PdfName("3DD"), streamObject.getIndirectReference());
        annot.put(new PdfName("3DV"), dictObject.getIndirectReference());
        PdfAppearance ap = writer.getDirectContent().createAppearance(rect.getWidth(), rect.getHeight());
        annot.setAppearance(PdfAnnotation.APPEARANCE_NORMAL, ap);
        annot.setPage();

        writer.addAnnotation(annot);
        // step 5
        document.close();
    }
	
	
	@AfterMethod
	public void AfterMethod(){
		if(fs!=null&&fs.exists())
			fs.delete();
	}
	
	@BeforeMethod
	public void BeforeMethod(){
		File fs=new File(RESULT);
		if(fs.exists())
			fs.delete();
	}
}
