package ate.actions;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Enumeration;
import java.util.HashMap;

import org.apache.camel.util.UnsafeUriCharactersEncoder;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Copy;
import org.apache.tools.ant.taskdefs.Expand;
import org.apache.tools.ant.taskdefs.Jar;
import org.apache.tools.ant.taskdefs.Mkdir;
import org.apache.tools.ant.taskdefs.Zip;
import org.apache.tools.ant.taskdefs.optional.net.FTP;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.FilterSet;
import org.apache.tools.ant.types.PatternSet;
import org.apache.tools.ant.types.ZipScanner;
import org.apache.tools.ant.util.FileNameMapper;
import org.apache.tools.ant.util.FlatFileNameMapper;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;

import ate.TaskMethod;
import ate.actions.ATaskRunner;
import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;
import ate.rm.dev.Host;

public class AntTask extends ATaskRunner {
	@Override
	public boolean onlyCanRunOnce(){
		return false;
	}
	public static HashMap<String,Method> msMap=new HashMap<String,Method>();
	public AntTask(){
		if(msMap.size()==0){
			Method[] ms=this.getClass().getDeclaredMethods();
			for (Method element : ms) {
				element.getAnnotations();
				if(element.isAnnotationPresent(TaskMethod.class)){
					msMap.put(element.getName().toLowerCase(), element);
					System.out.println(element.getName());
				}
			}
		}
		
	}
	public static void main(String[] args)throws Exception{
new AntTask();
		//AntTask.exec(" start java -version");
	}

	@Override
	public TTTask execute(TTTask task) {
		String[] args = task.getParas().split(" ");
		File antFile=null;
		if(args[0].equalsIgnoreCase("-file"))
			antFile=new File(args[1]);
		else
			antFile=new File(Host.HOME+File.separator+"conf"+File.separator+"ant_default.xml");
		if(!antFile.exists()){
			task.setExeResult(EXERESULT.FAIL);
			task.setResultDesc("ant file is not exist "+task.getParas());
			return task;
		}
		org.apache.tools.ant.Main.main(args);
		
		return null;
	}
	@TaskMethod
	public void svn(){
		
	}
	
	@TaskMethod
	public void exec(String ...args)throws Exception{
		Runtime run= Runtime.getRuntime();
		Process pro=run.exec(args);
		System.out.println(pro);
		System.out.println(pro.exitValue());
	}
	@TaskMethod
	public void ftp_get(String ...args)throws Exception{
		//uri="ftp://user:passwd@127.0.0.1:8080/dir/file.txt";
		Project project = new Project();
		
		project.init();
		if(args.length==2)
			project.setBasedir(args[1]);
		else
			project.setBasedir(".");
		FTP.Action ftpa = new FTP.Action();
		ftpa.setValue("get"); 
		FTP ftp=new FTP();
		ftp.setProject(project);
		
		URI u = new URI(UnsafeUriCharactersEncoder.encode(args[0]));
		ftp.setServer(u.getHost());
		ftp.setPort(u.getPort()==-1?21:u.getPort());	
		
		if(u.getUserInfo()!=null){
			String[] up=u.getUserInfo().split(":");
			ftp.setUserid(up[0]);
			ftp.setPassword(up.length==2?up[1]:"");
		} else
			ftp.setUserid("anonymous");
		String path=u.getPath();
		String src="";
		String includes="";
		if(path!=null){
			int fsp=path.lastIndexOf(File.separator);
			src=(fsp>0?"/":path.substring(0,fsp));
			includes=(fsp<=0?"*":path.substring(fsp));
		}
		FileSet fileset = new FileSet();
		fileset.setIncludes(includes);
		
		ftp.setRemotedir(src);
		ftp.addFileset(fileset);
		
//		String ftpUser = URLEncoder.encode(username, "UTF-8"); 
//		String ftpPass = URLEncoder.encode(password, "UTF-8"); 
//		String url = String.format("ftp://%s:%s@domain.com", ftpUser, ftpPass); 
		
		ftp.setVerbose(true);
		ftp.setAction(ftpa);
		
		ftp.execute();	
	}
	/**
	 * 文件copy
	 * from : args[0]
	 * to   : args[1]
	 * */	 
	@TaskMethod
	public void fileCopy(String ... args)throws Exception{
		Project prj=new Project(); 
		Copy copy=new Copy(); 
		copy.setProject(prj); 
		copy.setFile(new File(args[0])); 
		copy.setTodir(new File(args[1])); 
		copy.execute(); //将f1.txt文件copy到dir1中 
	}
	/**
	 * copy文件并同时替换其中的内容， 如将 xml中的 @eosapp_name@ 替换成真正的应用名称 
	 * from : args[0]
	 * to   : args[1]
	 * file type:args[2] (**\/*.xml)
	 * replace_string:args[3]
	 * to_string:args[4]
	 * */
	@TaskMethod
	public void copyTo(String...args)throws Exception{
		Project prj=new Project(); 
		Copy copy = new Copy(); 
		copy.setEncoding("UTF-8"); 
		copy.setProject(prj); 
		copy.setTodir(new File(args[1])); 
		copy.setFile(new File(args[0]));
		
		FileSet fileSet=new FileSet(); 
		fileSet.setDir(new File(args[1])); 
		fileSet.setIncludes(args[2]); 
		copy.addFileset(fileSet); 
	
		FilterSet filter=copy.createFilterSet(); 
		filter.addFilter(args[3],args[4]); 
		copy.execute(); 
	}
	/**
	 * 文件或目录移动  
	 * from : args[0]
	 * to   : args[1]	 
	 * */	 
	@TaskMethod
	public void move(String ...args)throws Exception{
		Project prj=new Project(); 
		Copy copy=new Copy(); 
		copy.setProject(prj); 
		copy.setFile(new File(args[0])); 
		copy.setTodir(new File(args[1])); 
		copy.execute(); 
	}
	/**
	 * rename file/folder
	 * from : args[0]
	 * to   : args[1]	 
	 * */
	@TaskMethod
	public void rename(String ...args )throws Exception{
		Project prj=new Project(); 
		Copy copy=new Copy(); 
		copy.setProject(prj); 
		copy.setFile(new File(args[0])); 
		copy.setTodir(new File(args[1])); 
		copy.execute(); //将f1.txt文件更名为f2.txt中 
	}
	
	//3．使用文件集 FileSet 
	//使用文件集可以同时将多个满足匹配条件的文件集合进行copy、move和压缩等操作。 
	public void copyFileSet()throws Exception{
		Project prj=new Project(); 
		Copy copy=new Copy(); 
		copy.setProject(prj); 
		copy.setTodir(new File("d:temptodir")); 
	
		FileSet fs=new FileSet(); 
		fs.setProject(prj); 
		fs.setDir(new File("d:javaprjsrc")); 
		fs.setIncludes("**/*.*"); //包含所有文件 
		fs.setExcludes("**/CVS,**/*.class"); //排除CVS相关文件，以及.class文件 
		copy.addFileset(fs); 
	
		copy.execute(); 
	}
	//注： FileSet的setIncludes, 和setExcludes方法输入pattern, pattern是一个使用“，”或空格分隔的匹配字符串，其中， “**”代表所有文件或目录，“*.*”代表说有文件， “*.java”代表所有扩展名为java的文件。 

	//4．目录扫描，查找文件 	
	public void find()throws Exception{
		DirectoryScanner ds=new DirectoryScanner(); 
		ds.setBasedir(new File("d:tempwar")); 
		ds.setIncludes(new String[]{} ); 
		ds.scan(); 
		if(ds.getIncludedFilesCount()>0) { 
			System.out.println("found jsp!"); 
			String[] includeFiles=ds.getIncludedFiles(); 
			for(String file:includeFiles)
				System.out.println(file); 
		} 
	}
	/**
	 * zip
	 * from : args[0]
	 * to   : args[1]	
	 * filter:args[2]  ("**\/*.java")
	 * */
	@TaskMethod
	public void zip(String ...args )throws Exception{
		Project prj=new Project(); 
		Zip zip=new Zip(); 
		zip.setProject(prj); 
		zip.setDestFile(new File(args[1])); 
		FileSet fileSet=new FileSet(); 
		fileSet.setProject(prj); 		
		fileSet.setDir(new File(args[0])); 
		if(args.length>2)
			fileSet.setIncludes(args[2]);
		zip.addFileset(fileSet); 
		zip.execute(); 
	}
	/**
	 * jar
	 * from : args[0]
	 * to   : args[1]	
	 * filter:args[2]  ("**\/*.class,**\/*.properties")
	 * */
	@TaskMethod
	public void jar(String ...args )throws Exception{
		//将class文件打成jar包 
		Project prj=new Project(); 
		Jar jar=new Jar(); 
		jar.setProject(prj); 
		jar.setDestFile(new File(args[1])); 
		FileSet fileSet=new FileSet(); 
		fileSet.setProject(prj); 
		fileSet.setDir(new File(args[0])); 
		fileSet.setIncludes(args[2]); 
		jar.addFileset(fileSet); 
		jar.execute(); 
	}
	
	/**
	 * unzip
	 * from : args[0]
	 * to   : args[1]	
	 * */
	@TaskMethod
	public void unzip(String ... args)throws Exception{
		Project prj=new Project(); 
		Expand expand=new Expand(); 
		expand.setProject(prj); 
		expand.setSrc(new File(args[0])); 
		expand.setOverwrite(true); 
		expand.setDest(new File(args[1])); 
		expand.execute(); 
	}
	//2）将压缩文件中的符合匹配条件的文件解压 
	public void unzip2()throws Exception{
		Project prj=new Project(); 
		Expand expand=new Expand(); 
		expand.setProject(prj); 
		expand.setSrc(new File("d:tempsrc.zip")); 
		expand.setOverwrite(true); 
		expand.setDest(new File("d:tempoutsrc")); 
		PatternSet patternset = new PatternSet(); 
		patternset.setIncludes("**/*.java"); 
		patternset.setProject(prj); 
		expand.addPatternset(patternset); 
		expand.execute(); 
	}
	//3）利用Mapper解压文件: 如将 .../lib/*.jar 解压到 .../WEB-INF/lib目录下（去除目录结构） 
	public void unzip3()throws Exception{
		Project prj=new Project(); 
		Expand expand = new Expand(); 
		expand.setProject(prj); 
		expand.setSrc(new File("zipFilePath")); 
		expand.setDest(new File("/WEB-INF/lib")); 
	
		PatternSet pattern = new PatternSet(); 
		pattern.setIncludes("lib/*.jar"); 
		expand.addPatternset(pattern); 
	
		FileNameMapper mapper=new FlatFileNameMapper(); 
		expand.add(mapper); 
	
		/* another way using mapper 
		Mapper mapper=expand.createMapper(); 
		MapperType type=new MapperType(); 
		type.setValue("flatten"); 
		mapper.setType(type); 
		*/ 
		expand.execute(); 
	}
	
	//7．读取zip文件 
	//1） 读取zip文件中的文件和目录 

	public void readFromZip()throws Exception{
		ZipFile zipfile = new ZipFile(new File("")); 
		for (Enumeration entries = zipfile.getEntries(); entries.hasMoreElements();) { 
		ZipEntry entry = (ZipEntry) entries.nextElement(); 
		if(entry.isDirectory()) 
			System.out.println("Directory: "+entry.getName()); 
		else 
			System.out.println("file: "+entry.getName()); 
		} 
		zipfile.close(); //ZipFile用完必须close，否则文件被锁定 
	}
	//2）zip文件扫描,在Zip文件中查找目录或文件 
	public void findiInZip()throws Exception{
		ZipScanner scan=new ZipScanner(); 
		scan.setSrc(new File("d:temptest.zip")); 
		scan.setIncludes(new String[]{} ); //查找目录（一、二级目录）； 
		scan.scan(); 
		scan.getIncludedDirectories(); 
		scan.setIncludes(new String[]{}); //查找文件 
		scan.scan(); 
		scan.getIncludedFiles();
	}
	/**
	 * mkdir
	 * dir : args[0]
	 * */
	@TaskMethod
	public void mkdir(String ...args)throws Exception{
		Project prj=new Project(); 
		Mkdir mkdir = new Mkdir(); 
		mkdir.setProject(prj); 
		mkdir.setDir(new File(args[0]));
		mkdir.execute();
	}

	@Override
	public String getType() {		
		return "Ant_Task";
	}
	@Override
	public String getDesc() {
		// TODO Auto-generated method stub
		return "ant Task";
	}
}
