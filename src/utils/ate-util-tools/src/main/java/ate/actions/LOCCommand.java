package ate.actions;

import java.util.logging.Level;

import org.satish.core.LOCCount;

import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;
import ate.util.GenData0;

public class LOCCommand extends ATaskRunner {

	@Override
	public TTTask execute(TTTask task) {
		LOCCount counter = new LOCCount(task.getParas(), true, true);
		
		counter.setLogLevel(Level.WARNING);
		try {			
			counter.process();
			task.setExeResult(EXERESULT.SUCCESS);
			task.setResultDesc("No. of files : " + counter.getFileList().size()+
							   "\n\tTotal no. of lines : " + counter.getLoc()+
							   "\n\tEmpty Lines   : " + counter.getEmptyLines());			
		} catch (Exception e) {
			task.setExeResult(EXERESULT.FAIL);
			task.setResultDesc(GenData0.getExceptionDesc(e));
			e.printStackTrace();
		}
		return task;
	}
	public static void main(String[] args){
		TTTask task=new TTTask();
		task.setParas("F:\\gh\\ATE4D\\src\\ate");
		new LOCCommand().execute(task);
		System.out.print(task.getResultDesc());
	}
	@Override
	public String getType() {	
		return "loc";
	}
	@Override
	public String getDesc() {		
		return "Line of Code";
	}
	
	@Override
	public int validTaskConfig(final TTTask task){
		if(task.getParas()==null||!task.getParas().toLowerCase().contains(":"))
			return PARAS;
		return super.validTaskConfig(task);
	}
}
