package ate.actions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.incava.util.diff.Diff;
import org.incava.util.diff.Difference;

public class DiffUtil {
	public static String fileDiff(String fromFile, String toFile) {
		String[] aLines = read(fromFile);
		String[] bLines = read(toFile);
		return diff(aLines, bLines);
	}
	public static void main(String[] args){
		int i=10;
		do{i--;}while((--i)>0);
		System.out.print(i);
		System.out.print(i--);
//		String a="asd\nghgf\nhh";
//		String b="asd\r\nghgf\r\nhh";
//		System.out.print(diff(a.split("\n"),b.split("\r\n")));
		
	}
	public static String diff(String[] aLines, String[] bLines) {
		List diffs = (new Diff(aLines, bLines)).diff();
		String toReturn = "";

		Iterator it = diffs.iterator();
		while (it.hasNext()) {
			Difference diff = (Difference) it.next();
			int delStart = diff.getDeletedStart();
			int delEnd = diff.getDeletedEnd();
			int addStart = diff.getAddedStart();
			int addEnd = diff.getAddedEnd();
			String from = toString(delStart, delEnd);
			String to = toString(addStart, addEnd);
			String type = delEnd != Difference.NONE
					&& addEnd != Difference.NONE ? "c"
					: (delEnd == Difference.NONE ? "a" : "d");

			toReturn += from + type + to + "\n\n";

			if (delEnd != Difference.NONE) {
				toReturn += printLines(delStart, delEnd, "<", aLines);
				if (addEnd != Difference.NONE) {
					toReturn += "---\n";
				}
			}
			if (addEnd != Difference.NONE) {
				toReturn += printLines(addStart, addEnd, ">", bLines) + "\n";
			}
		}
		return toReturn;
	}

	protected static String printLines(int start, int end, String ind,
			String[] lines) {
		String toReturn = "";
		for (int lnum = start; lnum <= end; ++lnum) {
			toReturn += ind + " " + lines[lnum] + "\n";
		}
		return toReturn;
	}

	protected static String toString(int start, int end) {
		// adjusted, because file lines are one-indexed, not zero.

		StringBuffer buf = new StringBuffer();

		// match the line numbering from diff(1):
		buf.append(end == Difference.NONE ? start : (1 + start));

		if (end != Difference.NONE && start != end) {
			buf.append(",").append(1 + end);
		}
		return buf.toString();
	}

	public static String[] read(String fileName) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			List contents = new ArrayList();
			String in;
			while ((in = br.readLine()) != null) {
				contents.add(in);
			}
			return (String[]) contents.toArray(new String[] {});
		} catch (Exception e) {
			System.err.println("error reading " + fileName + ": " + e);
			System.exit(1);
			return null;
		}
	}

}
