/*
 * Copyright (C) 2008 Satish Chandra
 *
 * This file is part of LOC Calculator.
 *
 * LOC Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LOC Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LOC Calculator.  If not, see <http://www.gnu.org/licenses/>. 
 */
package org.satish.core;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

/**
 * @author satish
 */
public class PropReader {

	private static Map<String, PropertyResourceBundle> propMap = new HashMap<String, PropertyResourceBundle>();

	public static PropertyResourceBundle getInstance(String name) throws IOException {
		PropertyResourceBundle pr = propMap.get(name);
		if (pr == null) {
			synchronized (propMap) {
				pr = new PropertyResourceBundle(PropReader.class.getResourceAsStream(name));
				propMap.put(name, pr);
			}
		}
		return pr;
	}
}
