/*
 * Copyright (C) 2008 Satish Chandra
 *
 * This file is part of LOC Calculator.
 *
 * LOC Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LOC Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LOC Calculator.  If not, see <http://www.gnu.org/licenses/>. 
 */

package org.satish.benchmark;

/**
 * A simple Timer class with start and stop functions. Reports the duration in
 * milli seconds and seconds.
 * 
 * @author satish
 * Created on Sep 15, 2007
 */
public class Timer {
	private long start;
	private long end;

	/**
	 * Initialize object.
	 */
	public Timer() {
		super();
	}

	/**
	 * Notes the start time of the timer. 
	 */
	public void start() {
		start = System.nanoTime();
	}

	/**
	 * Notes the end time of the timer.
	 */
	public void end() {
		end = System.nanoTime();
	}

	/**
	 * Returns timer duration in milli seconds.
	 * @return long
	 */
	public double  getTimeInMillis() {
		return (end - start)/1000000D;
	}

	/**
	 * Returns timer duration in seconds.
	 * @return double
	 */
	public double getTimeInSec() {
		return (end - start) / 1000000000D;
	}

	/**
	 * Returns timer duration in nano seconds.
	 * @return long
	 */
	public long getTimeInNanos() {
		return (end - start);
	}

	/**
	 * Resets the timer. 
	 */
	public void reset() {
		start = 0;
		end = 0;
	}

}
