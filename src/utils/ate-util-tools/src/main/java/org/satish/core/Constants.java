/*
 * Copyright (C) 2008 Satish Chandra
 *
 * This file is part of LOC Calculator.
 *
 * LOC Calculator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LOC Calculator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LOC Calculator.  If not, see <http://www.gnu.org/licenses/>. 
 */
package org.satish.core;

/**
 * @author satish
 */
public class Constants {

	//Icon Types
	public static final String AUDIO = "audio";
	public static final String COMPRESSED = "compressed";
	public static final String IMAGE = "image";
	public static final String OFF_DOC = "offDoc";
	public static final String PDF = "pdf";
	public static final String PRESENTATION = "presentation";
	public static final String SOURCE_CODE = "sourceCode";
	public static final String SPREADSHEET = "spreadsheet";
	public static final String VIDEO = "video";
	public static final String WEB_PAGE = "webpage";
	public static final String DOCUMENT_OPEN = "document-open";
	public static final String CALCULATE = "calculate";
	public static final String GENERIC = "generic";
	public static final String DIRECTORY = "directory";
	
	//Properies files
	public static final String ICON_MAPPINGS = "/org/satish/resources/iconMappings.properties";
}
