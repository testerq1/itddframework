package ate.util;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.imageio.ImageIO;
import javax.swing.JLabel;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGeneratorContext;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.fop.render.ps.EPSTranscoder;
import org.apache.fop.render.ps.PSTranscoder;
import org.apache.fop.svg.AbstractFOPTranscoder;
import org.apache.fop.svg.PDFTranscoder;
import org.scilab.forge.jlatexmath.DefaultTeXFont;
import org.scilab.forge.jlatexmath.ResourceParseException;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;
import org.scilab.forge.jlatexmath.cyrillic.CyrillicRegistration;
import org.scilab.forge.jlatexmath.greek.GreekRegistration;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import ate.ua.RunFailureException;

/**
 * fop的maven依赖有问题，需要手动修改本地仓库中的fop-1.1.pom如下：
 * <pre>
 * {@code
 * <dependency>
 *    <groupId>avalon-framework</groupId>
 *    <artifactId>avalon-framework-api</artifactId>
 *    <version>4.2.0</version>
 *  </dependency>
 *  <dependency>
 *    <groupId>avalon-framework</groupId>
 *    <artifactId>avalon-framework-impl</artifactId>
 *    <version>4.2.0</version>
 * </dependency>
 * }
 * </pre>
 * 
 * JLatexMath是纯java的Tex renderer,缺少原生tex的宏，因此用于生成数学公式比较好
 * 
 * @author Administrator
 *
 */
public class JLatexMath extends AbstractTool{
	private TeXFormula formula;
	private Color foreground=Color.black;
	private Color background=Color.white;
	
	private Insets default_margin=new Insets(5,5,5,5);
	private int default_icon_size=20;
	private TeXFormula.TeXIconBuilder builder;
	
	public JLatexMath(){}

	public JLatexMath(String latex){
		set_formula(latex);
	}
	
	public static void add_macro_resource(String fname){
		InputStream in=JLatexMath.class.getClassLoader().getResourceAsStream(fname);
		
		TeXFormula.addPredefinedCommands(in);
	}
	
	public static void add_macro_file(String fname){		 
		try {
			TeXFormula.addPredefinedCommands(new FileInputStream(fname));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
	}
	
	public File create_png(String fname){
		formula.createPNG(TeXConstants.STYLE_DISPLAY, default_icon_size, fname, background, foreground);
		return new File(fname);
	}
	
	public TeXFormula get_formula(){
		return formula;
	}

	public TeXFormula.TeXIconBuilder new_icon_builder(){
		return new_icon_builder(default_icon_size);
	}
	
	public TeXFormula.TeXIconBuilder new_icon_builder(int size){
		if(formula==null)
			throw new RunFailureException("formula ot set");
		
		builder = formula.new TeXIconBuilder().setStyle(TeXConstants.STYLE_DISPLAY).setSize(size);
		return builder;
	}
	
	public File png( String fname){
		if(builder==null)
			builder=new_icon_builder();
		return png(fname,builder);
	}
	
	public File png( String fname, TeXFormula.TeXIconBuilder icon_builder){	
		TeXIcon icon=icon_builder.build();
		icon.setInsets(default_margin);
		
		BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = image.createGraphics();
		g2.setColor(background);
		g2.fillRect(0,0,icon.getIconWidth(),icon.getIconHeight());
		JLabel jl = new JLabel();
		jl.setForeground(foreground);
		icon.paintIcon(jl, g2, 0, 0);
		
		File file = new File(fname);
		try {
		    ImageIO.write(image, "png", file.getAbsoluteFile());
		} catch (IOException ex) { 
			ex.printStackTrace();
		}
		return file;
	}
	
	public void set_formula(String latex){
		formula = new TeXFormula(latex);		
	}
	
	public void set_icon_size(int default_icon_size) {
		this.default_icon_size = default_icon_size;
	}
	
	public void set_margin(int top, int left, int bottom, int right) {
		this.default_margin = new Insets(top,left,bottom,right);
	}
	
	public void setBackground(Color background) {
		this.background = background;
	}
	
	public void setForeground(Color foreground) {
		this.foreground = foreground;
	}  
	public static final int PDF = 0;
	public static final int PS = 1;
	public static final int EPS = 2;

	public boolean toSVG( String file, boolean fontAsShapes){
		return toSVG(builder,file,fontAsShapes);
	}
	
	public boolean toSVG( TeXFormula.TeXIconBuilder icon_builder, String file, boolean fontAsShapes){
		DOMImplementation domImpl = GenericDOMImplementation
				.getDOMImplementation();
		String svgNS = "http://www.w3.org/2000/svg";
		Document document = domImpl.createDocument(svgNS, "svg", null);
		SVGGeneratorContext ctx = SVGGeneratorContext.createDefault(document);
		SVGGraphics2D g2 = new SVGGraphics2D(ctx, fontAsShapes);

		DefaultTeXFont.registerAlphabet(new CyrillicRegistration());
		DefaultTeXFont.registerAlphabet(new GreekRegistration());
		TeXIcon icon=icon_builder.build();
		icon.setInsets(default_margin);		
		
		g2.setSVGCanvasSize(new Dimension(icon.getIconWidth(), icon
				.getIconHeight()));
		g2.setColor(background);
		g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());

		JLabel jl = new JLabel();
		jl.setForeground(foreground);
		icon.paintIcon(jl, g2, 0, 0);

		boolean useCSS = true;
		try {
			FileOutputStream svgs = new FileOutputStream(file);
			Writer out = new OutputStreamWriter(svgs, "UTF-8");
			g2.stream(out, useCSS);
			svgs.flush();
			svgs.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean SVGTo(String inSVG, String out, int type) {
		AbstractFOPTranscoder trans;
		switch (type) {
		case PDF:
			trans = new PDFTranscoder();
			break;
		case PS:
			trans = new PSTranscoder();
			break;
		case EPS:
			trans = new EPSTranscoder();
			break;
		default:
			trans = null;
		}

		try {
			TranscoderInput input = new TranscoderInput(new FileInputStream(
					inSVG));
			OutputStream os = new FileOutputStream(out);
			TranscoderOutput output = new TranscoderOutput(os);
			trans.transcode(input, output);
			os.flush();
			os.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
