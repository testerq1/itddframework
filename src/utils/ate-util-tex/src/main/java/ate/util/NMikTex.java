package ate.util;

import java.io.File;
import java.io.IOException;

import de.nixosoft.jlr.JLRConverter;
import de.nixosoft.jlr.JLRGenerator;

/**
 * 原生MikTex的一个封装，参考：
 * http://www.nixo-soft.de/tutorials/jlr/JLRTutorial.html
 * 
 * @author Ravi Huang
 *
 */
public class NMikTex extends AbstractTool{
	File workingDirectory;
	File template;
	JLRGenerator pdfGen;
	JLRConverter converter;
	public NMikTex(){
		this("src/test/template","invoiceTemplate.tex");
	}
	public NMikTex(String workingDir,String templateFile){
		this.workingDirectory=new  File(workingDir);
		this.template=new File(templateFile);
		converter=new JLRConverter(workingDirectory);
		pdfGen = new JLRGenerator();
	}
	
	public File dump(String script,String outputFile){
		try {
			File out=new File(outputFile);
			if(converter.parse(script, out))
				return out;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	public File dump(String outputTexFile){
		try {
			File out=new File(outputTexFile);
			if(converter.parse(template, out))
				return out.getAbsoluteFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void replace(String attr,Object o){
		converter.replace(attr, o);
	}
	
	public JLRConverter getConverter(){
		return converter;
	}
	
	public void setWorkingDirectory(String dir) {
		this.workingDirectory = new File(dir);;
	}

	public void setTemplate(String file) {
		this.template=new File(file);
	}
	
	public File to_pdf(File tex,String outDir){
		try {
			File out=new File(outDir);
			if(!out.exists())
				out.createNewFile();
			
			if(pdfGen.generate(tex, out, workingDirectory)){
				return pdfGen.getPDF();
			}else{
				log.error(pdfGen.getErrorMessage());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
