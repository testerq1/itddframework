package ate.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.testcase.Testcase;

public class LatexMathTest extends Testcase{
	@BeforeClass
	public void before() {
		LHOST.rm("target/*.png");
		LHOST.rm("target/*.svg");
		LHOST.rm("target/*.pdf");
		LHOST.rm("target/*.eps");
		LHOST.rm("target/*.ps");
		LHOST.rm("target/*.tex");
	}	
	
	@Test
	public void jlr(){
		NMikTex mt=new NMikTex("src/test/template","invoiceTemplate.tex");
		mt.replace("Number", "1");
		mt.replace("CustomerName", "Ivan Pfeiffer");
		mt.replace("CustomerStreet", "Schwarzer Weg 4");
		mt.replace("CustomerZip", "13505 Berlin");

		ArrayList<ArrayList<String>> services = new ArrayList<ArrayList<String>>();
		ArrayList<String> subservice1 = new ArrayList<String>();
		ArrayList<String> subservice2 = new ArrayList<String>();
		ArrayList<String> subservice3 = new ArrayList<String>();
		subservice1.add("Software");
		subservice1.add("50");
		subservice2.add("Hardware1");
		subservice2.add("500");
		subservice3.add("Hardware2");
		subservice3.add("850");

		services.add(subservice1);
		services.add(subservice2);
		services.add(subservice3);

		mt.replace("services", services);
		
		File tmp=mt.to_pdf(mt.dump("target/temp/invoice1.tex"), "target");
		assertTrue(tmp!=null&&tmp.isFile());
		log.info(tmp.getAbsolutePath());
		
	}
	public void macro_test(){
		JLatexMath.add_macro_resource("Package_Foo.xml");
		
        String latex = "\\begin{array}{l}";
        latex += "\\fooA{\\pi}{C}\\\\";
        latex += "\\mbox{A red circle }\\fooB{75.3}\\\\";
        latex += "\\mbox{A red disk }\\fooC[abc]{126.7}\\\\";
        latex += "\\mbox{An other red circle }\\fooD{159.81}[ab]";
        latex += "\\end{array}";
        JLatexMath lat=new JLatexMath(latex);
		File tmp=lat.png("target/macro.png");
		assertTrue(tmp!=null&&tmp.exists());	
    }
	
	public void example7(){
		String latex = "\\mbox{abc abc abc abc abc abc abc abc abc abc abc abc abc abc\\\\abc abc abc abc abc abc abc\\\\abc abc abc abc abc abc abc}\\\\1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1";
		JLatexMath lat=new JLatexMath(latex);
		lat.new_icon_builder().setSize(30)
		.setWidth(TeXConstants.UNIT_CM, 4, TeXConstants.ALIGN_LEFT)
		.setInterLineSpacing(TeXConstants.UNIT_CM, 0.5f);
		
		File tmp=lat.png("target/example7.png");
		assertTrue(tmp!=null&&tmp.exists());
		assertTrue(lat.toSVG("target/Example1.svg", false));
		assertTrue(lat.SVGTo("target/Example1.svg", "target/Example1.pdf", lat.PDF));
		assertTrue(lat.toSVG("target/Example1.svg", false));
		assertTrue(lat.toSVG("target/Example1_shaped.svg", true));
		assertTrue(lat.SVGTo("target/Example1.svg", "Example1.pdf", lat.PDF));
		assertTrue(lat.SVGTo("target/Example1_shaped.svg", "Example1_shaped.pdf", lat.PDF));
		assertTrue(lat.SVGTo("target/Example1.svg", "Example1.ps", lat.PS));
		assertTrue(lat.SVGTo("target/Example1.svg", "Example1.eps", lat.EPS));
		
		latex = "\\text{hello world}";
		lat.set_formula(latex);
		lat.new_icon_builder(16).setWidth(TeXConstants.UNIT_PIXEL, 256f, TeXConstants.ALIGN_CENTER)
		.setIsMaxWidth(true).setInterLineSpacing(TeXConstants.UNIT_PIXEL, 20f);
		tmp=lat.png("target/example71.png");
		assertTrue(tmp!=null&&tmp.exists());		
	}
	
	public void example5(){
		String latex = "\\begin{array}{|c|l|||r|c|}";
		latex += "\\hline";
		latex += "\\text{Matrix}&\\multicolumn{2}{|c|}{\\text{Multicolumns}}&\\text{Font sizes commands}\\cr";
		latex += "\\hline";
		latex += "\\begin{pmatrix}\\alpha_{11}&\\cdots&\\alpha_{1n}\\cr\\hdotsfor{3}\\cr\\alpha_{n1}&\\cdots&\\alpha_{nn}\\end{pmatrix}&\\Large \\text{Large Right}&\\small \\text{small Left}&\\tiny \\text{tiny Tiny}\\cr";
		latex += "\\hline";
		latex += "\\multicolumn{4}{|c|}{\\Huge \\text{Huge Multicolumns}}\\cr";
		latex += "\\hline";
		latex += "\\end{array}";
		
		JLatexMath lat=new JLatexMath(latex);
		File tmp=lat.create_png("target/example5.png");
		assertTrue(tmp!=null&&tmp.exists());
	}
	
	public void example4(){
		String latex = "\\begin{array}{|c|c|c|c|}\n";
		latex += "\\multicolumn{4}{c}{\\shadowbox{\\text{\\Huge An image from the \\LaTeX3 project}}}\\cr\n";
		latex += "\\hline\n";
		latex += "\\text{Left}\\includegraphics{lion.png}\\text{Right} & \\text{Left}\\includegraphics[width=3cm,interpolation=bicubic]{lion.png}\\text{Right} & \\text{Left}\\includegraphics[angle=45,width=3cm]{lion.png}\\text{Right} & \\text{Left}\\includegraphics[angle=160]{lion.png}\\text{Right} \\cr\n";
		latex += "\\hline\n";
		latex += "\\text{\\backslash includegraphics\\{lion.png\\}} & \\text{\\backslash includegraphics[width=3cm,interpolation=bicubic]\\{lion.png\\}} & \\text{\\backslash includegraphics[angle=45,width=3cm]\\{lion.png\\}} & \\text{\\backslash includegraphics[angle=160]\\{lion.png\\}}\\cr\n";
		latex += "\\hline\n";
		latex += "\\end{array}\n";
		JLatexMath lat=new JLatexMath(latex);
		File tmp=lat.png("target/example4.png");
		assertTrue(tmp!=null&&tmp.exists());
		
		latex = "\\begin{array}{cc}";
		latex += "\\fbox{\\text{A framed box with \\textdbend}}&\\shadowbox{\\text{A shadowed box}}\\cr";
		latex += "\\doublebox{\\text{A double framed box}}&\\ovalbox{\\text{An oval framed box}}\\cr";
		latex += "\\end{array}";
		
		lat.set_formula(latex);
		lat.set_icon_size(30);
		tmp=lat.png("target/example41.png",lat.new_icon_builder());
		assertTrue(tmp!=null&&tmp.exists());
	}
	
	public void example3(){
        String latex = "\\definecolor{gris}{gray}{0.9}";
        latex += "\\definecolor{noir}{rgb}{0,0,0}";
        latex += "\\definecolor{bleu}{rgb}{0,0,1}\\newcommand{\\pa}{\\left|}";
        latex += "\\begin{array}{c}";
        latex += "\\JLaTeXMath\\\\";
        latex += "\\begin{split}";
        latex += " &Тепловой\\ поток\\ \\mathrm{Тепловой\\ поток}\\ \\mathtt{Тепловой\\ поток}\\\\";
        latex += " &\\boldsymbol{\\mathrm{Тепловой\\ поток}}\\ \\mathsf{Тепловой\\ поток}\\\\";
        latex += "|I_2| &= \\pa\\int_0^T\\psi(t)\\left\\{ u(a,t)-\\int_{\\gamma(t)}^a \\frac{d\\theta}{k} (\\theta,t) \\int_a^\\theta c(\\xi) u_t (\\xi,t)\\,d\\xi\\right\\}dt\\right|\\\\";
        latex += "&\\le C_6 \\Bigg|\\pa f \\int_\\Omega \\pa\\widetilde{S}^{-1,0}_{a,-} W_2(\\Omega, \\Gamma_1)\\right|\\ \\right|\\left| |u|\\overset{\\circ}{\\to} W_2^{\\widetilde{A}}(\\Omega;\\Gamma_r,T)\\right|\\Bigg|\\\\";
        latex += "&\\\\";
        latex += "&\\textcolor{magenta}{\\mathrm{Produit\\ avec\\ Java\\ et\\ \\LaTeX\\ par\\ }\\mathscr{C}\\mathcal{A}\\mathfrak{L}\\mathbf{I}\\mathtt{X}\\mathbb{T}\\mathsf{E}}\\\\";
        latex += "&\\begin{pmatrix}\\alpha&\\beta&\\gamma&\\delta\\\\\\aleph&\\beth&\\gimel&\\daleth\\\\\\mathfrak{A}&\\mathfrak{B}&\\mathfrak{C}&\\mathfrak{D}\\\\\\boldsymbol{\\mathfrak{a}}&\\boldsymbol{\\mathfrak{b}}&\\boldsymbol{\\mathfrak{c}}&\\boldsymbol{\\mathfrak{d}}\\end{pmatrix}\\quad{(a+b)}^{\\frac{n}{2}}=\\sqrt{\\sum_{k=0}^n\\tbinom{n}{k}a^kb^{n-k}}\\quad \\Biggl(\\biggl(\\Bigl(\\bigl(()\\bigr)\\Bigr)\\biggr)\\Biggr)\\\\";
        latex += "&\\forall\\varepsilon\\in\\mathbb{R}_+^*\\ \\exists\\eta>0\\ |x-x_0|\\leq\\eta\\Longrightarrow|f(x)-f(x_0)|\\leq\\varepsilon\\\\";
        latex += "&\\det\\begin{bmatrix}a_{11}&a_{12}&\\cdots&a_{1n}\\\\a_{21}&\\ddots&&\\vdots\\\\\\vdots&&\\ddots&\\vdots\\\\a_{n1}&\\cdots&\\cdots&a_{nn}\\end{bmatrix}\\overset{\\mathrm{def}}{=}\\sum_{\\sigma\\in\\mathfrak{S}_n}\\varepsilon(\\sigma)\\prod_{k=1}^n a_{k\\sigma(k)}\\\\";
        latex += "&\\Delta f(x,y)=\\frac{\\partial^2f}{\\partial x^2}+\\frac{\\partial^2f}{\\partial y^2}\\qquad\\qquad \\fcolorbox{noir}{gris}{n!\\underset{n\\rightarrow+\\infty}{\\sim} {\\left(\\frac{n}{e}\\right)}^n\\sqrt{2\\pi n}}\\\\";
        latex += "&\\sideset{_\\alpha^\\beta}{_\\gamma^\\delta}{\\begin{pmatrix}a&b\\\\c&d\\end{pmatrix}}\\xrightarrow[T]{n\\pm i-j}\\sideset{^t}{}A\\xleftarrow{\\overrightarrow{u}\\wedge\\overrightarrow{v}}\\underleftrightarrow{\\iint_{\\mathds{R}^2}e^{-\\left(x^2+y^2\\right)}\\,\\mathrm{d}x\\mathrm{d}y}";
        latex += "\\end{split}\\\\";
        latex += "\\rotatebox{30}{\\sum_{n=1}^{+\\infty}}\\quad\\mbox{Mirror rorriM}\\reflectbox{\\mbox{Mirror rorriM}}";
        latex += "\\end{array}";
        JLatexMath lat=new JLatexMath(latex);
		File tmp=lat.png("target/example3.png");
		assertTrue(tmp!=null&&tmp.exists());
		
	}
	public void example2(){
		String latex = "\\begin{array}{l}";
		latex += "\\forall\\varepsilon\\in\\mathbb{R}_+^*\\ \\exists\\eta>0\\ |x-x_0|\\leq\\eta\\Longrightarrow|f(x)-f(x_0)|\\leq\\varepsilon\\\\";
		latex += "\\det\\begin{bmatrix}a_{11}&a_{12}&\\cdots&a_{1n}\\\\a_{21}&\\ddots&&\\vdots\\\\\\vdots&&\\ddots&\\vdots\\\\a_{n1}&\\cdots&\\cdots&a_{nn}\\end{bmatrix}\\overset{\\mathrm{def}}{=}\\sum_{\\sigma\\in\\mathfrak{S}_n}\\varepsilon(\\sigma)\\prod_{k=1}^n a_{k\\sigma(k)}\\\\";
		latex += "\\sideset{_\\alpha^\\beta}{_\\gamma^\\delta}{\\begin{pmatrix}a&b\\\\c&d\\end{pmatrix}}\\\\";
		latex += "\\int_0^\\infty{x^{2n} e^{-a x^2}\\,dx} = \\frac{2n-1}{2a} \\int_0^\\infty{x^{2(n-1)} e^{-a x^2}\\,dx} = \\frac{(2n-1)!!}{2^{n+1}} \\sqrt{\\frac{\\pi}{a^{2n+1}}}\\\\";
		latex += "\\int_a^b{f(x)\\,dx} = (b - a) \\sum\\limits_{n = 1}^\\infty  {\\sum\\limits_{m = 1}^{2^n  - 1} {\\left( { - 1} \\right)^{m + 1} } } 2^{ - n} f(a + m\\left( {b - a} \\right)2^{-n} )\\\\";
		latex += "\\int_{-\\pi}^{\\pi} \\sin(\\alpha x) \\sin^n(\\beta x) dx = \\textstyle{\\left \\{ \\begin{array}{cc} (-1)^{(n+1)/2} (-1)^m \\frac{2 \\pi}{2^n} \\binom{n}{m} & n \\mbox{ odd},\\ \\alpha = \\beta (2m-n) \\\\ 0 & \\mbox{otherwise} \\\\ \\end{array} \\right .}\\\\";
		latex += "L = \\int_a^b \\sqrt{ \\left|\\sum_{i,j=1}^ng_{ij}(\\gamma(t))\\left(\\frac{d}{dt}x^i\\circ\\gamma(t)\\right)\\left(\\frac{d}{dt}x^j\\circ\\gamma(t)\\right)\\right|}\\,dt\\\\";
		latex += "\\begin{array}{rl} s &= \\int_a^b\\left\\|\\frac{d}{dt}\\vec{r}\\,(u(t),v(t))\\right\\|\\,dt \\\\ &= \\int_a^b \\sqrt{u'(t)^2\\,\\vec{r}_u\\cdot\\vec{r}_u + 2u'(t)v'(t)\\, \\vec{r}_u\\cdot\\vec{r}_v+ v'(t)^2\\,\\vec{r}_v\\cdot\\vec{r}_v}\\,\\,\\, dt. \\end{array}\\\\";
		latex += "\\end{array}";
		JLatexMath lat=new JLatexMath(latex);
		File tmp=lat.png("target/example2.png");
		assertTrue(tmp!=null&&tmp.exists());
		
	}
	public void basic(){
		String latex = "\\begin{array}{lr}\\mbox{\\textcolor{Blue}{Russian}}&\\mbox{\\textcolor{Melon}{Greek}}\\\\";
		latex += "\\mbox{" + "привет мир".toUpperCase() + "}&\\mbox{" + "γειά κόσμο".toUpperCase() + "}\\\\";
		latex += "\\mbox{привет мир}&\\mbox{γειά κόσμο}\\\\";
		latex += "\\mathbf{\\mbox{привет мир}}&\\mathbf{\\mbox{γειά κόσμο}}\\\\";
		latex += "\\mathit{\\mbox{привет мир}}&\\mathit{\\mbox{γειά κόσμο}}\\\\";
		latex += "\\mathsf{\\mbox{привет мир}}&\\mathsf{\\mbox{γειά κόσμο}}\\\\";
		latex += "\\mathtt{\\mbox{привет мир}}&\\mathtt{\\mbox{γειά κόσμο}}\\\\";
		latex += "\\mathbf{\\mathit{\\mbox{привет мир}}}&\\mathbf{\\mathit{\\mbox{γειά κόσμο}}}\\\\";
		latex += "\\mathbf{\\mathsf{\\mbox{привет мир}}}&\\mathbf{\\mathsf{\\mbox{γειά κόσμο}}}\\\\";
		latex += "\\mathsf{\\mathit{\\mbox{привет мир}}}&\\mathsf{\\mathit{\\mbox{γειά κόσμο}}}\\\\";
		latex += "&\\\\";
		latex += "\\mbox{\\textcolor{Salmon}{Bulgarian}}&\\mbox{\\textcolor{Tan}{Serbian}}\\\\";
		latex += "\\mbox{здравей свят}&\\mbox{Хелло уорлд}\\\\";
		latex += "&\\\\";
		latex += "\\mbox{\\textcolor{Turquoise}{Bielorussian}}&\\mbox{\\textcolor{LimeGreen}{Ukrainian}}\\\\";
		latex += "\\mbox{прывітаньне Свет}&\\mbox{привіт світ}\\\\";
		latex += "\\end{array}";
		JLatexMath lat=new JLatexMath(latex);
		File tmp=lat.png("target/basic.png");
		assertTrue(tmp!=null&&tmp.exists());
	}
	  
}
