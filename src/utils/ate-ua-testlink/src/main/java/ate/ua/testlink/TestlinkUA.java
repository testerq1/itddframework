package ate.ua.testlink;

import java.net.URL;

import ate.AbstractURIHandler;
import ate.ua.UAOption;
import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;

public class TestlinkUA extends AbstractURIHandler {
	public static final UAOption<String> TESTLINK_KEY = new UAOption<String>(
			"TESTLINK_KEY");

	public TestLinkAPI api;
	
	String uri="/lib/api/xmlrpc.php";
	String key;
	public TestlinkUA() {}
	
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		 if(option==UA_URI){
			 String tmp=value.toString().toLowerCase().replace("testlink://", "http://");
			 uri=tmp+uri;	        
		  } else if(option==TESTLINK_KEY)
			key=value.toString();		 
		 
		return this;
	}

	@Override
	public void teardown() {
		
	}

	@Override
	public void startup() {
		try {
			api=new TestLinkAPI(new URL(uri),key);
		} catch (Exception e) {
			e.printStackTrace();
		} 

	}

	@Override
	public String get_default_schema() {
		return "testlink";
	}

	@Override
	public boolean is_ready() {
		return api!=null;
	}

}
