package ate.test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import ate.AbstractURIHandler;
import ate.testcase.UATestcase;
import ate.ua.testlink.TestlinkUA;
import br.eti.kinoshita.testlinkjavaapi.model.TestProject;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class TestlinkUATest extends UATestcase {
	HttpTestServer server;
	TestlinkUA tl;

	@DataProvider(name = "createTestProjectDataProvider")
	public Object[][] createData() {
		return new Object[][] { { "Test Project created with TestNG", true,
				true, true, false, false, true } };
	}

	private void loadXMLRPCMockData(String xmlFile) {

		URL url = getClass().getResource(
				"/br/eti/kinoshita/testlinkjavaapi/testdata/" + xmlFile);
		File file = FileUtils.toFile(url);
		String mockXml;
		try {
			mockXml = FileUtils.readFileToString(file);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		server.setMockResponseBody(mockXml);

	}

	@Test(dataProvider = "createTestProjectDataProvider")
	public void testCreateProject(String notes, Boolean enableRequirements,
			Boolean enableTestPriority, Boolean enableAutomation,
			Boolean enableInventory, Boolean isActive, Boolean isPublic) {
		loadXMLRPCMockData("tl.createTestProject.xml");

		TestProject project = null;

		try {
			Random random = new Random(System.currentTimeMillis());

			project = tl.api.createTestProject(
					"Sample project " + System.currentTimeMillis(),
					"" + random.nextInt(9999), notes, enableRequirements,
					enableTestPriority, enableAutomation, enableInventory,
					isActive, isPublic);
		} catch (TestLinkAPIException e) {
			assertFalse(e.getMessage(), 1 == 1);
		}

		assertTrue(null != project);
		assertTrue(project.getId() > 0);
		assertFalse(project.isActive());
		
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		server = new HttpTestServer();
		loadXMLRPCMockData("tl.checkDevKey.xml");
		server.start();

		tl = new TestlinkUA();
		tl.build(AbstractURIHandler.UA_URI, "testlink://localhost:8888/testlink")
				.build(TestlinkUA.TESTLINK_KEY, "devKey");
		tl.startup();
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() throws Exception {
		server.stop();
		super.AfterClass();
	}
}
