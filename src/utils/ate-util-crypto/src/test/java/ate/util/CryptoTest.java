package ate.util;

import static org.testng.AssertJUnit.assertTrue;
import org.testng.annotations.Test;
import static ate.util.Crypto.*;

public class CryptoTest {
	@Test
	public void test() {
		assertTrue("19DD5C4C9331049D0BDA"
				.equalsIgnoreCase(Crypto.aead(
						"91945D3F4DCBEE0BF45EF52255F095A4",
						"BECAF043B0A23D843194BA972C66DEBD", "FA3BFD4806EB53FA",
						"F7FB",ENC)));
		assertTrue("F7FB"
				.equalsIgnoreCase(Crypto.aead(
						"91945D3F4DCBEE0BF45EF52255F095A4",
						"BECAF043B0A23D843194BA972C66DEBD", "FA3BFD4806EB53FA",
						"19DD5C4C9331049D0BDA",DEC)));
	}

}
