package ate.util;

import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.util.encoders.Hex;

public class Crypto {
	public final static int ENC=0;
	public final static int DEC=1;
	
	static{
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	}
	public static String aead(String k,String n,String a,String x, int mode){
		byte[] K = Hex.decode(k);
		byte[] N = Hex.decode(n);
		byte[] A = Hex.decode(a);
		byte[] X = Hex.decode(x);
        try {
			Cipher eax = Cipher.getInstance("AES/EAX/NoPadding", "BC");
			SecretKeySpec key = new SecretKeySpec(K, "AES");
			IvParameterSpec iv = new IvParameterSpec(N);
			if(mode==ENC)
				eax.init(Cipher.ENCRYPT_MODE, key, iv);
			else
				eax.init(Cipher.DECRYPT_MODE, key, iv);
				

			eax.updateAAD(A);
			byte[] c = eax.doFinal(X);
			return Hex.toHexString(c);
		} catch (Exception e) {			
			e.printStackTrace();
		}
        return null;
	}
	
	public static String caesar_decode(String enc, int offset) {
        return caesar_encode(enc, -offset);
    }
	/**
	 * caesar cipher
	 * @param enc
	 * @param offset
	 * @return
	 */
    public static String caesar_encode(String enc, int offset) {
        offset = offset % 26 + 26;
        StringBuilder encoded = new StringBuilder();
        for (char i : enc.toLowerCase().toCharArray()) {
            if (Character.isLetter(i)) {
                int j = (i - 'a' + offset) % 26;
                encoded.append((char) (j + 'a'));
            } else {
                encoded.append(i);
            }
        }
        return encoded.toString();
    }
}
