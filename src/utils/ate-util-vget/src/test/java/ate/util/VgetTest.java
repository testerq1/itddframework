package ate.util;

import org.testng.annotations.Test;

import ate.testcase.Testcase;

public class VgetTest extends Testcase {

	@Test
	public void app(){
		Vget vget=new Vget();
		vget.app_managed("http://www.youtube.com/watch?v=Nj6PFaDmp6c");
	}
	
	public void wget(){
		Vget wget=new Vget();
		wget.download("http://www.dd-wrt.com/routerdb/de/download/D-Link/DIR-300/A1/ap61.ram/2049");
	}
	
	@Test
	public void youtube(){
		Vget wget=new Vget();
		wget.youtube("http://www.youtube.com/watch?v=Nj6PFaDmp6c");
	}
}