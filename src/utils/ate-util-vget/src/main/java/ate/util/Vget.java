package ate.util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.github.axet.vget.VGet;
import com.github.axet.vget.info.VideoInfo;
import com.github.axet.vget.info.VideoInfoUser;
import com.github.axet.vget.info.VideoInfo.VideoQuality;
import com.github.axet.wget.WGet;
import com.github.axet.wget.info.DownloadInfo;
import com.github.axet.wget.info.DownloadInfo.Part;
import com.github.axet.wget.info.DownloadInfo.Part.States;


/**
 * https://github.com/axet/vget
 * @author ravi huang
 *
 */
public class Vget {
	File target_dir;

	/**
	 * 指定下载目的路径
	 * 
	 * @param home
	 */
	public Vget(String home) {
		target_dir = new File(home);
		if (target_dir.exists() && target_dir.isDirectory())
			return;
		target_dir.mkdirs();
	}

	public Vget() {
		target_dir = new File(".");
	}

	public void download(String path) {
		try {
			URL url = new URL(path);
			WGet w = new WGet(url, target_dir);
			w.download();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (RuntimeException allDownloadExceptions) {
			allDownloadExceptions.printStackTrace();
		}
	}

	public void youtube(String url) {

		try {
			VGet v = new VGet(new URL(url), this.target_dir);
			v.download();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	long last;

	public void app_managed(String url) {
		try {
			final VideoInfo info = new VideoInfo(new URL(url));

			AtomicBoolean stop = new AtomicBoolean(false);
			Runnable notify = new Runnable() {
				@Override
				public void run() {
					VideoInfo i1 = info;
					DownloadInfo i2 = i1.getInfo();

					// notify app or save download state
					// you can extract information from DownloadInfo info;
					switch (i1.getState()) {
					case EXTRACTING:
					case EXTRACTING_DONE:
					case DONE:
						System.out.println(i1.getState() + " "
								+ i1.getVideoQuality());
						break;
					case RETRYING:
						System.out.println(i1.getState() + " " + i1.getDelay());
						break;
					case DOWNLOADING:
						long now = System.currentTimeMillis();
						if (now - 1000 > last) {
							last = now;

							String parts = "";

							List<Part> pp = i2.getParts();
							if (pp != null) {
								// multipart download
								for (Part p : pp) {
									if (p.getState().equals(States.DOWNLOADING)) {
										parts += String
												.format("Part#%d(%.2f) ",
														p.getNumber(),
														p.getCount()
																/ (float) p
																		.getLength());
									}
								}
							}

							System.out.println(String.format("%s %.2f %s",
									i1.getState(),
									i2.getCount() / (float) i2.getLength(),
									parts));
						}
						break;
					default:
						break;
					}
				}
			};

			// [OPTIONAL] limit maximum quality, or do not call this function if
			// you wish maximum quality available.
			//
			// if youtube does not have video with requested quality, program
			// will raise an exception.
			VideoInfoUser user = new VideoInfoUser();
			user.setUserQuality(VideoQuality.p480);

			VGet v = new VGet(info, target_dir);

			// [OPTIONAL] call v.extract() only if you d like to get video title
			// before start download. or just skip it.
			v.extract(user, stop, notify);
			System.out.println(info.getTitle());

			v.download(user, stop, notify);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
