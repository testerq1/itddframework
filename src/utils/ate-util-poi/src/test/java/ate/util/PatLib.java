package ate.util;

import ate.testcase.UATestcase;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.poi.ss.usermodel.Sheet;
import org.testng.annotations.Test;

public class PatLib extends UATestcase {
	HashMap<String,HashMap> hmpattern = new HashMap<String,HashMap>();
	ExcelUtil excel = new ExcelUtil("pattern.xls");
	ArrayList<String> cnames=new ArrayList<String>();
	String[] rnames=new String[600];	
	int maxmask=0;
	
	void dump(){
		int rows=0;
		Object[][] list=new Object[rnames.length][cnames.size()];
		list[0]=cnames.toArray();
		
		for(int i=1;i<rnames.length;i++){			
			if(rnames[i]==null)
				continue;
			
			HashMap hm=hmpattern.get(rnames[i]);			
			if(hm==null){
				log.error(rnames[i]);
				continue;
			}
			Object[] cols=new Object[cnames.size()];
			list[i][0]=(int)hm.get("mask");
			list[i][1]=(String)rnames[i];
			
			for(int j=2;j<cnames.size();j++){
				Object v=hm.get("pattern."+cnames.get(j));
				if(v!=null)
					list[i][j]=(String)v+"\n";
			}	
			
		}
		
	    if(excel.switch_sheet("特征库")!=null){
	    	excel.update_sheet(list);
	    }else
	    	excel.update_sheet("特征库", list);
	    excel.dump();
	}
	
	@org.testng.annotations.BeforeClass
	public void beforeclass(){
		cnames.add("mask");
		cnames.add("App");
		
	}
	
	@Test
	public void lib() throws IOException {		
		Sheet sheet=excel.sheet;
		
		File[] fs = new File("./").listFiles(new FilenameFilter() {
			public boolean accept(File dir, String fname) {
				return fname != null && fname.toLowerCase().endsWith(".pat");
			}
		});
			
		
		for (int i = 0; i < fs.length; i++) {	
			String fname=fs[i].getName();
			if(fname.toLowerCase().startsWith("tm"))
				continue;
			
			String columName=fname.split("\\.")[0];			
			cnames.add(columName);
			
			FileInputStream f = new FileInputStream(fs[i]);
			BufferedReader dr = new BufferedReader(new InputStreamReader(f));
			String line = null;
			String currApp=null;
			int mask=0;

			HashMap currHm=null;
			while ((line =dr.readLine())!= null) {
				line=line.trim();
				if(line.startsWith("#mask")){
					String[] s=line.split(",");
					mask=Integer.parseInt(s[0].split("=")[1].trim());
					if(mask>maxmask)
						maxmask=mask;
					
					currApp=s[1].trim();
					if(rnames[mask]==null){
						rnames[mask]=currApp;						
					}
					
					if(hmpattern.get(currApp)==null){
						hmpattern.put(currApp, new HashMap());		
						hmpattern.get(currApp).put("mask",mask);
					}				
					
					currHm=hmpattern.get(currApp);		
					if(currHm.get("mask")!=null&&!currHm.get("mask").equals(mask)){
						log.error("error mask: {} {}",mask,currApp);
					}
					
										
				}else if(line.length()==0||currHm==null){
					continue;
				}
				else if(!line.startsWith("#")){
					String pname="pattern."+columName.toLowerCase();
					String[] split=line.split(",");				
					if(line.contains("mask=")&&!line.contains("mask="+mask+","))
						log.error("error mask: {}({}) in {}: {}",currApp,mask,columName,line);
					
					if(line.indexOf("exPatNum=")!=-1&&line.indexOf("exPatNum=0")==-1){						
						line=split[0]+","+line.substring(line.indexOf("exPatNum=")+11);
					}else
						line=split[0];
						
					if(currHm.get(pname)==null)
						currHm.put(pname,line);
					else
						currHm.put(pname, currHm.get(pname)+"\r\n"+line);
				}				
			}
			
		}
		dump();		
	}
	
	void printall(){
		Iterator<String> set=hmpattern.keySet().iterator();
		while(set.hasNext()){
			String key=set.next();
			HashMap hm=hmpattern.get(key);
			System.out.println(key+":");
			Iterator<String> seti=hm.keySet().iterator();				
			while(seti.hasNext()){
				String keyi=seti.next();
				System.out.println(keyi+" "+ hm.get(keyi));					
			}
			System.out.println();
		}
		
		System.out.println("列名:");
		Iterator it=cnames.iterator();
		while(it.hasNext()){
			System.out.println(it.next());
		}
		
		System.out.println("行名:");
		for(int i=0;i<=maxmask;i++){
			if(rnames[i]!=null){
				System.out.println(i+":"+rnames[i]);
			}else 
				System.out.println(i);
		}
	}
}
