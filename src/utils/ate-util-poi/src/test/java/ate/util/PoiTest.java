package ate.util;

import java.io.File;
import org.testng.annotations.*;

import ate.testcase.Testcase;

public class PoiTest extends Testcase{
	
	@BeforeClass
	public void beforeclass(){
		LHOST.rm("test.xls");
		LHOST.rm("test1.xls");
	}
	
	@Test
	public void basic(){
		Object[][] oo=new Object[][]{{1.0,2.0,3.0},{4.0,5.0,6.0}};
		ExcelUtil f=new ExcelUtil("test.xls");
		f.update_sheet("test", oo);
		f.dump();
		f.switch_sheet("test");
		assertTrue(new File("test.xls").exists());
		assertTrue(3==f.count_non_empty_columns(),f.count_non_empty_columns()+"");
		assertTrue(2==f.count_non_empty_rows(),f.count_non_empty_rows()+"");
		assertTrue(X2X0.is_equal(oo, f.load_sheet()));
		
		f.dump("test1.xls");
		assertTrue(new File("test1.xls").exists());
	}
}
