package ate.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * HSSF is the POI Project's pure Java implementation of the Excel '97(-2007) file format. 
 * XSSF is the POI Project's pure Java implementation of the Excel 2007 OOXML (.xlsx) file format.
 * 
 * @author ravi huang
 *
 */
public class ExcelUtil {
	String filename;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String filename = "Book1.xls";
		try {
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(
					filename));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			HSSFSheet sheet = wb.getSheetAt(0);
			System.out.println(wb.getNumberOfSheets());

			HSSFRow row = sheet.getRow(0);
			// System.out.println(sheet.getLastRowNum());
			// System.out.println(row.getPhysicalNumberOfCells());
			Writer fw1 = new BufferedWriter(new FileWriter("cn.properties"));
			Writer fw2 = new BufferedWriter(new FileWriter("en.properties"));
			fw1.write("");
			fw2.write("");
			for (int i = 0; i < sheet.getPhysicalNumberOfRows(); i++) {
				String cell2 = sheet.getRow(i).getCell(2).getStringCellValue();
				String cell3 = sheet.getRow(i).getCell(3).getStringCellValue();
				String cell4 = sheet.getRow(i).getCell(4).getStringCellValue();
				fw1.append(cell3 + "=" + cell2 + "\r\n");
				fw2.append(cell4 + "=" + cell2 + "\r\n");
			}
			fw1.flush();
			fw2.flush();

			Reader r1 = new BufferedReader(new FileReader("cn.properties"));
			Reader r2 = new BufferedReader(new FileReader("en.properties"));

			Properties cn = new Properties();
			Properties en = new Properties();

			cn.load(r1);
			en.load(r2);

			Iterator keys = cn.keySet().iterator();
			while (keys.hasNext()) {
				String key = keys.next().toString();
				System.out.println(key + "=" + cn.get(key));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	Workbook workbook;

	Sheet sheet;

	public ExcelUtil(String fileName) {
		this.filename = fileName;
		try {
			if(fileName.endsWith(".xlsx")){
				if (!new File(fileName).exists())				
					workbook=new XSSFWorkbook();				
				else
					workbook = new XSSFWorkbook(new FileInputStream(fileName));
			}else{
				if (!new File(fileName).exists())
					workbook=new HSSFWorkbook();				
				else
					workbook = new HSSFWorkbook(new FileInputStream(fileName));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ExcelUtil(String fileName, String sheetName) throws IOException {
		this(fileName);
		sheet = workbook.getSheet(sheetName);
	}

	public int count_non_empty_columns() {
		Row firstRow = sheet.getRow(0);
		return firstEmptyCellPosition(firstRow);
	}

	public int count_non_empty_rows() {
		for (int i = sheet.getLastRowNum(); i >= 0; i--) {
			Row r = sheet.getRow(i);
			if (r != null) {
				//从第0行开始
				return i+1;
			}
		}
		return 0;
	}
	
	public void clear_empty_rows(Sheet sheet){
		 for (int i = sheet.getLastRowNum(); i >=0;i--) {  
             Row r = sheet.getRow(i);  
             if(r == null){  
                 // 如果是空行（即没有任何数据、格式），直接把它以下的数据往上移动  
                 sheet.shiftRows(i+1, sheet.getLastRowNum(),-1);  
                 continue;  
             }  
             boolean flag = false;  
             for(Cell c:r){  
                 if(c.getCellType() != Cell.CELL_TYPE_BLANK){  
                     flag = true;  
                     break;  
                 }  
             }  
             if(flag){  
                 i++;  
                 continue;  
             }  
             else{//如果是空白行（即可能没有数据，但是有一定格式）  
                 if(i == sheet.getLastRowNum())//如果到了最后一行，直接将那一行remove掉  
                     sheet.removeRow(r);  
                 else//如果还没到最后一行，则数据往上移一行  
                     sheet.shiftRows(i+1, sheet.getLastRowNum(),-1);  
             }  
         }  
	}
	
	public Sheet update_sheet(String SheetName, Object[][] list) {
		Sheet sheet = workbook.getSheet(SheetName);
		if(sheet==null)
			sheet = workbook.createSheet(SheetName);
		
			
		Row row = null;
		Cell cell = null;

		for (int rowNum = 0; rowNum < list.length; rowNum++) {
			row =  sheet.createRow(rowNum);
			for (int cellNum = 0; cellNum < list[rowNum].length; cellNum++) {
				cell = row.createCell(cellNum);
				Object v = list[rowNum][cellNum];
				if (v instanceof String)
					cell.setCellValue((String) v);
				else if (v instanceof Integer)
					cell.setCellValue((Integer) v);
				else if (v instanceof Double)
					cell.setCellValue((Double) v);
				else if (v instanceof Boolean)
					cell.setCellValue((Boolean) v);
				else if (v instanceof Date)
					cell.setCellValue((Date) v);
				else if (v instanceof RichTextString)
					cell.setCellValue((RichTextString) v);
				else if (v instanceof Calendar)
					cell.setCellValue((Calendar) v);
			}
		}

		return sheet;
	}

	public void dump() {
		dump(this.filename);
	}

	public void dump(String fname) {
		try {
			FileOutputStream out = new FileOutputStream(fname);
			workbook.write(out);

			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 返回cell的值
	 * 
	 * @param iRow
	 * @param iColumn
	 * @return 空行时返回null；cell为空时返回字符串"",否则返回cell的值
	 */
	public Object get_cell_value(int iRow, int iColumn) {
		Row row = sheet.getRow(iRow);
		
		if (isEmpty(row))
			return null;

		Cell cell = row.getCell(iColumn);
		if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) {
			return "";
		} else {
			return objectFrom(cell);
		}
	}

	public Object[][] load_sheet() {
		return load_sheet(sheet);
	}

	public Object[][] load_sheet(String sheetName) throws IOException {
		sheet = workbook.getSheet(sheetName);
		return load_sheet(sheet);
	}

	public Sheet switch_sheet(String sheetName) {
		sheet = workbook.getSheet(sheetName);
		return sheet;
	}

	public Sheet switch_sheet(Sheet sheet) {
		this.sheet = sheet;
		return sheet;
	}

	public void update_sheet(Object[][] list) {
		String sheetName = sheet.getSheetName();
		if (list != null && list.length != sheet.getLastRowNum()) {
			for(int i=0;i<sheet.getLastRowNum();i++)
					sheet.removeRow(sheet.getRow(i));
		}
		sheet = update_sheet(sheetName, list);
	}



	/**
	 * Count the number of columns, using the number of non-empty cells in the
	 * first row.
	 */
	private int countNonEmptyColumns(Sheet sheet) {
		Row firstRow = sheet.getRow(0);
		return firstEmptyCellPosition(firstRow);
	}

	private Object evaluateCellFormula(final Cell cell) {
		FormulaEvaluator evaluator = workbook.getCreationHelper()
				.createFormulaEvaluator();
		CellValue cellValue = evaluator.evaluate(cell);
		Object result = null;

		if (cellValue.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
			result = cellValue.getBooleanValue();
		} else if (cellValue.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			result = cellValue.getNumberValue();
		} else if (cellValue.getCellType() == Cell.CELL_TYPE_STRING) {
			result = cellValue.getStringValue();
		}

		return result;
	}

	private int firstEmptyCellPosition(Row cells) {
		int columnCount = 0;
		for (Cell cell : cells) {
			if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
				break;
			}
			columnCount++;
		}
		return columnCount;
	}

	private Object getNumericCellValue(final Cell cell) {
		Object cellValue;
		if (DateUtil.isCellDateFormatted(cell)) {
			cellValue = new Date(cell.getDateCellValue().getTime());
		} else {
			cellValue = cell.getNumericCellValue();
		}
		return cellValue;
	}

	private boolean isEmpty(Row row) {
		Cell firstCell = row.getCell(0);
		boolean rowIsEmpty = (firstCell == null)
				|| (firstCell.getCellType() == Cell.CELL_TYPE_BLANK);
		return rowIsEmpty;
	}

	private Object[][] load_sheet(Sheet sheet) {
		int numberOfColumns = countNonEmptyColumns(sheet);
		int numberOfRows = sheet.getLastRowNum() + 1;

		Object[][] data = new Object[numberOfRows][numberOfColumns];

		for (int rowNum = 0; rowNum < numberOfRows; rowNum++) {
			Row row = sheet.getRow(rowNum);
			if (isEmpty(row)) {
				break;
			} else {
				for (int column = 0; column < numberOfColumns; column++) {
					Cell cell = row.getCell(column);
					data[rowNum][column] = objectFrom(cell);
				}
			}
		}

		return data;
	}

	private Object objectFrom(Cell cell) {
		if(cell==null)
			return null;
		switch (cell.getCellType()){
		    case Cell.CELL_TYPE_BLANK:
		        return "";
		    case Cell.CELL_TYPE_NUMERIC:
		        return getNumericCellValue(cell);
		    case Cell.CELL_TYPE_STRING:    
		        return cell.getRichStringCellValue().getString();
		    case Cell.CELL_TYPE_BOOLEAN:
		        return cell.getBooleanCellValue();
		    case Cell.CELL_TYPE_FORMULA:
		        return evaluateCellFormula(cell);
		    case Cell.CELL_TYPE_ERROR:
		    default:
		        break;
		}
		return null;
	}

}
