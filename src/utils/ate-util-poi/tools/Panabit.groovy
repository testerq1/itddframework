
//println "Before metaprogramming"
//3.times{
//  println Math.random()
//}
//
//Math.metaClass.static.random = {->
//  return 0.5
//}
//
//println "After metaprogramming"
//3.times{
//  println Math.random()
//}
//
//
//def method(bool) {
//    try {
//        if (bool) throw new Exception("foo")
//        1
//    } catch(e) {
//        2
//    } finally {
//        3
//    }
//}
//println method(false) == 1
//println method(true) == 2

import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class Panabit {
	static class Node {
		public String value;

		public Node parent;

		public List childs=new ArrayList()

		public Node(String value) {
			this.value = value;;
		}

		public boolean isLeaf(){
			return childs.size()==0
		}

		public void setParent(Node node){
			this.parent=node;
		}
		public void addChild(Node node){
			this.childs.add(node);
		}
	}

	def deep=new int[10]
	def cnt=0;
	public void printNode(Node node,int depth){
		deep[depth]++
		String id="";
		for(int i=0;i<10;i++){
			if(deep[i]!=0)
				id+=deep[i]+"."
			else
				break;
		}
		println id+","+node.value
		//println 
		depth++
		if(node.childs.size()==0)
			cnt++;
		for(int i=0;i<node.childs.size();i++){
			printNode(node.childs[i],depth)
		}
		deep[depth]=0
	}
	public void toCSV(){
		def nodes=new HashMap();
		def root=new Node("root")
		nodes.put("0",root)
		def file = new File('panabit.txt')
		file.eachLine {
			if(it.trim().size()>0){
				def ss=it.split (",")

				def node=new Node(ss[2].trim().substring(1,ss[2].length()-2))
				def parent=nodes.get(ss[1].trim())
				node.setParent(parent)
				nodes.put(ss[0].trim(), node)
				parent.addChild(node);
				//println "${ss[0]} ${ss[1]} ${ss[2]}"
			}
		}		
		printNode(root,0)
		println "class: "+root.childs[0].childs.size()
		println "item:" + cnt
	}

	public void group(String excelFilename){
		POIFSFileSystem fs=new POIFSFileSystem(new FileInputStream(excelFilename));
		HSSFWorkbook wb=new HSSFWorkbook(fs);
		HSSFSheet sheet= wb.getSheetAt(0);
		
		int num=sheet.getLastRowNum()		
		sheet.ungroupRow(0, num)
		int[] groupfrom=new int[10]
		int currdeep=0;
		for(int i=num-1;i>num-31;i--){
			//HSSFRow row=sheet.getRow(0);
			int type=sheet.getRow(i).getCell(0).getCellType();
			def c0="";
			if(type==0)
				c0= sheet.getRow(i).getCell(0).getNumericCellValue()+""
			else if(type==1)
				c0= sheet.getRow(i).getCell(0).getStringCellValue()+"0"
				
			int id=	c0.split("\\.").length
			
			if(currdeep<=id)
				currdeep=id
			else {
				sheet.groupRow(groupfrom[currdeep-2]+1,i)
				println "group from "+(groupfrom[currdeep-2]+1) + " to "+(i)
				currdeep=id
				groupfrom[id-2]=0 
			}	
			if(groupfrom[id-2]<i)
				groupfrom[id-2]=i
			
			println c0+" "+ c0.split("\\.").length
			
		}
		println "write to file"
		try{
			FileOutputStream out = new FileOutputStream(excelFilename);
			wb.write(out);
			out.close();
		}catch(Exception e){
			print e
		}
		
		//System.out.println(wb.getNumberOfSheets());
		
		//System.out.println();
		
	}

	public static void main(String[] args){
		new Panabit().group("panabit.xls");
	}
}



