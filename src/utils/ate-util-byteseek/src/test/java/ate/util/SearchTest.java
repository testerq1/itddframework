package ate.util;

import java.util.Collection;
import java.util.Vector;

import org.ahocorasick.trie.Token;
import org.ahocorasick.trie.Trie;
import org.testng.annotations.Test;

import ate.testcase.Testcase;

public class SearchTest extends Testcase{
    @Test
    public void test(){
        WuManber objWM=new WuManber();
        Vector<String> vKey=new Vector<String>();
        vKey.add("法轮功");
        vKey.add("中国");
        vKey.add("共产党");
        vKey.add("ABC");
        vKey.add("B52");

        if(objWM.addFilterKeyWord(vKey, 0)){
            
            String sResult=objWM.macth("我的法 轮功是否在中国的共产党中的ABC是不是革命党中的A B C是不是革命党中的ABC是不是革命B52",new Vector(0));
            
            System.out.println(sResult);
        }
        
        objWM.clear();
    }
    @Test 
    /**
     * https://github.com/robert-bor/aho-corasick
     */
    public void ahocorasick(){
        String speech = "The Answer to the Great Question... Of Life, " +
                "the Universe and Everything... Is... Forty-two,' said " +
                "Deep Thought, with infinite majesty and calm.";
        Trie trie = new Trie().removeOverlaps().onlyWholeWords().caseInsensitive();
        trie.addKeyword("great question");
        trie.addKeyword("forty-two");
        trie.addKeyword("deep thought");
        Collection<Token> tokens = trie.tokenize(speech);
        StringBuffer html = new StringBuffer();
        html.append("<html><body><p>");
        for (Token token : tokens) {
            if (token.isMatch()) {
                html.append("<i>");
            }
            html.append(token.getFragment());
            if (token.isMatch()) {
                html.append("</i>");
            }
        }
        html.append("</p></body></html>");
        System.out.println(html);
    }
}
