=JRI编译(winxp)=
JRI和R的编译环境是一样的
 * 下载R source: http://cran.rstudio.com/sources.html
 * 下载安装R：http://cran.rstudio.com/bin/windows/base/
 * 下载安装rtools:
   # http://cran.r-project.org/doc/manuals/r-patched/R-admin.html#The-Windows-toolset
   # http://cran.r-project.org/bin/windows/Rtools/
   # http://cran.r-project.org/bin/windows/Rtools/Rtools.txt
   # cygwin下还要安装readline devel
 * 安装mingw  
 * 设置环境变量
   # PATH=D:\Rtools\bin;%PATH%
     R_HOME=d:\PROGRA~1\R\R-2.15.0
     JAVA_HOME=D:\program files\Java\jdk1.6.0_31
     ? LIBRARY_PATH=C:\MinGW\lib
     
 * 下载source，编译
copy一份%R_HOME%\etc\i386文件夹下的Makeconf文件到%R_HOME%\etc
> svn co svn://svn.rforge.net/org/trunk/rosuda/JRI JRI 
> dos2unix configure.win
> sh configure.win
> make
 
=run test= 
必须先设置环境变量(winxp)
 > set PATH=D:\Rtools\bin;%PATH%;D:\Program Files\R\R-3.1.0\bin\i386
 > set R_HOME=D:\Program Files\R\R-3.1.0
 > run rtest 
 
=run under eclipse=
必须配置环境变量：
Run Configurations->Environment:
  PATH=D:\Rtools\bin;%PATH%;D:\Program Files\R\R-3.1.0\bin\i386
  R_HOME=D:\Program Files\R\R-3.1.0

=install packages= 
R Console
> install.packages("plotrix")
> install.packages("rjava")
> install.packages("gclus")
