package ate.test;

/*
 * #%L
 * ate-ua-r
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import java.lang.reflect.Method;
import java.util.Enumeration;

import org.rosuda.JRI.*;
import org.testng.annotations.*;

import ate.rm.dev.Host;
import ate.testcase.UATestcase;
import ate.ua.r.*;

public class RTest1 extends UATestcase{
	@BeforeClass
	public void before() {
		LHOST.rm("target/*.png");
		LHOST.rm("target/*.eps");
		LHOST.rm("target/*.svg");
	}	
	
	@Test
	public void line(){		
		RUA ua=new RUA();
		String script="library(gclus);"
				+"dta <- mtcars[c(1,3,5,6)];"
				+"dta.r <- abs(cor(dta));"
				+"dta.col <- dmat.color(dta.r);"
				+"dta.o <- order.single(dta.r);" 
				+"cpairs(dta, dta.o, panel.colors=dta.col, gap=.5,"
				+"main='Variables Ordered and Colored by Correlation' )";
		assertTrue(ua.png(script,"target/line.png"));
	}
	
	public void bar(){
		RUA ua=new RUA();
		String script="counts <- table(mtcars$gear);"+
		"barplot(counts, main='Car Distribution', horiz=TRUE,"+
		  "names.arg=c('3 Gears', '4 Gears', '5 Gears'))";
		assertTrue(ua.png(script,"target/bar.png"));
	}
	
	public void svg(){
		RUA ua=new RUA();
		String script="slices <- c(10, 12,4, 16, 8);"+
		"lbls <- c('US', 'UK', 'Australia', 'Germany', 'France');"+		
		"pie(slices, labels = lbls, main='Pie Chart of Countries')";
		assertTrue(ua.svg(script,"target/pie.svg",6,4));
	}
	
	public void pie(){
		RUA ua=new RUA();
		String script="slices <- c(10, 12,4, 16, 8);"+
		"lbls <- c('US', 'UK', 'Australia', 'Germany', 'France');"+		
		"pie(slices, labels = lbls, main='Pie Chart of Countries')";
		assertTrue(ua.png(script,"target/pie.png"));
		
		script="slices <- c(10, 12, 4, 16, 8);"
				+"lbls <- c('US', 'UK', 'Australia', 'Germany', 'France');"
				+"pct <- round(slices/sum(slices)*100);"
				+"lbls <- paste(lbls, pct);"
				+"lbls <- paste(lbls,'%',sep='');" 
				+"pie(slices,labels = lbls, col=rainbow(length(lbls)),main='Pie Chart of Countries');";
		assertTrue(ua.png(script,"target/Annotated_pie.png"));
		
		script="library(plotrix);"
				+"slices <- c(10, 12, 4, 16, 8);"
				+"lbls <- c('US', 'UK', 'Australia', 'Germany', 'France');"
				+"pie3D(slices,labels=lbls,explode=0.1,main='Pie Chart of Countries ')";
		assertTrue(ua.png(script,"target/3dpie.png"));
	}
	
	public void plot_png(){
		RUA ua=new RUA();
		assertTrue(ua.png("plot(sin, -pi, 2*pi)","target/test.png"));
		REXP tmp=ua.eval("iris");
		assertTrue(tmp!=null);
		log.info(tmp.toString());
	}
	
	
	
	public static void main(String[] args) {		
		// just making sure we have the right version of everything
		if (!Rengine.versionCheck()) {
			System.err.println("** Version mismatch - Java files don't match library version.");
			System.exit(1);
		}
		System.out.println("Creating Rengine (with arguments)");
		// 1) we pass the arguments from the command line
		// 2) we won't use the main loop at first, we'll start it later
		// (that's the "false" as second argument)
		// 3) the callbacks are implemented by the TextConsole class above
		Rengine re = new Rengine(args, false, new TextConsole());
		System.out.println("Rengine created, waiting for R");
		// the engine creates R is a new thread, so we should wait until it's
		// ready
		if (!re.waitForR()) {
			System.out.println("Cannot load R");
			return;
		}

		/*
		 * High-level API - do not use RNI methods unless there is no other way
		 * to accomplish what you want
		 */
		try {
			REXP x;
			re.eval("data(iris)", false);
			System.out.println(x = re.eval("iris"));
			// generic vectors are RVector to accomodate names
			RVector v = x.asVector();
			if (v.getNames() != null) {
				System.out.println("has names:");
				for (Enumeration e = v.getNames().elements(); e.hasMoreElements();) {
					System.out.println(e.nextElement());
				}
			}
			// for compatibility with Rserve we allow casting of vectors to
			// lists
			RList vl = x.asList();
			String[] k = vl.keys();
			if (k != null) {
				System.out.println("and once again from the list:");
				int i = 0;
				while (i < k.length)
					System.out.println(k[i++]);
			}

			// get boolean array
			System.out.println(x = re.eval("iris[[1]]>mean(iris[[1]])"));
			// R knows about TRUE/FALSE/NA, so we cannot use boolean[] this way
			// instead, we use int[] which is more convenient (and what R uses
			// internally anyway)
			int[] bi = x.asIntArray();
			{
				int i = 0;
				while (i < bi.length) {
					System.out.print(bi[i] == 0 ? "F " : (bi[i] == 1 ? "T " : "NA "));
					i++;
				}
				System.out.println("");
			}

			// push a boolean array
			boolean by[] = { true, false, false };
			re.assign("bool", by);
			System.out.println(x = re.eval("bool"));
			// asBool returns the first element of the array as RBool
			// (mostly useful for boolean arrays of the length 1). is should
			// return true
			System.out.println("isTRUE? " + x.asBool().isTRUE());

			// now for a real dotted-pair list:
			System.out.println(x = re.eval("pairlist(a=1,b='foo',c=1:5)"));
			RList l = x.asList();
			if (l != null) {
				int i = 0;
				String[] a = l.keys();
				System.out.println("Keys:");
				while (i < a.length)
					System.out.println(a[i++]);
				System.out.println("Contents:");
				i = 0;
				while (i < a.length)
					System.out.println(l.at(i++));
			}
			System.out.println(re.eval("sqrt(36)"));
		} catch (Exception e) {
			System.out.println("EX:" + e);
			e.printStackTrace();
		}

		// Part 2 - low-level API - for illustration purposes only!
		// System.exit(0);

		// simple assignment like a<-"hello" (env=0 means use R_GlobalEnv)
		long xp1 = re.rniPutString("hello");
		re.rniAssign("a", xp1, 0);

		// Example: how to create a named list or data.frame
		double da[] = { 1.2, 2.3, 4.5 };
		double db[] = { 1.4, 2.6, 4.2 };
		long xp3 = re.rniPutDoubleArray(da);
		long xp4 = re.rniPutDoubleArray(db);

		// now build a list (generic vector is how that's called in R)
		long la[] = { xp3, xp4 };
		long xp5 = re.rniPutVector(la);

		// now let's add names
		String sa[] = { "a", "b" };
		long xp2 = re.rniPutStringArray(sa);
		re.rniSetAttr(xp5, "names", xp2);

		// ok, we have a proper list now
		// we could use assign and then eval "b<-data.frame(b)", but for now
		// let's build it by hand:
		String rn[] = { "1", "2", "3" };
		long xp7 = re.rniPutStringArray(rn);
		re.rniSetAttr(xp5, "row.names", xp7);

		long xp6 = re.rniPutString("data.frame");
		re.rniSetAttr(xp5, "class", xp6);

		// assign the whole thing to the "b" variable
		re.rniAssign("b", xp5, 0);

		{
			System.out.println("Parsing");
			long e = re.rniParse("data(iris)", 1);
			System.out.println("Result = " + e + ", running eval");
			long r = re.rniEval(e, 0);
			System.out.println("Result = " + r + ", building REXP");
			REXP x = new REXP(re, r);
			System.out.println("REXP result = " + x);
		}
		{
			System.out.println("Parsing");
			long e = re.rniParse("iris", 1);
			System.out.println("Result = " + e + ", running eval");
			long r = re.rniEval(e, 0);
			System.out.println("Result = " + r + ", building REXP");
			REXP x = new REXP(re, r);
			System.out.println("REXP result = " + x);
		}
		{
			System.out.println("Parsing");
			long e = re.rniParse("names(iris)", 1);
			System.out.println("Result = " + e + ", running eval");
			long r = re.rniEval(e, 0);
			System.out.println("Result = " + r + ", building REXP");
			REXP x = new REXP(re, r);
			System.out.println("REXP result = " + x);
			String s[] = x.asStringArray();
			if (s != null) {
				int i = 0;
				while (i < s.length) {
					System.out.println("[" + i + "] \"" + s[i] + "\"");
					i++;
				}
			}
		}
		{
			System.out.println("Parsing");
			long e = re.rniParse("rnorm(10)", 1);
			System.out.println("Result = " + e + ", running eval");
			long r = re.rniEval(e, 0);
			System.out.println("Result = " + r + ", building REXP");
			REXP x = new REXP(re, r);
			System.out.println("REXP result = " + x);
			double d[] = x.asDoubleArray();
			if (d != null) {
				int i = 0;
				while (i < d.length) {
					System.out.print(((i == 0) ? "" : ", ") + d[i]);
					i++;
				}
				System.out.println("");
			}
			System.out.println("");
		}
		{
			REXP x = re.eval("1:10");
			System.out.println("REXP result = " + x);
			int d[] = x.asIntArray();
			if (d != null) {
				int i = 0;
				while (i < d.length) {
					System.out.print(((i == 0) ? "" : ", ") + d[i]);
					i++;
				}
				System.out.println("");
			}
		}

		re.eval("print(1:10/3)");

		if (true) {
			// so far we used R as a computational slave without REPL
			// now we start the loop, so the user can use the console
			System.out.println("Now the console is yours ... have fun");
			re.startMainLoop();
		} else {
			re.end();
			System.out.println("end");
		}
	}
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		//super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
