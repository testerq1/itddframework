package ate.ua.r;

import java.io.File;
import java.util.Date;

import org.rosuda.JRI.REXP;
import org.rosuda.JRI.Rengine;

import ate.ua.RunFailureException;
import ate.util.AbstractTool;

public class RUA extends AbstractTool{
	Rengine re;
	TextConsole console;
	int width=8;
	int height=6;
	
	public RUA() {
		if (System.getenv("R_HOME") == null) {
			log.error("R_HOME or PATH envirment variable not set");
			throw new Error("R_HOME or PATH envirment variable not set");
		}
		console=new TextConsole();
		re = new Rengine(null, false, console);
		
		// re = new Rengine();
		if (!re.waitForR()) {
			throw new RunFailureException("Cannot load R");
		}
	}
	/**
	 * eval字符串<br>
	 * eval("iris");
	 * @param script
	 * @return
	 */
	public REXP eval(String script) {
		console.reset();
		String rexp = "{"+script+"}";
		log.debug(rexp);
		
		REXP rr=re.eval(rexp);
		re.end();
		
		return rr;
	}
	/**
	 * 读一个文件，然后执行eval
	 * @param evalFile
	 * @return
	 */
	public REXP eval_file(String evalFile){
		return eval(read_all_lines(evalFile));
	}
	
	public String get_console_output(){
		return console.get_output();
	}
	/**
	 * 根据evalScript生成一个png图片<br>
	 * plot_png("plot(sin, -pi, 2*pi)","target/test.png")
	 * @param eval
	 * @param fName
	 * @return
	 */
	public boolean png(String evalScript, String fName) {
		long time=new Date().getTime();
		String rexp = "{png('" + fName + "',width ="+width+", height="+height+", units = 'in',res = 360);" + evalScript + "; dev.off();}";
		log.debug(rexp);
		console.reset();
		REXP rr=re.eval(rexp);
		re.end();
		
		File png = new File(fName);
		return png.exists()&&png.lastModified()>time&&png.length()>0;
	}
	
	public boolean svg(String evalScript, String fName,int width,int heigh) {
		long time=new Date().getTime();
		String rexp = "{svg('" + fName + "',width ="+width+", height="+height+");" + evalScript + "; dev.off();}";
		log.debug(rexp);
		console.reset();
		REXP rr=re.eval(rexp);
		re.end();
		
		File png = new File(fName);
		return png.exists()&&png.lastModified()>time&&png.length()>0;
	}
}
