package ate.utils;

import java.util.ArrayList;

import org.testng.annotations.Test;

import ate.testcase.Testcase;

public class I18nTest extends Testcase {

    @Test
    public void mime() {
        FileInfo fi = new FileInfo("src/test/java/ate/utils/I18nTest.java");
        assertTrue("text/x-java".equals(fi.mimetype()));
        assertTrue("Java source file".equals(fi.desc()));
        assertTrue("UTF-8".equals(fi.codec()));
    }

    @Test
    public void langDetect() {
        LangDetect ld = new LangDetect();
        assertTrue("zh-cn".equals(ld.detect("中国")));
    }

    @Test
    public void UChardetTest() {
        UChardet ua = UChardet.create_chardet();        
        assertTrue("UTF-8".equalsIgnoreCase(ua.detect("http://www.baidu.com")));
        assertTrue("Shift_JIS".equalsIgnoreCase(ua
                .detect("http://www.amazon.co.jp")));
        assertTrue("GB18030".equalsIgnoreCase(ua.detect("http://www.sohu.com")));
        assertTrue("UTF-8".equalsIgnoreCase(ua.detect("http://www.ifeng.com")));
        assertTrue("UTF-8".equalsIgnoreCase(ua.detect("中国人".getBytes())));
        assertTrue("UTF-8".equalsIgnoreCase(ua.detect("http://www.sina.com")));
        // assertTrue("Big5".equalsIgnoreCase(ua.detect("http://www.google.com.hk")));
        assertTrue("UTF-8".equalsIgnoreCase(ua.detect("http://www.ibm.com")));
        log.info(ua.detect("http://www.sohu.com"));
    }

    @Test
    public void ChardetTest1() {
        Chardet ua = Chardet.create_chardet();
        ArrayList<String> al = ua.detect("http://www.baidu.com");
        assertTrue("UTF-8".equalsIgnoreCase(al.get(0)));

        al = ua.detect("http://www.amazon.co.jp");
        assertTrue("UTF-8".equalsIgnoreCase(al.get(0)));

//        al = ua.detect("http://www.amazon.co.jp");
//        assertTrue("Shift_JIS".equalsIgnoreCase(al.get(0)));

        al = ua.detect("中国人".getBytes());
        // Eclipse工程配置了java文件保存为UTF-8类型时
        assertTrue("UTF-8".equalsIgnoreCase(al.get(0)), al.get(0));

        al = ua.detect("http://www.ifeng.com");
        // log.info(al.get(0));
        assertTrue("UTF-8".equalsIgnoreCase(al.get(0)));

        al = ua.detect("http://www.sohu.com");
        assertTrue("GB2312".equalsIgnoreCase(al.get(0)));

        al = ua.detect("http://www.sina.com");
        assertTrue("UTF-8".equalsIgnoreCase(al.get(0)));

        // al=ua.detect("http://www.google.com.hk");
        // assertTrue("Big5".equalsIgnoreCase(al.get(0)));

        al = ua.detect("http://www.ibm.com");
        assertTrue("UTF-8".equalsIgnoreCase(al.get(0)));
    }

}
