package ate.utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.mozilla.universalchardet.UniversalDetector;

import ate.ua.RunFailureException;

/**
 * 使用mozilla UniversalDetector检测
 * @author Administrator
 *
 */
public class UChardet {
	UniversalDetector detector;
	private UChardet(){
		detector = new UniversalDetector(null);
	}
	
	public static UChardet create_chardet() {
		return new UChardet();
	}
	
	public String detect(byte[] bs){
		detector.handleData(bs, 0, bs.length);
		detector.dataEnd();		
		
		String encoding = detector.getDetectedCharset();	     
	    detector.reset();
	    return encoding;
	}
	
	public String detect(String url) {
		URL u;
		try {
			u = new URL(url);
			BufferedInputStream imp = new BufferedInputStream(u.openStream());
			return detect(imp);
		} catch (Exception e) {
			throw new RunFailureException(e);
		}
	}
	
	public String detect(InputStream fis) {
		int nread;
		byte[] buf = new byte[1024];
        try {
			while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
			    detector.handleData(buf, 0, nread);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
        detector.dataEnd();
        String encoding = detector.getDetectedCharset();	     
	    detector.reset();
	    return encoding;
	}
}
