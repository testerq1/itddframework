package ate.utils;

import java.io.File;
import java.util.ArrayList;

import com.cybozu.labs.langdetect.*;

/**
 * 检测语言种类
 * @author Administrator
 *
 */
public class LangDetect {
    public static String profileDirectory="conf"+File.separator+"langdet";
    
    public LangDetect(){
        try {
            DetectorFactory.loadProfile(profileDirectory);
        } catch (LangDetectException e) {
            e.printStackTrace();
        }
    }
    public String detect(String text){
        Detector detector;
        try {
            detector = DetectorFactory.create();
            detector.append(text);
            return detector.detect();
        } catch (LangDetectException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public ArrayList detect_p(String text){
        Detector detector;
        try {
            detector = DetectorFactory.create();
            detector.append(text);
            detector.detect();
            
            ArrayList<Language> langlist = detector.getProbabilities();
            return langlist;
        } catch (LangDetectException e) {
            e.printStackTrace();
        }
        return null;
    }
}
