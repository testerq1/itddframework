package ate.utils;

import java.io.File;

import ate.util.AbstractTool;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;

public class FileInfo extends AbstractTool{
	MagicMatch magic;
	String fname;
	Chardet ua= Chardet.create_chardet();
	public FileInfo(String fpath){
		fname=fpath;
		try {
			magic = Magic.getMagicMatch(new File(fpath), false, true);
		} catch (MagicParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MagicMatchNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MagicException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	
	public String mimetype() {
        return magic.getMimeType().toLowerCase();
    }
	
	public String desc(){
		return magic.getDescription();
	}
	
	public String codec(){
		return ua.detect_file(fname).get(0);
	}
}
