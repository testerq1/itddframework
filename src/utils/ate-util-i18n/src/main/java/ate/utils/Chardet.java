package ate.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import ate.ua.RunFailureException;

import chardet.nsDetector;
import chardet.nsICharsetDetectionObserver;
import chardet.nsPSMDetector;

public class Chardet {
	// language:
	public static final int ALL = 0;
	public static final int JAPANESE = 1;
	public static final int CHINESE = 2;
	public static final int SIMPLIFIED_CHINESE = 3;
	public static final int TRADITIONAL_CHINESE = 4;
	public static final int KOREAN = 5;

	private nsDetector det;
	private int lang;
	private boolean found;
	private String charset;

	private Chardet() {
		this(nsPSMDetector.ALL);
	}

	private Chardet(int lang) {
		this.lang = lang;
		det = new nsDetector(lang);
		det.Init(new nsICharsetDetectionObserver() {
			public void Notify(String cs) {
				found = true;
				charset = cs;
			}
		});
	}

	public static Chardet create_chardet() {
		return new Chardet();
	}

	/**
	 * 
	 * @param lang
	 *            language
	 * @return
	 */
	public static Chardet create_chardet(int lang) {
		return new Chardet(lang);
	}

	public ArrayList<String> detect(byte[] bs) {
		ArrayList<String> al = new ArrayList<String>();
		det.Reset();
		if (det.isAscii(bs, bs.length))
			al.add("ASCII");
		else if (det.DoIt(bs, bs.length, false)) {
			al.add(this.charset);
		} else {
			String prob[] = det.getProbableCharsets();
			for (int i = 0; i < prob.length; i++) {
				al.add(prob[i]);
			}
		}
		return al;
	}

	public ArrayList<String> detect(String url) {
		URL u;
		try {
			u = new URL(url);
			BufferedInputStream imp = new BufferedInputStream(u.openStream());
			return detect(imp);
		} catch (Exception e) {
			throw new RunFailureException(e);
		}
	}
	
	public ArrayList<String> detect_file(String filename) {
		try {
			FileInputStream fis = new FileInputStream(filename);
			return detect(fis);
		} catch (FileNotFoundException e) {
			throw new RunFailureException(e);
		}
	}

	public ArrayList<String> detect(InputStream imp) {
		ArrayList<String> al = new ArrayList<String>();
		byte[] buf = new byte[1024];
		int len;
		boolean isAscii = true;
		boolean done = false;

		det.Reset();
		try {
			while ((len = imp.read(buf, 0, buf.length)) != -1) {
				// Check if the stream is only ascii.
				if (isAscii)
					isAscii = det.isAscii(buf, len);
				// DoIt if non-ascii and not done yet.
				if (!isAscii && det.DoIt(buf, len, false)) {
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		det.DataEnd();

		if (isAscii) {
			charset = "ASCII";
			found = true;
		}

		if (found) {
			al.add(this.charset);
		} else {
			String prob[] = det.getProbableCharsets();
			for (int i = 0; i < prob.length; i++) {
				al.add(prob[i]);
			}
		}
		found = false;
		charset = null;
		return al;
	}

	/**
	 * client
	 * @param argv
	 * @throws Exception
	 */
	public static void main(String argv[]) throws Exception {
		if (argv == null || argv.length == 0)
			argv = new String[] { "http://www.sohu.com" };

		System.out.println("Detect: " + argv[0]);

		if (argv.length != 1 && argv.length != 2) {
			System.out.println("Usage: ChardetUA <url> [<languageHint>]");
			System.out.println("");
			System.out.println("Where <url> is http://...");
			System.out.println("For optional <languageHint>. Use following...");
			System.out.println("		1 => Japanese");
			System.out.println("		2 => Chinese");
			System.out.println("		3 => Simplified Chinese");
			System.out.println("		4 => Traditional Chinese");
			System.out.println("		5 => Korean");
			System.out.println("		6 => Dont know (default)");
			return;
		}
		int lang = (argv.length == 2) ? Integer.parseInt(argv[1])
				: nsPSMDetector.ALL;
		Chardet ua = Chardet.create_chardet(lang);
		ArrayList<String> al = ua.detect(argv[0]);
		if (al.size() == 1)
			System.out.println("\tCHARSET = " + al.get(0));
		else if (al.size() > 1) {
			for (String tmp : al) {
				System.out.println("\tProbable Charset = " + tmp);
			}
		} else {
			System.out.println("\tnot matched");
		}
	}
}
