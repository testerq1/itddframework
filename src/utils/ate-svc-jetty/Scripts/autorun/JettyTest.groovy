import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.AbstractURIHandler;
import ate.svc.jetty.JettyServer;
import ate.testcase.UATestcase;

public class JettyServerTest extends UATestcase {
	
	public void xml() {
		def tmp = create_ua("jetty:jetty.xml#join");		
		tmp.startup();
	}
	
	@Test
	public void mime() {
		STEP("build mime test server");
        def tmp = create_ua("jetty://127.0.0.1/b?descriptor=jetty_web.xml&resourcebase=webapps");      
        tmp.startup();
        tmp.add_mime_type("txt", "text/plain;charset=GB2312");
        tmp.add_mime_type("jar", "application/x-compressed");
        tmp.add_mime_type("alx", "application/xml");
        tmp.add_mime_type("jnlp", "application/xml");
        tmp.join();
	}
	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		// ;
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}