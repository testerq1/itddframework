package ate.svc.jetty;

import org.apache.velocity.tools.view.VelocityViewServlet;

import java.io.File;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.Template;
import org.apache.velocity.context.Context;

public class CustomerVelocityServlet extends VelocityViewServlet {
    public static final String SERVLET_TMP_DIR = "javax.servlet.context.tempdir";
	// 设置返回视图为text/html编码为gbk
	@Override
	protected void setContentType(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=gbk");
	}

	// 处理请求
	@Override
	public Template handleRequest(HttpServletRequest request,HttpServletResponse response,
									Context context) {
		Vector v = new Vector();
		v.add("one");
		v.add("two");
		v.add("three");
		context.put("list", v);
		try {			
		    File tmpDir = (File) getServletContext().getAttribute(SERVLET_TMP_DIR);
		    String fname=tmpDir.getAbsolutePath()+request.getServletPath()+request.getPathInfo();
		    //G:\itddframework\src/utils\ate-svc-jetty\webapps\servlet
		    //G:\itddframework\src/utils\ate-svc-jetty\webapps/servlet/displaylist.vm
			return this.getTemplate(fname);
		} catch (Exception e) {
			System.out.println("Error " + e);
		}
		return null;
	}
}
