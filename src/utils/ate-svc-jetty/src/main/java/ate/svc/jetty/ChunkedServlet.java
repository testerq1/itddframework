package ate.svc.jetty;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.chain.web.WebContext;

public class ChunkedServlet extends HttpServlet {
    public static final String SERVLET_TMP_DIR = "javax.servlet.context.tempdir";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        System.out.println("-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
        final int bufferSize = 65536;
        resp.setBufferSize(bufferSize);
        OutputStream outStream = resp.getOutputStream();
        String path=req.getPathInfo();        
        
        File tmpDir = (File) getServletContext().getAttribute(SERVLET_TMP_DIR);
        
        FileInputStream stream = null;
        try {
            File f=null;
            //增加Content-Type头
            String mime=this.getServletContext().getMimeType(path.substring(path.indexOf(".")));
            resp.setContentType(mime);
            
            //文件传输
            if(tmpDir.exists()&&tmpDir.isDirectory())
                f=new File(tmpDir,path);
            else 
                f=new File("."+path);
            //String tt=f.getAbsolutePath();
            stream = new FileInputStream(f);
            int bytesRead;
            byte[] buffer = new byte[bufferSize];
            while( (bytesRead = stream.read(buffer, 0, bufferSize)) > 0 ) {
                outStream.write(buffer, 0, bytesRead);
                outStream.flush();
            }
        } catch(Exception e){
            e.printStackTrace();
        }finally   {
            if( stream != null )
                stream.close();
            outStream.close();
        }
        resp.flushBuffer();
        
    }
}