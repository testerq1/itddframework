package ate.svc.jetty;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.eclipse.jetty.http.MimeTypes;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.xml.XmlConfiguration;

import ate.AbstractURIHandler;
import ate.ua.UAOption;

public class JettyServer extends AbstractURIHandler {
	Server server;
	boolean join = false;
	private HandlerCollection handlers;

	public JettyServer() {
	}

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			if (this.get_host() == null) {
				try {
					String file = this.get_schemeSpecificPart();
					InputStream source = new FileInputStream("conf"
							+ File.separator + new File(file));
					XmlConfiguration configuration = new XmlConfiguration(
							source);
					server = (Server) configuration.configure();
					
					handlers=(HandlerCollection)server.getHandlers()[0];

				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				if (port <= 0)
					port = 80;
				
				server = new Server(port);				
				handlers=new ContextHandlerCollection();
				
				server.setHandler(handlers);

				if (get_query_para("war")!=null) {
					create_war(path,get_query_para("war"));					
				} else {
					String descriptor=this.get_query_para("descriptor");
					if(descriptor==null)
						descriptor="jetty_web.xml";		
					
					String resourcebase=this.get_query_para("resourcebase");
					if(resourcebase==null)
						resourcebase="webapps";
					create_web_xml(path,"conf"+File.separator+descriptor,resourcebase);
				}
			}

			if ("join".equalsIgnoreCase(this.get_fragment()))
				join = true;
		}
		return this;
	}
	
	public WebAppContext create_web_xml(String contextpath,String descriptor,String resourcebase){
		WebAppContext context = new WebAppContext();
		context.setContextPath(contextpath);		
		context.setTempDirectory(new File("webapps"));
		context.setDescriptor(descriptor);					
		context.setResourceBase(resourcebase);
		context.setParentLoaderPriority(true);
		context.setClassLoader(Thread.currentThread().getContextClassLoader());
		this.handlers.addHandler(context);
		return context;
	}
	
	public WebAppContext create_war(String contextpath,String file){
		WebAppContext context = new WebAppContext();
		context.setContextPath(contextpath);
		context.setTempDirectory(new File("tmp"));
		context.setWar(file);
		context.setParentLoaderPriority(true);		
		this.handlers.addHandler(context);
		return context;
	}
	/**
	 * 默认返回第一个WebAppContext
	 * @param ext
	 * @param mime
	 * @return
	 */
	public JettyServer add_mime_type(String extension,String type){
	    WebAppContext wc=get_context();
	            
	    if(wc!=null){
	        MimeTypes mt=wc.getMimeTypes();
	        if(mt==null){
	            mt=new MimeTypes();
	            wc.setMimeTypes(mt);
	        }   
	        mt.addMimeMapping(extension, type);
	    }
	    
	    return this;
	}
	
	public WebAppContext get_context(){
	    WebAppContext wc=null;
        for(Handler tmp:handlers.getHandlers()){
            if(tmp instanceof WebAppContext){
                wc=(WebAppContext)tmp;
                break;
            }
        }
	    return wc;
	}
	
	public void startup() {
		try {
			server.start();
			if (join)
				server.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void join(){
		try {
			server.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void teardown() {
		try {
			server.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String get_default_schema() {
		return "jetty";
	}

	@Override
	public boolean is_ready() {
		return server != null && server.isRunning();
	}
}
