package ate.test;

import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.AbstractURIHandler;
import ate.svc.jetty.ChunkedServlet;
import ate.svc.jetty.JettyServer;
import ate.testcase.UATestcase;

public class JettyServerTest extends UATestcase {

    public void all_in_one_01() {
        log.info("test all in one server:");
        JettyServer tmp = new JettyServer();
        // jetty.xml只能单独使用？20140312
        // tmp.build(AbstractURIHandler.UA_URI, "jetty:jetty.xml#join");
        tmp.build(AbstractURIHandler.UA_URI, "jetty://127.0.0.1:8080/b#join");
        tmp.create_war("/a", "src/test/addressbook-1.0.war");
        tmp.startup();
    }

    public void all_in_one_02() {
        log.info("test all in one server:");
        JettyServer tmp = new JettyServer();
        tmp.build(AbstractURIHandler.UA_URI,
                "jetty://127.0.0.1:8080/a?war=src/test/addressbook-1.0.war#join");
        tmp.create_web_xml("/b", "jetty_web.xml", "webapps");
        tmp.startup();

    }

    public void all_in_one_03() {
        log.info("test all in one server:");
        JettyServer tmp = new JettyServer();
        tmp.build(AbstractURIHandler.UA_URI,
                "jetty://127.0.0.1:8080/a?war=src/test/addressbook-1.0.war");

        tmp.startup();
        tmp.create_web_xml("/b", "jetty_web.xml", "webapps");
        tmp.join();
    }

    public void war() {
        log.info("test init server with war:");
        JettyServer tmp = new JettyServer();
        tmp.build(AbstractURIHandler.UA_URI,
                "jetty://127.0.0.1:8080/a?war=src/test/addressbook-1.0.war#join");
        tmp.startup();
    }

    public void xml() {
        log.info("test init server with jetty.xml:");
        JettyServer tmp = new JettyServer();
        tmp.build(AbstractURIHandler.UA_URI, "jetty:jetty.xml#join");
        tmp.startup();
    }

    @Test
    public void jsp() {
        log.info("test jsp, need run with JDK:");
        JettyServer tmp = this
                .create_ua(
                        "jetty://127.0.0.1/b?descriptor=jetty_web.xml&resourcebase=webapps",
                        new JettyServer());
        tmp.startup();
        tmp.add_mime_type("txt", "text/plain;charset=GB2312")
                .add_mime_type("jar", "application/x-compressed")
                .add_mime_type("alx", "application/xml")
                .add_mime_type("jnlp", "application/xml")
                .add_mime_type("sis", "application/vnd.symbian.install")
                .add_mime_type("sisx", "application/vnd.symbian.install")
                .add_mime_type("jar", "application/java-archive")
                .add_mime_type("apk", "application/vnd.android.package-archive")
                .add_mime_type("cab", "application/vnd.ms-cab-compressed")
                .add_mime_type("exe", "application/x-msdownload")
                .add_mime_type("ipa", "application/octet-stream")
                .add_mime_type("cod", "application/vnd.rim.cod")
                .add_mime_type("alx", "application/vnd.rim.cod")
                .add_mime_type("prc", "application/x-mobipocket-ebook")
                .add_mime_type("zip", "application/zip")
                .add_mime_type("rar", "application/x-rar-compressed")
                .add_mime_type("elf", "application/octet-stream")
                .add_mime_type("JPEG", "image/jpeg")
                .add_mime_type("GIF", "image/gif")
                .add_mime_type("PNG", "image/png")
                .add_mime_type("BMP", "image/bmp")
                .add_mime_type("TIFF", "image/tiff")
                .add_mime_type("JPEG2000", "image/jp2")
                .add_mime_type("txt", "text/plain")
                .add_mime_type("html", "text/html")
                .add_mime_type("mht", "message/rfc822")
                .add_mime_type("xml", "application/xml")
                .add_mime_type("wml", "text/vnd.wap.wml")
                .add_mime_type("xhtml", "application/xhtml+xml");
        tmp.get_context().addServlet(ChunkedServlet.class, "/chunked/*");
        
        join();
    }

    @BeforeMethod
    public void beforeMethod(Method m) {
        super.BeforeMethod(m);
    }

    @AfterMethod
    public void afterMethod(Method m) {
        super.shutdown_all_ua();
        super.AfterMethod(m);
    }

    @BeforeClass
    public void beforeClass() {

        super.BeforeClass();
    }

    @AfterClass
    public void afterClass() {
        super.AfterClass();
    }
}
