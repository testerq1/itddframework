package ate.mts.gtp;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.AbstractURIHandler;
import ate.testcase.CsUATestcase;
import ate.ua.raw.HA;
import ate.ua.raw.PcapReader;
import ate.ua.raw.UAPcapPacket;

import java.lang.reflect.Method;

import ate.mts.gtp.GtpUA;

import com.devoteam.srit.xmlloader.core.coding.binary.ElementAbstract;
import com.devoteam.srit.xmlloader.gtp.MsgGtp;
import com.devoteam.srit.xmlloader.gtp.data.DataPDU;
import com.devoteam.srit.xmlloader.gtp.data.HeaderGTPV1;
import com.devoteam.srit.xmlloader.gtp.data.HeaderGTPV2;

public class GtpUATest extends CsUATestcase{
	@Test
	public void gtpu_withoutpdu(){
		GtpUA svr= (GtpUA)create_server("gtp://"+siip+":2152/udp");
		GtpUA clt= (GtpUA)create_client("gtp://"+sip+":2152/udp");
		//svr.set_dump_file("gtp.pcap");
		start_all_ua();
		//回放时，pcap文件的首包必须是该ip的，否则会从server发第一个包，出现错误
		String filename="smtp_172.16.5.211_p.pcap";
		PcapReader pr=new PcapReader(filename);
		
		pr.set_matcher(HA.SIP,filename.split("_")[1]);
		pr.start_read();
		
		MsgGtp msg=clt.read_request_from_template("gtpu_withoutpdu");		
		
		UAPcapPacket pkt=pr.get_packet();
		int cnt=0;
		while(pkt!=null){
			cnt++;
//			if(cnt==33)
//				System.out.println(pkt.get_packet());
//			else 
//				System.out.println(cnt);
			
			DataPDU data=msg.getDataPdu();
			Object resp=null;
			pkt.set_mtu(1460);	
			
			for(int i=2;i>1;i--){				
				int mod = (i%253)+1;
				int div= 7+(i/254);
				
				if(pkt.is_uac()){
					data.setData(pkt.get_payload(HA.SIP,"6.6."+div+"."+mod));
					clt.send_message(msg);
					resp=svr.recv_message();					
				}else{									
					data.setData(pkt.get_payload(HA.DIP,"6.6."+div+"."+mod));
//					if(cnt==33)
//						log.info(msg.toString());
					svr.send_message(msg);
					resp=clt.recv_message();
				}	
				assertTrue(resp!=null);		
			}
			
			
			pkt=pr.get_packet();
		}		
		pr.close();
	}
	@Test
	public void gtpu_withpdu(){
		GtpUA svr= (GtpUA)create_server("gtp://"+siip+":2152/udp");
		GtpUA clt= (GtpUA)create_client("gtp://"+sip+":2152/udp");
		start_all_ua();
		String filename="smtp_172.16.5.211_p.pcap";
		PcapReader pr=new PcapReader(filename);
		
		pr.set_matcher(HA.SIP,filename.split("_")[1]);
		pr.start_read();
		
		MsgGtp msg=clt.read_request_from_template("gtpu");		
		
		UAPcapPacket pkt=pr.get_packet();
		int cnt=0;
		while(pkt!=null){
			cnt++;
//			if(cnt==12)
//				System.out.println(pkt.get_packet());
//			else 
//				System.out.println(cnt);
			
			DataPDU data=msg.getDataPdu();
			Object resp=null;
			pkt.set_mtu(1460);	
			/*		
			data.setData(pkt.get_payload(HA.SIP,"1.1.1.1"));
			
			if(pkt.is_uac()){	
				log.info("-------------------uac");
				clt.send_message(msg);
				resp=svr.recv_message();				
			}else{
				log.info("-------------------uas");
				svr.send_message(msg);
				resp=clt.recv_message();
			}	
			assertTrue(resp!=null);*/		
			
			for(int i=2;i>1;i--){				
				data.setData("h01020301020300");				
				int mod = (i%253)+1;
				int div= 7+(i/254);				
				if(pkt.is_uac()){					
					data.appendData(pkt.build(HA.SIP,"6.6."+div+"."+mod).get_bytes());
					
					clt.send_message(msg);
					resp=svr.recv_message();					
				}else{									
					data.appendData(pkt.build(HA.DIP,"6.6."+div+"."+mod).get_bytes());
					if(cnt==33)
						log.info(msg.toString());
					svr.send_message(msg);
					resp=clt.recv_message();
				}	
				assertTrue(resp!=null);		
			}
			
			
			pkt=pr.get_packet();
		}		
		pr.close();
	}
	
	@Test
	public void gtp_u() throws Exception{
		GtpUA svr= (GtpUA)create_server("gtp://"+siip+":2152/udp");
		GtpUA clt= (GtpUA)create_client("gtp://"+sip+":2152/udp");
		start_all_ua();
		MsgGtp msg=clt.read_request_from_template("gtpu");
		clt.send_message(msg);
		Object o = svr.recv_message();
		assertTrue(o!=null);
	}
	@Test
	public void gtp_u_edit() throws Exception{
		GtpUA ua= new GtpUA();
		MsgGtp msg=ua.read_request_from_template("gtpu");		
		HeaderGTPV1 h=(HeaderGTPV1)msg.getHeader();		
		h.setTunnelEndpointId(123456l);		
		DataPDU data=msg.getDataPdu();
		
		data.setData(new byte[]{0x1,0x2});
		data.appendData(new byte[]{0x3,0x4});
		assertTrue(data.getData().length==4);
		assertTrue(data.getData()[3]==4);
		
		data.setData("h0102");
		data.appendData("h0304");
		log.info(msg.toString());
		assertTrue(data.getData().length==4);
		assertTrue(data.getData()[3]==4);		
	}
	@Test
	public void debug() throws Exception{
		GtpUA ua= new GtpUA();
		MsgGtp msg=ua.read_request_from_template("create_session");
		
		HeaderGTPV2 h=(HeaderGTPV2)msg.getHeader();
		
		h.setTunnelEndpointId(123456l);
		
		ElementAbstract o=msg.getElement("Bearer Context:93.EBI:73");
		assertTrue(o.toString().contains("name=\"EBI\" value=\"0\""));
		o.setFileld("EBI", "1");
		
		assertTrue(((HeaderGTPV2)msg.getHeader()).getTunnelEndpointId()==123456l);
		assertTrue(o.toString().contains("name=\"EBI\" value=\"1\""));
		System.out.println(o);
	}
	
	@Test
	public void gtp_c(){
		log.info("test");
		GtpUA svr= new GtpUA();
		svr.build(AbstractURIHandler.UA_URI, "gtp://"+siip+":2123/udp")
			.build(AbstractURIHandler.UA_TYPE, AbstractURIHandler.SERVER);
		svr.startup();
		
		GtpUA clt= new GtpUA();
		clt.build(AbstractURIHandler.UA_URI, "gtp://"+sip+":2123/udp")
		.build(AbstractURIHandler.UA_TYPE, AbstractURIHandler.CLIENT);
		clt.startup();
		
		MsgGtp msg=clt.read_request_from_template("create_session");				
		clt.send_message(msg);		
		Object o = svr.recv_message();		
		MsgGtp resp=svr.create_response_from_emplate(msg, "create_session");		
		svr.send_message(resp);		
		o=clt.recv_message();		
		System.out.println(o);		
	}	
	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}

}
