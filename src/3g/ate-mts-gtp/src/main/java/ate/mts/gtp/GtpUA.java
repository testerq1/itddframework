package ate.mts.gtp;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.devoteam.srit.xmlloader.core.protocol.Msg;
import com.devoteam.srit.xmlloader.core.utils.XMLLoaderEntityResolver;
import com.devoteam.srit.xmlloader.gtp.MsgGtp;
import com.devoteam.srit.xmlloader.gtp.data.MessageGTP;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import ate.AbstractURIHandler;
import ate.rm.dev.Host;
import ate.ua.IAutoResponseMessage;
import ate.ua.UAOption;
import ate.ua.mina.AMinaUAImp;
import ate.ua.mina.NoExceptionProtocolCodecFilter;

public class GtpUA extends AMinaUAImp<Msg> implements IAutoResponseMessage<IoSession, Msg> {	
	
	class GtpDecoder extends CumulativeProtocolDecoder {
		@Override
		protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out)
				throws Exception {
			if (in.remaining() >= 0) {
				MsgGtp pkt = readFromStream(in.asInputStream());
				out.write(pkt);
				return true;
			}
			return false;
		}
	}

	class GtpEncoder extends ProtocolEncoderAdapter {
		@Override
		public void encode(IoSession session, Object message, ProtocolEncoderOutput out)
				throws Exception {
			if (message instanceof MsgGtp)
				out.write(IoBuffer.wrap(((MsgGtp)message).getBytesData()));
			else if (message instanceof IoBuffer)
				out.write(message);
		}
	}
	@Override
	protected void add_default_filter() {
		add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(
				new GtpEncoder(), new GtpDecoder()));
	}
	
	public GtpUA(){}
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option != UA_TYPE)
			return this;

		List<String> list = get_query_paras("type");
		
		
		return this;
	}
	public MsgGtp read_request_from_template(String fileName) {
		String filename = Host.HOME+File.separator+"conf"+File.separator+
				"gtp"+File.separator+fileName+"_client.xml";
		
		SAXReader reader = new SAXReader(false);
		reader.setEntityResolver(new XMLLoaderEntityResolver());

		File file = new File(filename);
		if(!file.exists())
			return null;
		
		try {
			org.dom4j.Document doc = reader.read(file);		
			//return (Element) doc.getRootElement().elements().get(0);
			MsgGtp msg = parseMsgFromXml(doc.getRootElement());		
			
			return msg;
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public MsgGtp create_response_from_emplate(MsgGtp request,String fileName) {
		String filename = Host.HOME+File.separator+"conf"+File.separator+
				"gtp"+File.separator+fileName+"_server.xml";
		
		SAXReader reader = new SAXReader(false);
		reader.setEntityResolver(new XMLLoaderEntityResolver());

		File file = new File(filename);
		
		if(!file.exists())
			return null;
		try {
			org.dom4j.Document doc = reader.read(file);		
			//return (Element) doc.getRootElement().elements().get(0);
			MsgGtp msg= parseMsgFromXml(doc.getRootElement());
			
			//msg.setTransaction(request.getTransaction());
			
			return msg;
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public String get_default_schema() {
		return "gtp";
	}

	private MsgGtp parseMsgFromXml(Element root) {
		try {
			MessageGTP message = new MessageGTP(root);
			return new MsgGtp(message);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public MsgGtp readFromStream(InputStream inputStream) {
		MessageGTP message = new MessageGTP();
		try {
			message.decodeFromStream(inputStream);
			return new MsgGtp(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Creates a Msg specific to each Stack Use for UDP like protocol : to build
	 * incoming message should become ABSTRACT later
	 */
	public Msg readFromDatas(byte[] datas, int length) throws Exception {
		byte[] newDatas = datas.clone();
		MessageGTP message = new MessageGTP();
		message.decodeFromBytes(newDatas);
		return new MsgGtp(message);
	}

	@Override
	public boolean message_matched(Msg msg, HashMap<String, Object> hm) {
		return true;
	}

	@Override
	public Msg get_auto_resp_msg(IoSession session, Msg msg) {
		return null;
	}
}
