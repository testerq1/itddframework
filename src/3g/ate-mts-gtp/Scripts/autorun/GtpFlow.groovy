import java.lang.reflect.Method
import java.util.HashMap
import org.testng.annotations.*
import org.testng.annotations.Test
import ate.testcase.*
import ate.ua.raw.*
import common.*

public class GtpFlow extends CsUATestcase {
    def svrc
    def cltc
    def svru
    def cltu
    def MSISDN=100000

    @DataProvider(name="Data_TestStep")
    public Object[][] Data_TestStep(){
        return [
            [
                "214-http.pcap",
                "172.16.5.214"
            ],
            ["tcp"]
        ]
    }

    @Test(invocationCount=1)
    public void raws(){
        gtpv2("10.121.206.83")

        replay("214-http.pcap","172.16.5.214","10.121")
        replay("sip.pcap",     "172.16.5.217","10.121")
        replay("all-217.pcap", "172.16.5.217","10.121")
    }
    
    def gtpv1(){
        STEP("gtp-c create_pdpcontext")
        def msg=cltc.read_request_from_template("create_pdpcontext")
        msg.getHeader().setTunnelEndpointId(17767)
        def TEID=msg.getElement("Tunnel Endpoint Identifier Control Plane:17")
        TEID.setFileld("Tunnel Endpoint Identifier Control Plane","4581")
        def MSI=msg.getElement("MSISDN:134")
        MSI.setFileld("MSISDN","8618617"+(MSISDN++))
        cltc.send_message(msg)

        STEP("gtp-c Server Recv Message1")
        def o = svrc.recv_message()
        def resp=svrc.create_response_from_emplate(msg, "create_pdpcontext")
        def hh=resp.getHeader()
        hh.setSequenceNumber(43981)
        hh.setTunnelEndpointId(17767)
        def user=resp.getElement("End User Address:128")
        user.setFileld("PDP address","101058307")
        log.info(user.toString())

        STEP("gtp-c Svr Send Message1")
        svrc.send_message(resp)

        STEP("gtp-c Client Recv Message2")
        o=cltc.recv_message()
        assertTrue(o!=null)

        STEP("gtp-c Client Send Message3")
        log.info("update_pdpcontext")
        def msg1=cltc.read_request_from_template("update_pdpcontext")
        def oo=msg1.getElement("ULI:152")
        oo.setFileld("LAC", "42227")
        oo.setFileld("CI", "42557")
        log.info(oo.toString())
        cltc.send_message(msg1)

        STEP("gtp-c Svr Recv Message3")
        def o1 = svrc.recv_message()
        def resp1=svrc.create_response_from_emplate(msg1, "update_pdpcontext")
        def TEID2=resp1.getHeader()
        TEID2.setTunnelEndpointId(4581)

        svrc.send_message(resp1)
        STEP("Client vRecv Message4")
        o1=cltc.recv_message()
        assertTrue(o1!=null)
    }
    def create_session_req
    def create_session_resp
    
    def gtpv2(ip){
        if(create_session_req==null)
            create_session_req=cltc.read_request_from_template("create_session")
        cltc.send_message(create_session_req);
        
        def  req = svrc.recv_message();
        def resp=svrc.create_response_from_emplate(req, "create_session");
        def o=resp.getElement("PAA:79")
        o.setFileld("IPv4 address", ip)
        
        svrc.send_message(resp);
        
        o=cltc.recv_message();
    }

    def replay(filename,sip,ip_prefix){
        STEP("gtpu start read")
        def pr=new PcapReader(filename)
        pr.set_matcher(HA.SIP,sip)
        pr.start_read()

        STEP("gtpu get template gtpuuu:")
        def msg=cltu.read_request_from_template("gtpuuu")

        def pkt=pr.get_packet()
        STEP("gtpu start replay")
        def notfirst=false
        
        while(pkt!=null){
            def data=msg.getDataPdu()
            //  pkt.set_mtu(1460)
            Object resp=null
            for(int i=2;i>1;i--){
                //  data.setData("h01020300");
                //def mod = (i%253)+1
                def mod = 206
                //int div= 7+(i/254)
                int div= 83

                if(pkt.is_uac()){
                    pkt.build(HA.SIP,"$ip_prefix.$div.$mod")
                    data.setData(pkt.get_bytes())                    
                    //data.setData(pkt.get_payload(HA.SIP,"$ip_prefix.$div.$mod"))
                    cltu.send_message(msg)
                    resp=svru.recv_message()
                    notfirst=true
                }else if(notfirst){
                    pkt.build(HA.DIP,"$ip_prefix.$div.$mod")
                    data.setData(pkt.get_bytes())
                    //data.setData(pkt.get_payload(HA.DIP,"$ip_prefix.$div.$mod"))
                    svru.send_message(msg)
                    resp=cltu.recv_message()
                }else{
                		continue;
                }
                assertTrue(resp!=null)
            }
            pkt=pr.get_packet()
        }
        pr.close()
    }

    @BeforeMethod
    public void beforeMethod(Method m) {
        super.BeforeMethod(m)
    }

    @AfterMethod
    public void afterMethod(Method m) {
        super.AfterMethod(m)
    }


    @BeforeClass
    public void beforeClass() {
        svrc = create_server("gtp://$siip:2123/udp")
        cltc = create_client("gtp://$sip:2123/udp")
        svru= create_server("gtp://$siip:2152/udp")
        cltu= create_client("gtp://$sip:2152/udp")
        start_all_ua()
        super.BeforeClass()
    }

    @AfterClass
    public void afterClass() {
        shutdown_all_ua()
        super.AfterClass()
    }
}
