import java.lang.reflect.Method;
import java.util.HashMap;
import org.testng.annotations.Test;
import org.testng.annotations.*
import ate.testcase.*
import ate.ua.raw.*;
import common.*

public class GtpTest extends CsUATestcase {
	@Test
	public void gtpu_withoutpdu(){
		def svr= create_server("gtp://"+siip+":2152/udp");
		def clt= create_client("gtp://"+sip+":2152/udp");
		start_all_ua();
		def filename="smtp_172.16.5.211_p.pcap";
		def pr=new PcapReader(filename);
		
		pr.set_matcher(HA.SIP,filename.split("_")[1]);
		pr.start_read();
		
		def msg=clt.read_request_from_template("gtpu_withoutpdu");
		
		def pkt=pr.get_packet();
		int cnt=0;
		while(pkt!=null){
			cnt++;
			System.out.println(cnt);
			def data=msg.getDataPdu();
			def resp=null;
			pkt.set_mtu(1460);
			
			for(int i=2;i>1;i--){
				int mod = (i%253)+1;
				int div= 7+(i/254);
				
				if(pkt.is_uac()){
					data.setData(pkt.build(HA.SIP,"6.6."+div+"."+mod));
					clt.send_message(msg);
					resp=svr.recv_message();
				}else{
					data.setData(pkt.build(HA.DIP,"6.6."+div+"."+mod));
					svr.send_message(msg);
					resp=clt.recv_message();
				}
				assertTrue(resp!=null);
			}
			
			
			pkt=pr.get_packet();
		}
		pr.close();
	}
	@DataProvider(name="Data_TestStep")
	public Object[][] Data_TestStep(){
		return [["h01020300"],["h0102030101020300"]];
	}
	@Test(dataProvider="Data_TestStep")
	public void gtpu_withpdu(ext_header){
		def svr= create_server("gtp://"+siip+":2152/udp");
		def clt= create_client("gtp://"+sip+":2152/udp");
		start_all_ua();
		def filename="smtp_172.16.5.211_p.pcap";
		def pr=new PcapReader(filename);

		pr.set_matcher(HA.SIP,filename.split("_")[1]);
		pr.start_read();

		def msg=clt.read_request_from_template("gtpu");

		def pkt=pr.get_packet();
		int cnt=0;
		while(pkt!=null){
			cnt++;

			def data=msg.getDataPdu();
			def resp=null;
			pkt.set_mtu(1460);

			for(int i=2;i>1;i--){
				data.setData(ext_header);
				int mod = (i%253)+1;
				int div= 7+(i/254);
				if(pkt.is_uac()){
					data.appendData(pkt.build(HA.SIP,"6.6."+div+"."+mod));
					clt.send_message(msg);
					resp=svr.recv_message();
				}else{
					data.appendData(pkt.build(HA.DIP,"6.6."+div+"."+mod));
					svr.send_message(msg);
					resp=clt.recv_message();
				}
				assertTrue(resp!=null);
			}


			pkt=pr.get_packet();
		}
		pr.close();
	}

	@Test
	public void gtp_u() throws Exception{
		def svr= create_server("gtp://"+siip+":2152/udp");
		def clt= create_client("gtp://"+sip+":2152/udp");
		start_all_ua();
		def msg=clt.read_request_from_template("gtpu");
		clt.send_message(msg);
		Object o = svr.recv_message();
		assertTrue(o!=null);
	}

	@Test
	public void gtp_u_edit() throws Exception{
		def ua= create_client("gtp://$sip:2152/udp")

		def msg=ua.read_request_from_template("gtpu");
		def h=msg.getHeader();
		h.setTunnelEndpointId(123456l);

		def data=msg.getDataPdu();
		data.setData([0x1, 0x2]as byte[]);
		data.appendData([0x3, 0x4]as byte[]);
		assertTrue(data.getData().length==4);
		assertTrue(data.getData()[3]==4);

		data.setData("h0102");
		data.appendData("h0304");
		assertTrue(data.getData().length==4);
		assertTrue(data.getData()[3]==4);
	}

	@Test
	public void gtp_c(){
		log.info("test");
		def svr= create_server("gtp://$siip:2123/udp")
		def clt= create_client("gtp://$sip:2123/udp")
		start_all_ua();

		def msg=clt.read_request_from_template("create_session");
		clt.send_message(msg);

		def o = svr.recv_message();
		def resp=svr.create_response_from_emplate(msg, "create_session");
		svr.send_message(resp);
		o=clt.recv_message();
		assertTrue(o!=null);
	}
    @Test
    public void gtp_create_pdp(){
        STEP("gtp_create_pdp:");
        def svr= create_server("gtp://$siip:2123/udp")
        def clt= create_client("gtp://$sip:2123/udp")
        start_all_ua();
        
        STEP("clt send create_pdpcontext");
        def msg=clt.read_request_from_template("create_pdpcontext");
        msg.getHeader().setTunnelEndpointId(17767);
        def TEID=msg.getElement("Tunnel Endpoint Identifier Control Plane:17")
        TEID.setFileld("Tunnel Endpoint Identifier Control Plane","4581")
        def MSI=msg.getElement("MSISDN:134")
        MSI.setFileld("MSISDN","8618617498561")
        clt.send_message(msg);
        
        STEP("svr recv create_pdpcontext")
        def o = svr.recv_message();
        assertTrue(o!=null);
        
        STEP("svr send create_pdpcontext_response")
        def resp=svr.create_response_from_emplate(msg, "create_pdpcontext");
        def hh=resp.getHeader();
        hh.setSequenceNumber(43981);
        hh.setTunnelEndpointId(17767)
        def user=resp.getElement("End User Address:128")
        user.setFileld("PDP address","101058307")        
        svr.send_message(resp);

        STEP("clt recv create_pdpcontext_response")
        o=clt.recv_message();
        assertTrue(o!=null);

        STEP("clt send update_pdpcontext")
        log.info("update_pdpcontext");
        def msg1=clt.read_request_from_template("update_pdpcontext");
        def oo=msg1.getElement("ULI:152");
        oo.setFileld("LAC", "42227");
        oo.setFileld("CI", "42557");
        log.info(oo.toString())
        clt.send_message(msg1);

        STEP("svr recv update_pdpcontext")
        o = svr.recv_message();
        assertTrue(o!=null);
        
        STEP("svr send update_pdpcontext_response")
        def resp1=svr.create_response_from_emplate(msg1, "update_pdpcontext");
        def TEID2=resp1.getHeader();
        TEID2.setTunnelEndpointId(4581);        
        svr.send_message(resp1);
        
        STEP("clt recv update_pdpcontext_response")
        o=clt.recv_message();
        assertTrue(o!=null);

/*      log.info("delete_pdpcontext");
        def msg2=clt.read_request_from_template("delete_pdpcontext");
        clt.send_message(msg2);

        def o2 = svr.recv_message();
        def resp2=svr.create_response_from_emplate(msg2, "delete_pdpcontext");
        svr.send_message(resp2);
        o2=clt.recv_message();
        assertTrue(o2!=null);   */



/*      def msg=clt.read_request_from_template("create_session");

        def h=msg.getHeader();

        h.setTunnelEndpointId(123456l);

        def o=msg.getElement("Bearer Context:93.EBI:73");
        assertTrue(o.toString().contains("name=\"EBI\" value=\"0\""));
        o.setFileld("EBI", "1");

        assertTrue(msg.getHeader().getTunnelEndpointId()==123456l);
        assertTrue(o.toString().contains("name=\"EBI\" value=\"1\""));
        log.info(o.toString());*/
}
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		// ;
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
