/* 
 * Copyright 2012 Devoteam http://www.devoteam.com
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * 
 * This file is part of Multi-Protocol Test Suite (MTS).
 * 
 * Multi-Protocol Test Suite (MTS) is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License.
 * 
 * Multi-Protocol Test Suite (MTS) is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Multi-Protocol Test Suite (MTS).
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package com.devoteam.srit.xmlloader.core.protocol;

import java.util.Iterator;

import com.devoteam.srit.xmlloader.core.Parameter;
import com.devoteam.srit.xmlloader.core.utils.Utils;

/**
 * Generic transaction manipulated by XML Loader's core.
 * @author fhenry
 */
public class Trans extends GroupMsg<Msg>
{
	
	public Trans(Msg beginMsg) throws Exception
    {
    	super(beginMsg);    	
    }

    /** Add a end message to the list of the transaction */
    public boolean addEndMessage(Msg msg) throws Exception
    {
    	boolean ret = super.addEndMessage(msg);
    	if (msg.endTransaction())
    	{
    		this.active = false;
    	}
    	return ret;
    }

    /** Get a parameter from the message */
    public Parameter getParameter(String path) throws Exception
    {
        String[] params = Utils.splitPath(path);
    	Parameter parameter = new Parameter();

        if (params.length == 1)
        {
        	parameter.add(this);
        }
        else if (params[1].equalsIgnoreCase("id"))
        {
            parameter.add(getBeginMsg().getTransactionId().toString());
        }
        else if (params[1].equalsIgnoreCase("name"))
        {
            parameter.add(getSummary(beginMsg.isSend()));
        }        
        else if (params[1].equalsIgnoreCase("request"))
        {
            parameter.add(getBeginMsg());
        }
        else if (params[1].equalsIgnoreCase("responses"))
        {
        	Iterator<Msg> iter = endListMsg.values().iterator();
        	while (iter.hasNext())
        	{
        		Msg msg = iter.next();
        		parameter.add(msg);	
        	}        
        }
        else
        {
        	Parameter.throwBadPathKeywordException(path);
        }
        return parameter;
    }
    
    /** Shall retransmit the transaction */
    public boolean shallRetransmit() throws Exception
    {
        Iterator<Msg> iter = endListMsg.values().iterator();
        while (iter.hasNext())
        {
            Msg msg = (Msg) iter.next();
            if (msg.shallStopRetransmit())
            {
                return false;
            }
        }
        return true;
    }

    public String getSummary(boolean send) throws Exception
    {
    	String ret = beginMsg.getSummary(send, true);
    	ret += " / ";
        Iterator<Msg> iter = endListMsg.values().iterator();
        while (iter.hasNext())
        {
            Msg msg = (Msg) iter.next();
            if (iter.hasNext())
            {
           		ret += msg.getSummary(!send, false) + " ";
            }
            else
            {
           		ret += msg.getSummary(!send, true);
            }
        }    
        return ret;
    }

    
    public static float getTimeDuration(MsgLight msg, long beginMsg)
    {
        float transTime = ((float) (msg.getTimestamp() - beginMsg)) / 1000;
        return transTime;
    }
	

    /** Returns the string description of the message. Used for logging as DEBUG level */
    public String toString()
    {
   		return super.toString("TRANSACTION:Request=", "TRANSACTION:Responses=");
	}

}
