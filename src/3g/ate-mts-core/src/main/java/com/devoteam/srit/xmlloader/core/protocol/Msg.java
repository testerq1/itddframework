/* 
 * Copyright 2012 Devoteam http://www.devoteam.com
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * 
 * This file is part of Multi-Protocol Test Suite (MTS).
 * 
 * Multi-Protocol Test Suite (MTS) is free software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License.
 * 
 * Multi-Protocol Test Suite (MTS) is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Multi-Protocol Test Suite (MTS).
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package com.devoteam.srit.xmlloader.core.protocol;

import com.devoteam.srit.xmlloader.core.Parameter;
import com.devoteam.srit.xmlloader.core.exception.ExecutionException;
import com.devoteam.srit.xmlloader.core.utils.Config;
import com.devoteam.srit.xmlloader.core.utils.Utils;
import com.devoteam.srit.xmlloader.core.utils.expireshashmap.Removable;

import gp.utils.arrays.Array;
import gp.utils.arrays.DefaultArray;

import java.net.URI;
import java.util.LinkedList;

/**
 * Generic message manipulated by XML Loader's core.<br/>
 * Should be inherited by protocol-specific messages.
 * @author gpasquiers
 */
public abstract class Msg extends MsgLight implements Removable
{
	/** Maximum number of characters to write into the log */
    protected static int MAX_STRING_LENGTH = Config.getConfigByName("tester.properties").getInteger("logs.MAX_STRING_LENGTH", 1000);

    protected TransactionId transactionId;
    protected boolean isTransactionIdSet;
    private MessageId messageId;
    private boolean isMessageIdSet;
    private RetransmissionId retransmissionId;
    private boolean isRetransmissionIdSet;
    private Trans transaction;
    private boolean isSessionIdSet;
    private SessionId sessionId;
    private LinkedList<String> scenarioName = null;       
    
    private String remoteHost = null;
    private int remotePort =  -1;
    private String transport = null;
    
    private Long timestampCaptureFile = null;
    
    public Msg()
    {
        this.isMessageIdSet = false;
        this.isTransactionIdSet = false;
        this.isRetransmissionIdSet = false;
        
    }

    /** Get a parameter from the message */
    public Parameter getParameter(String path) throws Exception
    {
        String[] params = Utils.splitPath(path);
        if (params.length < 1)
        {
            return null;
        }
        
		Parameter var = new Parameter();
        // DEPRECATED value
        //---------------------------------------------------------------------- message:connection -        
        if (params[0].equalsIgnoreCase("message"))
        { 
            if (params.length == 1)
            {
            	var.add(this);
            }
            else if (params[1].equalsIgnoreCase("type"))
            {
                var.add(getType());
            }
            else if (params[1].equalsIgnoreCase("typeComparison"))
            {
                var.add(getTypeComparison());
            }
            else if (params[1].equalsIgnoreCase("request"))
            {
            	var.add(String.valueOf(isRequest()));
            }
            else if (params[1].equalsIgnoreCase("result"))
            {
            	var.add(getResult());
            }
            else if (params[1].equalsIgnoreCase("resultComparison"))
            {
            	var.add(getResultComparison());
            }            
            else if (params[1].equalsIgnoreCase("protocol"))
            {
            	var.add(getProtocol());
            }
            else if (params[1].equalsIgnoreCase("transactionId"))
            {
            	TransactionId trans = getTransactionId();
            	if (trans != null)
            	{
            		var.add(trans.toString());
            	}
            }
            else if (params[1].equalsIgnoreCase("messageId"))
            {
            	MessageId mess = getMessageId();
            	if (mess != null)
            	{
            		var.add(mess.toString());
            	}
            }
            else if (params[1].equalsIgnoreCase("scenarioName"))
            {
            	var.add(getScenarioName());
            }
            else if (params[1].equalsIgnoreCase("retransmissionId"))
            {
            	RetransmissionId retrans = getRetransmissionId();
            	if (retrans != null)
            	{
            		var.add(retrans.toString());
            	}
            }
            else if (params[1].equalsIgnoreCase("sessionId"))
            {
            	SessionId sess = getSessionId();
            	if (sess != null)
            	{
            		var.add(sess.toString());
            	}
            }
            else if (params[1].equalsIgnoreCase("timestamp"))
            {
            	var.add(new Long(getTimestamp()).toString());
            }
            else if (params[1].equalsIgnoreCase("timestampCaptureFile"))
            {
                if(getTimestampCaptureFile() != null)
                    var.add(new Long(getTimestampCaptureFile()).toString());
            }
            else if (params[1].equalsIgnoreCase("length"))
            {
            	var.add(new Long(getLength()).toString());
            }
            else if (params[1].equalsIgnoreCase("binary"))
            {
            	var.add(Array.toHexString(new DefaultArray(getBytesData())));
            }
            else if (params[1].equalsIgnoreCase("text"))
            {
            	var.add(new String(getBytesData()));
            }
            else
            {
            	Parameter.throwBadPathKeywordException(path);
            }
            return var;
        }
        else if (params[0].equalsIgnoreCase("transaction"))
        {
        	if (this.transaction != null)
        	{
        		var = this.transaction.getParameter(path);
        	}
       		return var;
       	}
        
        return null;
    }

    /** Returns a value from the diameter message */
    private String getFromMessage(String paths) throws Exception
    {
        String[] tabPaths = Utils.splitNoRegex(paths, ",");
        String result = "";
        for (int i = 0; i < tabPaths.length;)
        {
            Parameter var = new Parameter();
            var = getParameter(tabPaths[i]);
            if (var.length() > 0)
            {
            	result += var.get(0);
            }
            i++;
            if (i != tabPaths.length)
            {
                result += "|";
            }

        }
        return result;
    }

    /** Get the message Identifier of this message */
    public MessageId getMessageId() throws Exception
    {
        //TODO hxy
        return this.messageId;
    }

    /** Get the scenario name owning this message (used for dispatching) */
    public LinkedList<String> getScenarioName() throws Exception
    {
    	//TODO hxy
        return scenarioName;
    }

    /** Get the transaction Identifier of this message */
    public TransactionId getTransactionId() throws Exception
    {
    	//TODO hxy
        return this.transactionId;
    }

    /** Get the transaction Identifier of this message */
    public void setTransactionId(TransactionId transactionId)
    {
        this.transactionId = transactionId;
        this.isTransactionIdSet = true;
    }

    public Trans getTransaction()
    {
        return this.transaction;
    }

    public void setTransaction(Trans transaction)
    {
        this.transaction = transaction;
    }

    public RetransmissionId getRetransmissionId() throws Exception
    {
    	//TODO hxy
        return this.retransmissionId;
    }

    public SessionId getSessionId() throws Exception
    {
    	//TODO hxy
        return this.sessionId;
    }

    /**
     *  Tell whether the message shall be retransmitted or not 
     * (= true by default) 
     */
    public boolean shallBeRetransmitted() throws Exception
    {
        return true;
    }

    /**
     *  Tell whether the message shall be stop the automatic 
     *  retransmmission mechanism or not 
     * (= true by default) 
     */
    public boolean shallStopRetransmit() throws Exception
    {
        return true;
    }

    /**
     *  Tell whether the response begin the transaction or not 
     * (= true by default) 
     */
    public boolean beginTransaction() throws Exception
    {
        return true;
    }

    /**
     *  Tell whether the response end the transaction or not 
     * (= true by default) 
     */
    public boolean endTransaction() throws Exception
    {
        return true;
    }

    /**
     *  Tell whether the message shall begin a new session 
     * (= false by default) 
     */
    public boolean beginSession() throws Exception
    {
        return false;
    }

    /**
     *  Tell whether the message shall end a session 
     * (= false by default) 
     */
    public boolean endSession() throws Exception
    {
        return false;
    }

    public void onRemove() throws Exception
    {
    	// nothing to do
    }
    
    /** Get the protocol of this message */
    public abstract String getProtocol();

    /** Get the type (eg the command code for aaa, the method for sip) of this message */
    public abstract String getType() throws Exception;

    /** Get the type for comparison (eg the command code for aaa, the method for sip) of this message */
    public String getTypeComparison() throws Exception
    {
    	return ":" + getTypeComplete() + ":";
    }

    /** Get the complete type (with dictionary conversion) of this message */
    public String getTypeComplete() throws Exception
    {
    	return getType();
    }

    /** Get the result of this answer (null if request) */
    public abstract String getResult() throws Exception;

    /** Get the result for comparison (eg the command code for aaa, the method for sip) of this message */
    public String getResultComparison() throws Exception
    {
    	return ":" + getResultComplete() + ":";
    }

    /** Get the complete result of this answer (null if request) */
    public String getResultComplete() throws Exception
    {
    	return getResult();
    }
    
    /** Return true if the message is a request else return false*/
    public abstract boolean isRequest() throws Exception;
    
    /** Get the data of this message */
    public abstract byte[] getBytesData();
       
    /** Return the length of the message*/
    public int getLength()
    {
    	return getBytesData().length;
    }

	public String getRemoteHost() {
		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	public int getRemotePort() {
		return remotePort;
	}

	public void setRemotePort(int remotePort) {
		this.remotePort = remotePort;
	}

	public String getRemoteUrl() {
		return protocol + ":" + remotePort + ":" + remotePort;
	}
	
	public void setRemoteUrl(String remoteUrl) throws Exception {
		//TODO hxy
	}
	
    /** Return the transport of the message*/
	public String getTransport() {
		return transport;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

    public void setTimestampCaptureFile(long time){
        timestampCaptureFile = time;
    }
    
    public Long getTimestampCaptureFile(){
        return timestampCaptureFile;
    }
	
    public String getSummary(boolean send, boolean prefix) throws Exception
    {
    	String ret = null;
    	//TODO hxy
    	return ret;
    }

    /** Returns a short description of the message. Used for logging as INFO level */
    /** This methods HAS TO be quick to execute for performance reason */
    public String toShortString() throws Exception {
        String ret = getProtocol() + " > " + getTypeComplete();
        if (!isRequest())
        {
        	ret += ">" + getResultComplete();
        }
        return ret + "\n";
    }
    
    /** Returns the string description of the message. Used for logging as DEBUG level */
    public String toString()
    {
    	String ret = " ";
		// display the xml representation
		try
    	{
			ret += toXml().trim();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

		// cut if message is too long
        if (ret.length() > MAX_STRING_LENGTH)
        {
        	ret = " {" + MAX_STRING_LENGTH + " of " + ret.length() + "}" + ret.substring(0, MAX_STRING_LENGTH);
        }

        ret += "\n\n";

        return " " + ret.trim();
    }

    /** Get the XML representation of the message for the genscript module. */
    public abstract String toXml() throws Exception;


    /** check wether message is a command to initiate TLS handshake over TCP socket or not **/
    public boolean isSTARTTLS_request()
    {
    	return false;
    }
    
    public boolean isSTARTTLS_answer()
    {
    	return false;
    }
    
    public boolean isSTOPTLS_answer()
    {
    	return false;
    }
    
    public boolean isSTOPTLS_request()
    {
    	return false;
    }
}
