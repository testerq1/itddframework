# GPRS core.

 - Bridge.
 - OpenGGSN SGSN. (sgsnemu)
 - Cisco GGSN. (dynamips)


# Bridge.

brctl addbr brGn
tunctl -t tap401 -u taro
tunctl -t tap409 -u taro
brctl addif brGn tap401
brctl addif brGn tap409
ifconfig tap401 0
ifconfig brGn 192.168.40.1 netmask 255.255.255.0
ifconfig tap409 192.168.40.9 netmask 255.255.255.0
ping 192.168.40.9
ifconfig tap409 0
ping 192.168.40.9


# OpenGGSN SGSN.


sgsnemu -l 192.168.40.1 -r 192.168.40.9 -a open --gtpversion=1 --createif


# Cisco GGSN.

dynamips -p 1:PA-FE-TX -s 1:0:tap:tap409 -p 2:PA-FE-TX \
/mnt/ext3/home/taro/IOS/c7200-advipservicesk9.124-2.T.bin


Cisco IOS commands for donfigure GGSN.


! config hostname
ena
conf ter
hostname GGSN
! enable ggsn on router
service gprs ggsn
! create VTI
interface loopback 0
ip address 192.168.1.1 255.255.255.0
description Link to VTI
exit
interface virtual-template 1
ip unnumbered loopback 0
encapsulation gtp
exit
! Configure IP address for Gn
interface fastEthernet 1/0
ip address 192.168.40.9 255.255.255.0
description Gn_interface to SGSN
no shutdown
exit
! Configure IP address for Gi
interface fastEthernet 2/0
ip address 192.168.30.6 255.255.255.0
description Gi_interface to INTERNET
no shutdown
exit
! Enable cisco express forwarding CEF
ip cef
interface fastEthernet 1/0
ip route-cache
exit
interface fastEthernet 2/0
ip route-cache
exit
gprs gtp ip udp ignore checksum
! define GPRS APs list
gprs access-point-list gprs
access-point 1
access-point-name cisco
exit
exit

! configure DHCP pool
ip dhcp pool pool_gprs
network 192.168.100.0 255.255.255.0
exit
interface loopback 1
ip address 192.168.100.1 255.255.255.0
exit
ip dhcp excluded-address 192.168.100.1

! link DHCP pool to APN
gprs access-point-list gprs
access-point 1
dhcp-server 192.168.100.1
dhcp-gateway-address 192.168.100.1
ip-address-pool dhcp-proxy-client
aggregate auto




! useful commands for monitoring
show gprs gtp pdp-context all
show gprs gtp pdp-context tid 2206210200000051
show gprs gtp status
show gprs gtp statistics
show gprs gtp parameters
show ip route
debug gprs gtp messages
debug gprs gtp events
