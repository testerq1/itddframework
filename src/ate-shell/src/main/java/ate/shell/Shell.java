package ate.shell;
import org.crsh.cli.impl.bootstrap.CommandProvider;
import org.crsh.cli.impl.descriptor.CommandDescriptorImpl;
import org.crsh.cli.impl.Delimiter;
import org.crsh.cli.impl.descriptor.HelpDescriptor;
import org.crsh.cli.impl.lang.CommandFactory;
import org.crsh.cli.impl.invocation.InvocationMatch;
import org.crsh.cli.impl.invocation.InvocationMatcher;

import ate.rm.dev.Host;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Shell {
	static String home=Host.HOME;
	
	public static void main(String[] args) throws Exception {
		java.util.logging.LogManager logManager = java.util.logging.LogManager
				.getLogManager();
		InputStream inputStream = null;
		try {
			// 重新初始化日志属性并重新读取日志配置。
			File logconf = new File(Host.CONF_HOME + File.separator
					+ "log.properties");
			inputStream = new FileInputStream(logconf);
			
			logManager.readConfiguration(inputStream);
		} catch (Exception e) {
			System.err.println(e);
		} finally {
			if (null != inputStream)
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}        
        
		args=new String[]{"--conf",
				home+File.separator+"shell"+File.separator+"conf",
				"--cmd",
				home+File.separator+"shell"+File.separator+"cmd"
				};		
	    ServiceLoader<CommandProvider> loader = ServiceLoader.load(CommandProvider.class);
	    Iterator<CommandProvider> iterator = loader.iterator();
	    if (iterator.hasNext()) {

	      //
	      StringBuilder line = new StringBuilder();
	      for (int i = 0;i < args.length;i++) {
	        if (i  > 0) {
	          line.append(' ');
	        }
	        Delimiter.EMPTY.escape(args[i], line);
	      }

	      //
	      CommandProvider commandProvider = iterator.next();
	      Class<?> commandClass = commandProvider.getCommandClass();
	      handle(commandClass, line.toString());
	    }
	  }

	  private static <T> void handle(Class<T> commandClass, String line) throws Exception {
	    CommandDescriptorImpl<T> descriptor = CommandFactory.DEFAULT.create(commandClass);
	    descriptor = HelpDescriptor.create(descriptor);
	    InvocationMatcher<T> matcher = descriptor.invoker("main");
	    InvocationMatch<T> match = matcher.match(line);
	    T instance = commandClass.newInstance();
	    Object o = match.invoke(instance);
	    if (o != null) {
	      System.out.println(o);
	    }
	  }
}
