welcome = { ->
  def hostName;
  try {
    hostName = java.net.InetAddress.getLocalHost().getHostName();
  } catch (java.net.UnknownHostException ignore) {
    hostName = "localhost";
  }
  return """\
        _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
      .'.             |          |
    .''```.           |          |_ _ _ _ _
  .'       `.         |          |         
.'           `.       |          |_ _ _ _ _         

Follow and support the project on ravi.huang@gmail.com
Welcome to $hostName + !
It is ${new Date()} now
""";
}

prompt = { ->
  return "% ";
}
