package ate.sbox.crawler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ate.AbstractURIHandler;
import ate.rm.file.FileUA;
import ate.ua.UAOption;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

public class CrawlerUA extends AbstractURIHandler{
	int numberOfCrawlers = 7;
	String crawlStorageFolder = "tmp"+File.separator+"crawl";
	CrawlController controller=null;
	List<String> seeds=new ArrayList<String>();
	int maxDepthOfCrawling=1;
	
	public void add_seed(String url){
		seeds.add(url);
	}
	
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			if(host!=null){
				this.seeds.add("http://"+this.get_authority());
				if(this.get_query_para("deep")!=null)
					maxDepthOfCrawling=Integer.parseInt(this.get_query_para("deep"));				
			}else
				set_seeds_file(this.get_schemeSpecificPart());			
		}
		return this;
	}
	
	@Override
	public void teardown() {
		controller.shutdown();
		seeds.clear();
	}

	public boolean is_finished(){
		return controller.isFinished();
	}	
	
	@Override
	public void startup() {
		CrawlConfig config = new CrawlConfig();
		// config.setMaxDepthOfCrawling(2);

		config.setCrawlStorageFolder(crawlStorageFolder);
		config.setMaxDepthOfCrawling(maxDepthOfCrawling);
		/*
		 * Instantiate the controller for this crawl.
		 */
		PageFetcher pageFetcher = new PageFetcher(config);
		RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
		RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
		
		try {
			controller = new CrawlController(config, pageFetcher, robotstxtServer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void go(){
		for (String tmp : seeds) {
			controller.addSeed(tmp);
		}	
		controller.start(BaseCrawler.class, numberOfCrawlers);		
	}

	public void set_seeds_file(String filepath){
//		HierarchicalINIConfiguration ini=
		FileUA fua=new FileUA();
		fua.build(UA_URI,filepath);
		
		if(fua.is_ready())
			seeds= fua.get_multi_line("config.seeds");
			
	}
	
	@Override
	public String get_default_schema() {
		return "crawl";
	}

	@Override
	public boolean is_ready() {
		return controller!=null&&seeds.size()>0;
	}

}
