package ate.test;

import java.io.IOException;
import java.lang.reflect.Method;

import org.testng.annotations.*;

import ate.sbox.crawler.CrawlerUA;
import ate.testcase.UATestcase;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
public class CrawlerTest extends UATestcase{
    @Test
    /**
     * jsoup: Java HTML Parser
     * @throws IOException
     */
	public void jsoup() throws IOException{
		String url = "http://en.wikipedia.org/";
        Document document = Jsoup.connect(url).get();


        Elements answerers = document.select("#mp-itn b a");
        for (Element answerer : answerers) {
            System.out.println("mp-itn: " + answerer.text());
        }
        
        document = Jsoup.connect("http://news.sohu.com").get();
        Elements links = document.select("body a");
       // Elements chi=document.children();
        for (Element link : links) {
            log.info("{} {}",link.attributes().get("href"), link.text());
        }
	}	
	@Test
	public void base(){		
		CrawlerUA ua=new CrawlerUA();
		ua.build(ua.UA_URI, "crawl:conf/crawl.ini#deep=2");		
		ua.startup();	
		ua.go();
		
	}
	@Test
	public void sohu(){		
		CrawlerUA ua=new CrawlerUA();
		ua.build(ua.UA_URI, "crawl://www.sohu.com");
		ua.startup();
		ua.go();
		
	}
	@Test
	public void base2(){		
		CrawlerUA ua=new CrawlerUA();		
		ua.add_seed("http://www.sohu.com/");		
		ua.startup();		
		ua.go();		
	}

	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
