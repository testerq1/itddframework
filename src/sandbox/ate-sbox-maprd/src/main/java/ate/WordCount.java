package ate;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

/**
 * Hello world!
 * $ hdfs dfs -mkdir -p /home/wordcount/input
 * $ hdfs dfs -mkdir /home/wordcount/output
 * 
 * $ echo "Hello World Bye World" >file01
 * $ echo "Hello Hadoop Goodbye Hadoop" >file02
 * 
 * $ hdfs dfs -copyFromLocal file0* /home/wordcount/input/
 * $ hdfs dfs -ls /home/wordcount/input/
 * 
 * $ hadoop jar ./hw.jar ate.WordCount /home/wordcount/input/ /home/wordcount/output/
 * $ hdfs dfs -ls /home/wordcount/output/
 * $ hdfs dfs -cat /home/wordcount/output/_SUCCESS 
 * $ hdfs dfs -cat /home/wordcount/output/part-r-00000 
 */
public class WordCount {
	public static void main(String[] args) throws IOException, InterruptedException,
			ClassNotFoundException {
		Path inputPath = new Path(args[0]);
		Path outputDir = new Path(args[1]);
		Configuration conf = new Configuration(true);		
		Job job = new Job(conf, "WordCount");
		job.setJarByClass(WordCountMapper.class);

		job.setMapperClass(WordCountMapper.class);
		job.setReducerClass(WordCountReducer.class);
		job.setNumReduceTasks(2);

		// Specify key / value
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		// Input
		FileInputFormat.addInputPath(job, inputPath);
		job.setInputFormatClass(TextInputFormat.class);

		// Output
		FileOutputFormat.setOutputPath(job, outputDir);
		job.setOutputFormatClass(TextOutputFormat.class);

		// Delete output if exists
		FileSystem hdfs = FileSystem.get(conf);
		if (hdfs.exists(outputDir))
			hdfs.delete(outputDir, true);

		// Execute job
		int code = job.waitForCompletion(true) ? 0 : 1;
		System.exit(code);		
	}
}
