package ate.test;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.sbox.LuceneUA;
import ate.testcase.UATestcase;


public class LuceneTest extends UATestcase {
	
	@Test
	public void base() throws Exception {
		LuceneUA ua=new LuceneUA();
		ua.build(ua.UA_URI, "lucene:F:/gh/itddframework.wiki");
		ua.startup();
		ua.search(new HashMap<String, String>() {
			{
//				put("paging", "1");
				//put("raw", "1");
				put("shell", "1");				
			}
		});
		
	}
	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
