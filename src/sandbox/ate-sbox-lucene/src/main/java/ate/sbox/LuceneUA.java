package ate.sbox;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.StoredDocument;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import ate.AbstractURIHandler;
import ate.ua.UAOption;

public class LuceneUA extends AbstractURIHandler {
	File indexDir;
	File docDir;
	boolean keep = false;

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			path = this.get_schemeSpecificPart();
			docDir = new File(path);
			keep = "keep".equalsIgnoreCase(this.get_fragment());

			if (!docDir.exists() || !docDir.canRead())
				throw new Error("指定的文件路径不存在");
		}
		return this;
	}

	@Override
	public void teardown() {
		if (indexDir.exists() && !keep)
			indexDir.delete();

	}

	private void indexDocs(IndexWriter writer, File file) throws IOException {
		if (file.canRead()) {
			if (file.isDirectory()) {
				String[] files = file.list();
				if (files != null) {
					for (int i = 0; i < files.length; i++) {
						indexDocs(writer, new File(file, files[i]));
					}
				}
			} else {
				FileInputStream fis;
				try {
					fis = new FileInputStream(file);

				} catch (FileNotFoundException fnfe) {
					return;
				}

				try {
					Document doc = new Document();
					Field pathField = new StringField("path", file.getPath(), Field.Store.YES);
					doc.add(pathField);
					doc.add(new LongField("modified", file.lastModified(), Field.Store.NO));
					doc.add(new TextField("contents", new BufferedReader(new InputStreamReader(fis,
							charset))));

					if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
						System.out.println("adding " + file);
						writer.addDocument(doc);
					} else {
						System.out.println("updating " + file);
						writer.updateDocument(new Term("path", file.getPath()), doc);
					}

				} finally {
					fis.close();
				}
			}
		}
	}

	@Override
	public void startup() {
		indexDir = new File("tmp");
		if (!indexDir.exists())
			indexDir.mkdir();

		Date start = new Date();
		try {
			Directory dir = FSDirectory.open(indexDir);
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_50);
			IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_50, analyzer);

			if (indexDir.listFiles().length > 0) {
				iwc.setOpenMode(OpenMode.CREATE);

			} else {
				iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);

			}

			IndexWriter writer = new IndexWriter(dir, iwc);
			indexDocs(writer, docDir);

			writer.close();
			dir.close();
			
			log.info(new Date().getTime() - start.getTime() + " total milliseconds");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void doPagingSearch(BufferedReader in, IndexSearcher searcher, Query query,
			int hitsPerPage, boolean raw, boolean interactive) throws IOException {
		TopDocs results = searcher.search(query, 5 * hitsPerPage);
		ScoreDoc[] hits = results.scoreDocs;

		int numTotalHits = results.totalHits;
		System.out.println(numTotalHits + " total matching documents");

		int start = 0;
		int end = Math.min(numTotalHits, hitsPerPage);

		while (true) {
			if (end > hits.length) {
				System.out.println("Only results 1 - " + hits.length + " of " + numTotalHits
						+ " total matching documents collected.");
				System.out.println("Collect more (y/n) ?");
				String line = in.readLine();
				if (line.length() == 0 || line.charAt(0) == 'n') {
					break;
				}

				hits = searcher.search(query, numTotalHits).scoreDocs;
			}

			end = Math.min(hits.length, start + hitsPerPage);

			for (int i = start; i < end; i++) {
				if (raw) {
					System.out.println("doc=" + hits[i].doc + " score=" + hits[i].score);
					continue;
				}

				StoredDocument doc = searcher.doc(hits[i].doc);
				String path = doc.get("path");
				if (path != null) {
					System.out.println((i + 1) + ". " + path);
					String title = doc.get("title");
					if (title != null) {
						System.out.println("   Title: " + doc.get("title"));
					}
				} else {
					System.out.println((i + 1) + ". " + "No path for this document");
				}

			}

			if (!interactive || end == 0) {
				break;
			}

			if (numTotalHits >= end) {
				boolean quit = false;
				while (true) {
					System.out.print("Press ");
					if (start - hitsPerPage >= 0) {
						System.out.print("(p)revious page, ");
					}
					if (start + hitsPerPage < numTotalHits) {
						System.out.print("(n)ext page, ");
					}
					System.out.println("(q)uit or enter number to jump to a page.");

					String line = in.readLine();
					if (line.length() == 0 || line.charAt(0) == 'q') {
						quit = true;
						break;
					}
					if (line.charAt(0) == 'p') {
						start = Math.max(0, start - hitsPerPage);
						break;
					} else if (line.charAt(0) == 'n') {
						if (start + hitsPerPage < numTotalHits) {
							start += hitsPerPage;
						}
						break;
					} else {
						int page = Integer.parseInt(line);
						if ((page - 1) * hitsPerPage < numTotalHits) {
							start = (page - 1) * hitsPerPage;
							break;
						} else {
							System.out.println("No such page");
						}
					}
				}
				if (quit)
					break;
				end = Math.min(numTotalHits, start + hitsPerPage);
			}
		}
	}

	/**
	 * field,
	 * queryFile,查询哪个文件
	 * query, 查询字符串
	 * repeat,重复查询次数，每次查询Top100
	 * raw,
	 * paging:每页显示的条数
	 * 
	 * @param args
	 * @throws Exception
	 */
	public void search(HashMap<String, String> args) throws Exception {
		String field = "contents";
		String queryFile = null;
		int repeat = 0;
		boolean raw = false;
		String queryString = null;
		int hitsPerPage = 10;
		boolean shell = false;

		for (String key : args.keySet()) {
			if ("field".equalsIgnoreCase(key)) {
				field = args.get(key);
			} else if ("queries".equalsIgnoreCase(key)) {
				queryFile = args.get(key);
			} else if ("query".equalsIgnoreCase(key)) {
				queryString = args.get(key);
			} else if ("repeat".equalsIgnoreCase(key)) {
				repeat = Integer.parseInt(args.get(key));
			} else if ("raw".equalsIgnoreCase(key)) {
				raw = true;
			} else if ("shell".equalsIgnoreCase(key)) {
				shell = true;
			} else if ("paging".equalsIgnoreCase(key)) {
				hitsPerPage = Integer.parseInt(args.get(key));
				if (hitsPerPage <= 0) {
					log.error("There must be at least 1 hit per page.");
					return;
				}
			}
		}

		IndexReader reader = DirectoryReader.open(FSDirectory.open(indexDir));
		IndexSearcher searcher = new IndexSearcher(reader);
		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_50);

		BufferedReader in = null;
		if (queryFile != null) {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(queryFile), charset));
		} else {
			in = new BufferedReader(new InputStreamReader(System.in, charset));
		}
		QueryParser parser = new QueryParser(Version.LUCENE_50, field, analyzer);
		do {
			if (queryFile == null && queryString == null) {
				System.out.println("Enter query: ");
			}

			String line = queryString != null ? queryString : in.readLine();

			if (line == null || line.length() == -1) {
				break;
			}

			line = line.trim();
			if (line.length() == 0) {
				break;
			}

			Query query = parser.parse(line);
			System.out.println("Searching for: " + query.toString(field));

			if (repeat > 0) {
				Date start = new Date();
				for (int i = 0; i < repeat; i++) {
					searcher.search(query, null, 100);
				}
				System.out.println("Time: " + (new Date().getTime() - start.getTime()) + "ms");
			}

			doPagingSearch(in, searcher, query, hitsPerPage, raw, queryFile == null
					&& queryString == null);

			if (queryString != null) {
				break;
			}
		} while (shell);

		reader.close();
	}

	@Override
	public String get_default_schema() {
		return "lucene";
	}

	@Override
	public boolean is_ready() {
		return true;
	}
}
