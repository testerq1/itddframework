@echo off
cd %~dp0%
taskkill /f /IM dynamips.exe
taskkill /f /IM dynagen.exe
del ilt_*
del *_log.txt

del .\topology\%1\*_nvram
del .\topology\%1\*_ram
del .\topology\%1\*_bootflash
del .\topology\%1\*_lock
del .\topology\%1\*_disk*
del .\topology\%1\*_rommon_vars
del .\topology\%1\*_log.txt
del .\topology\%1\*.ghost
exit 0