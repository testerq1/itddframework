import common.*;

//genZDC()
//genH3c()
genH3c()
def genPT(){
	int ap=10
	int sta=320
	int staOnline=100
	int online=10
	String prefix="1.3.6.1.4.1.25053.3"
	int offline=ap-online
	def sysname_p="ap"
	
	String[] ipap=GenData.incrIp("12.0.0.0",1,ap)
	String[] ipsta=GenData.incrIp("11.0.0.0",1,sta)

	def header="""# MIMIC Walkfile v5.10
# created by Simulation Wizard
# for efficiency, keep this header.
#########################################
"""
	def footer=""
	File file1=new File("znCapwapStationEntry_walkfile")
	file1.write("")
	file1.append(header)
	
	file1.append("# znCapwapStationEntry: INDEX { $prefix.1.1.3.3.1.1 }\n")
	file1.append("# znStationMAC: read-write OCTET STRING (SIZE (6))\n")
	for(int i=0;i<sta;i++){
		file1.append(" $prefix.1.1.3.3.1.6.$i: \\x"+GenData.genHexStringFromIp("c0 c2 %s %s %s %s",ipsta[i])+"\n")		
	}
	
	file1.append("# znStationIpAddress: read-write IpAddress \n")
	for(int i=0;i<sta;i++){
		file1.append(" $prefix.1.1.3.3.1.18.$i: ${ipsta[i]}\n")
	}
	
	file1.append("# znStationSSID: read-write OCTET STRING \n")
	for(int i=0;i<sta;i++){
		int id=i%ap
		file1.append(" $prefix.1.1.3.3.1.19.$i: WDXR_$id\n")
	}

	
	file1.append("# znApMac: read-write OCTET STRING (SIZE (6))\n")
	for(int i=0;i<sta;i++){
		int apid=i%ap
		file1.append(" $prefix.1.1.3.3.1.35.$i: \\x"+GenData.genHexStringFromIp("c0 c1 %s %s %s %s",ipap[apid])+"\n")
	}
	
//	file1.append(footer)

	file1=new File("znCapwapApStateEntry_walkfile")
	file1.write("")
	file1.append(header)
	file1.append("# znCapwapApStateEntry: INDEX { $prefix.1.1.3.1.1.1 } \n")
	file1.append("# znApIpAddress: read-write IpAddress\n")
	for(int i=0;i<ap;i++){
		file1.append(" $prefix.1.1.3.1.1.3.$i: ${ipap[i]}\n")
	}
	
	file1.append("# znApPhyAddress: read-write OCTET STRING (SIZE (6))\n")
	for(int i=0;i<ap;i++){
		file1.append(" $prefix.1.1.3.1.1.8.$i: \\x"+GenData.genHexStringFromIp("c0 c1 %s %s %s %s",ipap[i])+"\n")
	}
	file1.append("# znApState: read-write INTEGER\n")
	for(int i=0;i<ap;i++){
		file1.append(" $prefix.1.1.3.1.1.9.$i: 6\n")
	}
	
	file1.append("# znApName: read-write OCTET STRING\n")
	for(int i=0;i<ap;i++){
		file1.append(" $prefix.1.1.3.1.1.11.$i: ap_$i\n")
	}
	
	file1.append("# znApUserJoinRequestCount: read-write INTEGER\n")
	for(int i=0;i<ap;i++){
		def v=sta/ap
		file1.append(" $prefix.1.1.3.1.1.51.$i: $v\n")
	}
	
	file1=new File("znApConnectStatisEntry_walkfile")
	file1.write("")
	file1.append(header)
	file1.append("# znApConnectStatisEntry: INDEX { $prefix.1.1.3.23.1.1 } \n")
	//	file1.append("# offlineApIndex: read-write INTEGER\n")
	//	for(int i=online;i<ap;i++){
	//		file1.append(" $prefix.13.1.1.1.$i: 1.$i\n")
	//	}
	
	file1.append("# znApConnectReAssocTrySum: read-write INTEGER\n")
	for(int i=0;i<ap;i++){
		int cnt=GenData.random(10,300)
		file1.append(" $prefix.1.1.3.23.1.11.$i: $cnt\n")
	}
	
	file1.append("# znApConnectAssocFailNum: read-write INTEGER\n")
	for(int i=0;i<ap;i++){
		int cnt=GenData.random(10,300)
		file1.append(" $prefix.1.1.3.23.1.30.$i: $cnt\n")
	}
}

def genH3c(){
	String[] ipsta=["172.16.1.197","172.16.2.21","11.11.11.11","172.16.3.71","11.11.11.12","11.11.11.243","172.16.253.254","172.16.253.251","172.16.253.252","172.16.3.211",
		"172.16.8.42","172.16.8.12","172.16.2.3","172.16.3.89","172.16.253.238","172.16.3.111","172.16.2.32","172.16.1.20","172.16.8.6","172.16.8.32"]//GenData.incrIp("11.0.0.0",1,sta)
			
	int ap=1000
	int sta=32000
	def version="v03"
	//int staOnline=10
	//int online=500
	String prefix="1.3.6.1.4.1.2011.10.2.75"
	//int offline=ap-online
	def apid="210235A22WB095001"
	String[] ipap=GenData.incrIp("12.0.0.0",1,ap)
	//String[] ipsta=GenData.incrIp("11.0.0.0",1,sta)
	
	def header="""# MIMIC Walkfile v5.10
# created by Simulation Wizard
# for efficiency, keep this header.
#########################################
"""
	def footer=""
	File file1=new File("h3cDot11APObjectEntry_walkfile")
	file1.write("")
	file1.append(header)
	file1.append("# h3cDot11APConnectCount: read-write INTEGER")
	file1.append(" $prefix.1.1.2.1.0: $ap")
    file1.append("# h3cDot11StationConnectCount: read-write INTEGER")
	file1.append(" $prefix.1.1.2.2.0: $sta")
    file1.append("# h3cDot11StationCurAssocSum: read-write INTEGER")
	file1.append(" $prefix.1.1.3.6.0: $sta")
	
	file1.append("# h3cDot11APObjectEntry: INDEX { $prefix.2.1.2.1.1 }\n")
	file1.append("# h3cDot11CurrAPIPAddress: read-write IpAddress\n")	
	for(int i=0;i<ap;i++){
		def index=apid+i
		def len=index.length()
		def oid=X2X.snmp_str2index(index)
		file1.append(" $prefix.2.1.2.1.2.$len.$oid: ${ipap[i]}\n")
	}
	
	file1.append("# h3cDot11CurrAPMacAddress: read-only OCTET STRING (SIZE (6))\n")
	for(int i=0;i<ap;i++){
		def index=apid+i
		def len=index.length()
		def oid=X2X.snmp_str2index(index)
		file1.append(" $prefix.2.1.2.1.3.$len.$oid: \\x"+GenData.genHexStringFromIp("01 00 %s %s %s %s",ipap[i])+"\n")
	}
	file1.append("# h3cDot11CurrAPTemplateName: read-write OCTET STRING (SIZE (0..255))\n")
	int cnt=0
	for(int i=0;i<ap;i++){
		def index=apid+i
		def len=index.length()
		def oid=X2X.snmp_str2index(index)
		//def ap_name= "名字$version"+GenData.genString(i%24)		
		file1.append(" $prefix.2.1.2.1.6.$len.$oid: ap_$i\n")
	}
	
	file1.append("# h3cDot11CurrAPName: read-write OCTET STRING (SIZE (0..255))\n")
	for(int i=0;i<ap;i++){
		def index=apid+i
		def len=index.length()
		def oid=X2X.snmp_str2index(index)		
		//def ap_name= "中国$version"+GenData.genString(i%24)
		file1.append(" $prefix.2.1.2.1.8.$len.$oid: ap_$i\n")
	}
	
//	file1.append(footer)

	file1=new File("h3cDot11StationAssociateEntry_walkfile")
	file1.write("")
	file1.append(header)
	file1.append("# h3cDot11StationAssociateEntry: INDEX { $prefix.3.1.1.1 } \n")
	
	file1.append("# h3cDot11StationIPAddress: read-write IpAddress \n")
	for(int i=0;i<sta;i++){
		def mac=GenData.genStringFromIp("201.202.%s.%s.%s.%s",ipsta[i])		
		file1.append(" $prefix.3.1.1.1.2.$mac: ${ipsta[i]}\n")
	}
	
	file1.append("# h3cDot11StationSSIDName: read-write OCTET STRING (SIZE (0..127))\n")
	for(int i=0;i<sta;i++){
		def ssid="SSID_"+(i%ap)
		//def ssid= version+GenData.genString(i%29)
		def mac=GenData.genStringFromIp("201.202.%s.%s.%s.%s",ipsta[i])		
		file1.append(" $prefix.3.1.1.1.12.$mac: $ssid\n")
	}
	file1.append("# h3cDot11StationMac: read-write OCTET STRING (SIZE (6))\n")
	for(int i=0;i<sta;i++){		
		def mac=GenData.genStringFromIp("201.202.%s.%s.%s.%s",ipsta[i])
		file1.append(" $prefix.3.1.1.1.12.$mac: \\x"+GenData.genHexStringFromIp("c9 ca %s %s %s %s",ipsta[i])+"\n")
	}
//	file1.append(footer)

	file1=new File("h3cDot11StationAPRelationEntry_walkfile")
	file1.write("")
	file1.append(header)
	file1.append("# h3cDot11StationAPRelationEntry: INDEX { $prefix.3.1.1.1 } \n")
	file1.append("# h3cDot11CurrAPID: read-write OCTET STRING\n")
	for(int i=0;i<sta;i++){
		def id=apid+(i%ap)
		def mac=GenData.genStringFromIp("201.202.%s.%s.%s.%s",ipsta[i])
		file1.append(" $prefix.3.1.2.1.1.$mac: $id\n")
	}	
	println "done."
}


def genZDC(){
	int ap=1000
	int sta=32000
	int staOnline=10000
	int online=500
	String prefix="1.3.6.1.4.1.1567.80.6"
	int offline=ap-online
	def sysname_p="ap"	
	
	String[] ipap=GenData.incrIp("12.0.0.0",1,ap)
	String[] ipsta=GenData.incrIp("11.0.0.0",1,sta)

	def header="""# MIMIC Walkfile v5.10
# created by Simulation Wizard
# for efficiency, keep this header.
#########################################
"""
	def footer=""
	File file1=new File("commonAplistEntry_walkfile")
	file1.write("")
	file1.append(header)
	file1.append("# commonAplistEntry: INDEX { 1.3.6.1.2.1.2.2.1.1, $prefix.2.1.1 }\n")
	file1.append("# commonApIndex: read-only INTEGER\n")
	for(int i=0;i<ap;i++){
		file1.append(" $prefix.2.1.1.111.$i: $i\n")
	}
	
	file1.append("# sysMacAddress: read-only OCTET STRING (SIZE (6))\n")
	for(int i=0;i<ap;i++){
		file1.append(" $prefix.2.1.2.111.$i: \\x"+GenData.genHexStringFromIp("01 00 %s %s %s %s",ipap[i])+"\n")
	}

	file1.append("# sysIPAddress: read-only IpAddress\n")
	for(int i=0;i<ap;i++){
		file1.append(" $prefix.2.1.3.111.$i: ${ipap[i]}\n")
	}

	file1.append("# sysName: read-write OCTET STRING (SIZE (1..64))\n")
	for(int i=0;i<ap;i++){
		file1.append(" $prefix.2.1.4.111.$i: ${sysname_p}_$i\n")
	}
	
	file1.append("# tapAssocSum: read-write INTEGER\n")
	for(int i=0;i<ap;i++){
		int cnt=GenData.random(1000,3000)
		file1.append(" $prefix.2.1.41.111.$i: $cnt\n")
	}
	
	file1.append("# tapAssocFailSum: read-write INTEGER\n")
	for(int i=0;i<ap;i++){
		int cnt=GenData.random(10,100)
		file1.append(" $prefix.2.1.42.111.$i: $cnt\n")
	}
	
//	file1.append(footer)


	file1=new File("stationlistEntry_walkfile")
	file1.write("")
	file1.append(header)
	file1.append("# stationlistEntry: INDEX { $prefix.3.1.1 } \n")
	file1.append("# staIndex: read-write INTEGER\n")
	for(int i=0;i<sta;i++){
		file1.append(" $prefix.3.1.1.$i: $i\n")
	}
	
	file1.append("# staMAC: read-write OCTET STRING (SIZE (6))\n")
	for(int i=0;i<sta;i++){
		file1.append(" $prefix.3.1.2.$i: \\x"+GenData.genHexStringFromIp("00 00 %s %s %s %s",ipsta[i])+"\n")
	}
	
	file1.append("# apname: read-write OCTET STRING\n")
	for(int i=0;i<sta;i++){
		int id=i%ap
		file1.append(" $prefix.3.1.4.$i: ${sysname_p}_$id\n")
	}
	
	file1.append("# apMac: read-write OCTET STRING (SIZE (6))\n")
	for(int i=0;i<sta;i++){
		int id=i%ap
		file1.append(" $prefix.3.1.5.$i: \\x"+GenData.genHexStringFromIp("01 00 %s %s %s %s",ipap[id])+"\n")
	}
	
	file1.append("# staIPAddress: read-write IpAddress \n")
	for(int i=0;i<sta;i++){
		file1.append(" $prefix.3.1.6.$i: ${ipsta[i]}\n")
	}
	
	file1.append("# sSID: read-write OCTET STRING \n")
	for(int i=0;i<sta;i++){
		int id=i%ap
		file1.append(" $prefix.3.1.9.$i: WDXR_$id\n")
	}
	
//	file1.append(footer)

	file1=new File("offlineAplistEntry_walkfile")
	file1.write("")
	file1.append(header)
	file1.append("# offlineAplistEntry: INDEX { 1.3.6.1.2.1.2.2.1.1, $prefix.13.1.1 } \n")
	file1.append("# offlineApIndex: read-write INTEGER\n")
	for(int i=online;i<ap;i++){
		file1.append(" $prefix.13.1.1.1.$i: 1.$i\n")
	}
	
	file1.append("# offlineApMac: read-write OCTET STRING (SIZE (6))\n")
	for(int i=online;i<ap;i++){
		file1.append(" $prefix.13.1.2.1.$i: \\x"+GenData.genHexStringFromIp("01 00 %s %s %s %s",ipap[i])+"\n")
	}
	
	file1.append("# offlineAPIP: read-write IpAddress\n")
	for(int i=online;i<ap;i++){
		file1.append(" $prefix.13.1.3.1.$i: ${ipap[i]}\n")
	}
	
//	file1.append(footer)

	file1=new File("onlineAplistEntry_walkfile")
	file1.write("")
	file1.append(header)
	file1.append("# onlineAplistEntry: INDEX { $prefix.14.1.1 } \n")
	file1.append("# onlineApIndex: read-write INTEGER\n")
	for(int i=0;i<online;i++){
		file1.append(" $prefix.14.1.1.$i: $i\n")
	}
	
	file1.append("# onlineApMac: read-write OCTET STRING (SIZE (6))\n")
	for(int i=0;i<online;i++){
		file1.append(" $prefix.14.1.2.$i: \\x"+GenData.genHexStringFromIp("01 00 %s %s %s %s",ipap[i])+"\n")
	}
	
	file1.append("# onlineAPIP: read-write IpAddress\n")
	for(int i=0;i<online;i++){
		file1.append(" $prefix.14.1.3.$i: ${ipap[i]}\n")
	}
	
//	file1.append(footer)

	file1=new File("userInfoEntry_walkfile")
	file1.write("")
	file1.append(header)
	file1.append("# userInfoEntry: INDEX { $prefix.24.1.1, $prefix.24.1.2 } \n")
	
	file1.append("# apIndex: read-only INTEGER\n")
	for(int i=0;i<staOnline;i++){
		def mac=GenData.genStringFromIp("00.00.%s.%s.%s.%s",ipsta[i])
		int id=i%online
		file1.append(" $prefix.24.1.2.$id.$mac: $id\n")
	}
	
	file1.append("# userMacAddress: read-write OCTET STRING (SIZE (6))\n")
	for(int i=0;i<staOnline;i++){
		def mac=GenData.genStringFromIp("00.00.%s.%s.%s.%s",ipsta[i])
		int id=i%online
		file1.append(" $prefix.24.1.2.$id.$mac: \\x"+GenData.genHexStringFromIp("00 00 %s %s %s %s",ipsta[i])+"\n")
	}
	
	file1.append("# userIpAddress: read-write IpAddress\n")
	for(int i=0;i<staOnline;i++){
		int id=i%online
		def mac=GenData.genStringFromIp("00.00.%s.%s.%s.%s",ipsta[i])
		file1.append(" $prefix.24.1.3.$id.$mac: ${ipsta[i]}\n")
	}
	
	def num=13760439582
	file1.append("# userLoginName: read-write OCTET STRING\n")
	for(int i=0;i<staOnline;i++){		
		def mac=GenData.genStringFromIp("00.00.%s.%s.%s.%s",ipsta[i])
		int id=i%online
		file1.append(" $prefix.24.1.4.$id.$mac: $num\n")
		num++
	}
}