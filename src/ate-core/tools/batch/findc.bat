@echo off
rem set _ping_cmd=ping -n 5 127.0.0.1
rem FOR /f "tokens=4 delims=(=" %%G IN ('%_ping_cmd% ^|find "loss"') DO echo Result is [%%G]
rem findc.bat tcp lcl_ip:lcl_port
rem findc.bat qq
rem findc.bat udp lcl_ip lcl_port

if /i "%1" == "tcp" (
For /F "usebackq tokens=2,3,5" %%a IN (`netstat -onp tcp`) Do ( ^
If /i "%%a" equ "%2" For /F "usebackq tokens=1" %%i IN (`tasklist /fi "pid eq %%c" /nh`) Do ( Echo %%i)) 

) else if "%1" == "udp" (

For /F "usebackq tokens=2,3,6 delims=: " %%a IN (`netstat -aonp udp`) Do ( ^
If "%%b" equ "%3" (
If "%%a" == "%2" ( 
For /F "usebackq tokens=1" %%i IN (`tasklist /fi "pid eq %%c" /nh`) Do ( Echo %%i)
) else If "%%a" == "0.0.0.0" ( 
For /F "usebackq tokens=1" %%i IN (`tasklist /fi "pid eq %%c" /nh`) Do ( Echo %%i)
) else If "%%a" == "127.0.0.1" ( 
For /F "usebackq tokens=1" %%i IN (`tasklist /fi "pid eq %%c" /nh`) Do ( Echo %%i)
))

)
) else (
For /F "usebackq tokens=1,2" %%i IN (`tasklist /fi "imagename eq %1*" /nh`) Do ( ^
For /F "usebackq tokens=2,3,5" %%a IN (`netstat -onp tcp`) Do ( ^
If /i "%%c" equ "%%j" Echo TCP %%i %%a %%b))

For /F "usebackq tokens=1,2" %%i IN (`tasklist /fi "imagename eq %1*" /nh`) Do (^
For /F "usebackq tokens=2,4" %%a IN (`netstat -aonp udp`) Do (^
If /i "%%b" equ "%%j" Echo UDP %%i %%a %%b))
)
