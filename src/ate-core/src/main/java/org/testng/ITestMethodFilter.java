package org.testng;

/**
 * 
 * @author hxyshfy
 *
 */
public interface ITestMethodFilter {
	public boolean canRunClass(ITestClass testClass);
	public boolean canRunMethod(IMethodInstance method);
	public boolean canRunTestcase(ITestNGMethod method,Object[] parameterValues);
}
