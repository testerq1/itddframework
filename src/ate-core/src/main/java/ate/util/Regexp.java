package ate.util;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Vector;

import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.MatchResult;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.PatternMatcherInput;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;
import org.apache.oro.text.regex.Perl5Substitution;
import org.apache.oro.text.regex.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * perl5正则表达式
 * @author ravi huang
 *
 */
class Regexp {
	PatternMatcher matcher = new Perl5Matcher();
	PatternCompiler compiler = new Perl5Compiler();
	protected static Logger log = LoggerFactory.getLogger(Regexp.class);
	public String[] split(String input, String regularExpression) {
		return split(input, regularExpression, Util.SPLIT_ALL);
	}

	public String[] split(String input, String regularExpression, int limit) {
		Pattern pattern = null;
		try {
			pattern = compiler.compile(regularExpression);
		} catch (MalformedPatternException e) {
			e.printStackTrace();
			return null;
		}
		Vector<String> results = Util.split(matcher, pattern, input, limit);
		String[] arr = new String[results.size()];

		return results.toArray(arr);
	}
	
	void usage(String input, String regex) {
		PatternCompiler compiler = new Perl5Compiler();

		Pattern pattern = null;
		try {
			/*
			 * CASE_INSENSITIVE_MASK : 区分大小写 DEFAULT_MASK : 默认(不区分大小写)
			 * EXTENDED_MASK : 支持Perl5 扩展正则表达式 MULTILINE_MASK : 多行匹配，^$匹配每行内容．
			 * SINGLELINE_MASK　：单行匹配 ^$匹配全部内容. READ_ONLY_MASK : Perl5Pattern
			 * 是只读的，提高性能且线程安全．
			 */
			pattern = compiler.compile(regex, Perl5Compiler.READ_ONLY_MASK
					| Perl5Compiler.MULTILINE_MASK);
		} catch (MalformedPatternException e) {
			e.printStackTrace();
		}

		PatternMatcher matcher = new Perl5Matcher();

		if (matcher.contains(input, pattern)) {
			MatchResult matchResult = matcher.getMatch();

			System.out.println("a:"+matchResult.toString());
		}

		// 3.2 匹配多次, 使用PatternMatcherInput
		PatternMatcherInput patternMatcherInput = new PatternMatcherInput(
				input, 0, input.length());
		while (matcher.contains(patternMatcherInput, pattern)) {
			MatchResult matchResult = matcher.getMatch();

			for(int i=0;i<matchResult.groups();i++){
				String group = matchResult.group(i);
				System.out.println("b:"+group);
			}
			System.out.println(matchResult.begin(0));
			System.out.println(matchResult.end(0)); //
			System.out.println(matchResult.beginOffset(0)); //
			System.out.println(matchResult.endOffset(0)); //

		}

		// 4 创建替换对象 Substiution
		Perl5Substitution substiution = new Perl5Substitution("amos_tl");

		// 5 文本替换
		String output = Util.substitute(matcher, pattern, substiution, input,
				Util.SUBSTITUTE_ALL);

		System.out.println("output:" + output); // output:xxxxTangliangxxxamos_tlxxx
	}
	public boolean match_e(String input, String regex){
		Pattern pattern = null;
		try {			
			pattern = compiler.compile(regex, Perl5Compiler.READ_ONLY_MASK
					| Perl5Compiler.MULTILINE_MASK);
			return matcher.contains(input, pattern);
		} catch (MalformedPatternException e) {
			log.warn("错误的正则表达式："+regex);
		}
		return false;
	}	
	
	public String[] groups(String input, String regex){
		PatternCompiler compiler = new Perl5Compiler();
		Pattern pattern = null;
		try {			
			pattern = compiler.compile(regex, Perl5Compiler.READ_ONLY_MASK
					| Perl5Compiler.MULTILINE_MASK);
		} catch (MalformedPatternException e) {
			log.warn("错误的正则表达式："+regex);
			return null;
		}
		PatternMatcher matcher = new Perl5Matcher();

		if (matcher.contains(input, pattern)) {
			MatchResult matchResult = matcher.getMatch();
			
			String[] gps =new String[matchResult.groups()];
			for(int i=0;i<gps.length;i++)
				gps[i] = matchResult.group(i);
			return gps;
		}

		return null;
	}	
}
