package ate.util;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import static org.joou.Unsigned.*;

import java.nio.charset.Charset;
import java.util.Vector;

import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.MatchResult;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.PatternMatcherInput;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;
import org.apache.oro.text.regex.Perl5Substitution;
import org.apache.oro.text.regex.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.assertj.core.api.Assertions;
import org.joou.*;

import ate.Global;
import ate.ua.RunFailureException;

/**
 * 封装一组 expect函数，支持正则表达式
 * 
 * @author ravih
 *
 */
public final class Expect extends Assertions {
    public static enum MATCH_PATTERN {
        START, CONTAIN, REGEXP, END, EQUAL
    }

    /** The log. */
    protected static Logger log = LoggerFactory.getLogger(Expect.class);
        
    /** 是否区分大小写. 默认为不区分 */
    protected boolean ISNOCASE = true;

    /** 是否严格匹配，默认会在去掉所有WSP后进行比较. */
    protected boolean ISSTRICT = false;

    public Charset charset =Global.charset;
    
    private PatternMatcher matcher = new Perl5Matcher();

    private PatternCompiler compiler = new Perl5Compiler();

    public Pattern compile_e(String regex) throws MalformedPatternException{
        return compiler.compile(regex, Perl5Compiler.READ_ONLY_MASK
                    | Perl5Compiler.MULTILINE_MASK);
    }

    public boolean contains(Object cmd, String rtn) {
        return contains(cmd, rtn, true);
    }

    public boolean contains(Object cmd, String rtn, boolean exp) {
        return expect(cmd, rtn, exp, MATCH_PATTERN.CONTAIN);
    }

    /**
     * endsWith方式.
     * 
     * @see #expect(Object, String, boolean, int)
     *      exp=true,type=MATCH_PATTERN_END
     * @param cmd
     *            the cmd
     * @param rtn
     *            the rtn
     * @return true, if successful
     */
    public boolean end(Object cmd, String rtn) {
        return end(cmd, rtn, true);
    }
    
    /**
     * end.
     *
     * @param cmd
     *            the cmd
     * @param rtn
     *            the rtn
     * @param exp
     *            the exp
     * @return true, if successful
     * @see #expect(Object, String, boolean, int)
     */
    public boolean end(Object cmd, String rtn, boolean exp) {
        return expect(cmd, rtn, exp, MATCH_PATTERN.END);
    }
    
    /**
     * Equal方式，区分大小写.
     * 
     * @param cmd
     *            the cmd
     * @param rtn
     *            the rtn
     * @return true, if successful
     * @see #expect(Object, String, boolean, int)
     */
    public boolean equal(Object cmd, String rtn) {
        return equal(cmd, rtn, true);
    }

    public boolean equal(Object cmd, String rtn, boolean exp) {
        return expect(cmd, rtn, exp, MATCH_PATTERN.EQUAL);
    }

    public boolean equal_e(Object cmd, String rtn) {
        return expect(cmd, rtn, true, MATCH_PATTERN.REGEXP);
    }

    /**
     * Contains方式.
     * 
     * @param cmd
     *            the cmd
     * @param rtn
     *            the rtn
     * @return true, if successful
     * @see #expect(Object, String, boolean, int)
     */
    public boolean expect(Object cmd, String rtn) {
        return expect(cmd, rtn, true);
    }

    /**
     * Expect.
     *
     * @param cmd
     *            the cmd
     * @param rtn
     *            the rtn
     * @param exp
     *            匹配预期，默认为true
     * @return true, if successful
     * @see #expect(Object, String, boolean, int)
     */
    public boolean expect(Object cmd, String rtn, boolean exp) {
        return expect(cmd, rtn, exp, MATCH_PATTERN.CONTAIN);
    }

    /**
     * 执行cmd，给定预期返回值rtn，exp表示期望得到的结果、
     * 
     * Usage： expect("ping 127.0.0.1"," Lost=0",true,MATCH_PATTERN_CONTAIN)
     * 该方法可由子类实现，因此可以将多个参数打包成ArrayList通过cmd传给子类去处理
     *
     * @param cmd
     *            the cmd 要执行的命令
     * @param rtn
     *            the rtn 返回值
     * @param exp
     *            the exp 预期匹配结果
     * @param type
     *            the type 匹配类型，必须是
     * @return true, if successful
     */
    public boolean expect(Object cmd, String rtn, boolean exp,
            MATCH_PATTERN type) {
    	if(Global.verbose)
    		log.debug("Expect cmd:{} return: {}", cmd, rtn);
        if (cmd == null)
            return false;
        return match(cmd.toString(), rtn, exp, type);
    }

    public String[] groups(String input, String regex) {
        PatternCompiler compiler = new Perl5Compiler();
        Pattern pattern = null;
        try {
            pattern = compiler.compile(regex, Perl5Compiler.READ_ONLY_MASK
                    | Perl5Compiler.MULTILINE_MASK);
        } catch (MalformedPatternException e) {
            throw new Error("错误的正则表达式：" + regex);
        }

        PatternMatcher matcher = new Perl5Matcher();

        if (matcher.contains(input, pattern)) {
            MatchResult matchResult = matcher.getMatch();

            String[] gps = new String[matchResult.groups()];
            for (int i = 0; i < gps.length; i++)
                gps[i] = matchResult.group(i);
            return gps;
        }

        return null;
    }

    /**
     * 执行cmd，给定预期返回值rtn，exp表示期望得到的结果、 该函数使用startsWith方式的字符串比较. Usage：
     * match("ping 127.0.0.1"," Lost=0",true,MATCH_PATTERN_CONTAIN)
     *
     * @param rst
     *            the rst
     * @param pattern
     *            匹配模式
     * @param exp
     *            the exp 预期匹配结果
     * @param type
     *            the type 匹配类型，必须是
     * @return true, if successful
     */
    public boolean match(String rst, String pattern, boolean exp,
            MATCH_PATTERN type) {
        if (rst == null)
            return false;
        if (ISNOCASE) {
            rst = rst.toLowerCase();
            pattern = pattern.toLowerCase();
        }
        if (!ISSTRICT) {
            rst = rst.replaceAll("\\s", "");
            pattern = pattern.replaceAll("\\s", "");
        }
        switch (type) {
        case START:
            return rst.startsWith(pattern) == exp;
        case CONTAIN:
            return rst.contains(pattern) == exp;
        case END:
            return rst.endsWith(pattern) == exp;
        case EQUAL:
            return rst.equals(pattern) == exp;
        case REGEXP:
            return match_e(rst, pattern);
        default:
            break;
        }
        return false;
    }

    public boolean match_e(String input, Pattern pattern) {
        if(Global.verbose)
            log.debug("input:{} pattern: {}",input,pattern.getPattern());
        return matcher.contains(input, pattern);
    }

    public boolean match_e(String input, String regex) {  
        Pattern p;
        try {
            p = compile_e(regex);
            return match_e(input, p);
        } catch (MalformedPatternException e) {
            throw new RunFailureException("input:" + input + "   regex:"
                    + regex,e);
        }
    }

    public void set_is_nocase(boolean b) {
        this.ISNOCASE = b;
    }

    public void set_is_strict(boolean b) {
        this.ISSTRICT = b;
    }

    public String[] split(String input, String regularExpression) {
        return split(input, regularExpression, Util.SPLIT_ALL);
    }

    public String[] split(String input, String regularExpression, int limit) {
        Pattern pattern = null;
        try {
            pattern = compiler.compile(regularExpression);
        } catch (MalformedPatternException e) {
            e.printStackTrace();
            return null;
        }
        Vector<String> results = Util.split(matcher, pattern, input, limit);
        String[] arr = new String[results.size()];

        return results.toArray(arr);
    }

    /**
     * StartsWith方式.
     *
     * @param cmd
     *            the cmd
     * @param rtn
     *            the rtn
     * @return true, if successful
     * @see #expect(Object, String, boolean, int)
     */
    public boolean start(Object cmd, String rtn) {
        return start(cmd, rtn, true);
    }

    /**
     * start.
     *
     * @param cmd
     *            the cmd
     * @param rtn
     *            the rtn
     * @param exp
     *            the exp
     * @return true, if successful
     * @see #expect(Object, String, boolean, int)
     */
    public boolean start(Object cmd, String rtn, boolean exp) {
        return expect(cmd, rtn, exp, MATCH_PATTERN.START);
    }
    
    public int ucompare(UInteger ui,int i){
        return ui.compareTo(uint(i));
    }
    
    public int ucompare(UByte ub,int i){
        return ub.compareTo(ubyte(i));
    }
    
    public int ucompare(UShort ub,int i){
        return ub.compareTo(ushort(i));
    }
    public int ucompare(ULong ub,long i){
        return ub.compareTo(ulong(i));
    }
    void usage(String input, String regex) {
        PatternCompiler compiler = new Perl5Compiler();

        Pattern pattern = null;
        try {
            /*
             * CASE_INSENSITIVE_MASK : 区分大小写 DEFAULT_MASK : 默认(不区分大小写)
             * EXTENDED_MASK : 支持Perl5 扩展正则表达式 MULTILINE_MASK : 多行匹配，^$匹配每行内容．
             * SINGLELINE_MASK　：单行匹配 ^$匹配全部内容. READ_ONLY_MASK : Perl5Pattern
             * 是只读的，提高性能且线程安全．
             */
            pattern = compiler.compile(regex, Perl5Compiler.READ_ONLY_MASK
                    | Perl5Compiler.MULTILINE_MASK);
        } catch (MalformedPatternException e) {
            e.printStackTrace();
        }

        PatternMatcher matcher = new Perl5Matcher();

        if (matcher.contains(input, pattern)) {
            MatchResult matchResult = matcher.getMatch();

            System.out.println("a:" + matchResult.toString());
        }

        // 3.2 匹配多次, 使用PatternMatcherInput
        PatternMatcherInput patternMatcherInput = new PatternMatcherInput(
                input, 0, input.length());
        while (matcher.contains(patternMatcherInput, pattern)) {
            MatchResult matchResult = matcher.getMatch();

            for (int i = 0; i < matchResult.groups(); i++) {
                String group = matchResult.group(i);
                System.out.println("b:" + group);
            }
            System.out.println(matchResult.begin(0));
            System.out.println(matchResult.end(0)); //
            System.out.println(matchResult.beginOffset(0)); //
            System.out.println(matchResult.endOffset(0)); //

        }

        // 4 创建替换对象 Substiution
        Perl5Substitution substiution = new Perl5Substitution("amos_tl");

        // 5 文本替换
        String output = Util.substitute(matcher, pattern, substiution, input,
                Util.SUBSTITUTE_ALL);

        System.out.println("output:" + output); // output:xxxxTangliangxxxamos_tlxxx
    }

}
