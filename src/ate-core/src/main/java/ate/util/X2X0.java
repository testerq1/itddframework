package ate.util;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.binary.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.primitives.UnsignedBytes;
import com.google.common.primitives.UnsignedInts;
import com.google.common.primitives.UnsignedLong;
import com.google.common.primitives.UnsignedLongs;
import com.sun.xml.internal.xsom.impl.util.Uri;

import ate.comp.taskman.TTTask;
import ate.rm.dev.Host;

/**
 * 数据转化类
 * @author ravih
 *
 */
public class X2X0 {
	
	protected static Logger log = LoggerFactory
			.getLogger(X2X0.class);

	/**
	 * Array to string 如果为null,则加一个null进去 如果为""，则加一个#字符进去 否则，加toString()进去
	 * 
	 * @param al
	 *            the ArrayList
	 * @return the string
	 */
	public static String al2Str(ArrayList al) {
		String tmp = "";
		for (int i = 0; i < al.size(); i++) {
			Object s = al.get(i);
			if (s != null) {
				tmp += s.toString();
				if (s.toString().length() == 0)
					tmp += "#";
			} else
				tmp += "null";
		}
		return tmp;
	}
	
	public static String asString(byte[] array) {
		return asString(array, ':');
	}	
	/**
	 * Convers the given byte array to a string using the supplied separator
	 * character.
	 * 
	 * @param array
	 *            array to convert
	 * @param separator
	 *            separator character to use in between array elements
	 * @return the converted string
	 */
	public static String asString(byte[] array, char separator) {
		return asString(array, separator, 16); // Default HEX
	}
	/**
	 * Converts the given byte array to a string using the supplied separator
	 * character and radix for conversion of the numerical component.
	 * 
	 * @param array
	 *            array to convert
	 * @param separator
	 *            separator character to use in between array elements
	 * @param radix
	 *            numerical radix to use for numbers
	 * @return the converted string
	 */
	public static String asString(byte[] array, char separator, int radix) {
		return asString(array, separator, radix, 0, array.length);
	}
	
	/**
	 * Convers the given byte array to a string using the supplied separator
	 * character.
	 * 
	 * @param array
	 *            array to convert
	 * @param separator
	 *            separator character to use in between array elements
	 * @param radix
	 *            the radix
	 * @param start
	 *            the start
	 * @param len
	 *            the len
	 * @return the converted string
	 */
	public static String asString(byte[] array, char separator, int radix,
			int start, int len) {

		final StringBuilder buf = new StringBuilder();

		for (int i = start; i < (start + len); i++) {
			byte b = array[i];
			if (buf.length() != 0) {
				buf.append(separator);
			}

			buf.append(Integer.toString((b < 0) ? b + 256 : b, radix)
					.toUpperCase());
		}

		return buf.toString();
	}
	/**
	 * Convers the given byte array to a string using the supplied separator
	 * character.
	 * 
	 * @param array
	 *          array to convert
	 * @param separator
	 *          separator character to use in between array elements
	 * @param radix
	 *          the radix
	 * @param start
	 *          the start
	 * @param len
	 *          the len
	 * @return the converted string
	 */
	public static String asString(byte[] array,
			String separator,
			int radix,
			int start,
			int len) {

		final StringBuilder buf = new StringBuilder();

		for (int i = start; i < (start + len); i++) {
			byte b = array[i];
			if (buf.length() != 0)
				buf.append(separator);

			buf.append(Integer.toString((b < 0) ? b + 256 : b, radix).toUpperCase());
		}

		return buf.toString();
	}

	/**
	 * Handles various forms of ip6 addressing
	 * 
	 * <pre>
	 * 2001:0db8:0000:0000:0000:0000:1428:57ab
	 * 2001:0db8:0000:0000:0000::1428:57ab
	 * 2001:0db8:0:0:0:0:1428:57ab
	 * 2001:0db8:0:0::1428:57ab
	 * 2001:0db8::1428:57ab
	 * 2001:db8::1428:57ab
	 * </pre>
	 * 
	 * .
	 * 
	 * @param array
	 *            address array
	 * @param holes
	 *            if true holes are allowed
	 * @return formatted string
	 */
	public static String asStringIp6(byte[] array, boolean holes) {

		if (array.length != 16) {
			throw new IllegalArgumentException(
					"expecting 16 byte ip6 address array");
		}

		StringBuilder buf = new StringBuilder();

		int len = 0;
		int start = -1;
		/*
		 * Check for byte compression where sequential zeros are replaced with
		 * ::
		 */
		/*
		 * Bug Fix#117 - FormatUtils.asStringIp6() causes an
		 * OutOfMemoryException
		 */
		for (int i = 0; i < array.length && holes; i++) {
			if ((i % 2) == 0 && array[i] == 0 && array[i + 1] == 0) {
				if (len == 0) {
					start = i;
				}
			}
			i++;
			len += 2;

			/*
			 * Only the first sequence of 0s is compressed, so break out
			 */
			if (array[i] != 0 && len != 0) {
				break;
			}
		}

		/*
		 * Now round off to even length so that only pairs are compressed
		 */
		if (start != -1 && (start % 2) == 1) {
			start++;
			len--;
		}

		if (start != -1 && (len % 2) == 1) {
			len--;
		}

		for (int i = 0; i < array.length; i++) {
			if (i == start) {
				buf.append(':');
				i += len - 1;

				if (i == array.length - 1) {
					buf.append(':');
				}
				continue;
			}

			byte b = array[i];

			if (buf.length() != 0 && (i % 2) == 0) {
				buf.append(':');
			}
			
			/*
			 * Bug fix#125 FormatUtils.asStringIp6 prepends byte values < 0 with zeros 
			 */
			if (b >=0 && b < 16) {
				buf.append('0');
			}
			buf.append(Integer.toHexString((b < 0) ? b + 256 : b).toUpperCase());
		}

		return buf.toString();
	}

	/**
	 * Byte 2 hexstring.
	 * 
	 * @param b
	 *            the b
	 * @return the string
	 * @throws EncoderException
	 */
	public static String b2h(byte b) {
		String tmp=Integer.toHexString(b);
		int len=tmp.length();
		if(len>2)
			return tmp.substring(len-2);
		
		return len==1?("0"+tmp):tmp;
	}
	public static int b2i(byte b) {
		return bs2i(new byte[]{b});
	}
	/**
	 * bs2chs([0xa1,0xa2,0xa3,0xa4],".")
	 * ==>a1.a2.a3.a4
	 * 
	 * @param bs
	 * @param deli
	 * @return
	 */
	public static String bs2chs(byte[] bs,String deli){	    
		String s = "";
		if(bs==null||bs.length==0)
            return s;
		
		for (int i=0;i<bs.length-1;i++){
			s += b2h(bs[i])+deli;
		}		
		return s+b2h(bs[bs.length-1]);		
	}

	/**
	 * Bytes[] to hexstring.
	 *  bs2h("ddd".getBytes())
	 *  ==>545254525452
	 * @param bs
	 *            the bs
	 * @return the string
	 */
	public static String bs2h(byte[] bs) {
		byte[] cs = new Hex().encode(bs);
		String tmp = "";
		for (byte element : cs)
			tmp += element;
		return tmp;
	}

	public static int bs2i(byte[] b) {
		if(b.length>4)
			throw new IllegalArgumentException("out of size!");
		int mask = 0xff;
		int temp = 0;
		int n = 0;
		for (byte element : b) {
			n <<= 8;
			temp = element & mask;
			n |= temp;
		}
		return n;
	}
	
	public static String bs2ip(byte[] address){
		if (address.length == 4) {
			return asString(address, '.', 10);
		} else {
			return asStringIp6(address, true);
		}	
	}
	
	/**
	 * bytes数组转int数组，处理负号问题
	 * @param bs
	 * @return
	 */
	public static int[] bs2is(byte[] bs) {
		int[] tmp=new int[bs.length];
		for (int i = 0; i < bs.length; i++)
			tmp[i]=bs[i];
		for(int i=0;i<bs.length;i++)
			if(bs[i]>=0)
				tmp[i]=bs[i];
			else 
				tmp[i]=(bs[i]+256);
		return tmp;
	}
	
	/**
	 * Char to byte[2].
	 * 
	 * @param value
	 *            the value
	 * @return the byte[]
	 */
	public static byte[] ch2b(char value) {
		byte[] b = new byte[2];
		b[0] = (byte) (value >> 8);
		b[1] = (byte) (value);
		return b;
	}

	/**
	 * "10:00:00:00:00:00:00:00:1f:00:00:00" to byte[] chs2b(s).
	 * 
	 * @param colonstring
	 *            the colonstring
	 * @return the byte[]
	 * @throws DecoderException
	 *             the decoder exception
	 */
	public static byte[] chs2b(String colonstring){
		String[] ss = colonstring.split("[.|:|-| ]");
		String s = "";
		for (int i = 0; i < ss.length; i++) {
			if (ss[i].length() % 2 == 1)
				ss[i] = "0" + ss[i];
			s += ss[i];
		}
		return hexs2b(s);
	}
	
	public static String decode_base64(String s){		
		return new String(new Base64().decodeBase64(s.getBytes()));		
	}
	
	public static String encode_base64(String s){		
		return new String(new Base64().encodeBase64(s.getBytes()));		
	}
	
	/**
    * convert the amount of ms in ellapsed time (hh:mm:ss)
    * @param timeInSeconds
    * @return a string 
    */	
    public static String getElapsedTimeString(long timeInSeconds) {
        long hours, minutes, seconds;
        hours = timeInSeconds / 3600;
        timeInSeconds = timeInSeconds - (hours * 3600);
        minutes = timeInSeconds / 60;
        timeInSeconds = timeInSeconds - (minutes * 60);
        seconds = timeInSeconds;
        return hours + " h " + minutes + " min " + seconds + " sec";
     }
    /**
	 * 将一个十六进制数字的字符串解码为byte数组 hexs2b("000a0b0c0d") --> byte[]{00,0a,0b,0c,0d}.
	 * 
	 * @param hexstring
	 *            hex字符串
	 * @return byte[]数组
	 * @throws DecoderException
	 *             the decoder exception
	 */
	public static byte[] hexs2b(String hexstring) {
		try {
			return (byte[]) new Hex().decode(hexstring.replaceAll(" |\n", ""));
		} catch (DecoderException e) {			
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * int to hexstring.
	 * 
	 * @param i
	 *            the i
	 * @return the string
	 */
	public static String i2h(int i) {
		return Integer.toHexString(i);
	}

	

	public static String InputStreamTOString(InputStream in,Charset encoding) throws Exception{  
        
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();  
        byte[] data = new byte[1024];  
        int count = -1;  
        while((count = in.read(data,0,1024)) != -1)  
            outStream.write(data, 0, count);  
          
        data = null;  
        return new String(outStream.toByteArray(),encoding);  
    }
	
	/**
	 * Int to byte[len].
	 * 
	 * @param value
	 *            the value
	 * @param len
	 *            the len
	 * @return the byte[]
	 */
	public static byte[] int2b(int value, int len) {
		byte[] b = new byte[len];
		b[len - 4] = (byte) (value >>> 24);
		b[len - 3] = (byte) (value >>> 16);
		b[len - 2] = (byte) (value >>> 8);
		b[len - 1] = (byte) (value);
		return b;
	}  
	
	public static String int2ip(int value) {
		byte[] b = int2b(value,4);
		String ip="";
		for(int i=0;i<4;i++){
			if(b[i]<0)
				ip+=(b[i]+256);
			else 
				ip+=b[i];
			if(i!=3)
				ip+=".";
		}
		return ip;
	}
	/**
	 * Ip to byte[4].
	 * 
	 * @param ip
	 *            the ip
	 * @return the byte[]
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	public static byte[] ip2b(String ip){
		try {
			return InetAddress.getByName(ip).getAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * ip2i("1.2.3.4")==0x01020304
	 * @param ip
	 *            the ip
	 * @return the ip to int value
	 */
	public static int ip2i(String ip){
		byte[] bs;
		try {
			bs = InetAddress.getByName(ip).getAddress();
			return bs2i(bs);
		} catch (UnknownHostException e) {
			log.warn("bad ip!");
		}
		return -1;
	}
	
	/**
	 * Left pad.
	 * 
	 * @param v
	 *            the v
	 * @param pad
	 *            the pad
	 * @param l
	 *            the l
	 * @return the string
	 */
	public static String lPad(String v, String pad, int l) {
		int len = v.length();
		for (int i = 0; i < l - len; i++)
			v = pad + v;
		return v.substring(0, l);
	}
	
	public static boolean is_equal(Object[][] a1,Object[][] a2){
		if(a1.length!=a2.length)
			return false;
		for(int i=0;i<a1.length;i++)
			if(!Arrays.equals(a1[i], a2[i]))
				return false;
		
		return true;
	}
	
	public static byte[] mac2bs(String hexstring) {
		byte[] bs=chs2b(hexstring);
		
		if(bs.length!=6)
			throw new Error("wrong mac size!");
		
		return bs;
	}
	
	public static String now() {
        Date date=new Date();
        java.text.SimpleDateFormat dateFormat = null;
        try {
            dateFormat = new java.text.SimpleDateFormat("yyMMdd-HH:mm:ss");
        } catch (Exception e) {
            log.debug("时间格式错误，采用默认格式");
            dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
        }
        return dateFormat.format(date);
    }

	/**
	 * Right pad.
	 * 
	 * @param v
	 *            the v
	 * @param pad
	 *            the pad
	 * @param l
	 *            the l
	 * @return the string
	 */
	public static String rPad(String v, String pad, int l) {
		int len = v.length();
		for (int i = 0; i < l - len; i++)
			v += pad;
		return v.substring(0, l);
	}

	/**
	 * 将一个字符串转换为其ascii码对应的hex字符串 str2Hex("abcdefg") -->61626364656667.
	 * 
	 * @param s
	 *            the s
	 * @return the string
	 * @throws EncoderException
	 *             the encoder exception
	 */
	public static String s2h(String s) throws EncoderException {
		char[] cs = (char[]) new Hex().encode(s);
		String tmp = "";
		for (char element : cs)
			tmp += element;
		return tmp;
	}
	
	public static int s2i(String s) {		
		return Integer.parseInt(s);
	}
	public static byte[] s2bs(String s,int len,Charset charset) {	    
	    return Arrays.copyOf(s.getBytes(charset), len);
    }

	public static String task2xml(TTTask f) throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMLEncoder xmlEncoder = new XMLEncoder(baos);
		xmlEncoder.writeObject(f);
		xmlEncoder.close();
		return baos.toString();
	}
	
	/**
	 * Trim end.
	 * 
	 * @param s
	 *            the s
	 * @return the string
	 */
	public static String trimEnd(String s) {
		int len = s.length();

		byte[] val = s.getBytes(); /* avoid getfield opcode */

		while ((len > 0) && (val[len - 1] <= ' '))
			len--;
		return s.substring(0, len);
	}
		
	public static String encode_url(String input) {
        StringBuilder resultStr = new StringBuilder();
        for (char ch : input.toCharArray()) {
            if (isUnsafe(ch)) {
                resultStr.append('%');
                resultStr.append(toHex(ch / 16));
                resultStr.append(toHex(ch % 16));
            } else {
                resultStr.append(ch);
            }
        }
        return resultStr.toString();
    }

    private static char toHex(int ch) {
        return (char) (ch < 10 ? '0' + ch : 'A' + ch - 10);
    }

    private static boolean isUnsafe(char ch) {
        if (ch > 128 || ch < 0)
            return true;
        return " %$&+,/:;=?@<>#%".indexOf(ch) >= 0;
    }
    
    public static int unsigned_compare(byte b,int i){
        return Integer.compare(UnsignedBytes.toInt(b), i);
    }
    
//    public static int unsigned_compare(int b,long i){
//        return Long.compare(UnsignedLong.,(int)i);
//    }
//    
//    public static int unsigned_compare(long b,double i){
//        return UnsignedLongs.compare(b,(long)i);
//    } 
    
	public static TTTask xml2Task(String xml){
		XMLDecoder decoder = new XMLDecoder(new StringBufferInputStream(xml));
		TTTask o = (TTTask) decoder.readObject();
		decoder.close();
		return o;
	}
}
