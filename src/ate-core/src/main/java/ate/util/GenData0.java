package ate.util;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * The Class GenData.
 */
public class GenData0 {
	
	/**
	 * 从datas中随机取n个返回
	 * @param datas
	 * @param cnt
	 * @return
	 */
	public static String[] samples(List<String> datas, int cnt){
		if(cnt>=datas.size())
			return datas.toArray(new String[datas.size()]);		
		String[] rst=new String[cnt];		
		for(int i = 0;i<rst.length;i++){
			int id=random(0,datas.size()-1);
			rst[i]=datas.remove(id);
		}		
		return rst;		
	}
	public static String[] samples(String[] datas, int cnt){
		ArrayList<String> tmp=new ArrayList<String>();
		for(int i=0;i<datas.length;i++)
			tmp.add(datas[i]);
		return samples(tmp,cnt);
		
	}
    
	/**
	 * Gets the exception desc.
	 *
	 * @param e the e
	 * @return the exception desc
	 */
	public static String getExceptionDesc(Exception e){
		StringBuffer sb = new StringBuffer();
		StackTraceElement[] trace = e.getStackTrace();
        for (int i=0; i < trace.length; i++)
            sb.append("\tat " + trace[i]);

        Throwable ourCause = e.getCause();
        if (ourCause != null)
        	sb.append("\tat " + ourCause);
		return sb.toString();
	}
	/**
	 * Expand datas.
	 *
	 * @param entry the entry
	 * @return the array list
	 */
	public static ArrayList<ArrayList<String>> expandDatas(Map.Entry<String,Object> entry){
		ArrayList<ArrayList<String>> al =new ArrayList<ArrayList<String>>();
		Object o=entry.getValue();
		if(o==null||o instanceof String){
			ArrayList<String> tmp =new ArrayList<String>();
			tmp.add(entry.getKey());
			tmp.add((String)o);
			tmp.add("0");
			al.add(tmp);
		}else if(o instanceof ArrayList){
			ArrayList<String> v=(ArrayList<String>)o;
			for(int i=0;i<v.size();i++){
				ArrayList<String> tmp =new ArrayList<String>();
				tmp.add(entry.getKey());
				tmp.add((String)v.get(i));
				tmp.add("0");
				al.add(tmp);
			}
		}else if(o instanceof HashMap){
			HashMap<String,String> hm=(HashMap<String,String>)o;
			Iterator it=hm.keySet().iterator();
			while(it.hasNext()){
				ArrayList<String> tmp =new ArrayList<String>();
				tmp.add(entry.getKey());
				String value=(String)it.next();
				tmp.add(value);				
				Object rst=hm.get(value);
				if(rst==null||rst.toString().length()==0){
					tmp.add("0");
				}else{
					if(rst.toString().equals("&skip")){
						continue;
					}
					tmp.add(rst.toString());
				}
				al.add(tmp);
			}
		}		
		return al;
	}
	/**
	 * Gets the input stream from uri
	 *
	 * @param filepath the filepath
	 * @return the input stream
	 * @throws Exception the exception
	 */
	public static InputStream getInputStream(String filepath) throws Exception {
		if(isUrl(filepath)){
			URL url = URI.create(filepath).toURL();
			return url.openStream();
		}else{
			return new FileInputStream(new File(filepath));
		}
	}
	/**
	 * Checks if is url.
	 *
	 * @param path the path
	 * @return true, if is url
	 */
	public static boolean isUrl(String path) {
		return path.startsWith("http:") || path.startsWith("ftp:");
	}
	/**
	 * Checks if is email uri.
	 *
	 * @param uri the uri
	 * @return true, if is email uri
	 */
	public static boolean isEmailURI(String uri){
		return true;
	}
	
	public static byte[] randomBytes(int len){
	    byte[] b = new byte[len];
	    new Random().nextBytes(b);		
		return b;
	}
	/**
	 * Random. 
	 *
	 * @param from the from
	 * @param to the to
	 * @return the int
	 */
	public static int random(int from,int to){
		double d=Math.random();
		return (int) (from+Math.ceil((to-from)*d));		
	}
	public static long random(long from,long to){
		double d=Math.random();
		return (long) (from+Math.ceil((to-from)*d));		
	}
//	/**
//	 * Incr ip. 不包含beginIp
//	 *
//	 * @param beginIp the begin ip
//	 * @param step the step
//	 * @param cnt the cnt
//	 * @return the string[]
//	 * @throws UnknownHostException the unknown host exception
//	 */
//	
//	public static String[] incrIp(String beginIp,int step, int cnt) throws UnknownHostException{
//		String[] tmp=new String[cnt];		
//		int v = X2X0.ip2i(beginIp);
//		
//		byte[] bs=new byte[4];
//		for(int i=1;i<=cnt;i++){			
//			bs=X2X0.int2b(v+i, 4);			
//			tmp[i-1]=InetAddress.getByAddress(bs).getHostAddress();
//			//System.out.println(tmp[i-1]);
//		}			
//		return tmp;
//	}	
	public static int[] genIps(String beginIp,int step, int cnt){
		int[] tmp=new int[cnt];		
		int v = X2X0.ip2i(beginIp);
		for(int i=1;i<=cnt;i++){
			tmp[i-1]=v+step;
		}			
		return tmp;
	}	
	
	public String getAnyOne(Object[] o){
		return o[this.random(0, o.length-1)].toString();
	}
	
	public String getAnyOne(int[] o){
		return o[this.random(0, o.length-1)]+"";
	}
	
	/**
	 * Gen string from ip.
	 * Usage:
	 * 		genStringFromIp("00-00-%s-%s-%s-%s","10.11.12.13")
	 * 
	 * Return  00-00-10-11-12-13
	 * 
	 * @param format the format
	 * @param ip the ip
	 * @return the string
	 * @throws UnknownHostException the unknown host exception
	 */
	public static String genStringFromIp(String format,String ip) throws UnknownHostException{
		byte[] bs = InetAddress.getByName(ip).getAddress();		
		String[] hs= new String[bs.length];
		for(int i=0;i<hs.length;i++){
			if(bs[i]>=0)
				hs[i]=""+bs[i];
			else hs[i]=""+(bs[i]+256);			
		}
		
		return String.format(format, hs[0],hs[1],hs[2], hs[3]);		
	}

	/**
	 * Gen hex string from ip.
	 * Usage:
	 * 		genHexStringFromIp("00-00-%s-%s-%s-%s","10.11.12.13")
	 * 
	 * Return  00-00-0a-0b-0c-0d
	 *
	 * @param format the format
	 * @param ip the ip
	 * @return the string
	 * @throws UnknownHostException the unknown host exception
	 */
	public static String genHexStringFromIp(String format,String ip) throws UnknownHostException{
		byte[] bs = InetAddress.getByName(ip).getAddress();
		String[] hs= new String[bs.length];
		
		for(int i=0;i<hs.length;i++){			
			hs[i]=Integer.toHexString(bs[i]);
			int len=hs[i].length();
			if(hs[i].length()==1){
				hs[i]="0"+hs[i];
			}
			if(hs[i].length()>2){
				hs[i]=hs[i].substring(len-2);
			}
		}
		return String.format(format, hs[0],hs[1],hs[2], hs[3]);		
	}
	public static String genString(int len){
		String rtn="";
		for(int i=0;i<len;i++)			
			rtn+=(char)random('a','z');		
		return rtn;
	}
	public static void main(String[] args) throws Exception{
		ArrayList<String> ss=new ArrayList<String>(1000);
		for(int i=0;i<1000;i++){
			ss.add(i+"");
		}
		String[] sample=samples(ss,15);
		for(int i=0;i<sample.length;i++)
			System.out.println(sample[i]);		
	}	
}
