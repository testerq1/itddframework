package ate.util;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.rm.dev.Host;
import ate.rm.file.ReadWriteTextFileWithEncoding;

public abstract class AbstractTool {
	protected static String home=Host.HOME;
	protected static Logger log = LoggerFactory.getLogger(AbstractTool.class);
	
	public static File loadFile(String fname){		
		File tmp=new File(fname);
		if(tmp.exists()&&tmp.isFile())
			return tmp;
		return null;
	} 
	/**
	 * 读出文件中所有行
	 * @param fileName 文件名
	 * @param charset 指定字符集
	 * @return
	 */
	public static String read_all_lines(String fileName,String charset){
		ReadWriteTextFileWithEncoding rw=new ReadWriteTextFileWithEncoding(fileName,charset);		
		return rw.read_all_lines();
	}
	
	public static String read_all_lines(String fileName){
		ReadWriteTextFileWithEncoding rw=new ReadWriteTextFileWithEncoding(fileName);		
		return rw.read_all_lines();
	}
	
	public Properties loadProps(String fname) throws Exception{		
		Properties prop=new Properties();
		prop.load(new FileReader(fname));  
		return prop;
	}
	
	public static Object create_instance(Object[] paras,Class cls){
		try {				
			Class[] pcls=new Class[paras.length];
			for(int i=0;i<paras.length;i++)
				pcls[i]=paras[i].getClass();
			return cls.getConstructor(pcls).newInstance(paras);
		} catch (Exception e) {
			e.printStackTrace();				
		}
		return null;
	}
	public static Object create_instance_with_string(String para,Class cls){
		try {
			return cls.getConstructor(String.class).newInstance(para);
		} catch (Exception e) {
			e.printStackTrace();				
		}
		return null;
	}
	
	public String getProp(Map ht,String key){
		Object s=ht.get(key);
		if(s==null)
			return key;
		return s.toString();
	}
		
	public static void sleep(long millis) throws InterruptedException{
		Thread.currentThread();
		Thread.sleep(millis);
	}
}
