package ate.util;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;

public class MyConsole {
	
	private MyConsole(){}
	
	public static MyConsole console;	
	
	private byte[] rcb = new byte[1024];
	
	public static MyConsole getMyConsole(){
		if(console==null){
			console=new MyConsole();
		}
		return console;
	}
	
	public void printf(String fmt, Object... args) throws Exception {
		System.out.printf(fmt, args);
	}
	
	public void println(String s){
		System.out.println(s);
	}
	

	public String readLine(String fmt, Object... args) throws Exception {
		String line = null;

		if (fmt.length() != 0)
			System.out.format(fmt, args);
		try {
			byte[] ca = readline();
			if (ca != null)
				line = new String(ca);
		} catch (IOException x) {
			throw new Error(x);
		}
		return line;
	}

	private byte[] readline() throws IOException {
		int len = System.in.read(rcb, 0, rcb.length);
		if (len < 0)
			return null; // EOL
		if (rcb[len - 1] == '\r')
			len--; // remove CR at end;
		else if (rcb[len - 1] == '\n') {
			len--; // remove LF at end;
			if (len > 0 && rcb[len - 1] == '\r')
				len--; // remove the CR, if there is one
		}
		byte[] b = new byte[len];
		if (len > 0) {
			System.arraycopy(rcb, 0, b, 0, len);
		}
		return b;
	}

}
