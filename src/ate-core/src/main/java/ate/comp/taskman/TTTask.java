package ate.comp.taskman;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import ate.util.X2X0;

public class TTTask implements Serializable,Cloneable {	
	public static String formatLongToTimeStr(Long l) {
        int hour = 0;
        int minute = 0;
        int second = l.intValue() / 1000;

        if (second > 60) {
            minute = second / 60;
            second = second % 60;
        }
        if (minute > 60) {
            hour = minute / 60;
            minute = minute % 60;
        }
        return (hour + "h" + minute  + "m" + second  + "s");
    }
	protected long configTime;
	protected String cronExp="";
	protected long exeDuration=0l;
	protected EXERESULT exeResult=EXERESULT.NEW;
	protected String mailTo="";
	protected String paras="";
	protected String resultDesc="";
	protected String src_ip="";
	protected String type="";
	protected String workerClient="";
	protected TASKCLASS taskClass=TASKCLASS.RUNONCE;
	private String uniqueId;

	private static SimpleDateFormat dateformat1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss E");
	
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	
	public String getCronTaskID(){
		StringBuffer sb=new StringBuffer();
		sb.append(getType()).append("_").append(getParas().trim()).append("_").append(getTime());
		
		return sb.toString();
				
	}
	protected boolean isLocal=false;
	public boolean isLocal() {
		return isLocal;
	}
	public void setLocal(boolean isLocal) {
		this.isLocal = isLocal;
	}
	
	
	public TTTask(){		
		this.configTime=new Date().getTime();		
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public TASKCLASS getTaskClass() {
		return taskClass;
	}
	public boolean isCron(){
		return this.cronExp.length()>0;
	}
	
	public void setTaskClass(TASKCLASS taskClass) {
		this.taskClass = taskClass;
	}
	public TTTask clone(){
		TTTask tmp = new TTTask();
		tmp.workerClient        =workerClient       ;
		tmp.type        =type       ;
		tmp.paras       =paras      ;
		tmp.src_ip      =src_ip     ;
		tmp.mailTo      =mailTo     ;
		tmp.configTime  =configTime ;
		tmp.cronExp     =cronExp    ;
		tmp.exeResult   =exeResult  ;
		tmp.resultDesc  =resultDesc ;
		tmp.exeDuration =exeDuration;
		tmp.exeResult   =this.exeResult;
		tmp.taskClass   =taskClass;
		tmp.uniqueId    =uniqueId;
		return tmp;
	}
	public long getConfigTime() {
		return configTime;
	}
	public String getCronExp() {
		return cronExp;
	}
	public long getExeDuration() {
		
		return exeDuration;
	}
	public EXERESULT getExeResult() {
		return exeResult;
	}
	public String getMailTo() {
		return mailTo;
	}
	public String getParas() {
		return paras;
	}
	public String getResultDesc() {
		return resultDesc;
	}
	public String getSrc_ip() {
		return src_ip;
	}

	public String getType() {
		return type;
	}
	public String getWorkerClient() {
		return workerClient;
	}
	public void setCronExp(String cronExp) {
		this.cronExp = cronExp;
		
	}
	public String getTime(){		
		int spacePos=cronExp.indexOf(" ");
		return cronExp.substring(spacePos).trim();
	}
	
	public String getSubCmd(){		
		int spacePos=cronExp.indexOf(" ");
		return cronExp.substring(0,spacePos).trim();
	}
	
	public void setExeDuration(long exeDuration) {
		this.exeDuration = exeDuration;
	}
	public void setExeResult(EXERESULT exeResult) {
		this.exeResult = exeResult;
	}
	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}
	public void setParas(String paras) {
		this.paras = paras;
	}
	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}
	public void setSrc_ip(String src_ip) {
		this.src_ip = src_ip;
	}
	public void setType(String type) {
		this.type = type;
	}

	public void setWorkerClient(String workerClient) {
		this.workerClient = workerClient;
	}
    @Override
	public String toString(){
    	String s=null;
    	try {
			s=X2X0.task2xml(this);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}
    public String toSummary(){
    	return "type="+this.type+
    			";para="+this.paras+
    			";cronexp="+this.cronExp+
    			";configTime="+dateformat1.format(new Date(this.configTime))+
    			";configip="+this.src_ip+
    			";class="+this.taskClass;
    }
}
