package ate.actions;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.PrintStream;
import java.io.PrintWriter;

import org.quartz.CronScheduleBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.ITaskRunner;
import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;
import ate.rm.dev.Host;

public abstract class ATaskRunner implements ITaskRunner {
	private static String HOME = Host.HOME;

	protected static Logger logger = LoggerFactory.getLogger(ATaskRunner.class);
	protected PrintStream output;
	protected PrintWriter log;
	protected String[] paras;
	protected TTTask task;
	public final static int MAILTO = 4;
	public final static int CRONEXP = 3;
	public final static int PARAS = 2;
	public final static int NO_ERROR = 0;
	private INotifier notif;

	public ATaskRunner() {
		output = System.out;
		log = new PrintWriter(System.out);
	}

	@Override
	public boolean needReport() {
		return true;
	}

	@Override
	public boolean onlyCanRunOnce() {
		return true;
	}

	public static boolean isMailTo(String url) {
		if (url != null && url.startsWith("mailto:"))
			return true;
		return false;
	}

	@Override
	public int validTaskConfig(final TTTask task) {
		if (task.getMailTo().length() > 0 && !isMailTo(task.getMailTo())) {
			logger.debug(task.getMailTo() + " is illegal!");
			return MAILTO;
		}
		if (task.getCronExp().length() > 0 && !isLegalCronException(task.getCronExp())) {
			logger.debug(task.getCronExp() + " is illegal!");
			return CRONEXP;
		}
		return NO_ERROR;
	}

	public static boolean isLegalCronException(String exp) {
		exp = exp.trim().toLowerCase();
		if (!exp.startsWith("stop") && !exp.startsWith("start"))
			return false;
		try {
			CronScheduleBuilder.cronSchedule(exp.substring(exp.indexOf(" ")).trim());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public String paras2String(String[] paras) {
		String tmp = "";
		for (int i = 0; i < paras.length - 1; i++)
			tmp += paras[i] + " ";
		if (paras.length > 0)
			tmp += paras[paras.length - 1];
		return tmp;
	}

	@Override
	public void setNotifier(INotifier notif) {
		this.notif = notif;
	}

	@Override
	public void run() {
		task.setExeResult(EXERESULT.RUNNING);
		notif.INFORM(task);

		execute(task);

		notif.INFORM(task);

	}

	// public TTTask execute(String[] paras){
	// this.task=new TTTask();
	// this.task.setParas(paras2String(paras));
	// execute(task);
	// return task;
	// }
	//
	// public TTTask execute(String paras){
	// this.task=new TTTask();
	// this.task.setParas(paras);
	// this.paras=paras.split(" ");
	// return execute(task);
	// }
	public void showTips(String s) {
		log.println(s);
	}

	@Override
	public void setTask(TTTask task) {
		this.task = task;
	}

	@Override
	public TTTask getTask() {
		return task;
	}
}
