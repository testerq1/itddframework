package ate.actions;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.util.GroovyScriptEngine;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.groovy.control.CompilationFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.CommandLineArgs;
import org.testng.ITestResult;
import org.testng.SuiteRunner;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import ate.Global;
import ate.TTClassLoader;
import ate.comp.taskman.EXERESULT;
import ate.comp.taskman.TTTask;
import ate.rm.dev.Host;
import ate.rm.dev.Testbed;
import ate.testng.ATECommandLineArgs;
import ate.testng.MyTestListenerAdapter;
import ate.testng.MyTestMethodFilter;
import ate.util.GenData0;

public class RunTestScriptCommand extends ATaskRunner {
	private static Logger log = LoggerFactory
			.getLogger(RunTestScriptCommand.class);

	public static void main(String[] args) throws Exception {
		System.out.println(Host.HOME);

		RunTestScriptCommand cmd = new RunTestScriptCommand();

		cmd.execute(args);
	}

	TTClassLoader gcl;

	GroovyScriptEngine gse = null;
	GroovyShell gsh = null;

	public RunTestScriptCommand() {
		log.debug("TTOOL_HOME=" + Host.HOME);
	}

	public TTTask execute(String[] paras) {
		this.task = new TTTask();
		this.paras = paras;
		try {
			task.setResultDesc(run_testsuite(this.paras));
			task.setExeResult(EXERESULT.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			task.setExeResult(EXERESULT.FAIL);
			task.setResultDesc(task.getResultDesc() + "\n"
					+ GenData0.getExceptionDesc(e));
		}
		return task;
	}

	public TTTask execute(TTTask task) {
		this.task = task;
		this.paras = task.getParas().split(" ");
		try {
			task.setResultDesc(run_testsuite(this.paras));
			task.setExeResult(EXERESULT.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			task.setExeResult(EXERESULT.FAIL);
			task.setResultDesc(task.getResultDesc() + "\n"
					+ GenData0.getExceptionDesc(e));
		}
		return task;
	}

	public String getDesc() {

		return "Run a or some Scripts";
	}

	public String getType() {
		return "runscript";
	}

	@Override
	public boolean onlyCanRunOnce() {
		return false;
	}

	public void run_tools(String[] args) {
		log.debug("rund: {}", args);
		
		ATECommandLineArgs cla = new ATECommandLineArgs();
		JCommander commander = new JCommander(cla, args);
		
		if (cla.help) {
			commander.usage();
			return;
		}
		Testbed tb=Testbed.createTestbed(cla.testbed);
		
		String filename = cla.suiteFiles.get(0);
		File fs = new File(filename);
		if (!fs.exists())
			fs = new File(Host.HOME + File.separator + filename);

		gcl = new TTClassLoader();

		String[] arr = new String[args.length - 1];
		System.arraycopy(args, 1, arr, 0, arr.length);
		
		gsh = new GroovyShell(gcl, new Binding());
		
		tb.device_registe(Global.GSHELL, gsh);
		
		try {
			gsh.run(fs, arr);
		} catch (CompilationFailedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 默认测试床为loopback.ini,可以通过-testbed参数修改
	 * 
	 * @param args
	 * @return
	 * @throws Exception
	 */
	public String run_testsuite(String[] args) throws Exception {
		log.debug("run_testsuite: {}", args);

		ATECommandLineArgs cla = new ATECommandLineArgs();
		JCommander commander = new JCommander(cla, args);

		if (cla.help) {
			commander.usage();
			return "Usage";
		}
		//log.debug("suitename={}",cla.suiteName);
		//log.debug("testName={}",cla.testName);
		
		TestNG testng = new TestNG();
		Testbed tb=Testbed.createTestbed(cla.testbed);
		
		if(cla.outputDirectory!=null)
			tb.add_arg("testng_output_dir", cla.outputDirectory);
		
		String errorInfo = "";

		// 如果下面任何一个参数不为bull，则认为是原始TestNG方式
		if (cla.testJar == null
				&& cla.testClass == null && cla.commandLineMethods.size() == 0
				&& cla.methodSelectors == null) {
			if (cla.suiteFiles.size() == 0) {
				log.debug("no script path");
				return "no script path";
			}

			File fs = new File(cla.suiteFiles.get(0));
			if (!fs.exists()) {
				log.debug("Script path not exists: {}", fs.getName());
				fs = new File(Host.HOME + File.separator + fs.getName());
				if (!fs.exists())
					return "script not exists";
			}
			// 借用了这个参数，但是不必传入TestNG
			cla.suiteFiles.clear();

			ArrayList<Class> cls = new ArrayList<Class>();

			gcl = new TTClassLoader();
			
			tb.device_registe(Global.GSHELL, new GroovyShell(gcl, new Binding()));

			ArrayList al = gcl.loadTestuite(fs, cla.testsuite);

			for (Object parsed : al) {
				if (parsed instanceof String) {
					errorInfo += (parsed + "\n");
				} else {
					cls.add((Class) parsed);
				}
			}
			if (cls.size() == 0) {
				log.debug("Script path have not any scripts: {}",
						fs.getAbsolutePath());
				return "指定的path上不存在脚本：" + fs.getAbsolutePath();
			}

			Class[] tmp = new Class[cls.size()];
			cls.toArray(tmp);

			testng.setTestClasses(tmp);

			if (cla.testmethod != null || cla.testcase >= 0) {
				MyTestMethodFilter mfilter = new MyTestMethodFilter();
				mfilter.setRunMethodRegExp(cla.testmethod);
				mfilter.setRunTestcase(cla.testcase);
				testng.setTestMethodFilter(mfilter);
			}
		} else {
			log.info("TestNG Native mode:");
		}

		SuiteRunner.set_prerun(cla.prerun);
		testng.addListener(new MyTestListenerAdapter(cla.stopOnFail));
		if (cla.excludedGroups == null)
			cla.excludedGroups = "";
		cla.excludedGroups += Host.is_win() ? "windows" : "linux";

		testng.configure(cla);
        Object o=testng.getReporters();
        o=testng.getTestListeners();
        
		testng.run();

		ate.testcase.Testcase.clear();
		if (task != null)
			this.task.setResultDesc(errorInfo);
		return "";
	}

	public static void validateCommandLineParameters(CommandLineArgs args) {
		List<String> testNgXml = args.suiteFiles;
		String testJar = args.testJar;
		String slave = args.slave;
		List<String> methods = args.commandLineMethods;

		String groups = args.groups;
		String excludedGroups = args.excludedGroups;

		if (args.slave != null && args.master != null) {
			throw new ParameterException(CommandLineArgs.SLAVE
					+ " can't be combined with " + CommandLineArgs.MASTER);
		}

		Boolean junit = args.junit;
		Boolean mixed = args.mixed;
		if (junit && mixed) {
			throw new ParameterException(CommandLineArgs.MIXED
					+ " can't be combined with " + CommandLineArgs.JUNIT);
		}
	}
}
