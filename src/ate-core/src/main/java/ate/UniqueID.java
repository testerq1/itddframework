package ate;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import io.netty.util.internal.PlatformDependent;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;


public class UniqueID implements Comparable<UniqueID> {
	private static final AtomicInteger nextId = new AtomicInteger();
	public final int id;
	
	public UniqueID(ConcurrentMap map){		
		id = nextId.incrementAndGet();
		if (map.putIfAbsent(id, Boolean.TRUE) != null) {
            throw new IllegalArgumentException(String.format("'%s' is already in use", id));
        }        
	}
	
	@Override
    public int compareTo(UniqueID other) {
		return ((Integer) id).compareTo(other.id);		
	}

}
