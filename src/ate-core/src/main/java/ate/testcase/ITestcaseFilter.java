package ate.testcase;

import java.util.HashMap;

public interface ITestcaseFilter {
	boolean validate(HashMap hm);
}
