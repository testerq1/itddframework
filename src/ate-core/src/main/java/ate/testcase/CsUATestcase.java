package ate.testcase;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ate.AbstractURIHandler;
import ate.ua.AbstractPacketUAImp;
import ate.ua.Reconnectable;
import ate.ua.mina.AMinaUAImp;
import ate.ua.netty.ANettyUAImp;

/**
 * 
 * @author 黄小勇
 */
public abstract class CsUATestcase extends UATestcase {
	/** server IP in firewall */
	protected String siip = arg("HOST.SVR_IN_IP");
	protected String ciip = arg("HOST.CLT_IN_IP");

	/** server IP out of firewall, default in loopback.ini */
	protected String sip = arg("HOST.SVR_IP");
	protected String cip = arg("HOST.CLT_IP");
	
	public CsUATestcase(){
		siip = arg("HOST.SVR_IN_IP");
		sip = arg("HOST.SVR_IP");
		if(siip==null){
			siip="127.0.0.1";
			sip =siip;
		}
	}
	
	public void clear_all_q() {
		for (AbstractURIHandler ua : all_ua)
			if (ua instanceof AbstractPacketUAImp)
				((AbstractPacketUAImp) ua).clear_q();
	}
	
	/**
	 * 
	 * @param uri remote address
	 * @return
	 */
	public <T extends AbstractURIHandler> T create_client(String uri_rmt) {
		return create_ua(uri_rmt).build(AbstractPacketUAImp.UA_TYPE, AbstractPacketUAImp.CLIENT);
	}
	/**
	 * 
	 * @param uri remote address
	 * @param ua
	 * @return
	 */
	public <T extends AbstractURIHandler> T create_client(String uri_rmt, T ua) {
		return create_ua(uri_rmt, ua).build(AbstractPacketUAImp.UA_TYPE, AbstractPacketUAImp.CLIENT);
	}

	public <T extends AbstractURIHandler> T create_client(String uri_local, String uri_rmt) {
		return create_ua(uri_rmt).build(AbstractPacketUAImp.URI2, uri_local).build(
				AbstractPacketUAImp.UA_TYPE, AbstractPacketUAImp.CLIENT);
	}
	public <T extends AbstractURIHandler> T create_client(String uri_local, String uri_rmt,T ua) {
        return create_ua(uri_rmt,ua).build(AbstractPacketUAImp.URI2, uri_local).build(
                AbstractPacketUAImp.UA_TYPE, AbstractPacketUAImp.CLIENT);
    }
	public <T extends AbstractURIHandler> T create_client(String uri_local, String uri_rmt, Class<T> cls) {
		return create_ua(uri_rmt, cls).build(AbstractPacketUAImp.URI2, uri_local).build(
				AbstractPacketUAImp.UA_TYPE, AbstractURIHandler.CLIENT);
	}
	public <T extends AbstractURIHandler> T create_client(String uri_rmt, Class<T> cls) {
		return create_ua(uri_rmt).build(AbstractPacketUAImp.UA_TYPE, AbstractURIHandler.CLIENT);
	}
	
	public <T extends AbstractURIHandler> T create_server(String uri) {
		return create_ua(uri).build(AbstractPacketUAImp.UA_TYPE, AbstractURIHandler.SERVER);
	}

	public <T extends AbstractURIHandler> T create_server(String uri, T ua) {
		return create_ua(uri, ua).build(AbstractPacketUAImp.UA_TYPE, AbstractURIHandler.SERVER);
	}

	public <T extends AbstractURIHandler> T create_server(String uri, Class<T> cls) {
		return create_ua(uri, cls).build(AbstractPacketUAImp.UA_TYPE, AbstractURIHandler.SERVER);
	}

	/**
	 * UA重连
	 */
	public void reconnect_all_ua() {
		log.debug("reconnect ua ");
		//stop ssl, 如果使用原始的minia2.0.9版本，需要这段代码
//		for (AbstractURIHandler ua : all_ua) {
//		    if((ua instanceof AMinaUAImp)&&((AMinaUAImp)ua).is_ssl_enabled()){
//		        ((AMinaUAImp)ua).stopssl();
//		    }
//		}
		
		for (AbstractURIHandler ua : all_ua) {
			if (ua instanceof AMinaUAImp) {
				((AMinaUAImp) ua).reconnect();
			} else if (ua instanceof ANettyUAImp) {
				((ANettyUAImp) ua).reconnect();
			}
		}
		
		wait_for_all_ready();
	}

	public void set_timeout(int milliSeconds) {
		for (AbstractURIHandler ua : all_ua)
			if (ua instanceof AbstractPacketUAImp)
				((AbstractPacketUAImp) ua).set_timeout(milliSeconds);
	}

	@Override
	public void shutdown_all_ua() {
	    log.debug("shutdown all ua:");
	    //stop all ssl，如果使用原始的minia2.0.9版本，需要这段代码
//	    for (AbstractURIHandler ua : all_ua) {
//            if((ua instanceof AMinaUAImp)&&((AMinaUAImp)ua).is_ssl_enabled()){
//                ((AMinaUAImp)ua).stopssl();
//            }
//        }
	    
		// shutdown all client
        for (AbstractURIHandler ua : all_ua) {
            if ((ua instanceof Reconnectable) && ((Reconnectable) ua).is_client()) {
                ua.teardown();
            } 
        }
        // shutdown all others        
		for (AbstractURIHandler ua : all_ua) {
			if ((ua instanceof Reconnectable) && ((Reconnectable) ua).is_client())
				;
			else
			    ua.teardown();
		}

		all_ua.clear();
		
	}
	@Override
	public void BeforeClass() {
		super.BeforeClass();
		log.debug("Server Inner IP: {}; Server Outer IP:{}", siip, sip);
	}

}
