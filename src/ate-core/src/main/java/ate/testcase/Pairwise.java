package ate.testcase;

import java.util.*;

import ate.Global;

public class Pairwise {
    /**
     * paras=[["a",["1","2","3"]],[["b",["a","b","c"]]]
     * @param paras
     * @param deep
     * @return [[name,[:]],[name,[:]]]
     */
    public static Object[][] go_map(String prefix,List paras, int deep,ITestcaseFilter filter) {
        ArrayList al=new ArrayList<ArrayList>();        
        for(int i=0;i<paras.size();i++){       
            List tmp=null;
            Object row=paras.get(i);
            
            Object col1=null;
            if(row instanceof Object[])
                col1=((Object[])row)[1];
            else
                col1=((List)row).get(1);
            
            if(col1 instanceof List)
                tmp=(List)col1;
            else{
                Object[] o=(Object[])col1;
                tmp=new ArrayList(Arrays.asList(o));
            }
            if(Global.random)
                Collections.shuffle(tmp);
            al.add(tmp);
        }
        
        ArrayList tc= go(al,deep);
        ArrayList<HashMap> td=new ArrayList<HashMap>();
        
    	for(int i=0;i<tc.size();i++){
    		ArrayList atc=(ArrayList)tc.get(i);
    		HashMap hm=new HashMap();
    		for(int j=0;j<atc.size();j++){     
                Object row=paras.get(j);
                if(row instanceof Object[])
                    hm.put(((Object[])row)[0], atc.get(j));
                else
                    hm.put(((List)row).get(0),atc.get(j));
            }
    		if(filter==null||filter.validate(hm))
    			td.add(hm);    		
    	}        
        
        Object[][] tmp=new Object[td.size()][2];
        for(int i=0;i<tmp.length;i++){
            tmp[i][0] = prefix+"_R"+i;            
            tmp[i][1]=td.get(i);                                    
        }
        return tmp;
    }
    
    public static ArrayList go(String[][] paras, int deep) {
        ArrayList al=new ArrayList<ArrayList>();        
        for(int i=0;i<paras.length;i++){
            al.add(new ArrayList(Arrays.asList(paras[i])));
        }
        return go(al,deep);
    }
    
    public static ArrayList go(List<String> vl, int deep) {
        int m = vl.size() % deep;
        int p = vl.size() / deep;
        ArrayList al = null;
        for (int i = 0; i < p; i++) {
            ArrayList temp = new ArrayList();
            for (int j = 0; j < deep; j++) {
                temp.add(vl.get(j + i * deep));
            }
            if (al == null)
                al = myPair(temp);
            else
                al = appendList(al, myPair(temp));
        }

        ArrayList rmain = new ArrayList();
        for (int i = p * deep; i < vl.size(); i++) {
            rmain.add(vl.get(i));
        }

        return appendList(al, myPair(rmain));
    }

    private static ArrayList myPair(ArrayList al) {
        if (al.size() == 0) {
            return null;
        }

        if (al.size() == 1) {
            if (al.get(0) instanceof ArrayList)
                return (ArrayList) al.get(0);
            return al;
        }
        ArrayList arl = new ArrayList();
        int m = al.size() % 2;
        for (int i = 0; i < al.size() - m; i += 2) {
            arl.add(orthog((ArrayList) al.get(i), (ArrayList) al.get(i + 1)));
        }
        if (m == 1)
            arl.add(appendList((ArrayList) al.get(al.size() - 1), null));

        return myPair(arl);
    }

    private static ArrayList appendList(ArrayList v1, ArrayList v2) {
        ArrayList arr = new ArrayList();
        ArrayList max, min;
        if (v2 != null && v2.size() > v1.size()) {
            max = v2;
            min = v1;
        } else {
            max = v1;
            min = v2;
        }
        for (int i = 0; i < max.size(); i++) {
            ArrayList temp;
            if (max.get(i) instanceof ArrayList) {
                temp = (ArrayList) max.get(i);
            } else {
                temp = new ArrayList();
                temp.add(max.get(i));
            }
            if (min != null && min.size() > 0) {
                if (min.get(i % min.size()) instanceof ArrayList) {
                    if (v2.size() > v1.size())
                        temp.addAll(0, (ArrayList) min.get(i % min.size()));
                    else
                        temp.addAll((ArrayList) min.get(i % min.size()));
                } else {
                    if (v2.size() > v1.size())
                        temp.add(0, min.get(i % min.size()));
                    else
                        temp.add(min.get(i % min.size()));
                }
            }
            arr.add(temp);
        }
        return arr;

    }

    private static ArrayList orthog(ArrayList v1, ArrayList v2) {
        ArrayList arr = new ArrayList();
        for (int i = 0; i < v1.size(); i++) {
            for (int j = 0; j < v2.size(); j++) {
                ArrayList temp = new ArrayList();
                if (v1.get(i) instanceof ArrayList)
                    temp.addAll((ArrayList) v1.get(i));
                else
                    temp.add(v1.get(i));

                if (v2.get(j) instanceof ArrayList)
                    temp.addAll((ArrayList) v2.get(j));
                else
                    temp.add(v2.get(j));

                arr.add(temp);
            }
        }
        return arr;
    }

    class PairVaraible implements Comparable<PairVaraible> {
        public String name;
        public ArrayList values;
        public int preference;

        public int compareTo(PairVaraible other) {
            if (values == null || other.values == null)
                return 0;

            return other.values.size() - this.values.size();
        }
    }
}
