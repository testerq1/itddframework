package ate.testcase;

public class IPO {

	public final int M = 30; // 参数个数
	public final int N = 50; // 每个参数取值的最大个数
	public final int MAX = 5000; // 最终测试用例条数
	public final int MIN = N * N; // 临时测试用例集的条数

	int Para; // 待测参数数
	int ParaA[] = new int[M]; // 存放各参数取值个数的数组
	int T[][] = new int[MAX][M]; // 生成的测试集T
	int Tplus[][] = new int[MIN][M]; // 临时测试集T'
	int tag; // 记录T的大小,最后的返回
	int tag1; // 记录T'的大小
	int cnt[] = new int[50]; // 存放当前参数的各取值匹配多少
	boolean PairA[][][] = new boolean[N][M][N]; // 存放当前参数与其前面参数的配对
	int Use[][] = new int[M][N]; // 存放各参数各取值的选择次数

	// 从外部得到配置信息
	// 参数p是参数个数，数组para存放各参数的取值个数
	void Get(int p, int para[]) {
		Para = p;
		for (int i = 0; i < Para; i++) {
			ParaA[i] = para[i];
		}
	}

	// 重置配对数组
	void ResetPA() {
		int j, k, l;
		for (j = 0; j < N; j++) {
			for (k = 0; k < M; k++) {
				for (l = 0; l < N; l++) {
					PairA[j][k][l] = false;
				}
			}
		}
	}

	// 初始化当前参数和前面参数的匹配情况，参数I是当前参数的序号
	// PairA[j][k][l]中j是当前参数的第j个取值，k是第k个参数，l是第k个参数的第l个取值
	void InitPA(int I) {
		int j, k, l;
		for (j = 0; j < ParaA[I]; j++) {
			for (k = 0; k < I; k++) {
				for (l = 0; l < ParaA[k]; l++) {
					PairA[j][k][l] = true;
				}
			}
		}
	}

	// 处理的初始化工作
	void InitAll() {
		int i, j;
		tag = 0;
		tag1 = 0;
		// 重置Use
		for (i = 0; i < M; i++) {
			for (j = 0; j < N; j++) {
				Use[i][j] = 0;
			}
		}
		// 处理好前两个参数
		for (i = 0; i < ParaA[0]; i++) {
			for (j = 0; j < ParaA[1]; j++) {
				T[tag][0] = i;
				Use[0][i]++;
				T[tag][1] = j;
				Use[1][j]++;
				tag++;
			}
		}
	}

	// 删除掉T中一条用例中包含前面参数与新加入参数之间的配对
	// 参数i表示当前参数序号是i，参数j表示T中的第j条用例
	void DeleT(int i, int j) {
		int k;
		for (k = 0; k < i; k++) {
			if (T[j][k] != -1)
				PairA[T[j][i]][k][T[j][k]] = false;
		}
	}

	// 选择能产生最多配对的当前参数的取值
	// 参数i是当前参数的序号为i，j表示T中第j条用例
	void Choose(int i, int j) {
		int k, l, max;
		for (k = 0; k < ParaA[i]; k++)
			cnt[k] = 0;
		for (k = 0; k < i; k++) {
			for (l = 0; l < ParaA[i]; l++) {
				if (PairA[l][k][T[j][k]])
					cnt[l]++;
			}
		}
		for (k = 1, max = 0; k < ParaA[i]; k++) {
			if (cnt[k] > cnt[max])
				max = k;
		}
		T[j][i] = max;
		Use[i][max]++;
	}

	// 重置T'
	void ResetTplus() {
		int i, j;
		for (i = 0; i < MIN; i++) {
			for (j = 0; j < M; j++) {
				Tplus[i][j] = -1;
			}
		}
		tag1 = 0;
	}

	// 将一个配对试着插入到T'中
	// 参数a,b,c,d代表第a个参数的第b个取值与第c个参数的第d个取值的配对
	void InsertTplus(int a, int b, int c, int d) {
		int i;
		// 如果能和当前的临时用例匹配，则插入
		for (i = 0; i < tag1; i++) {
			if ((Tplus[i][c] == -1) && (Tplus[i][a] == b)) {
				Tplus[i][c] = d;
				return;
			}
		}
		// 否则开辟新的用例
		Tplus[i][c] = d;
		Tplus[i][a] = b;
		tag1++;
	}

	// 选择指定参数中出现最少的一个取值
	// 参数i代表指定参数序号
	int SelectMin(int i) {
		int j, min;
		for (j = 1, min = 0; j < ParaA[i]; j++) {
			if (Use[i][j] < Use[i][min]) {
				min = j;
			}
		}
		return min;
	}

	// 将T'中的最终用例追加到T中去
	// 参数I表示用例长度到I
	void Copy(int I) {
		int i, j;
		for (i = 0; i < tag1; i++) {
			for (j = 0; j < M; j++) {
				T[tag][j] = Tplus[i][j];
			}
			tag++;
		}
	}

	// 处理
	void Deal() {
		int i, j, k, l;
		for (i = 2; i < Para; i++) {
			// 处理T
			ResetPA();
			InitPA(i);
			if (tag <= ParaA[i])// 当用例数小于参数取值数
			{
				for (j = 0; j < tag; j++) {
					T[j][i] = j;
					Use[i][j]++;
					DeleT(i, j);
				}
			} else// 当用例数大于参数取值数
			{
				for (j = 0; j < ParaA[i]; j++) {
					T[j][i] = j;
					Use[i][j]++;
					DeleT(i, j);
				}
				for (; j < tag; j++) {

					Choose(i, j);
					DeleT(i, j);
				}
			}
			// 处理T'
			ResetTplus();
			for (j = 0; j < ParaA[i]; j++) {
				for (k = 0; k < i; k++) {
					for (l = 0; l < ParaA[k]; l++) {
						if (PairA[j][k][l]) {
							InsertTplus(i, j, k, l);
							PairA[j][k][l] = false;
						}
					}
				}
			}
			// 对T'中临时用例更新Use
			for (j = 0; j < tag1; j++) {
				for (k = 0; k <= i; k++) {
					if (Tplus[j][k] != -1) {
						Use[k][Tplus[j][k]]++;
					}
				}
			}
			// 对T'中的空位填补
			for (j = 0; j < tag1; j++) {
				for (k = 0; k < i; k++) {
					if (Tplus[j][k] == -1) {
						Tplus[j][k] = SelectMin(k);
						Use[k][Tplus[j][k]]++;
					}
				}
			}
			Copy(i);
		}
	}
	
	public String[][] pair(String[][] paras){
	    int[] ps=new int[paras.length];
	    for(int i=0;i<paras.length;i++){
	        ps[i]=paras[i].length;
	    }
	    Get(ps.length,ps);
	    InitAll();
        Deal();
	    String[][] ts=new String[tag][paras.length];
	    for(int i=0;i<tag;i++){
            for(int j=0;j<Para;j++){
                ts[i][j]=paras[j][T[i][j]];
            }            
        }
	    return ts;
	}
	
	public static void main(String[] args){
		IPO ipo=new IPO();
		
		ipo.Get(6, new int[]{4,7,3,5,6,3});
		ipo.InitAll();
		ipo.Deal();
		System.out.println("Total:"+ipo.tag);
		for(int i=0;i<ipo.tag;i++){
			for(int j=0;j<ipo.Para;j++){
				System.out.print(ipo.T[i][j]);
			}
			System.out.print("\n");
		}
	}

}

