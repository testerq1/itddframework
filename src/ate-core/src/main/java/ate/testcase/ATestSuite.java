package ate.testcase;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.rm.dev.Host;
import ate.rm.dev.Testbed;
import ate.ua.AbstractCrudImp;

public class ATestSuite{
	protected static Logger log = LoggerFactory.getLogger(ATestSuite.class);
	protected Testbed tb=Testbed.getTestbed();
	protected static Host localhost;
	
	/**
	 * 应该在注册Host后调用
	 */
	public void BeforeSuite() {		
		log.debug("BeforeSuite: home = {}", Host.HOME);		
	}	
	
	/**
	 * 重新初始化host静态变量
	 */
	public static void reinit_static(){
		localhost=Host.localhost();
	}
	
	public String arg(String key){
		if(System.getProperty(key)!=null)
			return System.getProperty(key);
		return tb.arg(key);
	}
	
	public void add_arg(String key,String value){
		tb.add_arg(key, value);
	}
	
	public void AfterSuite() {
		log.debug("AfterSuite {}", this);

	}

}
