package ate.testcase;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.Global;
import ate.rm.dev.Host;
import ate.rm.dev.Testbed;
import ate.rm.file.FileUA;
import ate.util.AbstractTool;
import ate.util.Expect;
import ate.util.Expect.MATCH_PATTERN;
import ate.util.GenData0;

/**
 * The Class Testcase.
 */
public abstract class Testcase {
	/** 当前方法名 */
	private static String currMethod = "";
	protected static String HOME = Host.HOME;

	/** The log. */
	protected static Logger log = LoggerFactory.getLogger(Testcase.class);

	/** 方法调用次数 */
	private static int methodCnt = 0;

	/** The mthd cnt. */
	private static int mthdCnt = 0;

	/** The STEP. */
	protected static String STEP;

	/** The test cnt. */
	private static int testCnt = 0;

	protected static Host LHOST = Host.localhost();

	/**
	 * Clear Step info
	 */
	public static void clear() {
		testCnt = 0;
		STEP = "";
		currMethod = "";
	}

	public static void reinit_static() {
		LHOST = Host.localhost();
	}

	/**
	 * 交互式模式下，waitYes()会等待console输入一个y,然后才能推出.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public static boolean wait_yes() {		
		return Global.wait_yes();
	}

	protected ExecutorService executor;

	/** Expect handler */
	protected Expect expect = new Expect();

	int stepcount = 1;

	private final Testbed tb;

	/**
	 * 脚本文件的测试床是在RunTestScriptCommand#run_testsuite中初始化的
	 * 如果没有用-testbed指定指定，则采用默认值loopback.ini 如果是单元测试，此时的测试床应该还没有初始化过
	 */
	public Testcase() {
		tb = Testbed.createTestbed();
	}

	/**
	 * After class.
	 */
	public void AfterClass() {
		mthdCnt++;
		log.info(testCnt + "." + mthdCnt + ". AfterClass("
				+ getClass().getName() + ")");
	}

	/**
	 * After method.
	 * 
	 * @param m
	 *            the m
	 */
	public void AfterMethod(Method m) {
		log.info(testCnt + "." + mthdCnt + "." + (++methodCnt)
				+ ". AfterMethod(" + currMethod + ")");
	}

	/**
	 * Gets the property from ini.
	 * 
	 * @param key
	 *            the key
	 * @return the property from ini
	 */
	public String arg(String key) {
		if (System.getProperty(key) != null)
			return System.getProperty(key);
		return tb.arg(key);
	}

	public void assertFalse(boolean condition) {
		assertTrue(!condition);
	}

	public void assertFalse(boolean bool, String message, Object... paras) {
		assertTrue(!bool, message, paras);
	}

	/**
	 * 
	 * @param message
	 * @param condition
	 * @deprecated please use assertFalse(boolean bool, String message,
	 *             Object... paras)
	 */
	public void assertFalse(String message, boolean condition) {
		assertTrue(!condition, message);
	}

	public void assertTrue(boolean bool) {
		assertTrue(bool, "");
	}

	/**
	 * 为方便其间，封装一下assertTrue方法，以减少异常打印堆栈 The new error message is built using
	 * {@link String#format(String, Object...)} if you provide args parameter
	 * (if you don't, the error message is taken as it is).
	 * 
	 * @param message
	 * @param bool
	 */
	public void assertTrue(boolean bool, Object message, Object... paras) {
		try {
			expect.assertThat(bool).overridingErrorMessage("" + message, paras)
					.isTrue();
			// org.testng.AssertJUnit.assertTrue(message, bool);
		} catch (AssertionError tr) {
			// debug模式下，assert的异常会打印完整栈，否则只会打印脚本和框架代码行
			if (Global.verbose)
				throw tr;
			StackTraceElement[] st = tr.getStackTrace();
			List<StackTraceElement> tmp = new ArrayList<StackTraceElement>();
			for (StackTraceElement element : st) {
				String cname = element.getClassName();
				String fname = element.getFileName();
				if ((cname != null && cname.startsWith("ate") && !cname
						.equals(Expect.class.getName()))
						|| (fname != null && fname.endsWith(".groovy")))
					tmp.add(element);
			}
			Error error = new Error(tr.getLocalizedMessage());
			StackTraceElement[] ns = new StackTraceElement[tmp.size()];
			tmp.toArray(ns);
			error.setStackTrace(ns);
			throw error;
		}
	}

	/**
	 * 
	 * @param message
	 * @param bool
	 * @deprecated please use assertTrue(boolean bool, String message, Object...
	 *             paras)
	 */
	public void assertTrue(String message, boolean bool) {
		assertTrue(bool, message);
	}

	/**
	 * Before class.初始化一个日志信息
	 * testCnt+"."+mthdCnt+". BeforeClass("+getClass().getName()+")"
	 */
	public void BeforeClass() {
		testCnt++;
		log.info(testCnt + ". " + getClass().getName());
		mthdCnt = 0;
		log.info(testCnt + "." + mthdCnt + ". BeforeClass("
				+ getClass().getName() + ")");
		STEP="";
	}

	/**
	 * Before method.
	 * 该方法会重置currMethod，如果子类要比较method，需要在调用super.BeforeMethod(m)前进行比较
	 * m.getName().equals(currMethod)
	 * 
	 * @param m
	 *            the m
	 */
	public void BeforeMethod(Method m) {
		/*
		 * if(m.getName().equals(currMethod)){ invocationCnt++; }else{
		 * invocationCnt=1; }
		 */
		this.stepcount = 1;
		methodCnt = 1;
		currMethod = m.getName();
		mthdCnt++;
		log.info(testCnt + "." + mthdCnt + ". " + currMethod);
		log.info(testCnt + "." + mthdCnt + ".0. BeforeMethod(" + currMethod
				+ ")");

		STEP = testCnt + "." + mthdCnt + "." + methodCnt;
	}

	/**
	 * 使用反射技术创建一个实例
	 * 
	 * @param paras
	 *            构造函数的参数
	 * @param cls
	 *            类
	 * @return 类实例
	 */

	protected Object create_instance(Object[] paras, Class cls) {
		return AbstractTool.create_instance(paras, cls);
	}

	/**
	 * 使用反射技术创建一个实例
	 * 
	 * @param para
	 *            构造函数的参数
	 * @param cls
	 *            类
	 * @return 类实例
	 */
	protected Object create_instance_with_string(String para, Class cls) {
		return AbstractTool.create_instance_with_string(para, cls);
	}

	/**
	 * Debug var.
	 * 
	 * @param o
	 *            the o
	 */
	public void debug_variable(Object o) {
		if (o == null || o instanceof String)
			System.out.println(o);
		else if (o instanceof ArrayList)
			for (Object tmp : (ArrayList) o)
				debug_variable(tmp);
		else if (o instanceof Map)
			for (Object tmp : ((Map) o).keySet()) {
				System.out.println("print " + tmp);
				debug_variable(((Map) o).get(tmp));
			}
	}

	public boolean executor_await_termination(long seconds) {
		if (executor == null) {
			log.error("executor is null");
			return false;
		}
		try {
			executor.shutdown();
			return executor.awaitTermination(1000, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return false;
	}

	public ExecutorService executor_create(int size) {

		executor = Executors.newScheduledThreadPool(size);

		return executor;
	}

	public boolean expect(Object cmd, String rtn, boolean exp,
			MATCH_PATTERN type) {
		return expect.match(cmd.toString(), rtn, exp, type);
	}

	/**
	 * 本方法和DA测试用例结合，用于自动生成Pairwise的测试用例
	 * 如果Global.random为 true，会在生成时做shuffle
	 * 默认deep为Global.random_deep
	 * @param paras 参数，形如：List<Object[]> paras = new ArrayList() {
            {
                add(new Object[] { "a", new String[] { "1", "2", "3" } });
                add(new Object[] { "b", new String[] { "a", "b", "c", "d" } });
                add(new Object[] { "c", new String[] { "A", "B", "C", "D" } });
                add(new Object[] { "d", new String[] { "x", "y" } });
            }
        };
	 * @return
	 */
	public Object[][] gen_testcases(List<Object[]> paras){	    
	    String name=this.getClass().getSimpleName();
	    name=name.substring(0,name.lastIndexOf("_"));
	    return Pairwise.go_map(name, paras, Math.min(Global.random_deep,paras.size()),null);
	}
	/**
	 * 带一个过滤器
	 * @param paras
	 * @param filter
	 * @return
	 */
	public Object[][] gen_testcases(List<Object[]> paras,ITestcaseFilter filter){	    
	    String name=this.getClass().getSimpleName();
	    name=name.substring(0,name.lastIndexOf("_"));
	    return Pairwise.go_map(name, paras, Math.min(Global.random_deep,paras.size()),filter);
	}
	
	/**
	 * 生成指定数目的测试用例
	 * @param paras
	 * @param num 返回用例数量
	 * @return
	 * @see #gen_testcases(List)
	 */
	public Object[][] gen_testcases(List<Object[]> paras,int num){
	    Object[][] tmp=gen_testcases(paras);
	    
	    return Arrays.copyOfRange(tmp, 0, Math.min(num,tmp.length));
	}
	/**
	 * 生成指定数目的经过过滤的测试用例
	 * @param paras
	 * @param num
	 * @param filter
	 * @return
	 */
	public Object[][] gen_testcases(List<Object[]> paras,int num,ITestcaseFilter filter){
		Object[][] tmp=gen_testcases(paras,filter);
		 return Arrays.copyOfRange(tmp, 0, Math.min(num,tmp.length));
	}
	/**
	 * 打印测试用例，测试用例格式为[name,[:]]
	 * @param rst
	 */
	public String to_string(Object[][] rst){
	    StringBuffer sb=new StringBuffer();
	    for (int i = 0; i < rst.length; i++) {
	        sb.append(rst[i][0] + " = ");
            HashMap hm = (HashMap) rst[i][1];

            for (Object o : hm.keySet()) {
                sb.append(o + ":" + hm.get(o) + "\t");
            }
            sb.append("\n");
        }
	    return sb.toString();
	}
	/**
	 * <p>
	 * 获得Testbed中保存的device,用来传递全局对象
	 * 
	 * @param name
	 *            the name
	 * @return the device
	 * @see Testbed#getRes(String)
	 */
	public Object get_device(String name) {
		Object dev = Testbed.getTestbed().getRes(name);
		if (dev == null)
			log.error("dev is null!");
		return dev;
	}


	/**
	 * 从file中得到map结构的数据
	 * 
	 * @param file
	 * @return
	 * @see FileUA#get_properties(String)
	 */
	public Properties get_map(String file) {
		return FileUA.get_properties(file);
	}

	/**
	 * 获得Testbed中保存的suite信息
	 * 
	 * @param suite
	 * @return
	 * @see Testbed#getRes(String)
	 */
	public Object get_testsuite(String suite) {
		return Testbed.getTestbed().getRes(suite);
	}
	
	/**
	 * 当前线程进入join
	 * @param millisecs
	 */
	public Thread mainThread;
	public void join(long millisecs){
	    try {
	        mainThread=Thread.currentThread();
	        mainThread.join(millisecs);
        } catch (InterruptedException e) {
            log.debug("mainThread interruptd");
        }	    
	}
	public void join(){
	    join(0);
	}
	/**
	 * 终端当前mainThread
	 */
	public void interrupt(){
	    if(mainThread==null)
	        mainThread=Thread.currentThread();
	    mainThread.interrupt();
	}
	/**
	 * @see Math#max(int, int)
	 * @param i1
	 * @param i2
	 * @return
	 */
	public int max(int i1, int i2) {
		return Math.max(i1, i2);
	}

	/**
	 * @see Math#min(int, int)
	 * @param i1
	 * @param i2
	 * @return
	 */
	public int min(int i1, int i2) {
		return Math.min(i1, i2);
	}

	/**
	 * @see System#currentTimeMillis()
	 * @return
	 */
	public long now() {
	    return System.currentTimeMillis();
	}

	/**
	 * @see GenData0#random(int, int)
	 * @param from
	 * @param to
	 * @return
	 */
	public int random(int from, int to) {
		return GenData0.random(from, to);
	}

	public long random(long from, long to) {
		return GenData0.random(from, to);
	}

	/**
	 * 用于产生随机测试数据，Usage: <br>
	 * 
	 * <PRE>
	 * Change:
	 *  @DataProvider(name="Data_TestStep")
	 * public Object[][] Data_TestStep(){
	 *  	return SAMPLES;
	 * 	}
	 * 
	 * To： 
	 * @DataProvider(name="Data_TestStep")
	 * public Object[][] Data_TestStep(){
	 *   	return random_samples(new Filter(),SAMPLES);
	 * 	}
	 * </PRE>
	 * 
	 * @param filter
	 * @param samples
	 * @return 随机化的抽样组合
	 * @see Filter#validate(java.util.HashMap)
	 */
	public List random_samples(Filter filter, List samples) {
		return filter.random(samples);
	}

	public List random_samples(Filter filter, Object[][] samples) {
		return filter.random(samples);
	}

	/**
	 * 按照固定的时间间隔循环执行一个task，用于执行时间可知的任务
	 * 
	 * @param task
	 * @param initialDelay
	 *            起始延迟
	 * @param period
	 *            循环周期
	 * @return
	 */
	public ScheduledFuture schedule_at_fixed_rate(Runnable task,
			long initialDelay, long period) {
		ScheduledExecutorService scheduler = (ScheduledExecutorService) executor;
		return scheduler.scheduleAtFixedRate(task, initialDelay, period,
				TimeUnit.SECONDS);
	}

	/**
	 * 循环执行task，上一次执行完后，等待delay秒后再执行，用于执行时间不可知的任务
	 * 
	 * @param task
	 * @param initialDelay
	 * @param delay
	 * @return
	 */
	public ScheduledFuture schedule_with_fixed_delay(Runnable task,
			long initialDelay, long delay) {
		ScheduledExecutorService scheduler = (ScheduledExecutorService) executor;
		return scheduler.scheduleWithFixedDelay(task, initialDelay, delay,
				TimeUnit.SECONDS);
	}

	/**
	 * @see Collections#shuffle(List)
	 * @param ar
	 */
	public void shuffle(ArrayList ar) {
		Collections.shuffle(ar);
	}

	/**
	 * Sleep
	 * 
	 * @see Thread#sleep(milliseconds);
	 * @param milliseconds
	 */
	public void sleep(long milliseconds) {
		Global.sleep(milliseconds);
	}
	
	public void STEP(Object msg) {
		if (stepcount <= 1)
			log.info(testCnt + "." + mthdCnt + ".1. " + currMethod);
		log.info(STEP + "." + stepcount + " " + msg);
		stepcount++;
	}

	/**
	 * 等待console输入yes后再执行，用于调试
	 * 
	 * @param info
	 * @return
	 */
	public static boolean wait_yes(String info) {
		log.debug(info);
		return wait_yes();
	}

}
