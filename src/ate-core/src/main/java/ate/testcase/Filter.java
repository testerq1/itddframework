package ate.testcase;

import java.util.*;

/**
 * 产生随机测试用例的数据，应该实现validate方法，用来校验测试数据组合的合法性<br>
 * 目前的
 * 
 * @author ravi huang
 * @see #validate(HashMap)
 * 
 */
public class Filter {
	/* 生成的用例个数=min( 最大正交组合长度, 初始长度*(1+STRENGTH)) */
	public static float STRENGTH = 0;

	// 是否需要原始测试数据组合？
	public static boolean NEED_ORIG = false;

	// 测试场面名称
	String prefix;
	// 适配样本点到算法
	List<PairVaraible> pvlist;
	// 样本点的名字
	List names;

	/**
	 * 对生成的组合进行过滤
	 * 
	 * @param map
	 * @return
	 */
	public boolean validate(HashMap<String, Object> map) {
		return true;
	}

	/**
	 * 根据原始数据，获得样本点组合，经过shuffle后,生成一个随机测试数据组合<br>
	 * 可以选择是否保留当前样本点组合
	 * @see #NEED_ORIG
	 * 
	 * @param list 原始数据
	 * @param strength 随机用例数因子
	 * @return
	 */
	public List random(List list, float strength) {
		if (strength == 0)
			return list;

		ArrayList al = new ArrayList();
		for (Object v : list) {
			List tmp = null;
			if (v instanceof List)
				tmp = (List) v;
			else if (v instanceof Object[])
				tmp = Arrays.asList((Object[]) v);

			// 取得测试场景名称
			if (prefix == null) {
				this.prefix = tmp.get(0).toString();
				prefix = prefix.substring(0, prefix.lastIndexOf("_"));
				prefix = prefix.substring(0, prefix.lastIndexOf("_"));
			}

			HashMap map = (HashMap) tmp.get(1);
			if (pvlist == null) {
				pvlist = new ArrayList<PairVaraible>(map.keySet().size());
				names = new ArrayList<PairVaraible>(map.keySet().size());
				// 初始化names,pvlist
				for (Object o : map.keySet()) {
					PairVaraible pv = new PairVaraible();
					pv.name = o;
					names.add(o);
					pvlist.add(pv);
				}
			}
			// values是个有序的Set
			for (int i = 0; i < names.size(); i++) {
				pvlist.get(i).values.add(map.get(names.get(i)));
			}
		}

		// 计算最大长度和当前组合深度
		int max = 1;
		int deep = 0;
		for (PairVaraible tmp : pvlist) {
			max *= tmp.values.size();
			tmp.varaibles.addAll(tmp.values);
			Collections.shuffle(tmp.varaibles);
			if (list.size() < max)
				deep++;
		}
		// 计算需要生成的随机组合最大长度
		if (max <= list.size())
			return list;
		else
			max = (int) Math.min(max, Math.floor(list.size() * (1 + strength)));

		// 填充当前测试数据
		if (NEED_ORIG)
			al.addAll(list);

		return samples(al,deep+1,max);
	}
	public List random(Object[][] list) {
		return random(Arrays.asList(list), STRENGTH);
	}

	public List random(List list) {
		return random(list, STRENGTH);
	}
	
	private List samples(ArrayList al, int deep, int len) {
		// 深度+1以后，生成新的样本点组合
		List ll = go(pvlist, deep);
		Collections.shuffle(ll);

		for (int i = 0; i < ll.size(); i++) {
			// 生成样本点组合映射表
			HashMap hm = new HashMap();
			for (int j = 0; j < names.size(); j++)
				hm.put(names.get(j), ((List) ll.get(i)).get(j));

			// 如果组合有效，则加入集合
			if (this.validate(hm)) {
				Object[] oo = new Object[2];
				oo[0] = this.prefix + "_R" + (i + 1);
				oo[1] = hm;
				al.add(oo);
			}

			if (al.size() >= len)
				break;
		}
		if (al.size() >= len || deep==names.size()){
			al.trimToSize();
			return al;
		}else 
			return samples(al,deep+1,len);
	}

	public ArrayList go(List<PairVaraible> vars, int deep) {
		ArrayList vl = new ArrayList();
		for (int i = 0; i < vars.size(); i++) {
			vl.add(vars.get(i).varaibles);
		}

		int m = vl.size() % deep;
		int p = vl.size() / deep;
		ArrayList al = null;
		for (int i = 0; i < p; i++) {
			ArrayList temp = new ArrayList();
			for (int j = 0; j < deep; j++) {
				temp.add(vl.get(j + i * deep));
			}
			if (al == null)
				al = myPair(temp);
			else
				al = appendList(al, myPair(temp));
		}

		ArrayList rmain = new ArrayList();
		for (int i = p * deep; i < vl.size(); i++) {
			rmain.add(vl.get(i));
		}

		return appendList(al, myPair(rmain));
	}

	private ArrayList myPair(ArrayList al) {
		if (al.size() == 0) {
			return null;
		}

		if (al.size() == 1) {
			if (al.get(0) instanceof ArrayList)
				return (ArrayList) al.get(0);
			return al;
		}
		ArrayList arl = new ArrayList();
		int m = al.size() % 2;
		for (int i = 0; i < al.size() - m; i += 2) {
			arl.add(orthog((ArrayList) al.get(i), (ArrayList) al.get(i + 1)));
		}
		if (m == 1)
			arl.add(appendList((ArrayList) al.get(al.size() - 1), null));

		return myPair(arl);
	}

	private ArrayList appendList(ArrayList v1, ArrayList v2) {
		ArrayList arr = new ArrayList();
		ArrayList max, min;
		if (v2 != null && v2.size() > v1.size()) {
			max = v2;
			min = v1;
		} else {
			max = v1;
			min = v2;
		}
		for (int i = 0; i < max.size(); i++) {
			ArrayList temp;
			if (max.get(i) instanceof ArrayList) {
				temp = (ArrayList) max.get(i);
			} else {
				temp = new ArrayList();
				temp.add(max.get(i));
			}
			if (min != null && min.size() > 0) {
				if (min.get(i % min.size()) instanceof ArrayList) {
					if (v2.size() > v1.size())
						temp.addAll(0, (ArrayList) min.get(i % min.size()));
					else
						temp.addAll((ArrayList) min.get(i % min.size()));
				} else {
					if (v2.size() > v1.size())
						temp.add(0, min.get(i % min.size()));
					else
						temp.add(min.get(i % min.size()));
				}
			}
			arr.add(temp);
		}
		return arr;

	}

	private ArrayList orthog(ArrayList v1, ArrayList v2) {
		ArrayList arr = new ArrayList();
		for (int i = 0; i < v1.size(); i++) {
			for (int j = 0; j < v2.size(); j++) {
				ArrayList temp = new ArrayList();
				if (v1.get(i) instanceof ArrayList)
					temp.addAll((ArrayList) v1.get(i));
				else
					temp.add(v1.get(i));

				if (v2.get(j) instanceof ArrayList)
					temp.addAll((ArrayList) v2.get(j));
				else
					temp.add(v2.get(j));

				arr.add(temp);
			}
		}
		return arr;
	}

}

class PairVaraible implements Comparable {
	Object name;
	LinkedHashSet values = new LinkedHashSet();
	List varaibles = new ArrayList();
	int preference;

	@Override
	public int compareTo(Object o) {
		PairVaraible other = (PairVaraible) o;
		if (values == null || other.values == null)
			return -1;

		return this.values.size() - other.values.size();
	}
}
