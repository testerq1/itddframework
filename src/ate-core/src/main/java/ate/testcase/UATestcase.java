package ate.testcase;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;

import ate.AbstractURIHandler;
import ate.ua.AbstractPacketUAImp;
import ate.ua.ResourceFactory;
import ate.ua.UAOption;

public class UATestcase extends Testcase {
	protected ArrayList<AbstractURIHandler> all_ua = new ArrayList<AbstractURIHandler>();
	
	public <T extends AbstractURIHandler> T create_ua(String connStr) {
		T ss = ResourceFactory.createUA(connStr);
		if (ss != null)
			all_ua.add(ss);
		return ss;
	}
	public void init_jre_log(String file){		
		/*SEVERE（最高值）
		WARNING
		INFO
		CONFIG
		FINE
		FINER
		FINEST（最低值）*/
		java.util.logging.LogManager logManager = java.util.logging.LogManager
				.getLogManager();
		InputStream inputStream = null;
		try {
			// 重新初始化日志属性并重新读取日志配置。
			File logconf = new File(file);
			if(logconf.exists()){
				inputStream = new FileInputStream(logconf);
				logManager.readConfiguration(inputStream);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != inputStream)
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

	}
	public <T extends AbstractURIHandler> T create_ua(String uri, T ua) {
		all_ua.add(ua);
		return ua.build(AbstractPacketUAImp.UA_URI,uri);
	}
	
	public <T extends AbstractURIHandler> T create_ua(String connStr,Class<T> cls) {
		T ss = ResourceFactory.createUA(connStr,cls);
		if (ss != null)
			all_ua.add(ss);
		return ss;
	}
	/**
	 * 做多等待30秒
	 */
	public void start_all_ua() {
		if(all_ua.isEmpty()){
			log.warn("no ua registered!");
			return;
		}
		for (AbstractURIHandler ua : all_ua){
			ua.startup();
			int cnt=600;			
			while (!ua.is_ready()&&(cnt--)>0)
				sleep(50);
			
		}
	}

	public void shutdown_all_ua() {
		for (AbstractURIHandler ua : all_ua)
			ua.teardown();
		all_ua.clear();
	}
	
	
	/**
	 * 做多等待30秒
	 */
	public void wait_for_all_ready() {
		int cnt=600;
		for (AbstractURIHandler ua : all_ua) {
			if (!ua.is_ready()&&(cnt--)>0)
				sleep(50);
		}
	}

	public void AfterMethod(Method m) {
		ResourceFactory.clear_all_replaced_ua_class();
		super.AfterMethod(m);
	}

	public void replace_ua_class(String name, Class my_class) {
		ResourceFactory.replace_ua_class(name, my_class);
	}

	public void restore_ua_class(String name) {
		ResourceFactory.restore_ua_class(name);
	}
}
