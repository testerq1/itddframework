package ate.ua;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
 * 封装消息及其session，为了保存packet的session信息，以支持多session的异步处理
 * @author ravi huang
 *
 */
public class AtePacket<C> {
	private C ctx;
	private Object packet;	
	public AtePacket(C session, Object packet){
		this.ctx=session;
		this.packet=packet;
				
	}
	
	public C getSession(){
		return ctx;
	}
	
	public Object getPacket(){
		return packet;
	}
}
