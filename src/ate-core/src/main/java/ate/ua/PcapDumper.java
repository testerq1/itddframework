package ate.ua;

import io.pkts.PcapOutputStream;
import io.pkts.buffer.Buffer;
import io.pkts.buffer.Buffers;
import io.pkts.frame.PcapGlobalHeader;
import io.pkts.packet.Packet;
import io.pkts.protocol.Protocol;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PcapDumper{
    protected static Logger log = LoggerFactory.getLogger(PcapDumper.class);
    private PcapGlobalHeader pgh;
    private PcapOutputStream pos;
    private int transport;
    
    public PcapDumper(String transport) {
        if(transport.equalsIgnoreCase("udp"))
            this.transport=1;
        else if(transport.equalsIgnoreCase("tcp"))
            this.transport=2;        
    }
    
    public void dump_to_pcap(SocketAddress src, SocketAddress dst, byte[] bs){
        if(pos==null)
            throw new RunFailureException("please set dump file first.");
        String srcAddr=((InetSocketAddress)src).getAddress().getHostAddress();
        int srcPort=((InetSocketAddress)src).getPort();
        String destAddr=((InetSocketAddress)dst).getAddress().getHostAddress();
        int destPort=((InetSocketAddress)dst).getPort();
        Buffer payload=Buffers.wrap(bs);
        Packet pkt=null;
        if(this.transport==1){
            pkt=GeneralPacketFactory.create_udp(srcAddr, srcPort, destAddr, destPort, payload);
            
        }else{
            pkt=GeneralPacketFactory.create_tcp(srcAddr, srcPort, destAddr, destPort, payload);
        }
        try {            
            pos.write(pkt);
        } catch (IOException e) {
            throw new RunFailureException(e);
        }        
    }
    
    /**
     * 初始化ETHERNET_II的pcap dumper
     * 
     * @param fname
     */
    public void set_dump_file(String fname){       
        if(pgh==null){
            pgh=PcapGlobalHeader.createDefaultHeader(Protocol.ETHERNET_II);
        }
        
        if(pos==null){          
            try {
                File ff=new File(fname);
                if(!ff.exists())
                    ff.createNewFile();
                
                OutputStream out = new FileOutputStream(new File(fname));                
                pos=PcapOutputStream.create(pgh, out);                
            } catch (Exception e) {
                throw new RunFailureException(e);
            }           
        }        
    }
    
}
