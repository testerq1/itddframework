package ate.ua;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.AbstractURIHandler;
import ate.Global;
import ate.util.Expect;

/**
 * 可在测试床文件中配置如下参数：
 *          UNAME： username
 *          PASSWD: passwd
 *          UNAME_P: username pattern
 *          PASSWD_P: password pattern
 *          PROMPT: prompt sign
 *          SUCCESS_FLAG: PS1
 * @author ravi huang
 *
 */
public abstract class AbstractVTUAImp extends
        AbstractPacketUAImp<String, String> implements Runnable {
    protected static Logger log = LoggerFactory
            .getLogger(AbstractVTUAImp.class);
    protected Expect expect = new Expect();
    
    /** ascii Ctrl+X */
    public static char ESC_CANCEL = 24;

    /** 重试次数，在connect失败,auth失败，session close后尝试redo的次数 */
    public int RETYR_TIMES = 5;

    /** 重传间隙(ms),在send_message失败后重传等待时间 */
    public int RESND_INTEVERAL = 20000;
    
    /**命令执行失败时，返回消息的标识*/
    private String errorFlag = "Error:";

    /** The instr. */
    protected InputStream instr;    
    
    /**通用prompt sign匹配符正则表达式,当读屏幕消息的线程匹配上时，将buffer加到vt的packet里去*/
    protected String prompt;
    protected Pattern line_endwith_pattern;
    
    
    /**换行符标识,所有新命令都会加上这个后发出*/
    private String line_mode;
    
    /** 读Screen的流 */
    protected OutputStream outstr;

    /**
     * 控制Screen reader线程是否退出，connect需要重连时应该先false该开关
     * @see #run()
     */
    protected AtomicBoolean quit = new AtomicBoolean(false);

    /** 读屏幕线程 */
    protected Thread reader;

    /**login时username和password匹配符*/
    protected String uname_p, passwd_p;
    
    /**login成功后的行匹配符*/
    protected String success_flag;

    /**url query中的ir参数(true/false)，表示是否发送起始换行符*/
    protected boolean initialrtn;

    @Override
    /**
     * 如果URL中没有指定，则从测试床文件中读取：UNAME,PASSWD作为username和passwd
     * 并且也会从测试床文件中尝试读取UNAME_P,PASSWD_P,PROMPT,SUCCESS_FLAG
     * 
     */
    public AbstractURIHandler build(UAOption option, Object value) {
        super.build(option, value);

        if (option != UA_URI) 
            return this;
        
        //从测试床ini文件中读配置参数
        if(section!=null){
            this.username=username!=null?username:arg("UNAME");
            this.passwd=passwd!=null?passwd:arg("PASSWD");
            this.uname_p=uname_p!=null?uname_p:arg("UNAME_P");
            this.passwd_p=passwd_p!=null?passwd_p:arg("PASSWD_P");
            this.prompt=prompt!=null?prompt:arg("PROMPT");
            this.success_flag=success_flag!=null?success_flag:arg("SUCCESS_FLAG");
        }
        
        try {
            line_endwith_pattern=expect.compile_e(".*");
        } catch (MalformedPatternException e) {
        }
        this.type = UA;
        initialrtn = "true".equals(get_query_para("ir"));
        
        return this;
    }

    /**
     * send 'cancel' to cancel current command
     * 
     * @throws Exception
     */
    public void cancel() throws Exception {
        send_char(ESC_CANCEL);
        send_char(ESC_CANCEL);
        send_line("");
    }

    /**
     * 发送一个回车换行符，然后返回结果
     * @return
     */
    public String get_prompt(){
        this.send_line("");
        return this.recv_message();
    }
    /**
     * 判断recv_message的返回值不为null并且不包含errorFlag
     * @return
     * @see #set_error_flag(String)
     */
    public boolean is_ok() {
        String rtn = this.recv_message();
        log.debug(rtn);
        return rtn != null && !expect.contains(rtn, this.errorFlag);
    }

    /**
     * login("login: ", "root", "assword: ", "passwd", "(]\\$ |: )$","[root");<br>
     * <br>
     * 以prompt为结束符，match 终端输出：<br>
     * if not match:<br>
     * 发空行后再检查（最多3次）<br>
     * else if (endsWith uname_p){<br>
     * send uname passwd<br>
     * } else {<br>
     * 已经login<br>
     * return <br>
     * }<br>
     * 
     * @param uname_p
     *            username 模式(endsWith)
     * @param uname
     *            username
     * @param passwd_p
     *            passwd模式(endsWith)
     * @param passwd
     *            password
     * @param prompt
     *            行标志(regexp.match_e)，没有做trim,因此必须注意空格，需要考虑login和longin成功后的两种情况
     * @param success_flag
     *            登陆成功标志(contains)
     */
    public boolean login(String uname_p, String uname, String passwd_p,
            String passwd, String prompt, String success_flag) {
        log.debug(this + " login...");
        if(uname_p==null||uname==null||passwd_p==null||passwd==null||prompt==null||success_flag==null)
            throw new RunFailureException("please set uname_p,uname,passwd_p,passwd,prompt,success_flag!");
        
        this.username = uname;
        this.uname_p = uname_p;
        this.passwd_p = passwd_p;
        this.passwd = passwd;
        this.success_flag = success_flag;
        this.prompt=prompt;
        set_line_endwith(prompt);

        HashMap<String, Object> hm = new HashMap<String, Object>();
        hm.put("pattern", prompt);
        String curr = null;        
        if (initialrtn){
            log.debug("send initial return {}",initialrtn);            
            curr = this.send_and_expect("", prompt);
        }else{
            curr = recv_message();
        }
        
        if(get_fragment()!=null)
            parseLineMode(get_fragment());
        else
            parseLineMode(curr);
        
        int retry = this.RETYR_TIMES;
        while ((curr == null || !message_matched(curr, hm))&& retry-- > 0) {
            send_line("");
            curr = recv_message(hm);
        }

        // login失败
        if (curr == null) {
            log.error("login failed!");
            return false;
        }

        log.debug("message received:> {} ====", curr);

        // 已经login了
        if (expect.end(curr, success_flag)) {
            log.debug("already login");
            return true;
        }

        retry = this.RETYR_TIMES;
        int tts = this.RETYR_TIMES;
        while (tts-- > 0) {
            while ((curr == null || expect.end(curr, passwd_p)) && retry-- > 0) {
                curr = this.send_and_expect("", uname_p);
            }
            if(curr==null){
                log.debug("can't get uname_p: {}",uname_p);
                continue;
            }            
            
            if ((curr=send_and_expect(uname, passwd_p)) == null){
                log.debug("login failure: uname={} flag={}",uname,passwd_p);
                continue;
            }
            if((curr=send_and_expect(passwd, success_flag)) == null){
                log.debug("login failure: passwd={} flag={}",passwd,success_flag);
                continue;
            }else{
                this.set_line_endwith(success_flag);
                log.debug("login successful,change flag to {}",success_flag);
                return true;
            }
        }
        return false;
    }
    /**使用默认参数登陆*/
    public boolean login(){
        return this.login(uname_p, username, passwd_p, passwd, prompt, success_flag);
    }
    
    @Override
    /**
     * hm中可以传一个模式字符串或者Pattern实例进行正则匹配，key="pattern",
     * @see org.apache.oro.text.regex.Pattern
     * @see Expect.match_e
     */
    public boolean message_matched(String msg, HashMap<String, Object> hm) {
        if (msg == null || msg.length() == 0)
            return false;

        if (hm == null)
            return true;

        Object pattern = hm.get("pattern");
        if (pattern instanceof Pattern)
            return expect.match_e(msg.toString(), (Pattern) pattern);
        else
            return expect.match_e(msg.toString(), pattern.toString());
    }
    /**
     * 解析换行符:如果value为
     *  n  -> \n
     *  r   -> \r
     *  其他 -> \r\n
     * 否则，如果Value
     *  包含\n\n -> \n
     *  包含\r\r -> \r
     *  否则 -> \r\n
     * 
     * @param value
     * @return
     */
    protected boolean parseLineMode(String value) {
        if (value == null) {
            log.error("parseLineMode failure, value={}", value);
            return false;
        }
        if (value.contains("\n\n")||value.equals("n")){
            log.debug("parse line mode: old={} value=n",line_mode);        
            line_mode = "\n";
        }else if (value.contains("\r\r")||value.equals("r")){
            log.debug("parse line mode: old={} value=r",line_mode); 
            line_mode = "\r";
        }else{
            log.debug("parse line mode: old={} value=rn",line_mode); 
            line_mode = "\r\n";
        }

        return true;
    }

    @Override
    public String recv_message() {
        Object o = super.recv_message();
        if (o != null)
            return o.toString();
        return null;
    }

    @Override
    public String recv_message(HashMap<String, Object> hm) {
        Object o = super.recv_message(hm);
        if (o != null)
            return o.toString();
        return null;
    }
    /**
     * 断链，等待RESND_INTEVERAL后重新连接，login
     * @return
     * @see AbstractVTUAImp#RESND_INTEVERAL
     */
    public boolean restart() {
        teardown();
        sleep(RESND_INTEVERAL);
        this.startup();
        return login(uname_p, this.username, passwd_p, this.passwd,
                this.prompt, success_flag);
    }

    @Override
    public void run() {
    	 byte[] bs = new byte[1024];
         int ret_read = 0;
         String screen = "";
        try {
           
            do {
                ret_read = instr.read(bs);

                if (ret_read > 0) {
                    String rcv = new String(bs, 0, ret_read, charset);
                    if (this.is_record) {
                        screen += rcv;
                        if (expect.match_e(screen, this.line_endwith_pattern)) {
                            this.add_packet(screen);
                            screen = "";
                        }
                    }
                    if(Global.verbose)
                    	log.debug("> {} {} ####",get_rcvd_count(), rcv);
                    this.rcvd();
                } 
            } while (ret_read >= 0 && !quit.get());
        } catch (Exception e) {
            new RunFailureException(e);
        }
        log.debug("reader quit: {} {}",ret_read,quit);
    }

    /**
     * 发送一个命令，返回响应
     * 
     * @param s
     * @return
     */
    public String send_and_expect(String s) {
        send_line(s);
        return recv_message();
    }

    /**
     * 发送一个命令，返回匹配pattern的响应,需要注意的是，使用此方法时，如果pattern不匹配，会等待直到超时 返回null
     * 
     * @param s
     * @param pattern
     * @return
     * @see #set_timeout(int)
     */
    public String send_and_expect(String s, final Object pattern) {
        send_line(s);
        
        //遍历整个message queue，匹配pattern
        int tts=this.RETYR_TIMES;
        String rtn=null;
        boolean rst=false;
        while((rtn=recv_message())!=null){
            if (pattern instanceof String)
                rst= expect.match_e(rtn, pattern.toString());
            else
                rst= expect.match_e(rtn, (Pattern)pattern);
            
            if(rst)
                return rtn;
            else{
                log.debug(rtn);
            }
        }
        if (pattern instanceof Pattern)
            log.warn("\nsend_and_expect failure: send={} \n pattern={}",s,((Pattern)pattern).getPattern());
        else
            log.warn("\nsend_and_expect failure: send={} \n pattern={}",s,pattern.toString());
            
        return null;
//        return recv_message(new HashMap() {
//            {
//                put("pattern", pattern);
//            }
//        });
    }
    /**
     * 发送一个字符
     * @param c
     * @throws Exception
     */
    public void send_char(char c) throws Exception {
        outstr.write(c);
        outstr.flush();
    }

    /**
     * 发送1个字符串+line_mode，在发送前会clear消息队列
     * 
     * @param s
     * @see #send_message(String)
     */
    public void send_line(String s) {
    	if(Global.verbose)
    		log.debug("{} send line: {}", this, s);
        this.clear_q();
        send_message(s + line_mode);
    }

    @Override
    /**
     * 发送1个字符串
     */
    public void send_message(String s) {
        int tts = this.RETYR_TIMES;
        try {
            outstr.write(s.getBytes());
            outstr.flush();
        } catch (IOException e) {
            boolean isdone = false;
            while (!isdone) {
                log.warn("try resend \"{}\" after {}ms ", s, RESND_INTEVERAL);
                sleep(RESND_INTEVERAL);
                try {
                    outstr.write(s.getBytes());
                    outstr.flush();
                    isdone = true;
                } catch (IOException e1) {
                    if (tts <= 0)
                        throw new RunFailureException(e1);
                } finally {
                    tts--;
                }
            }
        }
    }
    /**
     * 设置错误消息标识
     * @param flag
     * @see #is_ok()
     */
    public void set_error_flag(String flag) {
        this.errorFlag = flag;
    }       

    /**
     * 设置光标行标识
     * @param pattern
     * @throws MalformedPatternException
     * @see #run()
     */
    public void set_line_endwith(Object pattern) {
        if(pattern instanceof String)
            try {
                line_endwith_pattern= expect.compile_e((String)pattern);
            } catch (MalformedPatternException e) {
                new RunFailureException(pattern.toString(),e);
            }
        else
            line_endwith_pattern = (Pattern)pattern;
    }

    /**
     * \r \n \r\n,默认会简单判断一下
     * @param lm
     */
    public void set_line_mode(String lm){
    	this.line_mode=lm;
    }
    /**
     * 重发间隔，也用于重连
     * @param time
     * @see #restart()
     * @see #send_message(String)
     */
    public void set_resnd_interval(int time) {
        this.RESND_INTEVERAL = time;
    }
    
    /**
     * 重传次数
     * @param times
     * @see #send_message(String)
     */
    public void set_retry_times(int times) {
        this.RETYR_TIMES = times;
    }
    /**
     * 激活一个shell
     */
    public void shell() {
        while (true) {
            boolean end_loop = false;
            try {
                byte[] buff = new byte[1024];
                int ret_read = 0;

                do {
                    try {
                        ret_read = System.in.read(buff);
                        if (ret_read > 0) {

                            try {
                                outstr.write(buff, 0, ret_read);
                                outstr.flush();
                            } catch (IOException e) {
                                end_loop = true;
                            }

                        }
                    } catch (IOException e) {
                        System.err.println("Exception while reading keyboard:"
                                + e.getMessage());
                        end_loop = true;
                    }
                } while ((ret_read > 0) && (end_loop == false));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * 设置读线程quit标识，子类应该重载该方法，完成退出的清理工作，尤其是在restart时
     * @see #restart()
     */
    public void teardown() {
        this.quit.set(true);
    }
}
