package ate.ua;

public interface Reconnectable {
    void reconnect();
    boolean is_client();
}
