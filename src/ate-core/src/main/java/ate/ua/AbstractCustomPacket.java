package ate.ua;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.OutputStream;
import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.Global;

public abstract class AbstractCustomPacket<T> {
    protected Charset charset = Global.charset;
    protected static Logger log = LoggerFactory.getLogger(AbstractCustomPacket.class);
       
    protected String payload;
    protected byte[] data;
    
    protected int decode_status=0;
    
    public boolean is_ok(){
        return this.decode_status==0;
    }
    
    public AbstractCustomPacket() {
    }
    
    public AbstractCustomPacket(String message) {
        this.payload = message;
    }
    
    public AbstractCustomPacket(byte[] data) {
        this.data = data;
    }
    
    public abstract T build(String k,Object v);
    public abstract T get_packet();
    public abstract void encodePacket(OutputStream out);

    
    public byte[] get_bytes(Object value) {
        Integer i;
        if (value instanceof byte[])
            return (byte[]) value;
        else if (value instanceof String) {
            return ((String) value).getBytes(charset);
        } else if (value instanceof byte[])
            return (byte[]) value;
        else if (value instanceof Number)
            return new byte[] { ((Number) value).byteValue() };
        return null;
    }

    public void set_charset(Charset cs) {
        this.charset = cs;
    }
   
}
