package ate.ua.netty;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.net.InetSocketAddress;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.List;


@Sharable
public class MulticastEncoder extends MessageToMessageEncoder<CharSequence> {
    InetSocketAddress recipient, sender;
    
    public MulticastEncoder(InetSocketAddress recipient){
        this.recipient=recipient;
    }
    @Override
    protected void encode(ChannelHandlerContext ctx, CharSequence msg, List<Object> out) throws Exception {
        if (msg.length() == 0) {
            return;
        }
        out.add(new DatagramPacket((ByteBuf)msg,recipient));
    }
}