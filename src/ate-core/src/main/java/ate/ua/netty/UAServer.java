package ate.ua.netty;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import io.netty.bootstrap.AbstractBootstrap;
import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.NetUtil;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.FutureListener;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;

import ate.ua.RunFailureException;

@Sharable
public class UAServer extends ANettyIOAdapter implements Runnable {
	EventLoopGroup parentroup;
	EventLoopGroup group;
	AbstractBootstrap bootstrap;
	boolean started = false;
	ChannelFuture cf;

	public UAServer(ANettyUAImp csUA) {
		super(csUA);
	}

	@Override
	public boolean is_ready() {
		log.debug("is_ready: {} {} {}",
				new Object[] { cf.isCancelled(), cf.isDone(), cf.isSuccess() });
		log.debug("is_ready: {} {} {}",
				new Object[] { ch.isActive(), ch.isOpen(), ch.isWritable() });
		return ch != null && ch.isActive() && ch.isOpen() && ch.isWritable();
	}

	public void sync() {
		ch.closeFuture().syncUninterruptibly();
	}

	@Override
	public void startup() {
		log.debug("{} startup", this);
		try {
			group = new NioEventLoopGroup();
						
			if (ua.get_transport().equalsIgnoreCase("udp")) {
				//InetAddress.getByName("0.0.0.0");
				bootstrap = new Bootstrap();
				bootstrap.group(group).channel(NioDatagramChannel.class)
				         .localAddress(ua.get_host(), ua.get_port())
				         .option(ChannelOption.SO_BROADCAST, true)
						 .option(ChannelOption.SO_REUSEADDR, true)
						 .handler(handler);
				;

			} else if (ua.get_transport().equalsIgnoreCase("tcp")){
				bootstrap = new ServerBootstrap();
				parentroup = new NioEventLoopGroup();
				((ServerBootstrap) bootstrap).group(parentroup, group)
				        .localAddress(ua.get_host(), ua.get_port())
						.channel(NioServerSocketChannel.class)
						//.option(ChannelOption.TCP_NODELAY, true)
						.option(ChannelOption.SO_REUSEADDR, true)
						.option(ChannelOption.AUTO_READ, true)
						.childHandler(handler);
			} else if (ua.get_transport().equalsIgnoreCase("multicast")){			    
			    bootstrap = new Bootstrap();
			    InetSocketAddress addr=new InetSocketAddress(
		                NetUtil.LOCALHOST4, ua.get_port());
			    bootstrap.group(group).channel(NioDatagramChannel.class).localAddress(addr)
			             .option(ChannelOption.IP_MULTICAST_IF, ua.netif)
			             .option(ChannelOption.SO_REUSEADDR, true)
			             .handler(handler);			    
			} else{
			    throw new RunFailureException("wrong transport:"+ua.get_transport());
			}
			
			cf = bootstrap.bind().await();

			if (!cf.isDone())
				log.debug("{} bind failure",this);
			else {
				log.debug("{} bind port {} ",this, ua.get_port());
				ch = cf.sync().channel();
				ua.set_current_ctx(ch);				
			}
			// Thread tt= new Thread(this);
			// tt.start();
			// while(!started)
			// Thread.currentThread().sleep(50);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			started = true;
			ch.closeFuture().sync();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}

	@Override
	public void shutdown() {
		log.debug("{} shutdown", this);
		try {
			started = false;
			ch.close().sync();
			group.shutdownGracefully().sync();
            if (parentroup != null){                
                parentroup.shutdownGracefully().sync();
            }            
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getChannelKey(Channel ch) {
		if (ch.remoteAddress() == null)
			return "";
		return ch.remoteAddress().toString();
	}

	@Override
	public String toString() {
		return "UAServer";
	}
}
