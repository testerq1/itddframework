package ate.ua.netty;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.nio.charset.Charset;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.nio.NioDatagramChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.Global;
import ate.ua.AtePacket;

abstract class ANettyIOAdapter<T> extends SimpleChannelInboundHandler<T> {
	protected static Logger log = LoggerFactory
			.getLogger(ANettyIOAdapter.class);
	protected Channel ch;
	protected Charset charset = Charset.forName("UTF-8");

	protected ANettyUAImp<T> ua;
	protected ChannelInitializer<Channel> handler;
	protected boolean is_record=true;
	
	public void enable_record(boolean b){
		this.is_record=b;
	} 
	
	ANettyIOAdapter(ANettyUAImp ua) {
		this.ua = ua;
		ua.set_io_adapter(this);
		handler = new ChannelInitializer<Channel>() {
			@Override
			protected void initChannel(Channel ch) throws Exception {
				ANettyIOAdapter.this.ua.initChannel(ch);
			}
		};
	}
//    @Override
//    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
//        log.debug("{} channelRegistered {} ",ANettyIOAdapter.this, ctx.channel());
//    }
//    @Override
//    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
//    	log.debug("{} channelInactive {} ",ANettyIOAdapter.this, ctx.channel());
//        
//    }
	@Override
	public void channelRead0(ChannelHandlerContext ctx, T msg)
			throws Exception {
		log.debug("{} channelRead: {} {}", new Object[]{ANettyIOAdapter.this, ctx.channel(), msg.toString()});
		
		if(!ua.is_fuzzy()){   
    		T resp = ua.process_request(ctx, msg);
    		if (resp != null) {
    			log.debug("{} auto response!", ANettyIOAdapter.this);
    			ctx.write(resp);
    			return;
    		}		
		}
		ua.rcvd();	

		if(this.is_record)
			ua.add_packet(new AtePacket(ctx.channel(), msg));
	}

	// @Override
	// public void read(ChannelHandlerContext ctx) throws Exception {
	// log.info(ANettyChannelHandler.this+" read:");
	// ctx.read();
	// ctx.flush();
	// }
	//
	// @Override
	// public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise
	// promise) throws Exception {
	// log.info(ANettyChannelHandler.this+" "+ctx.channel()+" write "+msg);
	// ctx.write(msg);
	// ctx.flush();
	// }

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
	    if(Global.verbose)
	        log.warn("exceptionCaught: {}", cause);
		ctx.close();
	}

	abstract public String getChannelKey(Channel ctx);

	public abstract boolean is_ready();

	private void restart() {
		this.shutdown();
		try {
			Thread.currentThread().sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.startup();

	}

	public void send_string(String msg) {
		ch.writeAndFlush(msg);
	}

	public void set_ua(ANettyUAImp ua) {
		this.ua = ua;
	}

	abstract public void shutdown();

	public abstract void startup();
}
