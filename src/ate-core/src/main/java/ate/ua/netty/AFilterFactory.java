package ate.ua.netty;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import io.netty.channel.ChannelHandler;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AFilterFactory {
	private static final StringDecoder DECODER = new StringDecoder();
	private static final StringEncoder ENCODER = new StringEncoder();

	public abstract List<ChannelHandler> create_filters();
	
	public static AFilterFactory create_default(){
	    return new AFilterFactory() {
	        @Override
	        public List<ChannelHandler> create_filters() {
	            return new ArrayList<ChannelHandler>(
	                    Arrays.asList(
	                            new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()), 
	                            DECODER, 
	                            ENCODER)
	                    );
	        }

	    };
	}
    public static AFilterFactory create_default_mcast(final InetSocketAddress rmt){
        ENCODER.set_recipient(rmt);
        ENCODER.enable_mcast(true);
        return new AFilterFactory() {
            @Override
            public List<ChannelHandler> create_filters() {
                return new ArrayList<ChannelHandler>(
                        Arrays.asList(
                                new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()), 
                                DECODER,                                
                                ENCODER)
                        );
            }

        };
    }
}
