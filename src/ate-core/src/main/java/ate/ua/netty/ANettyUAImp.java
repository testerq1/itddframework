package ate.ua.netty;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.NetUtil;
import io.netty.util.concurrent.DefaultEventExecutorGroup;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.security.GeneralSecurityException;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;

import ate.AbstractURIHandler;
import ate.Global;
import ate.ua.AbstractPacketUAImp;
import ate.ua.AtePacket;
import ate.ua.IAutoResponseMessage;
import ate.ua.Reconnectable;
import ate.ua.RunFailureException;
import ate.ua.UAOption;
import ate.ua.ssl.BogusSslContextFactory;

/**
 * netty的io
 * @author ravi huang
 *
 * @param <T>
 */
public abstract class ANettyUAImp<T> extends AbstractPacketUAImp<T,AtePacket<Channel>> 
implements Reconnectable {
	protected boolean broadcast = false;
	protected Channel ch;
	protected AFilterFactory filtersFactory;
	protected ANettyIOAdapter<T> io;
	protected DefaultEventExecutorGroup executor;
	SSLContext ssl_ctx;
	InetSocketAddress groupAddress;
	NetworkInterface netif;
	
	public static final UAOption<String> NET_IF =
            new UAOption<String>("NET_IF");
	
	public void broadcast(Object msg, String recipient) {
		this.broadcast(msg, recipient, null);
	}

	public void broadcast(Object msg, String recipient, String sender) {
		log.debug("{} broadcast {} to {}",
				new Object[] { this, msg, recipient });

		if (!this.get_transport().equalsIgnoreCase("udp"))
			throw new Error("can't broadcast on no udp transport!");

		while (ch == null || !ch.isActive() || !ch.isWritable()) {
			log.warn("channel is null");
			sleep(1000);
		}
		MulticastPacket pkt = null;
		if (sender != null)
			pkt = new MulticastPacket(msg, recipient, sender);
		else
			pkt = new MulticastPacket(msg, recipient);

		this.send_message(pkt);
	}

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			if (this.path != null && this.path.length() > 3)
				this.transport = this.path.substring(1);

			if (this instanceof IAutoResponseMessage)
				this.register_auto_response("0", (IAutoResponseMessage) this);

			String br = get_query_para("broadcast");
			broadcast = br != null && br.equalsIgnoreCase("true");
		} else if (option == UA_TYPE) {
			if (type == SERVER)
				this.io = new UAServer(this);
			else
				this.io = new UAClient(this);
			
			io.enable_record(this.is_record);
		}else if(option==NET_IF){
		    String nif=value.toString();
		    try {
                netif=NetworkInterface.getByInetAddress(InetAddress.getByName(nif));
            } catch (Exception e) {
                throw new RunFailureException(e);
            } 
		}
		return this;
	}

	public void connect(ANettyUAImp clt) {
		if (this.get_transport().equalsIgnoreCase("udp")) {
			log.debug("{} connect to {}", ch, clt.ch);
			// this.ch.bind(this.ch.localAddress());
			ch = ch.connect(clt.ch.localAddress()).channel();
		}
	}

	public void enable_auto_reconnect(boolean b) {
		if (io instanceof UAClient)
			((UAClient) io).enable_auto_reconnect(b);
	}
	
	@Override
	public void set_is_record(boolean b){
		super.set_is_record(b);
		io.enable_record(b);		
	}
	
	public ANettyIOAdapter get_io_handler() {
		return io;
	}

	@Override
	public Object get_message() {	
	    if (Global.verbose)
            log.debug("{} get_message", this);
		Object o = super.get_message0();
		if (o == null)
			return null;
		if(o instanceof AtePacket){
    		this.ch = ((AtePacket<Channel>)o).getSession();
    		return ((AtePacket<Channel>)o).getPacket();
		}
		else
		    return o;
	}

	public void initChannel(Channel ch) {
		log.debug("initChannel threadpool={}",Global.threadpool);
		ChannelPipeline pipeline = ch.pipeline();
		if (filtersFactory == null) {
			log.warn("use default filtersFactory.");

	        if(this.transport.equalsIgnoreCase("multicast")){
	            InetSocketAddress addr = new InetSocketAddress(
	                    NetUtil.LOCALHOST4, get_port());
	            filtersFactory = AFilterFactory.create_default_mcast(addr);
	        }else
	            filtersFactory = AFilterFactory.create_default();
		}
		
		if(ssl_ctx!=null){
		    SSLEngine engine = ssl_ctx.createSSLEngine();
		    engine.setUseClientMode(type==CLIENT);		    
		    //engine.setNeedClientAuth(type==SERVER&&true);
		    pipeline.addLast("ssl", new SslHandler(engine));
		}
		
		List<ChannelHandler> chs = filtersFactory.create_filters();        
		for (ChannelHandler tmp : chs)
			pipeline.addLast(tmp);
		
		
		//调试模式下不使用线程池?
		if(this.type==this.CLIENT||Global.threadpool<=0)
		    pipeline.addLast(this.io);
		else {
		    if(executor==null)
		        executor=new DefaultEventExecutorGroup(Global.threadpool);
		    pipeline.addLast(executor,this.io);
		}
		
	}
	
	@Override
	public boolean is_client() {
		return io instanceof UAClient;
	}

	@Override
	public boolean is_ready() {
		return io != null && io.is_ready();
	}

	/**
	 * 便利消息处理器，返回得到第一个响应 消息自动处理器采用TreeMap，以key排序 这里应该是个final方法，暂时为public，以便兼容脚本
	 * 
	 * @param session
	 * @param req
	 * @return
	 * @see setAutoResponse()
	 */
	public T process_request(ChannelHandlerContext ctx, T msg) {
		for (String key : responseGenerator.keySet()) {
			IAutoResponseMessage<ChannelHandlerContext,T> tmp = responseGenerator.get(key);
			T resp = tmp.get_auto_resp_msg(ctx, msg);
			if (resp != null)
				return resp;
		}
		return null;
	}
	
	@Override
	public void reconnect() {
		if (io instanceof UAClient)
			((UAClient) io).reconnect();
	}

	private void send_message(Channel ch, Object msg) {
		log.debug("{} send message: {}", ch, msg);
		
		ChannelFuture cf = ch.writeAndFlush(msg);
		cf.awaitUninterruptibly();

		if (!cf.isSuccess())
			log.error("send message failure : {}", cf.cause());
	}

	@Override
	public void send_message(Object msg) {
		while (ch == null || 
		        !ch.isActive() || 
		        !ch.isWritable()) {
			log.warn("{} channel isn't available {} {} {}", new Object[] {
					this, ch, ch.isActive(), ch.isWritable() });
			sleep(1000);
		}
		send_message(ch, msg);
	}

	public void set_current_ctx(Channel ch) {
		this.ch = ch;
	}

	public void set_filters_factory(AFilterFactory filtersFactory) {
		this.filtersFactory = filtersFactory;
	}

	public void set_io_adapter(ANettyIOAdapter io) {
		this.io = io;
	}
	
	@Override
	public void startup() {
		io.startup();
	}

	@Override
	public void teardown() {
		io.shutdown();
	}
    public ANettyUAImp use_bogus_ssl(){
        System.setProperty( "sun.security.ssl.allowUnsafeRenegotiation", "true" );        
        try {             
            ssl_ctx=BogusSslContextFactory
                    .getInstance(type==SERVER);            
            return this;
        } catch (GeneralSecurityException e) {
            throw new RunFailureException(e);
        }       
    }
}
