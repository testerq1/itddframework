package ate.ua.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import io.netty.handler.codec.ReplayingDecoder;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import ate.Global;
import ate.ua.AbstractCustomPacket;
import ate.ua.DecoderState;

public abstract class AbstractCustomUA<T extends AbstractCustomPacket> extends
ANettyUAImp<T> {
    protected boolean needDecode = true;
    /** Key for decoder current state */
    private static final String DECODER_STATE_ATT = "pkt.ds";

    /** Key for the partial requests head */
    private static final String PARTIAL_HEAD_ATT = "pkt.ph";

    /** Key for the number of bytes remaining to read for completing the body */
    private static final String BODY_REMAINING_BYTES = "pkt.brb";

    class UADecoder extends ReplayingDecoder<DecoderState>  {
        @Override
        protected void decode(ChannelHandlerContext ctx, ByteBuf message, List out) throws Exception {
            if(message.readableBytes() <=0){
                return;
            }
            
            if(Global.verbose)
                log.debug("doDecode {} {}",this,message);
            
            switch(state()){
            case HEAD:
                log.debug("decoding HEAD");
//                int readableBytes = actualReadableBytes();
//                if (readableBytes > 0) {                    
//                    out.add(message.readBytes(actualReadableBytes()));
//                }                                
            case NEW:
                log.debug("decoding NEW:{} ",message.readableBytes());
                final T rq = decodePacket(message);
                if (rq == null) {
                    out.add(message);
                    out.add(DecoderState.HEAD);
                    return;
                }else{                
                    out.add(rq);
                    checkpoint(DecoderState.NEW);
                }                
                break;
           
            default:
                break;
            }
            
        }
    }

    class UAEncoder extends MessageToMessageEncoder<T> {        
        @Override
        protected void encode(ChannelHandlerContext ctx, T msg, List<Object> out) throws Exception {
            if (msg == null) {
                return;
            }
            if (msg instanceof AbstractCustomPacket) {
                T pkt = (T) msg;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                pkt.encodePacket(bos);
                byte[] bs = bos.toByteArray();
                log.debug("encode:{} {}",bs.length,Arrays.toString(bs));
                out.add(ByteBuffer.wrap(bs));
            }
        }
    }

    public abstract T decodePacket(ByteBuf bs);

    @Override
    public boolean message_matched(T msg, HashMap<String, Object> hm) {
        return true;
    }
}