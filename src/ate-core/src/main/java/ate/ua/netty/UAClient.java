package ate.ua.netty;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.channel.socket.DatagramChannel;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.util.NetUtil;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.FutureListener;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.concurrent.TimeUnit;

import ate.Global;
import ate.ua.RunFailureException;

@Sharable
public class UAClient extends ANettyIOAdapter {
	private boolean needReconnect = false;
	EventLoopGroup group;
	Bootstrap bootstrap;
	Channel ch;
	private int RETRY_TIMES=Global.RETRY_TIMES;
	
	public UAClient(ANettyUAImp ua) {
		super(ua);		
	}

	public void reconnect() {
		log.debug("{} reconnecting", this);
		boolean b=needReconnect;
		needReconnect=false;
		this.shutdown();
		this.startup();
		needReconnect=b;
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		super.channelInactive(ctx);
		if (!need_auto_reconnect())
			return;

		log.debug("channelInactive: auto reconnecting {}", ctx);
		while (is_ready()) {
			try {
				Thread.currentThread().sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		startup();
	}
   
	public boolean need_auto_reconnect() {
		return needReconnect;
	}

	public void enable_auto_reconnect(boolean b) {
		this.needReconnect = b;
	}

	@Override
	public boolean is_ready() {
		return ch != null && ch.isActive();
	}

	@Override
	public void startup() {
		log.debug("{} startup", this);
		try {
			group = new NioEventLoopGroup();
			bootstrap = new Bootstrap();
			if (ua.get_transport().equalsIgnoreCase("udp")) {
				bootstrap.group(group).channel(NioDatagramChannel.class)
						.option(ChannelOption.SO_BROADCAST, true)
						.option(ChannelOption.SO_REUSEADDR, true)
						.handler(handler);
				connect();
			} else if (ua.get_transport().equalsIgnoreCase("tcp")){
				bootstrap.group(group).channel(NioSocketChannel.class)
						.option(ChannelOption.TCP_NODELAY, true)
						.option(ChannelOption.SO_REUSEADDR, true)
						.handler(handler);
				connect();
			} else if (ua.get_transport().equalsIgnoreCase("multicast")){
			    InetSocketAddress addr=new InetSocketAddress(
                        NetUtil.LOCALHOST4, ua.get_port());
                bootstrap.group(group).channel(NioDatagramChannel.class)
                         .localAddress(ua.get_port()).remoteAddress(addr)
                         .option(ChannelOption.IP_MULTICAST_IF, ua.netif)
                         .option(ChannelOption.SO_REUSEADDR, true)
                         .handler(handler);  
                
                ch = bootstrap.bind().sync().channel();
                InetSocketAddress groupAddress = new InetSocketAddress(ua.get_host(), ua.get_port());
                ((DatagramChannel)ch).joinGroup(groupAddress, ua.netif).sync();
                
            } else{
                throw new RunFailureException("wrong transport:"+ua.get_transport());
            }		
			ua.set_current_ctx(ch);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("create channel: {}", ch);
	}
	
	private void connect(){
	    SocketAddress localaddr = ua.getLocalBindAddress();
        ChannelFuture cf = null;
        int retry=0;
        while((cf==null||ch==null)&& retry++ < RETRY_TIMES){
            if (localaddr != null) {
                cf = bootstrap.connect(ua.getTargetAddress(), localaddr);
            } else {
                cf = bootstrap.connect(ua.getTargetAddress());                
            }
            cf.awaitUninterruptibly(30000);
            if (!cf.isDone())
                log.warn("Channel Future Still Waiting. Cancelled: {}, retry:{}",
                        cf.cancel(true),retry);
            else {
                ch = cf.syncUninterruptibly().channel();                
            }
        }
	}
	
	@Override
	public String getChannelKey(Channel ch) {
		return ch.localAddress().toString();
	}

	@Override
	public void shutdown() {
		log.debug("{} shutdown", this);
		this.needReconnect=false;
		try {	    		    
		    ch.close().sync();
		    group.shutdownGracefully().sync();		    
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return "UAClient";
	}
}
