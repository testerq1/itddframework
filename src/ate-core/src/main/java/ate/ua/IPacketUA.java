package ate.ua;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashMap;

/**
 * 自动响应的接口，每收到请求消息时，IoHandler都会通过此接口要求UA按照request消息生成
 * response，允许用户通过attrs指定一组默认的属性，供生成响应时使用，可以是参数，标志等
 * 等 
 * @author xiaoyong.huang
 *
 */
public interface IPacketUA<T,M> {

	/**
	 * UA发消息接口
	 * @param msg
	 */
	void send_message(T msg);
	
	/**
	 * 用户或脚本中收消息的接口
	 * @param hm
	 * @return
	 * @see set_timeout enable_record
	 */
	Object recv_message(HashMap<String,Object> hm);
	Object recv_message();
	/**
	 * 在调用recv_message时做简单的消息过滤，注意，并不是socket上的过滤，而是msg queue上的过滤
	 * @param msg
	 * @param hm
	 * @return
	 */
	boolean message_matched(T msg,HashMap<String, Object> hm);
	
	/**
	 * 清除消息队列
	 */
	void clear_q();

	/**
	 * 是否需要记录
	 * @return
	 */
	boolean need_record();
	
	/**
	 * 将收到的消息保存到msgQue里去，为了统一rawsocket和mina框架的接口
	 * @param pkt
	 */
	void add_packet(Object pkt);
	
	/**
	 * 是否记录收到的消息，为false时不会调用add_packet
	 * @param b
	 */
	void set_is_record(boolean b);
}
