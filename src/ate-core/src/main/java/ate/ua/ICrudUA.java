package ate.ua;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.List;

public interface ICrudUA {
	/**
	 * insert,delete,update等不返回ResultSet的SQL
	 * @param qString
	 * @return
	 */
	public int update(String qString);
	
	/**
	 * 返回一个结果集的SQL
	 * @param qString
	 * @return
	 */
	public Object query(String qString);
	List<String> getDatabases();	
	List<String> getTables(String db);
	long countTable(String table);
	void teardown();
	void startup();
	
}
