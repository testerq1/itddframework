package ate.ua;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import groovy.sql.Sql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ate.AbstractURIHandler;
import ate.rm.dev.Host;

/**
 * 所有CRUD类型UA的父类， 支持Groovy Sql,当在uri中带上gsql的fragment时，初始化groovy sql<br>
 * See <a href="http://groovy.codehaus.org/api/groovy/sql/Sql.html">Groovy SQL</a>
 * 
 * @author xiaoyong.huang
 */
public abstract class AbstractCrudImp extends AbstractURIHandler implements ICrudUA {
	private static String home;
	protected static Host runtime=Host.localhost();
	
	boolean autoCommit = true;
	protected Connection con;
	protected Statement st;	
	public Sql gsql;
	protected boolean use_gsql=false;
	
	public static void reinit_static(){
		home=Host.HOME;
		runtime=Host.localhost();
	}
	
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
		    validate_url();
			use_gsql="gsql".equalsIgnoreCase(this.get_fragment());
		}
		return this;
	}
	
	public void commit() {
		if (autoCommit)
			return;
		try {
			this.con.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public long countTable(String table) {
		try {
			ResultSet rs = st.executeQuery("select count(*) from " + table);
			rs.next();
			return rs.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;

	}
	public long countTableWhere(String table,String where) {
		try {
			ResultSet rs = st.executeQuery("select count(*) from " + table+" where "+where);
			rs.next();
			return rs.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
	
	// private void walkRS(ResultSet rs) throws SQLException {
	//
	// while (rs.next()) {
	// String name = rs.getString("name");
	// int age = rs.getInt("age");
	// String sex = rs.getString("sex");
	// String address = rs.getString("address");
	// String depart = rs.getString("depart");
	// String worklen = rs.getString("worklen");
	// String wage = rs.getString("wage");
	//
	// // 输出查到的记录的各个字段的值
	// System.out.println(name + " " + age + " " + sex + " " + address
	// + " " + depart + " " + worklen + " " + wage);
	//
	// }
	// }

	@Override
	public int update(String sql) {
		try {
			return st.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	/**
	 * for RDBMS
	 * 
	 * @return
	 */
	public List<String> getDatabases() {
		ArrayList<String> ar = new ArrayList<String>();
		try {
			ResultSet rs = con.getMetaData().getCatalogs();
			while (rs.next()) {
				ar.add(rs.getString("TABLE_CAT"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ar;
	}

	abstract protected String getDriver();

	abstract protected String getJDBCUri();

	/**
	 * for RDBMS
	 * 
	 * @return
	 */
	public List<String> getSchemas() {
		ArrayList<String> ar = new ArrayList<String>();
		try {
			DatabaseMetaData md=con.getMetaData();
			ResultSet rs = md.getSchemas();
			while (rs.next()) {
				ar.add(rs.getString("TABLE_SCHEM") + " "
						+ rs.getString("TABLE_CATALOG"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ar;
	}

	public List<String> getTables(String db) {
		ArrayList<String> ar = new ArrayList<String>();
		try {
			DatabaseMetaData md=con.getMetaData();
			ResultSet tables = md.getTables(db, "DBA", "", null);
			String tableName;
			while (tables.next()) {
				tableName = tables.getString(3);
				ar.add(tableName);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ar;
	}

	@Override
	public boolean is_ready() {
		return con != null && st != null;
	}

	public PreparedStatement prepareStatement(String sql) {
		try {
			return con.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ResultSet query(String qString) {
		try {
			ResultSet rs=st.executeQuery(qString);
			
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setAutoCommit(boolean b) {
		autoCommit = b;
		try {
			con.setAutoCommit(b);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void startup() {		
		try {
			if(use_gsql){
				gsql=Sql.newInstance(getJDBCUri(),get_uname(),get_passwd());
			} else {
				Class driver=Class.forName(getDriver());
				con = DriverManager.getConnection(this.getJDBCUri(),
						this.get_uname(), this.get_passwd());
				st = (Statement) con.createStatement();
			}
		} catch (Exception e) {
			log.error("database connect failure:" + e.getMessage());
		}
	}

	public void teardown() {
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * # yum install mysql-libs mysql-devel<br>
	 * # ln -s ln -s /usr/lib64/mysql/libmysqlclient.so.16 /usr/lib64/mysql/libmysqlclient.so<br>
	 * # wget http://vegan.net/tony/supersmack/super-smack-1.3.tar.gz<br>
	 * <br>
	 * # configure --prefix=/usr --with-mysql --with-mysql-lib=/usr/lib64/mysql<br>
	 * modify src/dictionary.h cstring <br>
	 * modify src/query.cc L200 L219<br>
	 * # make  &&   make install<br>
	 * ......<br>
	 *  /usr/bin/install -c  super-smack /usr/bin/super-smack<br>
	 *  /usr/bin/install -c  gen-data /usr/bin/gen-data<br>
	 *  mkdir /usr/share/smacks<br>
	 *  mkdir /var/smack-data<br>
	 *  <br>
	 * Usage:<br>
	 * # gen-data -n 900000 -f %12-12s%n,%25-25s,%n,%d > /var/smack-data/words.dat<br>
	 * # 确保/var/smack-data/words.dat存在不为空<br>
	 * # ls -lh /var/smack-data/words.dat<br>
	 * <br>
	 * # super-smack -d mysql select-key.smack 10 100000<br>
	 *  参数： select-key.smack   Qps测试 <br>
	 *        update-select.smack Tps测试<br>
	 *        -d 指定测试DB类型 <br>
	 *        10 个线程 每个线程 1000 个查询<br>
	 *        -D  参数来指定数据文件，默认路径 /var/smack-data <br>
	 *  测试结果: <br>
	 *  connect: max=0ms  min=0ms avg= 0ms from 10 clients<br>
	 *  Query_type    num_queries    max_time    min_time    q_per_s<br>
	 *  select_index      2000000      0           0         64574.34<br>
	 *  max22=  min=   avg=   from 10 clients 连接的最大、最小及平均花费时间。<br>
	 *  q_per_s|  64574.34   QPS，每秒请求处理数<br>
	 */
	public void supersmack(){
		//TODO nothing
	}
	/**
	 * 
	 * # wget http://downloads.mysql.com/source/dbt2-0.37.50.3.tar.gz<br>
	 * <br>
	 * # ./configure --with-mysql --prefix=/usr<br>
	 * # make & make install<br>
	 * <br>
	 *  /usr/bin/install -c client datagen driver transaction_test '/usr/bin'<br>
	 * <br>
	 * Usage:<br>
	 * 生成数据：<br>
	 * datagen -w 300 -d /home/q/data300 --mysql<br>
	 * 参数说明：<br>
	 * -w：指定了数据仓库的个数<br>
	 * -d：指定了生成的数据所在的目录、<br>
 	 * <br>
	 * 加载数据：<br>
	 * cd dbt2/scripts/mysql<br>
	 * sudo ./build.sh -d dbt2 -f /home/q/data300 -s /tmp/mysql.sock -h localhost-u root -p ""<br>
	 *  <br>
	 * 参数说明<br>
	 * -d：数据库名<br>
	 * -f：数据文件地址<br>
	 * -g：生成数据文件<br>
	 * -m：数据库模式[OPTIMIZED|ORIG] (默认 OPTIMIZED)<br>
	 * -s：UNIX socket<br>
	 * -h：数据库主机<br>
	 * -u：用户名<br>
	 * -p：密码<br>
	 * -e：存储引擎[MYISAM|INNODB|BDB] (默认 INNODB)<br>
	 * -l：使用LOCAL关键字加载数据<br>
	 * -v：verbose<br>
	 * -w：warehouse<br>
	 *   <br>
	 * 测试：<br>
	 *  cd dbt2/scripts<br>
	 *  sudo ./run_workload.sh<br>
	 *   <br>
	 *  参数说明：<br>
	 *  -c：连接数<br>
	 *  -d：持续时间<br>
	 *  -H：主机<br>
	 *  -h：help<br>
	 *  -l：端口<br>
	 *  -n：NO-THINK<br>
	 *  -o：USE_OPROFILE=1<br>
	 *  -p：DB_PARAMS<br>
	 *  -s：SLEEPY<br>
	 *  -t：每个warehouse的线程数<br>
	 *  -u：用户名<br>
	 *  -w：WAREHOUSES<br>
	 *  -x：用户密码<br>
	 *  -z：注释COMMENT<br>
	 *  
	 * @return
	 * @see http://dev.mysql.com/downloads/benchmarks.html
	 */
	public void dbt2(){
		//TODO nothing
	}
	
	public String capabilities() {
		StringBuffer buf=new StringBuffer(runtime.capabilities());
		buf.append("super-smack: "+(runtime.which("super-smack")==null?"no":"yes")).append("\r\n");
		
		return buf.toString();
	}
	
	protected void validate_url(){
	    if(get_host()==null)
	        throw new RunFailureException("missing hostname: "+get_host());	        
	}
}
