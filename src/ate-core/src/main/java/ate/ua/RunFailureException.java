package ate.ua;

public class RunFailureException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RunFailureException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RunFailureException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RunFailureException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RunFailureException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
