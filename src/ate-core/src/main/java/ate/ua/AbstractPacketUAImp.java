package ate.ua;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import io.pkts.PcapOutputStream;
import io.pkts.frame.PcapGlobalHeader;
import io.pkts.protocol.Protocol;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;

import ate.AbstractURIHandler;
import ate.Global;

import com.google.common.collect.EvictingQueue;

/**
 * 协议相关UA的父类，子类包括Mina,Raw,Socket
 * 
 * @author xiaoyong.huang
 * 
 */
public abstract class AbstractPacketUAImp<T, M> extends AbstractURIHandler
		implements IPacketUA<T, M> {
	protected URI cltLocalUri;

	protected long deadline = 0;

	/** 是否记录 packet */
	protected boolean is_record = true;
	protected EvictingQueue msgQue = EvictingQueue
			.create(Global.queue_len);
	
	private long rcvd = 0;
	/** 消息自动处理器 */
	protected TreeMap<String, IAutoResponseMessage> responseGenerator = new TreeMap<String, IAutoResponseMessage>();

	private long snd = 0;

	/** 收消息时等待的时长 */
	protected int timeout = Global.timeout;
	
	protected String transport;
	
	//是否是畸形包测试
	private boolean fuzzy=false;
	
	@Override
	public void add_packet(Object pkt) {
		msgQue.add(pkt);
	}
	
	/**
	 * build packet ua
	 * 
	 * @param type
	 *            CLENT or SERVER
	 * @param local_uri
	 *            local bind uri
	 */
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option == URI2 && value != null) {
			try {
				this.cltLocalUri = new URI((String) value);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}

		return this;
	}

	/**
	 * 清除所有的消息处理器
	 */
	public void clean_responsers() {
		responseGenerator.clear();
	}

	@Override
	public void clear_q() {
		msgQue.clear();
		rcvd = 0;
	}
	
	
	
	/**
	 * 在指定时间内，是否收到指定个数的响应
	 * 
	 * @param v
	 *            响应个数
	 * @param time
	 *            时间
	 * @return
	 */
	public boolean expect_rcvd_count(long value, long time) {
		long end = new Date().getTime() + time;
		while (new Date().getTime() < end) {
			if (this.rcvd == value)
				return true;
			sleep(50);
		}
		log.debug("{} rcvd: {}", this, rcvd);
		return this.rcvd == value;
	}

	public boolean expect_rcvd_ge(long value, long time) {
		long end = new Date().getTime() + time;
		while (new Date().getTime() < end) {
			if (this.rcvd >= value)
				return true;
			sleep(50);
		}
		log.debug("{} rcvd: {}", this, rcvd);
		return false;
	}

	public boolean expect_rcvd_le(long value, long time) {
		long end = new Date().getTime() + time;
		while (new Date().getTime() < end) {
			if (this.rcvd <= value)
				return true;
			sleep(50);
		}
		log.debug("{} rcvd: {}", this, rcvd);
		return false;
	}

	/**
	 * local uri通常用于指定client的bind地址，用build()指定
	 * 
	 * @return 本地地址
	 * @see UAOption#URI2
	 */
	public URI get_client_local_uri() {
		return cltLocalUri;
	}
	
	public void fuzzy(boolean b){
	    this.fuzzy=b;
	}
	
	/**
	 * 子类重载该方法，可以在收消息后做进一步处理
	 * @return
	 * @see #add_packet(Object)
	 */
	public Object get_message() {
		return get_message0();
	}

	/**
	 * 在timeout时间内检查是否收到消息
	 * 
	 * @return
	 * @see #set_timeout(int)
	 * @see #message_matched(Object, HashMap)
	 * @see #recv_message()
	 */
	protected Object get_message0() {
		if (Global.verbose)
			log.debug("{} get_message", this);
		int i = 1;
		while (new Date().getTime() < deadline) {
			if (msgQue.size() > 0) {
				return msgQue.poll();
			} else
				try {
					Thread.currentThread().sleep(i * 5);
					i++;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	public EvictingQueue get_msg_q() {
		return msgQue;
	}

	public long get_rcvd_count() {
		return this.rcvd;
	}

	public long get_snd_count() {
		return this.snd;
	}

	public String get_transport() {
		return transport;
	}

	public SocketAddress getLocalBindAddress() {
		if (cltLocalUri == null)
			return null;
		try {
			return parse_address(this.cltLocalUri);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public SocketAddress getTargetAddress() {
		return new InetSocketAddress(get_host(), get_port());
	}

	@Override
	public boolean need_record() {
		return is_record;
	}

	public boolean is_fuzzy(){
	    return this.fuzzy;
	}
	   
	protected SocketAddress parse_address(String uri) {
		try {
			return parse_address(new URI(uri));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected SocketAddress parse_address(URI uri) {
		String host = uri.getHost();
		int port = uri.getPort();
		return new InetSocketAddress(host, port);
	}

	/**
	 * 统计收到多少消息，不论是否记录
	 * 
	 * @return
	 */
	public long rcvd() {
		rcvd++;
		return rcvd;
	}

	/**
	 * 用户收消息时使用
	 * 
	 * @see #recv_message(HashMap)
	 */
	public T recv_message() {
		return recv_message(null);
	}

	/**
	 * 收消息时的接口
	 */
	public T recv_message(HashMap<String, Object> hm) {
		if (Global.verbose)
			log.debug("{} try to recv_message", this);
		update_timeout();
		T msg = (T) get_message();
		while (msg != null) {
		    //不支持分片
		    if(this.is_fuzzy())
		        return msg;
		    
			if (message_matched(msg, hm))
				return msg;
			msg = (T) get_message();
		}
		return null;
	}
	/**
	 * 提供一个MessageMatcher接口
	 * @param matcher
	 * @return
	 */
	public T recv_with_matcher(MessageMatcher<T> matcher) {
	    if (Global.verbose)
            log.debug("{} recv_message", this);
        update_timeout();
        T msg = (T) get_message();
        while (msg != null) {
            if (matcher.match(msg))
                return msg;
            msg = (T) get_message();
        }
        return null;
	}
	/**
	 * 删除一个消息自动处理器
	 * @param id
	 */
    public void remove_auto_response(String id) {
        responseGenerator.remove(id);
    }
    
	/**
	 * 注册一个消息自动处理器<br>
	 * 这里提供非常简单的类似mina框架的IoFilter的处理机制，IAutoResponseMessage只有1个方法
	 * 
	 * @param id
	 * @param handler
	 */
	public void register_auto_response(String id, IAutoResponseMessage handler) {
		responseGenerator.put(id, handler);
	}

	public void reset_counter() {
		this.snd = 0;
		this.rcvd = 0;
	}
	
	@Override
	public void set_is_record(boolean b) {
		this.is_record = b;
	}
	
	public void set_timeout(int seconds) {
		timeout = seconds;
	}

	public long snd() {
		this.snd++;
		return snd;
	}

	/**
	 * 将Deadline设置为10秒以后，denaline到期后，将不会再收消息
	 */
	public void update_timeout() {
		deadline = new Date().getTime() + this.timeout * 1000;
		if (Global.verbose)
			log.debug("update_timeout(secs): {}", timeout);
	}
}
