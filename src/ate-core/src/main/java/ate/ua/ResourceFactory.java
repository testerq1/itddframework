package ate.ua;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashMap;

import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.AbstractURIHandler;

/**
 * createUA("sip://192.168.1.1:5060/udp")
 * 
 * 
 * @author xiaoyong.huang
 * 
 */
public class ResourceFactory {
	private static Logger log = LoggerFactory.getLogger(ResourceFactory.class);
	/** 保存自动加载的UA类  */
	static HashMap<String, AbstractURIHandler> uas = new HashMap<String, AbstractURIHandler>();
	
	/** 用自定义的UA类替换已有的，用于支持自定义类UA级别的重写 */
	static HashMap<String, Class> uas0 = new HashMap<String, Class>();
	static {
		Lookup lkp = Lookup.getDefault();
		Object[] rst = lkp.lookupAll(AbstractURIHandler.class).toArray();
		for (Object tmp : rst) {
			try {
				AbstractURIHandler ua = (AbstractURIHandler) tmp.getClass().newInstance();
				uas.put(ua.get_default_schema(), ua);
			} catch (Exception e) {
				log.error(e.getLocalizedMessage());
				e.printStackTrace();
			}
		}
	}

	public static void replace_ua_class(String name, Class my_class) {
		uas0.put(name, my_class);
	}

	public static void restore_ua_class(String name) {
		uas0.remove(name);
	}

	public static void clear_all_replaced_ua_class() {
		uas0.clear();
	}
	
	public static <T extends AbstractURIHandler> T createUA(String connStr) {
		return createUA(connStr, null);
	}

	public static <T extends AbstractURIHandler> T createUA(String connStr, Class cls) {
		log.debug("create ua: uri={} class= {}",connStr,cls);
		try {
			String scheme = connStr.split(":")[0];
			if (cls != null || (cls = uas0.get(scheme)) != null
					|| (cls = uas.get(scheme).getClass()) != null)			
			    return ((T)cls.newInstance()).build(AbstractURIHandler.UA_URI,connStr);
			else 
			    throw new RunFailureException("not find ua: "+connStr+" "+cls);
			
		} catch (Exception e) {
			throw new RunFailureException(e);
		}		
	}
}
