package ate.ua;

import ate.util.X2X0;
import io.pkts.buffer.Buffer;
import io.pkts.buffer.Buffers;
import io.pkts.frame.PcapRecordHeader;
import io.pkts.packet.MACPacket;
import io.pkts.packet.PCapPacket;
import io.pkts.packet.PacketFactory;
import io.pkts.packet.TCPPacket;
import io.pkts.packet.UDPPacket;
import io.pkts.packet.impl.IPPacketImpl;
import io.pkts.packet.impl.MACPacketImpl;
import io.pkts.packet.impl.PCapPacketImpl;
import io.pkts.packet.impl.TcpPacketImpl;
import io.pkts.packet.impl.TransportPacketFactoryImpl;
import io.pkts.protocol.Protocol;

public class GeneralPacketFactory {
    static TransportPacketFactoryImpl udpfactory;
    /**
     * Raw Ethernet II frame with a source and destination mac address of
     * 00:00:00:00:00:00 and the type is set to IP (0800 - the last two bytes).
     */
    private static final byte[] ehternetII = new byte[] {
            (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
            (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x08, (byte) 0x00 };

    /**
     * Raw IPv4 frame with source and destination IP:s set to 127.0.0.1 and a
     * protocol for TCP. The length and checksums must be corrected when
     * generating a new packet based on this template.
     */
    private static final byte[] ipv4 = new byte[] {
            (byte) 0x45, (byte) 0x00, (byte) 0x01, (byte) 0xed, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
            (byte) 0x40, (byte) 0x06, (byte) 0x3a, (byte) 0xfe, (byte) 0x7f, (byte) 0x00, (byte) 0x00, (byte) 0x01,
            (byte) 0x7f, (byte) 0x00, (byte) 0x00, (byte) 0x01 };

    /**
     * Raw tcp PSH frame where the source port is 36635 and the destination port
     * 139. You will certainly have to change the length of the tcp frame based
     * on your payload but you also need to re-calculate the checksum. And you
     * probably want to
     */
    private static final byte[] tcph = new byte[] {
            (byte)0x8f,(byte)0x1b,(byte)0x00,(byte)0x8b,(byte)0xfc,(byte)0x9e,(byte)0xb4,(byte)0x52,
            (byte)0xe0,(byte)0xdd,(byte)0x33,(byte)0x8d,(byte)0x80,(byte)0x18,(byte)0x01,(byte)0xc9,
            (byte)0x4f,(byte)0x8c,(byte)0x00,(byte)0x00 };
    private static final byte[] tcpopt = new byte[] {
        (byte)0x01,(byte)0x01,(byte)0x08,(byte)0x0a,
        (byte)0x16,(byte)0xef,(byte)0xea,(byte)0xea,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00 };

    /**
     * The total size of an empty UDP packet.
     */
    private static final int tcpLength = ehternetII.length + ipv4.length + tcph.length+tcpopt.length;
    
    public static TCPPacket create_tcp(final String srcAddr, final int srcPort,
            final String destAddr, final int destPort, final Buffer payload){        
        byte[] srcAddress=X2X0.ip2b(srcAddr);
        byte[] destAddress=X2X0.ip2b(destAddr);
        
        final long ts = System.currentTimeMillis();
        final int payloadSize = payload != null ? payload.getReadableBytes() : 0;
        final Buffer ethernet = Buffers.wrapAndClone(ehternetII);
        final Buffer ipv4buf = Buffers.wrapAndClone(ipv4);

        final PcapRecordHeader pcapRecordHeader = PcapRecordHeader.createDefaultHeader(ts);
        pcapRecordHeader.setCapturedLength(tcpLength + payloadSize);
        pcapRecordHeader.setTotalLength(tcpLength + payloadSize);

        final PCapPacket pkt = new PCapPacketImpl(pcapRecordHeader, null);
        final MACPacket mac = MACPacketImpl.create(pkt, ethernet);

        final IPPacketImpl ipPacket = new IPPacketImpl(mac, ipv4buf, 0, null);
        ipPacket.setTotalLength(ipv4buf.getReadableBytes());

        final TcpPacketImpl tcp = new TcpPacketImpl(ipPacket, Buffers.wrap(tcph), Buffers.wrap(tcpopt),payload);
        
        tcp.setSourceIP(srcAddress[0], srcAddress[1], srcAddress[2], srcAddress[3]);
        tcp.setDestinationIP(destAddress[0], destAddress[1], destAddress[2], destAddress[3]);
        tcp.setDestinationPort(destPort);
        tcp.setSourcePort(srcPort);        
        tcp.reCalculateChecksum();        
        return tcp;
    }
    
    public static UDPPacket create_udp(final String srcAddr, final int srcPort,
            final String destAddr, final int destPort, final Buffer payload){        
        byte[] srcAddress=X2X0.ip2b(srcAddr);
        byte[] destAddress=X2X0.ip2b(destAddr);
        if(udpfactory==null)
            udpfactory=new TransportPacketFactoryImpl(PacketFactory.getInstance());
        
        try {
            return (UDPPacket)udpfactory.create(Protocol.UDP, srcAddress, srcPort, destAddress, destPort, payload);
        } catch (Exception e) {
            throw new RunFailureException(e);
        }
        
    }
}
