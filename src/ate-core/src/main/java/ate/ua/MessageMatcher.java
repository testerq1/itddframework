package ate.ua;

import java.util.HashMap;

public interface MessageMatcher<T> {    
    boolean match(T msg);
}
