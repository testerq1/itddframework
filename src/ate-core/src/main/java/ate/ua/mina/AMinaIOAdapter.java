package ate.ua.mina;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Collection;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.service.IoService;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.util.ExpirationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.Global;
import ate.ua.AtePacket;
import ate.ua.IUA;

/**
 * Mina的适配器
 * 
 * @author xiaoyong.huang
 * 
 */
public abstract class AMinaIOAdapter<T> extends IoHandlerAdapter implements IUA {
    protected class DefaultExpirationListener implements
            ExpirationListener<IoSession> {
        @Override
        public void expired(IoSession expiredSession) {
            expiredSession.close(true);// 关闭超时的会话
        }
    }

    protected static Logger log = LoggerFactory.getLogger(AMinaIOAdapter.class);

    protected boolean is_record = true;

    protected AMinaUAImp<T> ua;

    protected IoService service;

    protected int RETRY_TIMES = Global.RETRY_TIMES;

    AMinaIOAdapter(AMinaUAImp csUA) {
        this.ua = csUA;
        ua.set_io_adapter(this);
    }

    public Collection<IoSession> all_session() {
        return service.getManagedSessions().values();
    }

    public void enable_record(boolean b) {
        this.is_record = b;
    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause)
            throws Exception {
        cause.printStackTrace();
        log.debug(session + "  " + cause.getMessage());
        // cause.printStackTrace();
    }

    /**
     * 子类实现， 如果是client，返回local address<br>
     * 如果是server，返回remote address
     * 
     * @param session
     * @return
     */
    abstract public String getSessionKey(IoSession session);

    public boolean is_ready() {
        return service != null && service.isActive();
    }

    @Override
    /**
     * 对自动响应包是不会记录的，如果需要记录，可在ua中自己实现
     */
    public void messageReceived(IoSession session, Object message)
            throws Exception {
        if (Global.verbose)
            log.debug("{} record: {}; fuzzy:{}", this, is_record, ua.is_fuzzy());

        if (!ua.is_fuzzy()) {
            T resp = ua.process_request(session, (T) message);
            if (resp != null) {
                log.debug(this + " auto response:");
                session.write(resp);
                return;
            }
        }

        ua.rcvd();

        if (is_record) {
            if (Global.verbose)
                log.debug("{} recv packet {}", this, message);
            ua.add_packet(new AtePacket(session, message));
        }
    }

    @Override
    public void sessionCreated(IoSession session) throws Exception {
        log.debug("sessionCreated:{} {}", this, session.getId());
        // 当前session只有一个
        ua.add_session(session);
    }

    // public void addSSLFilter() throws GeneralSecurityException {
    // SSLContext sslContext = BogusSslContextFactory.getInstance(false);
    // SslFilter sslFilter = new SslFilter(sslContext);
    // sslFilter.setUseClientMode(true);
    // chain.addFirst("sslFilter", sslFilter);
    // }
}
