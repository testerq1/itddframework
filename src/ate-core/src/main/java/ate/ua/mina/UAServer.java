package ate.ua.mina;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.session.ExpiringSessionRecycler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.executor.ExecutorFilter;
import org.apache.mina.transport.socket.DatagramSessionConfig;
import org.apache.mina.transport.socket.SocketSessionConfig;
import org.apache.mina.transport.socket.nio.NioDatagramAcceptor;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

/**
 * 由于send_message用的是最新的一个session，如果存在多个session时，会乱套，
 * 因此当需要支持一个server连接多个client时，需要修改一下session的管理，
 * 
 * @author ravi huang
 * 
 */
public class UAServer<T> extends AMinaIOAdapter<T> {
    private ExecutorService executors;
    private ExecutorFilter executorFilter;

    public UAServer(AMinaUAImp<T> csUA) {
        super(csUA);
    }

    @Override
    public String getSessionKey(IoSession session) {
        if (session.getRemoteAddress() == null)
            return "";
        return session.getRemoteAddress().toString();
    }

    public void startup() {
        log.debug("{} startup", this);
        if (service != null) {
            teardown();
            return;
        }
        // shutdown的时候会抛出一些异常，暂时屏蔽掉 20130912
//        ExceptionMonitor.setInstance(new ExceptionMonitor() {
//            @Override
//            public void exceptionCaught(Throwable cause) {
//                log.debug(cause.getLocalizedMessage());
//                cause.printStackTrace();
//            }
//        });

        service = this.create_acceptor(ua.get_transport());
        service.setFilterChainBuilder(ua.get_filters());
        service.setHandler(this);
        service.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 0);
        ((IoAcceptor) service).setCloseOnDeactivation(true);

//        executors = Executors.newCachedThreadPool();
//        executorFilter = new ExecutorFilter(executors);
//        service.getFilterChain().addLast("exceutor", executorFilter);

        try {
            ((IoAcceptor) service).bind(ua.getTargetAddress());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void close_all_sessions() {
        if (service != null){
            for (IoSession current_session : service.getManagedSessions().values()) {
                if (current_session.isClosing() || !current_session.isConnected())
                    continue;
                current_session.close(true);
                current_session.getCloseFuture().awaitUninterruptibly(1000);
            }
        } 
    }
    
    @Override
    public void teardown() {
        log.debug("{} shutdown", this);
        if (service != null){
            for (IoSession current_session : service.getManagedSessions().values()) {
                //启用ssl时break，临时规避挂起问题
//                if(current_session.isSecured())
//                    break;
                
                if (current_session.isClosing() || !current_session.isConnected())
                    continue;
                current_session.close(true);
                current_session.getCloseFuture().awaitUninterruptibly(1000);
            }
        } 
//        ((NioSocketAcceptor) service).getSessionConfig();
        
        service.dispose();
//        executorFilter.destroy();
//        executors.shutdownNow();
        ua.clear_q();
    }

    @Override
    public String toString() {
        return "UAServer";
    }

    protected IoAcceptor create_acceptor(String transport) {
        IoAcceptor acceptor = null;
        if (transport.equalsIgnoreCase("udp")) {
            acceptor = new NioDatagramAcceptor();
            DatagramSessionConfig dcfg = ((NioDatagramAcceptor) acceptor)
                    .getSessionConfig();
            dcfg.setReuseAddress(true);
            dcfg.setReceiveBufferSize(64 * 1024);

            // log.info("getBothIdleTimeInMillis "+dcfg.getBothIdleTimeInMillis());
            // log.info("getReaderIdleTime "+dcfg.getReaderIdleTime());
            dcfg.setBroadcast(true);
            ExpiringSessionRecycler expirer = new ExpiringSessionRecycler(0);
            ((NioDatagramAcceptor) acceptor).setSessionRecycler(expirer);

        } else if (transport.equalsIgnoreCase("tcp")) {
            acceptor = new NioSocketAcceptor(1);
            SocketSessionConfig scfg = ((NioSocketAcceptor) acceptor)
                    .getSessionConfig();
            scfg.setSoLinger(0);
            // executors=Executors.newCachedThreadPool();
            // executorFilter=new ExecutorFilter(executors);
            // acceptor.getFilterChain().addLast("threadPool", new
            // ExecutorFilter(executors));
        }
        return acceptor;
    }
}
