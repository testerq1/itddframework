package ate.ua.mina;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.util.GenData0;

/**
 * 模拟网络延迟及抖动
 * 
 * @author ravi.huang
 *
 */
public class JitterIoFilter extends IoFilterAdapter {
    protected static Logger log = LoggerFactory.getLogger(JitterIoFilter.class);
    /** milliseconds */
    private int min = 0;
    /** milliseconds */
    private int max = 10;

    public JitterIoFilter() {
    }

    /**
     * jitter的范围，单位为毫秒
     * 
     * @param min
     * @param max
     */
    public JitterIoFilter(int min, int max) {
        this.max = max;
        this.min = min;
    }
    
    @Override
    public void filterWrite(NextFilter nextFilter, IoSession session,
            WriteRequest writeRequest) throws Exception {
        //Object data = writeRequest.getOriginalRequest().getMessage();

        //if (data instanceof IoBuffer) {
        int rand = GenData0.random(min, max);        
        Thread.currentThread().sleep(rand);
        //}
        nextFilter.filterWrite(session, writeRequest);
    }
    //
    // @Override
    // public void filterClose(NextFilter nextFilter, IoSession session) throws
    // Exception {
    // nextFilter.filterClose(session);
    // }
    //
    // @Override
    // public void inputClosed(NextFilter nextFilter, IoSession session) throws
    // Exception {
    // nextFilter.inputClosed(session);
    // }

    public void set_max(int max) {
        this.min = min;
    }

    public void set_min(int min) {
        this.min = min;
    }
}
