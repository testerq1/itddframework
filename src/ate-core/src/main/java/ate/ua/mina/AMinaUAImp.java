package ate.ua.mina;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.security.GeneralSecurityException;
import java.util.HashMap;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.filterchain.DefaultIoFilterChainBuilder;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.filterchain.IoFilterChain;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.ssl.SslFilter;

import ate.AbstractURIHandler;
import ate.Global;
import ate.rm.dev.Host;
import ate.ua.AbstractPacketUAImp;
import ate.ua.AtePacket;
import ate.ua.IAutoResponseMessage;
import ate.ua.Reconnectable;
import ate.ua.RunFailureException;
import ate.ua.UAOption;
import ate.ua.ssl.BogusSslContextFactory;

/**
 * 采用Mina Socket的UA的父类 windows:设置TCP time_wait reg add
 * "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\Tcpip\Parameters" /v
 * "TcpTimedWaitDelay" /t REG_DWORD /d 30 /f
 * 
 * @author xiaoyong.huang
 * 
 */
public abstract class AMinaUAImp<T> extends
        AbstractPacketUAImp<T, AtePacket<IoSession>> implements Reconnectable {
    public enum FILTER {
        Jitter, StringResponse
    }

    public static String conf_path = Host.HOME + File.pathSeparator + "conf"
            + File.separator;

    public static AMinaUAImp create_simple_ua(String connStr) {
        AMinaUAImp ua = new AMinaUAImp<Object>() {
            @Override
            public void add_default_filter() {

            }

            @Override
            public String get_default_schema() {
                return "mina";
            }

            @Override
            public boolean message_matched(Object msg,
                    HashMap<String, Object> hm) {
                return true;
            }

        };
        ua.build(UA_URI, connStr);
        return ua;
    }

    protected IoSession current_session;

    private AMinaIOAdapter<T> io;

    SslFilter sslFilter;

    /** 记录缺省的iofilters,以便在时使用 */
    private DefaultIoFilterChainBuilder filters = new DefaultIoFilterChainBuilder();
    
    public void add_after_iofilter(String baseName,String name, IoFilter filter) {
        filters.addAfter(baseName, name, filter);
        if (io != null) {
            for (IoSession session : io.all_session()) {
                session.getFilterChain().addAfter(baseName,name, filter);
            }
        }
    }
    
    public void add_before_iofilter(String baseName,String name, IoFilter filter) {
        filters.addBefore(baseName, name, filter);
        if (io != null) {
            for (IoSession session : io.all_session()) {
                session.getFilterChain().addBefore(baseName,name, filter);
            }
        }
    }
    
    public void add_first_iofilter(String name, IoFilter filter) {
        filters.addFirst(name, filter);
        if (io != null) {
            for (IoSession session : io.all_session()) {
                session.getFilterChain().addFirst(name, filter);
            }
        }
    }
    
    /**
     * add IoFilter to last of every session
     * 
     * @param name
     * @param filter
     * @see #sessions
     */
    public void add_last_iofilter(String name, IoFilter filter) {
        log.debug("{} add {} {}",this,name,filter);
        filters.addLast(name, filter);
        if (io != null) {
            for (IoSession session : io.all_session()) {
                session.getFilterChain().addLast(name, filter);
            }
        }
    }

    /**
     * @see AMinaIOAdapter#sessionOpened(IoSession)
     * @param session
     */
    public void add_session(IoSession session) {
        this.current_session = session;
    }

    public void broadcast(IoSession session, Object msg, String address) {
        log.debug("{} broadcast {} to {}", new Object[] { this, msg, address });

        if (!get_transport().equalsIgnoreCase("udp"))
            throw new Error("can't broadcast on no udp transport!");

        while (session == null) {
            log.warn("session is null");
            sleep(1000);
        }

        if (!session.isConnected())
            throw new Error(session + " disconnected!");

        session.write(msg, parse_address(address));
    }

    public void broadcast(Object msg, String address) {
        broadcast(current_session, msg, address);
    }

    @Override
    public AbstractURIHandler build(UAOption option, Object value) {
        super.build(option, value);

        if (option == UA_URI) {
            if (path != null && path.length() > 3)
                transport = path.substring(1);
            // if (this instanceof IAutoResponseMessage)
            // register_auto_response("0", (IAutoResponseMessage) this);
        }
        return this;
    }

    public void clear_iofilters() throws Exception {
        filters.clear();
        for (IoSession session : io.all_session())
            session.getFilterChain().clear();
    }

    public IoFilter create_io_filter(FILTER type) {
        switch (type) {
        case Jitter:
            return new JitterIoFilter();
        case StringResponse:
            return new JitterIoFilter();
        }
        return null;
    }

    public void enable_auto_reconnect(boolean b) {
        if (io instanceof UAClient)
            ((UAClient) io).enable_auto_reconnect(b);
    }

    public DefaultIoFilterChainBuilder get_default_filters() {
        return filters;
    }

    public DefaultIoFilterChainBuilder get_filters() {
        return filters;
    }

    public AMinaIOAdapter get_io_handler() {
        return io;
    }

    @Override
    /**
     * 重载父类的方法，由于mina io封装了实际收到的包，因此这里解封装
     */
    public Object get_message() {
        if (Global.verbose)
            log.debug("{} get_message", this);
        Object o = super.get_message0();
        if (o == null)
            return null;

        if (o instanceof AtePacket) {
            this.current_session = ((AtePacket<IoSession>) o).getSession();
            return ((AtePacket) o).getPacket();
        } else
            return o;
    }

    /**
     * 如若需要动态改变某个session的Filter，用mina的API
     * 
     * @param session
     * @return
     * @see org.apache.mina.core.filterchain.IoFilterChain
     */
    public IoFilterChain getFilterChain(IoSession session) {
        return session.getFilterChain();
    }

    @Override
    public boolean is_client() {
        return io instanceof UAClient;
    }

    @Override
    public boolean is_ready() {
        return io != null && io.is_ready();
    }

    public boolean is_ssl_enabled(){
        return sslFilter!=null&&sslFilter.isSslStarted(this.current_session);
    }

    @Override
    /**
     * close server的所有session
     * reconnect所有client
     */
    public void reconnect() {
        if (io instanceof UAServer)
            ((UAServer) io).close_all_sessions();
        else
            ((UAClient) io).reconnect();
    }

    /**
     * 删除Session时会从每个active的session中都删除<br>
     * 暂时不提供按照session操作的IoFilter更新，如果需要这个功能，可以调用getFilterChain后按照 mina的API来做
     * 
     * @param name
     * @see #sessions
     * @see #getFilterChain(IoSession)
     */
    public void remove_iofilter(String name) {
        filters.remove(name);
        if (io != null) {
            for (IoSession session : io.all_session()) {
                session.getFilterChain().remove(name);
            }
        }
    }

    public void replace_iofilter(String name, IoFilter filter) {
        filters.replace(name, filter);
        if (io != null) {
            for (IoSession session : io.all_session())
                session.getFilterChain().replace(name, filter);
        }
    }

    public void send_buffer(IoBuffer buffer) {
        while (current_session == null) {// ||!session.isConnected()
            log.warn("current session is not active {}", this);
            sleep(1000);
        }
        current_session.write(buffer);
    }

    public void send_bytes(byte[] bs) {
        while (current_session == null) {// ||!session.isConnected()
            log.warn("current session is not active {}", this);
            sleep(1000);
        }
        current_session.write(bs);
    }

    public void send_hexstring(IoSession session, String s) {
        IoBuffer buffer = null;
        try {
            buffer = IoBuffer.wrap((byte[]) new Hex().decode(s));
        } catch (DecoderException e) {
            e.printStackTrace();
        }
        session.write(buffer);
    }

    public void send_hexstring(String s) {
        while (current_session == null) {// ||!session.isConnected()
            log.warn("current session is not active {}", this);
            sleep(1000);
        }
        send_hexstring(current_session, s);
    }

    public void send_message(IoSession session, T msg) {
        session.write(msg);
    }

    @Override
    public void send_message(T msg) {
        log.debug("{} send_message {}", this, msg);
        while (current_session == null) {// ||!session.isConnected()
            log.warn("current session is not active {}", this);
            sleep(1000);
        }
        current_session.write(msg);
    }

    public void send_string(IoSession session, String msg) {
        session.write(msg);
    }

    public void send_string(String s) {
        while (current_session == null) {// ||!session.isConnected()
            log.warn("current session is not active {}", this);
            sleep(1000);
        }
        current_session.write(s);
    }

    public void set_dump_file(String file) {
        PcapDumperFilter iofilter = new PcapDumperFilter(this.get_transport(),
                file);
        this.add_first_iofilter("pcapdump", iofilter);
    }

    public void set_io_adapter(AMinaIOAdapter io) {
        this.io = io;
    }

    @Override
    public void set_is_record(boolean b) {
        super.set_is_record(b);
        io.enable_record(b);
    }
    
    @Override
    public void startup() {
        this.add_default_filter();
        this.io = create_io(type);
        io.enable_record(is_record);
        this.io.startup();
    }
    
    public void stopssl(){
        if(sslFilter==null)
            return;
        for(IoSession session:io.all_session()){
            try {
                sslFilter.stopSsl(session).awaitUninterruptibly();
            } catch (SSLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            while(sslFilter.isSslStarted(session))
                sleep(500);            
        }
    }
    
    @Override
    public void teardown() {    
        this.filters.clear();
        this.io.teardown();
    }
    
    public AMinaUAImp use_bogus_ssl() {
        System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");

        try {
            SSLContext ctx = BogusSslContextFactory.getInstance(type == SERVER);
            sslFilter = new SslFilter(ctx);
            sslFilter.setUseClientMode(type == CLIENT);
            this.add_first_iofilter("SSL", sslFilter);
            return this;
        } catch (GeneralSecurityException e) {
            throw new RunFailureException(e);
        }
    }

    protected abstract void add_default_filter();

    protected AMinaIOAdapter<T> create_io(int type) {
        if (type == SERVER)
            return new UAServer(this);
        else
            return new UAClient(this);
    }

    /**
     * 便利消息处理器，返回得到第一个响应 消息自动处理器采用TreeMap，以key排序 这里应该是个final方法，暂时为public，以便兼容脚本
     * 
     * @param session
     * @param req
     * @return
     * @see setAutoResponse()
     */
    T process_request(IoSession session, T req) {
        for (String key : responseGenerator.keySet()) {
            IAutoResponseMessage<IoSession, T> tmp = responseGenerator.get(key);
            T resp = tmp.get_auto_resp_msg(session, req);
            if (resp != null)
                return resp;
        }
        return null;
    }

}
