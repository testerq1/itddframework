package ate.ua.mina;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import ate.Global;
import ate.ua.AbstractCustomPacket;
import ate.ua.DecoderState;
import ate.ua.RunFailureException;
import ate.util.X2X0;

public abstract class AbstractCustomUA<T extends AbstractCustomPacket> extends
        AMinaUAImp<T> {
    class UADecoder extends CumulativeProtocolDecoder {
        @Override
        protected boolean doDecode(IoSession session, IoBuffer msg,
                ProtocolDecoderOutput out) throws Exception {

            if (msg.remaining() <= 0) {
                return false;
            }

            if (Global.verbose)
                log.debug("doDecode {} {}", this, msg.remaining());

            if (AbstractCustomUA.this.is_fuzzy()) {
                out.write(msg);
                return true;
            }

            DecoderState state = (DecoderState) session
                    .getAttribute(DECODER_STATE_ATT);
            if (null == state) {
                session.setAttribute(DECODER_STATE_ATT, DecoderState.NEW);
                state = (DecoderState) session.getAttribute(DECODER_STATE_ATT);
            }
            switch (state) {
            case HEAD:
                log.debug("decoding HEAD");
                // grab the stored a partial HEAD request
                final ByteBuffer oldBuffer = (ByteBuffer) session
                        .getAttribute(PARTIAL_HEAD_ATT);
                log.info("oldBuffer:" + oldBuffer.capacity() + "");
                log.info("msg:" + msg.capacity() + "");
                // concat the old buffer and the new incoming one
                msg = IoBuffer
                        .allocate(oldBuffer.remaining() + msg.remaining())
                        .put(oldBuffer).put(msg).flip();
                // now let's decode like it was a new message
            case NEW:
                log.debug("decoding NEW:{} ", msg.remaining());
                final T rq = decodePacket(msg);

                if (rq == null) {
                    // we copy the incoming BB because it's going to be recycled
                    // by the inner IoProcessor for next reads
                    msg.position(0);
                    final ByteBuffer partial = ByteBuffer.allocate(msg
                            .remaining());
                    partial.put(msg.buf());
                    partial.flip();
                    msg.position(msg.capacity());
                    log.debug("set partial:{} ",
                            Arrays.toString(partial.array()));
                    // no request decoded, we accumulate
                    session.setAttribute(PARTIAL_HEAD_ATT, partial);
                    session.setAttribute(DECODER_STATE_ATT, DecoderState.HEAD);
                } else {

                    out.write(rq);
                    session.removeAttribute(DECODER_STATE_ATT);
                    session.removeAttribute(PARTIAL_HEAD_ATT);
                    return true;
                }
                break;

            default:
                throw new RunFailureException("Unknonwn decoder state : "
                        + state);
            }

            // if (in.remaining() >= 0) {
            // int len=in.remaining();
            //
            // log.info("decode:"+Arrays.toString(in.array()));
            // T pkt=decodePacket(in);
            // // if(pkt==null){
            // // in.position(0);
            // // log.info(in.remaining()+"");
            // // return false;
            // // }
            // out.write(pkt);
            // return true;
            // }
            return false;
        }
    }
    class UAEncoder extends ProtocolEncoderAdapter {
        @Override
        public void encode(IoSession session, Object message,
                ProtocolEncoderOutput out) throws Exception {
            byte[] bs = null;
            if (message instanceof AbstractCustomPacket) {
                T pkt = (T) message;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                pkt.encodePacket(bos);
                bs = bos.toByteArray();
            } else if (message instanceof byte[]) {
                bs = (byte[]) message;
            } else if (message instanceof String) {
                bs = X2X0.hexs2b("" + message);
            }
            log.debug("encode:{} {}", bs.length, Arrays.toString(bs));
            out.write(IoBuffer.wrap(bs));
        }
    }

    protected boolean needDecode = true;

    /** Key for decoder current state */
    private static final String DECODER_STATE_ATT = "pkt.ds";

    /** Key for the partial requests head */
    private static final String PARTIAL_HEAD_ATT = "pkt.ph";

    /** Key for the number of bytes remaining to read for completing the body */
    private static final String BODY_REMAINING_BYTES = "pkt.brb";

    public abstract T decodePacket(IoBuffer bs);

    @Override
    public boolean message_matched(T msg, HashMap<String, Object> hm) {
        return true;
    }

    @Override
    protected void add_default_filter() {
        add_last_iofilter(this.scheme, new NoExceptionProtocolCodecFilter(
                new UAEncoder(), new UADecoder()));
    }

}
