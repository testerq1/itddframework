package ate.ua.mina;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 没有完成，先放着
 * 
 * @author Administrator
 *
 */
public class StringResponseIoFilter extends IoFilterAdapter {
    protected static Logger log = LoggerFactory
            .getLogger(StringResponseIoFilter.class);
    public final static int UDP = 1;
    public final static int TCP = 2;
    public static String ioBufferToString(IoBuffer iobuffer) {
        byte[] b = new byte[iobuffer.limit()];
        iobuffer.get(b);
        System.arraycopy(iobuffer.array(), 0, b, 0, b.length);
        return new String(b);
    }
    private Object resp;

    private int type;

    public StringResponseIoFilter() {
    }

    public StringResponseIoFilter(int type) {
        this.type = type;
    }

    @Override
    public void messageReceived(NextFilter nextFilter, IoSession session,
            Object message) throws Exception {
        IoBuffer response = getResponseByRequest(session, message);
        if (response != null) {
            session.write(response);
            log.debug(this.ioBufferToString(response));
        } else
            log.warn("no response set to autoresponse filter!");

        nextFilter.messageReceived(session, message);
    }

    public void set_type(int type) {
        this.type = type;
    }

    public void setHexResponse(String msg) {
        try {
            this.resp = new Hex().decode(msg);
        } catch (DecoderException e) {
            e.printStackTrace();
        }
    }

    public void setResponse(Object msg) {
        this.resp = msg;
    }

    private IoBuffer getResponseByRequest(IoSession session, Object message) {
        if (type == UDP) {
            String s = session.getService().getTransportMetadata().getName();
            String tmp = "";
            if (s.equalsIgnoreCase("datagram"))
                tmp = "udp";
            else if (s.equalsIgnoreCase("socket"))
                tmp = "tcp";
            tmp = tmp + " from:" + session.getLocalAddress() + " to:"
                    + session.getRemoteAddress();
            return IoBuffer.wrap(tmp.getBytes());
        }
        if (resp == null)
            return null;
        if (resp instanceof String)
            return IoBuffer.wrap(((String) resp).getBytes());
        if (resp instanceof byte[])
            return IoBuffer.wrap((byte[]) resp);
        if (resp instanceof IoBuffer)
            return (IoBuffer) resp;
        return null;
    }
}
