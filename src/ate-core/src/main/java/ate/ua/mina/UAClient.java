package ate.ua.mina;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.net.SocketAddress;

import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.socket.DatagramSessionConfig;
import org.apache.mina.transport.socket.SocketSessionConfig;
import org.apache.mina.transport.socket.nio.NioDatagramConnector;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

/**
 * 
 * @author ravi huang
 * 
 */
public class UAClient<T> extends AMinaIOAdapter<T> {

    /** The future. */
    private boolean needReconnect = false;

    /**
     * Instantiates a new uA client.
     * 
     * @param ua
     *            the ua
     */
    public UAClient(AMinaUAImp ua) {
        super(ua);
    }

    /**
     * Enable_auto_reconnect.
     * 
     * @param b
     *            the b
     */
    public void enable_auto_reconnect(boolean b) {
        this.needReconnect = b;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * ate.ua.mina.AMinaIOAdapter#getSessionKey(org.apache.mina.core.session
     * .IoSession)
     */
    @Override
    public String getSessionKey(IoSession session) {
        return session.getLocalAddress().toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see ate.ua.IUA#is_ready()
     */
    public synchronized boolean is_ready() {
        return super.is_ready() && service.getManagedSessionCount() > 0;
    }

    /**
     * Need_auto_reconnect.
     * 
     * @return true, if successful
     */
    public boolean need_auto_reconnect() {
        return needReconnect;
    }

    public IoSession new_session() {
        return new_session(null);
    }

    public IoSession new_session(SocketAddress localaddr) {
        int retry = 0;
        ConnectFuture future = null;
        while ((future == null || !future.isConnected())
                && retry++ < RETRY_TIMES) {
            if (localaddr != null) {
                future = ((IoConnector) service).connect(ua.getTargetAddress(),
                        localaddr);
            } else {
                future = ((IoConnector) service).connect(ua.getTargetAddress());
            }
            if (future.getException() != null)
                future.getException().printStackTrace();

            future.awaitUninterruptibly();

            if (!future.isConnected()) {
                log.warn("connect failure,address: {}, retry {}", ua.get_uri(),
                        retry);
                continue;
            } else {
                log.debug("connect successful,address: {}, retry {}",
                        ua.get_uri(), retry);
                return future.getSession();
            }
        }
        return null;
    }

    /**
     * Reconnect.
     */
    public void reconnect() {
        log.debug("{} reconnecting", this);
        boolean b=needReconnect;
        needReconnect=false;        
        this.teardown();        
        this.startup();
        needReconnect=b;
    }

    /**
     * 自动重连.
     * 
     * @param session
     *            the session
     * @throws Exception
     *             the exception
     */
    @Override
    public void sessionClosed(IoSession session) throws Exception {
        super.sessionClosed(session);
        if (!need_auto_reconnect())
            return;

        log.debug("sessionClosed reconnect:" + session);
        while (is_ready()) {
            try {
                Thread.currentThread().sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        startup();
        // enableAutoReconnect(true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see ate.ua.IUA#startup()
     */
    public void startup() {
        log.debug("{} startup", this);
        if (service == null || !service.isActive()) {
            service = this.create_connector(ua.get_transport());
            service.setHandler(this);
            service.setFilterChainBuilder(ua.get_filters());
            service.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 0);
        }
        new_session(ua.getLocalBindAddress());
    }

    /*
     * (non-Javadoc)
     * 
     * @see ate.ua.mina.AMinaIOAdapter#teardown()
     */
    public void teardown() {
        log.debug("{} shutdown", this);
        this.needReconnect=false;
        if (service != null){
            for (IoSession current_session : service.getManagedSessions().values()) {
                if (current_session.isClosing() || !current_session.isConnected())
                    continue;
                current_session.close(false);
                current_session.getCloseFuture().awaitUninterruptibly(1000);
            }
            service.dispose();
            service=null;
        }        
        ua.clear_q();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UAClient";
    }

    protected IoConnector create_connector(String transport) {
        IoConnector connector = null;
        if (transport.equalsIgnoreCase("udp")) {
            connector = new NioDatagramConnector();
            DatagramSessionConfig dcfg = ((NioDatagramConnector) connector)
                    .getSessionConfig();
            dcfg.setBroadcast(true);
            dcfg.setReuseAddress(true);
            dcfg.setReceiveBufferSize(64 * 1024);

        } else if (transport.equalsIgnoreCase("tcp")) {
            connector = new NioSocketConnector(1);
            SocketSessionConfig scfg = ((NioSocketConnector) connector)
                    .getSessionConfig();
            scfg.setSoLinger(0);
            scfg.setReuseAddress(true);
        }
        return connector;
    }
}
