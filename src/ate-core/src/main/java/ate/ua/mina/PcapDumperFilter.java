package ate.ua.mina;

import java.util.Arrays;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.ua.PcapDumper;

public class PcapDumperFilter extends IoFilterAdapter {
    protected static Logger log = LoggerFactory.getLogger(PcapDumper.class);
    PcapDumper dumper;

    public PcapDumperFilter(String transport, String fname) {
        dumper = new PcapDumper(transport);
        dumper.set_dump_file(fname);
    }

    @Override
    public void filterWrite(NextFilter nextFilter, IoSession session,
            WriteRequest writeRequest) throws Exception {
        Object data = writeRequest.getOriginalRequest().getMessage();

        if (data instanceof IoBuffer) {
            IoBuffer buf = (IoBuffer) data;
            byte[] bs = buf.array();
            dumper.dump_to_pcap(session.getLocalAddress(),
                    session.getRemoteAddress(),
                    Arrays.copyOf(bs, buf.remaining()));
        }
        nextFilter.filterWrite(session, writeRequest);
    }

    @Override
    public void messageReceived(NextFilter nextFilter, IoSession session,
            Object message) throws Exception {
        if (message instanceof IoBuffer) {
            IoBuffer buf = (IoBuffer) message;
            byte[] bs = buf.array();
            dumper.dump_to_pcap(session.getRemoteAddress(),
                    session.getLocalAddress(),
                    Arrays.copyOf(bs, buf.remaining()));
        }
        nextFilter.messageReceived(session, message);
    }
}
