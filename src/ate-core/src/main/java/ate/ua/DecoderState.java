package ate.ua;

public enum DecoderState {
    NEW,
    HEAD,
    BODY;
}
