package ate.ua;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
 * 
 * @author Administrator
 *
 * @param <S>
 * @param <M>
 */
public interface IAutoResponseMessage<S,M> {
	/**
	 * 收到消息时调用该方法得到自动响应
	 * @param ua 当前UA
	 * @param session 当前session
	 * @param msg 当前消息
	 * @return
	 */
	M get_auto_resp_msg(S session, M msg);	

}
