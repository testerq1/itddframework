package ate;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import groovy.lang.GroovyClassLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.rm.dev.Host;

public class TTClassLoader extends GroovyClassLoader {
	static Logger log = LoggerFactory.getLogger(TTClassLoader.class);

	private class Scripthandler {
		long lastModify;
		Class parsedCls;
		String path;
	}

	TreeMap<String, Scripthandler> cls2File = new TreeMap<String, Scripthandler>();
	TreeMap<String, Scripthandler> path2Cls = new TreeMap<String, Scripthandler>();

	String scriptPath;
	
	public TTClassLoader() {
		if (System.getProperty("TTOOL_SCRIPT_HOME") == null) {
			scriptPath = Host.HOME + File.separator + "Scripts";
			System.setProperty("TTOOL_SCRIPT_HOME", scriptPath);
		}else
			scriptPath=System.getProperty("TTOOL_SCRIPT_HOME");
		log.debug("add script classpath: {}",scriptPath);
		addClasspath(scriptPath);
		// this.addClasspath(System.getProperty("TTOOL_SCRIPT_HOME")+File.separator+"common");
		// this.addClasspath(System.getProperty("TTOOL_SCRIPT_HOME")+File.separator+"flows");
	}

	public ArrayList loadTestuite(File testcase, String testSuite) {
		ArrayList al = new ArrayList();

		log.debug("scriptPath: {}", scriptPath);

		String path = testcase.getAbsolutePath();

		if (!testcase.exists())
			return al;

		for (int i = 0; i < getClassPath().length; i++)
			log.debug("groovy class path: " + getClassPath()[i]);

		if (!path.contains(scriptPath)) {
			log.debug("path=" + path);
			log.debug("scriptPath=" + scriptPath);
			al.addAll(loadGroovyScript(testcase));
			return al;
		}

		testSuite = scriptPath + File.separator + testSuite;
		al.addAll(loadGroovyScript(new File(testSuite)));
		al.addAll(loadGroovyScript(testcase));

		return al;
	}

	public ArrayList loadGroovyScript(File file) {
		ArrayList al = new ArrayList();
		if (!file.exists())
			return al;
		if (file.isFile()) {
			if (file.getName().endsWith(".groovy") || file.getName().endsWith(".gy"))
				try {
					al.add(parseFile(file));
				} catch (Exception e) {
					al.add(e.getLocalizedMessage());
					log.warn(e.getLocalizedMessage());
					e.printStackTrace();
				}
			return al;
		}
		for (File sub : file.listFiles())
			al.addAll(loadGroovyScript(sub));
		return al;
	}

	private Class parseFile(File file) throws Exception {
		Scripthandler sh = path2Cls.get(file.getAbsolutePath());
		Class cls = null;

		if (sh != null) {
			if (sh.lastModify >= file.lastModified())
				return sh.parsedCls;

			cls = recompile(file.toURL(), sh.parsedCls.getName(), sh.parsedCls);
		} else {
			sh = new Scripthandler();
			cls = this.parseClass(file);
		}
		sh.parsedCls = cls;
		sh.lastModify = file.lastModified();
		sh.path = file.getParent();
		if (sh.path != null) {
			cls2File.put(cls.getName(), sh);
			path2Cls.put(file.getAbsolutePath(), sh);
			// TTClassLoader cl=(TTClassLoader)o;

		}
		log.debug("load successful " + file.getAbsolutePath());
		return cls;

	}

	public String getScriptPath(Class cls) {
		Scripthandler sh = cls2File.get(cls.getName());
		if (sh != null)
			return sh.path;
		return null;
	}

	public Class getScriptClass(String className) {
		Scripthandler sh = cls2File.get(className);
		if (sh != null)
			return sh.parsedCls;
		return null;
	}
}
