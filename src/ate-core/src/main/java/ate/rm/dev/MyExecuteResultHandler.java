package ate.rm.dev;

import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;

public class MyExecuteResultHandler extends DefaultExecutor{
	PumpStreamHandler stream;
	public MyExecuteResultHandler(PumpStreamHandler stream){
		super();
		this.stream=stream;
	}
	public PumpStreamHandler get_stream_handler(){
		return this.stream;
	}
}
