package ate.rm.dev;

import java.io.File;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


public class Linux extends Host {
	String stap_home;
	boolean has_stap=false;	
	
	protected Linux(){super("tools");}	
	
	@Override
	public String capabilities() {
		StringBuffer buf=new StringBuffer();
		buf.append("http_pload: "+(which("http_pload")==null?"no":"yes")).append("\r\n");
		buf.append("stap: "+(which("stap")==null?"no":"yes")).append("\r\n");		
		buf.append("sysbench: "+(which("sysbench")==null?"no":"yes")).append("\r\n");
		buf.append("cpulimit: "+(which("cpulimit")==null?"no":"yes")).append("\r\n");	
		return buf.toString();
	}
	/**
	 * which命令
	 * @param cmd
	 * @return
	 */
	public String which(String cmd) {
		String rtn=exec0("which "+cmd);			
		
		if(rtn==null||rtn.trim().length()==0||expect.expect("which:no", rtn))
			return null;
		return rtn;					
	}
	

	public boolean rm(String fname){		
		return exec0("rm -f "+fname).length()==0;		
	}
	
	public boolean rmdir(String path){
		return exec0("rm -rf "+path).length()==0;
	}	
	
	@Override
	public boolean kill(String processname) {
		log.debug(exec0("kill "+processname));
		return true;
	}

	@Override
	public String ping(String ip,String options) {		
		return exec0("ping "+options+" "+ip);
	}

}
