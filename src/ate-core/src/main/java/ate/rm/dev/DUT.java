package ate.rm.dev;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.AbstractURIHandler;
import ate.Global;
import ate.TTClassLoader;
import ate.ua.AbstractPacketUAImp;
import ate.ua.AbstractVTUAImp;
import ate.ua.ResourceFactory;
import ate.util.Expect;

/**
 * The Class DeviceImp.
 */
public abstract class DUT {

    /** The log. */
    protected static Logger log = LoggerFactory.getLogger(DUT.class);
    protected AbstractVTUAImp vt;
    protected Expect expect = new Expect();
    private Testbed tb;
    
    /* 测试床配置文件中对应的section */
    private String section;

    /**
     * vt需要手动login
     * @param connstr
     */
    public DUT(String connstr) {
        log.debug("DUT: {}", connstr);
        vt = ResourceFactory.createUA(connstr);
        tb = Testbed.createTestbed();
        vt.startup();
        if(vt.get_section()!=null){
            this.section=section; 
            vt.login();
        }
    }
    public <T extends AbstractURIHandler> T create_ua(String connStr) {
        return ResourceFactory.createUA(connStr);
    }
    
    public <T extends AbstractURIHandler> T create_ua(String uri, T ua) {        
        return ua.build(AbstractPacketUAImp.UA_URI,uri);
    }
    
    public <T extends AbstractURIHandler> T create_ua(String connStr,Class<T> cls) {
        return ResourceFactory.createUA(connStr,cls);
    }
    
    public AbstractVTUAImp get_vt() {
        return vt;
    }

    /**
     * 从testbed配置文件中取得key.如果key中带有"."则直接取，否则会加上section前缀后取
     * 
     * @param key
     * @return
     * @see #section
     */
    public String arg(String key) {
        if (key.contains("."))
            return tb.arg(key);
        return tb.arg(section + "." + key);
    }
    
    public void quit(){
        vt.teardown();
        
    }
    
    public void restart(){
        vt.restart();
    }
    
    public void replace_ua_class(String name, Class my_class) {
        ResourceFactory.replace_ua_class(name, my_class);
    }

    public void restore_ua_class(String name) {
        ResourceFactory.restore_ua_class(name);
    }
    
    public void shell(){        
        GroovyShell gsh = (GroovyShell)Testbed.getTestbed().getRes("GSH");
        gsh.getContext().setVariable("o", this);
        while (true) {
            String line;
            try {
                line = Global.console.readLine(">");
                if(line.startsWith("quit"))
                    break;                
                gsh.evaluate(line);
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void sleep(long milliseconds) {
        Global.sleep(milliseconds);
    }
}
