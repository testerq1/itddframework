package ate.rm.dev;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * windows操作系统的一组工具集
 * 
 * @author ravi huang
 * 
 */
public class Windows extends Host{
	
	protected Windows(){
		this("tools");
	}
	
	protected Windows(String toolpath) {
		super(toolpath);
		if(System.getProperty("sun.jnu.encoding").equals("GBK"))
			set_charset("GBK");		
	}
	
	public static void main(String[] args){
//		Properties pr=System.getProperties();
//		for(Object tmp:pr.keySet())
//			System.out.println(tmp+" = "+pr.get(tmp));
//		
		Windows win=new Windows();
		//System.out.println(win.execToString("dir"));
//		System.out.println(win.execToString("copy /?"));
	}

	@Override
	public String capabilities() {
		StringBuffer buf=new StringBuffer();
		buf.append("nmcap: "+(which("nmcap")==null?"no":"yes")).append("\r\n");
		
		return buf.toString();
	}	
	/**
	 * which命令
	 * @param cmd
	 * @return
	 */
	public String which(String cmd) {
		String rtn=exec0(bin()+"which.bat "+cmd);		
		
		if(rtn==null||rtn.trim().length()==0||expect.expect("which:no", rtn))
			return null;
		
		return rtn;					
	}
	
	public boolean rm(String fname){		
		return exec0("cmd.exe /C del "+fname.replace("/", "\\")).length()==0;		
	}
	
	public boolean rmdir(String path){		
		return exec0("cmd.exe /C rmdir /s /q "+path.replace("/", "\\")).length()==0;
	}

	@Override
	public boolean kill(String processname) {
		//处理跨平台可执行文件名的兼容性，只考虑了名字不带.的可执行程序
		if(!processname.contains("."))
			processname+=".exe";
		log.debug(exec0("cmd.exe /C taskkill /f /im "+processname));
		return true;
	}
	@Override
	public String ping(String ip,String options) {		
		return exec0("ping "+options+" "+ip);
	}

}
