package ate.rm.dev;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.HashMap;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.Global;
import ate.util.Expect;

/**
 * 代表本机，封装一组工具
 * 采用Runtime的方式执行，每一个命令都可以用以下3种方式中的一种指定：
 * 1，绝对路径cmd<br>
 * 2，switchTo绝对路径+cmd<br>
 * 3，缺省绝对路径+switchTo相对路径+cmd<br>
 * 
 * @author ravi huang
 *
 */

public abstract class Host implements IDevice {		
	public static String CONF_HOME;
	public static String NATIVE_HOME;
	static {
		//System.out.println("in Host static");
	    //System.setProperty("java.net.preferIPv4Stack","true");
		if (System.getProperty("TTOOL_HOME") == null)
			System.setProperty("TTOOL_HOME", System.getProperty("user.dir"));
		init();
	}
	final static boolean isWindows = System.getProperty("os.name")
			.toLowerCase().startsWith("win");
	
	public final static String HOME=System.getProperty("TTOOL_HOME");	
	protected static Logger log = LoggerFactory.getLogger(Host.class);
	
	private static String classfier;
	public static Charset charset=Global.charset;
	
	private static void init(){		
		CONF_HOME=System.getProperty("TTOOL_HOME");
		
		//子模块调试时使用
		if(new File(CONF_HOME+"/conf/log4j.properties").exists()){
			CONF_HOME+="/conf/";
		}else if(new File(CONF_HOME+"/../assembly/").exists()){
			CONF_HOME=new File(System.getProperty("TTOOL_HOME")).getParent()+"/assembly/";			
		}else if(new File(CONF_HOME+"/../../assembly/").exists()){
			CONF_HOME=new File(System.getProperty("TTOOL_HOME")).getParentFile().getParent()+"/assembly/";
		}else{
			throw new Error("log4j.properties not exist");
		}
		
		NATIVE_HOME=System.getProperty("TTOOL_HOME") + File.separator
				+ "native" + File.separator + get_classfier();
		PropertyConfigurator.configure(CONF_HOME+"log4j.properties");		
		addLibraryPath(NATIVE_HOME);	
	}
	
	public static String get_classfier(){
		if(classfier==null){
			String os=System.getProperty("os.name").split(" ")[0];
			classfier=(os+"_"+System.getProperty("os.arch")).toLowerCase();			
		}
		return classfier;		
	}
	
	public static void addLibraryPath(String path) {
		if(System.getProperty("java.library.path")!=null)
			path=path+File.pathSeparator+System.getProperty("java.library.path");
		
	    System.setProperty("java.library.path", path);	    
	    //set sys_paths to null so that java.library.path will be reevalueted next time it is needed
	    try {
			final Field sysPathsField = ClassLoader.class.getDeclaredField("sys_paths");
			sysPathsField.setAccessible(true);
			sysPathsField.set(null, null);
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	/**
	 * 获得系统变量的值 Usage： getSystemInfo("hostname") getSystemInfo("ip")
	 * 返回LocalHost网卡ip地址.
	 * 
	 * @param type
	 *            the type
	 * @return the system info
	 */
	public static String getSystemInfo(String type) {
		try {
			if (System.getProperty(type) != null)
				return System.getProperty(type);
			if (type.equalsIgnoreCase("ip")) {
				InetAddress i = InetAddress.getLocalHost();
				if (i.getHostName() != null && i.getHostName().length() > 0)
					return i.getHostName();
				return i.getHostAddress();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Checks if is OS. is("win") is("linux")
	 * 
	 * @param os
	 *            the os name
	 * @return true, if successful
	 */
	public static boolean is(String os) {
		String tmp = System.getProperty("os.name").toLowerCase();
		return tmp.contains(os.toLowerCase());
	}	

	/**
	 * 是不是windows,否则当作linux处理
	 * 
	 * @return
	 */
	public static boolean is_win() {
		return isWindows;
	}

//	DefaultExecutor exec;
//
//	ByteArrayOutputStream outputStream;
	
	/** The home. */
	protected final String default_tool_home;
	// 缓存主机相关的属性，包括ini文件中配置的静态信息及动态信息
	private HashMap<String, String> rm;
	
	/** The tool路径. */
	private String toolpath = "";
	
	protected Expect expect=new Expect();
	private static Host instance;
	protected long watch_timer=60000;
	
	public void set_watch_timer(long ms){
	    this.watch_timer=ms;
	}
	/**
	 * Instantiates a new host.
	 * 默认工具路径为tools目录，switchTo的默认根路径
	 * 
	 * @see #switchTo(String)
	 */
	protected Host() {
		this("tools");
	}
	
	/**
	 * 初始化home到toolpath的相对路径
	 * 
	 * @param toolpath tools的相对路径
	 */
	protected Host(String toolpath) {
		default_tool_home = HOME + File.separator + toolpath + File.separator;
		instance=this;
	}
	
	public static void set_localhost(Host host){	
		instance=host;
	}
	
	public static Host localhost(){		
		if(instance==null){			
			if(isWindows){
				if(Testbed.getTestbed().getRes("HOST_WIN")!=null)
					instance=(Host)Testbed.getTestbed().getRes("HOST_WIN");
				else 
					instance=new Windows();
			}else {
				if(Testbed.getTestbed().getRes("HOST_LIN")!=null)
					instance=(Host)Testbed.getTestbed().getRes("HOST_LIN");
				else 
					instance=new Linux();
			}
		}
		log.debug("init localhost {}",instance);
		return instance;
	}	
	
	protected String bin(){
		return HOME+File.separator+"bin"+File.separator;
	}
	
	/**
	 * 工具能力集
	 * @return
	 */
	public abstract String capabilities();
	public abstract String ping(String ip,String options);
	/**
	 * 执行一个命令，返回原始结果.
	 * 会处理switchTo函数的传入目录
	 * @param cmd
	 *            the cmd
	 * @return the string
	 * @see #exec0(String)
	 * @see #getCmdPath(String)
	 */
	public String exec(String cmd) {
		return exec0(getCmdPath(cmd));
	}
	/**
	 * 执行 && 立刻返回
	 * 会处理switchTo函数的传入目录
	 * @param cmd
	 * @see #exec0_nowait(String)
	 * @see #getCmdPath(String)
	 */
	public void exec_nowait(String cmd) {		
		exec0_nowait(getCmdPath(cmd));
	}
	/**
	 * 执行 && 返回exit value
	 * 会处理switchTo函数的传入目录
	 * 
	 * @param cmd
	 * @see #exec_exit(String)
	 */
	public int exec_exit(String cmd) {
	    return exec0_exit(getCmdPath(cmd));		
	}
	/**
	 * 执行一个命令，返回原始结果.默认最多等待60秒
	 * 不会处理switchTo函数的传入目录
	 * @param cmd
	 *            the cmd
	 * @return the string
	 * @see #set_watch_timer(long)
	 */
	public String exec0(String cmd){		
		CommandLine cmdLine = CommandLine.parse(cmd);
		DefaultExecutor executor = new DefaultExecutor();
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
		executor.setStreamHandler(streamHandler);
		try {
			//executor.setExitValue(0);
			ExecuteWatchdog watchdog = new ExecuteWatchdog(watch_timer);
			executor.setWatchdog(watchdog);			
			executor.execute(cmdLine);			
			return outputStream.toString(charset.displayName());
		} catch (ExecuteException e) {
			log.error("Run \"{}\" exited with an error: {}", cmd, e.getExitValue());
			try {
				log.error(outputStream.toString(charset.displayName()));
			} catch (UnsupportedEncodingException e1) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}		
		return null;
	}
	/**
	 * 执行 && 立即返回
	 * 不会处理switchTo函数的传入目录
	 * @param cmd 
	 */
	public void exec0_nowait(String cmd) {
		CommandLine cmdLine = CommandLine.parse(cmd);
		DefaultExecutor executor = new DefaultExecutor();		
		try {
			executor.execute(cmdLine);	
		} catch (ExecuteException e) {
			log.error("Run \"{}\" exited with an error: {}", cmd, e.getExitValue());			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
//	@Deprecated
//	void exec0_wait0(String cmd) {
//		final String _cmd=cmd;
//		log.debug("Native Run: " + _cmd);
//		try {
//			Process pro = run.exec(_cmd);
//			final Process fpro = pro;
//			new Thread() {
//				@Override
//				public void run() {
//					try {
//						fpro.waitFor();
//						log.debug("exec_wait done {}", _cmd);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//				}
//			};
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * 执行 && 等待 && 返回状态
	 * 不会处理switchTo函数的传入目录
	 * 
	 * @param cmd
	 * @return ExitValue
	 */
	public int exec0_exit(String cmd) {
	    CommandLine cmdLine = CommandLine.parse(cmd);
        DefaultExecutor executor = new DefaultExecutor();
        DefaultExecuteResultHandler handler=new DefaultExecuteResultHandler();
        
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);      
        executor.setStreamHandler(streamHandler);
        
        try {
            executor.execute(cmdLine,handler);  
            handler.waitFor();
            return handler.getExitValue();
        } catch (ExecuteException e) {
            log.error("Run \"{}\" exited with an error: {}", cmd, e.getExitValue());
            try {
                log.error(outputStream.toString(charset.displayName()));
            } catch (UnsupportedEncodingException e1) {
                e.printStackTrace();
            }
            return e.getExitValue();
        } catch (Exception e) {
            e.printStackTrace();            
        }
        return -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ate.dev.IDevice#get(java.lang.String)
	 */
	@Override
	public String get(String key) {
		if(rm==null)
			rm=Testbed.getTestbed().getPhyProps("HOST");
		return rm.get(key);
	}
	/**
	 * Gets the int.
	 * 
	 * @param key
	 *            the key
	 * @return the int
	 */
	public int get_int(String key) {
		return Integer.parseInt(get(key));
	}

	/**
	 * 根据toolpath和cmd计算cmd的真实路径
	 * 
	 * @param cmd
	 * @return  当toolpath为空或者长度为0字节时: return cmd
     *          当cmd以/开头或者前3个字符中包含":"时: return cmd
     *          当toolpath以/开头或者前3个字符中包含":"时: return toolpath+File.separator+cmd
     *          否则，return home+toolpath+File.separator+cmd
     * @see #switchTo(String)
	 */
	private String getCmdPath(String cmd) {
	    if(toolpath==null||toolpath.trim().length()==0)
	        return cmd;
	        
		//cmd包含绝对路径
		if (cmd.startsWith("/") || cmd.indexOf(":") < 3)
			return cmd;
		
		//toolpath是一个绝对 path
		if (toolpath.startsWith("/") || toolpath.contains(":"))
			return toolpath + File.separator + cmd;
		
		//使用相对path
		return default_tool_home + toolpath + File.separator + cmd;
	}

	/**
	 * Gets the path.
	 * 
	 * @return the path
	 */
	protected String getPath() {
		return default_tool_home + toolpath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ate.dev.IDevice#getType()
	 */
	@Override
	public String getType() {
		return "HOST";
	}
	
	public String ls(String path){
		File file=new File(path);
		String rst="";
		if(file.exists())
			for(String tmp:file.list())
				rst+=tmp+"\n";
		return rst;
	}
	
	public abstract boolean rm(String fname);
	public abstract boolean rmdir(String path);
	public abstract boolean kill(String processname);
	
	/**
	 * 计算文件的md5，windows下必须先安装md5sum
	 * @param fname
	 * @return
	 * @see http://gnuwin32.sourceforge.net/packages/coreutils.htm
	 */
	public String md5sum(String fname) {
		String tmp=exec0("md5sum "+fname);
		if(tmp!=null&&tmp.length()>0){
			return tmp.split(" ")[0];
		}
		return null;
	}
	
	public boolean newfile(String fname){
		File file=new File(fname);
		if(file.exists()){
			if(!file.isDirectory())
				return true;
			else
				log.error("{} is a directory.",fname);
		} else
			try {
				return file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return false;
		
	}
	
	public boolean mkdirs(String path){
		File file=new File(path);
		boolean rst=false;
		if(!file.exists())
			rst=file.mkdirs();
		else if(file.isDirectory())
			rst=true;
		if(rst)
			log.debug("mkdirs {} {}",path,rst);
		else 
			log.error("mkdirs {} {}",path,rst);
		return rst;
	}
	
	
	/**
	 * host的字符集，默认为charset
	 * 
	 * @param charset
	 * @see Expect#charset
	 */
	public void set_charset(String charset) {
		Host.charset = Charset.forName(charset);
	}
	
	/**
	 * 当cmd以/开头或者前3个字符中包含:时，直接使用该cmd
	 * 当toolname以/开头或者前3个字符中包含:时，使用toolname+File.separator+cmd
	 * 否则，使用home+toolname+File.separator+cmd
	 *  
	 * switchTo("nmap") //nmap文件夹存放网络安全相关工具
	 * 
	 * @param toolname
	 *            the toolname
	 * @return the string
	 */
	public String switchTo(String toolname) {
		if (toolname == null)
			toolpath = "";
		else
			toolpath = toolname;
		return toolpath;
	}
	
	/**
	 * which命令
	 * @param cmd
	 * @return
	 */
	public abstract String which(String cmd);
	
	
	public boolean is_reachable(String host){
		return is_reachable(host,10000);
	}
	
	public boolean is_reachable(String host,int timeout){
		try {
			return InetAddress.getByName(host).isReachable(timeout);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}	
}
