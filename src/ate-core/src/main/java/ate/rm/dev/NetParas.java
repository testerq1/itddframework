package ate.rm.dev;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ate.util.X2X0;

public class NetParas {
	public byte[] src_mac=new byte[]{1,1,1,1,1,1};
	public byte[] dst_mac=new byte[]{1,1,1,1,1,2};
	public byte[] src_ip=new byte[]{1,1,1,1};
	public byte[] dst_ip=new byte[]{2,2,2,2};
	public short src_port = 1234;
	public short dst_port = 8888;
	public long ack=0;
	public long seq=0;
	
	public NetParas swap(){
		NetParas p=new NetParas();
		p.src_mac=this.dst_mac;
		p.dst_mac=this.src_mac;
		p.src_ip=this.dst_ip;
		p.dst_ip=this.src_ip;
		p.src_port=this.dst_port;
		p.dst_port=this.src_port;
		return p;
	}
	
	public void setDstPort(short dst_port) {
		this.dst_port = dst_port;
	}

	public void setSrcPort(short src_port) {
		this.src_port = src_port;
	}

	public void setSrcIp(String ip) {
		this.src_ip = X2X0.ip2b(ip);
	}

	public void setDstIp(String ip) {
		this.dst_ip = X2X0.ip2b(ip);
	}

	public void setSrcMac(String mac) {
		this.src_mac = X2X0.chs2b(mac);
	}

	public void setDstMac(String mac) {
		this.dst_mac = X2X0.chs2b(mac);
	}
}
