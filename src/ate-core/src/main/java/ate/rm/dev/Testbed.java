/*
 * 
 */
package ate.rm.dev;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.AbstractURIHandler;
import ate.rm.file.FileUA;
import ate.testcase.ATestSuite;
import ate.testcase.Testcase;
import ate.ua.AbstractCrudImp;
import ate.ua.ResourceFactory;

/**
 * 测试床是在解析所有脚本之前初始化完毕的，默认使用conf/loopback.ini初始化，可以在命令行中用-testbed参数指定定制的测试床
 */
public class Testbed {	
	protected static Logger log = LoggerFactory.getLogger(Testbed.class);
	static{
		log.debug("in Testbed static");
	}
	/** The tb. */
	private static Testbed tb;
	
	public static String current_testbed_name;
	
	/**
	 * 用默认的conf/devices.ini文件初始化测试床
	 * 
	 * @return the testbed
	 */
	public static Testbed createTestbed() {
		if (tb == null)
			tb = new Testbed(Host.CONF_HOME+"loopback.ini");
		return tb;
	} 

	/**
	 * Creates the testbed.
	 * 
	 * @param filename
	 *            the filename
	 * @return the testbed
	 * @throws ConfigurationException
	 *             the configuration exception
	 * @throws MalformedURLException
	 *             the malformed url exception
	 */
	public static Testbed createTestbed(String filename) {
		if (tb == null)
			tb = new Testbed(filename);
		return tb;
	}

	/**
	 * Gets the testbed.
	 * 
	 * @return the testbed
	 */
	public static Testbed getTestbed() {
		if(tb==null)
			tb=createTestbed();
		return tb;
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		Testbed tb = Testbed.createTestbed();
		System.out.println(tb.arg("FTP.server"));
	}

	public HashMap<String,String> paras=new HashMap<String,String>();

	/** The dev class. */
	private HashMap<String, Class> devClass = new HashMap<String, Class>();

	// logic dev <---> real dev
	/** The dev map. */
	private HashMap<String, String> devMap = new HashMap<String, String>();

	/** The ini. */
	private HierarchicalINIConfiguration ini;

	/** The real device. */
	private HashMap<String, Object> realDevice = new HashMap<String, Object>();

	public Testbed() {

	}

	/**
	 * Instantiates a new testbed.
	 * 
	 * @param filename
	 *            the filename
	 * @throws ConfigurationException
	 *             the configuration exception
	 * @throws MalformedURLException
	 *             the malformed url exception
	 */
	private Testbed(String filename) {
		log.debug("new Testbed");
		ini = FileUA.getIni(filename);
		if(ini!=null)
			prunToLogiTopo("basic");
		else 
			log.warn("testbed isn't configured!");
	}

	public void add_arg(String key,String value){
		paras.put(key, value);
	}
	
	/**
	 * Gets the property.
	 * 
	 * @param key
	 *            the key
	 * @return the property
	 */
	public String arg(String key) {		
		if(ini==null){
			log.warn("testbed ini is null");
			return null;
		}		
		log.debug("get testbed arg: {}={}",key,ini.getProperty(key));
		return (String) ini.getProperty(key);
	}
	/**
	 * 注册全局资源，比如被测设备，name应该和设备定义文件中的一致.
	 * 
	 * @param name
	 *            the name
	 * @param dev
	 *            the dev
	 */
	public void device_registe(String name, Object dev) {
		log.debug("device_registe {} {}",name,dev);
		realDevice.put(name, dev);
		if(name.contains("HOST")){		    
	        Host.set_localhost(null);
	        AbstractCrudImp.reinit_static();
	        Testcase.reinit_static();
	        ATestSuite.reinit_static();		    
		}	
	}
	/**
	 * 获得设备配置文件中相关Scetion的值.
	 * 
	 * @param name
	 *            section name
	 * @return the phy props
	 */
	public HashMap<String, String> getPhyProps(String name) {
		HashMap<String, String> hm = new HashMap<String, String>();
		SubnodeConfiguration sObj = ini.getSection(name);
		Iterator it1 = sObj.getKeys();

		while (it1.hasNext()) {
			Object key = it1.next();
			hm.put(key.toString(), sObj.getString(key.toString()));
		}
		return hm;
	}

	/**
	 * Gets 资源.用来传递全局对象
	 * 
	 * @param name
	 *            the logical device name or other resource
	 * @return the resource
	 * @see #device_registe(String, Object)
	 */
	public Object getRes(String name) {
		Object o = this.realDevice.get(devMap.get(name));

		if (o != null)
			return o;

		return realDevice.get(name);
	}
	
	/**
	 * 将逻辑拓扑影射到物理拓扑上去 暂时不处理复杂拓扑.
	 * 
	 * @param desc
	 *            逻辑拓扑描述
	 */
	public void prunToLogiTopo(String desc) {
		if (desc.equals("basic"))
			devMap = new HashMap<String, String>();

		Set setOfSections = ini.getSections();
		Iterator sectionNames = setOfSections.iterator();
		while (sectionNames.hasNext()) {
			String name = sectionNames.next().toString();
			devMap.put(name, name);
		}
	}
	
	/**
	 * 恢复使用默认的UA，会删除此前注册的UA类
	 * @param scheme
	 * @see #ua_registe(String, Class)
	 */
	public void ua_deregiste(String scheme){
        ResourceFactory.restore_ua_class(scheme);
    }

	/**
	 * 使用定制的UA替换默认UA
	 * @param scheme UA的Scheme
	 * @param cname  定制的UA类名
	 * @see #ua_deregiste(String)
	 */
	public void ua_registe(String scheme, Class<AbstractURIHandler> cname){
	    ResourceFactory.replace_ua_class(scheme, cname);
	}

	/**
	 * Inits the dev.
	 * 
	 * @param name
	 *            the name
	 * @param props
	 *            the props
	 */
	private void initDev(String name, Object props) {

	}

	/**
	 * Inits the host.
	 * 
	 * @param name
	 *            the name
	 * @param props
	 *            the props
	 */
	private void initHost(String name, Object props) {
		Object o = System.getProperties();
		String sys = System.getProperty("os.name");

		log.debug(sys);
	}
}
