package ate.rm.file;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Scanner;

import ate.rm.dev.Host;

/**
 * Read and write a file using an explicit encoding. JDK 1.5. Removing the
 * encoding from this code will simply cause the system's default encoding to be
 * used instead.
 */
public final class ReadWriteTextFileWithEncoding {

	public ReadWriteTextFileWithEncoding(String aFileName) {
		fEncoding = Host.charset.displayName();
		fFileName = aFileName;
	}
	
	public ReadWriteTextFileWithEncoding(String aFileName, Charset aEncoding) {
		fEncoding = aEncoding.displayName();
		fFileName = aFileName;
	}
	
	public ReadWriteTextFileWithEncoding(String aFileName, String aEncoding) {
		fEncoding = aEncoding;
		fFileName = aFileName;
	}

	/** Write fixed content to the given file. */
	public void write(String contents) throws IOException {
		Writer out = new OutputStreamWriter(new FileOutputStream(fFileName),
				fEncoding);
		try {
			out.write(contents);
		} finally {
			out.close();
		}
	}

	/** Read the contents of the given file. */
	public String read_all_lines() {
		StringBuilder text = new StringBuilder();
		String NL = System.getProperty("line.separator");
		Scanner scanner=null;
		try {
			scanner = new Scanner(new FileInputStream(fFileName), fEncoding);
			while (scanner.hasNextLine()) {
				text.append(scanner.nextLine() + NL);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}finally {
			if(scanner!=null)
				scanner.close();
		}
		return text.toString();
	}

	// PRIVATE
	private final String fFileName;
	private final String fEncoding;
	
}
