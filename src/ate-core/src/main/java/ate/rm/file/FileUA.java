/*
 * 
 */
package ate.rm.file;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;

import ate.AbstractURIHandler;
import ate.rm.dev.Host;
import ate.ua.UAOption;
import ate.util.GenData0;

/**
 * 该UA应该作为ATE core的一部分处理
 */
public class FileUA extends AbstractURIHandler {
	private static String LINE_SEPARATOR = System.getProperty("line.separator");
	/** The Constant INI. */
	public final static int INI = 0;

	/** The Constant XML. */
	public final static int XML = 1;

	private String fileType;
	private String fileUri;
	HierarchicalINIConfiguration ini;

	/**
	 * Gets the file type.
	 * 
	 * @return the file type
	 */
	public int getFileType() {
		if (fileType == null || fileType.equalsIgnoreCase("ini"))
			return INI;
		throw new Error("only support ini file now.");
	}

	/**
	 * Gets the file uri.
	 * 
	 * @return the file uri
	 */
	public String getFileUri() {
		return fileUri;
	}

	/**
	 * 
	 * build(UA_URI,"file:conf/test.ini#ini") ==
	 * build(UA_URI,"file:conf/test.ini")
	 */
	@Override
	public AbstractURIHandler build(UAOption option, Object v) {
		if (option == UA_URI) {
			String value = v.toString();
			if (value.contains("#")) {
				String[] tmp = value.split("#");
				fileType = tmp[1];
				value = tmp[0];
			} else {
				fileType = "ini";
			}
			if (value.startsWith("file:"))
				value = value.substring("file:".length());

			if ("ini".equalsIgnoreCase(fileType))
				ini = getIni(value);

			type = UA;
		}
		return this;
	}
	
	public static void change_names(File fs,String pattern){
    	File[] fss=fs.listFiles();
    	int len=pattern.length();
    	for(File tmp:fss){
    		int i=tmp.getName().indexOf(pattern);
    		if(tmp.isFile()&&i>0){
    			String fn=tmp.getName().substring(i+len,tmp.getName().indexOf("]", i));
    			String ft=tmp.getName().substring(tmp.getName().lastIndexOf("."));
    			System.out.println(tmp.getParent()+"/"+fn+ft);
    			tmp.renameTo(new File(tmp.getParent()+"/"+fn+ft));
    		}else if(tmp.isDirectory()){
    			if(i>0){
    				String fn=tmp.getName().substring(i+len,tmp.getName().indexOf("]", i));
    				System.out.println(tmp.getParent()+"/"+fn);
    				File nf=new File(tmp.getParent()+"/"+fn);
    				tmp.renameTo(nf);
    				tmp=nf;
    			}
    			change_names(tmp,pattern);    				
    		}
    	}
    }
	/**
	 * Gets the ini from conf or somewhere<br>
	 * if filename是个url：<br>
	 * return<br>
	 * else if 是个绝对路径<br>
	 * return<br>
	 * else if HOME路径下存在file<br>
	 * return<br>
	 * else if HOME/conf路径下存在file<br>
	 * return<br>
	 * else <br>
	 * return null;<br>
	 * 
	 * @param filename
	 *            the filename
	 * @return the ini
	 * @throws ConfigurationException
	 *             the configuration exception
	 * @throws MalformedURLException
	 *             the malformed url exception
	 */
	public static HierarchicalINIConfiguration getIni(String filename) {
		HierarchicalINIConfiguration ini = null;
		try {
			// http or ftp文件
			if (GenData0.isUrl(filename))
				ini = new HierarchicalINIConfiguration(new URL(filename));
			else {
				File file = null;
				if (filename.indexOf(':') == 1 || filename.startsWith("/")) {// 绝对路径
					file = new File(filename);
				} else {// 相对路径
					file = new File(Host.HOME + File.separator + filename);
					if (!file.exists())
						file = new File(Host.HOME + File.separator + "conf" + File.separator
								+ filename);
				}

				if (file.exists())
					ini = new HierarchicalINIConfiguration(file);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("load ini: {} ",filename);
		return ini;
	}

	public static Properties get_properties(String file) {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(file);
			prop.load(fis);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}

	public String get(String key) {
		if ("ini".equalsIgnoreCase(fileType))
			return ini.getString(key);

		return null;
	}

	/**
	 * 获得同一个属性的多个值
	 * 
	 * @param key
	 * @return
	 */
	public List<String> get_multi_line(String key) {
		List<String> list = new ArrayList<String>();
		if ("ini".equalsIgnoreCase(fileType)) {
			String v = ini.getString(key);
			String[] ll = v.split(LINE_SEPARATOR);
			for (String tmp : ll)
				list.add(tmp.trim());
		}

		return list;
	}
	public static String get_contents(File aFile) {
	    StringBuilder contents = new StringBuilder();
	    
	    try {
	      BufferedReader input =  new BufferedReader(new FileReader(aFile));
	      try {
	        String line = null; 
	        while (( line = input.readLine()) != null){
	          contents.append(line);
	          contents.append(System.getProperty("line.separator"));
	        }
	      }
	      finally {
	        input.close();
	      }
	    }
	    catch (IOException ex){
	      ex.printStackTrace();
	    }
	    
	    return contents.toString();
	  }
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see ate.ua.AUAImp#is_ready()
	 */
	@Override
	public boolean is_ready() {
		return this.ini != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ate.ua.AUAImp#getDefaultSchema()
	 */
	@Override
	public String get_default_schema() {
		return "ate";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ate.ua.AUAImp#setup_ua()
	 */
	@Override
	public void startup() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ate.ua.AUAImp#teardown_ua()
	 */
	@Override
	public void teardown() {
		// TODO Auto-generated method stub

	}

}
