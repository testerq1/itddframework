package ate;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ate.actions.INotifier;
import ate.comp.taskman.TTTask;

public interface ITaskRunner extends Runnable{	
//	public TTTask execute(String[] paras);	
//	public TTTask execute(String paras);
	public TTTask execute(TTTask task);
	boolean needReport();
	public String getType();
	public String getDesc();
	boolean onlyCanRunOnce();
	int validTaskConfig(final TTTask task);
	void setTask(TTTask task);
	TTTask getTask();
	void setNotifier(INotifier notif);
}
