package ate;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.rm.dev.Testbed;
import ate.util.Expect;
import ate.util.MyConsole;

public final class Global {
	/** The console. */
	public static final MyConsole console = MyConsole.getMyConsole();
	public static String GSHELL="GSHELL";
	
	/** wait_yes方法 shell的提示符 */
	private static String CONTINUE = "Continue?[y]";

	public static Logger log = LoggerFactory.getLogger(Global.class);
	
	/**是否生成随机用例*/
	public static boolean random = false;
	public static int random_deep = 2;
	
	public static boolean verbose = false;
	
	public static int threadpool=5;
	
	/** 交互模式下，testcase wait_yes方法会霍格一个shell，从而暂停脚本执行 */
	public static boolean wait = false;
	/** Expect handler */
	public static Expect expect = new Expect();
	
	/** 收消息时等待的时长 */
	public static int timeout = 15;

	/** UA的message queue最多保存条消息*/
	public static int queue_len = 50;
	
	public static Charset charset = Charset.forName("UTF-8");
	
	public static int RETRY_TIMES=3;
	
	/**
	 * 计时秒表等待时间
	 * 
	 * @param sec
	 *            需pass的时间
	 * @see #watch_start()
	 */
	public static void watch_pass(long from, int sec) {
		Date now = new Date();
		log.debug("watch now at " + now.toString());

		if ((now.getTime() - from) / 1000 >= sec)
			return;

		sleep(sec * 1000 + from - now.getTime());
	}

	/**
	 * 获得console
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String get_console_input() throws Exception {
		while (true)
			return console.readLine(CONTINUE, "").trim();
	}

	/**
	 * 开始一个计时秒表
	 */
	public static long watch_start() {
		Date now = new Date();
		log.debug("watch start at " + now.toString());
		return now.getTime();
	}

	/**
	 * 交互式模式下，waitYes()会等待console输入一个y,然后才能推出.
	 * @see ate.Testcase#wait_yes
	 */
	public static boolean wait_yes() {
		if (Global.wait)
			while (true) {
				String commandLine;
				try {
					commandLine = console.readLine(CONTINUE, "").trim();
					if (commandLine.length() == 0
							|| commandLine.startsWith("y"))
						break;
					else
						return false;
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		return true;
	}
	/**
	 * bind参数到GroovyShell
	 * @param name
	 * @param value
	 */
	public static void gbind(String name, Object value){ 
	    GroovyShell gsh = (GroovyShell)Testbed.getTestbed().getRes(GSHELL);
	    Binding bind=gsh.getContext();
	    bind.setVariable(name, value);
	}
	/**
	 * 提供一个shell，解析输入命令
	 * 行尾为\时表示换行
	 */
    public static void gshell(){   
        GroovyShell gsh = (GroovyShell)Testbed.getTestbed().getRes(GSHELL);
        gsh.getContext().setVariable("g", Global.class);
        String line="";
        while (true) {
            try {
                line += Global.console.readLine(">");
                if(line.equalsIgnoreCase("quit"))
                    break;     
                else if(line.trim().endsWith("\\"))
                    line+="\n";
                else{
                    gsh.evaluate(line);
                    line="";
                }
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
	public static void sleep(long milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void set_log_level(Level level) {
		java.util.Enumeration<org.apache.log4j.Logger> cc = LogManager
				.getCurrentLoggers();
		while (cc.hasMoreElements()) {
			org.apache.log4j.Logger o = cc.nextElement();
			// System.out.println(o.getName());
			o.setLevel(level);
		}
		// System.out.println(log.getName());
		// LogManager.getLogger(log.getName()).setLevel(level);

	}
	
}
