package ate.testng;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import org.testng.CommandLineArgs;
import com.beust.jcommander.Parameter;

public class ATECommandLineArgs extends CommandLineArgs {
	public static final String PRERUN = "-prerun";
	@Parameter(names = PRERUN, description = "Print methods(@)")
	public Boolean prerun=false;
	
	public static final String HELP = "-h";
	@Parameter(names = HELP, description = "Print usage(@)")
	public Boolean help=false;
	
	public static final String TESTMETHOD = "-testmethod";
	@Parameter(names = TESTMETHOD, description = "Testmethod filter(@)")
	public String testmethod;
	
	public static final String TESTBED = "-testbed";
	@Parameter(names = TESTBED, description = "Testbed File(@)")
	public String testbed = "loopback.ini";
	
	public static final String TESTSUITE = "-testsuite";
	@Parameter(names = TESTSUITE, description = "Testsuite File(@)")
	public String testsuite = "TestSuite.groovy";
	
	public static final String TESTCASE = "-testcase";
	@Parameter(names = TESTCASE, description = "Testcase sequence filter(@)")
	public int testcase=-1;

	public static final String STOPONFAIL = "-stoponfail";
	@Parameter(names = STOPONFAIL, description = "Stop On Fail mode(@)")
	public Boolean stopOnFail = Boolean.FALSE;

}
