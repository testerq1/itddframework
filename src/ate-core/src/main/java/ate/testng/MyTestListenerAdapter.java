package ate.testng;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class MyTestListenerAdapter extends TestListenerAdapter {
	private static Logger log = LoggerFactory.getLogger(MyTestListenerAdapter.class);
	
	private int m_count = 0;
	
	private boolean stopOnFail=false;
	
	public MyTestListenerAdapter(boolean stopOnFail){
		this.stopOnFail=stopOnFail;		
	}
	
	@Override
	public void onTestStart(ITestResult result) {}

	@Override
	public void onTestFailure(ITestResult tr) {
		if(tr.getThrowable()!=null){
			log.error(tr.getThrowable().getLocalizedMessage());
			StackTraceElement[] st=tr.getThrowable().getStackTrace();
			for(StackTraceElement tmp:st){
				log.error(tmp.toString());
			}
		}
		if(stopOnFail){					
			System.exit(1);
		}
	}

	@Override
	public void onTestSkipped(ITestResult tr) {
		log("");
	}

	@Override
	public void onTestSuccess(ITestResult tr) {
		log("");
	}

	private void log(String string) {
		System.out.print(string);
		if (++m_count % 40 == 0) {
			System.out.println("");
		}
	}
}