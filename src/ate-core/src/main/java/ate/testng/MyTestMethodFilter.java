package ate.testng;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.regex.Pattern;

import org.testng.IMethodInstance;
import org.testng.ITestClass;
import org.testng.ITestMethodFilter;
import org.testng.ITestNGMethod;

public class MyTestMethodFilter implements ITestMethodFilter{
	private String clsPattern=".*";
	private String mthdPattern=".*";
	private String current_method;
	private int current_testcase=-1;
	private int testcase=-1;
	
	public void setRunClassRegExp(String regexp){
		if(regexp!=null&&regexp.length()>0)
			clsPattern = regexp.toLowerCase();
	}
	
	public void setRunTestcase(int seq){
		testcase=seq;
	}
	
	public void setRunMethodRegExp(String regexp){		
		if(regexp!=null&&regexp.length()>0)
			mthdPattern = regexp.toLowerCase();
	}
	
	public boolean canRunClass(ITestClass testClass) {
		return Pattern.matches(clsPattern, testClass.getName().toLowerCase());
	}
	
	public boolean canRunMethod(IMethodInstance method) {		
		String tmp=method.getMethod().getMethodName().toLowerCase();
		if(!tmp.equals(current_method)){
			current_method=tmp;
			current_testcase=-1;
		}			
		return Pattern.matches(mthdPattern, current_method);
	}
	
	/**
	 * 按照顺序号过滤用例
	 */
	@Override
	public boolean canRunTestcase(ITestNGMethod method, Object[] parameterValues) {
		if(testcase==-1)
			return true;
		
		current_testcase++;
		
		return current_testcase==testcase;
	}
}