package ate;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.lang.reflect.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.rm.dev.Testbed;
import ate.ua.IUA;
import ate.ua.RunFailureException;
import ate.ua.UAOption;
import ate.util.Expect;

/**
 * 所有URI类型资源的父类
 * 
 * @author xiaoyong.huang
 * 
 */
public abstract class AbstractURIHandler implements IUA{	
	public static final UAOption<String> UA_URI =
            new UAOption<String>("UA_URI");
	protected static Logger log = LoggerFactory.getLogger(AbstractURIHandler.class);
	
	public static final UAOption<Integer> UA_TYPE = new UAOption<Integer>(
			"UA_TYPE");
	public static final UAOption<String> URI2 = new UAOption<String>("URI2");
	
	public final static int SERVER = 1;
	public final static int CLIENT = 2;
	public final static int UA = 3;

	protected Charset charset =Global.charset;

	protected String host;
		
	private boolean inited = false;

	private final Map<UAOption<?>, Object> options = new LinkedHashMap<UAOption<?>, Object>();

	protected String passwd;

	protected String path;

	protected int port;

	protected Map<String, List<String>> queryParameters;

	protected String scheme;

	// server or client or ...
	protected int type=SERVER;

	protected URI uri;

	protected String username;
	
	protected Testbed tb=Testbed.getTestbed();
	
	/**对应测试床配置文件中的section*/
	protected String section;
	/**
	 * 子类可以重载该函数，实现定制的初始化 相同的实例不可重复初始化，以保持简单
	 * 
	 * @param connStr
	 * @return TODO
	 */
//	public T build(String connStr) {
//		log.debug("{} init {}", this, connStr);
//		if (inited)
//			throw new Error("不可重复使用UA");
//
//		inited = true;
//		try {
//			this.uri = new URI(connStr);
//			scheme = uri.getScheme();
//			path = uri.getPath() != null ? uri.getPath() : "";
//			host = uri.getHost();
//			port = uri.getPort();
//
//			String userinfo = uri.getUserInfo();
//			if (userinfo != null) {
//				String[] tmp = userinfo.split(":");
//				this.username = tmp[0];
//				this.passwd = tmp.length > 1 ? tmp[1] : null;
//			}
//			queryParameters = parseQueryParameters(uri.getQuery());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return (T)this;
//	}
	/**
	 * 从测试床中取属性，如果section不为空，则会试加上section.后去取
	 * @param key
	 * @return
	 * @see #section
	 */
	public String arg(String key){
	    if(section!=null)
	        key=section+"."+key;
	    return tb.arg(key);
	}
	/**
	 * url中的auth信息
	 * @return
	 * @see URI.getAuthority()
	 */
	public String get_authority() {
		return uri.getAuthority();
	}
	
	public String get_schemeSpecificPart() {
		return uri.getSchemeSpecificPart();
	}
	
	public boolean get_bool_from_map(Map<String, String> map, String key) {
		String v = map.get(key);
		if (v.equalsIgnoreCase("true"))
			return true;
		return false;
	}

	public abstract String get_default_schema();

	public String get_fragment() {
		return uri.getFragment();
	}

	public String get_host() {
		return host;
	}

	public int get_int_from_map(Map<String, String> map, String key) {
		String v = map.get(key);
		return Integer.parseInt(v);
	}
	
	public <B> B get_option(UAOption<B> option){
		return (B)options.get(option);
	}
	
	public String get_passwd() {
		return passwd;
	}

	public String get_path() {
		return path;
	}

	public int get_port() {
		return port;
	}

	public String get_query() {
		return uri.getQuery();
	}

	public String get_query_para(String key) {
	    if(queryParameters==null)
	        return null;
	    
		List<String> p = this.queryParameters.get(key);
		if (p == null || p.size() == 0)
			return null;
		if (p.size() > 1)
			log.warn(key + " has multi value+ " + p);
		return p.get(0);
	}

	public List<String> get_query_paras(String key) {
	    if(queryParameters==null)
	        return null;
		return this.queryParameters.get(key);
	}
	
	public Map<String, List<String>> get_query_arameters(){
		return queryParameters;
	}
	
	public String get_scheme() {
		return scheme;
	}
	
	public String get_section(){
	    return this.section;
	}
	
	public String get_uname() {
		return username;
	}

	public URI get_uri() {
		return uri;
	}

	public abstract boolean is_ready();

	/**
	 * TODO
	 * @param option
	 * @param value
	 * @return
	 */
    @SuppressWarnings("unchecked")
    public <B, T extends AbstractURIHandler> T build(UAOption<B> option, B value) {
        if (option == null) {
            throw new NullPointerException("option");
        }
        if (value == null) {
            synchronized (options) {
                options.remove(option);
            }
        } else {
            synchronized (options) {
                options.put(option, value);
            }
        }
        
        if (option == UA_TYPE)
			this.type = (Integer) value;        
        else if(option==UA_URI){
	        if (inited)
				throw new Error("不可重复使用UA");
	        
			inited = true;
			
			try {
				this.uri = new URI((String)value);
				scheme = uri.getScheme();
				path = uri.getPath() != null ? uri.getPath() : "";
				host = uri.getHost();
				port = uri.getPort();
	
				String userinfo = uri.getUserInfo();
				if (userinfo != null) {
					String[] tmp = userinfo.split(":");
					this.username = tmp[0];
					this.passwd = tmp.length > 1 ? tmp[1] : null;
				}
				queryParameters = parseQueryParameters(uri.getQuery());
				section=get_query_para("section");
				
			} catch (Exception e) {
				throw new RunFailureException(e);
			}
        }
        return (T)this;
    }
    
	private Map<String, List<String>> parseQueryParameters(String query)
			throws UnsupportedEncodingException {
		Map<String, List<String>> params = new HashMap<String, List<String>>();
		if (query != null) {
			for (String param : query.split("&")) {
				String pair[] = param.split("=");
				String key = URLDecoder.decode(pair[0], charset.displayName());
				String value = "";
				if (pair.length > 1) {
					value = URLDecoder.decode(pair[1], charset.displayName());
				}
				List<String> values = params.get(key);
				if (values == null) {
					values = new ArrayList<String>();
					params.put(key, values);
				}
				values.add(value);
			}
		}
		return params;
	}

	/**
	 * 修改默认字符集
	 * 
	 * @param cs
	 */
	public void set_charset(String cs) {
		charset = Charset.forName(cs);
	}


	public static void sleep(long milliseconds) {
	    Global.sleep(milliseconds);
	}

	@Override
	public String toString() {
		if (type == CLIENT)
			return this.get_default_schema() + " client";
		else if (type == SERVER)
			return this.get_default_schema() + " server";
		else
			return this.get_default_schema() + " ua";
	}
}
