package ate.test;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.TreeMap;

import org.testng.annotations.Test;

import ate.testcase.Testcase;
import ate.util.X2X0;

public class X2XTest extends Testcase {
	@Test
	public void base64(){
		String b64="cHJlc2VudGF0aW9ucy8xNC1tYXItcmVndWxhcmV4cHJlc3Npb25zLUIubXA0";
		assertTrue("presentations/14-mar-regularexpressions-B.mp4".equals(X2X0.decode_base64(b64)));
	}	
	
	@Test
	public void ip2i() {
		log.info("ip2i");
		log.info(String.format(" -e 'show %s status'","abc"));
		
		assertTrue(X2X0.ip2i("1.1.1.1") == 0x01010101);
		assertTrue(X2X0.ip2i("255.255.255.255") == 0xffffffff);
		assertTrue(X2X0.ip2i("256.255.255.255") == -1);
		//assertTrue(X2X0.ip2i("a.b.c.d") == -1);
		// log.info(X2X0.int2ip(0xffffffff));
	}
	
	@Test
	public void int2ip() {
		log.info("int2ip");
		assertTrue(X2X0.int2ip(0xffffffff).equals("255.255.255.255"));
		assertTrue(X2X0.int2ip(0x01010101).equals("1.1.1.1"));
		assertTrue(X2X0.hexs2b("000a0b0c0d")[4] == 0x0d);
		assertTrue(X2X0.hexs2b("000a0b0c 0d")[4] == 0x0d);
	}
	
	@Test
	public void bs2chs(){
	    log.info("bs2chs");

	    assertTrue(X2X0.bs2chs("ddd".getBytes(),"").equals("646464"));
	}
	
	@Test
	public void map() {
		log.info("map");
		TreeMap<String, String> tm = new TreeMap();
		tm.put("b", "dd1");
		tm.put("a", "dd2");
		tm.put("c", "dd3");

		for (String tmp : tm.keySet())
			System.out.println(tm.get(tmp));
	}

}
