package ate.test;

import java.util.concurrent.*;

import org.testng.annotations.Test;

import ate.testcase.Testcase;

public class BQTest extends Testcase {  
    final Basket basket = new Basket();  
    /** *//** 
     * 定义装苹果的篮子 
     */  
    public static class Basket{  
        // 篮子，能够容纳3个苹果  
        BlockingQueue<Object> basket = new LinkedBlockingQueue<Object>();  
          
        // 生产苹果，放入篮子  
        public void produce() throws InterruptedException{  
            // put方法放入一个苹果，若basket满了，等到basket有位置  
            basket.put(new String("1234"));  
            System.out.println("put "+basket.size());
        }  
        // 消费苹果，从篮子中取走  
        public Object consume() throws InterruptedException{  
            // get方法取出一个苹果，若basket为空，等到basket有苹果为止  
            return basket.take();  
        }  
    }  
    class Producer implements Runnable {  
        public void run() {  
            try {  
                while (true) {  
                    // 生产苹果  
                    System.out.println("生产者准备生产苹果："   
                            + System.currentTimeMillis());  
                    basket.produce();  
                    System.out.println("生产者生产苹果完毕："   
                            + System.currentTimeMillis());  
                    // 休眠300ms  
                    Thread.sleep(300);  
                }  
            } catch (InterruptedException ex) {  
            }  
        }  
    }  
    // 定义苹果消费者  
    class Consumer implements Runnable {  
        public void run() {  
            try {  
                while (true) {  
                    // 消费苹果  
                    System.out.println("消费者准备消费苹果："   
                            + System.currentTimeMillis());  
                    basket.consume();  
                    System.out.println("消费者消费苹果完毕："   
                            + System.currentTimeMillis());  
                    // 休眠1000ms  
                    Thread.sleep(1000);  
                }  
            } catch (InterruptedException ex) {  
            }  
        }  
    }  
    @Test
    public void testBasket() {  
        ExecutorService service = Executors.newCachedThreadPool();  
        Producer producer = new Producer();  
        Consumer consumer = new Consumer();  
        service.submit(producer);  
        service.submit(consumer);  
        // 程序运行5s后，所有任务停止  
        try {  
            Thread.sleep(5000);  
        } catch (InterruptedException e) {  
        }  
        service.shutdownNow();  
    }  
 
}
