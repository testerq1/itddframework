package ate.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.testng.annotations.*;

import ate.testcase.Testcase;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * First iText example: Hello World.
 * http://itextpdf.com/book/examples.php
 */
public class PDFiTextTest extends Testcase{
	public static final String RESULT = "hello.pdf";
	File fs;
	@Test
	public void createPdf() throws DocumentException,
			IOException {
		// step 1
		Document document = new Document();
		// step 2
		PdfWriter.getInstance(document, new FileOutputStream(RESULT));
		// step 3
		document.open();
		// step 4
		document.add(new Paragraph("Hello World!"));
		// step 5
		document.close();
		assertTrue((fs=new File(RESULT)).exists());
		
	}
	@AfterMethod
	public void AfterMethod(){
		if(fs.exists())
			fs.delete();
	}
	@BeforeMethod
	public void BeforeMethod(){
		File fs=new File(RESULT);
		if(fs.exists())
			fs.delete();
	}
}
