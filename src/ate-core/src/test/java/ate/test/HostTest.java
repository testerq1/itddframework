package ate.test;

import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.rm.dev.Host;
import ate.rm.dev.Linux;
import ate.rm.dev.Windows;
import ate.testcase.Testcase;

public class HostTest extends Testcase {
	Host os=Host.localhost();
	
	//@Test
	public void basic() {
		log.info("basic");
		log.info(Host.get_classfier());
		
		assertTrue(os!=null);
		if(Host.is_win()){
			assertTrue(os.capabilities().contains("nmcap"));
		}else{
			assertTrue(os.capabilities().contains("http_pload"));
			assertTrue(os.which("ls").contains("/bin/ls"));
		}
		log.info(os.capabilities());
		
		log.info(os.ls("."));
		
		assertTrue(os.mkdirs("tmp/tmp/tmp"));
		assertTrue(os.rmdir("tmp"));
		assertFalse(os.rm("tmp"));
		assertTrue(os.newfile("tmp"));
		assertTrue(os.rm("tmp"));		
	}
	
	@Test
	public void ping(){
		if(Host.is_win()){
			log.info(os.ping("127.0.0.1", ""));
		}
	}
	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
