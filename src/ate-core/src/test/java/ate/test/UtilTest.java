package ate.test;

import java.io.*;
import java.net.InetAddress;
import java.nio.charset.Charset;

import org.apache.commons.net.util.SubnetUtils;
import org.apache.commons.net.util.SubnetUtils.SubnetInfo;
import org.testng.annotations.Test;

import com.beust.jcommander.JCommander;

import ate.rm.file.FileUA;
import ate.testcase.Testcase;
import ate.testng.ATECommandLineArgs;
import ate.util.X2X0;

public class UtilTest extends Testcase{
	@Test
	/**
	 * 
	 * @see http://gnuwin32.sourceforge.net/packages/coreutils.htm
	 */
	public void exec(){
		log.info("md5sum");
		//LHOST.exec0("mkdir abc");
		assertTrue(new File("abc").exists());
		LHOST.rmdir("abc");
		assertTrue(!new File("abc").exists());
		
		log.info(LHOST.md5sum("con/log4j.xml"));
		assertTrue("9d201602e2b5ea69004d334ccac5e739".equals(LHOST.md5sum("conf/log4j.xml")));
	}
	
	@Test
	public void basic(){
		log.info("ddd1");
		String[] argv=new String[]{"-mixed","-listener","123","abc"};
		ATECommandLineArgs cla = new ATECommandLineArgs();
	     new JCommander(cla, argv);
	     assertTrue(cla.mixed);
	     assertTrue("123".equals(cla.listener));
	     assertTrue(null==cla.host);
	     assertTrue("loopback.ini".equals(cla.testbed));
	     assertTrue(null==cla.testClass);
	     assertTrue(cla.testcase<0);
	}	
	
	public void changenames(){
		FileUA.change_names(new File("g:\tmp"), "Friends.");		
	}
	
	@Test
	public void defaultRoute() throws IOException{
	    File out=new File("route.bat");
	    FileWriter fr=new FileWriter(out);
	    
	    InputStream fis = new FileInputStream("cn.csv");
	    InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
	    BufferedReader br = new BufferedReader(isr);
	    String s=null;
	    while((s=br.readLine())!=null){
	        String[] sp=s.split(",");
	        if(sp.length<4){
	            log.info("--------------- {}",s);
	            continue;
	        }
	        long MASK=ipToLong(InetAddress.getByName("255.255.255.255"));
	        InetAddress addr=InetAddress.getByName(sp[0]);
	        long begin=ipToLong(addr);
	        long end=begin+Integer.parseInt(sp[2]);
	        long mask=1;
	        for(int i=1;i<32;i++){
	            begin=(begin>>>1);
	            end=(end>>>1);
	            if(begin==end){
	                mask=32-i+2;
	                break;
	            }
	        }
	        SubnetUtils su=new SubnetUtils(sp[0]+"/"+mask);
	        SubnetInfo si=su.getInfo();
	        log.info("{} {} {}",si.getAddress(),si.getLowAddress(),si.getHighAddress());
	        fr.append("route ADD "+si.getNetworkAddress()+" MASK "+si.getNetmask()+" 172.16.5.111")
	        .append("\r\n");
	        //break;
	    }
	    fr.flush();
	    fr.close();
	}
	public static long ipToLong(InetAddress ip) {
        byte[] octets = ip.getAddress();
        long result = 0;
        for (byte octet : octets) {
            result <<= 8;
            result |= octet & 0xff;
        }
        return result;
    }
}
