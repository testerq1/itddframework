package ate.test;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.testcase.Testcase;
import ate.util.Expect;
import static org.assertj.core.api.Assertions.*;

public class ExpectTest extends Testcase {
	Expect exp;
	@Test
	public void test_expect_end() {
		assertTrue(exp.equal_e("123abc", "^123*"));
		assertTrue(exp.end("123abc", "abc"));
		assertTrue(exp.end("123abc", "ab c"));
		exp.set_is_strict(true);
		assertFalse(exp.end("123abc", "ab c"));
	}
	
	@Test
	public void test_expect_equal_e() {
		assertFalse(exp.equal_e("abbbbbc", ".*a.*cd"));
		assertTrue(exp.equal_e("abbbbbc", "a.*c"));		
	}
	
	@Test
	public void test_expect3() {
		assertFalse(exp.expect("ab c", "ab",false));
		assertTrue(exp.expect("ab c", "ab",true));
	}
	
	@Test
	public void test_expect() {		
		assertTrue(exp.expect("ab c", "abc"));
		assertTrue(exp.expect("ab c", "ab  c"));
		assertTrue(exp.expect("ab c", "ab"));		
		assertTrue(exp.expect("ab c", "ab  c"));
		assertFalse(exp.expect("ab: c", "abc"));
		assertFalse(exp.expect("abc", "ab: c"));
		
		exp.set_is_strict(true);
		assertFalse(exp.expect("ab c", "abc"));
		assertFalse(exp.expect("abc", "ab c"));
		
	}

	@Test
	public void test_match() {
		Expect reg = new Expect();
		assertFalse(reg.match_e("abbbbbc", "*a*cd"));
		assertTrue(reg.match_e("abbbbbc", "a.*c"));

		String rtn = "Pinging 172.16.5.1 with 32 bytes of data:";
		rtn += "";
		rtn += "  Reply from 172.16.5.1: bytes=32 time<1ms TTL=64";
		rtn += "  Reply from 172.16.5.1: bytes=32 time<1ms TTL=64";
		rtn += "  Reply from 172.16.5.1: bytes=32 time<1ms TTL=64";
		rtn += "  Reply from 172.16.5.1: bytes=32 time<1ms TTL=64";
		rtn += "		  ";
		rtn += "  Ping statistics for 172.16.5.1:";
		rtn += "      Packets: Sent = 4, Received = 4, Lost = 999 (120% loss),";
		rtn += "  Approximate round trip times in milli-seconds:";
		rtn += "      Minimum = 0ms, Maximum = 0ms, Average = 0ms";
		String[] gs = reg.groups(rtn, "(\\d+)\\s+\\((\\d+)%");
		assertTrue(gs != null && gs[1].equals("999") && gs[2].equals("120"));

		gs = reg.groups(rtn, "Pinging\\s+((\\d+\\.){3}(\\d+))");

		assertTrue(gs != null && gs[1].equals("172.16.5.1"));

	}
	@BeforeMethod
	public void beforeMethod(Method m) {
		exp=new Expect();
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
