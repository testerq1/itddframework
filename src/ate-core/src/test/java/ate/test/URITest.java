package ate.test;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.codec.net.URLCodec;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.AbstractURIHandler;
import ate.testcase.Testcase;
import ate.ua.UAOption;

public class URITest extends Testcase {
	class URIUA extends AbstractURIHandler {
		@Override
		public String get_default_schema() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean is_ready() {
			return true;
		}

		@Override
		public void startup() {
			// TODO Auto-generated method stub

		}

		@Override
		public void teardown() {
			// TODO Auto-generated method stub

		}
	}
	
	@AfterMethod
	public void afterMethod() {
	}
	
	@BeforeClass
	public void beforeClass() {
	}

	 @BeforeMethod
	public void beforeMethod() {
	}

	@Test
	public void host(){
		URIUA uri = new URIUA();
		uri.build(URIUA.UA_URI, "test://:/?key=name:v");
		assertTrue(null==uri.get_host());
		assertTrue(-1==uri.get_port());
		log.info(uri.get_query_para("key"));
	}
	
	@Test  
	public void f() {
		URIUA uri = new URIUA();
		uri.build(URIUA.UA_URI, "test://127.0.0.1:1234/path?name=ferret#nose");
		assertTrue("name=ferret".equals(uri.get_query()));
		assertTrue("nose".equals(uri.get_fragment()));
	}

	@Test
	public void f1() {
		URIUA uri = new URIUA();
		uri.build(URIUA.UA_URI,
				"test://127.0.0.1:1234/path?name=ferret&sex=female#nose");
		assertTrue("name=ferret&sex=female".equals(uri.get_query()));
		assertTrue("ferret".equals(uri.get_query_paras("name").get(0)));
		assertTrue("female".equals(uri.get_query_paras("sex").get(0)));
		assertTrue("nose".equals(uri.get_fragment()));
	}

	@Test
	public void f2() {
		URIUA uri = new URIUA();
		uri.build(URIUA.UA_URI,
				"jdbc:test://127.0.0.1:1234/path?name=ferret&sex=female#nose");
		assertTrue("test://127.0.0.1:1234/path?name=ferret&sex=female"
				.equals(uri.get_schemeSpecificPart()));
		assertTrue(uri.get_query() == null);
		assertTrue(uri.get_query_paras("name") == null);
		assertTrue(uri.get_fragment().equals("nose"),uri.get_fragment());
	}

	@Test
	public void f3() {
		URIUA uri = new URIUA();
		uri.build(URIUA.UA_URI, "jdbc:/./path?name=ferret&sex=female#nose");
		assertTrue("/./path?name=ferret&sex=female".equals(uri
				.get_schemeSpecificPart()));
		assertTrue("/./path".equals(uri.get_path()));
		assertTrue("name=ferret&sex=female".equals(uri.get_query()));
		assertTrue("ferret".equals(uri.get_query_para("name")));
		assertTrue(uri.get_fragment().equals("nose"),uri.get_fragment());
	}

	@Test
	public void file() throws URISyntaxException {
		File file = new File("abc");
		assertFalse(file.isFile());
		file = new File("file:///c:/boot.ini");
		assertFalse(file.isFile());
		assertFalse(file.canRead());
		file =new File (new URI("file:///c:/boot.ini")); 
		assertTrue(file.isFile());
		assertTrue(file.canRead());
		
	}

	@Test
	public void url() throws URISyntaxException, UnsupportedEncodingException{
		log.info(new URLCodec().encode("http://www.baidu.com/s?wd=脚本&rsv_spt=1&issp=1&rsv_bp=0&ie=utf-8&tn=baiduhome_pg", "UTF-8"));
		//http://www.baidu.com/s?wd=%E8%84%9A%E6%9C%AC&rsv_spt=1&issp=1&rsv_bp=0&ie=utf-8&tn=baiduhome_pg
		
		URI uri = new URI(
		        "http", 
		        "www.google.com", 
		        "/ig/api",
		        "weather=asd=qwe",
		        null);
		log.info(uri.toASCIIString());
		log.info(uri.getQuery());
		
		uri = new URI("ldap://weather=asd=qwe:passwd@1.1.1.1:345/");
		log.info(uri.getAuthority());
	}

}
