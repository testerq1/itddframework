package ate.test;

import ate.testcase.Filter;
import ate.testcase.Testcase;
import org.testng.annotations.Test;
import java.util.*;

public class TestcaseFilterTest extends Testcase {
	
	@Test
	public void basic() {
		Object[][] SAMPLES = {
				{ "SVT_RADIUS_PARSER_FUNC_001_0", new HashMap<String, String>() {
					{
						put("User-Name", "13760439582");
						put("CHAP-Password", "07c42a8ac9bb36d2bcdc4ae8e05fcb8e8b");
						put("Calling-Station-Id", "00:26:82:B7:F5:BF");
						put("NAS-IP-Address", "192.168.1.88");
						put("Called-Station-Id", "00-17-7B-2B-D3-0C:zteopen");

					}
				} },
				{ "SVT_RADIUS_PARSER_FUNC_001_1", new HashMap<String, String>() {
					{
						put("User-Name", "13760439583");
						put("CHAP-Password", "07c42a8ac9bb36d2bcdc4ae8e05fcb8e8b");
						put("Calling-Station-Id", "00:26:82:B7:F5:BF");
						put("NAS-IP-Address", "192.168.1.88");
						put("Called-Station-Id", "00-17-7B-2B-D3-0C:zteopen");
					}
				} }, 
				{ "SVT_RADIUS_PARSER_FUNC_001_2", new HashMap<String, String>() {
					{
						put("User-Name", "13760439582");
						put("CHAP-Password", "07c42a8ac9bb36d2bcdc4ae8e05fcb8e8c");
						put("Calling-Station-Id", "00:26:82:B7:F6:BF");
						put("NAS-IP-Address", "192.168.1.88");
						put("Called-Station-Id", "00-17-7B-2B-D3-0D:zteopen");
					}
				} }
		};
		Filter ff=new Filter();
		List ll=ff.random(SAMPLES);
		assertTrue(ll!=null);
		assertTrue(ll.size()==SAMPLES.length);
		
		ff.STRENGTH=20;
		ll=ff.random(SAMPLES);
		assertTrue(ll!=null);
		assertTrue(ll.size()==16);	
		
		ff.STRENGTH=2;
		ll=ff.random(SAMPLES);
		assertTrue(ll!=null);
		assertTrue(ll.size()==9,"预期 长度不正确为 9,实际为%d", ll.size());//size*(STRENGTH +1)
	
	}

}
