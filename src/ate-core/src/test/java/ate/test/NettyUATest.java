package ate.test;

/*
 * #%L
 * iTDD Core
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import ate.Global;
import ate.testcase.CsUATestcase;
import ate.testcase.Testcase;
import ate.testcase.UATestcase;
import ate.ua.netty.ANettyUAImp;
import ate.ua.netty.UAClient;
import ate.ua.netty.UAServer;

public class NettyUATest extends CsUATestcase {
    @DataProvider(name = "Data_TestStep")
    public Object[][] Data_TestStep() {
        return new String[][] { { "udp" }, { "tcp" } };
    }
    
    //@Test
    public void multicast(){
        STEP("init:");
        ANettyUAImp svrua = create_server("socket://230.0.0.1:5555/multicast", new TestUA());
        ANettyUAImp cltua = create_client("socket://230.0.0.1:5555/multicast", new TestUA());
        svrua.build(svrua.NET_IF, "172.16.5.111");
        cltua.build(svrua.NET_IF, "172.16.5.111");        
        this.start_all_ua();
        
        assertTrue(svrua.is_ready() && cltua.is_ready());
        
        svrua.send_message("tets");
        
        Object msg=cltua.recv_message();
        
        assertTrue(msg!=null);
    }
    
    @Test(dataProvider="Data_TestStep")
    public void base_send_recv(String transport) {
        STEP("init:");
        ANettyUAImp svrua = create_server("socket://127.0.0.1:5555/" + transport, new TestUA());
        ANettyUAImp cltua = create_client("socket://127.0.0.1:5555/" + transport, new TestUA());
        
        this.start_all_ua();
        
        assertTrue(svrua.is_ready() && cltua.is_ready());

        svrua.connect(cltua);

        STEP("clt send:");
        cltua.send_message("test\r\n");
        Object o = svrua.recv_message();

        wait_yes();
        log.info(o.toString());
        assertTrue(o != null && o.toString().trim().equals("test"));

        STEP("svr send:");
        svrua.send_message("response\r\n");
        o = cltua.recv_message();
        log.info(o.toString());
        assertTrue(o != null && o.toString().trim().equals("response"));
    }

    class TestUA extends ANettyUAImp<Object> {
        public TestUA(){}
        public void set_type(int type) {
            this.type = type;
        }

        @Override
        public String get_default_schema() {
            return "netty";
        }

        @Override
        public boolean message_matched(Object msg, HashMap<String, Object> hm) {
            return true;
        }
    }

    @Test(dataProvider = "Data_TestStep")
    public void base_send(String transport) {
        STEP("init:");
        ANettyUAImp svrua = create_server("socket://127.0.0.1:5555/" + transport, new TestUA());
        ANettyUAImp cltua = create_client("socket://127.0.0.1:5555/" + transport, new TestUA());
        this.start_all_ua();
        
        assertTrue(svrua.is_ready() && cltua.is_ready());

        STEP("send rcv:");
        cltua.send_message("test1\r\n");
        Object o = svrua.recv_message();

        assertTrue(o != null && o.toString().trim().equals("test1"));

        STEP("reconnect:");
        cltua.teardown();
        assertTrue(!cltua.is_ready());
        cltua.startup();

        assertTrue(cltua.is_ready());

        STEP("resend:");
        cltua.send_message("test1\r\n");
        o = svrua.recv_message();

        assertTrue(o != null && o.toString().trim().equals("test1"));
        STEP("shutdown:");
    }
    @Test
    public void ssl() {
        Global.verbose=true;
        String transport="tcp";
        STEP("init:");
        ANettyUAImp svrua = create_server("socket://127.0.0.1:5555/" + transport, new TestUA());
        ANettyUAImp cltua = create_client("socket://127.0.0.1:5555/" + transport, new TestUA());
        
        svrua.use_bogus_ssl();
        cltua.use_bogus_ssl();
        
        this.start_all_ua();
        assertTrue(svrua.is_ready() && cltua.is_ready());

        STEP("send rcv:");
        cltua.send_message("test1\r\n");
        Object o = svrua.recv_message();

        assertTrue(o != null && o.toString().trim().equals("test1"));

        STEP("reconnect:");
        cltua.teardown();
        assertTrue(!cltua.is_ready());
        cltua.startup();

        assertTrue(cltua.is_ready());

        STEP("resend:");
        cltua.send_message("test1\r\n");
        o = svrua.recv_message();

        assertTrue(o != null && o.toString().trim().equals("test1"));
        STEP("shutdown:");
    }
    @BeforeMethod
    public void beforeMethod(Method m) {
        super.BeforeMethod(m);
    }

    @AfterMethod
    public void afterMethod(Method m) {
        this.shutdown_all_ua();
        super.AfterMethod(m);
    }

    @BeforeClass
    public void beforeClass() {
        super.BeforeClass();
    }

    @AfterClass
    public void afterClass() {
        super.AfterClass();
    }

}
