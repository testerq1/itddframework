package ate.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.testng.annotations.Test;

import ate.Global;
import ate.testcase.IPO;
import ate.testcase.Pairwise;
import ate.testcase.Testcase;

public class SVT_IPO_FUNC_01 extends Testcase {
    @Test
    public void pairwise_map1() {
        List<Object[]> paras = new ArrayList() {
            {
                add(new Object[] { "a", new String[] { "1", "2", "3" } });
                add(new Object[] { "b", new String[] { "a", "b", "c", "d" } });
                add(new Object[] { "c", new String[] { "A", "B", "C", "D" } });
                add(new Object[] { "d", new String[] { "x", "y" } });
            }
        };
        Object[][] rst = gen_testcases(paras);
        for (int i = 0; i < rst.length; i++) {
            System.out.print(rst[i][0] + " = ");
            HashMap hm = (HashMap) rst[i][1];

            for (Object o : hm.keySet()) {
                System.out.print(o + ":" + hm.get(o) + "\t");
            }
            System.out.print("\n");
        }
        System.out.print("\n\n");

        Global.random = true;
        rst = gen_testcases(paras, 3);
        for (int i = 0; i < rst.length; i++) {
            System.out.print(rst[i][0] + " = ");
            HashMap hm = (HashMap) rst[i][1];

            for (Object o : hm.keySet()) {
                System.out.print(o + ":" + hm.get(o) + "\t");
            }
            System.out.print("\n");
        }
    }

    public void pairwise_map() {
        List<Object[]> paras = new ArrayList() {
            {
                add(new Object[] { "a", new String[] { "1", "2", "3" } });
                add(new Object[] { "b", new String[] { "a", "b", "c", "d" } });
                add(new Object[] { "c", new String[] { "A", "B", "C", "D" } });
                add(new Object[] { "d", new String[] { "x", "y" } });
            }
        };
        Object[][] rst = Pairwise.go_map("SVN_FUNC", paras, 2, null);
        for (int i = 0; i < rst.length; i++) {
            System.out.print(rst[i][0] + " = ");
            HashMap hm = (HashMap) rst[i][1];

            for (Object o : hm.keySet()) {
                System.out.print(o + ":" + hm.get(o) + "\t");
            }
            System.out.print("\n");
        }
    }

    @Test
    public void pairwise() {
        String[][] paras = new String[][] { { "1", "2", "3" },
                { "a", "b", "c", "d" }, { "A", "B", "C", "D" }, { "x", "y" } };

        ArrayList al = new ArrayList<ArrayList>();

        for (int i = 0; i < paras.length; i++) {
            al.add(new ArrayList(Arrays.asList(paras[i])));
        }

        ArrayList rst = Pairwise.go(al, 1);
        log.info("total:{}" + rst.size());
        for (int i = 0; i < rst.size(); i++) {
            ArrayList tmp = (ArrayList) rst.get(i);
            for (int j = 0; j < tmp.size(); j++) {
                System.out.print(tmp.get(j));
            }
            System.out.print("\n");
        }
    }

    public void test() {
        String[][] paras = new String[][] { { "1", "2", "3" },
                { "a", "b", "c", "d" }, { "A", "B", "C", "D" }, { "x", "y" } };
        String[][] rst = new IPO().pair(paras);
        log.info("total:{}" + rst.length);
        for (int i = 0; i < rst.length; i++) {
            for (int j = 0; j < rst[i].length; j++) {
                System.out.print(rst[i][j]);
            }
            System.out.print("\n");
        }
    }
}
