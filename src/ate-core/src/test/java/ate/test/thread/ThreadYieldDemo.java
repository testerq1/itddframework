package ate.test.thread;

public class ThreadYieldDemo extends Thread
{
    static boolean finished = false;
    static int sum = 0;
    public static void main (String [] args)
    {
       new ThreadYieldDemo ().start ();
       for (int i = 1; i <= 50000; i++)
       {
            sum++;
            Thread.yield ();
       }
       finished = true;
    }
    public void run ()
    {
       while (!finished)
          System.out.println ("sum = " + sum);
    }
 }