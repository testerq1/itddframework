package ate.test.thread;

import org.testng.annotations.Test;

import ate.test.UtilTest;
import ate.testcase.Testcase;

public class ThreadSchedDemo extends Testcase{
    @Test
    public void jointest(){
        Thread t=new Thread(){
          public void run(){
              int cnt=5;
              while(cnt-->0){
                  System.out.println("wait");
                  try {
                    Thread.currentThread().sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
              }
              ThreadSchedDemo.this.interrupt();
          }  
        };
        t.start();
        join();
//      try {
//            t.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }
    
    @Test
    //主线程不会等子线程结束
    public void test(){
        new CalcThread ("CalcThread A").start ();
        //new CalcThread ("CalcThread B").start ();
    }
    
    //主线程会等子线程结束后才结束
    public static void main (String [] args)
    {
       new CalcThread ("CalcThread A").start ();
       //new CalcThread ("CalcThread B").start ();
    }
 }

 class CalcThread extends Thread
 {
    CalcThread (String name)
    {
       // Pass name to Thread layer.
       super (name);
    }
    double calcPI ()
    {
       boolean negative = true;
       double pi = 0.0;
       for (int i = 3; i < 100000; i += 2)
       {
            if (negative)
                pi -= (1.0 / i);
            else
                pi += (1.0 / i);
            negative = !negative;
       }
       pi += 1.0;
       pi *= 4.0;
       return pi;
    }
    public void run ()
    {
       for (int i = 0; i < 5; i++){
          System.out.println (getName () + ": " + calcPI ());
          try {
              Thread.currentThread().sleep(500);
          } catch (InterruptedException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
          }
       }
    }
 }
