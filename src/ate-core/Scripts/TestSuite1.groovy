import org.testng.annotations.*;
import ate.testcase.*
import ate.rm.dev.*
import common.*;

public class TestSuite1 extends ATestSuite{	
	@BeforeSuite
	public void BeforeSuite(){
		super.BeforeSuite()	
        tb.device_registe("HOST_WIN",new Win())
        tb.device_registe("HOST_LIN",new Lin())
        reinit_static()
	}	
    public void switchTestsuite(name){
        if(name==current_testsuite)
            return
        if(name=="1")
            testsuite_1()

        current_testsuite=name
    }

    def static main(args){
        def ss=new TestSuite();
        log.info(ss.arg("DUT.IP"))
    }
    
	@AfterSuite
	public void AfterSuite(){
		super.AfterSuite()
		//nfe.quit()
	}
}