package common;
import ate.util.*
import ate.rm.dev.*

/**
 * NFE的类 
 * @author huangxy
 *
 */
def class DUTDemo extends DUT{
    
    def static main(args){
        def nfe=new DUTDemo("ssh://root:passwd@172.16.5.219/?section=DUT")

    }
    
	public DUTDemo(connstr){
		super(connstr)
        def db=create_ua("mysql://root:123456@172.16.5.197/")
        println db
	}
	
	/**
	 * clear在线AP表
	 * debug模式下执行 feSetAPIPInit(1)
	 * @return
	 */
	def clear_ap_online(){
		enter_mode("debug");
		assert expect.contains(vt.send_and_expect("feSetAPIPInit(1)"),"error")==false
	}
	
	def enter_mode(mode){
		vt.send_line(mode);	
	}
	
//	private void init_nfe(COM){
//		def NFE_WAN_IP=get("WAN_IP")//172.16.5.115
//		def NFE_WAN_GATEWAY=get("WAN_GATEWAY")//172.16.5.1
//		def MGT_IP=get("WEB_IP")
//		COM.enterMode("");
//		
//		if(type=="linux")
//			return			
//			
//		COM.exec("set admin telnet enable")
//		COM.exec("set vif vif0 delete")
//		COM.exec("set vif vif1 delete")
//		COM.exec("set vif new vif0-10 phy-if ge0 10")
//		COM.exec("set vif new vif1-20 phy-if ge1 20")
//		COM.exec("set vif vif0-10 zone l3-in")
//		COM.exec("set vif vif1-20 zone l3-out")
//		COM.exec("set vif vif0-10 ip $MGT_IP netmask 255.255.255.0")
//		COM.exec("set vif vif1-20 ip $NFE_WAN_IP netmask 255.255.255.0")
//		COM.exec("set vif vif1-20 filters enable web,telnet,ssh,ping,snmp")
//		COM.exec("set route ip 0.0.0.0 0.0.0.0 $NFE_WAN_GATEWAY")
//		COM.exec("set vlan-port-mapping phy-if ge0 vid 10 8021p 0")
//		COM.exec("set vlan-port-mapping phy-if ge1 vid 20 8021p 0")
//		COM.exec("firewall set policy from ANY to ANY src-addr ANY dst-addr ANY service \"ANY\" action permit status enable ")
//		COM.exec("firewall set policy from ANY to ANY src-addr ANY dst-addr ANY service \"ANY\" action permit status enable ")
//		COM.exec("set vif vif1-20 nat")
//		COM.exec("set admin telnet timeout 3600")
//		COM.exec("set admin web timeout 3600")		
//	}
	
	/**
	 * set file-server ftp
	 * @param svr
	 * @param uname
	 * @param passwd
	 * @return
	 */
	def set_ftp(svr,uname,passwd){
		vt.send_line("set file-server ftp $svr $uname $passwd","error")
	}
	
	/**
	 * NFE8080/root_sys->download file ss ss
	 * This operation may take a few minutes, continue?
	 * @param src
	 * @param dst
	 * @return
	 */
	def download(src,dst){
		vt.send_and_expect("download file $src $dst","continue?")
		vt.send_line("y")
	}
	
	/**
	 *
	 *	library
	 *	license
	 *	sys                       --- sys <firmware | hardcode | bootrom> <file-name>
	 * @param type
	 * @param filename
	 * @return
	 */
	def update(type,filename){
		def rtn=""
		if(type.startsWith("lic")){
			rtn=vt.send_and_expect("update license $filename")
			assert rtn.contains("Error:")==false
			return
		}		
				
		if(type.startsWith("fir")){
			vt.send_and_expect("update sys firmware $filename","y/[n])")
		}else if(type.startsWith("hard")){
			vt.send_and_expect("update sys hardcode $filename","y/[n])")
		}else if(type.startsWith("boo")){
			vt.send_and_expect("update sys bootrom $filename","y/[n])")
		}else{
			println "update missing type:$type!"
			return
		}
		vt.set_timeout(120)
		rtn=vt.send_and_expect("y")
		vt.set_timeout(15)
		assert rtn.toLowerCase().contains("error:")==false
	}
	
	/**
	 * reboot
	 * @return
	 */
	def c_reboot(){		
		vt.send_and_expect("reboot","y/[n])")
		vt.send_and_expect("y","y/[n])")
		vt.send_line("y")
		vt.teardown()
		sleep(30)
		vt.restart()		
	}
	
	/**
	 * update
	 * @return
	 */
	def update(){		
		def folder=has_new_version()
		if(folder==null){
			log.warn "没有新版本"
			return false
		}
		def ftp_svr=args("FTP.server");
		def ftp_uname=args("FTP.uname");
		def ftp_passwd=args("FTP.passwd");
		
		set_ftp(ftp_svr,ftp_uname,ftp_passwd)
		
		folder.each{
			if(it.toLowerCase().contains("hardcode"))
				update("hardcode",it)
			else if(it.toLowerCase().contains("firmware"))
				update("firmware",it)		
			else if(it.toLowerCase().contains("bootrom"))
				update("bootrom",it)
			else if(it.toLowerCase().contains(".dat"))
				update("license",it)
		}
		c_reboot()		
		return true
		
	}

}
