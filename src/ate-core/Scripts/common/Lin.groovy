package common;
import java.io.*;
import java.net.*
import ate.sandbox.cli.*
import ate.util.*
import ate.rm.dev.*
/**
 * 封装linux下的CLI
 * 
 * 20140113: Linux故障注入：
 * See http://bigdata-blog.net/2013/12/13/%E6%A8%A1%E6%8B%9F%E6%9C%BA%E5%99%A8%E6%95%85%E9%9A%9C/
 * See http://lxr.free-electrons.com/source/Documentation/fault-injection/fault-injection.txt
 * See https://www.linux.com/learn/tutorials/442451-inspecting-disk-io-performance-with-fio/
 * @author Administrator
 */
class Lin extends Linux{
	/**
	 * 性能优先
	 * @param loop
	 * @param seconds
	 * @param file
	 * @return
	 */
	 
	def http_load_parallel(loop,seconds,file){
		return exec0("http_load -parallel $loop -seconds $seconds $file")
	}
	/**
	 * -c  客户端数目
	 * -i  运行几次测试，多次取平均值
	 * --number-of-queries=50000：query次数
	 * @return
	 */
	def mysqlslap(){
		switchTo("")
		return exec("mysqlslap -uroot -p123456 -h localhost -c 10,50,100,200,400 -i 2 --engine=innodb --auto-generate-sql-load-type=mixed --number-of-queries=50000 --number-char-cols=5 --number-int-cols=5 --auto-generate-sql")
	}
	
	/**
	 * 每秒速率
	 * @param loop
	 * @param seconds
	 * @param file
	 * @return
	 */
	def http_load_rate(loop,seconds,file){
		switchTo("")
		return exec("http_load -rate $loop -seconds $seconds $file")
	}
	
	def sysbench_io(){
		switchTo("")
		def rtn=""
		rtn+=exec("sysbench --test=fileio --file-total-size=150G prepare")
		rtn+=exec("sysbench --test=fileio --file-total-size=150G --file-test-mode=rndrw "+
			               "--init-rnd=on --max-time=300 --max-request=0 run")
		rtn+=exec("sysbench --test=fileio --file-total-size=150G cleanup")
		return rtn
	}
	
	def sysbench_cpu(){
		switchTo("")
		return exec("sysbench --test=cpu --cpu-max-prime=20000 run")
	}
	def sysbench_oltp(){
		switchTo("")
		def rtn=""
		//为基准测试做准备数据
		rtn+=exec("sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test "+
			               "--mysql-user=root prepare")
		
		rtn+=exec("sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test "+
			               "--mysql-user=root --max-time=60 --oltp-read-only=on --max-requests=0 "+
			               "--num-threads=8 run")
		return rtn
	}
	def dbt2_(){
			
	}
	/**
	 * yum install mysql-bench
	 * @return
	 */
	def sql_bench_all(){
		switchTo("/usr/share/sql-bench")
		return exec("run-all-test --server=mysql --user=root --log --fast")
	}
	def sql_bench_insert(){
		switchTo("/usr/share/sql-bench")
		return exec("test-insert")
	}
	/**
	 * yum install perl
	 * @param home
	 * @param log
	 * @return
	 */
	def mysql_dump_slow(home,log){
		switchTo(home)
		//访问次数最多的20个sql
		def rtn=exec("mysqldumpslow -s c -t 20 $log")
		//返回记录集最多的20个sql
		rtn+=exec("mysqldumpslow -s r -t 20 $log")
		rtn+=exec("mysqldumpslow -s t -t 10 $log")
		///path/mysqldumpslow -s t -t 10 -g “left join” /database/mysql/slow-log
		return rtn
	}
	/**
	 * 进程运行时间
	 */
	def uptime(process){
		return exec("ps -eo pid,comm,lstart,etime,time,args|grep -i $process |awk '{print \$8}' | head -n 1")
	}
	/**
	 * 限制进程CPU使用率为percent
	 * @param pid
	 * @param percent
	 * @return
	 */
	def cpu_limit(pid, percent){
		String rst=exec0("cpulimit -p "+pid+" -l "+percent);
		log.info(rst);
		return rst;
	}
	/**
	 * tc qdisc add dev eth0 root tbf rate 5800kbit latency 50ms burst 1540
	 * @param dev
	 * @param rate
	 * @param latency
	 * @param burst
	 * @return
	 */
	def network_bandwidth_limit( dev, rate, latency, burst){
		String rst=exec0("tc qdisc add dev "+dev+" root tbf rate "+rate+" latency "
				+latency+" burst "+burst);
		return rst;
	}
	/**
	 * tc qdisc del dev eth0 root tbf rate 5800kbit latency 50ms burst 1540
	 * @param dev
	 * @param rate
	 * @param latency
	 * @param burst
	 * @return
	 */
	def network_bandwidth_limit_restore(dev, rate, latency, burst){
		String rst=exec0("tc qdisc del dev "+dev+" root tbf rate "+rate+" latency "
				+latency+" burst "+burst);
		return rst;
	}
	/**
	 * tc qdisc add dev eth0 root netem delay 300ms
	 * @param dev
	 * @param delay
	 * @return
	 */
	def network_delay(dev, delay){
		String rst=exec0("tc qdisc add dev "+dev+" root netem delay "+delay);
		return rst;
	}
	/**
	 * tc qdisc del dev eth0 root netem delay 300ms
	 * @param dev
	 * @param delay
	 * @return
	 */
	def network_delay_restore(dev, delay){
		String rst=exec0("tc qdisc del dev "+dev+" root netem delay "+delay);
		return rst;
	}
	
	/**
	 * tc qdisc add dev eth0 root netem corrupt 5%
	 * @param dev
	 * @param percent
	 * @return
	 */
	def network_package_corrupt(dev, percent){
		return exec0("tc qdisc add dev "+dev+" root netem corrupt "+percent);
	}
	
	/**
	 * tc qdisc del dev eth0 root netem corrupt 5%
	 * @param dev
	 * @param percent
	 * @return
	 */
	def network_package_corrupt_restore(dev, percent){
		return exec0("tc qdisc add dev "+dev+" root netem corrupt "+percent);
	}
	/**
	 *  tc qdisc add dev eth0 root netem loss 5%
	 * @return
	 */
	def network_package_lost(dev, percent){
		return exec0("tc qdisc add dev "+dev+" root netem loss "+percent);
	}
	
	/**
	 *  tc qdisc del dev eth0 root netem loss 5%
	 * @return
	 */
	def network_package_lost_restore(dev, percent){
		return exec0("tc qdisc del dev "+dev+" root netem loss "+percent);
	}
	/**
	 * iptables -A OUTPUT -p tcp --dport 3306 -j DROP
	 * @return
	 */
	def network_unavailable(protocol,port){
		return exec0("iptables -A OUTPUT -p "+protocol+" --dport "+port+" -j DROP");
	}
	/**
	 * iptables -D OUTPUT -p tcp --dport 3306 -j DROP
	 * @return
	 */
	def network_unavailable_restore(protocol,port){
		return exec0("iptables -D OUTPUT -p "+protocol+" --dport "+port+" -j DROP");
	}
	
	def route_print(){
		switchTo("")
		return exec0("ip route")
	}
	
	/**
	 * 每秒Context Switch
	 * @return
	 */
	def stap_csw(){
		String rst=exec0("stap -e 'global cnt; probe scheduler.cpu_on {cnt<<<1;} probe timer.s(1){printf(\"%d\n\", @count(cnt)); delete cnt;}'");
		log.info(rst);
		return rst;
	}
	
	def stap_latencytap(pid){
		String rst=exec0("stap -DSTP_NO_OVERLOAD --all-modules -DMAXSKIPPED=1024  /usr/share/doc/systemtap-client-1.8/examples/profiling/latencytap.stp");
		log.info(rst);
		return rst;
	}
	
	def processor(){
		return Integer.parseInt(exec0("grep processor /proc/cpuinfo | wc -l"));
	}
	
	def sysinfo(){
		def tmp=exec0("cat /proc/loadavg")+"\r\n";
		tmp+=exec0("cat /proc/stat")+"\r\n";
		tmp+=exec0("cat /proc/vmstat | grep -E \"pswpin|pswpout\"")+"\r\n";
		
		return tmp;
	}
	public static void main(String[] args){
		Lin lin=new Lin()
		//println win.start_dynamips()
		println lin.ls("")
	}
}
