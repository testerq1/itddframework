package common;
import static org.testng.AssertJUnit.assertTrue;

import java.io.*;
import java.net.*
import ate.sandbox.cli.*
import ate.util.*
import ate.rm.dev.*

/**
 * 封装Windows本地命令
 * @author 黄小勇
 *
 */
public class Win extends Windows{
	
	def start_dynamips(topo){
		switchTo("dynamips")
		println "start_dynamips"
		println exec_exit("start_dyn.bat $topo")		
	}
	
	def stop_dynamips(topo){
		switchTo("dynamips")
		println "stop_dynamips"
		println exec_exit("stop_dyn.bat $topo")
	}
	
	def getConections(app){
		switchTo("batch")
		return exec("findc.bat $app")
	}
	
	def findp(paras){
		switchTo("batch")
		return exec("findc.bat $paras")
	}
	
	def add_loopback(){
		switchTo("batch")
		return exec("devcon.bat -r install %WINDIR%\\Inf\\Netloop.inf *MSLOOP")
	}
	
	def remove_loopbacks(){
		switchTo("batch")
		return exec("devcon.bat /r remove =net *MSLOOP")
	}
	
	/**
	 * 给定一个IP，获得该IP所在的网卡名，由于通过jvm得到的网卡名不被windows shell所认可，所以需要用该函数获得
	 * todo 适配非Windows的情况.
	 *
	 * @param ip the ip
	 * @return the device name
	 */
	def get_device_name(ip){
		def name=rm.get("nic_name_"+ip)	 
		if(name!=null)
			return name
			
		def info=exec("netsh interface ip show address").split("\n")			
		for(int i=0;i<info.length;i++){
			def tmp=info[i].trim();
			if(tmp.startsWith("接口")||tmp.startsWith("interface")){
				name=tmp.substring(tmp.indexOf('"')+1,tmp.lastIndexOf('"'));
			}			
			if(tmp.endsWith(ip)){
				rm.put("nic_name_"+ip, name)
				return name
			}
		}		
		log.warn(ip+"不是本机IP!");
		return null
	}
	
	/**
	 * add一个路由
	 * add_route("172.16.5.111","255.255.255.255","192.168.1.1","1")
	 *   c:\>route add 172.16.5.111 mask 255.255.255.255 192.168.1.1 metric 1 
	 * @param net 网络
	 * @param mask 掩码
	 * @param gateway 
	 * @param metric
	 * @return bool
	 */
	def add_route(net,mask,gateway,metric) {	
		switchTo("")
		return exec("route add $net mask $mask  $gateway metric $metric","")
	}
	
	/**
	 * 删除网络路由
	 * Usage：
	 * 	delRoute("172.16.5.111")
	 *  c:\>route delete 172.16.5.111
	 * @param net
	 * @return bool
	 */		
	def del_route(String net) {
		switchTo("")
		return exec("route delete $net","");
	}
	
	/**
	 * c:\>route print
	 */
	public def route_print(){
		switchTo("")
		return exec("route print")	
	}
	
	def ping_lost(host,option){
		switchTo("")
		def rtn=exec("ping $option $host")		
	}
	
	def arp(ip){
		switchTo("")
		def rtn=exec("arp -a $ip")
		if(expect.contains(rtn,"No ARP Entries Found")){
			println ping(ip,"")
			rtn=exec("arp -a $ip")
		}
		def mac=null;
		rtn.eachLine {			
			if(it.contains(ip+" ")){
				println "-------"+it
				mac=split(it,"\\s+")[1]
			}
		}
		return mac
	}
	
	def disable_nic(inf_ip){
		switchTo("batch")
		String name=get_device_name(inf_ip)
		
		return exec("netsh interface set interface $name DISABLED")	
	}
	
	/**
	 * @see #add_ip(String, String, String)
	 * @param target
	 * @param address
	 * @return
	 */
	def add_ip(String target,String address){
		switchTo("")
		return add_ip(target,address,"255.255.255.0");
	}
	
	/**
	 *  ECHO Setting Gateway
		netsh int ip set address name = "Local Area Connection" gateway = %vargw% gwmetric = 1
		
		ECHO Setting Primary DNS
		netsh int ip set dns name = "Local Area Connection" source = static addr = %vardns1%
		
		ECHO Setting Secondary DNS
		netsh int ip add dns name = "Local Area Connection" addr = %vardns2%
	 * @param target
	 * @param address
	 * @param mask
	 * @return
	 */
	def add_ip(String target,String address,String mask){
		switchTo("")
		String name=get_device_name(target);
		assertTrue(name!=null);
		
		return exec("netsh int ip add address name=\"$name\" addr=$address mask=$mask")
	}
	
	def set_mtu(inf_ip,mtu){
		switchTo("")
		String name=get_device_name(inf_ip)
		assertTrue(name!=null)
		
		return exec("netsh interface ipv4 set interface \"$name\" mtu=$mtu")
	}
	
	/**
	 * add_ip的逆操作
	 * @param address
	 * @return
	 */
	def delete_ip(String address){
		switchTo("")
		String name=get_device_name(address);
		assertTrue(name!=null);
		
		return exec("netsh int ip delete address \"$name\" addr=$address");
	}
	
	public static void main(String[] args){
		Win win=new Win()
		println win.which("find")
		println win.ls(".")
		//println win.start_dynamips()
//		println win.start_dynamips("preset_01")
//				
//		Thread.currentThread().sleep(50000)
//		
//		println win.stop_dynamips("preset_01")
		
		//Win.ping("www.google.com","")		
//		println win.arp("172.16.5.1")
//		println win.add_route("172.16.5.114","255.255.255.255","192.168.1.1","20")
//		println win.del_route("172.16.5.114")
//		
//		System.out.println(win.add_ip("192.168.1.100","192.168.1.101"));
//		System.out.println(win.delete_ip("192.168.1.101"));
		
	}
}
