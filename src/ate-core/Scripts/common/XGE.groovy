package common;
import ate.util.*
import ate.rm.dev.*

/**
 * NFE的类 
 * @author huangxy
 *
 */
def class XGE extends DUT{
	//debug开关，为true时，不执行初始配置任务
	def isConfiged=false
	
	public XGE(connstr,section){
		super(connstr)		
		create_testbed(section)
        vt.login(arg("PROMPT"),arg("SUCCESS"));
	}	
	
	
	def enter_mode(mode){
		vt.send_line(mode);	
	}	

}