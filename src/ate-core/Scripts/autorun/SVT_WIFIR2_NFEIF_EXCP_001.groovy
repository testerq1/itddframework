/** 
*@summary 用户信息中的特殊字符
*@description 用户信息带特殊字符时处理正常
*
*<p>
*@pre-set 基础测试床
*
*<p>
*@author 
*@since  2012-5-30 17:43:21
*@product 
*@revision 
*    日期                    修改人                       修改说明
*   
*/
import ate.testcase.*
import org.testng.annotations.*
import org.testng.TestNG
import org.testng.TestListenerAdapter
import static org.testng.AssertJUnit.*;
import java.lang.reflect.*;

import ate.util.*
import common.*

public class SVT_WIFIR2_NFEIF_EXCP_001 extends CsUATestcase{
    def SAMPLES=[
//         ["SVT_WIFIR2_NFEIF_EXCP_001_0",
//              [场景:"多个空格"
//         ]],
         ["SVT_WIFIR2_NFEIF_EXCP_001_1",
              [场景:"中文"
         ]]
     ]
	
    @Test(dataProvider="Data_TestStep",groups=["FUNC"])
    public void TestStep(name,samples){
        log.info "${STEP}. 用户信息中的特殊字符:$name"
		def username=""
		
		if(samples.get("场景").contains("多个空格")){
			username="abc                                        cde"
		}else if(samples.get("场景").contains("中文")){
			username="中国"
		}

        //@STEP 按照样本点构造数据包
        //@EXPECT OK
        log.info "${STEP}.1 按照样本点构造数据包,预期结果为:OK"
		Plugin2NFE pn= new Plugin2NFE()
		pn.add_user("11.0.0.2", "ap_2", username)

		
        //@STEP 将数据发送给NFE
        //@EXPECT OK
        log.info "${STEP}.2 将数据发送给NFE,预期结果为:OK"
		println pn.get_message()
		put_string(pn.get_message())
		assertTrue send_buffer()		


        //@STEP 在Web模式下查看AP信息，确认和下发的数据一致，多个空格可以被正确处理
        //@EXPECT OK
        log.info "${STEP}.3 在Web模式下查看AP信息，确认和下发的数据一致，多个空格可以被正确处理,预期结果为:OK"
		WEB.select_tree_node(["AP流控","在线AP/IP"])
		def table_value=WEB.walk_table("table1",[3,4],[2,4],2)
		assertTrue(expect(table_value,"ap_2 11.0.0.2"))

    }

    @DataProvider(name="Data_TestStep")
    public Object[][] Data_TestStep(){
        return SAMPLES;
    }	
	
	def WEB
	def NFE
	@BeforeClass
	public void BeforeClass(){
		NFE=get_device("NFE")
		def HOST=get_device("HOST")
		WEB=NFE.WEB
		def mgt_ip=NFE.get("WEB_IP")
		set_remote_address("tcp:$mgt_ip/50002")
		
		super.BeforeClass()
	}
	
	@BeforeMethod
	public void BeforeMethod(Method m){
		super.BeforeMethod(m);
	}
	
    @AfterMethod
    public void AfterMethod(Method m){		
		NFE.clear_ap_online()
		WEB.select_tree_node(["系统配置"])
        super.AfterMethod(m);	   
    }
	
	@AfterClass
	public void AfterClass(){
	   super.AfterClass();
	}
}
