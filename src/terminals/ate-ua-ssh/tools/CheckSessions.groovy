import ate.cli.*
import ate.util.*
import common.*
/**
 * F:\java\MCTool>bin\rund.bat tools\SessChk.groovy app dut host
 * 
 * F:\java\MCTool>bin\rund.bat tools\SessChk.groovy qq 172.16.5.1 172.16.5.111 
 * @author Administrator
 *
 */
public class SessChk extends AbstractTool{
	Win win
	def CLI	
	String app="qq"	
	def host="172.16.5.111"
	int cnt=0
	def svcs
	def protocols=["6":"tcp","17":"udp","1":"icmp"]
	
	public SessChk(args){	
		this.app=args[0]
		svcs=loadProps("tools/server.xls.txt")		
		win = new Win()		
		CLI=CLIUtil.spawn("ssh",args[1],"2222","debug","_NTc@2010*")
		CLI.multiTimeout(20)
		CLI.setLoggerOn(false)
		CLI.setBadCommand(".*(Error|Ambi).*")
		CLI.init([LOGIN_PROMPT:">"])		
		
		if(args.size()>2)
			this.host=args[2]
		println "$app ${args[1]} $host"	
	}
	
	public void run(){
		if(!CLI.login())
			return
			
		cnt++
		println "===================Round $cnt at ${new Date()}==================="
		def map=new HashMap()
		def rtn=win.getConections(app)
		def lines=0
		def udp_cnt=0
		def tcp_cnt=0
		rtn.eachLine {
			def ds = it.split("\\s+|:")		
			if(ds.size()>0&&(ds[0].startsWith("TCP")||ds[0].startsWith("UDP")))
				lines++
			
			if(ds.length==5){				
				if(ds[2]=="0.0.0.0")
					ds[2]=host
				if(ds[2]==host){
					udp_cnt++
					map.put("udp "+ds[2]+"|"+ds[3],ds[1])
				}
			}else if(ds.length==6){
				if(ds[2]==host){
					tcp_cnt++
					map.put("tcp "+ds[2]+"|"+ds[3]+" "+ds[4]+"|"+ds[5],ds[1])
				}
			}
		}
		println "App total session: $lines, TCP $tcp_cnt, UDP $udp_cnt"
		
		Thread.currentThread().sleep(3000)
		def session = CLI.exec("dshell show session-chip")
		while(session==null){
			session = CLI.exec("")
			session = CLI.exec("dshell show session-chip")			
		}
		println "dshell end at "+new Date()
		lines=0
		session.eachLine {
			lines++
			def ds=it.split("\\s+")
			if(ds.length>=13){
				if(ds[1]==host){
					//println it
					printInfo(ds,0,map)
				}else if(ds[2]==host){
					//println it
					printInfo(ds,1,map)
				}
			}
		}
		if(lines<10)
			println "ChkSession debug: "+session
		println "DUT total session: $lines"
		
		map.keySet().each{
			if(it.startsWith("tcp"))
				println "Warning: DUT not know $app session:$it"
		}
	}
	def printInfo(ds,orig,map){		
		def id
		def protocol=""
		if(ds[5]=="17"){			
			if(orig==0){
				id="udp "+ds[1]+"|"+ds[3]
				//print " Should be "+win.findp("udp $host ${ds[3]}")
			}else{
				id="udp "+ds[2]+"|"+ds[4]
				//print " Should be "+win.findp("udp $host ${ds[4]}")
			}
		}else if(ds[5]=="6"){			
			if(orig==0){
				id="tcp "+ds[1]+"|"+ds[3]+" "+ds[2]+"|"+ds[4]
				//print " Should be "+win.findp("tcp ${ds[1]}:${ds[3]}")
			}else{
				id="tcp "+ds[2]+"|"+ds[4]+" "+ds[1]+"|"+ds[3]
				//print " Should be "+win.findp("tcp ${ds[2]}:${ds[4]}")
			}
		}else{
			return
		}
	
		if(map.get(id)!=null){
			println "DUT has $app session: from "+ds[1]+":"+ds[3]+" to "+ds[2]+":"+ds[4]+" "+getProp(protocols,ds[5])+" as "+getProp(svcs,ds[8])			
			map.remove(id)
		}else{
			print "."
			//println "DUT has session: from "+ds[1]+":"+ds[3]+" to "+ds[2]+":"+ds[4]+" "+getProp(protocols,ds[5])+" as "+getProp(svcs,ds[8])
		}
	}
	def printInfo1(ds,orig,map){		
		print "DUT has session: from "+ds[1]+":"+ds[3]+" to "+ds[2]+":"+ds[4]+" "+getProp(protocols,ds[5])+" app "+getProp(svcs,ds[8])
		def protocol=""
		if(ds[5]=="17"){
			def id
			if(orig==0){
				id="udp "+ds[1]+"|"+ds[3]
				print " Should be "+win.findp("udp $host ${ds[3]}")
			}else{
				id="udp "+ds[2]+"|"+ds[4]
				print " Should be "+win.findp("udp $host ${ds[4]}")
			}
			
			if(map.get(id)!=null){
				//println "Warning: this session should be "+map.get(id)
				map.remove(id)
			}
		}else if(ds[5]=="6"){
			def id
			if(orig==0){
				id="tcp "+ds[1]+"|"+ds[3]+" "+ds[2]+"|"+ds[4]
				print " Should be "+win.findp("tcp ${ds[1]}:${ds[3]}")
			}else{
				id="tcp "+ds[2]+"|"+ds[4]+" "+ds[1]+"|"+ds[3]
				print " Should be "+win.findp("tcp ${ds[2]}:${ds[4]}")
			}
			
			if(map.get(id)!=null){
				//println "Warning: this session should be "+map.get(id)
				map.remove(id)
			}
		}
	}
	public static void main(String[] args){
		//SessChk cron=new SessChk(["qq","172.16.10.1","172.16.5.111"])
		SessChk cron=new SessChk(args)
		while(true){
			try{
				cron.run()
			}catch(e){
				cron.sleep(5000);
			}
		}
	}
}