package ate.test;

/*
 * #%L
 * iTDD UA SSH
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.Method;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.Global;
import ate.rm.dev.DUT;
import ate.testcase.UATestcase;
import ate.ua.ssh.SSHUA;

public class SSHUATest extends UATestcase {
    
    class DefaultDUT extends DUT{

        public DefaultDUT(String connstr) {
            super(connstr);
        }
        
    }
    @Test
    public void login_with_section(){
        Global.verbose=true;
        DUT dut=new DefaultDUT("ssh://root:passwd@localmaven/?section=DUT#n");
        log.info(dut.get_vt().send_and_expect("ls --color=never"));
        dut.quit();
        sleep(20000);
        dut.restart();
        log.info(dut.get_vt().send_and_expect("ls --color=never"));
    }    
    
	//@Test
	public void login() {
		SSHUA ssh = create_ua("ssh://root:passwd@localmaven/");
		this.start_all_ua();
		assertTrue(ssh.is_ready());
		ssh.login("(]\\$ |: |]# |])$", "root]");
		ssh.clear_q();
		int i=10;
		while(i-->0){
		    assertTrue(ssh.get_msg_q().size()==0);
		    STEP("send ls test");
		    ssh.send_line("ls --color=never");		
		    assertTrue(ssh.is_ok());	
		    assertTrue(ssh.get_msg_q().size()==0);
		}
	}
	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();

		super.AfterMethod(m);
	}
}
