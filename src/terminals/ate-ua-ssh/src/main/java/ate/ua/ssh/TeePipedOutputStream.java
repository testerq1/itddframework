package ate.ua.ssh;

/*
 * #%L
 * iTDD UA SSH
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.io.OutputStream;
import java.io.PipedOutputStream;

public class TeePipedOutputStream extends PipedOutputStream {
	private OutputStream tee;

    public TeePipedOutputStream(OutputStream tee) {
        this.tee = tee;
    }

    @Override
    public void write(int b) throws IOException {
        super.write(b);
        tee.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        super.write(b, off, len);
        tee.write(b, off, len);
    }
}
