package ate.ua.ssh;

/*
 * #%L
 * iTDD UA SSH
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.security.Security;

import org.apache.sshd.ClientChannel;
import org.apache.sshd.ClientSession;
import org.apache.sshd.SshClient;
import org.apache.sshd.client.future.AuthFuture;
import org.apache.sshd.client.future.ConnectFuture;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import ate.AbstractURIHandler;
import ate.Global;
import ate.ua.AbstractVTUAImp;
import ate.ua.RunFailureException;
import ate.ua.UAOption;

public class SSHUA extends AbstractVTUAImp {
    SshClient client;
    ClientChannel channel;
    ByteArrayOutputStream sent = new ByteArrayOutputStream();
    PipedOutputStream pipedIn = new TeePipedOutputStream(sent);
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    private String screen = "";

    @Override
    public AbstractURIHandler build(UAOption option, Object value) {
        super.build(option, value);

        if (option != UA_URI)
            return this;

        Security.addProvider(new BouncyCastleProvider());
        if (port <= 0)
            port = 22;
        client = SshClient.setUpDefaultClient();
        client.setSessionFactory(new FailOverSessionFactory(this));
        client.start();

        return this;
    }

    public void fireSessionClosedEvent() {
        if (quit.get())
            return;

        int tts = RETYR_TIMES;
        quit.set(true);
        channel.close(true);

        client.stop();
        channel = null;
        while (tts-- > 0 && channel == null) {
            log.warn("try startup again after 5s...");
            try {
                pipedIn.close();
                pipedIn = null;
                reader.join();
            } catch (Exception e) {

            }
            sleep(5000);
            pipedIn = new TeePipedOutputStream(sent);
            client.start();
            startup();
        }
    }

    @Override
    public String get_default_schema() {
        return "ssh";
    }

    @Override
    public boolean is_ready() {
        return channel != null;
    }

    /**
     * 登陆
     * 
     * @param prompt
     *            prompt匹配符，用于匹配新的光标行，包括输入密码时或者登陆成功时的提示符
     * @param success_flag
     *            config shell的prompt匹配符，登陆成功后看到的提示符
     * @see #set_line_endwith(String)
     */
    public boolean login(Object prompt, String success_flag) {
        log.debug("ssh login");
        
        if(prompt==null||success_flag==null)
            throw new RunFailureException("please set prompt,success_flag!");
        
        set_line_endwith(prompt);
        this.success_flag = success_flag;
        // send_line("");
        String screen = recv_message();
        
        if (get_fragment()!=null) {
            parseLineMode(get_fragment());
        } else {
            parseLineMode(screen);
        }

        if (expect.contains(screen, success_flag)) {
            set_line_endwith(success_flag);
            return true;
        } else {
            log.error("login failure screen={}, flag={}", screen, success_flag);
        }
        return false;
    }
    
    public boolean login(){
        return this.login(this.prompt,this.success_flag);
    }
    
    @Override
    public boolean restart() {
        pipedIn=new TeePipedOutputStream(sent);
        client = SshClient.setUpDefaultClient();
        client.setSessionFactory(new FailOverSessionFactory(this));
        client.start();
        this.startup();
        return login(this.line_endwith_pattern, success_flag);
    }

    @Override
    public void run() {
        while (!quit.get()) {
            String tmp = new String(out.toByteArray(), charset);
            out.reset();
            if (this.is_record && tmp != null && tmp.length() > 0) {
                screen = screen + tmp;
                if (expect.match_e(screen, this.line_endwith_pattern)) {
                    this.add_packet(screen);
                    screen = "";
                }
            } else {
                this.sleep(10);
            }

            if (tmp.trim().length() > 0 && Global.verbose) {
                log.debug("> {} {} ####", get_rcvd_count(), tmp);
                this.rcvd();
            }
        }
        log.debug("reader quit: {} {}", get_rcvd_count(), quit);
    }

    @Override
    public void startup() {
        ClientSession session = null;
        try {
            int tts = RETYR_TIMES;
            ConnectFuture cf = client.connect(host, port).await();

            while (!cf.isConnected() && tts-- > 0) {
                log.warn("connect failure,try again...");
                sleep(10000);
                cf = client.connect(host, port).await();
            }

            if (!cf.isConnected())
                throw new RunFailureException("ssh connect failure,addr = "
                        + host + ":" + port);

            session = cf.getSession();
            log.debug("ssh startup u/p: {}/{}", username, passwd);
            if (this.username == null || this.passwd == null)
                throw new RunFailureException("username or passwd missed");

            AuthFuture af = session.authPassword(this.username, this.passwd)
                    .await();

            tts = RETYR_TIMES;
            while (!af.isSuccess() && tts-- > 0) {
                log.warn("ssh auth failure,try again...");
                af = session.authPassword(this.username, this.passwd).await();
            }

            if (af.isFailure())
                throw new RunFailureException("ssh auth failure,u/p = "
                        + username + "/" + passwd);

            channel = session.createChannel(ClientChannel.CHANNEL_SHELL);
            this.outstr = pipedIn;
            channel.setIn(new PipedInputStream(pipedIn));
            channel.setOut(this.out);
            channel.setErr(this.out);
            channel.open();

            quit.set(false);
            reader = new Thread(this);
            reader.start();
        } catch (Exception e) {
            e.printStackTrace();
            if (channel != null)
                this.teardown();
            if (session != null) {
                session.close(true);
            }
        }
    }

    @Override
    public void teardown() {
        super.teardown();  
        try {
            this.reader.join();
            if (channel != null)
                channel.close(true);
            client.stop(); 
            out.close();
            sent.close();
            this.outstr.close();
            pipedIn.close();
        } catch (IOException e) {
        } catch (InterruptedException e) {           
            e.printStackTrace();
        }
    }
}
