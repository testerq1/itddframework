package ate.ua.ssh;

import org.apache.sshd.client.SessionFactory;
import org.apache.sshd.common.io.IoSession;
import org.apache.sshd.common.session.AbstractSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FailOverSessionFactory extends SessionFactory{
    protected static Logger log=LoggerFactory.getLogger(FailOverSessionFactory.class);
    SSHUA ua;
    public FailOverSessionFactory(SSHUA ua){
        this.ua=ua;
    }
    @Override
    public void sessionClosed(IoSession ioSession) throws Exception {
        log.debug("sessionClosed");
        AbstractSession.getSession(ioSession).close(true);
        ua.fireSessionClosedEvent();
    }
}
