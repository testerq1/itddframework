/**
 * 
 */
package ate.ua.telnet;

/*
 * #%L
 * iTDD UA Telnet
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;

import org.apache.commons.net.telnet.EchoOptionHandler;
import org.apache.commons.net.telnet.SuppressGAOptionHandler;
import org.apache.commons.net.telnet.TelnetClient;
import org.apache.commons.net.telnet.TerminalTypeOptionHandler;

import ate.AbstractURIHandler;
import ate.ua.AbstractVTUAImp;
import ate.ua.RunFailureException;
import ate.ua.UAOption;

/**
 * (telnet,ssh,serial)
 * telnet://root:passwd@localmaven/
 * telnet://root:passwd@localmaven/#r //r为换行符号(r,rn,n)
 * telnet://root:passwd@localmaven/?vt=VT100&section=DUT&ir=true 
 * telnet://root:passwd@localmaven/?ir=true&section=DUT#n
 * section:对应测试床ini文件中的section
 * ir:是否发送初始换行
 * #n：换行符为\n
 * @author ravi.huang
 */
public class TelnetUA extends AbstractVTUAImp {
	private TelnetClient tc;

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option != UA_URI)
			return this;
		
		if (port <= 0)
			port = 23;
		
		tc = new TelnetClient();
		String vt = "VT220";
		if (this.get_query_para("vt") != null)
			vt = get_query_para("vt");
		
		TerminalTypeOptionHandler ttopt = new TerminalTypeOptionHandler(vt, true, false, true, false);
        EchoOptionHandler echoopt = new EchoOptionHandler(true, false, false, false);
        SuppressGAOptionHandler gaopt = new SuppressGAOptionHandler(true, true, true, true);

        try
        {
            tc.addOptionHandler(ttopt);
            tc.addOptionHandler(echoopt);
            tc.addOptionHandler(gaopt);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
		tc.setReaderThread(true);
		return this;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see ate.ua.AUAImp#getDefaultSchema()
	 */
	@Override
	public String get_default_schema() {
		return "telnet";
	}

	@Override
	public void startup() {
		if (tc == null) {
			log.warn("please init telnet client first!");
			return;
		}
		if (tc.isConnected())
			return;
		try {
			tc.connect(this.host, this.port);
			
		} catch (Exception e) {
		    int tts=this.RETYR_TIMES;
			while(tts-->0&&!tc.isConnected())
			{
			    log.warn("connect failure,try again after 10s...");
			    sleep(10000);
			    try {
		            tc.connect(this.host, this.port);       
		        } catch (Exception e1) {}
			}
			if(!tc.isConnected())
			    throw new RunFailureException(e);
		}
        this.instr = new BufferedInputStream(tc.getInputStream());
        this.outstr = new BufferedOutputStream(tc.getOutputStream());
        
        quit.set(false);
        reader = new Thread(this);
        reader.start(); 
	}
	
	@Override
    public void send_message(String s){        
        try {
            outstr.write(s.getBytes());     
            outstr.flush();
        } catch (IOException e) {            
            int tts=this.RETYR_TIMES;
            log.warn("try resend \"{}\" after {}ms ",s,RESND_INTEVERAL);
            
            while(!restart()&&tts-->0){
                
            }
            if(!tc.isConnected())
                throw new RunFailureException(e);
            try {
                outstr.write(s.getBytes());     
                outstr.flush();
            } catch (IOException e1) {                
                throw new RunFailureException(e1);
            }
        }       
    }
	
	@Override
	public void teardown() {
	    super.teardown();
		try {
			this.tc.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean is_ready() {
		return tc != null && tc.isConnected();
	}	
}
