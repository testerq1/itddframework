package ate.test;

/*
 * #%L
 * iTDD UA Telnet
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.Method;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.Global;
import ate.rm.dev.DUT;
import ate.testcase.CsUATestcase;
import ate.testcase.UATestcase;
import ate.ua.telnet.TelnetUA;

public class TelnetUATest extends UATestcase {
    class DefaultDUT extends DUT{

        public DefaultDUT(String connstr) {
            super(connstr);
        }
        
    }
    @Test
	public void login_with_section(){
        Global.verbose=true;
	    DUT dut=new DefaultDUT("telnet://root:passwd@localmaven/?section=DUT#n");
	    log.info(dut.get_vt().send_and_expect("ls --color=never"));
	}
	
	public void login() {	    
		TelnetUA telnet = create_ua("telnet://root:passwd@localmaven/#n");
		this.start_all_ua();
		assertTrue(telnet.is_ready());
		String endwith="root]";
		telnet.login("login: ", "root", "assword: ", "passwd", "(]\\$ |: |# |])$",
				endwith);
		telnet.clear_q();
		int i=10;
        while(i-->0){            
            assertTrue(telnet.get_msg_q().size()==0);
        	STEP("send line");
            telnet.send_line("ls --color=never");            
            assertTrue(telnet.is_ok());
            assertTrue(telnet.get_msg_q().size()==0);
		}
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();

		super.AfterMethod(m);
	}

}
