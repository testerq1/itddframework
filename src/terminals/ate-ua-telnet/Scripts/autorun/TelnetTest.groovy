import org.testng.annotations.Test;

import java.lang.reflect.Method;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.testcase.UATestcase;

public class TelnetTest extends UATestcase {
	@Test
	public void telnet_login() {
		STEP("telnet_login")		
		def ua = create_ua("telnet://root:passwd@localmaven/");
		this.start_all_ua();
		      this.start_all_ua();
        assertTrue(ua.is_ready());
        def endwith="root]";
        ua.login("login: ", "root", "assword: ", "passwd", /(]\\$ |: |# |])$/,
                endwith);
        ua.clear_q();
        assertTrue(ua.get_msg_q().size()==0);
        
        STEP("send line");
        ua.send_line("ls --color=never");            
        assertTrue(ua.is_ok());
        assertTrue(ua.get_msg_q().size()==0);
        
        STEP("send_and_expect line2");
        def rtn=ua.send_and_expect("ls --color=never")
        println rtn
        assertTrue(rtn!=null);
        assertTrue(ua.get_msg_q().size()==0);
        
	}
			
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();

		super.AfterMethod(m);
	}
}
