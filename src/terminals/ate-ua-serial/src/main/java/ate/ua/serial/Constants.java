package ate.ua.serial;

import gnu.io.SerialPort;

import java.security.InvalidParameterException;

import org.apache.mina.transport.serial.SerialAddress.DataBits;
import org.apache.mina.transport.serial.SerialAddress.FlowControl;
import org.apache.mina.transport.serial.SerialAddress.Parity;
import org.apache.mina.transport.serial.SerialAddress.StopBits;

class Constants {
    static int parseBauds(String value){
        if(value==null||value.length()==0)
            return 38400;
        
        return Integer.parseInt(value);
    }
    
    static DataBits parseDataBits(String value) {
        if(value==null||value.length()==0)
            return DataBits.DATABITS_8;
        
        return DataBits.valueOf("DATABITS_" + value);
    }

    static FlowControl parseFlowControl(String value) {
        if(value==null||value.length()==0)
            return FlowControl.NONE;
        
        return FlowControl.valueOf(value.toUpperCase());
    }

    static Parity parseParity(String value) {
        if(value==null||value.length()==0)
            return Parity.NONE;
        
        return Parity.valueOf(value.toUpperCase());
    }

    static StopBits parseStopBits(String value) {
        if(value==null||value.length()==0)
            return StopBits.BITS_1;
        
        if (value.equals("1.5"))
            value = "1_5";
        return StopBits.valueOf("BITS_" + value);
    }
    
    static int getDataBitsForRXTX(DataBits dataBits) {
        switch (dataBits) {
        case DATABITS_5:
            return SerialPort.DATABITS_5;
        case DATABITS_6:
            return SerialPort.DATABITS_6;
        case DATABITS_7:
            return SerialPort.DATABITS_7;
        case DATABITS_8:
            return SerialPort.DATABITS_8;
        }
        throw new InvalidParameterException("broken databits");
    }

    static int getFLowControlForRXTX(FlowControl flowControl) {
        switch (flowControl) {
        case NONE:
            return SerialPort.FLOWCONTROL_NONE;
        case RTSCTS_IN:
            return SerialPort.FLOWCONTROL_RTSCTS_IN;
        case RTSCTS_OUT:
            return SerialPort.FLOWCONTROL_RTSCTS_OUT;
        case RTSCTS_IN_OUT:
            return SerialPort.FLOWCONTROL_RTSCTS_IN
                    | SerialPort.FLOWCONTROL_RTSCTS_OUT;
        case XONXOFF_IN:
            return SerialPort.FLOWCONTROL_XONXOFF_IN;
        case XONXOFF_OUT:
            return SerialPort.FLOWCONTROL_XONXOFF_OUT;
        case XONXOFF_IN_OUT:
            return SerialPort.FLOWCONTROL_XONXOFF_IN
                    | SerialPort.FLOWCONTROL_XONXOFF_OUT;
        }
        throw new InvalidParameterException("broken flow control");
    }

    static int getParityForRXTX(Parity parity) {
        switch (parity) {
        case EVEN:
            return SerialPort.PARITY_EVEN;
        case MARK:
            return SerialPort.PARITY_MARK;
        case NONE:
            return SerialPort.PARITY_NONE;
        case ODD:
            return SerialPort.PARITY_ODD;
        case SPACE:
            return SerialPort.PARITY_SPACE;
        }
        throw new InvalidParameterException("broken parity");
    }

    static int getStopBitsForRXTX(StopBits stopBits) {
        switch (stopBits) {
        case BITS_1:
            return SerialPort.STOPBITS_1;
        case BITS_1_5:
            return SerialPort.STOPBITS_1_5;
        case BITS_2:
            return SerialPort.STOPBITS_2;
        }
        throw new InvalidParameterException("broken stopbits");
    }
}
