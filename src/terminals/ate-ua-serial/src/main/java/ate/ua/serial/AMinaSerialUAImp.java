package ate.ua.serial;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.SerialPort;

import java.net.SocketAddress;

import org.apache.mina.transport.serial.SerialAddress;
import org.apache.mina.transport.serial.SerialAddress.DataBits;
import org.apache.mina.transport.serial.SerialAddress.FlowControl;
import org.apache.mina.transport.serial.SerialAddress.Parity;
import org.apache.mina.transport.serial.SerialAddress.StopBits;

import ate.AbstractURIHandler;
import ate.ua.UAOption;
import ate.ua.mina.AMinaIOAdapter;
import ate.ua.mina.AMinaUAImp;

/**
 * 串口协议UA的父类
 * @author ravi huang
 *
 * @param <T>
 */
public abstract class AMinaSerialUAImp<T> extends AMinaUAImp<T>{
    int bauds;
    DataBits dataBits = DataBits.DATABITS_8;
    FlowControl flowControl = FlowControl.NONE;
    Parity parity = Parity.NONE;
    SerialAddress portAddress;
    CommPortIdentifier portIdentifier;
    SerialPort sc;
    StopBits stopBits = StopBits.BITS_1;
    
    @Override
    protected AMinaIOAdapter<T> create_io(int type){
        return new SerialUAClient(this);
    }
    
    @Override
    public AbstractURIHandler build(UAOption option, Object value) {
        super.build(option, value);

        if (option != UA_URI)
            return this;

        if (!host.toLowerCase().startsWith("com"))
            host = "/dev/" + host;
        
        bauds = Constants.parseBauds(get_query_para("bauds"));
        dataBits = Constants.parseDataBits(this.get_query_para("db"));
        parity = Constants.parseParity(get_query_para("parity"));
        stopBits = Constants.parseStopBits(get_query_para("sb"));
        flowControl = Constants.parseFlowControl(get_query_para("fc"));
        
        portAddress = new SerialAddress(host, bauds, dataBits, stopBits,
                parity, flowControl);
        try {
            portIdentifier = CommPortIdentifier.getPortIdentifier(host);
        } catch (NoSuchPortException e) {
            e.printStackTrace();
        }

        if (portIdentifier.isCurrentlyOwned())
            throw new Error("Error: Port is currently in use:" + host);
        return this;
    }
    
    @Override
    public SocketAddress getTargetAddress() {
        return portAddress;
    }
}
