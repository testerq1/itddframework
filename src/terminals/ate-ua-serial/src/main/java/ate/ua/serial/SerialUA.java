package ate.ua.serial;

/*
 * #%L
 * iTDD UA Serial
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.SerialPort;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.security.InvalidParameterException;

import org.apache.mina.transport.serial.SerialAddress;
import org.apache.mina.transport.serial.SerialAddress.DataBits;
import org.apache.mina.transport.serial.SerialAddress.FlowControl;
import org.apache.mina.transport.serial.SerialAddress.Parity;
import org.apache.mina.transport.serial.SerialAddress.StopBits;

import ate.AbstractURIHandler;
import ate.ua.AbstractVTUAImp;
import ate.ua.UAOption;

/**
 * serial://root:admin@COM1/?bauds=38400&db=8&sb=1&parity=none&fc=none" db:
 * DataBits 5~8 sb: StopBits {1 1.5 2} parity: {NONE, ODD, EVEN, MARK, SPACE}
 * fc: FlowControl {NONE, RTSCTS_IN, RTSCTS_OUT, RTSCTS_IN_OUT, XONXOFF_IN,
 * XONXOFF_OUT, XONXOFF_IN_OUT}
 * 
 * @author Administrator
 * 
 */
public class SerialUA extends AbstractVTUAImp {
	public static void main(String[] args) {
		SerialUA ua1 = new SerialUA();
		ua1.build(UA_URI,
				"serial://root:admin@COM1/?bauds=38400&db=8&sb=1&parity=none&fc=none");
		System.out.println(ua1);
		SerialUA ua2 = new SerialUA();
		ua2.build(UA_URI,
				"serial://root:admin@tty1/?bauds=384&db=5&sb=1.5&parity=odd&fc=XONXOFF_IN");
		System.out.println(ua2);
	}

	private int bauds;
	private DataBits dataBits = DataBits.DATABITS_8;
	private FlowControl flowControl = FlowControl.NONE;
	private Parity parity = Parity.NONE;
	SerialAddress portAddress;
	CommPortIdentifier portIdentifier;
	SerialPort sc;

	private StopBits stopBits = StopBits.BITS_1;

	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);

		if (option != UA_URI)
			return this;

		if (!host.toLowerCase().startsWith("com"))
			host = "/dev/" + host;
		bauds = Integer.parseInt(get_query_para("bauds"));
		dataBits = Constants.parseDataBits(this.get_query_para("db"));
		parity = Constants.parseParity(get_query_para("parity"));
		stopBits = Constants.parseStopBits(get_query_para("sb"));
		flowControl = Constants.parseFlowControl(get_query_para("fc"));
		portAddress = new SerialAddress(host, 38400, dataBits, stopBits.BITS_1,
				parity, flowControl);
		try {
			portIdentifier = CommPortIdentifier.getPortIdentifier(host);
		} catch (NoSuchPortException e) {
			e.printStackTrace();
		}

		if (portIdentifier.isCurrentlyOwned())
			throw new Error("Error: Port is currently in use:" + host);
		return this;
	}

	@Override
	public String get_default_schema() {
		return "serial";
	}

	@Override
	public boolean is_ready() {
		return this.sc != null;
	}

	@Override
	public void startup() {
		try {
			CommPort commPort = portIdentifier.open(this.getClass().getName(),
					2000);

			if (!(commPort instanceof SerialPort))
				throw new Error("Only serial ports are handled.");

			sc = (SerialPort) commPort;
			
			sc.setSerialPortParams(portAddress.getBauds(),
			        Constants.getDataBitsForRXTX(this.dataBits), 
			        Constants.getStopBitsForRXTX(this.stopBits),
			        Constants.getParityForRXTX(this.parity));
			sc.setFlowControlMode(Constants.getFLowControlForRXTX(this.flowControl));
			
			this.instr = new BufferedInputStream(sc.getInputStream());
			this.outstr = new BufferedOutputStream(sc.getOutputStream());

			reader = new Thread(this);
			reader.start();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void teardown() {
	    super.teardown();
		if (sc == null) {
			log.debug("serial client not init!");
			return;
		}		
		try {
			this.sc.close();
			sc = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return host + " (bauds: " + bauds + ", dataBits: " + dataBits
				+ ", stopBits: " + stopBits + ", parity: " + parity
				+ ", flowControl: " + flowControl + ")";
	}
	@Override
	public boolean restart(){
		return this.login(uname_p, username, passwd_p, passwd,prompt, success_flag);
	}
}
