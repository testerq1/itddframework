package ate.ua.serial;

import org.apache.mina.core.service.IoConnector;
import org.apache.mina.transport.serial.SerialConnector;

import ate.ua.mina.UAClient;

public class SerialUAClient<T> extends UAClient {
    
    public SerialUAClient(AMinaSerialUAImp<T> ua) {
        super(ua);
    }
    
    @Override
    protected IoConnector create_connector(String transport){
        IoConnector connector = new SerialConnector();     
        
        return connector;
    }
}
