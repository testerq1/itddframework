package ate.ua.serial;

import org.testng.annotations.Test;

import ate.testcase.UATestcase;

public class SerialUATest extends UATestcase{
    @Test
    public void basic(){
        SerialUA ua1 = create_ua("serial://root:admin@COM1/?bauds=38400&db=8&sb=1&parity=none&fc=none");
        System.out.println(ua1);
        SerialUA ua2 = create_ua("serial://root:admin@tty1/?bauds=384&db=5&sb=1.5&parity=odd&fc=XONXOFF_IN");
        System.out.println(ua2);
    }
}
