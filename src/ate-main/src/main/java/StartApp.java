import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

public class StartApp {

	public static boolean keeprunning = true;

	static {
		String home = System.getProperty("user.dir");
		if (System.getProperty("TTOOL_HOME") == null) {
			if (home.endsWith(File.separator + "bin"))
				home = home.substring(0, home.length() - 4);
			System.setProperty("TTOOL_HOME", home);
		}
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		System.out.println("HOME=" + System.getProperty("TTOOL_HOME"));

		try {
			Vector<URL> v = load_Folder(new File(
					System.getProperty("TTOOL_HOME") + "/lib"));
			ClassLoader urlClassLoader = null;
			if (v != null) {
				URL[] urls = new URL[v.size()];
				v.toArray(urls);
				urlClassLoader = new URLClassLoader(urls,
						ClassLoader.getSystemClassLoader());

				Thread.currentThread().setContextClassLoader(urlClassLoader);
			} else
				urlClassLoader = ClassLoader.getSystemClassLoader();

			// Method m = URLClassLoader.class.getDeclaredMethod("addURL",
			// new Class[] { URL.class });
			// m.setAccessible(true);
			// load_jars(urlClassLoader,m,urls);
			if (args.length == 0) {
				System.out.println("please input app class name!");
				System.exit(1);

			} else{
				Class cls = urlClassLoader.loadClass(args[0]);
				Method begin = cls.getDeclaredMethod("main", String[].class);
				String[] argg = new String[args.length - 1];
				System.arraycopy(args, 1, argg, 0, args.length - 1);
				begin.invoke(null, new Object[] { argg });
			} 
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	public static Vector<URL> load_Folder(File file) throws Exception {
		Vector<URL> vec = new Vector<URL>();

		if (!file.exists())
			return null;

		for (File tmp : file.listFiles())
			if (tmp.isFile() && tmp.getName().endsWith(".jar"))
				vec.add(tmp.toURI().toURL());
			else if (tmp.isDirectory())
				vec.addAll(load_Folder(tmp));
		return vec;
	}

	

}
