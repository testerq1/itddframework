package ate;

/*
 * #%L
 * iTDD Main
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

//service:jmx:rmi:///jndi/rmi://localhost:1099/jmxrmi

public class Main {
	public static boolean keeprunning = true;
	
	static {
		String home = System.getProperty("user.dir");
		if (System.getProperty("TTOOL_HOME") == null) {
			if (home.endsWith(File.separator + "bin"))
				home = home.substring(0, home.length() - 4);
			System.setProperty("TTOOL_HOME", home);
		}		
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		System.out.println("HOME="
				+ System.getProperty("TTOOL_HOME"));

		try {
			Vector<URL> v = load_Folder(new File(System.getProperty("TTOOL_HOME") + "/lib"));
			ClassLoader urlClassLoader=null;
			if(v==null){
				v=new Vector<URL>();				
				String[] cp=System.getProperty("java.class.path").split(File.pathSeparator);
				for(String tmp:cp){
					v.add(new File(tmp).toURI().toURL());
				}
			}
			URL[] urls = new URL[v.size()];
			v.toArray(urls);
			urlClassLoader = new URLClassLoader(urls,
					ClassLoader.getSystemClassLoader());

			Thread.currentThread().setContextClassLoader(urlClassLoader);
			
			// Method m = URLClassLoader.class.getDeclaredMethod("addURL",
			// new Class[] { URL.class });
			// m.setAccessible(true);
			// load_jars(urlClassLoader,m,urls);
			if (args.length == 0) {
				Thread th = new Thread((Runnable) urlClassLoader.loadClass(
						"ate.MainThread").newInstance());
				th.setContextClassLoader(urlClassLoader);
				th.start();

			} else if (args[0].equalsIgnoreCase("shell")) {
				Class cls = urlClassLoader.loadClass("ate.shell.Shell");
				Method begin = cls.getDeclaredMethod("main", String[].class);
				String[] argg = new String[args.length - 1];
				System.arraycopy(args, 1, argg, 0, args.length - 1);
				begin.invoke(null, new Object[] { argg });

			} else if (args[0].equalsIgnoreCase("runtools")) {
				Class cls = urlClassLoader
						.loadClass("ate.actions.RunTestScriptCommand");
				Method begin = cls.getDeclaredMethod("run_tools",
						String[].class);
				String[] argg = new String[args.length - 1];
				System.arraycopy(args, 1, argg, 0, args.length - 1);
				begin.invoke(cls.newInstance(), new Object[] { argg });

			} else {
				Class cls = urlClassLoader
						.loadClass("ate.actions.RunTestScriptCommand");
				Method begin = cls.getDeclaredMethod("run_testsuite",
						String[].class);

				begin.invoke(cls.newInstance(), new Object[] { args });
				System.exit(0);
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				if (args.length == 0) {
					ServerSocket ss = new ServerSocket(12345);
					while (keeprunning && args.length == 0) {
						Socket s = ss.accept();
						BufferedReader is = new BufferedReader(
								new InputStreamReader(s.getInputStream()));
						String cmd = is.readLine();
						ObjectOutputStream oo = new ObjectOutputStream(
								s.getOutputStream());
						System.out.println("Receive command from port 12345 :"
								+ cmd);
						if (cmd.equalsIgnoreCase("reboot")) {
							System.out.println("exit with code 99 from "
									+ s.getRemoteSocketAddress());
							oo.writeBytes("exit with code 99");
							System.exit(99);
						} else if (cmd.equalsIgnoreCase("update")) {
							System.out.println("exit with code 98 from "
									+ s.getRemoteSocketAddress());
							oo.writeBytes("exit with code 98");
							System.exit(98);
						} else {
							String rsp = "";
							if (cmd.equalsIgnoreCase("help"))
								rsp = "\ncmd: reboot will exit with code 99"
										+ "\ncmd: update will exit with code 98";
							else
								rsp = "\nnot supported cmd:" + cmd;
							System.out.println(rsp);
							oo.writeBytes(rsp);
							oo.close();
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(101);
			}
		}
	}

	public static Vector<URL> load_Folder(File file) throws Exception {
		Vector<URL> vec = new Vector<URL>();
		
		if(!file.exists())
			return null;
		
		for (File tmp : file.listFiles())
			if (tmp.isFile() && tmp.getName().endsWith(".jar"))
				vec.add(tmp.toURI().toURL());
			else if (tmp.isDirectory())
				vec.addAll(load_Folder(tmp));
		return vec;
	}

	public static void load_jars(ClassLoader sysloader, Method m, File file)
			throws Exception {
		for (File tmp : file.listFiles())
			if (tmp.isFile() && tmp.getName().endsWith(".jar"))
				m.invoke(sysloader, new Object[] { tmp.toURI().toURL() });
			else if (tmp.isDirectory())
				load_jars(sysloader, m, tmp);
	}

}
