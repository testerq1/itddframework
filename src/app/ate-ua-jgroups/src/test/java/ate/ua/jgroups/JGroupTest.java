package ate.ua.jgroups;

import org.jgroups.JChannel;
import org.jgroups.demos.Draw;
import org.jgroups.demos.ReplCacheDemo;
import org.jgroups.demos.ReplicatedHashMapDemo;
import org.jgroups.demos.wb.Whiteboard;
import org.jgroups.stack.AddressGenerator;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import ate.Global;
import ate.testcase.Testcase;

public class JGroupTest extends Testcase {
    String           props="./src/test/udp.xml";
    boolean          no_channel=false;
    boolean          jmx=true;
    boolean          use_state=false;
    long             state_timeout=5000;
    boolean          use_unicasts=false;
    String           name=null;
    boolean          send_own_state_on_merge=true;
    AddressGenerator generator=null;
    
    @Test
    public void replCacheDemo(){
        try {
            ReplCacheDemo.main(new String[]{"-props",props});
            ReplCacheDemo.main(new String[]{"-props",props});
            ReplCacheDemo.main(new String[]{"-props",props});
            ReplCacheDemo.main(new String[]{"-props",props});
        } catch (Exception e) {
            e.printStackTrace();
        }
        wait_yes();
    }
    
    @Test
    public void replicatedHashMapDemo(){
        ReplicatedHashMapDemo     client1=new ReplicatedHashMapDemo(),
        client2=new ReplicatedHashMapDemo();
        JChannel                  channel1,channel2;
        try {
            channel1=new JChannel(props);
            channel1.connect("ReplicatedHashMapDemo-Cluster");
            client1.start(channel1);
            wait_yes();
            
            channel2=new JChannel(props);
            channel2.connect("ReplicatedHashMapDemo-Cluster");
            client2.start(channel1);
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        wait_yes();   
    }
    
    @Test 
    public void whiteboard(){        
        new ApplFrame("Whiteboard Application1", new Whiteboard(props));
        new ApplFrame("Whiteboard Application2", new Whiteboard(props));
        wait_yes();        
    }
    
    @Test
    public void draw_state(){          
        use_state=true;
        Draw d1,d2;
        try {
            d1 = new Draw(props, no_channel, jmx, use_state, state_timeout, use_unicasts, name,
                    send_own_state_on_merge, generator);
            d1.go();
            wait_yes();
            d2 = new Draw(props, no_channel, jmx, use_state, state_timeout, use_unicasts, name,
                    send_own_state_on_merge, generator);
            d2.go();
            wait_yes();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }          
    }
    public void draw(){        
        Draw d1,d2;
        try {
            d1 = new Draw(props, no_channel, jmx, use_state, state_timeout, use_unicasts, name,
                    send_own_state_on_merge, generator);
            d1.go();
            wait_yes();
            
            d2 = new Draw(props, no_channel, jmx, use_state, state_timeout, use_unicasts, name,
                    send_own_state_on_merge, generator);
            d2.go();
            wait_yes();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }          
    }
    @BeforeClass
    public void BeforeClass() {
        Global.wait=true;
    }
    
}
