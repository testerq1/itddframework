package ate.ua.jgroups;

import java.awt.Frame;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import org.jgroups.demos.wb.Whiteboard;

public class ApplFrame extends Frame implements WindowListener, ComponentListener {
    Whiteboard wb = null;

    public ApplFrame(String title, Whiteboard wb) {
        super(title);
        this.wb = wb;
        add(wb);
        setSize(299, 299);
        setVisible(true);
        wb.init();
        setSize(300, 300);
        addWindowListener(this);
        addComponentListener(this);
    }


    public void windowOpened(WindowEvent e) {
    }

    public void windowClosing(WindowEvent e) {
        dispose();
        System.exit(0);
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
        wb.repaint();
    }

    public void windowActivated(WindowEvent e) {
        wb.repaint();
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void componentResized(ComponentEvent e) {
        wb.repaint();
    }

    public void componentMoved(ComponentEvent e) {
    }


    public void componentShown(ComponentEvent e) {
    }

    public void componentHidden(ComponentEvent e) {
    }


}