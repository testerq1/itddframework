package ate;

import org.testng.annotations.Test;

import ate.testcase.Testcase;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.Handler;

public class VertxTest extends Testcase {
    @Test
    public void start() {
        new Server().start();
        sleep(20000);
    }

    @Test
    public void testApp() {
        Vertx vertx = Vertx.vertx();
        HttpServerOptions httpServerOptions = new HttpServerOptions()
                .setMaxWebsocketFrameSize(1000000);
        httpServerOptions.setPort(12345);

        HttpServer httpServer = vertx.createHttpServer(httpServerOptions);

        httpServer.requestHandler(
                request -> {
                    request.response().putHeader("content-type", "text/html")
                            .end("Hello,Vertx app!");
                }).listen(listen -> {
            if (listen.succeeded()) {
                log.info("The httpServer is now listenning now");
            } else {
                log.info("The httpServer failed to bind!");
            }
        });
        sleep(20000);
    }
}

class Server extends AbstractVerticle {
    public void start() {
        vertx = Vertx.vertx();
        vertx.createHttpServer()
                .requestHandler(new Handler<HttpServerRequest>() {
                    public void handle(HttpServerRequest req) {
                        if (req.path().equals("/"))
                            req.response().sendFile("pom.xml");
                        else
                            req.response()
                                    .putHeader("content-type", "text/html")
                                    .end("Hello,Vertx Server!");

                    }
                }).listen(8080);
    }
}