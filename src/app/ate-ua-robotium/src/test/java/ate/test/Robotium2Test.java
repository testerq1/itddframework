package ate.test;

import io.selendroid.common.SelendroidCapabilities;
import io.selendroid.common.device.DeviceTargetPlatform;
import io.selendroid.standalone.exceptions.AndroidSdkException;
import io.selendroid.standalone.android.AndroidDevice;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.testcase.UATestcase;
import ate.ua.robotium.parser.RobotiumUA;

/**
 * Robotium based on Selendroid
 * 
 * @author ravih
 *
 */
public class Robotium2Test extends UATestcase {	
	@Test
	public void test() throws Exception{
		RobotiumUA ua=create_ua("robotium:./apk/AndroidCalculator.apk#test*",new RobotiumUA());
		SelendroidCapabilities cap=ua.get_aut_cap("io.selendroid.testapp:0.16.0");
		cap.setPlatformVersion(DeviceTargetPlatform.ANDROID19);
		ua.startup();
		AndroidDevice device=ua.get_android_device(cap);
		
		log.info(device.getScreenSize()+"");//854x480
		log.info(device.getSerial());//
		//log.info(device.getLocale().getCountry());
//		DeviceTargetPlatform tp=device.getTargetPlatform();
//		log.info(tp.getSdkFolderName());//android-15
//		log.info(tp.getVersionNumber());//4.0.3
//		log.info(tp.getApi()+"");//15
		log.info(ua.get_app_id());
		log.info(ua.get_app().getBasePackage());
		log.info(ua.get_app().getMainActivity());
		log.info(ua.get_app().getVersionName());
		log.info(ua.get_app().getAbsolutePath());
	}
	
	@Test
	public void basic() throws AndroidSdkException {
		RobotiumUA ua;
		ua=create_ua("robotium:./apk/AndroidCalculator.apk#test*",new RobotiumUA());
		assertTrue(ua.get_apk_file().exists());
		assertTrue(ua.get_all_test_apk().size()==1,ua.get_all_test_apk());
		assertTrue(ua.get_all_test_apk().get(0).getName().equals("testAndroidCalculatorBlackBox.apk"));
		
		ua=create_ua("robotium:AndroidCalculator.apk#test*",new RobotiumUA());
		assertTrue(ua.get_apk_file().exists());
		assertTrue(ua.get_all_test_apk().size()==1);
		assertTrue(ua.get_all_test_apk().get(0).getName().equals("testAndroidCalculatorBlackBox.apk"));
		
		ua=create_ua("robotium:AndroidCalculator.apk#./apk/test*",new RobotiumUA());
		assertTrue(ua.get_apk_file().exists());
		assertTrue(ua.get_all_test_apk().size()==1);
		assertTrue(ua.get_all_test_apk().get(0).getName().equals("testAndroidCalculatorBlackBox.apk"));
		
	}
	
	@BeforeClass
	public void beforeclass() {				
		//start_all_ua();		
		super.BeforeClass();
	}

	@AfterClass
	public void afterclass() {
		//shutdown_all_ua();
		super.AfterClass();
	}
}
