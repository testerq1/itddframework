package ate.test;

import java.util.HashMap;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ate.testcase.UATestcase;
import ate.ua.robotium.RobotiumUA;

/**
 * 需要设置ANDROID_HOME=D:\tools\Android\sdk
 * @author Administrator
 *
 */
public class RobotiumTest extends UATestcase {	
	@Test
	public void monkey_runner(){
		RobotiumUA ua;
		ua=create_ua("robotium:AndroidCalculator.apk#test*",new RobotiumUA());		
		ua.startup();		
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("sdkPlatform", "15");
		//map.put("device", "");//指定设备串码 AbstractAndroidMojo.shouldDoWithThisDevice
		//map.put("devices", new String[]{});//指定设备组
		assertTrue(ua.monkey_runner(map).is_ok());	
	}
	
	@Test
	public void robotium(){
		RobotiumUA ua;
		ua=create_ua("robotium:AndroidCalculator.apk#test*",new RobotiumUA());		
		ua.startup();		
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("sdkPlatform", "15");
		//map.put("device", "");//指定设备串码 AbstractAndroidMojo.shouldDoWithThisDevice
		//map.put("devices", new String[]{});//指定设备组
		assertTrue(ua.robotium(map).is_ok());		
	}
	
	public void monkey(){
		RobotiumUA ua;
		ua=create_ua("robotium:AndroidCalculator.apk#test*",new RobotiumUA());		
		ua.startup();		
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("EventCount", 1000);
		map.put("Seed", 123456);
		map.put("sdkPlatform", "15");
		//map.put("device", "");//指定设备串码 AbstractAndroidMojo.shouldDoWithThisDevice
		//map.put("devices", new String[]{});//指定设备组
		assertTrue(ua.monkey(map).is_ok());		
	}
	
	public void basic(){
		RobotiumUA ua;
		ua=create_ua("robotium:./apk/AndroidCalculator.apk#test*",new RobotiumUA());
		assertTrue(ua.get_apk_file().exists());
		assertTrue(ua.get_all_test_apk().size()==1);
		assertTrue(ua.get_all_test_apk().get(0).getName().equals("testAndroidCalculatorBlackBox.apk"));
		
		ua=create_ua("robotium:AndroidCalculator.apk#test*",new RobotiumUA());
		assertTrue(ua.get_apk_file().exists());
		assertTrue(ua.get_all_test_apk().size()==1);
		assertTrue(ua.get_all_test_apk().get(0).getName().equals("testAndroidCalculatorBlackBox.apk"));
		
		ua=create_ua("robotium:AndroidCalculator.apk#./apk/test*",new RobotiumUA());
		assertTrue(ua.get_apk_file().exists());
		assertTrue(ua.get_all_test_apk().size()==1);
		assertTrue(ua.get_all_test_apk().get(0).getName().equals("testAndroidCalculatorBlackBox.apk"));
		
	}	
	
	@BeforeClass
	public void beforeclass() {				
		//start_all_ua();		
		super.BeforeClass();
	}

	@AfterClass
	public void afterclass() {
		//shutdown_all_ua();
		super.AfterClass();
	}
}
