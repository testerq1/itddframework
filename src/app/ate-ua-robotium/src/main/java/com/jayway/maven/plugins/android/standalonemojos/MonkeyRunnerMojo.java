/*
 * Copyright (C) 2009 Jayway AB
 * Copyright (C) 2007-2008 JVending Masa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jayway.maven.plugins.android.standalonemojos;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.codehaus.plexus.interpolation.os.Os;
import org.codehaus.plexus.util.cli.shell.BourneShell;

import ate.ua.robotium.MonkeyRunnerAndroidTestRunListener;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.testrunner.ITestRunListener;
import com.android.ddmlib.testrunner.ITestRunListener.TestFailure;
import com.android.ddmlib.testrunner.TestIdentifier;
import com.jayway.maven.plugins.android.AbstractAndroidMojo;
import com.jayway.maven.plugins.android.CommandExecutor;
import com.jayway.maven.plugins.android.DeviceCallback;
import com.jayway.maven.plugins.android.ExecutionException;
import com.jayway.maven.plugins.android.configuration.Program;

/**
 * Can execute monkey runner programs.<br/>
 * Implements parsing parameters from pom or command line arguments and sets useful defaults as well. This goal will
 * invoke monkey runner scripts. If the application crashes during the exercise, this goal can fail the build. <br />
 * A typical usage of this goal can be found at <a
 * href="https://github.com/stephanenicolas/Quality-Tools-for-Android">Quality tools for Android project</a>.
 * 
 * @see <a href="http://developer.android.com/tools/help/monkey.html">Monkey docs by Google</a>
 * @see <a href="http://stackoverflow.com/q/3968064/693752">Stack Over Flow thread for parsing monkey output.</a>
 * @author Stéphane Nicolas <snicolas@octo.com>
 * @goal monkeyrunner
 * @requiresProject true
 */
@SuppressWarnings( "unused" )
public class MonkeyRunnerMojo extends AbstractAndroidMojo
{
    
    /**
     * This is commonly used to prevent failure of build when (some) tests fail. We
     * honor it too.
     */
    private boolean parsedIgnoreTestFailure=false;

    private String[] parsedPlugins;

    /**
     * Runs the contents of the file as a Python program.
     * 
     * <pre>
     * &lt;programs&gt;
     *   &lt;program&gt;
     *     &lt;filename&gt;foo.py&lt;/filename&gt;
     *     &lt;options&gt;bar&lt;/options&gt;
     *   &lt;program&gt;
     *   &lt;program&gt;
     *     &lt;filename&gt;foo2.py&lt;/filename&gt;
     *   &lt;program&gt;
     *   [..]
     * &lt;/programs&gt;
     * </pre>
     * 
     * @parameter
     */
    private List< Program > parsedPrograms;
    private Boolean parsedCreateReport=true;
    /**
     * Decides whether or not to inject device serial number as a parameter to each monkey runner script. The parameter
     * will be the first parameter passed to the script. This parameter allows to support monkey runner tests on
     * multiple devices. In that case, monkey runner scripts have to be modified to take the new parameter into account.
     * Follow that <a href="http://stackoverflow.com/a/13460438/693752">thread on stack over flow to learn more about
     * it</a>.
     * 
     * Defaults to false.
     * 
     */
    private Boolean parsedInjectDeviceSerialNumberIntoScript=false;
    private String parsedRunName;
    private int parsedEventCount=123456;

    private long elapsedTime;
    private Map< String, String > runMetrics;
    private TestIdentifier mCurrentTestIndentifier;
    private MonkeyRunnerErrorListener errorListener;

    public void execute() throws Exception
    {
        doWithDevices( new DeviceCallback()
        {
            @Override
            public void doWithDevice( IDevice device ) throws Exception
            {
            	MonkeyRunnerAndroidTestRunListener testRunListener = new MonkeyRunnerAndroidTestRunListener( device );
                add_listener(testRunListener);
				testRunListener.set_create_report(parsedCreateReport);
				
                run( device, testRunListener );                
            }
        } );
    }

    /**
     * Whether or not test failures should be ignored.
     * 
     * @return a boolean indicating whether or not test failures should be ignored.
     */
    protected boolean isIgnoreTestFailures()
    {
        return parsedIgnoreTestFailure;
    }

    /**
     * Actually plays tests.
     * 
     * @param device
     *            the device on which tests are going to be executed.
     * @param iTestRunListeners
     *            test run listeners.
     * @throws MojoExecutionException
     *             if exercising app threw an exception and isIgnoreTestFailures is false..
     * @throws MojoFailureException
     *             if exercising app failed and isIgnoreTestFailures is false.
     */
    protected void run( IDevice device, ITestRunListener... iTestRunListeners ) throws Exception
    {
        log.debug( "Parsed values for Android Monkey Runner invocation: " );

        CommandExecutor executor = CommandExecutor.Factory.createDefaultCommmandExecutor();
        if ( !Os.isFamily( Os.FAMILY_WINDOWS ) )
        {
            executor.setCustomShell( new CustomBourneShell() );
        }
        executor.setLogger( this.log );

        String command = getAndroidSdk().getMonkeyRunnerPath();

        List< String > pluginParameters = new ArrayList< String >();

        if ( parsedPlugins != null && parsedPlugins.length != 0 )
        {
            for ( String plugin : parsedPlugins )
            {
                String pluginFilePath = new File( "./apk", plugin ).getAbsolutePath();
                pluginParameters.add( "-plugin " + pluginFilePath );
            }
        }

        if ( parsedPrograms != null && !parsedPrograms.isEmpty() )
        {
            handleTestRunStarted();
            errorListener = new MonkeyRunnerErrorListener();
            executor.setErrorListener( errorListener );

            for ( Program program : parsedPrograms )
            {
                List< String > parameters = new ArrayList< String >( pluginParameters );

                String programFileName = new File( "./apk", program.getFilename() ).getAbsolutePath();
                parameters.add( programFileName );
                String testName = programFileName;
                if ( testName.contains( "/" ) )
                {
                    testName.substring( testName.indexOf( '/' ) + 1 );
                }
                mCurrentTestIndentifier = new TestIdentifier( "MonkeyTest ", testName );

                String programOptions = program.getOptions();
                if ( parsedInjectDeviceSerialNumberIntoScript != null && parsedInjectDeviceSerialNumberIntoScript )
                {
                    parameters.add( device.getSerialNumber() );
                }
                if ( programOptions != null && !StringUtils.isEmpty( programOptions ) )
                {
                    parameters.add( programOptions );
                }

                try
                {
                    log.info( "Running command: " + command );
                    log.info( "with parameters: " + parameters );
                    handleTestStarted();
                    executor.executeCommand( command, parameters, true );
                    handleTestEnded();
                }
                catch ( ExecutionException e )
                {
                    log.info( "Monkey runner produced errors" );
                    handleTestRunFailed( e.getMessage() );

                    if ( !isIgnoreTestFailures() )
                    {
                        log.info( "Project is configured to fail on error." );
                        log.info(
                                "Inspect monkey runner reports or re-run with -X to see monkey runner errors in log" );
                        log.info( "Failing build as configured. Ignore following error message." );
                        if ( errorListener.hasError )
                        {
                            log.info( "Stack trace is:" );
                            log.info( errorListener.getStackTrace() );
                        }
                        throw new MojoExecutionException( "", e );
                    }
                }

                if ( errorListener.hasError() )
                {
                    handleCrash();
                }
            }
            handleTestRunEnded();
        }

        log.info( "Monkey runner test runs completed successfully." );
    }

    private void handleTestRunStarted()
    {
        runMetrics = new HashMap< String, String >();
        elapsedTime = System.currentTimeMillis();
        for ( ITestRunListener tmp : listener )
        {
        	tmp.testRunStarted( parsedRunName, parsedEventCount );
        }
    }

    private void handleTestRunFailed( String error )
    {
        for ( ITestRunListener tmp:listener )
        {
            tmp.testRunFailed( error );
        }
    }

    private void handleTestRunEnded()
    {
        elapsedTime = System.currentTimeMillis() - elapsedTime;

        for ( ITestRunListener tmp : listener )
        {
            tmp.testRunEnded( elapsedTime, runMetrics );
        }
    }

    private void handleTestStarted()
    {
        log.debug( "TEST START " + listener.size());
        for ( ITestRunListener tmp : listener )
        {
            tmp.testStarted( mCurrentTestIndentifier );
        }
    }

    private void handleTestEnded()
    {
        if ( mCurrentTestIndentifier != null )
        {
            for ( ITestRunListener tmp:listener )
            {
                tmp.testEnded( mCurrentTestIndentifier, new HashMap< String, String >() );
            }
            mCurrentTestIndentifier = null;
        }
    }

    private void handleCrash()
    {

        String trace = errorListener.getStackTrace();

        for ( ITestRunListener tmp:listener )
        {
            tmp.testFailed( TestFailure.ERROR, mCurrentTestIndentifier, trace );
        }
        mCurrentTestIndentifier = null;

    }
    
    public MonkeyRunnerErrorListener get_error_listener(){
    	return this.errorListener;
    }
    
    private final class MonkeyRunnerErrorListener implements CommandExecutor.ErrorListener
    {
        private StringBuilder stackTraceBuilder = new StringBuilder();
        private boolean hasError = false;

        @Override
        public boolean isError( String error )
        {

            // Unconditionally ignore *All* build warning if configured to
            if ( isIgnoreTestFailures() )
            {
                return false;
            }

            if ( hasError )
            {
                stackTraceBuilder.append( error ).append( '\n' );
            }

            final Pattern pattern = Pattern.compile( ".*error.*|.*exception.*", Pattern.CASE_INSENSITIVE );
            final Matcher matcher = pattern.matcher( error );

            // If the the reg.exp actually matches, we can safely say this is not an error
            // since in theory the user told us so
            if ( matcher.matches() )
            {
                hasError = true;
                stackTraceBuilder.append( error ).append( '\n' );
                return true;
            }

            // Otherwise, it is just another error
            return false;
        }

        public String getStackTrace()
        {
            if ( hasError )
            {
                return stackTraceBuilder.toString();
            }
            else
            {
                return null;
            }
        }

        public boolean hasError()
        {
            return hasError;
        }
    }

   
    /**
     * @return default plugins.
     */
    public String[] getPlugins()
    {
        return parsedPlugins;
    }

    private static final class CustomBourneShell extends BourneShell
    {
        @Override
        public List< String > getShellArgsList()
        {
            List< String > shellArgs = new ArrayList< String >();
            List< String > existingShellArgs = super.getShellArgsList();

            if ( existingShellArgs != null && !existingShellArgs.isEmpty() )
            {
                shellArgs.addAll( existingShellArgs );
            }

            return shellArgs;
        }

        @Override
        public String[] getShellArgs()
        {
            String[] shellArgs = super.getShellArgs();
            if ( shellArgs == null )
            {
                shellArgs = new String[ 0 ];
            }

            return shellArgs;
        }

    }

    public List< Program > getPrograms()
    {
        // return null if not set
        return parsedPrograms;
    }
}
