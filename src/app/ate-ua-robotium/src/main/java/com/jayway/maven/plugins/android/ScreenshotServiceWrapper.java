package com.jayway.maven.plugins.android;

import static com.github.rtyley.android.screenshot.paparazzo.processors.util.Dimensions.square;
import static com.jayway.maven.plugins.android.common.DeviceHelper.getDescriptiveName;
import static org.apache.commons.io.FileUtils.forceMkdir;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;

import ate.rm.dev.Testbed;

import com.android.ddmlib.IDevice;
import com.github.rtyley.android.screenshot.paparazzo.OnDemandScreenshotService;
import com.github.rtyley.android.screenshot.paparazzo.processors.AnimatedGifCreator;
import com.github.rtyley.android.screenshot.paparazzo.processors.ImageSaver;
import com.github.rtyley.android.screenshot.paparazzo.processors.ImageScaler;

/**
 * ScreenshotServiceWrapper wraps the feature to capture a screenshot during an instrumentation test run.
 */
public class ScreenshotServiceWrapper implements DeviceCallback
{

    private final DeviceCallback delegate;
    private final Logger log;
    private final File screenshotParentDir;
    private static final int MAX_BOUNDS = 320;

    public ScreenshotServiceWrapper( DeviceCallback delegate, Logger log )
    {
        this.delegate = delegate;
        this.log = log;
        Testbed tb = Testbed.getTestbed();
		if (tb.arg("testng_output_dir") != null)
			screenshotParentDir =new File(tb.arg("testng_output_dir")+File.separator+"screenshots");
		else
			screenshotParentDir =new File("test-output"+File.separator+"screenshots");
		
		create( screenshotParentDir );
    }


    @Override
    public void doWithDevice( final IDevice device ) throws Exception
    {
        String deviceName = getDescriptiveName( device );

        File deviceGifFile = new File( screenshotParentDir, deviceName + ".gif" );
        File deviceScreenshotDir = new File( screenshotParentDir, deviceName );
        create( deviceScreenshotDir );


        OnDemandScreenshotService screenshotService = new OnDemandScreenshotService( device,
                new ImageSaver( deviceScreenshotDir ),
                new ImageScaler( new AnimatedGifCreator( deviceGifFile ), square( MAX_BOUNDS ) ) );

        screenshotService.start();

        delegate.doWithDevice( device );

        screenshotService.finish();
    }

    private void create( File dir )
    {
        try
        {
            forceMkdir( dir );
        }
        catch ( IOException e )
        {
            log.warn( "Unable to create screenshot directory: " + screenshotParentDir.getAbsolutePath(), e );
        }
    }
}
