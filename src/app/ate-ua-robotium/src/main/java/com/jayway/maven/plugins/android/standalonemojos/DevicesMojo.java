package com.jayway.maven.plugins.android.standalonemojos;

import com.jayway.maven.plugins.android.AbstractAndroidMojo;

/**
 * DevicesMojo lists all attached devices and emulators found with the android debug bridge. It uses the same
 * naming convention for the emulator as used in other places in the Android Maven Plugin and adds the status
 * of the device in the list.
 * <p/>
 * TODO The goal is very simple and could be enhanced for better display, a verbose option to display and to take the
 * android.device paramter into account.
 *
 * @author Manfred Moser <manfred@simpligility.com>
 * @goal devices
 * @requiresProject false
 */
public class DevicesMojo extends AbstractAndroidMojo
{
	public DevicesMojo (){
		
	}
}
