/*
 * Copyright (C) 2009-2011 Jayway AB
 * Copyright (C) 2007-2008 JVending Masa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jayway.maven.plugins.android;

import static org.apache.commons.lang.StringUtils.isBlank;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ate.ua.RunFailureException;
import ate.ua.robotium.ATestRunListener;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.InstallException;
import com.jayway.maven.plugins.android.common.AndroidExtension;
import com.jayway.maven.plugins.android.common.DeviceHelper;
import com.jayway.maven.plugins.android.config.ConfigPojo;
import com.jayway.maven.plugins.android.configuration.Ndk;
import com.jayway.maven.plugins.android.configuration.Sdk;

/**
 * Contains common fields and methods for android mojos.
 * 
 * @author hugo.josefson@jayway.com
 * @author Manfred Moser <manfred@simpligility.com>
 * @author William Ferguson <william.ferguson@xandar.com.au>
 * @author Malachi de AElfweald malachid@gmail.com
 */
public abstract class AbstractAndroidMojo {
	protected static Logger log = LoggerFactory.getLogger(AbstractAndroidMojo.class);
	
	// 报错测试的结果和日志
	protected List<ATestRunListener> listener = new ArrayList<ATestRunListener>();

	public static final List<String> SUPPORTED_PACKAGING_TYPES = new ArrayList<String>();

	static {
		SUPPORTED_PACKAGING_TYPES.add(AndroidExtension.APK);
	}

	/**
	 * Android Debug Bridge initialization timeout in milliseconds.
	 */
	private static final long ADB_TIMEOUT_MS = 60L * 1000;

	/**
	 * The <code>ANDROID_NDK_HOME</code> environment variable name.
	 */
	public static final String ENV_ANDROID_NDK_HOME = "ANDROID_NDK_HOME";

	/**
	 * <p>
	 * The Android NDK to use.
	 * </p>
	 * <p>
	 * Looks like this:
	 * </p>
	 * 
	 * <pre>
	 * &lt;ndk&gt;
	 *     &lt;path&gt;/opt/android-ndk-r4&lt;/path&gt;
	 * &lt;/ndk&gt;
	 * </pre>
	 * <p>
	 * The <code>&lt;path&gt;</code> parameter is optional. The default is the
	 * setting of the ANDROID_NDK_HOME environment variable. The parameter can
	 * be used to override this setting with a different environment variable
	 * like this:
	 * </p>
	 * 
	 * <pre>
	 * &lt;ndk&gt;
	 *     &lt;path&gt;${env.ANDROID_NDK_HOME}&lt;/path&gt;
	 * &lt;/ndk&gt;
	 * </pre>
	 * <p>
	 * or just with a hardcoded absolute path. The parameters can also be
	 * configured from command-line with parameter
	 * <code>-Dandroid.ndk.path</code>.
	 * </p>
	 * 
	 * @parameter
	 */
	@ConfigPojo(prefix = "ndk")
	private Ndk ndk;

	/**
	 * Specifies which the serial number of the device to connect to. Using the
	 * special values "usb" or "emulator" is also valid. "usb" will connect to
	 * all actual devices connected (via usb). "emulator" will connect to all
	 * emulators connected. Multiple devices will be iterated over in terms of
	 * goals to run. All device interaction goals support this so you can e..
	 * deploy the apk to all attached emulators and devices. Goals supporting
	 * this are devices, deploy, undeploy, redeploy, pull, push and instrument.
	 * 
	 * @parameter expression="${android.device}"
	 */
	protected String device;

	/**
	 * <p>
	 * Specifies a list of serial numbers of each device you want to connect to.
	 * Using the special values "usb" or "emulator" is also valid. "usb" will
	 * connect to all actual devices connected (via usb). "emulator" will
	 * connect to all emulators connected. Multiple devices will be iterated
	 * over in terms of goals to run. All device interaction goals support this
	 * so you can e.. deploy the apk to all attached emulators and devices.
	 * Goals supporting this are devices, deploy, undeploy, redeploy, pull, push
	 * and instrument.
	 * </p>
	 * 
	 * <pre>
	 * &lt;devices&gt;
	 *     &lt;device&gt;usb&lt;/device&gt;
	 *     &lt;device&gt;emulator-5554&lt;/device&gt;
	 * &lt;/devices&gt;
	 * </pre>
	 * <p>
	 * This parameter can also be configured from command-line with parameter
	 * <code>-Dandroid.devices=usb,emulator</code>.
	 * </p>
	 * 
	 * @parameter expression="${android.devices}"
	 */
	protected String[] devices;

	/**
	 * A selection of configurations to be included in the APK as a comma
	 * separated list. This will limit the configurations for a certain type.
	 * For example, specifying <code>hdpi</code> will exclude all resource
	 * folders with the <code>mdpi</code> or <code>ldpi</code> modifiers, but
	 * won't affect language or orientation modifiers. For more information
	 * about this option, look in the aapt command line help.
	 * 
	 * @parameter expression="${android.configurations}"
	 */
	protected String configurations;

	/**
	 * <p>
	 * The Android SDK to use.
	 * </p>
	 * <p>
	 * Looks like this:
	 * </p>
	 * 
	 * <pre>
	 * &lt;sdk&gt;
	 *     &lt;path&gt;/opt/android-sdk-linux&lt;/path&gt;
	 *     &lt;platform&gt;2.1&lt;/platform&gt;
	 * &lt;/sdk&gt;
	 * </pre>
	 * <p>
	 * The <code>&lt;platform&gt;</code> parameter is optional, and corresponds
	 * to the <code>platforms/android-*</code> directories in the Android SDK
	 * directory. Default is the latest available version, so you only need to
	 * set it if you for example want to use platform 1.5 but also have e.g. 2.2
	 * installed. Has no effect when used on an Android SDK 1.1. The parameter
	 * can also be coded as the API level. Therefore valid values are 1.1, 1.5,
	 * 1.6, 2.0, 2.01, 2.1, 2.2 and so as well as 3, 4, 5, 6, 7, 8... 16. If a
	 * platform/api level is not installed on the machine an error message will
	 * be produced.
	 * </p>
	 * <p>
	 * The <code>&lt;path&gt;</code> parameter is optional. The default is the
	 * setting of the ANDROID_HOME environment variable. The parameter can be
	 * used to override this setting with a different environment variable like
	 * this:
	 * </p>
	 * 
	 * <pre>
	 * &lt;sdk&gt;
	 *     &lt;path&gt;${env.ANDROID_SDK}&lt;/path&gt;
	 * &lt;/sdk&gt;
	 * </pre>
	 * <p>
	 * or just with a hard-coded absolute path. The parameters can also be
	 * configured from command-line with parameters
	 * <code>-Dandroid.sdk.path</code> and <code>-Dandroid.sdk.platform</code>.
	 * </p>
	 * 
	 * @parameter
	 */
	private Sdk sdk;

	/**
	 * <p>
	 * Parameter designed to pick up <code>-Dandroid.sdk.path</code> in case
	 * there is no pom with an <code>&lt;sdk&gt;</code> configuration tag.
	 * </p>
	 * <p>
	 * Corresponds to
	 * {@link com.jayway.maven.plugins.android.configuration.Sdk#path}.
	 * </p>
	 * 
	 * @parameter expression="${android.sdk.path}"
	 * @readonly
	 */
	private File sdkPath;

	/**
	 * <p>
	 * Parameter designed to pick up environment variable
	 * <code>ANDROID_HOME</code> in case <code>android.sdk.path</code> is not
	 * configured.
	 * </p>
	 * 
	 * @parameter expression="${env.ANDROID_HOME}"
	 * @readonly
	 */
	private String envAndroidHome;

	/**
	 * The <code>ANDROID_HOME</code> environment variable name.
	 */
	public static final String ENV_ANDROID_HOME = "ANDROID_HOME";

	/**
	 * <p>
	 * Parameter designed to pick up <code>-Dandroid.sdk.platform</code> in case
	 * there is no pom with an <code>&lt;sdk&gt;</code> configuration tag.
	 * </p>
	 * <p>
	 * Corresponds to
	 * {@link com.jayway.maven.plugins.android.configuration.Sdk#platform}.
	 * </p>
	 * 
	 * @parameter expression="${android.sdk.platform}"
	 * @readonly
	 */
	private String sdkPlatform;

	/**
	 * <p>
	 * Whether to undeploy an apk from the device before deploying it.
	 * </p>
	 * <p/>
	 * <p>
	 * Only has effect when running <code>mvn android:deploy</code> in an
	 * Android application project manually, or when running
	 * <code>mvn integration-test</code> (or <code>mvn install</code>) in a
	 * project with instrumentation tests.
	 * </p>
	 * <p/>
	 * <p>
	 * It is useful to keep this set to <code>true</code> at all times, because
	 * if an apk with the same package was previously signed with a different
	 * keystore, and deployed to the device, deployment will fail becuase your
	 * keystore is different.
	 * </p>
	 * 
	 * @parameter default-value=false
	 *            expression="${android.undeployBeforeDeploy}"
	 */
	protected boolean undeployBeforeDeploy;

	/**
	 * <p>
	 * Parameter designed to pick up <code>-Dandroid.ndk.path</code> in case
	 * there is no pom with an <code>&lt;ndk&gt;</code> configuration tag.
	 * </p>
	 * <p>
	 * Corresponds to
	 * {@link com.jayway.maven.plugins.android.configuration.Ndk#path}.
	 * </p>
	 * 
	 * @parameter expression="${android.ndk.path}"
	 * @readonly
	 */
	private File ndkPath;

	/**
     *
     */
	private static final Object ADB_LOCK = new Object();
	/**
     *
     */
	private static boolean adbInitialized = false;

	public void set_sdk_path(String path) {
		this.sdkPath = new File(path);
	}

	public void set_sdk_platform(String platform) {
		this.sdkPlatform = platform;
	}

	public void add_listener(ATestRunListener ll) {
		listener.add(ll);
	}

	public void clear_all_listener() {
		listener.clear();
	}

	public List<ATestRunListener> get_all_listener() {
		return listener;
	}

	/**
	 * Initialize the Android Debug Bridge and wait for it to start. Does not
	 * reinitialize it if it has already been initialized (that would through
	 * and IllegalStateException...). Synchronized sine the init call in the
	 * library is also synchronized .. just in case.
	 * 
	 * @return
	 */
	protected AndroidDebugBridge initAndroidDebugBridge() {
		synchronized (ADB_LOCK) {
			if (!adbInitialized) {
				AndroidDebugBridge.init(false);
				adbInitialized = true;
			}
			AndroidDebugBridge androidDebugBridge = AndroidDebugBridge
					.createBridge(getAndroidSdk().getAdbPath(), false);
			waitUntilConnected(androidDebugBridge);
			return androidDebugBridge;
		}
	}

	/**
	 * Run a wait loop until adb is connected or trials run out. This method
	 * seems to work more reliably then using a listener.
	 * 
	 * @param adb
	 */
	private void waitUntilConnected(AndroidDebugBridge adb) {
		int trials = 10;
		final int connectionWaitTime = 50;
		while (trials > 0) {
			try {
				Thread.sleep(connectionWaitTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (adb.isConnected()) {
				break;
			}
			trials--;
		}
	}

	/**
	 * Wait for the Android Debug Bridge to return an initial device list.
	 * 
	 * @param androidDebugBridge
	 * @throws MojoExecutionException
	 */
	protected void waitForInitialDeviceList(
			final AndroidDebugBridge androidDebugBridge) {
		if (!androidDebugBridge.hasInitialDeviceList()) {
			log.info("Waiting for initial device list from the Android Debug Bridge");
			long limitTime = System.currentTimeMillis() + ADB_TIMEOUT_MS;
			while (!androidDebugBridge.hasInitialDeviceList()
					&& (System.currentTimeMillis() < limitTime)) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					throw new RunFailureException(
							"Interrupted waiting for initial device list from Android Debug Bridge");
				}
			}
			if (!androidDebugBridge.hasInitialDeviceList()) {
				log.error("Did not receive initial device list from the Android Debug Bridge.");
			}
		}
	}

	/**
	 * Deploys an apk file to a connected emulator or usb device.
	 * 
	 * @param apkFile
	 *            the file to deploy
	 * @throws MojoExecutionException
	 *             If there is a problem deploying the apk file.
	 */
	protected void deployApk(final File apkFile) throws Exception {
		if (undeployBeforeDeploy) {
			undeployApk(apkFile);
		}
		doWithDevices(new DeviceCallback() {
			@Override
			public void doWithDevice(final IDevice device) throws Exception {
				String deviceLogLinePrefix = DeviceHelper
						.getDeviceLogLinePrefix(device);
				try {
					String result = device.installPackage(
							apkFile.getAbsolutePath(), true);
					// according to the docs for installPackage, not null
					// response is error
					if (result != null) {
						throw new Exception(deviceLogLinePrefix + "Install of "
								+ apkFile.getAbsolutePath() + " failed - ["
								+ result + "]");
					}
					log.info(deviceLogLinePrefix + "Successfully installed "
							+ apkFile.getAbsolutePath());
					log.debug(" to " + DeviceHelper.getDescriptiveName(device));
				} catch (InstallException e) {
					throw new Exception(deviceLogLinePrefix + "Install of "
							+ apkFile.getAbsolutePath() + " failed.", e);
				}
			}
		});
	}

	List<IDevice> local_devices;

	public void initDevices() {
		final AndroidDebugBridge androidDebugBridge = initAndroidDebugBridge();

		if (!androidDebugBridge.isConnected()) {
			throw new RunFailureException(
					"Android Debug Bridge is not connected.");
		}

		waitForInitialDeviceList(androidDebugBridge);
		local_devices = Arrays.asList(androidDebugBridge.getDevices());
		int numberOfDevices = local_devices.size();
		log.debug("Found " + numberOfDevices
				+ " devices connected with the Android Debug Bridge");
		if (local_devices.size() == 0) {
			throw new RunFailureException("No online devices attached.");
		}
	}

	public IDevice get_default_device() {
		if (local_devices == null)
			initDevices();
		if (local_devices.isEmpty())
			return null;

		return local_devices.get(0);
	}

	public IDevice get_device_by_name(String name) {
		if (local_devices == null)
			initDevices();
		for (final IDevice idevice : this.local_devices) {
			if (name.equalsIgnoreCase(idevice.getName()))
				return idevice;
		}
		return null;
	}

	public IDevice get_device_by_serial(String name) {
		if (local_devices == null)
			initDevices();
		for (final IDevice idevice : this.local_devices) {
			if (name.equalsIgnoreCase(idevice.getSerialNumber()))
				return idevice;
		}
		return null;
	}

	/**
	 * Performs the callback action on the devices determined by
	 * {@link #shouldDoWithThisDevice(com.android.ddmlib.IDevice)}
	 * 
	 * @param deviceCallback
	 *            the action to perform on each device
	 * @throws org.apache.maven.plugin.MojoExecutionException
	 *             in case there is a problem
	 * @throws org.apache.maven.plugin.MojoFailureException
	 *             in case there is a problem
	 */
	protected void doWithDevices(final DeviceCallback deviceCallback)
			throws Exception {
		if (this.local_devices == null)
			this.initDevices();

		boolean shouldRunOnAllDevices = getDevices().size() == 0;
		if (shouldRunOnAllDevices) {
			log.info("android.devices parameter not set, using all attached devices");
		} else {
			log.info("android.devices parameter set to "
					+ getDevices().toString());
		}

		ArrayList<DoThread> doThreads = new ArrayList<DoThread>();
		for (final IDevice idevice : this.local_devices) {
			if (shouldRunOnAllDevices) {
				String deviceType = idevice.isEmulator() ? "Emulator "
						: "Device ";
				log.info(deviceType + DeviceHelper.getDescriptiveName(idevice)
						+ " found.");
			}
			if (shouldRunOnAllDevices || shouldDoWithThisDevice(idevice)) {
				DoThread deviceDoThread = new DoThread() {
					@Override
					public void runDo() throws Exception {
						deviceCallback.doWithDevice(idevice);
					}
				};
				doThreads.add(deviceDoThread);
				deviceDoThread.start();
			}
		}

		joinAllThreads(doThreads);
		throwAnyDoThreadErrors(doThreads);

		if (!shouldRunOnAllDevices && doThreads.isEmpty()) {
			throw new Exception("No device found for android.device="
					+ getDevices().toString());
		}
	}

	private void joinAllThreads(ArrayList<DoThread> doThreads) {
		for (Thread deviceDoThread : doThreads) {
			try {
				deviceDoThread.join();
			} catch (InterruptedException e) {
				new Exception("Thread#join error for device: "
						+ getDevices().toString());
			}
		}
	}

	private void throwAnyDoThreadErrors(ArrayList<DoThread> doThreads)
			throws Exception {
		for (DoThread deviceDoThread : doThreads) {
			if (deviceDoThread.failure != null) {
				throw deviceDoThread.failure;
			}
		}
	}

	/**
	 * Determines if this {@link IDevice}(s) should be used
	 * 
	 * @param idevice
	 *            the device to check
	 * @return if the device should be used
	 * @throws org.apache.maven.plugin.MojoExecutionException
	 *             in case there is a problem
	 * @throws org.apache.maven.plugin.MojoFailureException
	 *             in case there is a problem
	 */
	private boolean shouldDoWithThisDevice(IDevice idevice) throws Exception {

		for (String device : getDevices()) {
			// use specified device or all emulators or all devices
			if ("emulator".equals(device) && idevice.isEmulator()) {
				return true;
			}

			if ("usb".equals(device) && !idevice.isEmulator()) {
				return true;
			}

			if (idevice.isEmulator()
					&& (device.equalsIgnoreCase(idevice.getAvdName()) || device
							.equalsIgnoreCase(idevice.getSerialNumber()))) {
				return true;
			}

			if (!idevice.isEmulator()
					&& device.equals(idevice.getSerialNumber())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Undeploys an apk from a connected emulator or usb device. Also deletes
	 * the application's data and cache directories on the device.
	 * 
	 * @param apkFile
	 *            the file to undeploy
	 * @return <code>true</code> if successfully undeployed, <code>false</code>
	 *         otherwise.
	 */
	protected boolean undeployApk(File apkFile) throws Exception {
		final String packageName;
		packageName = extractPackageNameFromApk(apkFile);
		return undeployApk(packageName);
	}

	/**
	 * Undeploys an apk, specified by package name, from a connected emulator or
	 * usb device. Also deletes the application's data and cache directories on
	 * the device.
	 * 
	 * @param packageName
	 *            the package name to undeploy.
	 * @return <code>true</code> if successfully undeployed, <code>false</code>
	 *         otherwise.
	 */
	protected boolean undeployApk(final String packageName) throws Exception {

		final AtomicBoolean result = new AtomicBoolean(true); // if no devices
																// are present,
																// it counts as
																// successful

		doWithDevices(new DeviceCallback() {
			@Override
			public void doWithDevice(final IDevice device) throws Exception {
				String deviceLogLinePrefix = DeviceHelper
						.getDeviceLogLinePrefix(device);
				try {
					device.uninstallPackage(packageName);
					log.info(deviceLogLinePrefix + "Successfully uninstalled "
							+ packageName);
					log.debug(" from "
							+ DeviceHelper.getDescriptiveName(device));
					result.set(true);
				} catch (InstallException e) {
					result.set(false);
					throw new Exception(deviceLogLinePrefix + "Uninstall of "
							+ packageName + " failed.", e);
				}
			}
		});

		return result.get();
	}

	/**
	 * Extracts the package name from an apk file.
	 * 
	 * @param apkFile
	 *            apk file to extract package name from.
	 * @return the package name from inside the apk file.
	 */
	protected String extractPackageNameFromApk(File apkFile) throws Exception {
		CommandExecutor executor = CommandExecutor.Factory
				.createDefaultCommmandExecutor();
		executor.setLogger(log);
		executor.setCaptureStdOut(true);
		executor.setCaptureStdErr(true);
		List<String> commands = new ArrayList<String>();
		commands.add("dump");
		commands.add("xmltree");
		commands.add(apkFile.getAbsolutePath());
		commands.add("AndroidManifest.xml");
		log.info(getAndroidSdk().getAaptPath() + " " + commands.toString());
		try {
			executor.executeCommand(getAndroidSdk().getAaptPath(), commands,
					false);
			final String xmlTree = executor.getStandardOut();
			return extractPackageNameFromAndroidManifestXmlTree(xmlTree);
		} catch (ExecutionException e) {
			throw new Exception(
					"Error while trying to figure out package name from inside apk file "
							+ apkFile);
		} finally {
			String errout = executor.getStandardError();
			if ((errout != null) && (errout.trim().length() > 0)) {
				log.error(errout);
			}
		}
	}

	/**
	 * Extracts the package name from an XmlTree dump of AndroidManifest.xml by
	 * the <code>aapt</code> tool.
	 * 
	 * @param aaptDumpXmlTree
	 *            output from
	 *            <code>aapt dump xmltree &lt;apkFile&gt; AndroidManifest.xml
	 * @return the package name from inside the apkFile.
	 */
	protected String extractPackageNameFromAndroidManifestXmlTree(
			String aaptDumpXmlTree) {
		final Scanner scanner = new Scanner(aaptDumpXmlTree);
		// Finds the root element named "manifest".
		scanner.findWithinHorizon("^E: manifest", 0);
		// Finds the manifest element's attribute named "package".
		scanner.findWithinHorizon("  A: package=\"", 0);
		// Extracts the package value including the trailing double quote.
		String packageName = scanner.next(".*?\"");
		// Removes the double quote.
		packageName = packageName.substring(0, packageName.length() - 1);
		return packageName;
	}

	/**
	 * <p>
	 * Returns the Android SDK to use.
	 * </p>
	 * <p/>
	 * <p>
	 * Current implementation looks for System property
	 * <code>android.sdk.path</code>, then <code>&lt;sdk&gt;&lt;path&gt;</code>
	 * configuration in pom, then environment variable <code>ANDROID_HOME</code>.
	 * <p/>
	 * <p>
	 * This is where we collect all logic for how to lookup where it is, and
	 * which one to choose. The lookup is based on available parameters. This
	 * method should be the only one you should need to look at to understand
	 * how the Android SDK is chosen, and from where on disk.
	 * </p>
	 * 
	 * @return the Android SDK to use.
	 * @throws org.apache.maven.plugin.MojoExecutionException
	 *             if no Android SDK path configuration is available at all.
	 */
	protected AndroidSdk getAndroidSdk() {
		File chosenSdkPath;
		String chosenSdkPlatform;

		if (sdk != null) {
			// An <sdk> tag exists in the pom.

			if (sdk.getPath() != null) {
				// An <sdk><path> tag is set in the pom.

				chosenSdkPath = sdk.getPath();
			} else {
				// There is no <sdk><path> tag in the pom.

				if (sdkPath != null) {
					// -Dandroid.sdk.path is set on command line, or via
					// <properties><android.sdk.path>...
					chosenSdkPath = sdkPath;
				} else {
					// No -Dandroid.sdk.path is set on command line, or via
					// <properties><android.sdk.path>...
					chosenSdkPath = new File(getAndroidHomeOrThrow());
				}
			}

			// Use <sdk><platform> from pom if it's there, otherwise try
			// -Dandroid.sdk.platform from command line or
			// <properties><sdk.platform>...
			if (!isBlank(sdk.getPlatform())) {
				chosenSdkPlatform = sdk.getPlatform();
			} else {
				chosenSdkPlatform = sdkPlatform;
			}
		} else {
			// There is no <sdk> tag in the pom.

			if (sdkPath != null) {
				// -Dandroid.sdk.path is set on command line, or via
				// <properties><android.sdk.path>...
				chosenSdkPath = sdkPath;
			} else {
				// No -Dandroid.sdk.path is set on command line, or via
				// <properties><android.sdk.path>...
				chosenSdkPath = new File(getAndroidHomeOrThrow());
			}

			// Use any -Dandroid.sdk.platform from command line or
			// <properties><sdk.platform>...
			chosenSdkPlatform = sdkPlatform;
		}

		return new AndroidSdk(chosenSdkPath, chosenSdkPlatform);
	}

	/**
	 * 
	 * @return
	 * @throws MojoExecutionException
	 */
	private String getAndroidHomeOrThrow() {
		final String androidHome = System.getenv(ENV_ANDROID_HOME);
		if (isBlank(androidHome)) {
			throw new RunFailureException(
					"No Android SDK path could be found. You may configure it in the "
							+ "plugin configuration section in the pom file using <sdk><path>...</path></sdk> or "
							+ "<properties><android.sdk.path>...</android.sdk.path></properties> or on command-line "
							+ "using -Dandroid.sdk.path=... or by setting environment variable "
							+ ENV_ANDROID_HOME);
		}
		return androidHome;
	}

	/**
	 * <p>
	 * Returns the Android NDK to use.
	 * </p>
	 * <p/>
	 * <p>
	 * Current implementation looks for <code>&lt;ndk&gt;&lt;path&gt;</code>
	 * configuration in pom, then System property <code>android.ndk.path</code>,
	 * then environment variable <code>ANDROID_NDK_HOME</code>.
	 * <p/>
	 * <p>
	 * This is where we collect all logic for how to lookup where it is, and
	 * which one to choose. The lookup is based on available parameters. This
	 * method should be the only one you should need to look at to understand
	 * how the Android NDK is chosen, and from where on disk.
	 * </p>
	 * 
	 * @return the Android NDK to use.
	 * @throws org.apache.maven.plugin.MojoExecutionException
	 *             if no Android NDK path configuration is available at all.
	 */
	protected AndroidNdk getAndroidNdk() throws Exception {
		File chosenNdkPath = null;
		// There is no <ndk> tag in the pom.
		if (ndkPath != null) {
			// -Dandroid.ndk.path is set on command line, or via
			// <properties><ndk.path>...
			chosenNdkPath = ndkPath;
		} else if (ndk != null && ndk.getPath() != null) {
			chosenNdkPath = ndk.getPath();
		} else {
			// No -Dandroid.ndk.path is set on command line, or via
			// <properties><ndk.path>...
			chosenNdkPath = new File(getAndroidNdkHomeOrThrow());
		}
		return new AndroidNdk(chosenNdkPath);
	}

	/**
	 * 
	 * @return
	 * @throws MojoExecutionException
	 */
	private String getAndroidNdkHomeOrThrow() throws Exception {
		final String androidHome = System.getenv(ENV_ANDROID_NDK_HOME);
		if (isBlank(androidHome)) {
			throw new Exception(
					"No Android NDK path could be found. You may configure it in the pom "
							+ "using <ndk><path>...</path></ndk> or <properties><ndk.path>...</ndk.path></properties> or on "
							+ "command-line using -Dandroid.ndk.path=... or by setting environment variable "
							+ ENV_ANDROID_NDK_HOME);
		}
		return androidHome;
	}

	private Set<String> getDevices() {
		Set<String> list = new HashSet<String>();

		if (StringUtils.isNotBlank(device)) {
			list.add(device);
		}

		if (devices != null)
			list.addAll(Arrays.asList(devices));

		return list;
	}

	private abstract class DoThread extends Thread {
		Exception failure;

		@Override
		public final void run() {
			try {
				runDo();
			} catch (Exception e) {
				e.printStackTrace();
				failure = e;
			}
		}

		protected abstract void runDo() throws Exception;
	}

}
