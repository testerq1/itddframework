package com.jayway.maven.plugins.android.configuration;

import java.util.List;

/**
 * Configuration for the integration test runs. This class is only the definition of the parameters that are
 * shadowed in
 * {@link ate.ua.robotium.InstrumentationMojo} and used there.
 *
 * @author Manfred Moser <manfred@simpligility.com>
 */
public class Test
{
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testSkip}
     */
    private String skip;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testInstrumentationPackage}
     */
    private String instrumentationPackage;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testInstrumentationRunner}
     */
    private String instrumentationRunner;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testDebug}
     */
    private Boolean debug;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testCoverage}
     */
    private Boolean coverage;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testCoverageFile}
     */
    private String coverageFile;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testLogOnly}
     */
    private Boolean logOnly;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testTestSize}
     */
    private String testSize;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testCreateReport}
     */
    private Boolean createReport;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testPackages}
     */
    protected List<String> packages;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testClasses}
     */
    protected List<String> classes;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testAnnotations}
     */
    private List<String> annotations;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testExcludeAnnotations}
     */
    private List<String> excludeAnnotations;
    /**
     * Mirror of {@link ate.ua.robotium.InstrumentationMojo#testInstrumentationArgs}
     */
    private List<String> instrumentationArgs;

    public String getSkip()
    {
        return skip;
    }

    public String getInstrumentationPackage()
    {
        return instrumentationPackage;
    }

    public String getInstrumentationRunner()
    {
        return instrumentationRunner;
    }

    public Boolean isDebug()
    {
        return debug;
    }

    public Boolean isCoverage()
    {
        return coverage;
    }

    public String getCoverageFile()
    {
        return coverageFile;
    }

    public Boolean isLogOnly()
    {
        return logOnly;
    }

    public String getTestSize()
    {
        return testSize;
    }

    public Boolean isCreateReport()
    {
        return createReport;
    }

    public List<String> getPackages()
    {
        return packages;
    }

    public List<String> getClasses()
    {
        return classes;
    }

    public List<String> getAnnotations()
    {
        return annotations;
    }

    public List<String> getExcludeAnnotations()
    {
        return excludeAnnotations;
    }

    public List<String> getInstrumentationArgs()
    {
        return instrumentationArgs;
    }
}
