package io.selendroid.standalone.server.model;

import io.selendroid.common.SelendroidCapabilities;
import io.selendroid.standalone.SelendroidConfiguration;
import io.selendroid.standalone.android.AndroidApp;
import io.selendroid.standalone.android.AndroidDevice;
import io.selendroid.standalone.exceptions.AndroidDeviceException;
import io.selendroid.standalone.exceptions.AndroidSdkException;

import java.util.Map;

public class MyDriver extends SelendroidStandaloneDriver{

	public MyDriver(SelendroidConfiguration serverConfiguration)
			throws AndroidSdkException, AndroidDeviceException {
		super(serverConfiguration);
		
	}

	
	public Map<String, AndroidApp> getConfiguredApps() {
	    return super.getConfiguredApps();
	}

}
