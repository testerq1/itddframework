/*
 * Copyright (C) 2009-2011 Jayway AB
 * Copyright (C) 2007-2008 JVending Masa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ate.ua.robotium;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.testrunner.IRemoteAndroidTestRunner;
import com.android.ddmlib.testrunner.RemoteAndroidTestRunner;
import com.jayway.maven.plugins.android.AbstractAndroidMojo;
import com.jayway.maven.plugins.android.DeviceCallback;
import com.jayway.maven.plugins.android.ScreenshotServiceWrapper;
import com.jayway.maven.plugins.android.common.DeviceHelper;

public class InstrumentationMojo extends AbstractAndroidMojo {
	private boolean classesExists;
	private boolean packagesExists;

	private String parsedInstrumentationPackage;
	private String parsedInstrumentationRunner;
	private List<String> parsedClasses;
	private List<String> parsedPackages;
	private List<String> parsedAnnotations;
	private List<String> parsedExcludeAnnotations;
	private Map<String, String> parsedInstrumentationArgs;
	private String parsedTestSize;
	private Boolean parsedCoverage = false;
	private String parsedCoverageFile = "expression";
	private Boolean parsedDebug = false;
	private Boolean parsedLogOnly = false;
	private Boolean parsedCreateReport = true;

	private String packagesList;

	public void instrument() throws Exception {
		// only run Tests in specific package
		packagesList = buildCommaSeparatedString(parsedPackages);
		packagesExists = StringUtils.isNotBlank(packagesList);

		if (parsedClasses != null) {
			classesExists = parsedClasses.size() > 0;
		} else {
			classesExists = false;
		}

		if (classesExists && packagesExists) {
			// if both packages and classes are specified --> ERROR
			throw new Exception(
					"packages and classes are mutually exclusive. They cannot be specified at"
							+ " the same time. Please specify either packages or classes. For details, see "
							+ "http://developer.android.com/guide/developing/testing/testing_otheride.html");
		}

		DeviceCallback instrumentationTestExecutor = new DeviceCallback() {
			public void doWithDevice(final IDevice device) throws Exception {
				String deviceLogLinePrefix = DeviceHelper
						.getDeviceLogLinePrefix(device);

				RemoteAndroidTestRunner remoteAndroidTestRunner = new RemoteAndroidTestRunner(
						parsedInstrumentationPackage,
						parsedInstrumentationRunner, device);

				if (packagesExists) {
					for (String str : packagesList.split(",")) {
						remoteAndroidTestRunner.setTestPackageName(str);
						log.info(deviceLogLinePrefix
								+ "Running tests for specified test package: "
								+ str);
					}
				}

				if (classesExists) {
					remoteAndroidTestRunner.setClassNames(parsedClasses
							.toArray(new String[parsedClasses.size()]));
					log.info(deviceLogLinePrefix
							+ "Running tests for specified test classes/methods: "
							+ parsedClasses);
				}

				if (parsedAnnotations != null) {
					for (String annotation : parsedAnnotations) {
						remoteAndroidTestRunner.addInstrumentationArg(
								"annotation", annotation);
					}
				}

				if (parsedExcludeAnnotations != null) {
					for (String annotation : parsedExcludeAnnotations) {
						remoteAndroidTestRunner.addInstrumentationArg(
								"notAnnotation", annotation);
					}

				}

				remoteAndroidTestRunner.setDebug(parsedDebug);
				remoteAndroidTestRunner.setCoverage(parsedCoverage);
				if (!"".equals(parsedCoverageFile)) {
					remoteAndroidTestRunner.addInstrumentationArg(
							"coverageFile", parsedCoverageFile);
				}
				remoteAndroidTestRunner.setLogOnly(parsedLogOnly);

				if (StringUtils.isNotBlank(parsedTestSize)) {
					IRemoteAndroidTestRunner.TestSize validSize = IRemoteAndroidTestRunner.TestSize
							.getTestSize(parsedTestSize);
					remoteAndroidTestRunner.setTestSize(validSize);
				}

				addAllInstrumentationArgs(remoteAndroidTestRunner,
						parsedInstrumentationArgs);

				log.info(deviceLogLinePrefix
						+ "Running instrumentation tests in "
						+ parsedInstrumentationPackage);

				RobotiumAndroidTestRunListener testRunListener = new RobotiumAndroidTestRunListener(
						device);
				testRunListener.set_create_report(parsedCreateReport);
				add_listener(testRunListener);

				remoteAndroidTestRunner.run(testRunListener);
				if (testRunListener.hasFailuresOrErrors()) {
					throw new Exception(deviceLogLinePrefix
							+ "Tests failed on device.");
				}
				if (testRunListener.testRunFailed()) {
					throw new Exception(deviceLogLinePrefix
							+ "Test run failed to complete: "
							+ testRunListener.getTestRunFailureCause());
				}
				if (testRunListener.threwException()) {
					throw new Exception(deviceLogLinePrefix
							+ testRunListener.getExceptionMessages());
				}

			}
		};

		instrumentationTestExecutor = new ScreenshotServiceWrapper(
				instrumentationTestExecutor, log);

		doWithDevices(instrumentationTestExecutor);
	}

	private void addAllInstrumentationArgs(
			final RemoteAndroidTestRunner remoteAndroidTestRunner,
			final Map<String, String> parsedInstrumentationArgs) {

		if (null == parsedInstrumentationArgs)
			return;

		for (final Map.Entry<String, String> entry : parsedInstrumentationArgs
				.entrySet()) {
			remoteAndroidTestRunner.addInstrumentationArg(entry.getKey(),
					entry.getValue());
		}
	}

	// private void parseConfiguration() {
	// // we got config in pom ... lets use it,
	// if (test != null) {
	// if (StringUtils.isNotEmpty(test.getSkip())) {
	// parsedSkip = test.getSkip();
	// } else {
	// parsedSkip = testSkip;
	// }
	// if (StringUtils.isNotEmpty(test.getInstrumentationPackage())) {
	// parsedInstrumentationPackage = test.getInstrumentationPackage();
	// } else {
	// parsedInstrumentationPackage = testInstrumentationPackage;
	// }
	// if (StringUtils.isNotEmpty(test.getInstrumentationRunner())) {
	// parsedInstrumentationRunner = test.getInstrumentationRunner();
	// } else {
	// parsedInstrumentationRunner = testInstrumentationRunner;
	// }
	// if (test.getClasses() != null && !test.getClasses().isEmpty()) {
	// parsedClasses = test.getClasses();
	// } else {
	// parsedClasses = testClasses;
	// }
	// if (test.getAnnotations() != null
	// && !test.getAnnotations().isEmpty()) {
	// parsedAnnotations = test.getAnnotations();
	// } else {
	// parsedAnnotations = testAnnotations;
	// }
	// if (test.getExcludeAnnotations() != null
	// && !test.getExcludeAnnotations().isEmpty()) {
	// parsedExcludeAnnotations = test.getExcludeAnnotations();
	// } else {
	// parsedExcludeAnnotations = testExcludeAnnotations;
	// }
	// if (test.getPackages() != null && !test.getPackages().isEmpty()) {
	// parsedPackages = test.getPackages();
	// } else {
	// parsedPackages = testPackages;
	// }
	// if (StringUtils.isNotEmpty(test.getTestSize())) {
	// parsedTestSize = test.getTestSize();
	// } else {
	// parsedTestSize = testTestSize;
	// }
	// if (test.isCoverage() != null) {
	// parsedCoverage = test.isCoverage();
	// } else {
	// parsedCoverage = testCoverage;
	// }
	// if (test.getCoverageFile() != null) {
	// parsedCoverageFile = test.getCoverageFile();
	// } else {
	// parsedCoverageFile = "";
	// }
	// if (test.isDebug() != null) {
	// parsedDebug = test.isDebug();
	// } else {
	// parsedDebug = testDebug;
	// }
	// if (test.isLogOnly() != null) {
	// parsedLogOnly = test.isLogOnly();
	// } else {
	// parsedLogOnly = testLogOnly;
	// }
	// if (test.isCreateReport() != null) {
	// parsedCreateReport = test.isCreateReport();
	// } else {
	// parsedCreateReport = testCreateReport;
	// }
	//
	// parsedInstrumentationArgs = InstrumentationArgumentParser
	// .parse(test.getInstrumentationArgs());
	// }
	// // no pom, we take properties
	// else {
	// parsedSkip = testSkip;
	// parsedInstrumentationPackage = testInstrumentationPackage;
	// parsedInstrumentationRunner = testInstrumentationRunner;
	// parsedClasses = testClasses;
	// parsedAnnotations = testAnnotations;
	// parsedExcludeAnnotations = testExcludeAnnotations;
	// parsedPackages = testPackages;
	// parsedTestSize = testTestSize;
	// parsedCoverage = testCoverage;
	// parsedCoverageFile = testCoverageFile;
	// parsedDebug = testDebug;
	// parsedLogOnly = testLogOnly;
	// parsedCreateReport = testCreateReport;
	// parsedInstrumentationArgs = InstrumentationArgumentParser
	// .parse(testInstrumentationArgs);
	// }
	// }

	/**
	 * Helper method to build a comma separated string from a list. Blank
	 * strings are filtered out
	 * 
	 * @param lines
	 *            A list of strings
	 * @return Comma separated String from given list
	 */
	protected static String buildCommaSeparatedString(List<String> lines) {
		if (lines == null || lines.size() == 0) {
			return null;
		}

		List<String> strings = new ArrayList<String>(lines.size());
		for (String str : lines) { // filter out blank strings
			if (StringUtils.isNotBlank(str)) {
				strings.add(StringUtils.trimToEmpty(str));
			}
		}

		return StringUtils.join(strings, ",");
	}
}
