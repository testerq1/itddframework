package ate.ua.robotium;

import ate.util.Expect;

import com.android.ddmlib.MultiLineReceiver;

public class RegExpMatchReceiver extends MultiLineReceiver {
	private String pattern;
	private String rtn="";
	private Expect expect=new Expect();
	public RegExpMatchReceiver(String pattern) {
		this.pattern=pattern;
	}

	@Override
	public void processNewLines(String[] lines) {
		for (String line : lines) {
			rtn+=line;
		}
	}

	@Override
	public boolean isCancelled() {
		return false;
	}

	public boolean matched() {
		this.flush();
		return expect.match(rtn,pattern, true, Expect.MATCH_PATTERN.REGEXP);
	}
}