package ate.ua.robotium;

import static com.android.ddmlib.testrunner.ITestRunListener.TestFailure.ERROR;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import ate.rm.dev.Testbed;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.testrunner.ITestRunListener;
import com.android.ddmlib.testrunner.TestIdentifier;
import com.jayway.maven.plugins.android.common.DeviceHelper;

public abstract class ATestRunListener implements ITestRunListener {
	protected String directory;
	protected static Logger log = LoggerFactory
			.getLogger(ATestRunListener.class);
	protected boolean parsedCreateReport = false;
	/**
	 * the indent used in the log to group items that belong together visually *
	 */
	protected static final String INDENT = "  ";
	/**
	 * Junit report schema documentation is sparse. Here are some hints
	 * 
	 * @see "http://mail-archives.apache.org/mod_mbox/ ant-dev/200902.mbox/%3
	 *      Cdffc72020902241548l4316d645w2e98caf5f0aac770@mail.gmail.com%3E"
	 * @see "http://junitpdfreport.sourceforge.net/managedcontent/PdfTranslation"
	 */
	protected static final String TAG_TESTSUITES = "testsuites";

	protected static final String TAG_TESTSUITE = "testsuite";
	protected static final String ATTR_TESTSUITE_ERRORS = "errors";
	protected static final String ATTR_TESTSUITE_FAILURES = "failures";
	protected static final String ATTR_TESTSUITE_HOSTNAME = "hostname";
	protected static final String ATTR_TESTSUITE_NAME = "name";
	protected static final String ATTR_TESTSUITE_TESTS = "tests";
	protected static final String ATTR_TESTSUITE_TIME = "time";
	protected static final String ATTR_TESTSUITE_TIMESTAMP = "timestamp";

	protected static final String TAG_PROPERTIES = "properties";
	protected static final String TAG_PROPERTY = "property";
	protected static final String ATTR_PROPERTY_NAME = "name";
	protected static final String ATTR_PROPERTY_VALUE = "value";

	protected static final String TAG_TESTCASE = "testcase";
	protected static final String ATTR_TESTCASE_NAME = "name";
	protected static final String ATTR_TESTCASE_CLASSNAME = "classname";
	protected static final String ATTR_TESTCASE_TIME = "time";

	protected static final String TAG_ERROR = "error";
	protected static final String TAG_FAILURE = "failure";
	protected static final String ATTR_MESSAGE = "message";
	protected static final String ATTR_TYPE = "type";

	/**
	 * time format for the output of milliseconds in seconds in the xml file *
	 */
	protected final NumberFormat timeFormatter = new DecimalFormat("#0.0000");

	protected int testCount = 0;
	protected int testRunCount = 0;
	protected int testFailureCount = 0;
	protected int testErrorCount = 0;
	protected String testRunFailureCause = null;

	/**
	 * the emulator or device we are running the tests on *
	 */
	protected final IDevice device;

	protected final String deviceLogLinePrefix;

	// junit xml report related fields
	protected Document junitReport;

	protected Node testSuiteNode;

	/**
	 * node for the current test case for junit report
	 */
	protected Node currentTestCaseNode;

	/**
	 * start time of current test case in millis, reset with each test start
	 */
	protected long currentTestCaseStartTime;

	// we track if we have problems and then report upstream
	protected boolean threwException = false;

	protected final StringBuilder exceptionMessages = new StringBuilder();

	protected ATestRunListener(IDevice device) {
		this.device = device;
		this.deviceLogLinePrefix = DeviceHelper.getDeviceLogLinePrefix(device);
		Testbed tb = Testbed.getTestbed();
		if (tb.arg("testng_output_dir") != null)
			directory = tb.arg("testng_output_dir");
		else
			directory = "test-output";
	}

	public IDevice get_device() {
		return device;
	}

	/**
	 * @return all exception messages thrown during test execution on the test
	 *         run time (not the Android device or emulator)
	 */
	public String getExceptionMessages() {
		return exceptionMessages.toString();
	}

	/**
	 * @return the cause of test failure if any.
	 */
	public String getTestRunFailureCause() {
		return testRunFailureCause;
	}

	/**
	 * @return if any failures or errors occurred in the test run.
	 */
	public boolean hasFailuresOrErrors() {
		return testErrorCount > 0 || testFailureCount > 0;
	}

	/**
	 * Log all the metrics out in to key: value lines.
	 * 
	 * @param metrics
	 */
	protected void logMetrics(Map<String, String> metrics) {
		for (Map.Entry<String, String> entry : metrics.entrySet()) {
			log.info(deviceLogLinePrefix + INDENT + INDENT + entry.getKey()
					+ ": " + entry.getValue());
		}
	}
	/**
	 * Parse a trace string for the exception class. Assumes that it is the
	 * start of the trace and ends at the first ":".
	 * 
	 * @param trace
	 * @return Exception class as string or empty string
	 */
	protected String parseForException(String trace) {
		if (StringUtils.isNotBlank(trace)) {
			return trace.substring(0, trace.indexOf(":"));
		} else {
			return StringUtils.EMPTY;
		}
	}

	/**
	 * Parse a trace string for the message in it. Assumes that the message is
	 * located after ":" and before "\r\n".
	 * 
	 * @param trace
	 * @return message or empty string
	 */
	protected String parseForMessage(String trace) {
		if (StringUtils.isNotBlank(trace)) {
			String newline = "\r\n";
			// if there is message like
			// junit.junit.framework.AssertionFailedError ... there is no
			// message
			int messageEnd = trace.indexOf(newline);
			boolean hasMessage = !trace.startsWith("junit.") && messageEnd > 0;
			if (hasMessage) {
				int messageStart = trace.indexOf(":") + 2;
				if (messageStart > messageEnd) {
					messageEnd = trace.indexOf(newline + "at");
					// match start of stack trace "\r\nat org.junit....."
					if (messageStart > messageEnd) {
						// ':' wasn't found in message but in stack trace
						messageStart = 0;
					}
				}
				return trace.substring(messageStart, messageEnd);
			} else {
				return StringUtils.EMPTY;
			}
		} else {
			return StringUtils.EMPTY;
		}
	}
	public void set_create_report(boolean b) {
		this.parsedCreateReport = b;
	}

	@Override
	public void testEnded(TestIdentifier testIdentifier,
			Map<String, String> testMetrics) {
		log.info(deviceLogLinePrefix
				+ String.format("%1$s%1$sEnd [%2$d/%3$d]: %4$s", INDENT,
						testRunCount, testCount, testIdentifier.toString()));
		logMetrics(testMetrics);

		if (parsedCreateReport) {
			testSuiteNode.appendChild(currentTestCaseNode);
			NamedNodeMap testCaseAttributes = currentTestCaseNode
					.getAttributes();

			Attr timeAttr = junitReport.createAttribute(ATTR_TESTCASE_TIME);

			long now = new Date().getTime();
			double seconds = (now - currentTestCaseStartTime) / 1000.0;
			timeAttr.setValue(timeFormatter.format(seconds));
			testCaseAttributes.setNamedItem(timeAttr);
		}
	}
	@Override
	public void testFailed(TestFailure status, TestIdentifier testIdentifier,
			String trace) {
		if (status == ERROR) {
			++testErrorCount;
		} else {
			++testFailureCount;
		}
		log.info(deviceLogLinePrefix + INDENT + INDENT + status.name() + ":"
				+ testIdentifier.toString());
		log.info(deviceLogLinePrefix + INDENT + INDENT + trace);

		if (parsedCreateReport) {
			Node errorFailureNode;
			NamedNodeMap errorfailureAttributes;
			if (status == ERROR) {
				errorFailureNode = junitReport.createElement(TAG_ERROR);
				errorfailureAttributes = errorFailureNode.getAttributes();
			} else {
				errorFailureNode = junitReport.createElement(TAG_FAILURE);
				errorfailureAttributes = errorFailureNode.getAttributes();
			}

			//errorFailureNode.setTextContent(trace);

			Attr msgAttr = junitReport.createAttribute(ATTR_MESSAGE);
			msgAttr.setValue(parseForMessage(trace));
			errorfailureAttributes.setNamedItem(msgAttr);

			Attr typeAttr = junitReport.createAttribute(ATTR_TYPE);
			typeAttr.setValue(parseForException(trace));
			errorfailureAttributes.setNamedItem(typeAttr);

			currentTestCaseNode.appendChild(errorFailureNode);
		}
	}

	@Override
	public void testRunEnded(long elapsedTime, Map<String, String> runMetrics) {
		log.info(deviceLogLinePrefix + INDENT + "Run ended: " + elapsedTime
				+ " ms");
		if (hasFailuresOrErrors()) {
			log.error(deviceLogLinePrefix + INDENT + "FAILURES!!!");
		}
		log.info(INDENT + "Tests run: " + testRunCount
				+ (testRunCount < testCount ? " (of " + testCount + ")" : "")
				+ ",  Failures: " + testFailureCount + ",  Errors: "
				+ testErrorCount);

		if (parsedCreateReport) {
			NamedNodeMap testSuiteAttributes = testSuiteNode.getAttributes();

			Attr testCountAttr = junitReport
					.createAttribute(ATTR_TESTSUITE_TESTS);
			testCountAttr.setValue(Integer.toString(testCount));
			testSuiteAttributes.setNamedItem(testCountAttr);

			Attr testFailuresAttr = junitReport
					.createAttribute(ATTR_TESTSUITE_FAILURES);
			testFailuresAttr.setValue(Integer.toString(testFailureCount));
			testSuiteAttributes.setNamedItem(testFailuresAttr);

			Attr testErrorsAttr = junitReport
					.createAttribute(ATTR_TESTSUITE_ERRORS);
			testErrorsAttr.setValue(Integer.toString(testErrorCount));
			testSuiteAttributes.setNamedItem(testErrorsAttr);

			Attr timeAttr = junitReport.createAttribute(ATTR_TESTSUITE_TIME);
			timeAttr.setValue(timeFormatter.format(elapsedTime / 1000.0));
			testSuiteAttributes.setNamedItem(timeAttr);

			Attr timeStampAttr = junitReport
					.createAttribute(ATTR_TESTSUITE_TIMESTAMP);
			timeStampAttr.setValue(new Date().toString());
			testSuiteAttributes.setNamedItem(timeStampAttr);
		}

		logMetrics(runMetrics);

		if (parsedCreateReport) {
			writeJunitReportToFile();
		}
	}

	/**
	 * @return if the test run itself failed - a failure in the test
	 *         infrastructure, not a test failure.
	 */
	public boolean testRunFailed() {
		return testRunFailureCause != null;
	}

	public void testRunFailed(String errorMessage) {
		testRunFailureCause = errorMessage;
		log.info(deviceLogLinePrefix + INDENT + "Run failed: " + errorMessage);
	}

	@Override
	public void testRunStarted(String runName, int testCount) {
		this.testCount = testCount;
		log.info(deviceLogLinePrefix + INDENT + "Run started: " + runName
				+ ", " + testCount + " tests:");

		if (parsedCreateReport) {
			try {
				DocumentBuilderFactory fact = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder parser = null;
				parser = fact.newDocumentBuilder();
				junitReport = parser.newDocument();

				Node testSuitesNode = junitReport.createElement(TAG_TESTSUITES);
				junitReport.appendChild(testSuitesNode);

				testSuiteNode = junitReport.createElement(TAG_TESTSUITE);
				NamedNodeMap testSuiteAttributes = testSuiteNode
						.getAttributes();

				Attr nameAttr = junitReport
						.createAttribute(ATTR_TESTSUITE_NAME);
				nameAttr.setValue(runName);
				testSuiteAttributes.setNamedItem(nameAttr);

				Attr hostnameAttr = junitReport
						.createAttribute(ATTR_TESTSUITE_HOSTNAME);
				hostnameAttr.setValue(DeviceHelper.getDescriptiveName(device));
				testSuiteAttributes.setNamedItem(hostnameAttr);

				Node propertiesNode = junitReport.createElement(TAG_PROPERTIES);
				Node propertyNode;
				NamedNodeMap propertyAttributes;
				Attr propNameAttr;
				Attr propValueAttr;
				for (Map.Entry<Object, Object> systemProperty : System
						.getProperties().entrySet()) {
					propertyNode = junitReport.createElement(TAG_PROPERTY);
					propertyAttributes = propertyNode.getAttributes();

					propNameAttr = junitReport
							.createAttribute(ATTR_PROPERTY_NAME);
					propNameAttr.setValue(systemProperty.getKey().toString());
					propertyAttributes.setNamedItem(propNameAttr);

					propValueAttr = junitReport
							.createAttribute(ATTR_PROPERTY_VALUE);
					propValueAttr
							.setValue(systemProperty.getValue().toString());
					propertyAttributes.setNamedItem(propValueAttr);

					propertiesNode.appendChild(propertyNode);
				}
				Map<String, String> deviceProperties = device.getProperties();
				for (Map.Entry<String, String> deviceProperty : deviceProperties
						.entrySet()) {
					propertyNode = junitReport.createElement(TAG_PROPERTY);
					propertyAttributes = propertyNode.getAttributes();

					propNameAttr = junitReport
							.createAttribute(ATTR_PROPERTY_NAME);
					propNameAttr.setValue(deviceProperty.getKey());
					propertyAttributes.setNamedItem(propNameAttr);

					propValueAttr = junitReport
							.createAttribute(ATTR_PROPERTY_VALUE);
					propValueAttr.setValue(deviceProperty.getValue());
					propertyAttributes.setNamedItem(propValueAttr);

					propertiesNode.appendChild(propertyNode);
				}

				testSuiteNode.appendChild(propertiesNode);

				testSuitesNode.appendChild(testSuiteNode);

			} catch (ParserConfigurationException e) {
				threwException = true;
				exceptionMessages.append("Failed to create document");
				exceptionMessages.append(e.getMessage());
			}
		}
	}

	public void testRunStopped(long elapsedTime) {
		log.info(deviceLogLinePrefix + INDENT + "Run stopped:" + elapsedTime);
	}
	
	@Override
	public void testStarted(TestIdentifier testIdentifier) {
		testRunCount++;
		log.info(deviceLogLinePrefix
				+ String.format("%1$s%1$sStart [%2$d/%3$d]: %4$s", INDENT,
						testRunCount, testCount, testIdentifier.toString()));

		if (parsedCreateReport) {
			// reset start time for each test run
			currentTestCaseStartTime = new Date().getTime();

			currentTestCaseNode = junitReport.createElement(TAG_TESTCASE);
			NamedNodeMap testCaseAttributes = currentTestCaseNode
					.getAttributes();

			Attr classAttr = junitReport
					.createAttribute(ATTR_TESTCASE_CLASSNAME);
			classAttr.setValue(testIdentifier.getClassName());
			testCaseAttributes.setNamedItem(classAttr);

			Attr methodAttr = junitReport.createAttribute(ATTR_TESTCASE_NAME);
			methodAttr.setValue(testIdentifier.getTestName());
			testCaseAttributes.setNamedItem(methodAttr);
		}
	}

	/**
	 * @return if any exception was thrown during the test run on the build
	 *         system (not the Android device or emulator)
	 */
	public boolean threwException() {
		return threwException;
	}

	protected abstract void writeJunitReportToFile();

	/**
	 * Write the junit report xml file.
	 */
	protected void writeJunitReportToFile(String prefix) {
		TransformerFactory xfactory = TransformerFactory.newInstance();
		Transformer xformer = null;
		try {
			xformer = xfactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}
		Source source = new DOMSource(junitReport);

		FileWriter writer = null;
		try {
			FileUtils.forceMkdir(new File(directory));

			String fileName = new StringBuilder().append(directory)
					.append("/" + prefix + "-")
					.append(DeviceHelper.getDescriptiveName(device))
					.append(".xml").toString();
			File reportFile = new File(fileName);
			writer = new FileWriter(reportFile);
			Result result = new StreamResult(writer);

			xformer.transform(source, result);
			log.info(deviceLogLinePrefix + "Report file written to "
					+ reportFile.getAbsolutePath());
		} catch (IOException e) {
			threwException = true;
			exceptionMessages.append("Failed to write test report file");
			exceptionMessages.append(e.getMessage());
		} catch (TransformerException e) {
			threwException = true;
			exceptionMessages
					.append("Failed to transform document to write to test report file");
			exceptionMessages.append(e.getMessage());
		} finally {
			IOUtils.closeQuietly(writer);
		}
	}
}
