package ate.ua.robotium;

import com.android.ddmlib.IDevice;

public class RobotiumAndroidTestRunListener extends ATestRunListener {

	public RobotiumAndroidTestRunListener(IDevice device) {
		super(device);
	}
	protected void writeJunitReportToFile() {
		writeJunitReportToFile("robotium");
	}
}