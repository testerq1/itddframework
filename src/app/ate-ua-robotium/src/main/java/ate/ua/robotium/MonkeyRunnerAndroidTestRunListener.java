package ate.ua.robotium;

import java.io.IOException;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;

public class MonkeyRunnerAndroidTestRunListener extends ATestRunListener {
	
        private static final String SCREENSHOT_SUFFIX = "_screenshot.png";        

        /**
         * Create a new test run listener.
         * 
         * @param project
         *            the test project.
         * @param device
         *            the device on which test is executed.
         */
        public MonkeyRunnerAndroidTestRunListener( IDevice device )
        {
        	super(device);            
        }
       
        private void executeOnAdbShell( String command )
        {
            try
            {
                device.executeShellCommand( command, new IShellOutputReceiver()
                {
                    @Override
                    public boolean isCancelled()
                    {
                        return false;
                    }

                    @Override
                    public void flush()
                    {
                    }

                    @Override
                    public void addOutput( byte[] data, int offset, int length )
                    {
                    }
                } );
            }
            catch ( TimeoutException e )
            {
                log.error( e +"");
            }
            catch ( AdbCommandRejectedException e )
            {
                log.error( e +"" );
            }
            catch ( ShellCommandUnresponsiveException e )
            {
                log.error( e +"" );
            }
            catch ( IOException e )
            {
                log.error( e +"" );
            }
        }

        @Override
        public void testRunFailed( String errorMessage )
        {
            testRunFailureCause = errorMessage;
            log.info( deviceLogLinePrefix + INDENT + "Run failed: " + errorMessage );
        }

        @Override
        public void testRunStopped( long elapsedTime )
        {
            log.info( deviceLogLinePrefix + INDENT + "Run stopped:" + elapsedTime );
        }

        

        /**
         * Write the junit report xml file.
         */
        protected void writeJunitReportToFile()
        {
        	writeJunitReportToFile("monkeyrunner");
        }
       
}
