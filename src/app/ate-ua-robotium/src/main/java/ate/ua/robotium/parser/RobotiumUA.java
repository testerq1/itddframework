package ate.ua.robotium.parser;

import io.selendroid.common.SelendroidCapabilities;
import io.selendroid.standalone.SelendroidConfiguration;
import io.selendroid.standalone.android.AndroidApp;
import io.selendroid.standalone.android.AndroidDevice;
import io.selendroid.standalone.android.AndroidEmulator;
import io.selendroid.standalone.android.AndroidEmulatorPowerStateListener;
import io.selendroid.standalone.android.AndroidSdk;
import io.selendroid.standalone.android.DeviceManager;
import io.selendroid.standalone.android.HardwareDeviceListener;
import io.selendroid.standalone.android.impl.DefaultAndroidEmulator;
import io.selendroid.standalone.android.impl.DefaultDeviceManager;
import io.selendroid.standalone.builder.SelendroidServerBuilder;
import io.selendroid.standalone.exceptions.AndroidDeviceException;
import io.selendroid.standalone.exceptions.AndroidSdkException;
import io.selendroid.standalone.exceptions.DeviceStoreException;
import io.selendroid.standalone.exceptions.ShellCommandException;
import io.selendroid.standalone.server.model.DeviceStore;
import io.selendroid.standalone.server.model.MyDriver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ate.AbstractURIHandler;
import ate.rm.dev.Host;
import ate.ua.UAOption;
import ate.util.Expect;

import com.jayway.maven.plugins.android.standalonemojos.DevicesMojo;

/**
 * 默认在apk文件夹下寻找测试对象apk及测试apk<br>
 * 
 * @author ravih
 * 
 * @param <T>
 */
public class RobotiumUA extends AbstractURIHandler {
	public class DefaultHardwareDeviceListener implements
			HardwareDeviceListener {
		private DeviceStore store = null;

		public DefaultHardwareDeviceListener(DeviceStore store) {
			this.store = store;
		}

		@Override
		public void onDeviceConnected(AndroidDevice device) {
			try {
				store.addDevice(device);
			} catch (AndroidDeviceException e) {
				log.info(e.getMessage());
			}
		}

		@Override
		public void onDeviceDisconnected(AndroidDevice device) {
			try {
				// remove device from store
				store.removeAndroidDevice(device);
			} catch (DeviceStoreException e) {
				log.warn("The device cannot be removed: " + e.getMessage());
			}
		}

		@Override
		public void onDeviceChanged(AndroidDevice arg0) {
			// TODO Auto-generated method stub

		}
	}

	private static String APK_HOME = Host.HOME + File.separator + "apk";
	protected SelendroidCapabilities cap = new SelendroidCapabilities();
	protected AndroidDevice device;
	private Expect regexp = new Expect();
	protected MyDriver driver;
	protected SelendroidConfiguration config = new SelendroidConfiguration();
	protected AndroidApp app;
	SelendroidServerBuilder selendroidApkBuilder;
	File apk_file;
	List<File> test_apk_files = new ArrayList<File>();
	private DeviceStore deviceStore;

	private HardwareDeviceListener hardwareDeviceListener = null;

	private DeviceManager hardwareDeviceManager;

	private DevicesMojo devices;

	@Override
	public RobotiumUA build(UAOption option, Object value) {
		super.build(option, value);

		if (option == UA_URI) {
			String tmp = this.get_schemeSpecificPart();
			if (tmp.startsWith(".") || tmp.startsWith("/"))
				apk_file = new File(tmp);
			else if (tmp.startsWith("file://")) {
				try {
					apk_file = new File(new URI(tmp));
				} catch (URISyntaxException e) {
					throw new Error("Invalid file path" + tmp);
				}
			} else
				apk_file = new File("apk" + File.separator + tmp);

			if (!apk_file.exists() || !apk_file.isFile())
				throw new Error("Invalid file path" + tmp);

			tmp = this.get_fragment();
			String folder, filter;
			if (tmp.lastIndexOf("/") > 0) {
				folder = tmp.substring(0, tmp.lastIndexOf("/"));
				filter = tmp.substring(tmp.lastIndexOf("/") + 1);
			} else {
				folder = "apk";
				filter = tmp;
			}

			File ff = null;
			if (folder.startsWith("file://")) {
				try {
					ff = new File(new URI(folder));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			} else {
				ff = new File(folder);
			}
			for (File fname : ff.listFiles()) {
				if (regexp.match_e(fname.getName(), filter))
					test_apk_files.add(fname);
			}
		}

		return this;
	}

	public List<File> get_all_test_apk() {
		return this.test_apk_files;
	}

	/**
	 * {"desiredCapabilities": {"aut": "io.selendroid.testapp:0.8.0"}}
	 * 
	 * @param caps
	 * @return
	 * @throws AndroidDeviceException
	 */
	public AndroidDevice get_android_device(SelendroidCapabilities caps) {
		AndroidDevice device = null;
		// String serial = caps.getSerial();
		// if (serial != null) {
		// if (serial.startsWith("emulator")) {
		// deviceStore.setAvdName(getAvdName(Integer.parseInt(serial
		// .substring(9))));
		// } else {
		// deviceStore.setDeviceSerial(serial);
		// }
		// }
		try {
			device = deviceStore.findAndroidDevice(caps);				
		} catch (DeviceStoreException e) {
			e.printStackTrace();
			log.warn(caps.getRawCapabilities().toString());
		}

		return device;
	}

	public File get_apk_file() {
		return this.apk_file;
	}

	public AndroidApp get_app() {
		return this.app;
	}

	public String get_app_id() {
		try {
			return this.app.getAppId();
		} catch (AndroidSdkException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public SelendroidCapabilities get_aut_cap(String aut) {
		// JSONObject jaut=new JSONObject();
		Map<String, Map> from = new HashMap();
		HashMap<String, String> aMap = new HashMap<String, String>();
		aMap.put("aut", aut);
		from.put("desiredCapabilities", aMap);
		return new SelendroidCapabilities(from);
	}

	public SelendroidCapabilities get_cap() {
		return this.cap;
	}

	@Override
	public String get_default_schema() {
		return "robotium";
	}

	private String getAvdName(int port) {
		if (port == -1) {
			return null;
		}
		String cmd = "(sleep 4; echo 'avd name') | telnet localhost " + port;
		try {
			Process proc = Runtime.getRuntime().exec(
					new String[] { "/bin/sh", "-c", cmd });
			try {
				proc.waitFor();
			} catch (InterruptedException e) {
			}
			BufferedReader read = new BufferedReader(new InputStreamReader(
					proc.getInputStream()));
			while (read.ready()) {
				if (read.readLine().equals("OK")) {
					break;
				}
			}
			return read.readLine();
		} catch (IOException e) {
			return null;
		}
	}

	private void initAndroidDevices() throws AndroidDeviceException {
		hardwareDeviceManager = new DefaultDeviceManager(AndroidSdk.adb()
				.getAbsolutePath(), config.shouldKeepAdbAlive());
		deviceStore = new DeviceStore(config.getEmulatorPort(),
				hardwareDeviceManager);
		AndroidEmulatorPowerStateListener emulatorPowerStateListener = new AndroidEmulatorPowerStateListener() {

			@Override
			public void onDeviceStarted(String avdName, String serial) {
				AndroidEmulator emulator = findEmulator(avdName);
				if (emulator != null) {
					Integer port = Integer.parseInt(serial.replace("emulator-",
							""));
					emulator.setSerial(port);
					emulator.setWasStartedBySelendroid(false);
				}
			}

			AndroidEmulator findEmulator(String avdName) {
				for (AndroidDevice device : deviceStore.getDevices()) {
					if (device instanceof AndroidEmulator) {
						AndroidEmulator emulator = (AndroidEmulator) device;
						if (avdName.equals(emulator.getAvdName())) {
							return emulator;
						}
					}
				}
				return null;
			}

			@Override
			public void onDeviceStopped(String avdName) {
				// do nothing
			}
		};
		
		if (hardwareDeviceListener == null) {
			hardwareDeviceListener = new DefaultHardwareDeviceListener(
					deviceStore);
		}

		hardwareDeviceManager.initialize(hardwareDeviceListener,
				emulatorPowerStateListener);

		List<AndroidEmulator> emulators = DefaultAndroidEmulator
				.listAvailableAvds();
		deviceStore.addEmulators(emulators);

		if (deviceStore.getDevices().isEmpty()) {
			log.warn("Warning: "
					+ new Exception(
							"No android virtual devices were found. "
									+ "Please start the android tool and create emulators and restart the selendroid-standalone "
									+ "or plugin an Android hardware device via USB."));
		}
	}

	@Override
	public boolean is_ready() {
		return app != null;
	}

	private AndroidApp resignApp(File appfile) throws ShellCommandException,
			AndroidSdkException {
		AndroidApp app = selendroidApkBuilder.resignApp(appfile);
		log.info("App " + app.getAppId() + " has been resignApp.");
		return app;
	}

	public void set_screen_size(String screenSize) {
		cap.setScreenSize(screenSize);
	}

	public void set_serial(String serial) {
		cap.setSerial(serial);
	}

	@Override
	public void startup() {
		java.util.logging.LogManager logManager = java.util.logging.LogManager
				.getLogManager();
		InputStream inputStream = null;
		try {
			// 重新初始化日志属性并重新读取日志配置。
			File logconf = new File(Host.CONF_HOME + File.separator
					+ "log.properties");
			inputStream = new FileInputStream(logconf);

			logManager.readConfiguration(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != inputStream)
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

		if (System.getenv("ANDROID_HOME") == null) {
			log.error("ANDROID_HOME envirment variable not set");
			throw new Error("ANDROID_HOME envirment variable not set");
		}

		selendroidApkBuilder = new SelendroidServerBuilder(config);

		// robotium不支持webview
		// config.setNoWebViewApp(true);
		try {
			app = resignApp(apk_file);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Error("resign app failure ");
		}
		try {
			initAndroidDevices();

			// driver = new MyDriver(config);
			//
			// for (String tmp : driver.getConfiguredApps().keySet()) {
			// app = driver.getConfiguredApps().get(tmp);
			// log.info("installed: {} {}", tmp, app.getAppId());
			// }
			//
			// cap.setAut(app.getAppId());
			//
			// device = driver.getAndroidDevice(cap);
			//
			// log.info(device.getSerial());

		} catch (Exception e) {
			e.printStackTrace();
			throw new Error("init devices failure.");
		}
	}

	@Override
	public void teardown() {
		if (driver != null) {
			driver.quitSelendroid();
			try {
				device.uninstall(app);
			} catch (AndroidSdkException e) {
				e.printStackTrace();
			}
		}
		Host.localhost().kill("adb");
	}
}
