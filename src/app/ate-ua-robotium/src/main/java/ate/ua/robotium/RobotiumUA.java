package ate.ua.robotium;

import io.selendroid.standalone.SelendroidConfiguration;
import io.selendroid.standalone.android.AndroidApp;
import io.selendroid.standalone.android.impl.DefaultAndroidApp;
import io.selendroid.standalone.builder.SelendroidServerBuilder;
import io.selendroid.standalone.exceptions.AndroidSdkException;
import io.selendroid.standalone.exceptions.ShellCommandException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.azeckoski.reflectutils.ReflectUtils;
import org.azeckoski.reflectutils.exceptions.FieldnameNotFoundException;

import ate.AbstractURIHandler;
import ate.rm.dev.Host;
import ate.ua.RunFailureException;
import ate.ua.UAOption;
import ate.util.Expect;

import com.android.ddmlib.DdmPreferences;
import com.android.ddmlib.IDevice;
import com.github.rtyley.android.screenshot.celebrity.Screenshots;
import com.jayway.maven.plugins.android.AbstractAndroidMojo;
import com.jayway.maven.plugins.android.standalonemojos.DevicesMojo;
import com.jayway.maven.plugins.android.standalonemojos.MonkeyMojo;
import com.jayway.maven.plugins.android.standalonemojos.MonkeyRunnerMojo;

/**
 * 默认在apk文件夹下寻找测试对象apk及测试apk<br>
 * 
 * @author ravih
 * 
 * @param <T>
 */
public class RobotiumUA extends AbstractURIHandler {
	public enum MOJO_RESULT {
		FAILED_ON_DEVICE, FAILED_TO_COMPLETE, FAILED_ON_EXCEPTION, OK;

		public boolean is_ok() {
			return this.equals(OK);
		}
	}
	static{		
		/*SEVERE（最高值）
		WARNING
		INFO
		CONFIG
		FINE
		FINER
		FINEST（最低值）*/
		java.util.logging.LogManager logManager = java.util.logging.LogManager
				.getLogManager();
		InputStream inputStream = null;
		try {
			// 重新初始化日志属性并重新读取日志配置。
			File logconf = new File(Host.CONF_HOME + File.separator
					+ "log.properties");
			inputStream = new FileInputStream(logconf);

			logManager.readConfiguration(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != inputStream)
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

		if (System.getenv("ANDROID_HOME") == null) {
			log.error("ANDROID_HOME envirment variable not set");
			throw new Error("ANDROID_HOME envirment variable not set");
		}
	}
	
	private Expect regexp = new Expect();
	private SelendroidConfiguration config = new SelendroidConfiguration();
	private AndroidApp resigned_app;
	private AndroidApp resigned_testapp;
	private SelendroidServerBuilder selendroidApkBuilder;
	private File apk_file;
	private List<File> test_apk_files = new ArrayList<File>();
	private File apk_parent;
	private AbstractAndroidMojo current_mojo;
	private DevicesMojo devices;
	private String sdkPlatform;

	private IDevice device = null;
	
	/**
	 * 清除测试listener及uninstall app
	 */
	public void after_method(){
		clean_all_test_result();
		
		if(resigned_testapp!=null)
			log.debug(uninstall(resigned_testapp));
		if(resigned_app!=null)
			log.debug(uninstall(this.resigned_app));
	}
	
	@Override
	public RobotiumUA build(UAOption option, Object value) {
		super.build(option, value);

		if (option == UA_URI) {
			String tmp = this.get_schemeSpecificPart();
			if (tmp.startsWith(".") || tmp.startsWith("/"))
				apk_file = new File(tmp);
			else if (tmp.startsWith("file://")) {
				try {
					apk_file = new File(new URI(tmp));
				} catch (URISyntaxException e) {
					throw new Error("Invalid file path" + tmp);
				}
			} else
				apk_file = new File("apk" + File.separator + tmp);

			if (!apk_file.exists() || !apk_file.isFile())
				throw new Error("Invalid file path" + tmp);

			tmp = this.get_fragment();
			String folder, filter;
			if (tmp.lastIndexOf("/") > 0) {
				folder = tmp.substring(0, tmp.lastIndexOf("/"));
				filter = tmp.substring(tmp.lastIndexOf("/") + 1);
			} else {
				folder = "apk";
				filter = tmp;
			}

			if (folder.startsWith("file://")) {
				try {
					apk_parent = new File(new URI(folder));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			} else {
				apk_parent = new File(folder);
			}
			for (File fname : apk_parent.listFiles()) {
				if (regexp.match_e(fname.getName(), filter)&& !fname.getName().startsWith("resigned-"))
					test_apk_files.add(fname);
			}
		}
		return this;
	} 
	
	/**
	 * 清除文件夹下所有以resigned-开头的文件，
	 * 在startup以后执行
	 */
	public void clear_resigned_apks(){
		if(null==apk_parent||!apk_parent.exists()){
			log.warn("apk folder doesn't exist.");
			return;
		}
		for (File fname : apk_parent.listFiles()) {
			if (fname.getName().startsWith("resigned-"))
				fname.delete();
		}	
	}
	
	/**
	 * 清除当前mojo的所有日志listener
	 */
	public void clean_all_test_result(){
		if(this.current_mojo!=null)
			current_mojo.clear_all_listener();
	}	
	
	public List<File> get_all_test_apk() {
		return this.test_apk_files;
	}
	
	public List<ATestRunListener> get_all_test_result(){
		if(this.current_mojo!=null)
			return current_mojo.get_all_listener();
		return null;
	}	

	public File get_apk_file() {
		return this.apk_file;
	}

	public AndroidApp get_app() {
		return this.resigned_app;
	}
	
	public String get_app_id() {
		try {
			return this.resigned_app.getAppId();
		} catch (AndroidSdkException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public IDevice get_default_device() {
		return this.device;
	}
	
	@Override
	public String get_default_schema() {
		return "robotium";
	}

	public List<File> get_test_apk_files(){
		return this.test_apk_files;
	}

	public AndroidApp get_test_app() {
		return this.resigned_testapp;
	}
	/**
	 * 安装APP 
	 * adb install <file>
	 * @param app
	 * @return
	 */
	public String install(AndroidApp app) {
		if (device == null) {
			device = devices.get_default_device();
		}

		try {
			if (this.isInstalled(app.getBasePackage()))
				return null;
			String tmp = device.installPackage(app.getAbsolutePath(), true);
			log.debug("app installed: {} {}",app.getBasePackage(),tmp);
			return tmp;
		} catch (Exception e) {
			throw new RuntimeException("install fail:"+app.getAbsolutePath(),e);
		}
	}

	public String install(String packageFilePath) {
		return install(new DefaultAndroidApp(new File(packageFilePath)));
	}

	@Override
	public boolean is_ready() {
		return resigned_app != null;
	}
	
	/**
	 * 确认APP是否已经安装<br>
	 * adb shell pm list packages com.calculator
	 * @param apkPackage
	 * @return
	 * @throws AndroidSdkException
	 */
	public boolean isInstalled(String apkPackage) throws AndroidSdkException {
		String cmd = String.format("pm list packages %1$s", apkPackage);
		RegExpMatchReceiver receiver = new RegExpMatchReceiver("^package:"
				+ apkPackage);
		try {
			device.executeShellCommand(cmd, receiver,30000,TimeUnit.SECONDS);
			return receiver.matched();
		} catch (Exception e) {
			throw new RuntimeException("isInstalled fail:"+cmd,e);
		}		
	}

	/**
	 * 执行URL中指定的apk的monkey测试，输出测试结果到monkey.xxx.xml文件中<br>
	 * 默认为在所有设备上执行,如果需要指定特定设备，可以设置device或者devices参数
	 * 支持的参数包括:<br>
	 * loglevel(verbose, debug, info, warn, error, assert)<br>
	 * String类：sdkPlatform,Packages,device,devices,configurations等<br>
	 * boolean类: CreateReport(true)，MonitorNativeCrashes，KillProcessAfterError,IgnoreSecurityExceptions,
	 * IgnoreTimeouts,IgnoreCrashes,Hprof,DebugNoEvents,(未特殊指明时，默认值为false)<br>
	 * String数组：Categories，Packages(默认设置为BasePackage)<br>
	 * 整型：Throttle，PercentAnyevent,PercentAppswitch,PercentSyskeys,PercentMajorNav,
	 * PercentNav,PercentTrackball,PercentMotion,PercentTouch,Seed，EventCount<br>
	 * 
	 * 判断返回结果：<br>
	 * assertTrue(monkey(map).is_ok())<br>
	 * 
	 * @param map 需要设置的mojo的Field
	 * @return MONKEY_RESULT中包含3种异常情况，可参考代码
	 * @see com.jayway.maven.plugins.android.AbstractAndroidMojo#shouldDoWithThisDevice
	 * @see #process_paras_map
	 * @see com.android.ddmlib.testrunner.MonkeyMojo
	 */
	public MOJO_RESULT monkey(HashMap<String, Object> map) {
		try {
			if (map.get("Packages") == null)
				map.put("Packages",
						new String[] { resigned_app.getBasePackage() });

			MonkeyMojo mojo = new MonkeyMojo();
			current_mojo=mojo;
			
			//设置参数
			process_paras_map(mojo, map);

			//安装resign后的app
			install(this.resigned_app);

			// 执行monkey
			mojo.exerciseApp();			
			
			// 处理结果
			//注意，为了方便扩展测试结果日志，mojo中的日志listener是静态管理的，因此需要手动清除listener
			return process_test_result(mojo);
		} catch (Exception e) {			
			throw new RunFailureException("run monkey fail",e);
		}
	}
	/**
	 * 执行URL中指定的apk的monkey runner测试，输出测试结果到monkey.xxx.xml文件中<br>
	 * 默认为在所有设备上执行,如果需要指定特定设备，可以设置device或者devices参数
	 * 支持的参数包括:<br>
	 * loglevel(verbose, debug, info, warn, error, assert)<br>
	 * String类：RunName,sdkPlatform,device,devices,configurations等<br>
	 * boolean类: CreateReport(true)，IgnoreTestFailure,InjectDeviceSerialNumberIntoScript(未特殊指明时，默认值为false)<br>
	 * String数组：Plugins，Programs<br>
	 * 整型：EventCount(123456)<br>
	 * 
	 * Programs参数指明python脚本文件名的List，默认为apk/monkeyrunner下以py结尾的所有文件 ,
	 * 如果文件名不包括路径，则会自动添加apk/monkeyrunner/前缀
	 * 
	 * 
	 * 判断返回结果：<br>
	 * assertTrue(monkey(map).is_ok())<br> 
	 * @param map
	 * @return
	 */
	public MOJO_RESULT monkey_runner(HashMap<String, Object> map) {
		Object programs=map.get("Programs");
		List<String> pfiles=null;
		if(programs!=null){
			if(programs instanceof String[])
				pfiles=Arrays.asList((String[])programs);
			else if(programs instanceof List)
				pfiles=(List<String>)programs;
			else
				throw new RunFailureException("Programs should be string list");
			for(String tmp:pfiles){
				if(!pfiles.contains(File.separator))
					tmp="apk" + File.separator +"monkeyrunner"+File.separator+ tmp;
			}
		}else{
			pfiles=new ArrayList<String>();
			File f=new File("apk"+File.separator+"monkeyrunner");
			
			if(!f.exists())
				throw new RunFailureException("apk/monkeyrunner folder not exist");
			for(String tmp:f.list()){
				if(tmp.toLowerCase().endsWith("py"))
					pfiles.add(tmp);
			}
		}		
		
		map.put("Programs", pfiles);
		
		MonkeyRunnerMojo mojo=new MonkeyRunnerMojo();
		current_mojo=mojo;

		process_paras_map(mojo, map);
		
		install(this.resigned_app);
		
		try {
			mojo.execute();
			
			return process_test_result(mojo);			
		} catch (Exception e) {
			throw new RunFailureException("monkey runner fail",e);
		}		
	}
	/**
	 * 抓屏
	 * @see <a href="https://code.google.com/p/maven-android-plugin/wiki/AutomatedScreenshots">AutomatedScreenshots</a>
	 */
    public static void poseForScreenshot() {
    	Screenshots.poseForScreenshot();
    }
    

    /**
     * 抓屏
     * @param name
     * @see <a href="https://code.google.com/p/maven-android-plugin/wiki/AutomatedScreenshots">AutomatedScreenshots</a>
     */
    public static void poseForScreenshotNamed(String name) {
    	Screenshots.poseForScreenshotNamed(name);
    }
    
    /**
     * 打印device属性
     * @param device
     */    
	public void print_device_props(IDevice device) {
		Map<String, String> prop = device.getProperties();
		for (String tmp : prop.keySet())
			log.info("{}={}", tmp, prop.get(tmp));
	}
	/**
	 * 以反射的方式设置mojo中的参数值，先尝试给map中的每个key加上parser前缀以后作为mojo
	 * 的Field设置，如果设置失败则尝试直接以key作为Field Name进行设置
	 * @param mojo
	 * @param map
	 */
	private void process_paras_map(Object mojo, HashMap<String, Object> map) {
		ReflectUtils setter = ReflectUtils.getInstance();
		if(map.get("loglevel")!=null)
			DdmPreferences.setLogLevel(map.remove("loglevel").toString());
		
		for (String key : map.keySet()) {
			Object o = map.get(key);
			try {
				setter.setFieldValue(mojo, "parsed" + key, map.get(key));
				log.debug("set mojo's parsed {} to {}", key, o);
			} catch (FieldnameNotFoundException e) {
				setter.setFieldValue(mojo, key, o, true);
				log.debug("set mojo's {} to {}", key, o);
			}
		}

		if (map.get("sdkPlatform") != null) {
			this.sdkPlatform = map.get("sdkPlatform").toString();
			devices.set_sdk_platform(sdkPlatform);
		}
	}
	/**
	 * 测试结果分为4种:FAILED_ON_DEVICE,FAILED_TO_COMPLETE,FAILED_ON_EXCEPTION,OK
	 * @param mojo
	 * @return MOJO_RESULT,支持is_ok方法
	 */
	private MOJO_RESULT process_test_result(AbstractAndroidMojo mojo) {
		MOJO_RESULT rst = MOJO_RESULT.OK;
		for (ATestRunListener testRunListener : mojo.get_all_listener()) {
			if (testRunListener.hasFailuresOrErrors()) {
				log.error("Tests failed on device: {}", testRunListener.get_device()
						.getSerialNumber());
				rst = MOJO_RESULT.FAILED_ON_DEVICE;
			} else if (testRunListener.testRunFailed()) {
				log.error("Test run failed to complete on device: {}, {}", testRunListener
						.get_device().getSerialNumber(), testRunListener
						.getTestRunFailureCause());
				rst = MOJO_RESULT.FAILED_TO_COMPLETE;
			} else if (testRunListener.threwException()) {
				log.error("Test run failed on device {}, {}",
						testRunListener.getExceptionMessages());
				rst = MOJO_RESULT.FAILED_ON_EXCEPTION;
			} else
				continue;
			break;
		}
		// 清除所有日志listener
		//mojo.clear_listener();
		
		return rst;
	}

	private AndroidApp resignApp(File appfile) throws ShellCommandException,
			AndroidSdkException {
		AndroidApp app = selendroidApkBuilder.resignApp(appfile);
		log.debug("App " + app.getAppId() + " has been resignApp.");
		return app;
	}
	/**
	 * 执行URL中指定的Robotium apk及 Test apk测试，输出测试结果到robotium.xxx.xml文件中
	 * @param map 需要设置的mojo的Field
	 * @return
	 * @see #robotium(String, HashMap)
	 * @see #process_paras_map
	 */
	public MOJO_RESULT robotium(HashMap<String, Object> map) {
		return robotium(null,map);
	}
	
	/**
	 * 执行URL中指定的Robotium apk及 Test apk测试，输出测试结果到robotium.xxx.xml文件中
	 * 支持的参数包括:<br>
	 * loglevel(verbose, debug, info, warn, error, assert)<br>
	 * boolean: Debug,LogOnly,CreateReport(true)
	 * String：sdkPlatform,Categories，InstrumentationPackage(默认设置为BasePackage)，InstrumentationRunner，
	 * TestSize，CoverageFile(expression),device,devices,configurations等<br>
	 * String List：Classes，Packages，Annotations，ExcludeAnnotations，InstrumentationArgs，<br>
	 * Map<String, String>:InstrumentationArgs<br>
	 * <br>
	 * 
	 * @param testapk 测试apk,为null时使用扫描到的第一个apk
	 * @param map 初始化mojo时的参数
	 * @return
	 * @see InstrumentationMojo
	 */
	public MOJO_RESULT robotium(String testapk, HashMap<String, Object> map) {
		File testFile=null;
		try {
			if(test_apk_files.size()==0)
				throw new RunFailureException("Test APK doesn't exist");
			
			if(testapk==null)
				testFile=test_apk_files.get(0);
			else {
				if(!testapk.endsWith(".apk"))
					testapk+=".apk";			
				
				for(File tmp:test_apk_files){
					if(tmp.getName().equalsIgnoreCase(testapk)){
						testFile=tmp;
						break;
					}					
				}
			}
			
			if(testFile==null)
				throw new RunFailureException("Test APK doesn't exist: "+testapk);			
			
			AndroidApp testapp=new DefaultAndroidApp(new File(testFile.getParent()+File.separator+"resigned-"+testFile.getName()));
			
			if (map.get("InstrumentationPackage") == null)
				map.put("InstrumentationPackage",
						new String[] { testapp.getBasePackage() });
			
			this.install(this.resigned_app);
			
			this.install(testapp);
			
			InstrumentationMojo mojo = new InstrumentationMojo();
			current_mojo=mojo;
			
			process_paras_map(mojo, map);
			
			mojo.instrument();

			return process_test_result(mojo);
		} catch (Exception e) {
			throw new RunFailureException("run robotium fail",e);
		}
	}

	@Override
	public void startup() {
		selendroidApkBuilder = new SelendroidServerBuilder(config);

		// robotium不支持webview
		// config.setNoWebViewApp(true);
		try {
			File rfile = new File(apk_file.getParent(), "resigned-"
					+ apk_file.getName());
			if (rfile.exists()
					&& rfile.lastModified() > apk_file.lastModified()) {
				resigned_app = new DefaultAndroidApp(rfile);
				resigned_app.getAppId();
			} else
				resigned_app = resignApp(apk_file);
			
			for(File tmp:this.test_apk_files){
				File tmprfile = new File(apk_file.getParent(), "resigned-"
						+ tmp.getName());
				if (!tmprfile.exists()|| tmprfile.lastModified() < tmp.lastModified()) {
					resignApp(tmp);
				}
			}

			devices = new DevicesMojo();

		} catch (Exception e) {
			throw new RunFailureException("startup failure",e);
		}
	}
	
	@Override
	public void teardown() {
		after_method();
		Host.localhost().kill("adb");
	}

	public String uninstall(AndroidApp app) {
		if (device == null)
			device = devices.get_default_device();
		try {
			return device.uninstallPackage(app.getBasePackage());
		} catch (Exception e) {
			throw new RunFailureException("uninstall fail",e);
		}
	}

	public String uninstall(String filename) {
		return uninstall(new DefaultAndroidApp(new File(filename)));
	}

}
