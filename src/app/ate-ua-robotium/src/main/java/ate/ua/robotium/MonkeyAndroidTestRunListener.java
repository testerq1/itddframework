package ate.ua.robotium;

import java.io.IOException;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;

/**
 * AndroidTestRunListener produces a nice output for the log for the test run as
 * well as an xml file compatible with the junit xml report file format
 * understood by many tools.
 * <p/>
 * It will do so for each device/emulator the tests run on.
 */
public class MonkeyAndroidTestRunListener extends ATestRunListener {
	private static final String SCREENSHOT_SUFFIX = "_screenshot.png";

	/**
	 * Create a new test run listener.
	 * 
	 * @param project
	 *            the test project.
	 * @param device
	 *            the device on which test is executed.
	 */
	public MonkeyAndroidTestRunListener(IDevice device) {
		super(device);
	}

	private void executeOnAdbShell(String command) {
		try {
			device.executeShellCommand(command, new IShellOutputReceiver() {
				@Override
				public boolean isCancelled() {
					return false;
				}

				@Override
				public void flush() {
				}

				@Override
				public void addOutput(byte[] data, int offset, int length) {
				}
			});
		} catch (TimeoutException e) {
			log.error(e.getMessage());
		} catch (AdbCommandRejectedException e) {
			log.error(e.getMessage());
		} catch (ShellCommandUnresponsiveException e) {
			log.error(e.getMessage());
		} catch (IOException e) {
			log.error(e.getMessage());
		}
	}



	/**
	 * Write the junit report xml file.
	 */
	protected void writeJunitReportToFile() {
		writeJunitReportToFile("monkey");
	}
}