import java.util.HashMap;
import org.testng.annotations.*
import ate.testcase.UATestcase
import common.*

public class RobotiumTest extends UATestcase {
	
	@Test
	public void robotium(){
		log.info "$STEP.1 robotium demo"
		def ua=create_ua("robotium:AndroidCalculator.apk#test*");		
		ua.startup();
		
		log.info "$STEP.2 设置UA参数"
		def map=["sdkPlatform":"15"];
		//map.put("device", "");//指定设备串码 AbstractAndroidMojo.shouldDoWithThisDevice
		//map.put("devices", new String[]{});//指定设备组
		
		log.info "$STEP.3 执行robotium instrument test, 确认测试结果"
		assertTrue(ua.robotium(map).is_ok());		
	}
	
	@Test
	public void monkey(){
		log.info "$STEP.1 monkey demo"
		def ua=create_ua("robotium:AndroidCalculator.apk#test*");		
		ua.startup();		
		
		log.info "$STEP.2 设置UA参数"
		def map=["EventCount":1000,"Seed":123456,"sdkPlatform":"15"]
		//map.put("device", "");//指定设备串码 AbstractAndroidMojo.shouldDoWithThisDevice
		//map.put("devices", new String[]{});//指定设备组
		
		log.info "$STEP.3 执行monkey test, 确认测试结果"
		assertTrue(ua.monkey(map).is_ok());		
	}
	
	@Test
	public void basic(){
		def ua=create_ua("robotium:./apk/AndroidCalculator.apk#test*");
		assertTrue(ua.get_apk_file().exists());
		assertTrue(ua.get_all_test_apk().size()==1);
		assertTrue(ua.get_all_test_apk().get(0).getName().equals("testAndroidCalculatorBlackBox.apk"));
		
		ua=create_ua("robotium:AndroidCalculator.apk#test*",new RobotiumUA());
		assertTrue(ua.get_apk_file().exists());
		assertTrue(ua.get_all_test_apk().size()==1);
		assertTrue(ua.get_all_test_apk().get(0).getName().equals("testAndroidCalculatorBlackBox.apk"));
		
		ua=create_ua("robotium:AndroidCalculator.apk#./apk/test*",new RobotiumUA());
		assertTrue(ua.get_apk_file().exists());
		assertTrue(ua.get_all_test_apk().size()==1);
		assertTrue(ua.get_all_test_apk().get(0).getName().equals("testAndroidCalculatorBlackBox.apk"));
		
	}	
	
	@BeforeClass
	public void beforeclass() {				
		//start_all_ua();		
		super.BeforeClass();
	}

	@AfterClass
	public void afterclass() {
		//shutdown_all_ua();
		super.AfterClass();
	}
}
