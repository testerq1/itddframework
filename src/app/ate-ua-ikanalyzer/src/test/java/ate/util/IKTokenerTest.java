/**
 * 
 */
package ate.util;

import java.io.IOException;
import java.io.StringReader;

import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.testng.annotations.Test;
import org.wltea.analyzer.cfg.Configuration;
import org.wltea.analyzer.help.CharacterHelper;
import org.wltea.analyzer.lucene.IKTokenizer;

import ate.testcase.Testcase;


/**
 * @author 林良益
 *
 */
public class IKTokenerTest extends Testcase {
	@Test
	public void testCfgLoading(){
		System.out.println(Configuration.getExtDictionarys().size());
		System.out.println(Configuration.getExtStopWordDictionarys().size());
	}
	
	@Test
	public void testLucene3Tokenizer(){
		String t = "IK分词器Lucene Analyzer接口实现类 民生银行";
		IKTokenizer tokenizer = new IKTokenizer(new StringReader(t) , false);
		try {
			while(tokenizer.incrementToken()){
				CharTermAttribute termAtt = tokenizer.getAttribute(CharTermAttribute.class);
				System.out.println(termAtt);				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	@Test
	public void testSBC2DBCChar(){
        char a = '‘';
//      char a = 'Ｏ';
//      char a = '○';
        System.out.println((int)a);
        System.out.println(CharacterHelper.regularize(a));
        System.out.println((int)CharacterHelper.regularize(a));
        
        String sss  = "智灵通乳酸钙冲剂(5g\14袋)-1244466518522.txt";
        System.out.println(sss);
        System.out.println(sss.replaceAll("[\\\\]", "每"));
	}
	@Test
	public void testCharBlock(){
		
		Character.UnicodeBlock ub = Character.UnicodeBlock.of('년');
		System.out.println(ub.toString());
		
		ub = Character.UnicodeBlock.of('⑧');
		System.out.println(ub.toString());		
		ub = Character.UnicodeBlock.of('①');
		System.out.println(ub.toString());
		ub = Character.UnicodeBlock.of('⑴');
		System.out.println(ub.toString());
		ub = Character.UnicodeBlock.of('⒈');
		System.out.println(ub.toString());
		
	}

}
