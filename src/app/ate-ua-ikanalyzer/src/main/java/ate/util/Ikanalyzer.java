package ate.util;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import org.wltea.analyzer.lucene.IKTokenizer;

public class Ikanalyzer extends AbstractTool {
    public List<String> tokenizer(String t) {
        List<String> list=new ArrayList<String>();
        IKTokenizer tokenizer = new IKTokenizer(new StringReader(t), true);
        try {            
            while (tokenizer.incrementToken()) {
                TermAttribute termAtt = tokenizer
                        .getAttribute(TermAttribute.class);
                list.add(termAtt.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
