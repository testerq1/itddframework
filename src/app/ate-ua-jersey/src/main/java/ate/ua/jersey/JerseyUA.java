package ate.ua.jersey;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Configurable;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import ate.AbstractURIHandler;
import ate.ua.UAOption;

public class JerseyUA extends AbstractURIHandler{
	private URI BASE_URI;
	
	private ResourceConfig serverConfig;
	private HttpServer server;
	
	private ClientConfig clientConfig;
	private Client client;
	
	private boolean ssl=false;
	
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			if(port<=0)
				port=80;				
		}else if(option == UA_TYPE) {
			if(type==SERVER){
				serverConfig=new ResourceConfig();				
			}else{
				clientConfig=new ClientConfig();
			}
		}
		return this;
	}
	
	public Configurable config(){
		if(type==SERVER){
			return serverConfig;				
		}else{
			return clientConfig;
		}
	}
	
	public void set_ssl(boolean b){
		ssl=b;
	}
	
	@Override
	public void teardown() {
		if(type==SERVER){
			server.stop();				
		}else{
			client.close();
		}		
	}

	@Override
	public void startup() {
		if(ssl){
			BASE_URI = URI.create("https://"+host+":"+port+path);				
		}else{
			BASE_URI = URI.create("http://"+host+":"+port+path);
		}
		
		if(type==SERVER){			
			server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI,
					serverConfig);
		}else{
			client = ClientBuilder.newClient(clientConfig);
		}		
	}

	@Override
	public String get_default_schema() {
		return "jersey";
	}

	@Override
	public boolean is_ready() {
		if(type==SERVER){
			return server.isStarted();		
		}else{
			return client!=null;
		}
	}
	public WebTarget target() {
		return client.target(this.BASE_URI);		
	}
	public WebTarget target(String url) {
		return client.target(url);		
	}
	
	public HttpServer server() {
		return server;		
	}
}
