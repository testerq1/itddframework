package ate.ua.jersey;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;
import org.glassfish.jersey.message.internal.MediaTypes;
import org.glassfish.jersey.server.ResourceConfig;
import org.testng.annotations.*;

import ate.jersey.helloworld.HelloWorldResource;
import ate.jessey.jackson.CombinedAnnotationBean;
import ate.jessey.jackson.EmptyArrayBean;
import ate.jessey.jackson.MyApplication;
import ate.jessey.jackson.MyMoxyJSONProvider;
import ate.jessey.jackson.MyObjectMapperProvider;
import ate.jessey.jackson.NonJaxbBean;
import ate.testcase.CsUATestcase;

public class JerseyTest extends CsUATestcase {
	private static final URI BASE_URI = URI.create("http://localhost:8080/");
	public static final String ROOT_PATH = "helloworld";

	@BeforeClass
	public void BeforeClass(){
	    //init_jre_log("src/test/log.properties");
	}
	
	@Test
	public void test0() {
		/*The configuration explained:
handlers=java.util.logging.ConsoleHandler - log messages are written to System.err
java.util.logging.ConsoleHandler.level=FINEST - allow to log FINEST message;
default is INFO so more detailed messages are suppressed by default
java.util.logging.SimpleFormatter.format - example of simple and single line format - just level, logger name and message;
Note: It just works with Java 7.
org.glassfish.jersey.level=CONFIG - example of per-package level configuration;
log all SEVERE, WARNING, INFO and CONFIG level Jersey messages
org.glassfish.jersey.tracing.level=FINEST - example of detailed level configuration of specified package
		 * */		
		JerseyUA svr = this.create_server("http://localhost:8080/",
				new JerseyUA());
		JerseyUA clt = this.create_client("http://localhost:8080/",
				new JerseyUA());
		svr.config().register(HelloWorldResource.class);
		((ResourceConfig) svr.config()).packages("ate.jessey.jackson");
		
		//TracingConfig: ON_DEMAND,ALL,OFF
		((ResourceConfig) svr.config()).property("jersey.config.server.tracing", "ALL");
		
		//TracingLogger.Level:TRACE VERBOSE,SUMMARY
		((ResourceConfig) svr.config()).property("jersey.config.server.tracing.threshold",
		"VERBOSE");
		
		clt.config().register(MyMoxyJSONProvider.class);
		clt.config().register(new JacksonFeature())
				.register(MyObjectMapperProvider.class);
		this.start_all_ua();
		
		NonJaxbBean bean = clt.target().path("nonJaxbResource")
				.request("application/javascript")
				//TracingLogger.Level:TRACE VERBOSE,SUMMARY
				.header("X-Jersey-Tracing-Threshold", "TRACE")
				.get(NonJaxbBean.class);
		assertTrue(null != bean);
	}
	@Test
	public void test1() {
		ResourceConfig resourceConfig = new MyApplication();
		resourceConfig.register(HelloWorldResource.class);
		resourceConfig.packages("ate.jessey.jackson");
		resourceConfig.property("jersey.config.server.tracing", "ALL");// ON_DEMAND,ALL,OFF
																		// from
																		// TracingConfig
		resourceConfig.property("jersey.config.server.tracing.threshold",
				"VERBOSE");

		HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI,
				resourceConfig);

		ClientConfig config = new ClientConfig();

		config.register(MyMoxyJSONProvider.class);
		config.register(new JacksonFeature()).register(
				MyObjectMapperProvider.class);
		Client client = ClientBuilder.newClient(config);

		WebTarget target = client.target("http://localhost:8080/");

		Invocation.Builder req = target.path("nonJaxbResource")
				.request("application/javascript")
				.header("X-Jersey-Tracing-Threshold", "TRACE");// VERBOSE,SUMMARY
																// TracingLogger.Level
		NonJaxbBean bean3 = req.get(NonJaxbBean.class);
		assertTrue(null != bean3);

		server.stop();
	}

	public void test() {
		ResourceConfig resourceConfig = new MyApplication();
		resourceConfig.register(HelloWorldResource.class);
		HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI,
				resourceConfig);

		ClientConfig config = new ClientConfig();
		config.register(new JsonProcessingFeature()).register(
				MyObjectMapperProvider.class);
		config.register(new JacksonFeature()).register(
				MyObjectMapperProvider.class);

		Client client = ClientBuilder.newClient(config);

		WebTarget target = client.target("http://localhost:8080/helloworld");
		String jsonRes = target.request().accept(MediaType.TEXT_PLAIN)
				.get(String.class);
		assertTrue("Hello World!".equals(jsonRes));

		target = client.target("http://localhost:8080/");

		EmptyArrayBean bean = target.path("emptyArrayResource")
				.request(MediaType.APPLICATION_JSON).get(EmptyArrayBean.class);
		assertTrue(null != bean);
		CombinedAnnotationBean bean2 = target.path("combinedAnnotations")
				.request("application/json").get(CombinedAnnotationBean.class);
		assertTrue(null != bean2);

		String responseMsg = target.path("emptyArrayResource")
				.request(MediaType.APPLICATION_JSON).get(String.class);
		assertTrue(responseMsg.replaceAll("[ \t]*", "").contains("[]"));

		responseMsg = target.path("nonJaxbResource")
				.request("application/javascript").get(String.class);
		assertTrue(responseMsg.startsWith("callback("));

		responseMsg = target.path("nonJaxbResource")
				.request("application/json").get(String.class);
		assertTrue(!responseMsg.contains("jsonSource"));

		responseMsg = target.path("combinedAnnotations")
				.request("application/json").get(String.class);
		assertTrue(responseMsg.contains("account")
				&& responseMsg.contains("value"));

		String serviceWadl = target.path("application.wadl")
				.request(MediaTypes.WADL).get(String.class);
		assertTrue(serviceWadl.length() > 0);

		Response response = target
				.path("parseExceptionTest")
				.request("application/json")
				.put(Entity.entity("Malformed json string.",
						MediaType.valueOf("application/json")));

		assertTrue(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode() == response
				.getStatus());

		server.stop();
	}

}
