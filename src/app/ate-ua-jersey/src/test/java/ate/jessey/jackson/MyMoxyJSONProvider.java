package ate.jessey.jackson;

import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import org.eclipse.persistence.jaxb.rs.MOXyJsonProvider;

@Provider
@Consumes("application/javascript")
public class MyMoxyJSONProvider extends MOXyJsonProvider {
    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return mediaType.equals(MediaType.valueOf("application/javascript"));
    }
}
