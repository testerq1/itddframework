package ate.ua.vlc;

import ate.AbstractURIHandler;
import ate.ua.UAOption;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;

/**
 * resources中需要vlc的 plugins文件目录
 * vlc://127.0.0.1:5555/rtp
 */
public class VlcUA extends AbstractURIHandler{
	protected static final String[] DEFAULT_FACTORY_ARGUMENTS = {
		"--video-title=vlcj video output", "--no-snapshot-preview", "--quiet",
		"--quiet-synchro", "--sub-filter=logo:marq", "--intf=dummy" };
	String protocol="rtp";
	String options;
	String options2;
	MediaPlayer mediaPlayer;
	boolean stop=false;
	VlcListener listener=new VlcListener();
	
	@Override
	public AbstractURIHandler build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			if(get_fragment()!=null)
				protocol=this.get_fragment();
		}
		return this;
	}
	
	public boolean isOk(){
		return this.listener.avaliable();
	}
	
	public MediaPlayer get_player(){
		return this.mediaPlayer;
	}
	
	public MediaPlayer stream(String mrl) {
		System.out.println("Streaming '" + mrl + "' to '" + options + "'");	
		if("rtp".equalsIgnoreCase(protocol)||"rtsp".equalsIgnoreCase(protocol))
			mediaPlayer.playMedia(mrl, options, 
					":no-sout-rtp-sap", 
					":no-sout-standard-sap",
					":sout-all", 
					":sout-keep");
		else
			mediaPlayer.playMedia(mrl, options);	
		
		return mediaPlayer;
	}
	
	public MediaPlayer play() {		
		return play(protocol+"://"+host+":"+port+path);				
	}
	
	public MediaPlayer play(String mrl) {
		log.debug("play:"+mrl);
		if(type==SERVER)
			throw new Error("server can't play");
		//String options = ":sout=#transcode{vcodec=h264,venc=x264{cfr=16},scale=1,acodec=mp4a,ab=160,channels=2,samplerate=44100}";

		mediaPlayer.playMedia(mrl);
		return mediaPlayer;		
	}
	
	public void wait_to_stop() {
		try {
			Thread.currentThread().sleep(3000);
			while (!stop||mediaPlayer.isPlaying())
				Thread.currentThread().sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void teardown() {
		stop=true;
		mediaPlayer.stop();
		listener.clear();
	}
	
	@Override
	public void startup() {		
		stop=false;
		if(type==SERVER){
			StringBuilder sb = new StringBuilder(60);
			if("rtp".equals(protocol)){
				sb.append(":sout=#rtp{dst=");
				//sb.append("230.0.0.1");
				sb.append(host);
				sb.append(",port=");
				sb.append(port);
				sb.append(",mux=ts}");		
				
			}else if("rtsp".equals(protocol)){
				sb.append(":sout=#rtp{sdp=rtsp://@");
				//sb.append("127.0.0.1");
				sb.append(host);
				sb.append(':');
				sb.append(port);
				sb.append(path);
				sb.append("}");
			}else if("http".equals(protocol)){
				sb.append(":sout=#duplicate{dst=std{access=http,mux=ts,");
		        sb.append("dst=");
		        //sb.append("127.0.0.1");
		        sb.append(host);
		        sb.append(':');
		        sb.append(port);
		        sb.append("}}");
			}
			
			options=sb.toString();
		}else{
			
		}

		MediaPlayerFactory mediaPlayerFactory = new MediaPlayerFactory();
		mediaPlayer = mediaPlayerFactory.newHeadlessMediaPlayer();
		mediaPlayer.addMediaPlayerEventListener(listener);
	}
	@Override
	public String get_default_schema() {
		return "vlc";
	}
	@Override
	public boolean is_ready() {
		return mediaPlayer!=null;
	}
}
