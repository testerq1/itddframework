package ate.ua.vlc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.caprica.vlcj.binding.internal.libvlc_media_t;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventListener;

public class VlcListener implements MediaPlayerEventListener {
	protected static Logger log = LoggerFactory.getLogger(VlcListener.class);
	int buffering=0;
	int error=0;
	
	public boolean avaliable(){
		log.debug("buffering={} error={}",buffering,error);
		return buffering>1&&error==0;
	}
	
	public void clear(){
		buffering=0;
		error=0;
	}
	@Override
	public void stopped(MediaPlayer mediaPlayer) {
		log.debug("in");
	}

	@Override
	public void opening(MediaPlayer mediaPlayer) {
		log.debug("in");
	}

	@Override
	public void buffering(MediaPlayer mediaPlayer, float newCache) {
		buffering++;
	}

	@Override
	public void playing(MediaPlayer mediaPlayer) {
		log.debug("in");
	}

	@Override
	public void finished(MediaPlayer mediaPlayer) {
		log.debug("in");
	}

	@Override
	public void paused(MediaPlayer mediaPlayer) {
		log.debug("in");
		
	}

	@Override
	public void forward(MediaPlayer mediaPlayer) {
		log.debug("in");
		
	}

	@Override
	public void backward(MediaPlayer mediaPlayer) {
		log.debug("in");
		
	}

	@Override
	public void seekableChanged(MediaPlayer mediaPlayer, int newSeekable) {
		log.debug("in");
		
	}

	@Override
	public void pausableChanged(MediaPlayer mediaPlayer, int newPausable) {
		log.debug("in");
		
	}

	@Override
	public void titleChanged(MediaPlayer mediaPlayer, int newTitle) {
		log.debug("in");
		
	}

	@Override
	public void snapshotTaken(MediaPlayer mediaPlayer, String filename) {
		log.debug("in");
		
	}

	@Override
	public void videoOutput(MediaPlayer mediaPlayer, int newCount) {
		log.debug("in");
		
	}

	@Override
	public void error(MediaPlayer mediaPlayer) {
		error++;		
	}

	@Override
	public void mediaSubItemAdded(MediaPlayer mediaPlayer, libvlc_media_t subItem) {
		log.debug("in");
		
	}

	@Override
	public void mediaParsedChanged(MediaPlayer mediaPlayer, int newStatus) {
		log.debug("in");
		
	}

	@Override
	public void mediaFreed(MediaPlayer mediaPlayer) {
		log.debug("in");
		
	}

	@Override
	public void mediaStateChanged(MediaPlayer mediaPlayer, int newState) {
		log.debug("in");
		
	}

	@Override
	public void newMedia(MediaPlayer mediaPlayer) {
		log.debug("in");
		
	}

	@Override
	public void subItemPlayed(MediaPlayer mediaPlayer, int subItemIndex) {
		log.debug("in");
		
	}

	@Override
	public void subItemFinished(MediaPlayer mediaPlayer, int subItemIndex) {
		log.debug("in");
		
	}

	@Override
	public void endOfSubItems(MediaPlayer mediaPlayer) {
		log.debug("in");
		
	}

	@Override
	public void timeChanged(MediaPlayer mediaPlayer, long newTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void positionChanged(MediaPlayer mediaPlayer, float newPosition) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void lengthChanged(MediaPlayer mediaPlayer, long newLength) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mediaDurationChanged(MediaPlayer mediaPlayer, long newDuration) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void mediaChanged(MediaPlayer mediaPlayer, libvlc_media_t media, String mrl) {
		//log.debug("in");		
	}
	@Override
	public void mediaMetaChanged(MediaPlayer mediaPlayer, int metaType) {
		//log.debug("in");
		
	}
}
