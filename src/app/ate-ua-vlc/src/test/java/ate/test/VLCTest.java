package ate.test;

import java.lang.reflect.Method;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.testng.annotations.*;

import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.player.*;

import ate.testcase.*;
import ate.ua.vlc.VlcUA;

public class VLCTest extends CsUATestcase {
	URL url=getClass().getResource("/test.mp3");
	String mrl =url.getPath().substring(1).replaceAll("/", "\\\\");
	
	@Test
	// An example of how to stream a media file using RTSP.
	public void streamrtsp() throws Exception {
		VlcUA uas=(VlcUA)create_server("vlc://"+sip+":5577/demo#rtsp",new VlcUA());
		VlcUA uac=(VlcUA)create_client("vlc://"+sip+":5577/demo#rtsp",new VlcUA());
		this.start_all_ua();
					
		uas.stream(mrl);
		//uas.wait_to_stop();
						
		uac.play();
		
		sleep(10000);
		assertTrue(uac.isOk());
	}
	@Test
	public void streamHttp(){
		VlcUA uas=(VlcUA)create_server("vlc://"+sip+":5566/#http",new VlcUA());
		VlcUA uac=(VlcUA)create_client("vlc://"+sip+":5566/#http",new VlcUA());
		this.start_all_ua();
					
		uas.stream(mrl);
		//uas.wait_to_stop();
						
		uac.play();
		
		sleep(10000);
		assertTrue(uac.isOk());		
	}
	@Test
	// An example of how to stream a media file using RTP.
	public void streamRtp() {
		VlcUA uas=(VlcUA)create_server("vlc://"+sip+":5555/#rtp",new VlcUA());
		VlcUA uac=(VlcUA)create_client("vlc://"+sip+":5555/#rtp",new VlcUA());
		this.start_all_ua();
					
		uas.stream(mrl);
		//uas.wait_to_stop();
						
		uac.play();
		
		sleep(10000);
		assertTrue(uac.isOk());	
		
	}
	
	// An example of how to play a audio file
	public void test2() {
		VlcUA ua=new VlcUA();
		ua.build(VlcUA.UA_URI, "vlc:vlc")
		  .build(VlcUA.UA_TYPE,VlcUA.CLIENT);
		//"rtp://@230.0.0.1:5555"
		//player.playMedia(mrl);
		ua.startup();
		
		ua.get_player().addMediaPlayerEventListener(new MediaPlayerEventAdapter() {
			
		});
		ua.play(mrl);
		sleep(10000);
		assertTrue(ua.isOk());
	}

	//
	public void test() {
		// LibVlc libVlc = LibVlcFactory.factory().create();
		// log.info("version: {}", libVlc.libvlc_get_version());
		// log.info("compiler: {}", libVlc.libvlc_get_compiler());
		// log.info("changeset: {}", libVlc.libvlc_get_changeset());

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Example1().start(mrl);
			}
		});
		try {
			Thread.currentThread().join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	class Example1 {

		private final JFrame frame;

		private final EmbeddedMediaPlayerComponent mediaPlayerComponent;

		public Example1() {
			mediaPlayerComponent = new EmbeddedMediaPlayerComponent();

			frame = new JFrame("vlcj quickstart");
			frame.setLocation(50, 50);
			frame.setSize(1400, 800);
			frame.setContentPane(mediaPlayerComponent);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setVisible(true);
		}

		private void start(String mrl) {
			mediaPlayerComponent.getMediaPlayer().playMedia(mrl);
		}
	}
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
