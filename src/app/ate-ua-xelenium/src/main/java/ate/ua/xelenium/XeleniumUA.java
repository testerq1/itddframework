package ate.ua.xelenium;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.util.*;

import org.apache.commons.lang.StringEscapeUtils;

import ate.AbstractURIHandler;

public class XeleniumUA extends AbstractURIHandler {
	HashMap<String, String> map = new HashMap<String, String>();
	ByteArrayOutputStream myOut = new ByteArrayOutputStream();
	String stdOput;
	String logincreds = "";

	List<Map<String, String>> predef_urls = new ArrayList<Map<String, String>>();

	private List<String> _urls_scan = new ArrayList<String>();
	private List<String> _attack_vec = new ArrayList<String>();

	public void add_url_scan(String url) {
		_urls_scan.add(url);
	}

	public void add_attack_vector(String avc) {
		_attack_vec.add(avc);
	}

	public void add_predef_urls(Map<String, String> v) {
		predef_urls.add(v);
	}

	@Override
	public void teardown() {
		WebScan.quit = true;
	}

	private boolean xss_scan() {
		BufferedWriter bw = null;
		float pass_cnt = 0;
		float fail_cnt = 0;
		float warn_cnt = 0;

		if (null != username)
			logincreds = username + "<>" + passwd;

		try {
			bw = new BufferedWriter(new FileWriter(new File("logs"
					+ File.separator + "xss_scan.htm")));

			for (int j = 0; j < _urls_scan.size(); j++) {
				Thread Thrd[] = new Thread[_urls_scan.size()];

				for (int i = 0; i < _urls_scan.size(); i++) {
					Thrd[i] = new WebScan(j + _urls_scan.get(i), i
							+ _attack_vec.get(i), this);
					Thrd[i].start();
				}

				for (int t = 0; t < _attack_vec.size(); t++) {
					try {
						Thrd[t].join();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}

				TreeSet<String> keys = new TreeSet<String>(map.keySet());
				String txt_key = "";

				String res;
				String html_update = "";
				// String trow_data1 = "<TR COLS=10>";
				// String trow_data2 = "<TR COLS=10>";
				for (String key : keys) {
					if (!(txt_key.endsWith(key.substring(3)))) {
						if (!(txt_key.equalsIgnoreCase(""))) {
							bw.write("<h4> Test Summary (No of tests executed :"
									+ (int) (pass_cnt + fail_cnt + warn_cnt)
									+ ")</h4>");
							bw.write("<table style='width: 100%'><tbody><tr><td nowrap='nowrap'>Failures:</td><td class='bar'><div style='width:"
									+ ((fail_cnt / (pass_cnt + fail_cnt + warn_cnt)) * 100)
									+ "%;'class='fail'>"
									+ (int) fail_cnt
									+ "</div></td></tr>");
							// bw.write("<tr><td nowrap='nowrap'>Warnings:</td><td class='bar'><div style='width:"
							// + ((warn_cnt/(pass_cnt+fail_cnt+warn_cnt))*100) +
							// "%;'class='warn'>"+ (int)warn_cnt
							// +"</div></td></tr>");
							bw.write("<tr><td nowrap='nowrap'>Passes:</td><td class='bar'><div style='width:"
									+ ((pass_cnt / (pass_cnt + fail_cnt + warn_cnt)) * 100)
									+ "%;'class='pass'>"
									+ (int) pass_cnt
									+ "</div></td></tr></tbody></table>");
							if (!(html_update.equalsIgnoreCase(""))) {
								bw.write("<h4>XSS String Test Results</h4>");
								bw.write("<div class='results'><b>Results:</b><br />");
								bw.write(html_update);
								bw.write("</div>");
							}

							bw.write("</div>");
							// bw.write("</div>");
							pass_cnt = 0;
							fail_cnt = 0;
							warn_cnt = 0;
							html_update = "";
						}
						txt_key = key;
						String key_ext = key.substring(3);
						String[] arrkeys = key_ext.split("<>");
						bw.write("<div class='scan'>Text Field : " + arrkeys[0]
								+ ", Button : " + arrkeys[1]);
					}
					res = map.get(key);

					if (res.equalsIgnoreCase("0")) {
						pass_cnt = pass_cnt + 1;
					} else if (res.equalsIgnoreCase("1")) {
						fail_cnt = fail_cnt + 1;
						String avctr_ind = key.substring(2, 3);
						String atk_vctr = _attack_vec.get(Integer
								.parseInt(avctr_ind));
						String escapedVctr = StringEscapeUtils
								.escapeHtml(atk_vctr);
						html_update = html_update
								+ "<div class='fail'>Field appears to be vulnerable to the below mentioned XSS String.<br />Tested value: "
								+ escapedVctr + "</div>";
					} else if (res.equalsIgnoreCase("-1")) {
						warn_cnt = warn_cnt + 1;
					}
				}

				bw.write("<h4> Test Summary (No of tests executed :"
						+ (int) (pass_cnt + fail_cnt + warn_cnt) + ")</h4>");
				bw.write("<table style='width: 100%'><tbody><tr><td nowrap='nowrap'>Failures:</td><td class='bar'><div style='width:"
						+ ((fail_cnt / (pass_cnt + fail_cnt + warn_cnt)) * 100)
						+ "%;'class='fail'>"
						+ (int) fail_cnt
						+ "</div></td></tr>");
				// bw.write("<tr><td nowrap='nowrap'>Warnings:</td><td class='bar'><div style='width:"
				// + ((warn_cnt/(pass_cnt+fail_cnt+warn_cnt))*100) +
				// "%;'class='warn'>"+ (int) warn_cnt +"</div></td></tr>");
				bw.write("<tr><td nowrap='nowrap'>Passes:</td><td class='bar'><div style='width:"
						+ ((pass_cnt / (pass_cnt + fail_cnt + warn_cnt)) * 100)
						+ "%;'class='pass'>"
						+ (int) pass_cnt
						+ "</div></td></tr></tbody></table>");
				if (!(html_update.equalsIgnoreCase(""))) {
					bw.write("<h4>XSS String Test Results</h4>");
					bw.write("<div class='results'><b>Results:</b><br />");
					bw.write(html_update);
				}
				bw.write("</div>");
				bw.write("</div>");
				bw.write("</div>");
				this.map = new HashMap<String, String>();
				// bw.write("</div>");
				myOut = new ByteArrayOutputStream();
				this.stdOput = "";
			}

			bw.write("</body>");
			bw.write("</html>");
			bw.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return pass_cnt > 0 && fail_cnt <= 0;
	}

	@Override
	public void startup() {
		
	}

	@Override
	public String get_default_schema() {
		return "xelenium";
	}

	@Override
	public boolean is_ready() {
		return _urls_scan.size()>0;
	}

}
