package common;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.*
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.remote.*
import com.thoughtworks.selenium.Selenium;
import ate.util.*

/**
 * 封装一些Web操作，将来可以考虑区别不同设备
 * @author huangxy
 *
 */
public class Web extends WebDriverBackedSelenium{
	protected static WebDriver driver//=new FirefoxDriver();
	protected String baseUrl;
	
	/**
	 * 
	 * @param driver
	 * @param baseUrl
	 */
	public Web(WebDriver driver,String baseUrl){
		super(driver, baseUrl)		
		this.driver=driver	
	}
	/**
	 * 关闭连接
	 */
	public void disconnect(){
		close()
	}
	/**
	 * 创建一个driver,类型包括ie,httpunit,rm,firefox,缺省为firefox
	 * @param type
	 * @return
	 */
	def static create_driver(type){
		WebDriver driver =null;
		switch ( type ) {
			case "ie":
				
				DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
				ieCapabilities.setJavascriptEnabled(true);
				ieCapabilities.setCapability("nativeEvents", false);
				
//				ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
//				ieCapabilities.setJavascriptEnabled(true);				
				return new InternetExplorerDriver(ieCapabilities);
			case "httpunit":
				return new HtmlUnitDriver()				
			case "rm":
				// Any driver could be used for test
				DesiredCapabilities capabilities = new DesiredCapabilities();
				// Enable javascript support
				capabilities.setJavascriptEnabled(true);		
				// Get driver handle
				return new RemoteWebDriver(capabilities);			
			case "firefox":				
			default:
				return new FirefoxDriver()
		}
	}
	
	/**
	 * 添加流量控制表参数
	 * @param ip_j11
	 * @param ip_j21
	 */
	public void add_flow_security(ip_j11,ip_j21){
		selectFrame("leftframe");
		
		click("link=流量安全");
		selectFrame("relative=top");
		selectFrame("cnt");
		
		click("name=Submit3222");
		waitForPageToLoad("3000000");
		click("id=a1");
		select("name=action_type", "label=DENY");
		select("name=service_select", "label=ICMP_ANY");
		type("id=j11", "192.168.1.1");
		type("id=j21", "172.16.5.111");
		click("id=setting");
		waitForPageToLoad("30000");			
	}
	/**
	 * 取得某张表制定行和列的值，拼接成字符串后返回值
	 * 
	 * @param name  表名
	 * @param rows  行s
	 * @param cols  列s
	 * @param hide  末尾的隐藏行
	 * @return String
	 */
	def walk_table(name,rows,cols,hide){
		selectFrame("relative=top");
		def table = "//table[@id='$name']";
		def format="/tbody/tr[%s]/td[%s]"
		selectFrame("cnt");
		int row_total=getXpathCount(table+"/tbody/tr").intValue()-hide
		int col_total=getXpathCount(table+"/tbody/tr[1]/td").intValue()
		
		def al=new ArrayList()
		loop:for(int x=0;x<rows.size();x++){
			if(rows[x]>row_total)
				break loop;
			for(int y=0;y<cols.size();y++){
				def xpath=table+String.format(format, rows[x]+"",cols[y]+"")
				al.add(getText(xpath))		
				if(cols[y]>col_total)
					break loop;
			}
		}
		return X2X.al2Str(al)		
	}
	/**
	 * 选择树上的节点
	 * @param al
	 * @return
	 */
	def select_tree_node(al){
		selectFrame("relative=top");
		selectFrame("leftframe");
		al.each {
			click("link=$it");
		}		
		
//		WEB.selectFrame("cnt");
//		WEB.click("css=input.link");
		waitForPageToLoad("30000");
	}
	
	/**
	 * 点击删除
	 */
	public void del_flow_security(){
		chooseOkOnNextConfirmation();
		this.click("link=删除");
		this.waitForPageToLoad("30000")
	}
	/**
	 * login
	 */
	public void login(){
		open("/logo.html");
		if(driver instanceof InternetExplorerDriver)
			driver.navigate().to("javascript:document.getElementById('overridelink').click()");
		type("id=username2", "admin");
		type("id=password2", "NTc@2010");
		click("name=goto");
		waitForPageToLoad("30000");	
	}
}
