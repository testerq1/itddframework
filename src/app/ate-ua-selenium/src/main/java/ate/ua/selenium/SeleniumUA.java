package ate.ua.selenium;

/*
 * #%L
 * iTDD UA Selenium
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.File;

import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.WebDriver;

import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.firefox.FirefoxDriver; 
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.opera.core.systems.OperaDriver;

import ate.AbstractURIHandler;
import ate.rm.dev.Host;
import ate.ua.UAOption;

/**
 * Open IE<br>
 * Go to Tools -> Internet Options -> Security<br>
 * Set all zones to the same protected mode, enabled or disabled should not matter.<br>
 * 
 * Got to tools ->Zoom<br>
 * Set Browser zoom level to 100%<br>
 * 
 * @author ravih
 *
 */
public class SeleniumUA extends AbstractURIHandler{
	WebDriverBackedSelenium selenium;
	WebDriver driver;
	String baseuri="";
	DesiredCapabilities capabilities;
	String browser="ie";
	String ieDriverName="IEDriverServer.exe";
	
	@Override
	public void teardown() {
		selenium.close();
		if(capabilities.getBrowserName().equalsIgnoreCase("internet explorer"))
			Host.localhost().kill(ieDriverName);	
	}

	public DesiredCapabilities get_capabilities(){
		return this.capabilities;
	}	
	
	public WebDriverBackedSelenium get_selenium(){
		return this.selenium;
	}
	
	@Override
	public void startup() {
		if(get_query_para("unit")!=null||browser.equalsIgnoreCase("htmlUnit")){			
			driver=new HtmlUnitDriver(this.capabilities);			
		}if(browser.equalsIgnoreCase("chrome"))	
			driver=new ChromeDriver(this.capabilities);
		else if(browser.equalsIgnoreCase("firefox"))	
			driver=new FirefoxDriver(this.capabilities);
		else if(browser.equalsIgnoreCase("opera"))	
			driver=new OperaDriver(this.capabilities);
//		else if(browser.equalsIgnoreCase("phantomjs"))	
//			driver=new PhantomJSDriver(this.capabilities);
		else if(browser.equalsIgnoreCase("safari"))	
			driver=new SafariDriver(this.capabilities);
		else {
			try{
				driver=new InternetExplorerDriver(this.capabilities);
			}catch(Exception e){
				e.printStackTrace();
				log.error("Open IE\n"+
					 " Go to Tools -> Internet Options -> Security\n"+
					 " Set all zones to the same protected mode, enabled or disabled should not matter\n"+
					 " Got to tools ->Zoom\n"+
					 " Set Browser zoom level to 100%");
			}
		}
		
		selenium=new WebDriverBackedSelenium(driver,baseuri);		
	}
	
	/**
	 * build(UA_URI,"selenium://www.google.com/?browser=ie#https")
	 * build(UA_URI,"selenium://www.google.com/?browser=firefox")
	 */
	@Override
	public SeleniumUA build(UAOption option, Object value) {
		super.build(option, value);

		if (option == UA_URI) {
			browser=get_query_para(CapabilityType.BROWSER_NAME);
			if(browser==null)
				browser="ie";
			
			if(browser.equalsIgnoreCase("android"))	
				capabilities = DesiredCapabilities.android();
			else if(browser.equalsIgnoreCase("chrome"))	
				capabilities = DesiredCapabilities.chrome();
			else if(browser.equalsIgnoreCase("firefox"))	
				capabilities = DesiredCapabilities.firefox();
			else if(browser.equalsIgnoreCase("htmlUnit"))	
				capabilities = DesiredCapabilities.htmlUnit();
			else if(browser.equalsIgnoreCase("ipad"))	
				capabilities = DesiredCapabilities.ipad();
			else if(browser.equalsIgnoreCase("iphone"))	
				capabilities = DesiredCapabilities.iphone();
			else if(browser.equalsIgnoreCase("opera"))	
				capabilities = DesiredCapabilities.opera();
			else if(browser.equalsIgnoreCase("phantomjs"))	
				capabilities = DesiredCapabilities.phantomjs();
			else if(browser.equalsIgnoreCase("safari"))	
				capabilities = DesiredCapabilities.safari();
			else{
				if(null==System.getProperty("webdriver.ie.driver"))
					System.setProperty("webdriver.ie.driver", Host.NATIVE_HOME+File.separator+ieDriverName);
				capabilities = DesiredCapabilities.internetExplorer();
			}
			
			if(get_query_para(CapabilityType.VERSION)!=null)
				capabilities.setVersion(get_query_para(CapabilityType.VERSION));
			
			if(get_query_para("js")!=null)
				capabilities.setJavascriptEnabled(true);
			
			String sch=this.get_fragment();
			if(sch!=null)
				baseuri=sch;
			else 
				baseuri="http";
			
			baseuri+="://"+get_authority()+this.get_path();
			
			type = UA;
		}

		return this;
	}
	
	@Override
	public String get_default_schema() {		
		return "selenium";
	}

	@Override
	public boolean is_ready() {		
		return driver!=null&&selenium!=null;
	}	
	
	public void set_proxy(String addr,String uname,String passwd){
		org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();
		proxy.setProxyType(ProxyType.MANUAL).setHttpProxy(addr)
			 .setSslProxy(addr).setFtpProxy(addr);
		if(uname!=null)
			proxy.setSocksUsername(uname).setSocksPassword(passwd);
		this.capabilities.setCapability(CapabilityType.PROXY, proxy);
	}

}
