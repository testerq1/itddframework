package ate.test;

/*
 * #%L
 * iTDD UA Selenium
 * %%
 * Copyright (C) 2012 - 2013 Ravi Huang
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.Method;

import org.testng.annotations.*;

import ate.testcase.UATestcase;
import ate.ua.selenium.SeleniumUA;

public class SeleniumUATest extends UATestcase{
	@Test
	public void base() {
		STEP("init:");
		SeleniumUA svr = create_ua("selenium://www.baidu.com/?browserName=ie&js=jj&unit=uu");		
		start_all_ua();		
	}
	
	@BeforeMethod
	public void BeforeMethod(Method m){
		super.BeforeMethod(m);
	}
	
	@AfterMethod
	public void AfterMethod(Method m){
		super.AfterMethod(m);
	}
	
	@BeforeClass
	public void BeforeClass(){
		super.BeforeClass();
	}
	
	@AfterClass
	public void AfterClass(){
		this.shutdown_all_ua();
		super.AfterClass();
	}
}
