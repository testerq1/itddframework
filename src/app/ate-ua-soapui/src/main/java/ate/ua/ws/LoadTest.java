/*
 * Copyright 2004-2014 SmartBear Software
 *
 * Licensed under the EUPL, Version 1.1 or - as soon as they will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the Licence for the specific language governing permissions and limitations
 * under the Licence.
*/

package ate.ua.ws;

import ate.AbstractURIHandler;

import com.eviware.soapui.SoapUI;
import com.eviware.soapui.impl.wsdl.loadtest.WsdlLoadTest;
import com.eviware.soapui.impl.wsdl.loadtest.data.actions.ExportStatisticsAction;
import com.eviware.soapui.model.testsuite.LoadTestRunContext;
import com.eviware.soapui.model.testsuite.LoadTestRunner;
import com.eviware.soapui.model.testsuite.TestCaseRunContext;
import com.eviware.soapui.model.testsuite.TestCaseRunner;
import com.eviware.soapui.model.testsuite.TestStepResult;
import com.eviware.soapui.tools.SoapUILoadTestRunner;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.Properties;

/**
 * Runs SoapUI loadtests
 * 
 */

public class LoadTest extends SoapUILoadTestRunner
{
	protected static Logger log = LoggerFactory.getLogger(LoadTest.class);
	
	int test_failure=0;
	
	@Override
	public void afterTestStep( LoadTestRunner loadTestRunner, LoadTestRunContext context, TestCaseRunner testRunner,
			TestCaseRunContext runContext, TestStepResult testStepResult )
	{
		super.afterStep( testRunner, runContext, testStepResult );
		if(!testStepResult.getStatus().equals(TestStepResult.TestStepStatus.OK)){
			printTestStepResult(testStepResult.getMessages());
		}
	}
	
	private void printTestStepResult(String[] msgs){
		for(String msg:msgs)
			log.error(msg);
	}
	
	@Override
	public void afterRun( TestCaseRunner testRunner, TestCaseRunContext runContext )
	{
		List<TestStepResult> rst=testRunner.getResults();
		for(TestStepResult tmp:rst){
			if(!tmp.getStatus().equals(TestStepResult.TestStepStatus.OK)){
				test_failure++;
				for(String msg:tmp.getMessages())
					log.error(msg);
			}
		}
	}
//	public void loadTestStopped( LoadTestRunner loadTestRunner, LoadTestRunContext context )
//	{
//		WsdlLoadTest loadTest=
//		ExportStatisticsAction exportStatisticsAction = new ExportStatisticsAction( loadTestRunner.getLoadTest().getStatisticsModel() );
//		
//	}
	
	public boolean is_ok(){
		boolean b=test_failure>0;
		test_failure=0;		
		return b;
	}

}
