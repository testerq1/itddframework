package ate.ua.ws;

import java.io.File;
import java.util.HashMap;

import org.azeckoski.reflectutils.ReflectUtils;
import org.azeckoski.reflectutils.exceptions.FieldnameNotFoundException;

import ate.AbstractURIHandler;
import ate.ua.RunFailureException;
import ate.ua.UAOption;

import com.eviware.soapui.tools.AbstractSoapUIRunner;
import com.eviware.soapui.tools.SoapUIMockServiceRunner;
import com.eviware.soapui.tools.SoapUISecurityTestRunner;
import com.eviware.soapui.tools.SoapUITestCaseRunner;
import com.eviware.soapui.tools.SoapUIToolRunner;

/**
 * URI:<br>
 * soapui:./project.xml#test<br>
 * soapui:./project.xml#mockservice<br>
 * soapui:project.xml#loadtest<br>
 * soapui:./project.xml#tool<br>
 * soapui:./project.xml#security<br>
 * 
 * 通用可选参数：
 * soapuiProperties：map<String,String>
 * 
 * 
 * <p>
 * mock_service可选参数：<br>
 * String类： mockService,path,port,settingsFile,projectPassword,soapUISettingsPassword<br>
 * String[]： globalProperties,projectProperties<br>
 * boolean：block,saveAfterRun<br>
 * <p>
 * LoadTest可选参数：<br>
 * String类： endpoint, testSuite, testCase, loadTest, username, password, wssPasswordType,
 * domain, host, outputFolder, settingsFile, projectPassword,soapUISettingsPassword<br>
 * 
 * 整型：limit,threadCount<br>
 * boolean: printReport,saveAfterRun<br>
 * String[]： globalProperties,projectProperties<br>
 * 
 * <p>
 * Tool可选参数：iface,tool,settingsFile,projectPassword,settingsPassword,outputFolder
 * 
 * <p>
 * Test/SecurityTest可选参数：<br>
 * String类： endpoint, testSuite, testCase, username, password, wssPasswordType,
 * domain, host, outputFolder, settingsFile, projectPassword,soapUISettingsPassword<br>
 * String[]： globalProperties,projectProperties<br>
 * boolean:  printReport,exportAll,testFailIgnore,saveAfterRun<br> 
 * 
 * @author ravihuang
 * @see {@link http://www.soapui.org} {@link }
 * 
 */
public class SoapuiUA extends AbstractURIHandler {
	String projectFileName;
	String soapui="test";
	
	AbstractSoapUIRunner runner;
	HashMap<String, Object> paras;

	@Override
	public SoapuiUA build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			String tmp = this.get_schemeSpecificPart();
			if (tmp.startsWith(".") || tmp.startsWith("/")
					|| tmp.startsWith("file://"))
				projectFileName = tmp;
			else
				projectFileName = "soapui" + File.separator + tmp;
			
			if(null!=this.get_fragment())
				soapui=this.get_fragment().toLowerCase();
			
		}

		return this;
	}

	@Override
	public void teardown() {
		if (runner instanceof SoapUIMockServiceRunner) {
			((SoapUIMockServiceRunner) runner).stopAll();
		}
	}

	private void process_paras_map(Object mojo, HashMap<String, Object> map) {
		ReflectUtils setter = ReflectUtils.getInstance();

		HashMap<String, String> soapuiProperties = (HashMap<String, String>) map
				.remove("soapuiProperties");
		
		if (soapuiProperties != null && soapuiProperties.size() > 0)
			for (String key : soapuiProperties.keySet()) {
				log.debug("Setting " + (String) key + " value "
						+ soapuiProperties.get(key));
				System.setProperty(key, soapuiProperties.get(key));
			}

		for (String key : map.keySet()) {
			Object o = map.get(key);
			try {
				setter.setFieldValue(mojo, "parsed" + key, map.get(key));
				log.debug("set runner's parsed {} to {}", key, o);
			} catch (FieldnameNotFoundException e) {
				setter.setFieldValue(mojo, key, o, true);
				log.debug("set runner's {} to {}", key, o);
			}
		}
	}

	public void set_paras(HashMap<String, Object> map) {
		paras = map;
	}

	@Override
	public void startup() {
		if (this.projectFileName == null || this.paras == null)
			throw new RunFailureException(
					"please set projectFileName and paras");
		
		if (this.soapui.startsWith("mock")) {
			SoapUIMockServiceRunner tmp = new SoapUIMockServiceRunner();
			
			runner=tmp;
		} else if (this.soapui.startsWith("load")) {
			runner = new LoadTest();
		}else if (this.soapui.startsWith("tool")) {
			SoapUIToolRunner tmp = new SoapUIToolRunner();
			
			runner=tmp;
		}else if(this.soapui.startsWith("secu")){
			SoapUISecurityTestRunner tmp=new SoapUISecurityTestRunner();
			tmp.setJUnitReport(true);
			runner=tmp;	
		}else{
			SoapUITestCaseRunner tmp = new SoapUITestCaseRunner();
			tmp.setJUnitReport(true);			
			runner=tmp;			
		}
		
		if (tb.arg("testng_output_dir") != null)
			runner.setOutputFolder(tb.arg("testng_output_dir"));
		else
			runner.setOutputFolder("test-output");
		
		runner.setProjectFile(this.projectFileName);	
		
		process_paras_map(runner, paras);
		
		try {
			runner.run();
		} catch (Exception e) {
			log.error(e.toString());
			throw new RunFailureException("SoapUI run failed " + runner, e);
		}
	}

	@Override
	public String get_default_schema() {
		return "soapui";
	}

	@Override
	public boolean is_ready() {
		return runner != null;
	}
	
}
