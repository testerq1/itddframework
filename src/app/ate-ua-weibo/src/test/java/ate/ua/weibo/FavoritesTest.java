package ate.ua.weibo;

import java.util.List;

import org.testng.annotations.Test;

import weibo4j.Favorite;
import weibo4j.model.Favorites;
import weibo4j.model.FavoritesIds;
import weibo4j.model.FavoritesTag;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONObject;

public class FavoritesTest extends AbstractTest{    
    @Test
    public void UpdateFavoritesTagsBatch() {
        Favorite fm = new Favorite(access_token);
        String tid = "tid";
        String tag= "tag";
        try {
            JSONObject json = fm.updateFavoritesTagsBatch(tid, tag);
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void UpdateFavoritesTags() {
        Favorite fm = new Favorite(access_token);
        String id = "id";
        String tags= "tags";
        try {
            Favorites favors = fm.updateFavoritesTags(id, tags);
            log.info(favors.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void ShowFavorite() {
        Favorite fm = new Favorite(access_token);
        String id = "id";
        try {
            Favorites favors = fm.showFavorites(id);
            log.info(favors.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFavoritesTags() {
        Favorite fm = new Favorite(access_token);
        try {
            List<FavoritesTag> favors = fm.getFavoritesTags();
            for(FavoritesTag s : favors){
                log.info(s.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFavoritesIdsByTags() {
        Favorite fm = new Favorite(access_token);
        String tid = "tid";
        try {
            List<FavoritesIds> favors = fm.getFavoritesIdsByTags(tid);
            for(FavoritesIds s : favors){
                log.info(s.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFavoritesIds() {
        Favorite fm = new Favorite(access_token);
        try {
            List<FavoritesIds> ids = fm.getFavoritesIds();
            for(FavoritesIds s : ids){
                log.info(s.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }

    public void GetFavoritesByTags() {
        Favorite fm = new Favorite(access_token);
        String tid = "tid";
        try {
            List<Favorites> favors = fm.getFavoritesByTags(tid);
            for(Favorites s : favors){
                log.info(s.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFavorites() {
        Favorite fm = new Favorite(access_token);
        try {
            List<Favorites> favors = fm.getFavorites();
            for(Favorites s : favors){
                log.info(s.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }

    
    public void DestroyFavoritesTagsBatch() {
        Favorite fm = new Favorite(access_token);
        boolean result = false;
        String ids = "ids";
        try {
            result = fm.destroyFavoritesTagsBatch(ids);
            log.info(String.valueOf(result));
        } catch (WeiboException e) {

            e.printStackTrace();
        }
    }
    
    public void DestroyFavoritesBatch() {
        boolean result = false;
        String ids = "ids";
        Favorite fm = new Favorite(access_token);
        try {
            result = fm.destroyFavoritesTagsBatch(ids);
            log.info(String.valueOf(result));
        } catch (WeiboException e) {

            e.printStackTrace();
        }
    }
    
    public void DestroyFavorites() {
        String id = "id";
        Favorite fm = new Favorite(access_token);
        try {
            Favorites favors = fm.destroyFavorites(id);
            log.info(favors.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void CreateFavorites() {
        String id = "id";
        Favorite fm = new Favorite(access_token);
        try {
            Favorites favors = fm.createFavorites(id);
            log.info(favors.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
}
