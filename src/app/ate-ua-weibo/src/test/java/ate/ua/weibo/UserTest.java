package ate.ua.weibo;

import java.util.List;

import org.testng.annotations.Test;

import weibo4j.Users;
import weibo4j.model.User;
import weibo4j.model.UserCounts;
import weibo4j.model.WeiboException;

public class UserTest extends AbstractTest {
    @Test
    public void ShowUserByDomain() {
        String domain = "domain";
        Users um = new Users(access_token);
        try {
            User user = um.showUserByDomain(domain);
            log.info(user.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void ShowUser() {
        String uid = "uid";
        Users um = new Users(access_token);
        try {
            User user = um.showUserById(uid);
            log.info(user.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void UserCount() {
        String uids = "uid";
        Users um = new Users(access_token);
        try {
            List<UserCounts> user = um.getUserCount(uids);
            for (UserCounts u : user) {
                log.info(u.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
}
