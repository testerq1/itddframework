package ate.ua.weibo;

import org.testng.annotations.Test;

import weibo4j.Suggestion;
import weibo4j.model.User;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONArray;

public class SuggestionTest extends AbstractTest {
    @Test
    public void SuggestionsUsersNot_interested() {
        String uid = "";
        Suggestion suggestion = new Suggestion(access_token);
        try {
            User user = suggestion.suggestionsUsersNotInterested(uid);
            System.out.println(user.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void SuggestionsUsersMayInterested() {
        Suggestion suggestion = new Suggestion(access_token);
        try {
            JSONArray jo = suggestion.suggestionsUsersMayInterested();
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void SuggestionsFavoritesHot() {
        Suggestion suggestion = new Suggestion(access_token);
        try {
            JSONArray jo = suggestion.suggestionsFavoritesHot();
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    public void SuggestionsUsersHot() {
        Suggestion suggestion = new Suggestion(access_token);
        try {
            JSONArray jo = suggestion.suggestionsUsersHot();
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
}
