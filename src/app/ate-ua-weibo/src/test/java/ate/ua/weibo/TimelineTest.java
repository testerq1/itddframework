package ate.ua.weibo;

import java.util.List;

import org.testng.annotations.Test;

import weibo4j.Timeline;
import weibo4j.http.ImageItem;
import weibo4j.model.Emotion;
import weibo4j.model.FriendsTimelineIds;
import weibo4j.model.MentionsIds;
import weibo4j.model.RepostTimelineIds;
import weibo4j.model.Status;
import weibo4j.model.StatusWapper;
import weibo4j.model.UserTimelineIds;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONException;
import weibo4j.org.json.JSONObject;

public class TimelineTest extends AbstractTest {
    @Test
    public void Upload() {
        try {
            try {
                String comments="comments";
                byte[] content = readFileImage(pic);
                System.out.println("content length:" + content.length);
                ImageItem pic = new ImageItem("pic", content);
                String s = java.net.URLEncoder.encode(comments, "utf-8");
                                Timeline tm = new Timeline(access_token);
                Status status = tm.uploadStatus(s, pic);

                System.out.println("Successfully upload the status to ["
                        + status.getText() + "].");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        } catch (Exception ioe) {
            System.out.println("Failed to read the system input.");
        }
    }
    
    public void UpdateStatus() {
        String statuses = "statuses";
        Timeline tm = new Timeline(access_token);
        try {
            Status status = tm.updateStatus(statuses);
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }   
    }
    
    public void ShowStatus() {
        String id = "id";
        Timeline tm = new Timeline(access_token);
        try {
            Status status = tm.showStatus(id);
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void Repost() {
        String id = "id";
        Timeline tm = new Timeline(access_token);
        try {
            Status status = tm.repost(id);
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void QueryMid() {
        String id = "id";
        Timeline tm = new Timeline(access_token);
        try {
            JSONObject mid = tm.queryMid(1, id);
            log.info(mid.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void QueryId() {
        String mid = "mid";
        Timeline tm = new Timeline(access_token);
        try {
            JSONObject id = tm.queryId( mid, 1,1);
            log.info(id.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void GetUserTimelineIds() {
        String uid = "uid";
        Timeline tm = new Timeline(access_token);
        try {
            UserTimelineIds ids = tm.getUserTimelineIdsByUid(uid);
            log.info(ids.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetUserTimeline() {
        Timeline tm = new Timeline(access_token);
        try {
            StatusWapper status = tm.getUserTimeline();
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetStatusesCount() {
        String ids = "ids";
        try {
            Timeline tm = new Timeline(access_token);
            try {
                JSONArray json = tm.getStatusesCount(ids);
                for (int i = 0; i < json.length(); i++) {
                    log.info(json.getString(i));
                }
            } catch (WeiboException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    
    public void GetRepostTimelineIds() {
        String id = "id";
        Timeline tm = new Timeline(access_token);
        try {
            RepostTimelineIds ids = tm.getRepostTimelineIds(id);
            log.info(ids.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetRepostTimeline() {
        String id = "id";
        Timeline tm = new Timeline(access_token);
        try {
            StatusWapper status = tm.getRepostTimeline(id);
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetRepostByMe() {
        Timeline tm = new Timeline(access_token);
        try {
            StatusWapper status = tm.getRepostByMe();
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void GetPublicTimeline() {
        Timeline tm = new Timeline(access_token);
        try {
            StatusWapper status = tm.getPublicTimeline();
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetMentionsIds() {
        Timeline tm = new Timeline(access_token);
        try {
            MentionsIds ids = tm.getMentionsIds();
            log.info(ids.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetMentions() {
        Timeline tm = new Timeline(access_token);
        try {
            StatusWapper status = tm.getMentions();
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetHomeTimeline() {
        Timeline tm = new Timeline(access_token);
        try {
            StatusWapper status = tm.getHomeTimeline();
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFriendsTimelineIds() {
        Timeline tm = new Timeline(access_token);
        try {
            FriendsTimelineIds status = tm.getFriendsTimelineIds();
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void GetFriendsTimeline() {      
        Timeline tm = new Timeline(access_token);
        try {
            StatusWapper status = tm.getFriendsTimeline();
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void GetEmotions() {
        Timeline tm = new Timeline(access_token);
        try {
            List<Emotion> emotions =  tm.getEmotions();
            for(Emotion e : emotions){
                log.info(e.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetBilateralTimeline() {
        Timeline tm = new Timeline(access_token);
        try {
            StatusWapper status = tm.getBilateralTimeline();
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void Destroy() {
        String id =  "id";
        Timeline tm = new Timeline(access_token);
        try {
            Status status = tm.destroy(id);
            log.info(status.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
}
