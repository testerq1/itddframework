package ate.ua.weibo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.testng.annotations.Test;

import ate.testcase.Testcase;
import weibo4j.Oauth;
import weibo4j.model.WeiboException;
import weibo4j.util.BareBonesBrowserLaunch;

public class OAuthTest extends Testcase {
    
    @Test
    public void oauth2() {
        Oauth oauth = new Oauth();
        try {
            BareBonesBrowserLaunch.openURL(oauth.authorize("code"));
            System.out.println(oauth.authorize("code"));
            System.out.print("Hit enter when it's done.[Enter]:");
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    System.in));
            String code = br.readLine();
            log.info("code: " + code);

            System.out.println(oauth.getAccessTokenByCode(code));
        } catch (WeiboException e) {
            if (401 == e.getStatusCode()) {
                log.info("Unable to get the access token.");
            } else {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
