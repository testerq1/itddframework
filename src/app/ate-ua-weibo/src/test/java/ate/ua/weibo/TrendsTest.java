package ate.ua.weibo;

import java.util.List;

import org.testng.annotations.Test;

import weibo4j.Trend;
import weibo4j.model.Trends;
import weibo4j.model.UserTrend;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONObject;

public class TrendsTest extends AbstractTest {
    @Test
    public void TrendsFollow() {
        Trend tm = new Trend(access_token);
        String trend_name = "trend_name";
        try {
            UserTrend ut = tm.trendsFollow(trend_name);
            log.info(ut.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void TrendDestroy(){
        Trend tm = new Trend(access_token);
        int trendId = 123456;
        try {
            JSONObject result = tm.trendsDestroy(trendId);
            log.info(String.valueOf(result));
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void IsFollow() {
        String trend_name = "trend_name";
        Trend tm = new Trend(access_token);
        try {
            JSONObject result = tm.isFollow(trend_name);
            log.info(String.valueOf(result));
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetTrendsWeekly() {
        Trend tm = new Trend(access_token);
        try {
            List<Trends> trends = tm.getTrendsWeekly();
            for(Trends ts : trends){
                log.info(ts.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetTrendsHourly() {
        Trend tm = new Trend(access_token);
        try {
            List<Trends> trends = tm.getTrendsHourly();
            for(Trends ts : trends){
                log.info(ts.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetTrendsDaily() {
        Trend tm = new Trend(access_token);
        try {
            List<Trends> trends = tm.getTrendsDaily();
            for(Trends ts : trends){
                log.info(ts.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetTrends() {
        String uid = "uid";
        Trend tm = new Trend(access_token);
        try {
            List<UserTrend> trends = tm.getTrends(uid);
            for(UserTrend t : trends){
                log.info(t.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
}
