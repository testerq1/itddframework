package ate.ua.weibo;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import ate.testcase.Testcase;

public abstract class AbstractTest extends Testcase {
    protected String access_token = "1255539415";
    protected String client_SERCRET = "2b2fb34146314a25a10353dc86bccad5";
    protected String pic="test.jpg";
    
    protected static byte[] readFileImage(String filename) throws IOException {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(
                new FileInputStream(filename));
        int len = bufferedInputStream.available();
        byte[] bytes = new byte[len];
        int r = bufferedInputStream.read(bytes);
        if (len != r) {
            bytes = null;
            throw new IOException("读取文件不正确");
        }
        bufferedInputStream.close();
        return bytes;
    }
}
