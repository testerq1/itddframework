package ate.ua.weibo;

import org.testng.annotations.Test;

import weibo4j.PublicService;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONObject;

public class PublicserviceTest extends AbstractTest {
    @Test
    public void ProvinceList() {
        String country = "中国";
        PublicService ps = new PublicService(access_token);
        try {
            JSONArray jo = ps.provinceList(country);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
        
    }
    
    public void GetTimeZone() {
        PublicService ps = new PublicService(access_token);
        try {
            JSONObject  jo = ps.getTomeZone();
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
        
    }
    
    public void GetLocationByCode() {
        String codes = "0755";
        PublicService ps = new PublicService(access_token);
        try {
            JSONArray jo = ps.getLocationByCode(codes);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
        
    }
    public void CountryList() {
        PublicService ps = new PublicService(access_token);
        try {
            JSONArray jo = ps.countryList();
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
        
    }
    
    public void CityList() {
        String province = "广州";
        PublicService ps = new PublicService(access_token);
        JSONArray jo;
        try {
            jo = ps.cityList(province);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
        
    }
}
