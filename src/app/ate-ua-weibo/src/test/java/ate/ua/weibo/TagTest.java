package ate.ua.weibo;

import java.util.List;

import org.testng.annotations.Test;

import weibo4j.Tags;
import weibo4j.model.Tag;
import weibo4j.model.TagWapper;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONObject;

public class TagTest extends AbstractTest {
    @Test
    public void GetTagsSuggestions() {
        Tags tm = new Tags(access_token);
        List<Tag> tags = null;
        try {
            tags = tm.getTagsSuggestions();
            for(Tag tag : tags){                
                log.info(tag.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetTagsBatch() {
        String uids = "uids";
        Tags tm = new Tags(access_token);
        try {
            TagWapper tags = tm.getTagsBatch(uids);
            log.info(tags.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void GetTags() {
        String uid = "uid";
        Tags tm  = new Tags(access_token);
        try {
            List<Tag> tags = tm.getTags(uid);
            for(Tag tag:tags ){             
                log.info(tag.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void DestroyTagsBatch() {
        String ids = "ids";
        Tags tm = new Tags(access_token);
        try {
            List<Tag> tags = tm.destroyTagsBatch(ids);
            for(Tag t : tags){
                log.info(t.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void DestroyTag() {
        int tag_id = 123;//Integer.parseInt(args[1]);
        Tags tm = new Tags(access_token);
        try {
            JSONObject result = tm.destoryTag(tag_id);
            log.info(String.valueOf(result));
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void CreateTags(){
        String tag = "tag";
        Tags tm = new Tags(access_token);
        try {
            JSONArray tags = tm.createTags(tag);
            log.info(tags.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    
}
