package ate.ua.weibo;

import org.testng.annotations.Test;

import weibo4j.ShortUrl;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONObject;

public class ShortUrlTest extends AbstractTest {
    @Test
    public void StatusesContentUrl() {
        String url = "";
        ShortUrl su = new ShortUrl(access_token);
        try {
            JSONObject jo = su.statusesContentUrl(url);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void ShortToLongUrl() {
        String url = "";
        ShortUrl su = new ShortUrl(access_token);
        try {
            JSONObject jo = su.shortToLongUrl(url);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void ShareCountsOfUrl() {
        String url = "";
        ShortUrl su = new ShortUrl(access_token);
        try {
            JSONObject jo = su.shareCountsOfUrl(url);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void ReferersOfUrl() {
        String url ="";
        ShortUrl su = new ShortUrl(access_token);
        try {
            JSONObject jo = su.referersOfUrl(url);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void LongToShortUrl() {
        String url = "";
        ShortUrl su = new ShortUrl(access_token);
        try {
            JSONObject jo = su.longToShortUrl(url);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void LocationOfUrl() {
        String url = "";
        ShortUrl su = new ShortUrl(access_token);
        try {
            JSONObject jo = su.locationsOfUrl(url);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void CommentsContenUrl() {
        String url = "";
        ShortUrl su = new ShortUrl(access_token);
        try {
            JSONObject jo = su.commentsContentUrl(url);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void CommentCountOfUrl() {
        String url = "";
        ShortUrl su = new ShortUrl(access_token);
        try {
            JSONObject jo = su.commentCountOfUrl(url);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void CliksOfUrl() {
        String url = "";
        ShortUrl su = new ShortUrl(access_token);
        try {
            JSONObject jo = su.clicksOfUrl(url);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
}
