package ate.ua.weibo;

import java.util.List;

import org.testng.annotations.Test;

import weibo4j.Search;
import weibo4j.model.SchoolSearch;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONArray;

public class SearchTest extends AbstractTest {
    @Test
    public void SearchSuggestionsUsers() {
        String q = "q";
        Search search = new Search(access_token);
        try {
            JSONArray jo = search.searchSuggestionsUsers(q);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void SearchSuggestionsSchools() {
        String q = "q";
        Search search = new Search(access_token);
        try {
            List<SchoolSearch> list = search.searchSuggestionsSchools(q);
            for (SchoolSearch ss : list) {
                log.info(ss.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void SearchSuggestionsCompanies() {
        String q = "q";
        Search search = new Search(access_token);
        try {
            JSONArray jo = search.searchSuggestionsCompanies(q);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void SearchSuggestionsAtUsers() {
        String q = "11.11";
        int type = 1;
        Search search = new Search(access_token);
        try {
            JSONArray jo = search.searchSuggestionsAtUsers(q, type);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void SearchSuggestionsApps() {
        String q = "q";
        int count = 11;
        Search search = new Search(access_token);
        try {
            JSONArray jo = search.searchSuggestionsApps(q, count);
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
}
