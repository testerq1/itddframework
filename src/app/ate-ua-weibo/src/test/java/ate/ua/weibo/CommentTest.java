package ate.ua.weibo;

import java.util.List;

import org.testng.annotations.Test;

import weibo4j.Comments;
import weibo4j.model.Comment;
import weibo4j.model.CommentWapper;
import weibo4j.model.WeiboException;

public class CommentTest extends AbstractTest {
    @Test
    public void ReplyComment() {
        String cid = "cid";
        String id = "id";
        String comments = "comments";
        Comments cm = new Comments(access_token);
        try {
            Comment com = cm.replyComment(cid, id, comments);
            log.info(com.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetCommentToMe() {
        Comments cm = new Comments(access_token);
        try {
            CommentWapper comment = cm.getCommentToMe();
            log.info(comment.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetCommentTimeline() {
        Comments cm = new Comments(access_token);
        try {
            CommentWapper comment = cm.getCommentTimeline();
            log.info(comment.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetCommentShowBatch() {
        String cids = "cid1 cid2";
        Comments cm = new Comments(access_token);
        try {
            List<Comment> comment = cm.getCommentShowBatch(cids);
            for (Comment c : comment) {
                log.info(c.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetCommentMentions() {
        Comments cm = new Comments(access_token);
        try {
            CommentWapper comment = cm.getCommentMentions();
            log.info(comment.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetCommentByMe() {      
        Comments cm = new Comments(access_token);
        try {
            CommentWapper comment = cm.getCommentByMe();
            log.info(comment.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetCommentById() {
        String id = "cid";
        Comments cm = new Comments(access_token);
        try {
            CommentWapper comment = cm.getCommentById(id);
            log.info(comment.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void CreateComment() {        
        String comments = "comments";
        String cid = "cid";
        Comments cm = new Comments(access_token);
        try {
            Comment comment = cm.createComment(comments, cid);
            log.info(comment.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void DestroyComment() {      
        String cid = "cid";
        Comments cm = new Comments(access_token);
        try {
            Comment com = cm.destroyComment(cid);
            log.info(com.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
}
