package ate.ua.weibo;

import java.util.List;

import org.testng.annotations.Test;

import weibo4j.Account;
import weibo4j.Comments;
import weibo4j.model.Comment;
import weibo4j.model.Privacy;
import weibo4j.model.RateLimitStatus;
import weibo4j.model.School;
import weibo4j.model.User;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONObject;

public class AccountTest extends AbstractTest {
    @Test
    public void DestroyCommentBatch() {
        String cids = "cid1 cid2";
        Comments cm = new Comments(access_token);
        try {
            List<Comment> list = cm.destoryCommentBatch(cids);
            for (Comment c : list) {
                log.info(c.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void GetUid() {
        Account am = new Account(access_token);
        try {
            JSONObject uid = am.getUid();
            log.info(uid.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void GetAccountRateLimitStatus() {
        Account am = new Account(access_token);
        try {
            RateLimitStatus json = am.getAccountRateLimitStatus();
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void GetAccountProfileSchoolList() {
        Account am = new Account(access_token);
        String province = "广东";
        String capital = "东莞";
        try {
            List<School> schools = am.getAccountProfileSchoolList(province,
                    capital);
            for (School school : schools) {
                log.info(school.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void GetAccountPrivacy() {
        Account am = new Account(access_token);
        try {
            Privacy privacy = am.getAccountPrivacy();
            log.info(privacy.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void EndSession() {
        Account am = new Account(access_token);
        try {
            User user = am.endSession();
            log.info(user.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
}
