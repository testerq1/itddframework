package ate.ua.weibo;

import org.testng.annotations.Test;

import weibo4j.Friendships;
import weibo4j.model.User;
import weibo4j.model.UserWapper;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONArray;
import weibo4j.org.json.JSONObject;

public class FriendshipsTest extends AbstractTest{
    String uid = "uid";
    
    @Test
    public void GetRemark() {
        Friendships fm = new Friendships(access_token);
        String uids = "uid";
        try {
            JSONArray user = fm.getRemark(uids);
            System.out.println(user.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }

    public void GetFriendsInCommon() {
        Friendships fm = new Friendships(access_token);
        try {
            UserWapper users = fm.getFriendsInCommon(uid);
            for(User u : users.getUsers()){
                log.info(u.toString());
            }
            System.out.println(users.getNextCursor());
            System.out.println(users.getPreviousCursor());
            System.out.println(users.getTotalNumber());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }

    
    public void GetFriendsIdsByName() {
        String screenName = "screenName";
        Friendships fm = new Friendships(access_token);
        try {
            String[] ids = fm.getFriendsIdsByName(screenName);
            for(String s : ids){
                log.info(s);
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void GetFriendsIds() {
        Friendships fm = new Friendships(access_token);
        try {
            String[] ids = fm.getFriendsIdsByUid(uid);
            for(String s : ids){
                log.info(s);
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void GetFriendshipsByName() {
        String source = "name1";
        String target = "name2";
        Friendships fm = new Friendships(access_token);
        try {
            JSONObject json = fm.getFriendshipsByName(source, target);
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFriendshipsById() {
        long source = Long.parseLong("1111111");
        long target = Long.parseLong("2222222");
        Friendships fm = new Friendships(access_token);
        try {
            JSONObject json = fm.getFriendshipsById(source, target);
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFriendsChainFollowers() {
        Friendships fm = new Friendships(access_token);
        try {
            UserWapper  users = fm.getFriendsChainFollowers(uid);
            for(User s : users.getUsers()){
                log.info(s.toString());
            }
            System.out.println(users.getNextCursor());
            System.out.println(users.getPreviousCursor());
            System.out.println(users.getTotalNumber());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFriendsByName() {
        String name = "eva.tang";
        Friendships fm = new Friendships(access_token);
        try {
            UserWapper users = fm.getFriendsByScreenName(name);
            for(User u : users.getUsers()){
                System.out.println(u.toString());
            }
            System.out.println(users.getNextCursor());
            System.out.println(users.getPreviousCursor());
            System.out.println(users.getTotalNumber());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void GetFriendsById() {
        Friendships fm = new Friendships(access_token);
        try {
            UserWapper users = fm.getFriendsByID(uid);
            for(User u : users.getUsers()){
                System.out.println(u.toString());
            }
            System.out.println(users.getNextCursor());
            System.out.println(users.getPreviousCursor());
            System.out.println(users.getTotalNumber());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }

    
    public void GetFriendsBilateralIds() {
        Friendships fm = new Friendships(access_token);
        try {
            String[] ids = fm.getFriendsBilateralIds(uid);
            for(String s : ids){
                log.info(s);
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFriendsBilateral() {
        Friendships fm = new Friendships(access_token);
        try {
            UserWapper users = fm.getFriendsBilateral(uid);
            for(User u : users.getUsers()){
                log.info(u.toString());
            }
            System.out.println(users.getNextCursor());
            System.out.println(users.getPreviousCursor());
            System.out.println(users.getTotalNumber());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFollowsActive() {
        Friendships fm = new Friendships(access_token);
        try {
            UserWapper users = fm.getFollowersActive(uid);
            for(User u : users.getUsers()){
                log.info(u.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFollowersIds() {
        Friendships fm = new Friendships(access_token);
        try {
            String[] ids = fm.getFollowersIdsById(uid);
            for(String u : ids){
                log.info(u);
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFollowersById() {
        Friendships fm = new Friendships(access_token);
        try {
            UserWapper users = fm.getFollowersById(uid);
            for(User u : users.getUsers()){
                log.info(u.toString());
            }
            System.out.println(users.getNextCursor());
            System.out.println(users.getPreviousCursor());
            System.out.println(users.getTotalNumber());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFollowers() {
        Friendships fm = new Friendships(access_token);
        String screen_name = "eva.tang";
        try {
            UserWapper users = fm.getFollowersByName(screen_name);
            for(User u : users.getUsers()){
                log.info(u.toString());
            }
            System.out.println(users.getNextCursor());
            System.out.println(users.getPreviousCursor());
            System.out.println(users.getTotalNumber());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void DestroyFriendshipsById() {      
        Friendships fm = new Friendships(access_token);
        try {
            User user = fm.destroyFriendshipsById(uid);
            log.info(user.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void CreateFriendships() {
        
        Friendships fm = new Friendships(access_token);
        try {
            User user = fm.createFriendshipsById(uid);
            log.info(user.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
}
