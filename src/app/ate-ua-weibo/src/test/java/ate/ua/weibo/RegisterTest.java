package ate.ua.weibo;

import org.testng.annotations.Test;

import weibo4j.Register;
import weibo4j.Reminds;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONObject;

public class RegisterTest extends AbstractTest {
    @Test
    public void Remind() {
        Reminds rm = new Reminds(access_token);
        try {
            JSONObject jo = rm.getUnreadCountOfRemind();
            System.out.println(jo.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
        
    }
    
    public void VerifyNickname() {
        String nickname = "david";
        Register reg = new Register(access_token);
        try {
            JSONObject json = reg.verifyNickname(nickname);
            System.out.println(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
}
