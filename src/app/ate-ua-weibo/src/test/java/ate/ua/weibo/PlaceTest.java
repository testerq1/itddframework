package ate.ua.weibo;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.testng.annotations.Test;

import weibo4j.Place;
import weibo4j.http.ImageItem;
import weibo4j.model.Places;
import weibo4j.model.PoisitionCategory;
import weibo4j.model.Status;
import weibo4j.model.StatusWapper;
import weibo4j.model.UserWapper;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONObject;

public class PlaceTest extends AbstractTest {
    @Test
    public void GetUserTimeLine() {
        String uid = "uid";
        Place p = new Place(access_token);
        try {
            StatusWapper sw = p.userTimeLine(uid);
            log.info(sw.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetUserPhotoList() {
        String uid = "uid";
        Place p = new Place(access_token);
        try {
            StatusWapper sw = p.userPhotoList(uid);
            log.info(sw.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetUserInfoInLbs() {
        String uid = "uid";
        Place p = new Place(access_token);
        try {
            JSONObject sw = p.userInfoInLBS(uid);
            log.info(sw.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetUserCheckins() {
        String uid = "uid";
        Place p = new Place(access_token);
        try {
            List<Places> list = p.checkinsList(uid);
            for (Places pl : list) {
                log.info(pl.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetStatusesShow() {
        String id = "id";
        Place p = new Place(access_token);
        try {
            Status s = p.statusesShow(id);
            log.info(s.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetPoisUserList() {
        String poiid = "poiid";
        Place p = new Place(access_token);
        try {
            UserWapper uw = p.poisUsersList(poiid);
            log.info(uw.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetPoisTimeLine() {
        String poiid = "poiid";
        Place p = new Place(access_token);
        try {
            StatusWapper sw = p.poisTimeLine(poiid);
            log.info(sw.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetPoisShow() {
        String poiid = "poiid";
        Place p = new Place(access_token);
        try {
            Places pl = p.poisShow(poiid);
            log.info(pl.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void GetPoisSearch() {
        String keyword = "beijing";
        Place p = new Place(access_token);
        try {
            List<Places> list = p.poisSearch(keyword);
            for (Places pl : list) {
                log.info(pl.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void GetPoisPhotoList() {
        String poiid = "poiid";
        Place p = new Place(access_token);
        try {
            StatusWapper sw = p.poisPhotoList(poiid);
            log.info(sw.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetPoisCategory() {
        Place p = new Place(access_token);
        try {
            List<PoisitionCategory> list = p.poisCategory();
            for (PoisitionCategory pois : list) {
                log.info(pois.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetNearbyUsers() {
        String lat = "lat";
        String lon = "lon";
        Place p = new Place(access_token);
        try {
            UserWapper uw = p.nearbyUsers(lat, lon);
            log.info(uw.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetNearbyTimeLine() {
        String lat = "lat";
        String lon = "lon";
        Place p = new Place(access_token);
        try {
            StatusWapper sw = p.nearbyTimeLine(lat, lon);
            log.info(sw.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetNearbyPois() {
        String lat = "lat";
        String lon = "lon";
        Place p = new Place(access_token);
        try {
            List<Places> list = p.nearbyPois(lat, lon);
            for (Places pl : list) {
                log.info(pl.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetNearbyPhoto() {
        String lat = "lat";
        String lon = "lon";
        Place p = new Place(access_token);
        try {
            StatusWapper sw = p.nearbyPhoto(lat, lon);
            log.info(sw.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetFriendsTimeLine() {
        Place p = new Place(access_token);
        try {
            StatusWapper sw = p.friendsTimeLine();
            log.info(sw.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void AddTip() {
        String poiid = "poiid";
        String status = "status";
        Place p = new Place(access_token);
        try {
            Status s = p.addTip(poiid, status);
            log.info(s.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void AddPhoto() {
        String poiid = "poiid";
        String status = "status";
        Place p = new Place(access_token);
        try {
            byte[] pic =readFileImage("123.jpg");
            ImageItem item = new ImageItem(pic);
            Status s = p.addPhoto(poiid, status, item);
            log.info(s.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void AddCheckin() {
        try {
            String poiid = "poiid";
            String status = java.net.URLEncoder.encode("trtrt", "UTF-8");
            byte[] pic = readFileImage("test.jpg");
            Place p = new Place(access_token);
            ImageItem item = new ImageItem(pic);
            Status s = p.addCheckin(poiid, status, item);
            log.info(s.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
