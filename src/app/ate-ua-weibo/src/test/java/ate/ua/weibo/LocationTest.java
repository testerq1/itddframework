package ate.ua.weibo;

import java.util.List;

import org.testng.annotations.Test;

import weibo4j.Location;
import weibo4j.model.Geos;
import weibo4j.model.Poisition;
import weibo4j.model.WeiboException;
import weibo4j.org.json.JSONObject;

public class LocationTest extends AbstractTest {
    @Test
    public void ShowPoisBatch() {
        String srcids = "srcids";
        Location l = new Location(access_token);
        try {
            List<Poisition> list = l.showPoisBatch(srcids);
            for (Poisition p : list) {
                log.info(p.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void SearchPoisByLocation() {
        String q = "uenc";
        Location l = new Location(access_token);
        try {
            List<Poisition> list = l.searchPoisByLocationByQ(q);
            for (Poisition p : list) {
                log.info(p.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void SearchPoisByGeo() {
        String q = "uenc";
        String cenname = "cenname";
        Location l = new Location(access_token);
        try {
            JSONObject json = l.searchPoisByGeoByCenname(q, cenname);
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }

    }
    
    public void SearchPoisByArea() {
        String q = "uenc";
        String coordinates = "123.45,234.56";
        Location l = new Location(access_token);
        try {
            List<Poisition> list = l.searchPoisByAreaByQ(q, coordinates);
            for (Poisition p : list) {
                log.info(p.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void SearchDriveRoute() {
        String beginCoordinate = "123.45,234,56";
        String endCoordinate = "124.45,235,56";
        Location l = new Location(access_token);
        try {
            JSONObject json = l.searchDriveRouteByCoordinate(beginCoordinate,
                    endCoordinate);
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void SearchBusStation() {
        String q = "uenc";
        Location l = new Location(access_token);
        try {
            JSONObject json = l.searchBusStation(q);
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    
    public void SearchBusRoute() {
        String beginCoordinate = "123.45,234,56";
        String endCoordinate = "124.45,235,56";
        Location l = new Location(access_token);
        try {
            JSONObject json = l.searchBusRouteByCoordinate(
                    beginCoordinate, endCoordinate);
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void SearchBusLine() {
        String q = "uenc";
        Location l = new Location(access_token);
        try {
            JSONObject json = l.searchBusLine(q);
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void IsDomestic() {
        String coordinates = "123.45,234.56";
        Location l = new Location(access_token);
        try {
            JSONObject json = l.isDomestic(coordinates);
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void IpToGeo() {
        String ip = "1.1.1.1";
        Location l = new Location(access_token);
        try {
            List<Geos> list = l.ipToGeo(ip);
            for (Geos g : list) {
                log.info(g.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GpsToOffset() {        
        String coordinate = "123.45,234.56";
        Location l = new Location(access_token);
        try {
            JSONObject json = l.gpsToOffset(coordinate);
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetMapImage() {
        String city = "010";
        Location l = new Location(access_token);
        try {
            JSONObject json = l.getMapImageByCity(city);
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void AddressToGeo() {
        String address = "address";
        Location l = new Location(access_token);
        try {
            List<Geos> list = l.addressToGeo(address);
            for (Geos g : list) {
                log.info(g.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GeoToAddress() {
        String coordinate = "123.45,1234.56";
        Location l = new Location(access_token);
        try {
            List<Geos> list = l.geoToAddress(coordinate);
            for (Geos g : list) {
                log.info(g.toString());
            }
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void GetLocation() {
        String j = "{json}";
        Location l = new Location(access_token);
        try {
            JSONObject json = l.getLocation(j);
            log.info(json.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
    
    public void AddPois() {
        String srcid = "srcid";
        String name = "name";
        String address = "address";
        String cityName = "cityName";
        String category = "category";
        String longitude = "123";
        String latitude = "456";
        Location l = new Location(access_token);
        try {
            Poisition pois = l.addPois(srcid, name, address, cityName,
                    category, longitude, latitude);
            log.info(pois.toString());
        } catch (WeiboException e) {
            e.printStackTrace();
        }
    }
}
