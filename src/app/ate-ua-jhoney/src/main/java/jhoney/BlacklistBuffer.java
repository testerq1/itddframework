package jhoney;
import java.io.*;
import java.util.*;

/*
*	This class stores blacklist IP addresses and contains functionality
*	for adding/removing the IP from the firewall blacklist.
*/
public class BlacklistBuffer implements Runnable {
	//Declarations
	private static BlacklistBuffer instance;
	private Vector bl_list;
	private final String objfile = "blacklist.db";
	private HSProperties prop;
	
	private long removetime;
	//
	
	public static BlacklistBuffer getInstance() {
		if(instance == null) {
			instance = new BlacklistBuffer();
		}
		return instance;
	}
	
	/*
	*	Singleton class.
	*/
	private BlacklistBuffer() {
		prop = HSProperties.getInstance();
		removetime = prop.getBlacklistTimeout();
		
		readList();
		
		Thread thr = new Thread(this);
		thr.start();
	}
	
	/*
	*	Thread for removing blacklisting older than the user selected time.
	*/
	public void run() {
		boolean running = true;
		while(running) {
			try {
				Thread.sleep(12007);
			}
			catch(Exception err) {
			}
			
			update = false;
			checkRemove();
			if(update) {
				saveList();
			}
		}
	}
	
	private boolean update = false;
	/*
	*	Checks if there exists old blacklistings in the db file.
	*	If old blacklistings are found, they are removed and the 
	*	update variable is set to true (and the thread saves the new
	*	db file).
	*/
	private void checkRemove() {
		long current = System.currentTimeMillis();
		for(int i=0;i<bl_list.size();i++) {
			BlObject obj = (BlObject)bl_list.elementAt(i);
			if(current - obj.timestamp > removetime) {
				bl_list.removeElementAt(i);
				allow(obj.ip);
				update = true;
				checkRemove();
			}
		}
	}
	
	/*
	*	Adds a new blacklisted IP (if it is not found in the db file).
	*	The drop(ip) method is called to execute the firewall blacklisting.
	*/
	public synchronized long add(String ip) {	
	
		long diff = 10000;
		BlObject obj = new BlObject(ip);
		
		boolean upd = false;
		boolean found = false;
		for(int i=0;i<bl_list.size();i++) {
			BlObject tmp = (BlObject)bl_list.elementAt(i);
			if(tmp.ip.equals(ip)) {
				found = true;
				if(obj.timestamp > tmp.timestamp) {
					diff = tmp.timestamp - obj.timestamp;
					tmp.timestamp = obj.timestamp;
					upd = true;
				}
				break;
			}
		}
		
		if(!found) {
			drop(obj.ip);
			bl_list.add(obj);
			saveList();
		}
		return diff;
	}
	
	/*
	*	Reads the blacklist db file.
	*/
	private void readList() {
		try {
			ObjectInputStream oin = new ObjectInputStream(new FileInputStream(objfile));
			bl_list = (Vector)oin.readObject();
			oin.close();
		}
		catch(Exception err) {
			bl_list = new Vector();
		}
	}
	
	/*
	*	Saves the blacklist db file.
	*/
	private void saveList() {
		try {
			ObjectOutputStream oout = new ObjectOutputStream(new FileOutputStream(objfile));
			oout.writeObject(bl_list);
			oout.close();
		}
		catch(Exception err) {
			MsgLog.add("FATAL: Unable to write blacklist.db");
		}
	}
	
	/*
	*	Executes the drop shell script to set the firewall to blacklist the
	*	IP address.
	*/
	private void drop(String ip) {
		if(!prop.useAutoBlacklist()) return;
		
		ExecScript es = new ExecScript("drop",ip);
	}
	
	/*
	*	Executes the allow shell script to set the firewall to allow traffic
	*	from the IP address.
	*/
	private void allow(String ip) {
		if(!prop.useAutoBlacklist()) return;
	
		ExecScript es = new ExecScript("allow",ip);
	}
}