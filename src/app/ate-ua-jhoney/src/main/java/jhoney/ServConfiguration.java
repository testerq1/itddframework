package jhoney;
import java.io.*;
import java.util.*;
import java.net.URLDecoder;

/*
*	Contains the methods for JHServ Configuration menu alternatives
*/
public class ServConfiguration extends ServBasics {
	
	public ServConfiguration() {
		
	}
	
	/*
	*	Edit "blacklist" script
	*/
	public void FW_Blacklist(BufferedWriter writer, String file, String[] params) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		String par = getParam(params,"Help");
	
		if(params == null || par != null) {
			writeLine("<table width=500><tr><td>",writer);
			String conf = "";
			try {
				BufferedReader reader = new BufferedReader(new FileReader("blacklist"));
				String line = reader.readLine();
				while(line != null) {
					conf += line + "\n";
					line = reader.readLine();
				}
			}
			catch(Exception err) {
				
			}
			writeLine("<p class=Subtitle>Firewall show blacklist entries&nbsp;&nbsp;<a href=\"conf_list.html?Help=FWscript\"><img src=help.png></a></p>",writer);
			writeLine("<form enctype=\"multipart/form-data\">",writer);
			writeLine("<textarea class=conf2 cols=70 name=text rows=16>"+conf+"</textarea><br>",writer);
			writeLine("<br><input class=button id=saveconf type=submit value=\"Save\"/>",writer);
			writeLine("</form>",writer);
			writeLine("</td></tr></table>",writer);
			
			//Show help
			if(par != null) {
				writeLine("<br><table width=650><tr><td>",writer);
				sendTextfile("help/FWscript.html",writer);
				writeLine("</td></tr></table>",writer);
			}
			//
		}
		else { //Update config file
			String tmp = params[1];
			try {
				String text = URLDecoder.decode(tmp,"UTF-8");
				
				BufferedWriter wr = new BufferedWriter(new FileWriter("blacklist"));
				wr.write(text);
				wr.newLine();
				wr.close();
				
				writeLine("<P>Blacklist script update successfully</p>",writer);
			}
			catch(Exception err) {
				err.printStackTrace();
				writeLine("<P><b>ERROR</b><br>Unable to update Blacklist script</p>",writer);
			}
		}
		
		sendTextfile("http/footer.html",writer);
	}
	
	/*
	*	Edit "drop" script
	*/
	public void FW_Drop(BufferedWriter writer, String file, String[] params) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		String par = getParam(params,"Help");
	
		if(params == null || par != null) {
			writeLine("<table width=500><tr><td>",writer);
			String conf = "";
			try {
				BufferedReader reader = new BufferedReader(new FileReader("drop"));
				String line = reader.readLine();
				while(line != null) {
					conf += line + "\n";
					line = reader.readLine();
				}
			}
			catch(Exception err) {
				
			}
			writeLine("<p class=Subtitle>Firewall Drop traffic from IP script&nbsp;&nbsp;<a href=\"conf_drop.html?Help=FWscript\"><img src=help.png></a></p>",writer);
			writeLine("<form enctype=\"multipart/form-data\">",writer);
			writeLine("<textarea class=conf2 cols=70 name=text rows=16>"+conf+"</textarea><br>",writer);
			writeLine("<br><input class=button id=saveconf type=submit value=\"Save\"/>",writer);
			writeLine("</form>",writer);
			writeLine("</td></tr></table>",writer);
			
			//Show help
			if(par != null) {
				writeLine("<br><table width=650><tr><td>",writer);
				sendTextfile("help/FWscript.html",writer);
				writeLine("</td></tr></table>",writer);
			}
			//
		}
		else { //Update config file
			String tmp = params[1];
			try {
				String text = URLDecoder.decode(tmp,"UTF-8");
				
				BufferedWriter wr = new BufferedWriter(new FileWriter("drop"));
				wr.write(text);
				wr.newLine();
				wr.close();
				
				writeLine("<P>Drop script update successfully</p>",writer);
			}
			catch(Exception err) {
				err.printStackTrace();
				writeLine("<P><b>ERROR</b><br>Unable to update Drop script</p>",writer);
			}
		}
		
		sendTextfile("http/footer.html",writer);
	}
	
	/*
	*	Edit allow script
	*/
	public void FW_Allow(BufferedWriter writer, String file, String[] params) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		String par = getParam(params,"Help");
	
		if(params == null || par != null) {
			writeLine("<table width=500><tr><td>",writer);
			String conf = "";
			try {
				BufferedReader reader = new BufferedReader(new FileReader("allow"));
				String line = reader.readLine();
				while(line != null) {
					conf += line + "\n";
					line = reader.readLine();
				}
			}
			catch(Exception err) {
				
			}
			writeLine("<p class=Subtitle>Firewall Allow traffic from IP script&nbsp;&nbsp;<a href=\"conf_allow.html?Help=FWscript\"><img src=help.png></a></p>",writer);
			writeLine("<form enctype=\"multipart/form-data\">",writer);
			writeLine("<textarea class=conf2 cols=70 name=text rows=16>"+conf+"</textarea><br>",writer);
			writeLine("<br><input class=button id=saveconf type=submit value=\"Save\"/>",writer);
			writeLine("</form>",writer);
			writeLine("</td></tr></table>",writer);
			
			//Show help
			if(par != null) {
				writeLine("<br><table width=650><tr><td>",writer);
				sendTextfile("help/FWscript.html",writer);
				writeLine("</td></tr></table>",writer);
			}
			//
		}
		else { //Update config file
			String tmp = params[1];
			try {
				String text = URLDecoder.decode(tmp,"UTF-8");
				
				BufferedWriter wr = new BufferedWriter(new FileWriter("allow"));
				wr.write(text);
				wr.newLine();
				wr.close();
				
				writeLine("<P>Allow script update successfully</p>",writer);
			}
			catch(Exception err) {
				err.printStackTrace();
				writeLine("<P><b>ERROR</b><br>Unable to update Allow script</p>",writer);
			}
		}
		
		sendTextfile("http/footer.html",writer);
	}
	
	/*
	*	Edit jhoney.conf config file
	*/
	public void conf(BufferedWriter writer, String file, String[] params) {
		String par = getParam(params,"Help");
	
		if(params == null || par != null) {
			sendHeader("HTTP/1.1 200 OK",writer);
			sendTextfile("http/header.html",writer);
			
			HSProperties p = HSProperties.getInstance();
			p.load();
			Properties prop = p.getProperties();
			
			writeLine("<form enctype=\"multipart/form-data\">",writer);
			writeLine("<br><table width=300>",writer);
			
			writeLine("<tr><td colspan=3><p class=Subtitle>Honeypot deamon configuration</p></td></tr>",writer);
			
			writeLine("<tr>",writer);
			writeLine("  <td class=conf1 width=35%><p class=conf1>Ports</p></td><td class=conf2><input type=textfield name=Ports value=\""+prop.getProperty("Ports","139")+"\"></td><td class=conf2><div align=center><a href=\"conf_jh.html?Help=Ports\"><img src=help.png></a></div></td>",writer);
			writeLine("</tr>",writer);
			
			writeLine("<tr>",writer);
			writeLine("  <td class=conf1 width=35%><p class=conf1>Auto blacklisting</p></td><td class=conf2>",writer);
			if(p.useAutoBlacklist()) {
				writeLine("    <p class=conf2>On <INPUT TYPE=radio NAME=AutoBlacklist VALUE=true CHECKED>",writer);
				writeLine("    Off <INPUT TYPE=radio NAME=AutoBlacklist VALUE=false>",writer);
			}
			else {
				writeLine("    <p class=conf2>On <INPUT TYPE=radio NAME=AutoBlacklist VALUE=true>",writer);
				writeLine("    Off <INPUT TYPE=radio NAME=AutoBlacklist VALUE=false CHECKED>",writer);
			}
			writeLine("</td><td class=conf2><div align=center><a href=\"conf_jh.html?Help=AutoBlacklist\"><img src=help.png></a></div></td>",writer);
			writeLine("</tr>",writer);
			
			writeLine("<tr>",writer);
			writeLine("  <td class=conf1 width=35%><p class=conf1>Blacklist timeout</p></td><td class=conf2><input type=textfield name=BlacklistTimeout value=\""+prop.getProperty("BlacklistTimeout","600")+"\"></td><td class=conf2><div align=center><a href=\"conf_jh.html?Help=BlacklistTimeout\"><img src=help.png></a></div></td>",writer);
			writeLine("</tr>",writer);
			
			writeLine("<tr>",writer);
			writeLine("  <td colspan=3><input class=button id=saveconf type=submit value=\"Save\"/></td>",writer);
			writeLine("</tr>",writer);
			
			writeLine("</table>",writer);
			writeLine("</form>",writer);
			
			//Show help
			if(par != null) {
				writeLine("<br><table width=500><tr><td>",writer);
				if(par.equals("Ports")) {
					sendTextfile("help/ConfPorts.html",writer);
				}
				else if(par.equals("AutoBlacklist")) {
					sendTextfile("help/ConfAutobl.html",writer);
				}
				else if(par.equals("BlacklistTimeout")) {
					sendTextfile("help/ConfTimeout.html",writer);
				}
				writeLine("</td></tr></table>",writer);
			}
			//
			
			sendTextfile("http/footer.html",writer);
		}
		else {
			sendHeader("HTTP/1.1 200 OK",writer);
			sendTextfile("http/header.html",writer);
			
			HSProperties p = HSProperties.getInstance();
			boolean ok = p.store(params);
			
			writeLine("<br><table width=300><tr><td>",writer);
			if(ok) { writeLine("<p>Configuration saved without errors",writer); }
			else { writeLine("<p>Unable to save configuration",writer); }
			writeLine("</td></tr></table>",writer);
			
			sendTextfile("http/footer.html",writer);
		}
	}
}