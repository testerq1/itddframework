package jhoney;
import java.io.*;
import java.util.*;

/*
*	Contains the methods for JHServ Logviewer menu alternatives
*/
public class ServLogviewer extends ServBasics {
	
	public ServLogviewer() {
		
	}

	public void showDates(BufferedWriter writer, String[] params) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		String par = getParam(params,"Help");
		
		writeLine("<table width=500><tr><td>",writer);
		
		writeLine("<p class=Subtitle>Attacks log&nbsp;&nbsp;<a href=\"log_alert.html?Help=Alert\"><img src=help.png></a></p><p>",writer);
		
		try {
			Vector v = new Vector();
			BufferedReader reader = new BufferedReader(new FileReader("alert.log"));
			String line = reader.readLine();
			while(line != null) {
				StringTokenizer st = new StringTokenizer(line," ");
				String date = st.nextToken();
				date = date.substring(1,date.length());
				
				boolean found = false;
				for(int i=0;i<v.size();i++) {
					String tmp = (String)v.elementAt(i);
					if(tmp.equals(date)) found = true;	
				}
				if(!found) {
					v.addElement(date);
				}
			
				line = reader.readLine();
			}
			
			for(int i=0;i<v.size();i++) {
				String date = (String)v.elementAt(i);
				writeLine("<a href=log_alert.html?date="+date+">"+date+"</a><br>",writer);
			}
			if(v.size() > 0) {
				writeLine("<a href=log_alert.html?date=all>All</a><br>",writer);
			}
			else {
				writeLine("<p>Attack log is empty</p>",writer);
			}
			
		}
		catch(Exception err) {
			writeLine("<p><b>ERROR</b><br>",writer);
			writeLine("The file <I>alert.log</I> is missing. The file is generated the first time your computer is attacked.</p>",writer);
		}
		
		writeLine("</td></tr></table>",writer);
		
		//Show help
		if(par != null) {
			writeLine("<br><table width=500><tr><td>",writer);
			sendTextfile("help/LValert.html",writer);
			writeLine("</td></tr></table>",writer);
		}
		//
		
		sendTextfile("http/footer.html",writer);
	}
	
	public void attacklog(String date, BufferedWriter writer) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		writeLine("<table width=710><tr><td>",writer);
		
		if(date.equals("all")) {
			date = "";
		}
		
		writeLine("<p class=Subtitle>Registered attacks</p>",writer);
		
		writeLine("<table width=700 border=0 cellspacing=1>",writer);
		try {
			File file = new File("alert.log");
			long bytes = file.length();
			double mb = bytes / (double)1024 / (double)1024 * 100;
			mb = Math.round(mb);
			mb = mb / 100;
			
			writeLine("<tr><td colspan=5 class=filled><p class=menu>alert.log ("+mb+" Mb)</td></tr>",writer);
			
			BufferedReader reader = new BufferedReader(new FileReader("alert.log"));
			String line = reader.readLine();
			StringTokenizer t;
			int cnt = 25;
			while(line != null) {
				if(line.indexOf(date) > -1) {
					if(cnt == 25) {
						cnt = 0;
						writeLine("<tr><td class=filled><p class=menu>Time</td>",writer);
						writeLine("<td class=filled><p class=menu>Local Port</td>",writer);
						writeLine("<td class=filled><p class=menu>Intruder IP</td>",writer);
						writeLine("<td class=filled><p class=menu>Intruder Host</td></tr>",writer);
					}
				
					t = new StringTokenizer(line," ");
					writeLine("<tr><td><p>"+t.nextToken()+" "+t.nextToken()+"</td>",writer);
					writeLine("<td><p>"+t.nextToken()+"</td>",writer);
					writeLine("<td><p>"+t.nextToken()+"</td>",writer);
					writeLine("<td><p>"+t.nextToken()+"</td></tr>",writer);
					cnt++;
				}
				line = reader.readLine();
			}
			
			reader.close();
		}
		catch(Exception err) {
			writeLine("<tr><td><p><b>ERROR</b><br>",writer);
			writeLine("The file <I>alert.log</I> is missing. The file is generated the first time your computer is attacked.</td></tr>",writer);
		}
			
		writeLine("<tr><td colspan=5 class=filled><p></td></tr>",writer);
		writeLine("</table>",writer);
		
		writeLine("</td></tr></table>",writer);
			
		sendTextfile("http/footer.html",writer);
	}
	
	public void statuslog(BufferedWriter writer, String[] params) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		String par = getParam(params,"Help");
		
		writeLine("<table width=710><tr><td>",writer);
		
		writeLine("<p class=Subtitle>Deamon status/error log&nbsp;&nbsp;<a href=\"log_status.html?Help=Deamon\"><img src=help.png></a></p>",writer);
			
		writeLine("<table width=700 border=0 cellspacing=1>",writer);
		writeLine("<tr><td class=filled><p class=menu>jhoney.log</td></tr>",writer);
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader("jhoney.log"));
			String line = reader.readLine();
			while(line != null) {
				writeLine("<tr><td><p>"+line+"</td></tr>",writer);
				line = reader.readLine();
			}
			
			reader.close();
		}
		catch(Exception err) {
			writeLine("<tr><td><p><b>ERROR</b><br>",writer);
			writeLine("The file <I>jhoney.log</I> is missing. The file is generated the first time JHoney deamon is started.</td></tr>",writer);
		}
		
		writeLine("<tr><td class=filled><p></td></tr>",writer);
		writeLine("</table>",writer);
		
		writeLine("</td></tr></table>",writer);
		
		//Show help
		if(par != null) {
			writeLine("<br><table width=500><tr><td>",writer);
			sendTextfile("help/LVdeamon.html",writer);
			writeLine("</td></tr></table>",writer);
		}
		//
			
		sendTextfile("http/footer.html",writer);
	}
}