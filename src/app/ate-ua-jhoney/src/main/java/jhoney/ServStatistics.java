package jhoney;
import java.io.*;
import java.util.*;

/*
*	Contains the methods for JHServ Statistics menu alternatives
*/
public class ServStatistics extends ServBasics {
	//Declarations
	private int[] attacks;
	private int total;
	private String start;
	private String end;
	private String date;
	
	private Vector ports;
	
	private BufferedWriter writer;
	//
	
	public ServStatistics() {
		
	}

	public void listDates(BufferedWriter writer, String[] params) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		String par = getParam(params,"Help");
		
		writeLine("<table width=500><tr><td>",writer);
			
		writeLine("<p class=Subtitle>Attack statistics&nbsp;&nbsp;<a href=\"stat_list.html?Help=Stats\"><img src=help.png></a></p><p>",writer);
		
		try {	
			Vector v = new Vector();
			BufferedReader reader = new BufferedReader(new FileReader("alert.log"));
			String line = reader.readLine();
			while(line != null) {
				StringTokenizer st = new StringTokenizer(line," ");
				String date = st.nextToken();
				date = date.substring(1,date.length());
				
				boolean found = false;
				for(int i=0;i<v.size();i++) {
					String tmp = (String)v.elementAt(i);
					if(tmp.equals(date)) found = true;	
				}
				if(!found) {
					v.addElement(date);
				}
			
				line = reader.readLine();
			}
			
			for(int i=0;i<v.size();i++) {
				String date = (String)v.elementAt(i);
				writeLine("<a href=stat_view.html?date="+date+">"+date+"</a><br>",writer);
			}
			if(v.size() > 0) {
				writeLine("<a href=stat_view.html?date=all>All</a><br>",writer);
			}
			else {
				writeLine("<p>Attack log is empty</p>",writer);
			}
			
		}
		catch(Exception err) {
			writeLine("<p><b>ERROR</b><br>",writer);
			writeLine("The file <I>alert.log</I> is missing. The file is generated the first time your computer is attacked.</p>",writer);
		}
		writeLine("</td></tr></table>",writer);
		
		//Show help
		if(par != null) {
			writeLine("<br><table width=500><tr><td>",writer);
			sendTextfile("help/Stats.html",writer);
			writeLine("</td></tr></table>",writer);
		}
		//
			
		sendTextfile("http/footer.html",writer);
	}
	
	public void view(String date, BufferedWriter writer) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		try {	
			genstats(date,writer);
		}
		catch(Exception err) {
			writeLine("<p><b>ERROR</b><br>",writer);
			writeLine("The file <I>alert.log</I> is missing. The file is generated the first time your computer is attacked.</p>",writer);
		}
			
		sendTextfile("http/footer.html",writer);
	}
	
	/*
	*	all			-> Write statistics for all entries in the log
	*	YYMMDD		-> Write statistics for the specific day
	*	yesterday	-> Write statistics for yesterday
	*	today		-> Write statistics for today
	*/
	private void genstats(String date, BufferedWriter writer) throws Exception {
	
		attacks = null;
		total = 0;
		start = "";
		end = "";
	
		ports = new Vector();
	
		this.writer = writer;
		
		if(date.equals("yesterday")) {
			this.date = getTimeString("yesterday");
		}
		else if(date.equals("today")) {
			this.date = getTimeString("today");
		}
		else if(date.equals("all")) {
			this.date = "";
		}
		else {
			this.date = date;
		}
		
		readLog();
		
		writeStats();
	}
	
	/*
	*	Returns the current time in the format "YYYYMMDD HH:MM:SS"
	*/
	private String getTimeString(String param) {
		String time = "";
		
		Calendar c = Calendar.getInstance();
		
		if(param.equals("yesterday")) {
			c.setTimeInMillis(System.currentTimeMillis() - 86400000);
		}
		
		time += format(c.get(Calendar.YEAR));
		time += format(c.get(Calendar.MONTH));
		time += format(c.get(Calendar.DAY_OF_MONTH));
		
		return time;
	}
	
	/*
	*	Values < 10 is returned as "0v" (val=5 returns "05")
	*	Values >= 10 is not changed.
	*/
	private String format(int val) {
		String res = "";
		if(val < 10) {
			res = "0"+val;
		}
		else {
			res = ""+val;
		} 
		return res;
	}
	
	/*
	*	Adds a line to stat.html
	*/
	private void add(String line) {
		try {
			writer.write(line,0,line.length());
			writer.newLine();
		}
		catch(Exception err) {
			MsgLog.add("Response error: "+err.getMessage());
		}
	}
	
	/*
	*	Writes statistics after calculations from alert.log (performed in readLog() )
	*/
	private void writeStats() {
		add("<table width=500><tr><td>");
	
		add("<TABLE WIDTH=480 border=0><TR><TD class=filled>");
		add("<P class=menu>Attack statistics</P></TD></TR>");
		add("<TABLE WIDTH=400 border=0>");
		if(!date.equals("")) {
			add("<TR><TD><P>Date</TD><TD><P>"+date+"</TD></TR>");
		}
		add("<TR><TD><P>Time of first attack</TD><TD><P>"+start+"</TD></TR>");
		add("<TR><TD><P>Time of last attack</TD><TD><P>"+end+"</TD></TR>");
		add("<TR><TD><P>Total number of attacks</TD><TD><P>"+total+"</TD></TR>");
		add("</TABLE>");	
		add("<TABLE WIDTH=480 border=0 cellspacing=1>");
		add("<TR><TD class=filled colspan=4><P class=menu>Attacks per hour of day</TD></TR>");
		add("<TR><TD class=filled width=50><P class=menu>Hour</TD><TD class=filled width=60><P class=menu>Attacks</TD><TD class=filled width=70><P class=menu>Percent</TD><TD class=filled width=300><P class=menu>Graph<TD></TR>");
			
		int gwidth = 900;
			
		for(int i=0;i<attacks.length;i++) {
			String tmp = i+"";
			if(i < 10) tmp = "0"+i;
			
			add("<TR>");
			add("    <TD><P>"+tmp+"</TD>");
			add("    <TD><P>"+attacks[i]+"</TD>");
						
			
			double perc = (double)attacks[i] / (double)total * (double)100;
			perc = perc * (double)100;
			perc = Math.round(perc);
			perc = perc / (double)100;
			add("    <TD><P>"+perc+" %</TD>");
			
			int l = (int)Math.round( (double)gwidth*perc/(double)100 );
			if(l > 300) l = 300;
			
			if(l > 0) add("    <TD> <TABLE width="+l+" height=12><TR><TD class=stats></TD></TR></TABLE> </TD>");
			else add("    <TD> </TD>");
			add("</TR>");
		}
		
		add("</TABLE>");
		add("<TABLE WIDTH=480 border=0 cellspacing=1>");
		add("<TR><TD class=filled colspan=4><P class=menu>Attacks per port</TD></TR>");
		add("<TR><TD class=filled width=50><P class=menu>Port</TD><TD class=filled width=60><P class=menu>Attacks</TD><TD class=filled width=70><P class=menu>Percent</TD><TD class=filled width=300><P class=menu>Graph<TD></TR>");
		
		gwidth = 300;
		
		for(int i=0;i<ports.size();i++) {
			add("<TR>");
		
			PortElement p = (PortElement)ports.elementAt(i);
			add("    <TD><P>"+p.port+"</TD>");
			add("    <TD><P>"+p.attacks+"</TD>");
			
			double perc = (double)p.attacks / (double)total * (double)100;
			perc = perc * (double)100;
			perc = Math.round(perc);
			perc = perc / (double)100;
			add("    <TD><P>"+perc+" %</TD>");
			
			int l = (int)Math.round( (double)gwidth*perc/(double)100 );
			if(l > 0) add("    <TD> <TABLE width="+l+" height=12><TR><TD class=stats></TD></TR></TABLE> </TD>");
			else add("    <TD> </TD>");
			
			add("</TR>");
		}
		
		add("</TABLE>");
		
		add("</td></tr></table>");
	}
	
	/*
	*	Contains port/noAttacks for each port found in alert.log
	*/
	private class PortElement {
		public String port;
		public int attacks;
		
		public PortElement(String port, int attacks) {
			this.port = port;
			this.attacks = attacks;
		}
	}
	
	/*
	*	Parses a line from alert.log and update the PortElement Vector
	*/
	private void addPort(String line) {
		if(ports == null) {
			ports = new Vector();
		}
		
		StringTokenizer st = new StringTokenizer(line," ");
		st.nextToken();
		st.nextToken();
		String port_str = st.nextToken();
		boolean found = false;
		for(int i=0;i<ports.size();i++) {
			PortElement p = (PortElement)ports.elementAt(i);
			if(p.port.equals(port_str)) {
				p.attacks++;
				found = true;
			}
		}
		
		if(!found) {
			ports.add(new PortElement(port_str,1));
		}
	}
	
	/*
	*	Reads alert.log and calculates attack statistics
	*/
	private void readLog() throws Exception {
		try {
			attacks = new int[24];
			total = 0;
			
			BufferedReader reader = new BufferedReader(new FileReader("alert.log"));
			String line = reader.readLine();
			
			while(line.indexOf(date) == -1) {
				line = reader.readLine();
				
				if(line == null) {
					MsgLog.add("Response error: Date "+date+" not found in alert.log");
				}
			}
			
			StringTokenizer st = new StringTokenizer(line,"]");
			start = st.nextToken();
			start = start.substring(1,start.length());
			
			while(line != null) {
				total++;
				
				//Check ports stat
				addPort(line);
				//
								
				st = new StringTokenizer(line,"]");
				end = st.nextToken();
				end = end.substring(1,end.length());
			
				if(end.indexOf(" 00:") > -1) attacks[0]++;
				else if(end.indexOf(" 01:") > -1) attacks[1]++;
				else if(end.indexOf(" 02:") > -1) attacks[2]++;
				else if(end.indexOf(" 03:") > -1) attacks[3]++;
				else if(end.indexOf(" 04:") > -1) attacks[4]++;
				else if(end.indexOf(" 05:") > -1) attacks[5]++;
				else if(end.indexOf(" 06:") > -1) attacks[6]++;
				else if(end.indexOf(" 07:") > -1) attacks[7]++;
				else if(end.indexOf(" 08:") > -1) attacks[8]++;
				else if(end.indexOf(" 09:") > -1) attacks[9]++;
				else if(end.indexOf(" 10:") > -1) attacks[10]++;
				else if(end.indexOf(" 11:") > -1) attacks[11]++;
				else if(end.indexOf(" 12:") > -1) attacks[12]++;
				else if(end.indexOf(" 13:") > -1) attacks[13]++;
				else if(end.indexOf(" 14:") > -1) attacks[14]++;
				else if(end.indexOf(" 15:") > -1) attacks[15]++;
				else if(end.indexOf(" 16:") > -1) attacks[16]++;
				else if(end.indexOf(" 17:") > -1) attacks[17]++;
				else if(end.indexOf(" 18:") > -1) attacks[18]++;
				else if(end.indexOf(" 19:") > -1) attacks[19]++;
				else if(end.indexOf(" 20:") > -1) attacks[20]++;
				else if(end.indexOf(" 21:") > -1) attacks[21]++;
				else if(end.indexOf(" 22:") > -1) attacks[22]++;
				else if(end.indexOf(" 23:") > -1) attacks[23]++;
				
				line = reader.readLine();
				
				if(line != null) {
					if(line.indexOf(date) == -1) {
						reader.close();
						return;
					}
				}
			}
			reader.close();
		}
		catch(Exception err) {
			throw new Exception("Response error: Unable to read alert.log");
		}
	}
}