package jhoney;
import java.io.*;
import java.util.*;

/*
*	Contains the methods for JHServ Home menu alternative
*/
public class ServMain extends ServBasics {
	
	public ServMain() {
			
	}

	public void showMain(BufferedWriter writer, String username, String IP, String[] params) {
		sendHeader("HTTP/1.1 200 OK",writer);
		
		HSProperties p = HSProperties.getInstance();
		String par = getParam(params,"Theme");
		if(par != null) {
			Properties prop = p.getProperties();
			prop.setProperty("Theme",par);
			p.store(params);
			p.load();
		}
		
		sendTextfile("http/header.html",writer);
		
		writeLine("<br><table width=500><tr><td>",writer);
		
		writeLine("<p class=Subtitle>"+username+"@"+IP+"</p>",writer);
		writeLine("<p>Change Control Center theme<br>",writer);
		 
		File f = new File("http");
		File[] files = f.listFiles();
		for(int i=0;i<files.length;i++) {
			if(files[i].getName().endsWith(".css")) {
				String theme = files[i].getName().substring(0,files[i].getName().indexOf("."));
				if(!files[i].getName().equals(p.getTheme())) writeLine("<a href=index.html?Theme="+theme+">"+theme+"</a><br>",writer);
				else writeLine("<a href=index.html?Theme="+theme+">"+theme+" (current)</a><br>",writer);
			}
		}
		
		writeLine("</td></tr></table>",writer);
		
		sendTextfile("http/footer.html",writer);
		
	}
}