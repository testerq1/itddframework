package jhoney;
import java.util.*;
import java.io.*;
import java.net.URLDecoder;

/*
*	This class contains functions for reading the config file.
*/
public class HSProperties {
	//Declarations
	private static HSProperties instance;
	private Properties prop;
	//
	
	public static HSProperties getInstance() {
		if(instance == null) {
			instance = new HSProperties();
		}
		return instance;
	}
	
	/*
	*	Singleton class.
	*/
	private HSProperties() {
		load();
	}
	
	/**
	* Get raw Properties object
	*/
	public Properties getProperties() {
		return prop;
	}
	
	/**
	* Stores the params in the String[] to the config file
	*/
	public boolean store(String[] params) {
		boolean ok = false;
		
		try {
			for(int i=0;i<params.length-1;i+=2) {
				prop.setProperty(params[i],URLDecoder.decode(params[i+1],"UTF-8"));
			}
			prop.store(new FileOutputStream("jhoney.conf"),"");
			ok = true;
		}
		catch(Exception err) {
			ok = false;
		}
		
		return ok;
	}
	
	/*
	* Loads the config file
	*/
	public void load() {
		try {
			prop = new Properties();
			prop.load(new FileInputStream("jhoney.conf"));
		}
		catch(Exception err) {
			MsgLog.add("Cannot find jhoney.conf");
		}
	}
	
	/*
	* Returns the current theme
	*/
	public String getTheme() {
		String theme = prop.getProperty("Theme","Terminal");
		return theme+".css";
	}
	
	/*
	*	Returns the time an IP address is blacklisted.
	*/
	public int getBlacklistTimeout() {
		int to = 600;
		try {
			String tmp = prop.getProperty("BlacklistTimeout","600");
			to = Integer.parseInt(tmp);
		}
		catch(Exception err) {
			to = 600;
		}
		
		return to*1000;
	}
	
	/*
	*	Use automatic blacklisting
	*/
	public boolean useAutoBlacklist() {
		boolean use = false;
		
		String tmp = prop.getProperty("AutoBlacklist","true");
		if(tmp.equals("true")) {
			use = true;
		}
		
		return use;
	}
	
	/*
	*	Returns the ports to start servers at.
	*/
	public int[] getPorts() {
		int[] ports = null;
		String tmp = prop.getProperty("Ports","139");
		try {			
			StringTokenizer st = new StringTokenizer(tmp,":");
			if(st.hasMoreTokens()) {
				int no = st.countTokens();
				ports = new int[no];
				for(int i=0;i<no;i++) {
					ports[i] = Integer.parseInt(st.nextToken());
				}
			}
			else {
				ports = new int[1];
				ports[0] = Integer.parseInt(tmp);
			}
		}
		catch(Exception err) {
			ports = null;
		}
		return ports;
	}
	
	/*
	*	Returns the current time in the format "YYYYMMDD HH:MM:SS"
	*/
	public static String getTimeString() {
		String time = "";
		
		Calendar c = Calendar.getInstance();
		
		time += format(c.get(Calendar.YEAR));
		time += format(c.get(Calendar.MONTH)+1);
		time += format(c.get(Calendar.DAY_OF_MONTH));
		time += " ";
		time += format(c.get(Calendar.HOUR_OF_DAY))+":";
		time += format(c.get(Calendar.MINUTE))+":";
		time += format(c.get(Calendar.SECOND));
		
		return time;
	}
	
	/*
	*	Values < 10 is returned as "0v" (val=5 returns "05")
	*	Values >= 10 is not changed.
	*/
	public static String format(int val) {
		String res = "";
		if(val < 10) {
			res = "0"+val;
		}
		else {
			res = ""+val;
		} 
		return res;
	}
}