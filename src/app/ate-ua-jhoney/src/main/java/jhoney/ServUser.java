package jhoney;
import java.io.*;
import java.util.*;

/*
*	Handles JHServ user logins
*/
public class ServUser extends ServBasics {
	
	private Vector sessions;
	private final String userdatafile = "users";
	private final int sessionTimeoutSec = 300;

	/**
	* User sessions object
	*/
	private class Session {
		public String IP;
		public String username;
		public long lastAccess;
		
		public Session(String IP, String username) {
			this.IP = IP;
			this.username = username;
			this.lastAccess = System.currentTimeMillis();
		}
	}
	
	public ServUser() {
		sessions = new Vector();
	}
	
	/**
	* Shows the login page
	*/
	public void login(BufferedWriter writer) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/login.html",writer);
		sendTextfile("http/footer.html",writer);
	}
	
	/**
	* Gets the username for IP
	*/
	public String getUsername(String IP) {
		for(int i=0;i<sessions.size();i++) {
			Session s = (Session)sessions.elementAt(i);
			if(s.IP.equals(IP)) {
				return s.username;
			}
		}
		return "";
	}
	
	/**
	* Returns true if the user at IP is logged in
	*/
	public boolean isLoggedIn(String IP) {
		for(int i=0;i<sessions.size();i++) {
			Session s = (Session)sessions.elementAt(i);
			if(s.IP.equals(IP)) {
				long diff = System.currentTimeMillis() - s.lastAccess;
				if(diff < sessionTimeoutSec * 1000) {
					s.lastAccess = System.currentTimeMillis();
					return true;
				}
				else {
					logout(IP);
					return false;
				}
			}
		}
		return false;
	}
	
	public void logout(String IP) {
		for(int i=0;i<sessions.size();i++) {
			Session s = (Session)sessions.elementAt(i);
			if(s.IP.equals(IP)) {
				sessions.removeElementAt(i);
				break;
			}
		}
	}
	
	/**
	* Checks username and password
	*/
	public boolean checkLogin(String un, String pw, BufferedWriter writer, String IP) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(userdatafile));
			
			String line = reader.readLine();
			while(line != null) {
				StringTokenizer st = new StringTokenizer(line,":");
				String myun = st.nextToken();
				String mypw = st.nextToken();
				
				if(un.equals(myun) && pw.equals(mypw)) {
					sessions.addElement(new Session(IP,un));
					reader.close();
					return true;
				}
				
				line = reader.readLine();
			}
			
			reader.close();
			return false;
		}
		catch(Exception err) {
			return false;
		}
	}
	
	public void addUser(String un, String pw) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(userdatafile,true));
			String line = un+":"+pw;
			writer.write(line,0,line.length());
			writer.newLine();
			writer.close();
		}
		catch(Exception err) {
			MsgLog.add("Unable to add user "+un);
		}
	}
	
	public void removeUser(String un, String pw) {
		try {
			Vector users = new Vector();
		
			BufferedReader reader = new BufferedReader(new FileReader(userdatafile));
			String line = reader.readLine();
			while(line != null) {
				StringTokenizer st = new StringTokenizer(line,":");
				String myun = st.nextToken();
				String mypw = st.nextToken();
				
				if(un.equals(myun) && pw.equals(mypw)) {
					//Removed from user list
				}
				else {
					users.addElement(myun+":"+mypw);
				}
				
				line = reader.readLine();
			}
			reader.close();
		
			BufferedWriter writer = new BufferedWriter(new FileWriter(userdatafile,false));
			for(int i=0;i<users.size();i++) {
				line = (String)users.elementAt(i);
				writer.write(line,0,line.length());
				writer.newLine();
			}
			writer.close();
		}
		catch(Exception err) {
			MsgLog.add("Unable to add user "+un);
		}
	}
}