package jhoney;
import java.io.*;
import java.net.*;

/*
*	This class starts a server at the selected port and if anyone connects to the
*	port their IP address is logged and added to the blacklist buffer.
*/
public class ServerThread implements Runnable {
	//Declarations
	public int port;
	public boolean running = false;
	private HSProperties prop;
	
	private BlacklistBuffer buffer;
	
	private ServerSocket server;
	//
	
	/*
	*	Starts a server at port 'port'.
	*/
	public ServerThread(int port) throws Exception {
		this.port = port;
		
		prop = HSProperties.getInstance();
		buffer = BlacklistBuffer.getInstance();
		
		Thread thr = new Thread(this);
		thr.start();
 
		try {
			Thread.sleep(500);
		}
		catch(Exception err) {}
		
		if(!running) {
			throw new Exception("Unable to start server at port "+port);
		}
	}
	
	public boolean stop() {
		try {
			server.close();
			running = false;
			return true;
		}
		catch(Exception err) {
			return false;
		}
	}
	
	/*
	*	Server thread.
	*	1. Starts ServerSocket
	*	2. Logs all connection attempts
	*	3. Adds all connection IPs to the blacklist buffer
	*/
	public void run() {
		running = true;
		
		try {
			server = new ServerSocket(port);
		}
		catch(Exception err) {
			MsgLog.add("Unable to start server at port "+port);
			running = false;
		}
			
		while(running) {
			
			try {
				Socket sock = server.accept();
				
				InetAddress adr = sock.getInetAddress();
				String remoteip = adr.getHostAddress();
				String remotehost = adr.getHostName();
				String localport = sock.getLocalPort()+"";
				
				alert(remoteip,remotehost,localport);
				
				sock.close();
					
				Thread.sleep(1000);
								
			}
			catch(Exception err) {
				MsgLog.add("Server closed at port "+port);
				running = false;
			}
		}
	}
	
	/*
	*	1. Adds the IP to the blacklist buffer
	*	2. Writes remoteip, remotehost, localport and currenttime to the alert.log	
	*/
	private void alert(String remoteip, String remotehost, String localport) {
		
		long diff = buffer.add(remoteip);
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter("alert.log",true));
			String time = HSProperties.getTimeString();
			
			String line = "["+time+"] "+localport+" "+remoteip+" "+remotehost;
			
			if( diff >= 1500) {
				writer.write(line,0,line.length());
				writer.newLine();
			}
			
			writer.close();
		}
		catch(Exception err) {
			MsgLog.add("Unable to access alert.log");
		}
	}
}