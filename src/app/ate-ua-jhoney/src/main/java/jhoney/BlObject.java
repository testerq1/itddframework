package jhoney;
import java.io.*;

/*
*	This class represents a blacklist IP entry in the blacklist file.
*/
public class BlObject implements Serializable {
	//Declarations
	public String ip;
	public long timestamp;
	public String timestr;
	//
	
	public BlObject(String ip) {
		this.ip = ip;
		timestamp = System.currentTimeMillis();
		timestr = HSProperties.getTimeString();
	}
}