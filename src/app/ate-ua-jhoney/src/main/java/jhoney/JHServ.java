package jhoney;
import java.net.*;
import java.io.*;
import java.util.*;

public class JHServ extends ServBasics implements Runnable {
	//Declarations
	private boolean running;
	private ServUser user;
	private ServConfiguration configuration;
	private ServStatistics statistics;
	private ServLogviewer logviewer;
	private ServCleanup cleanup;
	private ServBlacklist blacklist;
	private ServDeamon deamon;
	private ServMain main;
	
	private BufferedWriter writer;
	private OutputStream out;
	
	private final String[] filetypes = {
		".html",	"HTML",
		".htm",		"HTML",
		"/",		"HTML",
		".css",		"TEXT",
		".ico",		"BINARY",
		".jpg",		"BINARY",
		".gif",		"BINARY",
		".png",		"BINARY"
	};
	//
	
	public static void main(String[] args) {
		if(args.length == 3) {
			if(args[0].equals("adduser")) {
				ServUser user = new ServUser();
				user.addUser(args[1],args[2]);
			}
			else if(args[0].equals("removeuser")) {
				ServUser user = new ServUser();
				user.removeUser(args[1],args[2]);
			}
			else {
				JHServ serv = new JHServ();
			}
		}
		else {
			JHServ serv = new JHServ();
		}
	}
	
	public void adduUser(String name,String passwd){
	    user.addUser(name,passwd);
	}
	
	public void removeUser(String name,String passwd){
	    user.removeUser(name,passwd);
	}
	
	public JHServ() {
		user = new ServUser();
		configuration = new ServConfiguration();
		statistics = new ServStatistics();
		logviewer = new ServLogviewer();
		cleanup = new ServCleanup();
		blacklist = new ServBlacklist();
		deamon = new ServDeamon();
		main = new ServMain();
	}
	
	public void stopServer(){
	    running=false;
	}
	
	public void run() {
		try {
			ServerSocket server = new ServerSocket(8333);
			running = true;
			
			while(running) {
				Socket socket = server.accept();
				
				String IP = socket.getInetAddress().getHostAddress();
				
				BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				parseRequest(reader);
				
				writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
				out = socket.getOutputStream();
				
				get(request,IP);
				writer.close();
				reader.close();
			}
		}
		catch(Exception err) {
			MsgLog.add("HTTP Server error: "+err.getMessage());
			err.printStackTrace();
			running = false;
		}
	}
	
	/**
	* Returns true if the requested file is of the filetype 'type'
	* Type is "HTML", "TEXT" or "BINARY"
	*/
	private boolean isOfType(String file, String type) {
		for(int i=0;i<filetypes.length-1;i+=2) {
			if(filetypes[i+1].equals(type)) {
				if(file.endsWith(filetypes[i])) {
					return true;
				}
			}
		}
		return false;
	}
	
	private void get(String req, String IP) {
		String[] params = parseParams(req);
		String file = parseFile(req);
		
		//Check if login is required
		if(!user.isLoggedIn(IP)) {
			String p = getParam(params,"action");
			if(p == null) {
				if(isOfType(file,"HTML")) { //Send login.html if a HTML page is requested and the user is not logged in
					user.login(writer);
					return;
				}
				else { //If not HTML page is requested, the page is sent even if the user is not logged in
					sendHeader("HTTP/1.1 200 OK",writer);
					sendTextfile("http"+file,writer);
				}
			}
			else {
				if(user.checkLogin(getParam(params,"username"),getParam(params,"password"),writer,IP)) {
					//If user login is correct, do nothing. Continue
				}
				else {
					//User is incorrect. Show denied.html
					sendHeader("HTTP/1.1 200 OK",writer);
					sendTextfile("http/denied.html",writer);
					sendTextfile("http/footer.html",writer);
					return;
				}
			}
		}
		//
		
		//
		// User is logged in. Send all requests
		//
		if(file.equals("/") || file.equals("/index.html")) {
			main.showMain(writer,user.getUsername(IP),IP,params);
		}
		else if(file.equals("/logout.html")) {
			if(params == null) {
				user.logout(IP);
				user.login(writer);
			}
			else {
				main.showMain(writer,user.getUsername(IP),IP,params);
			}
		}
		else if(isOfType(file,"TEXT")) {
			File f = new File("http"+file);
			if(f.exists()) {
				sendHeader("HTTP/1.1 200 OK",writer);
				sendTextfile("http"+file,writer);
			}
			else {
				notFound(writer,file);
			}
		}
		else if(isOfType(file,"BINARY")) {
			File f = new File("http"+file);
			if(f.exists()) {
				sendBinaryfile("http"+file,out);
			}
			else {
				if(!file.equals("/favicon.ico")) MsgLog.add("Requested file "+file+" not found");
			}
		}
		else if(file.equals("/help.html")) {
			String par = getParam(params,"Help");
			if(par != null) {
				showHelp(writer,par);
			}
			else {
				//notFound(writer,file);
				main.showMain(writer,user.getUsername(IP),IP,params);
			}
		}
		else if(file.equals("/jh_start.html")) {
			deamon.start(writer);
		}
		else if(file.equals("/jh_status.html")) {
			deamon.status(writer);
		}
		else if(file.equals("/jh_stop.html")) {
			deamon.stop(writer);
		}
		else if(file.equals("/jh_restart.html")) {
			deamon.restart(writer);
		}
		else if(file.equals("/db_list.html")) {
			blacklist.db(writer,params);
		}
		else if(file.equals("/db_blacklist.html")) {
			blacklist.FW(writer,params);
		}
		else if(file.equals("/log_alert.html")) {
			String par = getParam(params,"Help");
			if(params == null || par != null) logviewer.showDates(writer,params);
			else logviewer.attacklog(params[1],writer);
		}
		else if(file.equals("/clean_alert.html")) {
			String par = getParam(params,"Help");
			if(params == null || par != null) cleanup.showDates(writer,deamon.running(),params);
			else cleanup.attacklog(params,writer);
		}
		else if(file.equals("/log_status.html")) {
			logviewer.statuslog(writer,params);
		}
		else if(file.equals("/stat_list.html")) {
			statistics.listDates(writer,params);
		}
		else if(file.equals("/stat_view.html")) {
			statistics.view(params[1],writer);
		}
		else if(file.equals("/conf_jh.html")) {
			configuration.conf(writer,file,params);
		}
		else if(file.startsWith("/conf_allow.html")) {
			configuration.FW_Allow(writer,file,params);
		}
		else if(file.startsWith("/conf_drop.html")) {
			configuration.FW_Drop(writer,file,params);
		}
		else if(file.startsWith("/conf_list.html")) {
			configuration.FW_Blacklist(writer,file,params);
		}
		else {
			notFound(writer,file);
		}
	}
	
	private void showHelp(BufferedWriter writer, String help) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		writeLine("<br><table width=500><tr><td>",writer);
		if(help.equals("Deamon")) sendTextfile("help/Deamon.html",writer);
		writeLine("</td></tr></table>",writer);
		
		sendTextfile("http/footer.html",writer);
	}
	
	private void notFound(BufferedWriter writer,String file) {
		sendHeader("HTTP/1.1 404 Not Found",writer);
		sendTextfile("http/404.html",writer);
	}
}