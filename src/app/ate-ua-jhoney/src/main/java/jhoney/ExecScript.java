package jhoney;
import java.util.*;
import java.io.*;

/*
*	Executes the allow or drop scripts to blacklist, allow traffic from an IP address.
*	The following command can be used:
*	exec(command)						Executes 'command'
*	AppendToFile(file,line)				Appends 'line' to file 'file'
*	AddToFile(file,line,beforeline)		Adds 'line' to file 'file' before 'beforeline'
*	RemoveFromFile(file,line)			Removes 'line' from file 'file'
*	ReadFile(file)						Reads the file 'file' to output
*/
public class ExecScript {
	//Declarations
	private String scriptfile;
	private String IP;
	public Vector output;
	//
	
	/*
	*	Executes the script 'scriptfile'. IP is a global variable and is referred to as $IP in the script
	*/
	public ExecScript(String scriptfile, String IP) {
		this.scriptfile = scriptfile;
		this.IP = IP;
		this.output = new Vector();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(scriptfile));
			String line = reader.readLine();
			while(line != null) {
				if(!line.startsWith("#")) {
					parseCmd(line);
				}
				line = reader.readLine();
			}
			reader.close();
		}
		catch(Exception err) {
			MsgLog.add("Unable to read script '"+scriptfile+"'");
		}
	}
	
	/**
	*	Parses a line from the script
	*/
	private void parseCmd(String line) {
		if(line.equals("")) return;
		if(line.startsWith(" ")) return;
		
		try {
			String cmd = line.substring(0,line.indexOf("("));
			String param = line.substring(line.indexOf("(")+1,line.indexOf(")"));
			
			if(cmd.equals("exec")) {
				exec(param);
			}
			else if(cmd.equals("AppendToFile")) {
				append(param);
			}
			else if(cmd.equals("RemoveFromFile")) {
				remove(param);
			}
			else if(cmd.equals("AddToFile")) {
				add(param);
			}
			else if(cmd.equals("ReadFile")) {
				read(param);
			}
			else {
				MsgLog.add("Error in script '"+scriptfile+"': Unknown command '"+cmd+"'");
			}
		}
		catch(Exception err) {
			MsgLog.add("Error in script '"+scriptfile+"': Parse error in line '"+line+"'");
		}
	}
	
	/*
	*	Reads the file 'file' to output
	*/
	private void read(String param) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(param));
			String line = reader.readLine();
			while(line != null) {
				output.add(line);
				line = reader.readLine();
			}
			reader.close();
		}
		catch(Exception err) {
			MsgLog.add("Error reading from file '"+param+"'");
		}
	}
	
	/**
	*	Adds the line 'line' to file 'file' before 'beforeline'
	*/
	private void add(String param) {
		try {
			StringTokenizer st = new StringTokenizer(param,",");
			String filename = st.nextToken();
			String info = st.nextToken();
			String beforeline = st.nextToken();
			
			st = new StringTokenizer(info," ");
			String cmd = "";
			while(st.hasMoreTokens()) {
				String tmp = st.nextToken();
				if(tmp.equals("$IP")) {
					cmd += IP+" ";
				}
				else {
					cmd += tmp+" ";
				}
			}
			cmd = cmd.trim();
			
			BufferedWriter writer = new BufferedWriter(new FileWriter("tmp"));
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String line = reader.readLine();
			while(line != null) {
				if(!line.equals(beforeline)) {
					writer.write(line,0,line.length());
					writer.newLine();
				}
				else {
					writer.write(cmd,0,cmd.length());
					writer.newLine();
					writer.write(beforeline,0,beforeline.length());
					writer.newLine();
				}
				line = reader.readLine();
			}
			reader.close();
			writer.close();
			
			File source = new File("tmp");
			File dest = new File(filename);
			dest.delete();
			source.renameTo(dest);
		}
		catch(Exception err) {
			MsgLog.add("Error adding to file '"+param+"'");
		}
	}
	
	/**
	*	Appends the line 'line' to the file 'file'
	*/
	private void append(String param) {
		try {
			StringTokenizer st = new StringTokenizer(param,",");
			String filename = st.nextToken();
			String line = st.nextToken();
			
			st = new StringTokenizer(line," ");
			String cmd = "";
			while(st.hasMoreTokens()) {
				String tmp = st.nextToken();
				if(tmp.equals("$IP")) {
					cmd += IP+" ";
				}
				else {
					cmd += tmp+" ";
				}
			}
			cmd = cmd.trim();
			
			BufferedWriter writer = new BufferedWriter(new FileWriter(filename,true));
			writer.write(cmd,0,cmd.length());
			writer.newLine();
			writer.close();
		}
		catch(Exception err) {
			MsgLog.add("Error appending to file '"+param+"'");
		}
	}
	
	/*
	*	Removes the line 'line' from the file 'file'
	*/
	private void remove(String param) {
		try {
			StringTokenizer st = new StringTokenizer(param,",");
			String filename = st.nextToken();
			String info = st.nextToken();
			
			st = new StringTokenizer(info," ");
			String cmd = "";
			while(st.hasMoreTokens()) {
				String tmp = st.nextToken();
				if(tmp.equals("$IP")) {
					cmd += IP+" ";
				}
				else {
					cmd += tmp+" ";
				}
			}
			cmd = cmd.trim();
			
			BufferedWriter writer = new BufferedWriter(new FileWriter("tmp"));
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String line = reader.readLine();
			while(line != null) {
				if(!line.equals(cmd)) {
					writer.write(line,0,line.length());
					writer.newLine();
				}
				line = reader.readLine();
			}
			reader.close();
			writer.close();
			
			File source = new File("tmp");
			File dest = new File(filename);
			dest.delete();
			source.renameTo(dest);
		}
		catch(Exception err) {
			MsgLog.add("Error removing from file '"+param+"'");
			err.printStackTrace();
		}
	}
	
	/*
	*	Executes the command 'param'
	*/
	private void exec(String param) {
		try {
			StringTokenizer st = new StringTokenizer(param," ");
			String cmd = "";
			while(st.hasMoreTokens()) {
				String tmp = st.nextToken();
				if(tmp.equals("$IP")) {
					cmd += IP+" ";
				}
				else {
					cmd += tmp+" ";
				}
			}
			cmd = cmd.trim();
			
			Runtime rt = Runtime.getRuntime();
			Process p = rt.exec(cmd);
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = reader.readLine();
			while(line != null) {
				output.add(line);
				line = reader.readLine();
			}
			reader.close();
		}
		catch(Exception err) {
			MsgLog.add("Error executing command '"+param+"'");
		}
	}
}