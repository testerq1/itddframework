package jhoney;
import java.util.Vector;

/**
*	This class opens a thread for each user selected port.
*/
public class HoneyMain {
	
	private Vector threads;
	private int cnt;
	private int no;
	
	public HoneyMain() throws Exception {
		MsgLog.add("Deamon started");
		
		HSProperties prop = HSProperties.getInstance();
		prop.load();
		int[] ports = prop.getPorts();
		cnt = 0;
		no = ports.length;
		
		if(ports != null) {
			threads = new Vector();
			
			for(int i=0;i<ports.length;i++) {
				MsgLog.add("Starting server at port "+ports[i]);
				try {
					ServerThread sth = new ServerThread(ports[i]);
					threads.addElement(sth);
				}
				catch(Exception err) {
					cnt++;
				}
			}
		}
		else {
			throw new Exception("No server ports specifiec in config file");
		}
	}
	
	public void errors() throws Exception {
		if(cnt != 0) {
			if(cnt != no) throw new Exception("Warning: Unable to open "+cnt+" of "+no+" ports");
			else throw new Exception("Error: Unable to start server deamon");
		}
	}
	
	public boolean stop() {
		boolean ok = true;
		for(int i=0;i<threads.size();i++) {
			ServerThread sth = (ServerThread)threads.elementAt(i);
			boolean tmp = sth.stop();
			if(!tmp) {
				ok = false;
			}
		}
		return ok;
	}
	
	public String getPorts() {
		String ports_str = "";
		for(int i=0;i<threads.size()-1;i++) {
			ServerThread sth = (ServerThread)threads.elementAt(i);
			ports_str += sth.port+", ";
		}
		ServerThread sth = (ServerThread)threads.elementAt(threads.size()-1);
		ports_str += sth.port+"";
		
		return ports_str;
	}
}