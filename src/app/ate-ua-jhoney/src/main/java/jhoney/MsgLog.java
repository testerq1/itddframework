package jhoney;
import java.io.*;

/*
*	This class writes logmessages to the application log.
*/
public class MsgLog {
	
	/*
	*	Logs a new message.
	*/
	public static void add(String msg) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter("jhoney.log",true));
			String time = HSProperties.getTimeString();
			
			String line = "["+time+"] "+msg;
			
			writer.write(line,0,line.length());
			writer.newLine();
			
			writer.close();
		}
		catch(Exception err) {
			System.out.println("JHoney FATAL: Cannot access jhoney.log");		
		}
	}
}