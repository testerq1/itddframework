package jhoney;
import java.io.*;
import java.util.*;

/*
*	Contains the methods for JHServ Blacklist menu alternatives
*/
public class ServBlacklist extends ServBasics {
	
	public ServBlacklist() {
		
	}
	
	public void FW(BufferedWriter writer, String[] params) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
			
		String par = getParam(params,"Help");
		
		writeLine("<table width=500><tr><td>",writer);
		writeLine("<p class=Subtitle>Firewall dynamic blacklist entries&nbsp;&nbsp;<a href=\"db_blacklist.html?Help=Blacklist\"><img src=help.png></a></p>",writer);
			
		writeLine("<table width=700 border=0 cellspacing=1>",writer);
		writeLine("<tr><td class=filled><p></td></tr>",writer);
		
		ExecScript es = new ExecScript("blacklist","none");
		Vector v = es.output;
		for(int i=0;i<v.size();i++) {
			String line = (String)v.elementAt(i);
			writeLine("<tr><td><p>"+line+"</td></tr>",writer);
		}
		
		writeLine("<tr><td class=filled><p></td></tr>",writer);
		writeLine("</table>",writer);
		
		writeLine("</td></tr></table>",writer);
		
		//Show help
		if(par != null) {
			writeLine("<br><table width=650><tr><td>",writer);
			sendTextfile("help/DbBlacklist.html",writer);
			writeLine("</td></tr></table>",writer);
		}
		//
			
		sendTextfile("http/footer.html",writer);
	}
	
	public void db(BufferedWriter writer, String[] params) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		String par = getParam(params,"Help");
		
		writeLine("<table width=500><tr><td>",writer);
		
		writeLine("<p class=Subtitle>Current blacklisted IP's&nbsp;&nbsp;<a href=\"db_list.html?Help=Blacklist\"><img src=help.png></a></p>",writer);
			
		writeLine("<table width=400 border=0 cellspacing=1>",writer);
		writeLine("<tr><td class=filled width=45%><p class=menu>IP</td><td class=filled width=55%><p class=menu>Time</td></tr>",writer);
		
		try {	
			ObjectInputStream reader = new ObjectInputStream(new FileInputStream("blacklist.db"));
			Vector v = (Vector)reader.readObject();
			reader.close();
			
			for(int i=0;i<v.size();i++) {
				BlObject obj = (BlObject)v.elementAt(i);
				writeLine("<tr><td><p>"+obj.ip+"</td><td><p>"+obj.timestr+"</td></tr>",writer);
			}
		}
		catch(Exception err) {
			writeLine("<tr><td><p><b>ERROR</b><br>",writer);
			writeLine("The file <I>blacklist.db</I> is missing. The file is generated the first time your computer is attacked.</td><td></td></tr>",writer);	
		}
			
		writeLine("<tr><td class=filled><p></td><td class=filled><p></td></tr>",writer);
		writeLine("</table>",writer);
		
		writeLine("</td></tr></table>",writer);
		
		//Show help
		if(par != null) {
			writeLine("<br><table width=650><tr><td>",writer);
			sendTextfile("help/DbList.html",writer);
			writeLine("</td></tr></table>",writer);
		}
		//
			
		sendTextfile("http/footer.html",writer);
	}
}