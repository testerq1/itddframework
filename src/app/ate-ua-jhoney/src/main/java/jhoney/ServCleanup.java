package jhoney;
import java.io.*;
import java.util.*;

/*
*	Contains the methods for JHServ Log cleanup menu alternatives
*/
public class ServCleanup extends ServBasics {
	//Declarations
	private String[] params;
	//
	
	public ServCleanup() {
		
	}

	public void showDates(BufferedWriter writer, boolean running, String[] params) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		String par = getParam(params,"Help");
		
		writeLine("<table width=500><tr><td>",writer);
		
		writeLine("<p class=Subtitle>Cleanup attack log&nbsp;&nbsp;<a href=\"clean_alert.html?Help=Alert\"><img src=help.png></a></p>",writer);
		
		if(!running) {
			try {
				Vector v = new Vector();
				BufferedReader reader = new BufferedReader(new FileReader("alert.log"));
				String line = reader.readLine();
				while(line != null) {
					StringTokenizer st = new StringTokenizer(line," ");
					String date = st.nextToken();
					date = date.substring(1,date.length());
					
					boolean found = false;
					for(int i=0;i<v.size();i++) {
						String tmp = (String)v.elementAt(i);
						if(tmp.equals(date)) found = true;	
					}
					if(!found) {
						v.addElement(date);
					}
				
					line = reader.readLine();
				}
				
				writeLine("<form enctype=\"multipart/form-data\"><p>",writer);
				for(int i=0;i<v.size();i++) {
					String date = (String)v.elementAt(i);
					writeLine("<input type=checkbox name=date value="+date+">"+date+"<br>",writer);
				}
				if(v.size() == 0) {
					writeLine("<p>Attack log is empty</p>",writer);
				}
				
				writeLine("<br><input class=button id=saveconf type=submit value=\"Cleanup\"/>",writer);
				writeLine("</form>",writer);
			}
			catch(Exception err) {
				writeLine("<p><b>ERROR</b><br>",writer);
				writeLine("The file <I>alert.log</I> is missing. The file is generated the first time your computer is attacked.</p>",writer);
			}
		}
		else {
			writeLine("<p><b>ERROR</b><br>",writer);
				writeLine("<p>The JHoney deamon must be stopped before<br>",writer);
				writeLine("a cleanup of the attacks log can be performed.</p>",writer);
		}
		
		writeLine("</td></tr></table>",writer);
		
		//Show help
		if(par != null) {
			writeLine("<br><table width=500><tr><td>",writer);
			sendTextfile("help/CleanAlert.html",writer);
			writeLine("</td></tr></table>",writer);
		}
		//
		
		sendTextfile("http/footer.html",writer);
	}
	
	private boolean add(String line) {
		boolean result = true;
		for(int i=0;i<params.length-1;i+=2) {
			if(line.indexOf(params[i+1]) > -1) {
				result = false;
				break;
			}
		}
		return result;
	}
	
	public void attacklog(String[] params, BufferedWriter writer) {
		this.params = params;
		
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		writeLine("<table width=500><tr><td>",writer);
		
		try {
			//Rewrite alert.log
			BufferedReader logr = new BufferedReader(new FileReader("alert.log"));
			BufferedWriter logw = new BufferedWriter(new FileWriter("alert.log.2"));
			
			String line = logr.readLine();
			while(line != null) {
				if( add(line) ) {
					logw.write(line,0,line.length());
					logw.newLine();
				}
				line = logr.readLine();
			}
			logr.close();
			logw.close();
			//
			
			//Delete old file and change name of new
			File old = new File("alert.log");
			boolean del = old.delete();
			if(!del) {
				throw new Exception("Error deleting old log file");
			}
			else {
				File source = new File("alert.log.2");
				File dest = new File("alert.log");
				source.renameTo(dest);
			}
			//
			
			writeLine("<P>Cleanup attacks log performed successfully</p>",writer);
		}
		catch(Exception err) {
			writeLine("<tr><td><p><b>ERROR</b><br>",writer);
			writeLine("Unable to cleanup attacks log: "+err.getMessage()+"</td></tr>",writer);
		}
		
		writeLine("</td></tr></table>",writer);
			
		sendTextfile("http/footer.html",writer);
	}
}