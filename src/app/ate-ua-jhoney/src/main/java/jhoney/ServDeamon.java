package jhoney;
import java.io.*;
import java.util.*;

/*
*	Contains the methods for JHServ Deamon menu alternatives
*/
public class ServDeamon extends ServBasics {
	
	private HoneyMain main;

	public ServDeamon() {
		
	}
	
	public boolean running() {
		if(main == null) return false;
		else return true;
	}
	
	public void stop(BufferedWriter writer) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		writeLine("<br><table width=500><tr><td>",writer);
			
		writeLine("<table width=400 border=0 cellspacing=1>",writer);
		writeLine("<tr><td class=filled width=80%><p class=menu>Action</td><td class=filled width=10%><p class=menu>Status</td><td class=filled width=10%></td></tr>",writer);
			
		writeLine("<tr><td><p>Stopping JHoney deamon</td>",writer);
		
		boolean ok = false;
		if(running()) ok = main.stop();
		main = null;
		if(ok) {
			MsgLog.add("Deamon stopped");
			writeLine("<td class=ok></td>",writer);
		}
		else {
			writeLine("<td class=error></td>",writer);
		}
		writeLine("<td><div align=center><a href=\"help.html?Help=Deamon\"><img src=help.png></a></div></td></tr>",writer);
		
		writeLine("</table>",writer);
		
		writeLine("</td></tr></table>",writer);
			
		sendTextfile("http/footer.html",writer);
	}
	
	public void start(BufferedWriter writer) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		writeLine("<br><table width=500><tr><td>",writer);
			
		writeLine("<table width=400 border=0 cellspacing=1>",writer);
		writeLine("<tr><td class=filled width=80%><p class=menu>Action</td><td class=filled width=10%><p class=menu>Status</td><td class=filled width=10%></td></tr>",writer);
			
		writeLine("<tr><td><p>Check if deamon is running</td>",writer);
		if(running()) {
			writeLine("<td class=ok></td>",writer);
		}
		else {
			writeLine("<td class=error></td>",writer);
		}
		writeLine("<td><div align=center><a href=\"help.html?Help=Deamon\"><img src=help.png></a></div></td></tr>",writer);
			
		if(!running()) {
			//Emtpy msg/err log
			File f = new File("jhoney.log");
			f.delete();
			//
			
			writeLine("<tr><td><p>Starting JHoney deamon</td>",writer);
			try {
				main = new HoneyMain();
				main.errors();
				writeLine("<td class=ok></td>",writer);
			}
			catch(Exception err) {
				if(err.getMessage().startsWith("Error")) {
					writeLine("<td class=error></td>",writer);
					main = null;
				}
				else {
					writeLine("<td class=okwitherrors></td>",writer);
				}
			}
			writeLine("<td><div align=center><a href=\"help.html?Help=Deamon\"><img src=help.png></a></div></td></tr>",writer);
		}
		
		writeLine("</table>",writer);
		
		writeLine("</td></tr></table>",writer);
			
		sendTextfile("http/footer.html",writer);
	}
	
	public void restart(BufferedWriter writer) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		writeLine("<br><table width=500><tr><td>",writer);
			
		writeLine("<table width=400 border=0 cellspacing=1>",writer);
		writeLine("<tr><td class=filled width=80%><p class=menu>Action</td><td class=filled width=10%><p class=menu>Status</td><td class=filled width=10%></td></tr>",writer);
			
		writeLine("<tr><td><p>Stopping JHoney deamon</td>",writer);
		boolean ok = false;
		if(running()) ok = main.stop();
		main = null;
		if(ok) {
			MsgLog.add("Deamon stopped");
			writeLine("<td class=ok></td>",writer);
		}
		else {
			writeLine("<td class=error></td>",writer);
		}
		
		try {
			Thread.sleep(2000);
		}
		catch(InterruptedException err) {
		}
			
		//Emtpy msg/err log
		File f = new File("jhoney.log");
		f.delete();
		//
				
		writeLine("<tr><td><p>Starting JHoney deamon</td>",writer);
		try {
			main = new HoneyMain();
			main.errors();
			writeLine("<td class=ok></td>",writer);
		}
		catch(Exception err) {
			if(err.getMessage().startsWith("Error")) {
				writeLine("<td class=error></td>",writer);
				main = null;
			}
			else {
				writeLine("<td class=okwitherrors></td>",writer);
			}
		}
		writeLine("<td><div align=center><a href=\"help.html?Help=Deamon\"><img src=help.png></a></div></td></tr>",writer);
			
		writeLine("</table>",writer);
		
		writeLine("</td></tr></table>",writer);
			
		sendTextfile("http/footer.html",writer);
	}
	
	public void status(BufferedWriter writer) {
		sendHeader("HTTP/1.1 200 OK",writer);
		sendTextfile("http/header.html",writer);
		
		writeLine("<br><table width=500><tr><td>",writer);
			
		writeLine("<table width=400 border=0 cellspacing=1>",writer);
		writeLine("<tr><td class=filled width=80%><p class=menu>Action</td><td class=filled width=10%><p class=menu>Status</td><td class=filled width=10%></td></tr>",writer);
			
		writeLine("<tr><td><p>Check if process is running</td>",writer);
		
		boolean running = false;
		if(running()) {
			writeLine("<td class=ok></td>",writer);
			running = true;
		}
		else {
			writeLine("<td class=error></td>",writer);
		}
		//
		writeLine("<td><div align=center><a href=\"help.html?Help=Deamon\"><img src=help.png></a></div></td></tr>",writer);
		
		writeLine("</table>",writer);
		
		if(running) {
		
			writeLine("<br><table width=400 border=0 cellspacing=1>",writer);
			writeLine("<tr><td class=filled><p class=menu>Config settings</td></tr>",writer);
			HSProperties prop = HSProperties.getInstance();
			String ports_str = main.getPorts();
			
			writeLine("<tr><td><p>Deamon running at ports "+ports_str+"</td></tr>",writer);
			
			writeLine("<tr><td><p>Blacklistings are removed after "+(prop.getBlacklistTimeout()/1000)+" sec</td></tr>",writer);
			
			if(prop.useAutoBlacklist()) {
				writeLine("<tr><td><p>Automatic blacklisting is set to true</td></tr>",writer);
			}
			else {
				writeLine("<tr><td><p>Automatic blacklisting is set to false</td></tr>",writer);
			}
			writeLine("<tr><td class=filled><p></td></tr><table>",writer);
		}
		
		writeLine("</td></tr></table>",writer);
		
		sendTextfile("http/footer.html",writer);
	}
}