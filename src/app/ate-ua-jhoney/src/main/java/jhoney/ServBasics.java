package jhoney;
import java.io.*;
import java.net.*;
import java.util.*;

public abstract class ServBasics {

	private String crlf;
	public String request;
	public String method;
	
	public final String version = "1.00";
	public final String releaseDate = "2004-12-21";

	public void writeLine(String line, BufferedWriter writer) {
		try {
			writer.write(line,0,line.length());
			writer.write(crlf,0,crlf.length());
		}
		catch(Exception err) {
			MsgLog.add("Response error: "+err.getMessage());
		}
	}
	
	public void sendHeader(String type, BufferedWriter writer) {
		if(crlf == null) {
			byte[] b = {(byte)13,(byte)10};
			crlf = new String(b);
		}
		
		try {
			writeLine(type,writer);
			writeLine("Connection: close",writer);
			writeLine("Content-Type: text/html",writer);
			writeLine("",writer);
		}
		catch(Exception err) {
			MsgLog.add("Response error: "+err.getMessage());
		}
	}
	
	/*
	* If a HTML line contains the tag <ServerVariable id=[id]> ,
	* the tag is replaced by a variable with id [id]
	*/
	private void serverVariable(String line, BufferedWriter writer) {
		String[] tokens = line.split("<ServerVariable ");
		
		String parsed = "";
		for(int i=0;i<tokens.length;i++) {
			if(tokens[i].startsWith("id=")) {
				String rest = tokens[i].substring(tokens[i].indexOf(">")+1,tokens[i].length());
				
				//Add the actual server variables
				if(tokens[i].startsWith("id=Version")) {
					parsed += version;
				}
				else if(tokens[i].startsWith("id=ReleaseDate")) {
					parsed += releaseDate;
				}
				else if(tokens[i].startsWith("id=Theme")) {
					HSProperties prop = HSProperties.getInstance();
					parsed += prop.getTheme();
				}
				parsed += rest;
			}
			else {
				parsed += tokens[i];
			}
		}
		writeLine(parsed,writer);
	}
	
	/**
	* Sends a text file to the client
	*/
	public void sendTextfile(String filename, BufferedWriter writer) {
		if(crlf == null) {
			byte[] b = {(byte)13,(byte)10};
			crlf = new String(b);
		}
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			
			String line = reader.readLine();
			while(line != null) {
				if(line.indexOf("<ServerVariable id=") == -1) {
					writeLine(line,writer);
				}
				else {
					serverVariable(line,writer);
				}
				
				line = reader.readLine();
			}
			
			reader.close();
		}
		catch(Exception err) {
			MsgLog.add("Text response error: "+err.getMessage());
		}
	}
	
	/**
	* Sends a binary file to the client
	*/
	public void sendBinaryfile(String filename, OutputStream out) {
		try {
			File file = new File(filename);
			FileInputStream in = new FileInputStream(file);
			
			int r = 0;
			byte[] buffer = new byte[512];
			int current = 0;
			int total = (int)file.length();
			
			r = in.read(buffer);
			while(current < total) {
				current += r;
				out.write(buffer,0,r);
				
				r = in.read(buffer);
			}
			
			in.close();
			out.close();
		}
		catch(Exception err) {
			MsgLog.add("Binary response error: "+err.getMessage());
			err.printStackTrace();
		}
	}
	
	/**
	* Gets method and the requested file from the HTTP request
	*/
	public void parseRequest(BufferedReader reader) {
		String r = null;
		
		try {
			String line = reader.readLine();
			while(line != null) {
				if(line.indexOf("GET") > -1) {
					r = line;
				}
			
				line = reader.readLine();
				
				if(line.equals("")) break;
			}
		}
		catch(Exception err) {
			r = null;
		}
		
		if(r != null) {
			StringTokenizer st = new StringTokenizer(r," ");
			method = st.nextToken();
			request = st.nextToken();
		}
	}
	
	/**
	* Parses the file from an URL string
	*/
	public String parseFile(String url) {
		if(url.indexOf("?") == -1) {
			return url;
		}
		else {
			return url.substring(0,url.indexOf("?"));
		}
	}
	
	/**
	* Parses params from an URL string
	* Example:
	* http://index.html?p1=a&p2=b
	* is returned as {p1,a,p2,b}
	*/
	public String[] parseParams(String url) {	
		if(url.indexOf("?") == -1) {
			return null;
		}
	
		String params = url.substring(url.indexOf("?")+1,url.length());
		
		StringTokenizer st = new StringTokenizer(params,"&");
		int no = st.countTokens();
		
		String[] pars = new String[st.countTokens()*2];
		for(int i=0;i<no;i++) {
			String tmp = st.nextToken();
			StringTokenizer s = new StringTokenizer(tmp,"=");
			pars[i*2] = s.nextToken();
			if(s.hasMoreTokens()) { pars[i*2+1] = s.nextToken(); }
			else { pars[i*2+1] = ""; }
		}
		
		return pars;
	}
	
	/**
	* Returns the value of 'param'
	*/
	public String getParam(String[] params, String param) {
		String p = null;
		if(params == null) return null;
		for(int i=0;i<params.length-1;i+=2) {
			if(params[i].equals(param)) {
				p = params[i+1];
				break;
			}
		}
		
		return p;
	}
}