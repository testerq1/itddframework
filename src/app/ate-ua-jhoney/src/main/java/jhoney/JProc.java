package jhoney;
import java.io.*;
import java.util.*;

public class JProc {
	//Declarations
	private String procname;
	private boolean ok = false;
	private String action;
	//

	public static void main(String[] args) {
	
		String arg = "";
		String param = "";
		for(int i=0;i<args.length;i++) {
			if(!args[i].startsWith("--")) {
				arg += args[i]+" ";
			}
			else {
				param = args[i];
			}
		}
		arg = arg.trim();	
	
		if(arg.indexOf("java") == -1) {
			System.exit(1);
		}
		
		JProc kp = new JProc(arg,param);
	}
	
	public JProc(String procname, String action) {
		this.procname = procname;
		this.action = action;
	
		try {
			Runtime rt = Runtime.getRuntime();
			Process p = rt.exec("ps -C java");
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = reader.readLine();
			while(line != null) {
				if(line.indexOf("java") > -1) {
					check(line);
				}
				line = reader.readLine();
			}
			
			if(!ok) {
				System.exit(1);
			}
			else {
				System.exit(0);
			}
		}
		catch(Exception err) {
			System.exit(1);
		}
	}
	
	private void check(String line) {
		try {
			line = line.trim();
			StringTokenizer st = new StringTokenizer(line," ");
			
			String pid = st.nextToken();
			
			Runtime rt = Runtime.getRuntime();
			Process p = rt.exec("ps "+pid);
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String tmp = reader.readLine();
			while(tmp != null) {				
				if(tmp.indexOf(procname) > -1) {
					if(tmp.indexOf("JProc") == -1) {
						kill(tmp);
					}
				}
				tmp = reader.readLine();
			}
		}
		catch(Exception err) {
			System.exit(1);
		}
	}
	
	private void kill(String line) {
		try {
			line = line.trim();
			StringTokenizer st = new StringTokenizer(line," ");
			String pid = st.nextToken();
			
			if(action.equals("--kill")) {
				Runtime rt = Runtime.getRuntime();
				rt.exec("kill "+pid);
			}
			
			ok = true;
		}
		catch(Exception err) {
			System.exit(1);
		}
	}
}