package ate.ua.jhoney;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;

import org.honeynet.hpfeeds.Hpfeeds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExampleErrorHandler implements Hpfeeds.ErrorHandler {
    private static Logger log = LoggerFactory.getLogger(ExampleErrorHandler.class);
    @Override
    public void onError(ByteBuffer msg) {
        try {
            log.error("error message from broker: {}", Hpfeeds.decodeString(msg));            
        }
        catch (CharacterCodingException e) {
            throw new RuntimeException(e);
        }
    }
}