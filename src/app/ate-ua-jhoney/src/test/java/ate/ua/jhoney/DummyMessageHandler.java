package ate.ua.jhoney;

import java.nio.ByteBuffer;

import org.honeynet.hpfeeds.Hpfeeds;

public class DummyMessageHandler implements Hpfeeds.MessageHandler {
    @Override
    public void onMessage(String ident, String channel, ByteBuffer msg) {
        try {
            Thread.sleep(100);
        }
        catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}