package ate.ua.jhoney;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.honeynet.hpfeeds.Hpfeeds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MD5Handler implements Hpfeeds.MessageHandler {
    private static Logger log = LoggerFactory.getLogger(MD5Handler.class);
    private MessageDigest md;
    
    public MD5Handler() {
        try {
            md = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public void onMessage(String ident, String chan, ByteBuffer msg) {
        int len = msg.remaining();
        md.reset();
        md.update(msg); 
        
        String md5 = new BigInteger(1, md.digest()).toString(16);
        // pad
        if (md5.length() < 32) {
            StringBuilder sb = new StringBuilder(32);
            int i = 32 - md5.length();
            while (i > 0) {
                sb.append("0");
                i--;
            }
            sb.append(md5);
            md5 = sb.toString();
        }
        log.info("publish to {} by {}: length={} md5={}", new Object[]{chan, ident, len, md5});
    }
}