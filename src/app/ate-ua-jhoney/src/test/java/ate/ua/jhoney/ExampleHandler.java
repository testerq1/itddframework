package ate.ua.jhoney;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.util.regex.Pattern;

import org.honeynet.hpfeeds.Hpfeeds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExampleHandler implements Hpfeeds.MessageHandler {
    private static Logger log = LoggerFactory.getLogger(ExampleHandler.class);
    
    private Pattern pat = Pattern.compile(".*\\P{Print}");
    
    @Override
    public void onMessage(String ident, String chan, ByteBuffer msg) {
        try {
            String out = null;
            boolean notPrintable = false;
            
            ByteBuffer buf = msg.slice();
            int bufLen = buf.remaining();
            if (bufLen > 20) {
                buf.limit(20);
                bufLen = 20;
            }
            
            try {
                notPrintable = pat.matcher(Hpfeeds.decodeString(buf)).matches();
            } catch (CharacterCodingException e) {
                notPrintable = true;
            }
            
            if (notPrintable) {
                StringBuilder outBuilder = new StringBuilder(64);
                buf.rewind();
                for (int i = bufLen ; i != 0 ; i--) {
                    outBuilder.append(String.format("%02x ", buf.get()));
                }
                outBuilder.append("...");
                out = outBuilder.toString();
            }
            else {
                out = Hpfeeds.decodeString(msg);
            }
            
            log.info("publish to {} by {}: {}", new Object[]{chan, ident, out});
        }
        catch (CharacterCodingException e) {
            throw new RuntimeException(e);
        }
    }
}