package ate.ua.jhoney;

import java.io.IOException;
import java.io.InputStream;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.Date;

import org.honeynet.hpfeeds.Hpfeeds;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

public class CsvHandler implements Hpfeeds.MessageHandler {
    private ObjectMapper jsonObjectMapper = new ObjectMapper();
    private CsvMapper csvObjectMapper = new CsvMapper();
    CsvSchema csvShema = csvObjectMapper.schemaFor(Attack.class);
    
    private static class Attack {
        public long timestamp;
        public String ident, chan;
        public String daddr, dport, saddr, sport, md5, sha512, url;
        
        @Override
        public String toString() {
            return String.format("%d %s %s %s %s %s %s %s %s %s", timestamp, ident, chan, daddr, dport, saddr, sport, md5, sha512, url);
        }
    }
    
    @Override
    public void onMessage(String ident, String chan, ByteBuffer msg) {
        long timestamp = new Date().getTime();
        try {
            Attack attack = jsonObjectMapper.readValue(new ByteBufferInputStream(msg), Attack.class);
            attack.timestamp = timestamp;
            attack.ident = ident;
            attack.chan = chan;
            //FIXME double and triple commas?
            String csv = csvObjectMapper.writer().withSchema(csvShema).writeValueAsString(attack);
            System.out.print(csv);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }



public static class ByteBufferInputStream extends InputStream {
    private ByteBuffer buf;
    
    public ByteBufferInputStream(ByteBuffer buf) {
        this.buf = buf;
    }

    @Override
    public int read() throws IOException {
        try {
            return buf.get() & 0xff;
        }
        catch (BufferUnderflowException e) {
            return -1;
        }
    }
}
}