package ate.ua.jhoney;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.util.regex.Pattern;

import jhoney.JHServ;

import org.testng.annotations.Test;

import ate.testcase.Testcase;


public class JHoneyTest extends Testcase {
    @Test
    public void jhserv(){
        JHServ svr=new JHServ();
        Thread thr = new Thread(svr);
        thr.start();
        sleep(10000);
        svr.stopServer();
        thr.interrupt();
    }  
    
}
