package ate.ua.jhoney;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;

import org.honeynet.hpfeeds.Hpfeeds;
import org.honeynet.hpfeeds.Hpfeeds.EOSException;
import org.honeynet.hpfeeds.Hpfeeds.InvalidStateException;
import org.honeynet.hpfeeds.Hpfeeds.LargeMessageException;
import org.honeynet.hpfeeds.Hpfeeds.ReadTimeOutException;
import org.testng.annotations.Test;

import ate.testcase.Testcase;

public class HpfeedsTest extends Testcase {
    String[] channels = {"channel1","channel2"};
    
    @Test
    public void hpfeeds(){
        Hpfeeds hp=new Hpfeeds("172.16.5.117",10000,"ravi.huang@gmail.com","00000a");
        try {
            String[] channels = {"channel1","channel2"};
            hp.connect();
            hp.subscribe(channels);
            hp.run(new ExampleHandler(), new ExampleErrorHandler());
            //hp.run(new CsvHandler(), new ExampleErrorHandler());            
        } catch (IOException | EOSException | ReadTimeOutException
                | LargeMessageException | InvalidStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }        
    }
    
    public void csvConsumer(){
        Hpfeeds hp=new Hpfeeds("172.16.5.117",10000,"ravi.huang@gmail.com","00000a");
        try {
            String[] channels = {"channel1","channel2"};
            hp.connect();
            hp.subscribe(channels);
            hp.run(new CsvHandler(), new ExampleErrorHandler());            
        } catch (IOException | EOSException | ReadTimeOutException
                | LargeMessageException | InvalidStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public void md5Consumer(){
        Hpfeeds hp=new Hpfeeds("172.16.5.117",10000,"ravi.huang@gmail.com","00000a");
        try {
            
            hp.connect();
            hp.subscribe(channels);
            hp.run(new MD5Handler(), new ExampleErrorHandler());              
            
        } catch (IOException | EOSException | ReadTimeOutException
                | LargeMessageException | InvalidStateException e) {
            e.printStackTrace();
        }
    }
    public void filePublisher(){     
        int MAX_FILE_SIZE=10 * 1024*1024;
        final Hpfeeds hp=new Hpfeeds("172.16.5.117",10000,"ravi.huang@gmail.com","00000a");
        try {
            File f = new File("filename");
            long fs = f.length();            
            assertTrue(fs<=MAX_FILE_SIZE);
            
            FileChannel fc = new FileInputStream(f).getChannel();
            String[] channels = {"channel1","channel2"};
            hp.connect();
            hp.subscribe(channels);
            hp.run(new MD5Handler(), new ExampleErrorHandler());  
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                   try {
                       hp.run(new DummyMessageHandler(), new ExampleErrorHandler());
                    } catch (IOException | EOSException | ReadTimeOutException
                            | LargeMessageException | InvalidStateException e) {
                        e.printStackTrace();
                    }   
                }
            });
            t.setDaemon(true);
            t.start();
            
            ByteBuffer buf = fc.map(MapMode.READ_ONLY, 0, fs);
            hp.publish(channels, buf);
            
            hp.stop();
            hp.disconnect();
        } catch (IOException | EOSException | ReadTimeOutException
                | LargeMessageException | InvalidStateException e) {
            e.printStackTrace();
        }
    }        
}
