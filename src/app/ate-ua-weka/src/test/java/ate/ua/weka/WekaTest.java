package ate.ua.weka;

import ate.testcase.UATestcase;
import org.testng.annotations.*;

import weka.classifiers.trees.J48;
import weka.experiment.Experiment;

public class WekaTest extends UATestcase {
	
	public void experiment(){
		try {
			Experiment.main(new String[]{"-r","-T","src/test/data/iris.arff",
					"-D","weka.experiment.InstancesResultListener",
					"-P","weka.experiment.RandomSplitResultProducer","--",
					"-W","weka.experiment.ClassifierSplitEvaluator","--",
					"-W","weka.classifiers.rules.OneR"});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Test
	public void j48(){
		try {
			J48 j48=new J48();
			log.info(WekaUtil.help(j48));			
			log.info(WekaUtil.evaluateModel(j48, new String[]{"-t", "src/test/data/iris.arff","-v"}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
