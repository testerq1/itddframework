package ate.ua.weka;

import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Evaluation;
import weka.core.Instances;
public class WekaUtil extends Evaluation{

	public WekaUtil(Instances data) throws Exception {
		super(data);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static String help(Classifier classifier){
		return makeOptionString(classifier,true);
	}

}
