package ate.ua.spdy;

import org.testng.annotations.Test;

import ate.testcase.UATestcase;

public class OkhttpUATest extends UATestcase {
    @Test
    public void test_json_client(){
        OkhttpUA ua=this.create_ua("okhttp:test",new OkhttpUA());
        ua.use_ssl();
        ua.startup();
        String json= "{'winCondition':'HIGH_SCORE',"
                + "'name':'Bowling',"
                + "'round':4,"
                + "'lastSaved':1367702411696,"
                + "'dateStarted':1367702378785,"
                + "'players':["
                + "{'name':'player1','history':[10,8,6,7,8],'color':-13388315,'total':39},"
                + "{'name':'player2','history':[6,10,5,10,10],'color':-48060,'total':41}"
                + "]}";
        assertTrue(200==ua.post_json("http://www.roundsapp.com/post", json));
        
    }
}
