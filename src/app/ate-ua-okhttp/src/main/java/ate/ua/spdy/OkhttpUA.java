package ate.ua.spdy;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.net.ssl.SSLContext;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import ate.AbstractURIHandler;
import ate.ua.RunFailureException;

/**
 * okhttp是一个支持SPDY的客户端工具
 * https://github.com/square/okhttp
 * @author Administrator
 *
 */
public class OkhttpUA extends AbstractURIHandler{
    OkHttpClient client;
    static MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private boolean use_ssl;
    
    public OkhttpUA use_ssl(){
        this.use_ssl=true;        
        return this;
    }
    
    @Override
    public void teardown() {
        
    }

    @Override
    public void startup() {
        client = new OkHttpClient();
        if(use_ssl){
            SSLContext sslContext;
            try {
              sslContext = SSLContext.getInstance("TLS");
              sslContext.init(null, null, null);
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
              throw new RunFailureException(); // The system has no TLS. Just give up.
            }
            client.setSslSocketFactory(sslContext.getSocketFactory());
        }
    }
    
    private int post0(String url,RequestBody body){
        Request request = new Request.Builder()
        .url(url)
        .post(body)
        .build();
        try {
            Response response = client.newCall(request).execute();
            log.debug(response.code()+"");
            return response.code();
        } catch (IOException e) {
            e.printStackTrace();
        }   
        return -1;
    }
    
    public int post(String url, String mimetype,String content){
        return post0(url,RequestBody.create(MediaType.parse(mimetype), content));
    }
    
    public int post_json(String url, String json){
        return post0(url,RequestBody.create(JSON, json));
    }
    
    public String get(String url) throws IOException {
        Request request = new Request.Builder()
            .url(url)
            .build();

        Response response = client.newCall(request).execute();
        if(response!=null)
            return response.body().string();
        return null;
      }
    @Override
    public String get_default_schema() {
        return "okhttp";
    }

    @Override
    public boolean is_ready() {
        return false;
    }

}
