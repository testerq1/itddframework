package ate.ua.docker;

import java.net.URI;
import java.nio.file.Paths;

import org.testng.annotations.Test;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerCertificateException;
import com.spotify.docker.client.DockerCertificates;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.DockerException;

import ate.testcase.Testcase;

/**
 * https://github.com/spotify/docker-client
 * 
 * sudo docker -H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock -d &
 * 
 * @author Ravi Huang
 *
 */
public class DockerClientTest extends Testcase{
    @Test
    public void EnvClient()
    {
     STEP("Create a client based on DOCKER_HOST and DOCKER_CERT_PATH env vars:");
        try {
            final DockerClient docker = DefaultDockerClient.fromEnv().build();
            
        } catch (DockerCertificateException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void UnixSocketClient(){
        final DockerClient docker = new DefaultDockerClient("unix:///var/run/docker.sock");
    } 
    
    @Test
    public void HTTPSClient(){
        try {
            final DockerClient docker = DefaultDockerClient.builder()
                    .uri(URI.create("http://172.16.5.115:2376"))
                    //.dockerCertificates(new DockerCertificates(Paths.get("/Users/rohan/.docker/boot2docker-vm/")))
                    .build();
            log.info(docker.info().toString());
            
        } catch (DockerException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
