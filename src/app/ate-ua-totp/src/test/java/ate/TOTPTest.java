package ate;

import org.jboss.aerogear.security.otp.Totp;
import org.jboss.aerogear.security.otp.api.Base32;
import org.testng.annotations.Test;

import ate.testcase.Testcase;


public class TOTPTest extends Testcase
{

    @Test
    public void testApp()
    {
        String secret = Base32.random();
        Totp totp = new Totp(secret);
        String key=totp.now(); //427773
        log.info(totp.uri("john#doe"));
        assertTrue(totp.verify(key)); //true
        sleep(4000);
        assertFalse(totp.verify(key),key); //false
    }
}
