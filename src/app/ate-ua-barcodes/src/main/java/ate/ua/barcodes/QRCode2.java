package ate.ua.barcodes;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import ezvcard.Ezvcard;
import ezvcard.VCard;
import net.glxn.qrgen.javase.QRCode;

/**
 * https://github.com/mangstadt/ez-vcard
 * https://github.com/kenglxn/QRGen
 * https://code.google.com/p/vcardio/
 *
 */
public class QRCode2 extends QRCode {

    protected QRCode2(String text) {
        super(text);
    }
    
    public static VCard readVCard(String str) {
        VCard vcard = Ezvcard.parse(str).first();
        return vcard;
    }
    
    public static String readString(InputStream inputStream) {
        try {
            return readString(ImageIO.read(inputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String readString(BufferedImage image) {
        /*判断是否是图片*/
        if (image == null) {
            System.out.println("Could not decode image");
        }
            /*解析二维码用到的辅助类*/
        LuminanceSource source = new BufferedImageLuminanceSource(image);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Hashtable<DecodeHintType, Object> hints = new Hashtable<DecodeHintType, Object>();
            /*解码设置编码方式为：UTF-8*/
        hints.put(DecodeHintType.CHARACTER_SET, "UTF-8");

        try {
            Result result = new MultiFormatReader().decode(bitmap, hints);
            String resultStr = result.getText();
            return resultStr;
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 解析二维码
     *
     * @param path 图片的绝对路径
     */
    public static String readString(String path) {
        if (path == null || path.equals("")) {
            System.out.println("文件路径不能为空!");
        }
        File file = new File(path);
        try {
            BufferedImage image = ImageIO.read(file);
                /*判断是否是图片*/
            if (image != null) {
                return readString(image);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
