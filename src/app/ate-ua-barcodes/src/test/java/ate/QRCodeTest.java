package ate;

import ate.testcase.Testcase;
import ate.ua.barcodes.QRCode2;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import ezvcard.Ezvcard;
import ezvcard.VCard;
import ezvcard.VCardVersion;
import ezvcard.property.StructuredName;
import net.glxn.qrgen.core.exception.QRGenerationException;
import net.glxn.qrgen.core.image.ImageType;
import net.glxn.qrgen.javase.QRCode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

public class QRCodeTest extends Testcase{    

    @Test
    public static void string() {  
        String str = "abc";  
        String str1 = "abc";  
        String str2 = new String("abc");  
        log.info("str == str1 : {}",str == str1);  //true
        log.info("str1 == abc :{}",str1 == "abc");  //true
        log.info("str2 == abc :{}",str2 == "abc");  //false
        log.info("str1 == str2 :{}",str1 == str2);  //false
        log.info("str1.equals(str2) :{}",str1.equals(str2)); //true  
        log.info("str1 == str2.intern() :{}",str1 == str2.intern()); //true  
        log.info("str2 == str2.intern() :{}",str2 == str2.intern()); //false 
        log.info("abc == str2.intern() :{}","abc" == str2.intern()); //true 
        log.info("str1.hashCode() == str2.hashCode() :{}",str1.hashCode() == str2.hashCode());  //true
    }  
    
    @Test
    public void read(){
    	String s=QRCode2.readString("qr.png");
        log.info(s);
    }
    
//    public void shouldGetSvgFromText() throws Exception {
//        File file = QRCode.from("www.example.org").svg();
//        assertTrue(file!=null);
//    }
//
//    @Test
//    public void shouldGetSvgWithSizeFromText() throws Exception {
//        File file = QRCode.from("www.example.com").withSize(250, 250).svg();
//        assertTrue(file!=null);
//    }
//
//    @Test
//    public void shouldGetSvgWithSizeAndColorFromText() {
//        File file = QRCode.from("www.example.com").withSize(250, 250).withColor(30, 90).svg();
//        assertTrue(file!=null);
//    }

    @Test
    public void ezVcard() throws Exception {
        VCard vcard = new VCard();

        StructuredName n = new StructuredName();
        n.setFamily("Doe");
        n.setGiven("Jonathan");
        n.addPrefix("Mr");
        
        vcard.setStructuredName(n);
        vcard.setFormattedName("John Doe");
        String str = Ezvcard.write(vcard).version(VCardVersion.V4_0).go();        
        
        File file=new File("code.png");
        QRCode.from(str).writeTo(new FileOutputStream(file));
        Assert.assertNotNull(file);
        
        String s=QRCode2.readString("code.png");
        log.info(s);
        
        file.deleteOnExit();        
    }

    @Test
    public void shouldGetBitmapFileFromText() throws Exception {
        File file=new File("code.png");
        QRCode.from("www.example.org").to(ImageType.BMP).writeTo(new FileOutputStream(file));
        assertTrue(file!=null);        
        String s=QRCode2.readString("code.png");
        assertTrue("www.example.org".equals(s));
    }

    @Test
    public void shouldGetSTREAMFromTextWithDefaults() throws Exception {
        ByteArrayOutputStream stream = QRCode.from("Hello World").stream();
        Assert.assertNotNull(stream);
    }

    @Test
    public void shouldGetFileFromTextWithImageTypeOverrides() throws Exception {
        File jpg = QRCode.from("Hello World").to(ImageType.JPG).file();
        Assert.assertNotNull(jpg);
        File gif = QRCode.from("Hello World").to(ImageType.GIF).file();
        Assert.assertNotNull(gif);
    }

    @Test
    public void shouldGetFileWithNameFromTextWithImageTypeOverrides() throws Exception {
        File jpg = QRCode.from("Hello World").to(ImageType.JPG).file("Hello World");
        Assert.assertNotNull(jpg);
        assertTrue(jpg.getName().startsWith("Hello World"));
        File gif = QRCode.from("Hello World").to(ImageType.GIF).file("Hello World");
        Assert.assertNotNull(gif);
        assertTrue(gif.getName().startsWith("Hello World"));
    }

    @Test
    public void shouldGetStreamFromText() throws Exception {
        ByteArrayOutputStream stream = QRCode.from("Hello World").to(ImageType.PNG).stream();
        Assert.assertNotNull(stream);
        File tempFile = File.createTempFile("test", ".tmp");
        long lengthBefore = tempFile.length();
        FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
        stream.writeTo(fileOutputStream);
        assertTrue(lengthBefore < tempFile.length());
    }

    @Test
    public void shouldWriteToSuppliedStream() throws Exception {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        QRCode.from("Hello World").writeTo(stream);

        Assert.assertNotNull(stream);
        File tempFile = File.createTempFile("test", ".tmp");
        long lengthBefore = tempFile.length();
        FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
        stream.writeTo(fileOutputStream);
        assertTrue(lengthBefore < tempFile.length());
    }

    @Test
    public void shouldBeAbleToOverrideDimensionsToFile() throws Exception {
        long defaultSize = QRCode.from("Hello World").to(ImageType.PNG).file().length();
        long defaultSize2 = QRCode.from("Hello World").to(ImageType.PNG).file().length();
        File file = QRCode.from("Hello World").to(ImageType.PNG).withSize(250, 250).file();
        assertTrue(file!=null);
        assertTrue(defaultSize == defaultSize2);
        assertTrue(defaultSize < file.length());
    }

    @Test
    public void shouldBeAbleToOverrideDimensionsToFileWithName() throws Exception {
        long defaultSize = QRCode.from("Hello World").to(ImageType.PNG).file("Hello World").length();
        long defaultSize2 = QRCode.from("Hello World").to(ImageType.PNG).file("Hello World").length();
        File file = QRCode.from("Hello World").to(ImageType.PNG).withSize(250, 250).file("Hello World");
        assertTrue(file!=null);
        assertTrue(defaultSize == defaultSize2);
        assertTrue(defaultSize < file.length());
        assertTrue(file.getName().startsWith("Hello World"));
    }

    @Test
    public void shouldBeAbleToSupplyEncodingHint() throws Exception {
        String expected = "UTF-8";
        final Object[] capture = new Object[1];
        try {
            final QRCode from = QRCode.from("Jour férié");
            from.setQrWriter(writerWithCapture(capture));
            from.to(ImageType.PNG).withCharset(expected).stream();
        } catch (QRGenerationException ignored) {
        }
        assertCapturedHint(expected, capture, EncodeHintType.CHARACTER_SET);
    }

    @Test
    public void shouldBeAbleToSupplyErrorCorrectionHint() throws Exception {
        ErrorCorrectionLevel expected = ErrorCorrectionLevel.L;
        final Object[] capture = new Object[1];
        try {
            final QRCode from = QRCode.from("Jour férié");
            from.setQrWriter(writerWithCapture(capture));
            from.to(ImageType.PNG).withErrorCorrection(ErrorCorrectionLevel.L).stream();
        } catch (QRGenerationException ignored) {
        }
        assertCapturedHint(expected, capture, EncodeHintType.ERROR_CORRECTION);
    }

    @Test
    public void shouldBeAbleToSupplyAnyHint() throws Exception {
        String expected = "a hint";
        EncodeHintType[] hintTypes = EncodeHintType.values();
        for (EncodeHintType type : hintTypes) {
            final Object[] capture = new Object[1];
            try {
                final QRCode from = QRCode.from("Jour férié");
                from.setQrWriter(writerWithCapture(capture));
                from.to(ImageType.PNG).withHint(type, expected).stream();
            } catch (QRGenerationException ignored) {
            }
            assertCapturedHint(expected, capture, type);
        }
    }

//    @Test
//    public void shouldColorOutput() throws IOException {
//        File file = QRCode.from("Hello World").withColor(0xFFFF0000, 0xFFFFFFAA).file();
//        File tempFile = File.createTempFile("qr_", ".png");
//        Files.copy(file.toPath(), new FileOutputStream(tempFile));
//        System.out.println(tempFile.getAbsoluteFile());
//    }
//
    @SuppressWarnings("unchecked")
    private void assertCapturedHint(Object expected, Object[] capture, EncodeHintType type) {
        Assert.assertEquals(expected, ((Map<EncodeHintType, ?>) capture[0]).get(type));
    }

    private Writer writerWithCapture(final Object[] capture) {
        return new Writer() {
            @Override
            public BitMatrix encode(String contents, BarcodeFormat format, int width, int height) throws WriterException {
                throw new UnsupportedOperationException("not implemented");
            }

            @Override
            public BitMatrix encode(String c, BarcodeFormat f, int w, int h, Map<EncodeHintType, ?> hs) throws WriterException {
                capture[0] = hs;
                return new BitMatrix(0);
            }
        };
    }
}
