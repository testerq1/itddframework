import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.annotations.*
import ate.testcase.*
import ate.ua.mina.* 
import common.*

public class CsTest extends CsUATestcase {
	
	@DataProvider(name="Data_TestStep")
    public Object[][] Data_TestStep(){		
        return [["udp"],["tcp"]];
    }
	
	@Test(dataProvider="Data_TestStep")
	public void multi_clt_auto_response(String transport) {
		log.info(STEP+" multi_clt_auto_response:");
		log.info(STEP+".1 init:");
		def svr = create_server("socket://$siip:5000/"+transport);
		def clt1=create_client("socket://$sip:5000/"+transport);
		def clt2=create_client("socket://$sip:5000/"+transport);
		svr.set_auto_response(true);
		start_all_ua();
		
		log.info(STEP+".2 client send/recv message:");		
		clt1.send_message("hello im clt1");
		clt2.send_message("hello im clt2");
		def resp1 = clt1.recv_message();
		assertTrue(resp1.startsWith(transport+" from:/$sip:5000"));
		log.info("clt1 recv:"+resp1);
		def resp2 = (String)clt2.recv_message();
		assertTrue(resp2.startsWith(transport+" from:/$sip:5000"));
		log.info("clt2 recv:"+resp2);
		
		log.info(STEP+".3 assert 2 resps isn't equal:");
		assertFalse(resp1.equals(resp2));
		
	}
	@Test(dataProvider="Data_TestStep")
	public void multi_client(String transport) {
		log.info(STEP+" multi_client");
		log.info(STEP+".1 init: "+transport);
		def svr=create_server("socket://$siip:5000/"+transport);
		def clt1=create_client("socket://$sip:5000/"+transport);
		def clt2=create_client("socket://$sip:5000/"+transport);
						
		start_all_ua();
		assertTrue(svr.is_ready()&&clt1.is_ready()&&clt2.is_ready());
		
		log.info(STEP+".2 client1 send message:");
		clt1.send_message("clt1");
		def req = (String)svr.recv_message();
		assertTrue(req.equals("clt1"));
		
		svr.add_last_io_filter("log", new SessionLogFilter(svr));
		log.info(STEP+".3 server send response to clt1:");
		svr.send_message("hello clt1");
		req=(String)clt1.recv_message();
		assertTrue(req.equals("hello clt1"));		
		
		log.info(STEP+".4 client2 send message:");
		clt2.send_message("clt2");
		req = svr.recv_message();
		assertTrue(req.equals("clt2"));
		
		svr.remove_io_filter("log");
		log.info(STEP+".5 server send response to clt2:");
		svr.send_message("hello clt2");
		req=(String)clt2.recv_message();
		assertTrue(req.equals("hello clt2"));
		
		log.info(STEP+".6 reconnect all ua:");
		reconnect_all_ua();				
		
		log.info(STEP+".7 client1 send message:");
		clt1.send_message("clt1 loop");
		req = (String)svr.recv_message();
		assertTrue(req.equals("clt1 loop"));
		
		log.info(STEP+".8 server send response to clt1:");
		svr.send_message("hello clt1 loop");
		req=(String)clt1.recv_message();
		assertTrue(req.equals("hello clt1 loop"));		
		
		svr.add_last_io_filter("log", new SessionLogFilter(svr));
		log.info(STEP+".9 client2 send message:");
		clt2.send_message("clt2 loop");
		req = (String)svr.recv_message();
		assertTrue(req.equals("clt2 loop"));
		
		log.info(STEP+".10 server send response to clt2:");
		svr.send_message("hello clt2 loop");
		req=(String)clt2.recv_message();
		assertTrue(req.equals("hello clt2 loop"));		
	}
	
	@Test(dataProvider="Data_TestStep")
	public void send_rcv(String transport) {
		log.info(STEP+" send_rcv");
		log.info(STEP+".1 send_rcv: "+transport);
		def svr = create_server("socket://$siip:5000/"+transport);
		def clt = create_client("socket://$sip:5000/"+transport);		
		
		start_all_ua();
		
		log.info(STEP+".2 client send message:");
		clt.send_message("abcdefg");
		def req = svr.recv_message();
		assertTrue(req.equals("abcdefg"));
		
		log.info(STEP+".3 server send 1234567:");
		svr.send_message("1234567");
		req=(String)clt.recv_message();
		assertTrue(req.equals("1234567"));		
		
		log.info(STEP+".4 reconnect all ua:");
		reconnect_all_ua();		
		clt.send_message("abcdefgh");
		req = (String)svr.recv_message();
		assertTrue(req!=null&&req.equals("abcdefgh"));
		
		log.info(STEP+".5 server send 1234567h:");
		svr.send_message("1234567h");
		req=(String)clt.recv_message();
		assertTrue(req.equals("1234567h"));		
	}

	@Test(dataProvider="Data_TestStep")
	public void autosend(String transport) {
		log.info(STEP+" autosend:");
		log.info(STEP+".1 init: $siip $sip");
		def svr = create_server("socket://$siip:5000/"+transport);
		def clt = this.create_client("socket://$sip:5000/"+transport);
		svr.set_auto_response(true);
		
		log.info(STEP+".2 start_all_ua:");				
		this.start_all_ua();
		
		log.info(STEP+".3 client send/recv message:");
		clt.send_message("abcdefg");
		def req = clt.recv_message();		
		assertTrue(req.startsWith(transport+" from:/$sip:5000"));
			
		log.info(STEP+".4 client auto response:");
		clt.set_auto_response(true);
		svr.set_auto_response(false);
		
		svr.send_message("1234567");
		req = (String)svr.recv_message();
		assertTrue(req.endsWith("to:/$sip:5000"));		
		
		log.info(STEP+".5 server auto response:");
		svr.set_auto_response(true);		
		clt.set_auto_response(false);
		reconnect_all_ua();
		clt.send_message("abcdefgh");
		req = (String)clt.recv_message();
		assertTrue(req.startsWith(transport+" from:/$sip:5000"));
		
		log.info(STEP+".6 client auto response 2:");
		svr.set_auto_response(false);		
		clt.set_auto_response(true);
		svr.send_message("1234567h");
		req = (String)svr.recv_message();
		assertTrue(req.endsWith("to:/$sip:5000"));		 
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {		
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		//;
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
