package ate.seletestcase;

import java.io.File;

import io.selendroid.MyLauncher;
import io.selendroid.SelendroidCapabilities;
import io.selendroid.SelendroidConfiguration;
import io.selendroid.SelendroidLauncher;
import io.selendroid.android.AndroidApp;
import io.selendroid.android.AndroidDevice;
import io.selendroid.exceptions.AndroidSdkException;
import io.selendroid.server.model.MyDriver;
import ate.rm.dev.Host;
import ate.testcase.ATestSuite;

public class DefaultTestSuite extends ATestSuite {
	private static MyLauncher selendroidServer = null;
	private static String APK_HOME = Host.HOME + File.separator + "apk";
	protected AndroidApp app;
	protected AndroidDevice device;

	public void BeforeSuite(String apkname) {
		super.BeforeSuite();
		if (System.getenv("ANDROID_HOME") == null) {
			log.error("ANDROID_HOME envirment variable not set");
			throw new Error("ANDROID_HOME envirment variable not set");
		}

		if (selendroidServer != null) {
			selendroidServer.stopSelendroid();
		}

		SelendroidConfiguration config = new SelendroidConfiguration();

		String apppath = APK_HOME + File.separator + apkname;
		if (!new File(apppath).exists()) {
			log.error("apk file is not exist: " + apppath);
			throw new Error("apk file is not exist: " + apppath);
		}
		config.addSupportedApp(apppath);

		selendroidServer = new MyLauncher(config);

		selendroidServer.lauchSelendroid();

		MyDriver driver = selendroidServer.get_myserver().getDriver();
		try {
			for (String tmp : driver.getConfiguredApps().keySet()) {
				app = driver.getConfiguredApps().get(tmp);
				log.debug("installed: {} {}", tmp, app.getAppId());
			}			
			//selendroid不支持now
//			SelendroidCapabilities cap = new SelendroidCapabilities(
//					app.getAppId());
//			device = driver.getAndroidDevice(cap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void AfterSuite() {
		super.AfterSuite();
		if (selendroidServer != null) {			
			selendroidServer.stopSelendroid();
		}
		localhost.kill("adb");
	}

}
