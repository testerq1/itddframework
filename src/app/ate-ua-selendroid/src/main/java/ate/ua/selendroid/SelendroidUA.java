package ate.ua.selendroid;

import org.openqa.selenium.WebDriver;

import io.selendroid.SelendroidCapabilities;
import io.selendroid.SelendroidDriver;
import ate.AbstractURIHandler;
import ate.ua.UAOption;

public class SelendroidUA extends AbstractURIHandler{
	private WebDriver driver = null;
	private String app_id;
	
	public WebDriver get_driver(){
		return driver;
	}
	
	public String get_app_id(){
		return app_id;
	}
	
	@Override
	public SelendroidUA build(UAOption option, Object value) {
		super.build(option, value);
		if (option == UA_URI) {
			app_id=this.get_schemeSpecificPart();				
		}
		
		return this;
	}
	
	
	
	@Override
	public void teardown() {
		driver.quit();		
	}

	@Override
	public void startup() {
		try {
			driver = new SelendroidDriver(new SelendroidCapabilities(app_id));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public String get_default_schema() {
		return "selendroid";
	}

	@Override
	/**
	 * apk_name和app_id必须都存在
	 */
	public boolean is_ready() {
		return driver!=null&&app_id!=null;
	}

}
