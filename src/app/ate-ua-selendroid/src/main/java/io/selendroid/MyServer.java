package io.selendroid;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.webbitserver.WebServer;
import org.webbitserver.WebServers;
import org.webbitserver.helpers.NamingThreadFactory;

import io.selendroid.exceptions.AndroidDeviceException;
import io.selendroid.exceptions.AndroidSdkException;
import io.selendroid.server.SelendroidServlet;
import io.selendroid.server.SelendroidStandaloneServer;
import io.selendroid.server.StatusServlet;
import io.selendroid.server.grid.SelfRegisteringRemote;
import io.selendroid.server.model.MyDriver;
import io.selendroid.server.model.SelendroidStandaloneDriver;

public class MyServer {
	SelendroidConfiguration configuration;
	private MyDriver driver;

	private static final Logger log = Logger.getLogger(SelendroidStandaloneServer.class.getName());
    private WebServer webServer;

	  /**
	   * for testing only
	   * 
	   * @throws AndroidSdkException
	   */
	  protected MyServer(SelendroidConfiguration configuration,
			  MyDriver driver) throws AndroidSdkException {
	    this.configuration = configuration;
	    this.driver = driver;
	    NamingThreadFactory namingThreadFactory =
	        new NamingThreadFactory(Executors.defaultThreadFactory(), "selendroid-standalone-handler");
	    webServer =
	        WebServers.createWebServer(Executors.newCachedThreadPool(namingThreadFactory), new InetSocketAddress(
	            configuration.getPort()), URI.create("http://127.0.0.1"
	            + (configuration.getPort() == 80 ? "" : (":" + configuration.getPort())) + "/"));
	    init();
	  }

	  public MyServer(SelendroidConfiguration configuration)
	      throws AndroidSdkException, AndroidDeviceException {
	    this.configuration = configuration;
	    NamingThreadFactory namingThreadFactory =
	        new NamingThreadFactory(Executors.defaultThreadFactory(), "selendroid-standalone-handler");
	    webServer =
	        WebServers.createWebServer(Executors.newCachedThreadPool(namingThreadFactory), new InetSocketAddress(
	            configuration.getPort()), remoteUri(configuration.getPort()));
	    driver = initializeSelendroidServer();
	    init();
	  }

	  private static URI remoteUri(int port) {
	    try {
	      InetAddress address = InetAddress.getByName("0.0.0.0");

	      return new URI("http://" + address.getHostAddress() + (port == 80 ? "" : (":" + port)) + "/");
	    } catch (Exception e) {
	      e.printStackTrace();
	      throw new RuntimeException("can not create URI from HostAddress", e);
	    }
	  }

	  protected void init() throws AndroidSdkException {
	    // just make sure the connection will not be staled because
	    // the long emulator starting time and therefore long time
	    // it needs to create a session
	    webServer.staleConnectionTimeout(configuration.getTimeoutEmulatorStart());
	    webServer.add("/wd/hub/status", new StatusServlet(driver));
	    webServer.add(new SelendroidServlet(driver, configuration));
	  }

	  protected MyDriver initializeSelendroidServer() throws AndroidSdkException,
	      AndroidDeviceException {
	    return new MyDriver(configuration);
	  }

	  public void start() {
	    webServer.start();
	    if (StringUtils.isBlank(configuration.getRegistrationUrl()) == false
	        && StringUtils.isBlank(configuration.getServerHost()) == false) {
	      try {
	        new SelfRegisteringRemote(configuration, driver).performRegistration();
	      } catch (Exception e) {
	        log.severe("An error occured while registering selendroid into grid hub.");
	        e.printStackTrace();
	      }
	    }
	    log.debug("selendroid-standalone server has been started on port: " + configuration.getPort());
	  }

	  public void stop() {
	    log.debug("About to stop selendroid-standalone server");
	    driver.quitSelendroid();
	    webServer.stop();
	  }

	  public int getPort() {
	    return webServer.getPort();
	  }

	  public MyDriver getDriver() {
	    return driver;
	  }
}
