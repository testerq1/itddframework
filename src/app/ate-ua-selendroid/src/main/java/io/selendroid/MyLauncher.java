package io.selendroid;

import io.selendroid.exceptions.AndroidSdkException;
import io.selendroid.io.ShellCommand;
import io.selendroid.server.SelendroidStandaloneServer;
import io.selendroid.server.util.HttpClientUtil;

import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.google.common.base.Throwables;

public class MyLauncher {
	 public static final String LOGGER_NAME = "io.selendroid";
	  private static final Logger log = Logger.getLogger(SelendroidLauncher.class.getName());
	  private MyServer server = null;
	  private SelendroidConfiguration config = null;
	  
	  public MyServer get_myserver(){
		  return this.server;
	  }
	  
	  public static SelendroidLauncher getInstance(String[] args) {
		try {
	      configureLogging();
	      
	    } catch (Exception e1) {
	      log.severe("Error occurred while registering logging file handler.");
	    }
	    log.debug("################# Selendroid #################");
	    SelendroidConfiguration config = new SelendroidConfiguration();
	    try {
	      new JCommander(config, args);
	    } catch (ParameterException e) {
	      log.severe("An error occurred while starting selendroid: " + e.getMessage());
	      System.exit(0);
	    }
	    if (config.isVerbose()) {
	      log.setLevel(Level.FINE);
	      ShellCommand.setVerbose();
	    }
	    SelendroidLauncher launcher = new SelendroidLauncher(config);  
	    return launcher;
	  }
	  
	  public MyLauncher(SelendroidConfiguration config) {
	    this.config = config;
	    
	  }

	  private void lauchServer() {
	    try {
	      log.debug("Starting selendroid-server port " + config.getPort());
	      server = new MyServer(config);
	      server.start();
	    } catch (AndroidSdkException e) {
	      log.severe("Selendroid was not able to interact with the Android SDK: " + e.getMessage());
	      log.severe("Please make sure you have the latest version with the latest updates installed: ");
	      log.severe("http://developer.android.com/sdk/index.html");
	      throw Throwables.propagate(e);
	    } catch (Exception e) {
	      log.severe("Error occurred while building server: " + e.getMessage());
	      e.printStackTrace();
	      throw Throwables.propagate(e);
	    }
	    Runtime.getRuntime().addShutdownHook(new Thread() {
	      public void run() {
	        log.debug("Shutting down Selendroid standalone");
	        if (server != null) {
	          server.stop();
	        }
	      }
	    });
	  }

	  public void lauchSelendroid() {
	    lauchServer();
	    
	    HttpClientUtil.waitForServer(config.getPort());
	  }

	  public static void main(String[] args) {
	    try {
	      configureLogging();
	    } catch (Exception e1) {
	      log.severe("Error occurred while registering logging file handler.");
	    }

	    log.debug("################# Selendroid #################");
	    SelendroidConfiguration config = new SelendroidConfiguration();
	    try {
	      new JCommander(config, args);
	    } catch (ParameterException e) {
	      log.severe("An error occurred while starting selendroid: " + e.getMessage());
	      System.exit(0);
	    }
	    if (config.isVerbose()) {
	      log.setLevel(Level.FINE);
	      ShellCommand.setVerbose();
	    }
	    MyLauncher laucher = new MyLauncher(config);
	    laucher.lauchServer();
	  }

	  private static void configureLogging() throws Exception {
	    Handler fh = new FileHandler("%h/selendroid.log", 2097152, 1);

	    fh.setFormatter(new SimpleFormatter());
	    Logger.getLogger(LOGGER_NAME).addHandler(fh);
	  }

	  public void stopSelendroid() {
	    if (server != null) {
	      server.stop();
	    }
	  }
}
