package io.selendroid.server.model;

import java.util.Collections;
import java.util.Map;

import io.selendroid.SelendroidCapabilities;
import io.selendroid.SelendroidConfiguration;
import io.selendroid.android.AndroidApp;
import io.selendroid.android.AndroidDevice;
import io.selendroid.exceptions.AndroidDeviceException;
import io.selendroid.exceptions.AndroidSdkException;

public class MyDriver extends SelendroidStandaloneDriver{

	public MyDriver(SelendroidConfiguration serverConfiguration)
			throws AndroidSdkException, AndroidDeviceException {
		super(serverConfiguration);
		
	}
	
	public AndroidDevice getAndroidDevice(SelendroidCapabilities caps) throws AndroidDeviceException{
		return super.getAndroidDevice(caps);
	} 
	
	public Map<String, AndroidApp> getConfiguredApps() {
		this.getSupportedDevices();
	    return super.getConfiguredApps();
	}

}
