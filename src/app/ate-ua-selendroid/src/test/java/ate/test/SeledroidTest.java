package ate.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import ate.seletestcase.DefaultTestSuite;
import ate.testcase.UATestcase;
import ate.ua.selendroid.SelendroidUA;

public class SeledroidTest extends UATestcase {
	DefaultTestSuite suite = new DefaultTestSuite();
	SelendroidUA ua;
	WebDriver driver;

	@Test
	public void basic() {
		log.info("basic");
		WebElement inputField = ua.get_driver().findElement(
				By.id("my_text_field"));
		inputField.sendKeys("Selendroid");
		assertTrue("Selendroid".equals(inputField.getText()));
	}

	// @Test
	public void shouldSearchWithEbay() {
		// And now use this to visit ebay
		driver.get("http://m.ebay.de");

		// Find the text input element by its id
		WebElement element = driver.findElement(By.id("kw"));

		// Enter something to search for
		element.sendKeys("Nexus 5");

		// Now submit the form. WebDriver will find the form for us from the
		// element
		element.submit();

		// Check the title of the page
		System.out.println("Page title is: " + driver.getTitle());
		driver.quit();
	}

	// @Test
	public void assertUserAccountCanRegistered() throws Exception {
		// Initialize test data
		UserDO user = new UserDO("u$erNAme", "me@myserver.com", "mySecret",
				"John Doe", "Python");

		registerUser(user);
		verifyUser(user);
	}

	private void registerUser(UserDO user) throws Exception {
		driver.get("and-activity://io.selendroid.testapp.RegisterUserActivity");

		WebElement username = driver.findElement(By.id("inputUsername"));
		username.sendKeys(user.username);

		driver.findElement(By.name("email of the customer")).sendKeys(
				user.email);
		driver.findElement(By.id("inputPassword")).sendKeys(user.password);

		WebElement nameInput = driver.findElement(By
				.xpath("//EditText[@id='inputName']"));
		assertTrue("Mr. Burns".equals(nameInput.getText()));
		nameInput.clear();
		nameInput.sendKeys(user.name);

		driver.findElement(By.tagName("Spinner")).click();
		driver.findElement(By.linkText(user.programmingLanguage)).click();

		driver.findElement(By.className("android.widget.CheckBox")).click();

		driver.findElement(By.linkText("Register User (verify)")).click();
		assertTrue("and-activity://VerifyUserActivity".equals(driver.getCurrentUrl()));
	}

	private void verifyUser(UserDO user) throws Exception {
		assertTrue(user.username.equals(driver.findElement(By.id("label_username_data"))
				.getText()));
		assertTrue(user.email.equals(driver.findElement(By.id("label_email_data"))
				.getText()));
		assertTrue(user.password.equals(driver.findElement(By.id("label_password_data"))
				.getText()));
		assertTrue(user.name.equals(driver.findElement(By.id("label_name_data"))
				.getText()));
		assertTrue(user.programmingLanguage.equals(
				driver.findElement(
						By.id("label_preferedProgrammingLanguage_data"))
						.getText()));
		assertTrue("true".equals(driver.findElement(By.id("label_acceptAdds_data"))
				.getText()));
	}

	@BeforeSuite
	public void beforesuite() {
		suite.BeforeSuite("selendroid-test-app-0.8.0.apk");
	}

	@AfterSuite
	public void aftersuite() {
		suite.AfterSuite();
	}

	@BeforeClass
	public void beforeclass() {
		super.BeforeClass();
		ua = this.create_ua("selendroid:io.selendroid.testapp:0.8.0",
				new SelendroidUA());

		assertTrue("io.selendroid.testapp:0.8.0".equals(ua.get_app_id()));
		ua.startup();
		driver = ua.get_driver();
	}

	@AfterClass
	public void afterclass() {
		super.AfterClass();
		ua.teardown();
	}

}
