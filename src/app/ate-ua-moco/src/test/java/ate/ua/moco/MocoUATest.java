package ate.ua.moco;

import org.apache.http.client.fluent.Content;
import org.testng.annotations.*;

import ate.testcase.CsUATestcase;

public class MocoUATest extends CsUATestcase{

	@Test
	public void file(){
		MocoUA s=this.create_server("moco://127.0.0.1:12345",MocoUA.class);
		MocoUA c=this.create_client("moco://127.0.0.1:12345",MocoUA.class);		
		s.bind_by_file("src/test/foo.json");
		this.start_all_ua();
		
		Content o = c.get("/");
		assertTrue(o!=null);
		assertTrue(o.asString().equals("Hello, Moco"));		
	}
	
	@Test
	public void test(){
		MocoUA s=this.create_server("moco://127.0.0.1:12345",MocoUA.class);
		MocoUA c=this.create_client("moco://127.0.0.1:12345",MocoUA.class);		
		this.start_all_ua();
		
		Content o = c.get("/");
		assertTrue(o!=null);
		assertTrue(o.asString().equals("Hello, Moco"));
		
		s.response("for test");
		o = c.get("/");
		assertTrue(o!=null);
		assertTrue(o.asString().equals("for test"));
		
		s.bind_by_string("[{\"response\" :{\"text\" : \"Hello, Test\"}}]\")");
		o = c.get("/");
		assertTrue(o!=null);
		assertTrue(o.asString().equals("Hello, Test"));
	}
	@AfterMethod
	public void aftermethod(){
		this.shutdown_all_ua();
	}
}
