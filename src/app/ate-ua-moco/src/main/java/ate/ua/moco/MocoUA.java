package ate.ua.moco;

import static com.github.dreamhead.moco.Moco.httpserver;
import static com.github.dreamhead.moco.Runner.runner;
import static com.google.common.base.Optional.fromNullable;
import static com.google.common.collect.FluentIterable.from;
import static com.google.common.collect.ImmutableList.of;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;

import ate.AbstractURIHandler;
import ate.ua.RunFailureException;
import ate.ua.UAOption;

import com.github.dreamhead.moco.HttpServer;
import com.github.dreamhead.moco.internal.ActualHttpServer;
import com.github.dreamhead.moco.runner.JsonRunner;
/**
 * 
 * 
 * @author Administrator
 * @see {@link https://github.com/dreamhead/moco} {@link http
 *      ://www.infoq.com/cn/news/2013/07/zhengye-on-moco}
 */
public class MocoUA extends AbstractURIHandler {
	public JsonRunner runner;
	String prefix = "http";

	@Override
	public MocoUA build(UAOption option, Object value) {
		super.build(option, value);
		if (option == this.UA_TYPE) {
//			if (type == this.SERVER) {
//				server = httpserver(port);
//			}
		} else if (option == this.UA_URI) {
			if (port <= 0)
				port = 80;
		}

		return this;
	}

	@Override
	public void teardown() {
		HttpServer server = httpserver(12306);
//        server.response("foo");
        runner(server);
//        runner.start();
		
		if (runner != null)
			runner.stop();
	}
	/**
	 * 
	 * @param content JSON字符串
	 */
	public void bind_by_string(String content) {
		bind(new ByteArrayInputStream(content.getBytes(charset)));
	}
	
	/**
	 * 文件路径
	 * @param filename
	 */
	public void bind_by_file(String filename) {
		try {
			bind(new FileInputStream(new File(filename)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void bind(InputStream in) {
		if(runner==null){
			runner = JsonRunner
					.newJsonRunnerWithStreams(of(in), fromNullable(port));
			
		}else{
			HttpServer ss=runner.createHttpServer(from(of(in)).transform(JsonRunner.toRunnerSetting()), fromNullable(port));
			runner.get_moco_runner().replace_server((ActualHttpServer)ss);
		}
	}
	
	@Override
	public void startup() {
		prefix += "://" + host + ":" + port;
		if (type == this.SERVER) {
			if(runner==null)
				bind_by_string("[{\"response\" :{\"text\" : \"Hello, Moco\"}}]\")");
			runner.run();
		}
	}
	
	public void response(String content) {
		HttpServer server = httpserver(port);
        server.response(content);
        runner.get_moco_runner().replace_server((ActualHttpServer)server);
        //this.server.replace_setting((ActualHttpServer)server);		
	}

	public Content get(String path) {
		String uri = prefix + path;
		try {
			return Request.Get(uri).execute().returnContent();
		} catch (Exception e) {
			throw new RunFailureException("Get failure:" + uri, e);
		}
	}

	@Override
	public String get_default_schema() {
		return "moco";
	}

	@Override
	public boolean is_ready() {
		if (type == SERVER)
			return runner != null;
		else
			return true;
	}

}
