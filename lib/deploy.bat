set URL=http://admin:admin123@localmaven:8081/nexus/content/repositories/thirdparty/
mvn deploy:deploy-file -Dfile=./snmp4j-agent-2.0.7.jar    -DgroupId=snmp4j         -DartifactId=snmp4j-agent    -Dversion=2.0.7    -Dpackaging=jar -Durl=%URL%
mvn deploy:deploy-file -Dfile=./snmp4j-2.3.0.jar          -DgroupId=snmp4j         -DartifactId=snmp4j          -Dversion=2.3.0    -Dpackaging=jar -Durl=%URL%
mvn deploy:deploy-file -Dfile=./openflowj-1.0.2.jar       -DgroupId=openflow       -DartifactId=openflow-java   -Dversion=1.0.2    -Dpackaging=jar -Durl=%URL%
mvn deploy:deploy-file -Dfile=./camel-snmp-snm4j.jar      -DgroupId=camel          -DartifactId=camel-snmp      -Dversion=1.0      -Dpackaging=jar -Durl=%URL%
mvn deploy:deploy-file -Dfile=./ojdbc6.jar                -DgroupId=com.oracle     -DartifactId=jdbc16          -Dversion=12.1.0.1 -Dpackaging=jar -Durl=%URL%
mvn deploy:deploy-file -Dfile=./gp-utils-1.15.jar         -DgroupId=gp-utils       -DartifactId=gp-utils        -Dversion=1.15     -Dpackaging=jar -Durl=%URL%
mvn deploy:deploy-file -Dfile=./nist-sdp-1.0.jar          -DgroupId=javax.sdp      -DartifactId=nist-sdp        -Dversion=1.0      -Dpackaging=jar -Durl=%URL%
mvn deploy:deploy-file -Dfile=./JavaSCTP-0.5.7.jar        -DgroupId=JavaSCTP       -DartifactId=JavaSCTP        -Dversion=0.5.7    -Dpackaging=jar -Durl=%URL%
mvn deploy:deploy-file -Dfile=./serotonin-timer-2.0.6.jar -DgroupId=com.serotonin  -DartifactId=serotonin-timer -Dversion=2.0.6    -Dpackaging=jar -Durl=%URL%
mvn deploy:deploy-file -Dfile=./serotonin-utils-2.1.3.jar -DgroupId=com.serotonin  -DartifactId=serotonin-utils -Dversion=2.1.3    -Dpackaging=jar -Durl=%URL%
mvn deploy:deploy-file -Dfile=./jlr.jar                   -DgroupId=latex.nixosoft -DartifactId=jlr             -Dversion=1.0      -Dpackaging=jar -Durl=%URL%
mvn deploy:deploy-file -Dfile=./trove.jar                 -DgroupId=trove          -DartifactId=trove           -Dversion=2.1.1    -Dpackaging=jar -Durl=%URL%

mvn deploy:deploy-file -Dfile=./mibble-parser-2.9.3.jar   -DgroupId=snmp           -DartifactId=mibble-parser   -Dversion=2.9.3    -Dpackaging=jar -Durl=%URL%