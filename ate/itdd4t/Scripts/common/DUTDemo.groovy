package common;
import ate.util.*
import ate.rm.dev.*
import ate.Global
/**
 * DUT Demo 
 * @author huangxy
 *
 */
def class DUTDemo extends DUT{
    def db
    def static main(args){
        def nfe=new DUTDemo("ssh://root:passwd@172.16.5.219/?section=DUT")
        Global.gbind("nfe",nfe)
        Global.gshell()
    }
    
	public DUTDemo(connstr){
		super(connstr)
	}
	
    public void quit(){
        db.teardown()
    }

}
