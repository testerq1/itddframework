package common;
import ate.util.*
import ate.rm.dev.*

/**
 * XGE的类 
 * @author huangxy
 *
 */
def class XGE extends DUT{

    public  static void main(args){
        XGE xge=new XGE("ssh://root:NTc%402010@172.16.5.204:2222/?section=XGE")
        xge.clear_session()
        xge.quit()
    }

    public XGE(connstr){ 
        super(connstr)
    }
    /**
     * 配置全局流控策略用于协议识别
     * @return
     */
    def security_policy_all(){
         vt.send_and_expect("config no security-policy all")
         vt.send_and_expect("config security-policy add name 1 src-if-group ANY dst-if-group ANY src-addr-type ip src-addr ANY dst-addr-type ip dst-addr ANY action permit service ANY status enable direction end")     
    }
    /**
     * 清楚XGFE会话
     * @return
     */
    def clear_session(){
         vt.send_and_expect("config ad clear session")
       
    }  
    /**
     * 配置App_name阻挡策略
     * @return
     */
    def deny_app(app_name){
        def im =[
            "MSN",
            "AIM",
            "YMSG",
            "QQ",
            "Jabber",
            "PoPo",
            "ET",
            "UC",
            "WangWang",
            "Fetion",
            "BAIDUHI",
            "GuaGua",
            "YYYuyin",
            "Wechat",
            "MOBLILE_QQ",
            "MoMo",
            "WangXin",
            "Mobile_MSN",
            "FeiLiao",
            "PC_Miliao", 
            "Mobile_Miliao",
            "MobileYiXin",
            "RenrenDesktop",
            "Mobile_YY",
            "SINASHOW",
            "Gtalk",
            "laiwang",
            "9158SHOW",
            "Mobileqq_Voice",
            "QQTalk",
            "woyou",
            "guaguacaijing",
            "qixivideo"
        ];          //im的集合(im的阻挡和其他应用的阻挡策略配置不一样)
           vt.send_line("config no security-policy all") //清除流控策略
           if (im.contains(app_name))
           {
               vt.send_line("config security-profile im_profile_add name $app_name")
               vt.send_line("config security-profile im_service_add name $app_name svc_name $app_name text-chat enable voice-chat enable file-transfer enable log disable")
               vt.send_line("config security-profile p2p_profile_add name $app_name session_num 0")
               vt.send_line("config security-profile im_profile name $app_name im-profile-name $app_name im-profile_status enable")
               vt.send_and_expect("config security-policy add name $app_name src-if-group ANY dst-if-group ANY src-addr-type ip src-addr ANY dst-addr-type ip dst-addr ANY action permit service ANY safe-config $app_name status enable direction end")              
           }
           else{
               vt.send_line("config security-profile p2p_profile_add name $app_name session_num 0")
               vt.send_line("config security-profile p2p_service_add name $app_name svc_name $app_name block enable bandwidth-control disable qos_name no session-control disable max-new-session-num 0 p2p-detection disable")
               vt.send_line("config security-profile p2p_profile name $app_name p2p-profile-name $app_name p2p-profile_status enable")
               vt.send_and_expect("config security-policy add name $app_name src-if-group ANY dst-if-group ANY src-addr-type ip src-addr ANY dst-addr-type ip dst-addr ANY action permit service ANY safe-config $app_name status enable direction end")               
           }
        
    }
    /**
     * 恢复配置，清楚session
     * @return
     */
    def replay_config(app_name){
        vt.send_line("config no security-policy all")
        vt.send_line("config no security-profile name $app_name")
        vt.send_line("config ad clear session")
        vt.send_and_expect("config save")
        
    }
    /**
     * XGE 输入命令并有返回值
     * @param cmd
     * @return
     */
    def send_and_expect(cmd){
        return  vt.send_and_expect(cmd)
    }
    /**
     * XGE 输入命令没有返回值
     * @param cmd
     * @return
     */
    def send_line(cmd){
        vt.send_line(cmd)
    }
    /**
     * 查询XGFE的软件版本号
     * @return 查询XGFE的软件版本号
     */

    def version_soft(){
        def XGE_ver=vt.send_and_expect("cat /usr/private/Firmware_version");
        println  "XGE_ver=="+   XGE_ver
        String []  v=XGE_ver.split("-")
        String []  v1=v[1].split(",")
        def XGE_ver_num=v1[0]
        return XGE_ver_num
    }
    /**
     * 查询XGFE的硬件版本号
     * @return 查询XGFE的硬件版本号
     */
    def XGE_hard_ver(){
        def XGE_hard_ver=vt.send_and_expect("fdebug -k indirAccBurstRead -l0x1 -l0x1")
        def h=expect.groups(XGE_hard_ver,"addr 0x00000001\\s---------\\s0x(\\d+)")
        def h1="20"+h[1]
        return h1
    }
    /**
     * 查询硬件修改说明version.txt里面的最新版本号
     * @param txt_line TestSuiteREPdai.groovy 里传参数
     * @return 返回version.txt的最新版本号
     */
    def XGE_txt_ver(txt_line){
        String [] t=txt_line.split("--")
        String [] t1=t[1].split(" ")
        String [] t2=t1[0].split("[.]")
        def t5=t2[0]+t2[1]+t2[2]+t2[3]
        return t5
    }
    /**
     * XGFE 软件升级
     * @param from 远程服务器194存放XGE最新软件的地址
     * @param to   XGE 软件版本
     * @return     
     */

    def XGE_update(from,to){
        def word="scp  root@$from /usr/download/$to";
        println word
        vt.set_timeout(60)
        vt.send_and_expect(word);
        vt.send_line(vt.get_passwd());
        vt.send_and_expect("cd /usr/download");
        vt.send_and_expect("pwd")
        vt.send_and_expect("ace_upsys "+to);
    }
    /**
     * XGFE 硬件升级
     * @param from 远程服务器194存放XGE最新硬件的地址
     * @param to   XGE 硬件版本
     * @return
     */
    def XGE_hard_update(from,to){
        def word="scp  root@$from /usr/private/$to"
        println word
        vt.send_and_expect(word);
        vt.send_and_expect(vt.get_passwd());
        vt.send_and_expect("ls")
        def word3="fdebug -k ace_update_hardcore -s/usr/private/$to"
        def p2= vt.send_and_expect(word3)
    }
    /**
     * XGE 重启
     * @return
     */

    def XGE_reboot(){
        vt.send_and_expect("config save")
        vt.send_and_expect("reboot")
        quit();
        sleep(350000);
        restart();
    }

    def enter_mode(mode){
        vt.send_line(mode);
    }
}