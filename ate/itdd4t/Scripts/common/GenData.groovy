package common
import java.net.*;
import ate.util.*

/**
 * The Class GenData.
 */
public class GenData extends GenData0{	
	/**
	 * Incr ip. 不包含beginIp
	 *
	 * @param beginIp the begin ip
	 * @param step the step
	 * @param cnt the cnt
	 * @return the string[]
	 * @throws UnknownHostException the unknown host exception
	 */
	
	public static String[] incrIp(String beginIp,int step, int cnt) throws UnknownHostException{
		String[] tmp=new String[cnt];
		int v = X2X.ip2i(beginIp);
		
		byte[] bs=new byte[4];
		for(int i=1;i<=cnt;i++){
			bs=X2X.int2b(v+i, 4);
			tmp[i-1]=InetAddress.getByAddress(bs).getHostAddress();
			//System.out.println(tmp[i-1]);
		}
		return tmp;
	}
	/**
	 * incrMac("00-00-%s-%s-%s-%s",["1.1.1.1","1.1.1.2"])
	 * @param format
	 * @param ip
	 * @return
	 */
	public static String[] incrMac(format,ip){
		String[] macs=new String[ip.size()];
		for(int i=0;i<macs.length;i++)
			macs[i]=genHexStringFromIp(format,ip[i])
		return macs
	}
	/**
	 * incrMac("00-00-%s-%s-%s-%s",10)
	 * @param format
	 * @param ip
	 * @return
	 */
	public static String[] incrMac(String format,int cnt){
		String[] macs=new String[cnt];
		int n=format.split("%").length
		String[] hs= new String[n];
		
		for(int i=0;i<hs.length;i++){
			int j=random(1,255)
			hs[i]=Integer.toHexString(j);
			int len=hs[i].length();
			if(len==1)
				hs[i]="0"+hs[i]
			else if(len>2)
				hs[i]=hs[i].substring(len-2);	
		}
			
		for(int i=0;i<macs.length;i++)
			macs[i]=String.format(format, hs[0],hs[1],hs[2], hs[3]);
		return macs
	}
	/**
	 * Gen string from ip.
	 * Usage:
	 * 		genStringFromIp("00-00-%s-%s-%s-%s","10.11.12.13")
	 *
	 * Return  00-00-10-11-12-13
	 *
	 * @param format the format
	 * @param ip the ip
	 * @return the string
	 * @throws UnknownHostException the unknown host exception
	 */
	public static String genStringFromIp(String format,String ip) throws UnknownHostException{
		byte[] bs = InetAddress.getByName(ip).getAddress();
		String[] hs= new String[bs.length];
		for(int i=0;i<hs.length;i++){
			if(bs[i]==0)
				hs[i]="0"+bs[i];
			else if(bs[i]>0)
				hs[i]=""+bs[i];
			else
			    hs[i]=""+(bs[i]+256);
		}
		return String.format(format, hs[0],hs[1],hs[2], hs[3]);
	}
	
	/**
	 * Gen hex string from ip.
	 * Usage:
	 * 		genHexStringFromIp("00-00-%s-%s-%s-%s","10.11.12.13")
	 *
	 * Return  00-00-a-b-c-d
	 *
	 * @param format the format
	 * @param ip the ip
	 * @return the string
	 * @throws UnknownHostException the unknown host exception
	 */
	public static String genHexStringFromIp(String format,String ip) throws UnknownHostException{
		byte[] bs = InetAddress.getByName(ip).getAddress();
		String[] hs= new String[bs.length];
		for(int i=0;i<hs.length;i++){
			hs[i]=Integer.toHexString(bs[i]);
			int len=hs[i].length();
			if(hs[i].length()==1)
				hs[i]="0"+hs[i]
			else if(hs[i].length()>2)
				hs[i]=hs[i].substring(len-2);			
		}
		return String.format(format, hs[0],hs[1],hs[2], hs[3]);
		
	}
	
	public static void main(String[] args) throws Exception{
		//incrIp("129.0.0.127",1,10000);
		println "中国"
		println incrIp("1.0.0.0",2,10)
		println genHexStringFromIp ("00-01-%s-%s-%s-%s","12.34.56.78")
		System.out.println(genHexStringFromIp("00 00 %s %s %s %s","120.130.140.150"));
		
	}
}
