package common;
import ate.util.*
import ate.rm.dev.*


/**
 * Reporter的类 
 * @author huangxy
 *
 */
def class REP extends DUT{
    def db,dbs_last
    public  static void main(args){
        REP rep=new REP("ssh://root:NTc%402010@172.16.5.197/?section=REP")
        rep.raw_database()
        rep.raw_tables()
        rep.test()
        rep.quit()
    }

    public REP(connstr){
        super(connstr)
        db= create_ua("mysql://root:123456@${vt.get_host()}:13361/")
        db.startup()
    }
    /**
     * 获取stat_raw的数据库
     * @return dbs_last
     */
    def raw_database(){
        def dbs_name=[
            "p-NFE-stat-raw-2014"]                                     //5分钟数据库名字的关键字
        def dbs=new ArrayList();
        def rs
        rs=db.getDatabases()

        rs.each{
            //获取有关键字的数据库
            if (it.startsWith(dbs_name)){
                dbs.add(it)
            }
        }
        dbs_last=dbs.last()
        println "dbs_last=="+dbs_last
        return dbs_last
    }
    /**
     * 获取最后一张表dbs_last中的带关键字的表
     * @return
     */
    def raw_tables(){
        def tables_name=[
            "area_complex",
            "complex",
            "ip_complex"
        ];               //5分钟有service字段表的关键字
        def rs1
        def tables=[:]
        rs1 = db.getTables(dbs_last)                                    //获取最后一个数据库的所有表
        println "rs1=="+rs1
        rs1.each {
            for (int i=0;i<tables_name.size();i++){
                if (it.startsWith(tables_name[i])){
                    tables.put(tables_name[i], it)
                    break
                }
            }
        }
        println "tables=="+tables
        return tables
    }
    /**
     * 数据库的查询语句的输入
     * @param cmd
     * @return
     */
    def query(cmd){
        db.query(cmd)
    }
    def test(){
        def rset2=db.query("select count(*) from `p-NFE-stat-raw-2014-07-18`.`area_complex-16` where service='QQ'")
        assert(rset2!=0)
    }
    /**
     * 查询reporter 版本号
     * @return reporter版本号
     */
    def get_firmware_version(){
        def ver=vt.send_and_expect("cat /usr/private/Firmware_version");
        println ver
        String []  o=ver.split("-")
        String []  o1=o[1].split(",")
        def old_ver_num=o1[0]
        return  old_ver_num

    }
    /**
     * reporter 升级
     * @param from 远程服务器194存放reporter版本的路劲
     * @param to   reporter
     * @return
     */
    def upgrade(from,to){
        def word="scp root@$from /usr/download/$to";
        println word
        vt.set_timeout(120)
        vt.send_and_expect(word);
        vt.send_line(vt.get_passwd());
        vt.send_line("cd /usr/download");
        vt.send_line("pwd")
        vt.send_and_expect("ace_upsys "+to, "Starting lighttpd.*OK");
        vt.send_and_expect("")
        vt.set_timeout(15)

    }

    def enter_mode(mode){
        vt.send_line(mode);
    }
    /**
     * 退出db
     */
    public void quit(){
        super.quit()
        db.teardown()
    }
}