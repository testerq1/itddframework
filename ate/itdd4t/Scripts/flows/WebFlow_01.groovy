import ate.testcase.*
import com.thoughtworks.selenium.Selenium;

import org.testng.annotations.*
import org.testng.TestNG
import org.testng.TestListenerAdapter
import static org.testng.AssertJUnit.*;
import java.lang.reflect.*;
import java.util.*
import org.openqa.selenium.*
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.remote.*

public class WebFlow_01 extends Testcase{
	
	
	@BeforeClass
	public void BeforeClass(){
		//driver = new FirefoxDriver();
		
		//selenium = new WebDriverBackedSelenium(driver, baseUrl);
		super.BeforeClass();
	}
	@AfterClass
	public void AfterClass(){
		super.AfterClass();
	}
	@BeforeMethod
	public void BeforeMethod(Method m){ 
		super.BeforeMethod(m);
	}
	@AfterMethod
	public void AfterMethod(Method m){
		super.AfterMethod(m);
	}
	/**
	*
	*/
	@Test(dataProvider="Data_Func_Helloworld")
	final void Test_Func_Helloworld(para){
		log.info "${STEP}.1. Test_Func_Helloworld ${para}"
		WebDriver driver =null;
		switch ( para ) {
			case "ie":
				DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
				ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				ieCapabilities.setJavascriptEnabled(true);
				driver=new InternetExplorerDriver(ieCapabilities);
				break		
			case "firefox":
				driver = new FirefoxDriver()
				break
			case "httpunit":
				driver = new HtmlUnitDriver()
				break
			case "rm":
				// Any driver could be used for test
				DesiredCapabilities capabilities = new DesiredCapabilities();
				// Enable javascript support
				capabilities.setJavascriptEnabled(true);
		
		        // Get driver handle
				driver = new RemoteWebDriver(capabilities);
				break
			default:
				result = "default"
		}
//		driver.get("http://bulgaria.craigslist.org/");
//		System.out.println("IE Start: " + Calendar.getInstance().getTime().toString());
//		System.out.println(driver.findElements(By.xpath("//a[@href]")).size());
//		System.out.println("IE Stop: " + Calendar.getInstance().getTime().toString());
//		
		driver.get("http://www.baidu.com");
		System.out.println("IE Start: " + Calendar.getInstance().getTime().toString());
		WebElement we=driver.findElement(By.xpath("//input"))
		we.sendKeys("ahsdkjadkjashd")
		System.out.println("IE Stop: " + Calendar.getInstance().getTime().toString());
		
	}

}