import java.lang.reflect.Method;
import java.util.HashMap;
import org.testng.annotations.*
import ate.testcase.*
import ate.ua.raw.*;
import common.*

public class GtpTest extends CsUATestcase {
	@Test
	public void raws(){
		def svr= create_server("gtp://"+siip+":2152/udp");
		def clt= create_client("gtp://"+sip+":2152/udp");
		start_all_ua();
		
		def pr=new PcapReader("pcap.pcap");
		pr.set_matcher(PcapReader.BY_SIP,"172.16.5.113");
		pr.start_read();
				
		def msg=clt.read_request_from_template("gtpu");		
		def pkt=pr.get_packet();		
		while(pkt!=null){
			def data=msg.getDataPdu();
			data.setData("h01020300");
			pkt.set_mtu(1460);
						
			Object resp=null;			
			if(pkt.is_uac()){
				data.appendData(pkt.get_payload(HA.SIP,"1.1.1.1"));
				clt.send_message(msg);
				resp=svr.recv_message();				
			}else{
				data.appendData(pkt.get_payload(HA.DIP,"1.1.1.2"));
				svr.send_message(msg);
				resp=clt.recv_message();
			}	
			assertTrue(resp!=null);		
			
			pkt=pr.get_packet();
		}		
		pr.close();
	}
	
	//@Test
	public void gtp_u() throws Exception{
		def svr= create_server("gtp://"+siip+":2152/udp");
		def clt= create_client("gtp://"+sip+":2152/udp");
		start_all_ua();
		def msg=clt.read_request_from_template("gtpu");
		clt.send_message(msg);
		Object o = svr.recv_message();
		assertTrue(o!=null);
	}
	
	//@Test
	public void gtp_u_edit() throws Exception{
		def ua= create_client("gtp://$sip:2152/udp")
		
		def msg=ua.read_request_from_template("gtpu");
		def h=msg.getHeader();
		h.setTunnelEndpointId(123456l);
		
		def data=msg.getDataPdu();
		data.setData([0x1,0x2] as byte[]);
		data.appendData([0x3,0x4] as byte[]);
		assertTrue(data.getData().length==4);
		assertTrue(data.getData()[3]==4);
		
		data.setData("h0102");
		data.appendData("h0304");
		assertTrue(data.getData().length==4);
		assertTrue(data.getData()[3]==4);
	}
	
	//@Test
	public void gtp_c(){
		log.info("test");
		def svr= create_server("gtp://$siip:2123/udp")
		def clt= create_client("gtp://$sip:2123/udp")
		start_all_ua();
		
		def msg=clt.read_request_from_template("create_session");				
		clt.send_message(msg);		
		
		def o = svr.recv_message();		
		def resp=svr.create_response_from_emplate(msg, "create_session");		
		svr.send_message(resp);		
		o=clt.recv_message();		
		assertTrue(o!=null);		
	}	
	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		// set_log_level(DEBUG);
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
