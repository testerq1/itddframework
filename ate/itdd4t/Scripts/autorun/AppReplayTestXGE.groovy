import java.lang.reflect.Method;
import org.testng.annotations.*;
import org.testng.annotations.Test;
import ate.testcase.*;
import ate.ua.raw.RawSocketUA;
import ate.ua.ssh.SSHUA;
import common.*;

public class AppReplayTestXGE extends RawUATestcase {
    def path="pcap";   
    def uac,uas,dut,REP,db,curr_method,replay,handler,up,down
    def map=get_map("$path/map.properties")

    @BeforeMethod
    public void beforeMethod(Method m) {
        if(curr_method==null){

            curr_method=m.getName()
        }
        else if(curr_method!=m.getName()){
            sleep(300000)
            curr_method=m.getName()
        }
        super.BeforeMethod(m);
    }

    @AfterMethod
    public void afterMethod(Method m) {
        replay.reset()
        super.AfterMethod(m);
    }


    @BeforeClass
    public void beforeClass() {
        super.BeforeClass();

        uac = create_ua("raw://$IP1");
        uas = create_ua("raw://$IP2");
        dut = get_device("XGE")
        db =  get_device("REP");

        uas.enable_capture(true)

        log.info("创建一个过滤器jitter,模拟网络抖动")
        def filter = create_io_filter(RawSocketUA.FILTER.Jitter);
        filter.set_min(20);
        filter.set_max(50);

        uac.add_first_io_filter(filter);
        uas.add_first_io_filter(filter);
        start_all_ua();

        log.info("创建回放器，按照IP地址运行")
        replay = create_replayer(uac, uas);
        handler = create_handler(replay,RawUATestcase.REPLAY.DIRECTIP4);
    }

    @AfterClass
    public void afterClass() {
        super.shutdown_all_ua();
        super.AfterClass();
    }



    def findPcapFiles(path) {
        def result=new ArrayList();
        new File(path).eachFileMatch(~/.*\.pcap/) {
            def tmp=it.name.split("_")
            if(map.get(tmp[0])!=null)
                result.add([it] as File[])
        }

        log.info("Total Pcap Files:"+result.size())
        return result.toArray();
    }



    @DataProvider(name="Data_TestStep")
    public Object[][] Data_TestStep(){
        return findPcapFiles(path)
    }


    def findApp(app_name){
        STEP("查询应用的carid")
        def rtn= dut.send_and_expect("fdebug -k sc_show_service_control_by_name -s$app_name")
        
        println rtn
        def gs=expect.groups(rtn,"carid\\s=\\s(.*)");
        def car_id=gs[1]
        log.info("car_id= {}",car_id);

        STEP("查询服务上下行的流量")

        def rtn1=dut.send_and_expect("fdebug -l show_service_flow -l$car_id")
       
        println rtn1
        def gs1=expect.groups(rtn1,"up\\s=\\s(\\d+)kbyte\\sdn\\s=\\s(\\d+)kbyte");
        up=gs1[1]
        down=gs1[2]
        log.info("up={}",up)
        log.info("down={}",down)
        return up+" "+down
        
    }

    def count(){
        println "uas rcvd: "+uas.get_rcvd_count()
        println "uas snd: "+uas.get_snd_count()
        println "uac snd: "+uac.get_snd_count()
    }


    @Test(dataProvider="Data_TestStep")
    public void SVT_XGE_REPLAYBYIP_FUNC_01(file) {
        //测试XGFE上的应用是否被识别,阻挡
        STEP("配置全局流控策略")
        dut.security_policy_all()

        STEP("回放文件:$file")

        def tmp=file.name.split("_")
        def app_name=tmp[0]
        def pcap_sip=tmp[1]
        def app_id=map.getProperty(app_name)

        handler.set_sip(pcap_sip);
        replay.replay(file.absolutePath, handler);
        assertTrue(handler.get_seq()> 0);
        count()
        assertTrue(findApp(app_name)!="0 0")

        STEP("设备清除Session")
        dut.clear_session()

        STEP("配置阻挡该应用:$app_name")
        dut.deny_app(app_name)

        STEP("重新回放文件")

        replay.reset()
        replay.replay(file.absolutePath, handler);
        assertTrue(handler.get_seq() > 0);

        STEP("确认该app Session被阻挡")

        count()
        assertTrue(findApp(app_name)=="0 0")
        
        assertTrue((uas.get_rcvd_count()-uas.get_snd_count()) < uac.get_snd_count()/2,
                "没有阻挡该应用的Session： ${uac.get_snd_count()} ${uas.get_rcvd_count()}",);

        STEP("DUT上恢复配置，清除Session")
        dut.replay_config(app_name)


    }
    @Test(dataProvider="Data_TestStep")                                   //测试Reporter上stat-raw数据(5分钟)正确
    public void SVT_XGE_REPLAYBYIP_FUNC_02(file) {
        def tables_name=[
            "area_complex",
            "complex",
            "ip_complex"
        ];
        STEP("根据pcap文件名获得app_name")

        def tmp=file.name.split("_")
        def app_name=tmp[0]
       
        STEP("得到5分钟最后一个数据库")
                     
        
        
            
        

        STEP("判断表中的service的count是否为0")

        tables_name.each {
              def base= db.raw_database()
            def table= db.raw_tables(tables_name)[it]
            if (table.contains("area_complex")) {
                println "select count(*) from `$base`.`$table` where service='$app_name' "
                def rset1=db.query("select count(*) from `$base`.`$table` where service='$app_name' ");
                assertTrue(rset1.next());
                assertTrue(rset1.getInt(1)!=0)

            }
            else if (table.contains("complex")){
                println "select count(*) from `$base`.`$table` where service='$app_name' "
                def rset2=db.query("select count(*) from `$base`.`$table` where service='$app_name'")
                assertTrue(rset2.next());
                assertTrue(rset2.getInt(1)!=0)

            }
            else if (table.contains("ip_complex")){
                println "select count(*) from `$base`.`$table` where service='$app_name' "
                def rset3=db.query("select count(*) from `$base`.`$table` where service='$app_name'")
                assertTrue(rset3.next())
                assertTrue(rset3.getInt(1)!=0)

            }
        }
    }

}


