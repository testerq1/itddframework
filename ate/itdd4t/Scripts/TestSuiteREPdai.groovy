import org.testng.annotations.*;
import ate.testcase.*
import ate.rm.dev.*
import common.*;
import ate.util.*;
import java.io.File;
import ate.*

public class TestSuiteREPdai extends ATestSuite{
    def tb,current_testsuite,tmp
    def rep_IP=   arg("REP.IP");
    def rep_port= arg("REP.MYSQL_PORT")
    def AUTH=     arg("REP.AUTH");
    def XGE_IP=   arg("XGE.SSH_IP")
    def XGE_PORT= arg("XGE.SSH_PORT")
    def XGE_AUTH= arg("XGE.AUTH")
    def REP,XGE,smb

    @BeforeSuite
    public void BeforeSuite(){
        super.BeforeSuite()
        tb=Testbed.createTestbed()

        tb.device_registe("HOST_WIN",new Win())
        tb.device_registe("HOST_LIN",new Lin())
        
        
        REP=new REP("ssh://$AUTH@$rep_IP/?section=REP")
        tb.device_registe("REP",REP)        
        
        
        
        XGE=new XGE("ssh://$XGE_AUTH@$XGE_IP:$XGE_PORT/?section=XGE")
        tb.device_registe("XGE",XGE)
      
      
       
        reinit_static()
    }
    /**
     * reporter 升级
     */
    public void update_reporter(){
            if(smb==null){
                smb=new CifsUtil("smb://dongdong.dai:Dongd%40D@172.16.161.243");
            smb.startup();
        }
       
        def rep_path='/home/ci/Reporter/'       

        def local=null;
        new File(rep_path).listFiles().each{
            if(local==null||local.getLastModified()<it.getLastModified())
                local=it;
            println "local=="+local
        }
        def fi=smb.get_smb_file("/Test_Version/reporter_for_wlan/64bit/");
        def rmt=null;
        fi.listFiles().each{
            if(rmt==null ||rmt.getLastModified()<it.getLastModified())
            {
                rmt=it
            }
        }     
        def target;
        rmt.listFiles().each{
            println it.getName()
            if(it.isFile()&&it.getName().endsWith(".bin")){
                target=it;
            }else if(it.isDirectory()&&it.getName().startsWith("NETOP")){
                it.listFiles().each{tmp->
                    if(tmp.isFile()&&tmp.getName().endsWith(".bin"))
                        target=tmp;

                }
            }
        }
        println "target=="+target
        if(target==null){
            log.info("不存在升级文件")
            return
        }       
        smb.copy(smb.get_smb_file(target.getPath()), rep_path+rmt.getName());         
        def old=REP.get_firmware_version();         
        assert(old!=null);
        def dstfile=rep_path+rmt.getName()+target.getName()
        String [] new_ver= target.getName().split("-")
        def new_ver_num=new_ver[1]      
          if(old.compareTo(new_ver_num)<=0) {             
               REP.upgrade("172.16.5.194:"+dstfile,target.getName())            
               if (REP.get_firmware_version().equals(new_ver_num)){
                  println "update success"
               }else{
                    println "update failed"
               }
          }else
                println "没有新版本，不需要升级"    
     

    }
   /**
    * XGE 升级
    */
    
    public void update_XGE(){
          if(smb==null){
                smb=new CifsUtil("smb://dongdong.dai:Dongd%40D@172.16.161.243");
            smb.startup();
        }
        
         def XGE_soft_path='/home/ci/XGE/software/'
         def XGE_hard_path='/home/ci/XGE/hardware/'        
 
         def XGE_soft_local=null;
         def XGE_hard_local=null;
        new File(XGE_soft_path).listFiles().each{
            if(XGE_soft_local==null || XGE_soft_local.getLastModified()<it.getLastModified())
                XGE_soft_local=it;            
        } 
         new File(XGE_hard_path).listFiles().each{
             if(XGE_hard_local==null || XGE_hard_local.getLastModified()<it.getLastModified())
                 XGE_hard_local=it;            
         }
         
         def fi_xge=smb.get_smb_file("/Test_Version/XGE64_NEW/TRUNK/WIFI/");
         def fi_hard=smb.get_smb_file("/Test_Version/asic_fpga/xge/ipv6_DPI/XGFE2013/");  
         def rmt_xge=null;
         def rmt_hard=null;
       fi_xge.listFiles().each{                                   
             if(it.getName().endsWith(".bin")){
           if(rmt_xge==null ||rmt_xge.getLastModified()<it.getLastModified())
           {
               rmt_xge=it
           }
         }
        }
          fi_hard.listFiles().each{                           
                 
             if(rmt_hard==null ||rmt_hard.getLastModified()<it.getLastModified())
             {
                 rmt_hard=it
             }
           
         }
         def target_hard
         rmt_hard.listFiles().each{
            if(it.isFile() && it.getName().endsWith(".bin"))
               target_hard=it
        
        }
         def target_txt
         rmt_hard.listFiles().each{
            if(it.isFile() && it.getName().endsWith(".txt"))
               target_txt=it
        
        }
         println "target_txt.getName()=="+target_txt.getName()
        
         
         println "rmt_hard.getName()=="+rmt_hard.getName()
         println "rmt_hard.getPath()=="+rmt_hard.getPath()
         println "target_hard.getName()=="+  target_hard.getName() 
         smb.copy(ut.get_smb_file(rmt_hard.getPath()), XGE_hard_path+rmt_hard.getName());  
         println XGE_hard_path+rmt_hard.getName()+target_txt.getName() 
         File txt=new File(XGE_hard_path+rmt_hard.getName()+target_txt.getName())
         def txt_line
         txt.eachLine{
            if (it.startsWith("Fpga1 Version:"))
              txt_line=it
             
        }
        println "txt_line=="+txt_line
        def p= XGE.XGE_txt_ver(txt_line);
        println "p=="+p
        ut.copy(ut.get_smb_file(rmt_xge.getPath()), XGE_soft_path+rmt_xge.getName()); 
        def old_ver=XGE.XGE_version()+".bin"  
        println     "old_ver=="+old_ver
        assert(old_ver!=null)
        def xge_dstfile=XGE_soft_path+rmt_xge.getName()
        println "xge_dstfile=="+xge_dstfile
        String [] new_xge_ver=rmt_xge.getName().split("-")
        def new_xge_ver_num=new_xge_ver[3]
        println "new_xge_ver_num=="+new_xge_ver_num
           
           def p1=XGE.XGE_hard_ver();
           println "p1=="+p1
              
           def xge_hard_dstfile= XGE_hard_path+rmt_hard.getName()+target_hard.getName()
           println  "xge_hard_dstfile=="+xge_hard_dstfile 
         
         if (old_ver.compareTo(new_xge_ver_num)<0){
             XGE.XGE_update("172.16.5.194:"+xge_dstfile,rmt_xge.getName())
             
                }
             else
             println "没有新的XGFE软件版本，不需要升级"
  
          if (p.compareTo(p1)<0){
            XGE.XGE_hard_update("172.16.5.194:"+xge_hard_dstfile,target_hard.getName())          
            
          }
          else
          println "没有新的XGFE软件版本，不需要升级"
          XGE.XGE_reboot()
          if(XGE.XGE_version().equals(new_xge_ver_num))
               println "update success"
               else
               println "update falied"
          if (p.equals(p1))
            println "update success"
            else
            println "update falied"
          
  
    } 
   

    @AfterSuite
    public void AfterSuite(){
        super.AfterSuite()
        REP.quit()
        XGE.quit()
    }

    public  static void main(args){
     
      TestSuiteREPdai suite=new TestSuiteREPdai();
      suite.BeforeSuite();
     // suite.update_reporter();
     // suite.update_XGE();
      suite.AfterSuite();
  }
}