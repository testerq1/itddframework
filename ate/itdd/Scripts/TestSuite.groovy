import org.testng.annotations.*;
import ate.testcase.*
import ate.rm.dev.*

public class TestSuite extends ATestSuite{	
	def tb
	
	@BeforeSuite
	public void BeforeSuite(){
		super.BeforeSuite()
		debug(true)
		
		tb=Testbed.createTestbed()
		//		nfe=new NFE()
		//		nfe.set_properties(tb.getPhyProps("NFE"))
		//		tb.device_registe("NFE",nfe)
		//		nfe.type=nfe.get("TYPE")
		//		nfe.init(LAW.T_COM)
		//		//nfe.init(1)
		//
		//		def host=new Win()
		//		host.set_properties(tb.getPhyProps("HOST"))
		//		tb.device_registe("HOST",host)
		//
		//		//nfe.update()
		//		tb.device_registe("TESTSUITE",this)
	}
	public void switchTestsuite(name){
		if(name==current_testsuite)
			return
		if(name=="1")
			testsuite_1()
			
		current_testsuite=name
	}
	
	public void testsuite_1(){
		def HOST=tb.getRes("HOST")
		def HOST_WAN_IP=HOST.get("WAN_IP")
		def rtn=HOST.ping(HOST_WAN_IP, "-n 5")
		if(rtn.contains("Packets: Sent = 5, Received = 5, Lost = 0 (0% loss)"))
			HOST.add_route(HOST_WAN_IP, "255.255.255.255", nfe.get("LAN_IP"), "1");
			
		 
//		log.info "${STEP}.3 NFE配置block所有到大网IP的ICMP流量："
//			NFE.CLI.enterMode("config")
//			NFE.CLI.exec("firewall clear policy all")
//			NFE.CLI.exec("firewall set policy from ANY to ANY src-addr ANY dst-addr $HOST_WAN_IP service \"ICMP_ANY\" action deny status enable ")
//			NFE.CLI.exec("firewall set policy from ANY to ANY src-addr ANY dst-addr ANY service \"ANY\" action permit status enable")
//

//		log.info "${STEP}.4 从小网网口ping大网网口，确认ping不到："
//			rtn=HOST.ping(HOST_WAN_IP, "-n 2")
//			assertTrue "ping ok",expect(rtn,"Packets: Sent = 2, Received = 0, Lost = 2 (100% loss)")
//
//		//@STEP 恢复环境配置
//		//@EXPECT ok
//		log.info "${STEP}.5 恢复环境配置："
	}
	
	@AfterSuite
	public void AfterSuite(){
		super.AfterSuite()
		//nfe.quit()
	}
}