import ate.pcap.*
import ate.util.*
import ate.testcase.*
import java.net.InetAddress;
import org.testng.annotations.*
import org.testng.*
import static org.testng.Assert.*;
import java.lang.reflect.*;
import org.jnetpcap.packet.*;
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.network.Ip4;

public class Replay extends PcapReplayTestcase{	
	@BeforeClass
	public void BeforeClass() {			
		create_client(get_from_ini("HOST.WAN_IP"))
		create_server(get_from_ini("HOST.LAN_IP"))				
		super.BeforeClass();
	}
	
	@Test
	void replay(){
		bySourceMac=false;
		enable_record(false)		
		replay_file("d:/replay_1.pcap")
	}	
	
	@BeforeMethod
	public void BeforeMethod(Method m){
		super.BeforeMethod(m);
	}
	@AfterClass
	public void AfterClass(){		
	   super.AfterClass();
	}
}
