import org.testng.*
import org.testng.annotations.*
import static org.testng.AssertJUnit.*;
import java.lang.reflect.*;
import ate.testcase.*

public class DB extends DBTestcase{
	@Test
	public void mysql(){
		String create_table= "create table IF NOT EXISTS test.MyEmployees3 ( "+ 
		   "   id INT PRIMARY KEY, firstName VARCHAR(20), lastName VARCHAR(20), " + 
		   "   title VARCHAR(20), salary INT )";
		
		def db = create_connection("mysql://root:123456@172.16.5.202");
		db.startup();
		assertTrue(db.execute("drop database if exists test") > -1);

		assertTrue(db.execute("create database IF NOT EXISTS test") > -1);
		def rs = db.getDatabases();
		assertTrue(rs.contains("mysql"));
		assertTrue(rs.contains("test"));
		def rs1 = db.getSchemas();
		assertTrue(rs.equals(rs1));
		rs1=db.getTables("mysql");
		assertTrue(rs1.contains("user"));
		
		assertTrue(db.execute("drop table if exists test.MyEmployees3") > -1);
		
		assertTrue(db.execute(create_table) > -1);
		
		db.execute("insert into test.MyEmployees3(id, firstName) values(100, 'A')");
		
		db.execute("delete from test.MyEmployees3");
		
		def prest=db.prepareStatement("insert into test.MyEmployees3(id, firstName) values(?, ?)");
		def a=System.currentTimeMillis();
		
		for(int x = 0; x < 1000000; x++){
			prest.setInt(1, x);
			prest.setString(2, "张三");
			prest.addBatch();
		}
		prest.executeBatch();
		//db.commit();
		log.info "MySql批量插入100万条记录用时"+ (System.currentTimeMillis()-a)+" ms" 
	}
	
}