import java.lang.reflect.Method;

import java.util.HashMap;

import org.testng.annotations.*;
import org.tinyradius.packet.RadiusPacket;

import ate.testcase.CsUATestcase;
import ate.ua.mina.*;
import ate.ua.radius.RadiusUA;
/**
 * eth0   192.168.100.1 40:16:9F:F2:31:81
 * eth1   192.168.101.1 50:E5:49:21:1D:76
 *
 * arp -s 1.1.1.1 40:16:9F:F2:31:81 -i eth1
   arp -s 2.2.2.2 50:E5:49:21:1D:76  -i eth0
   ip route add 1.1.1.1  dev eth1
   ip route add 2.2.2.2 dev eth0
	iptables -t nat -A POSTROUTING -d 2.2.2.2 -j SNAT --to-source 1.1.1.1
	iptables -t nat -A POSTROUTING -d 1.1.1.1 -j SNAT --to-source 2.2.2.2
	iptables -t nat -A PREROUTING -d 1.1.1.1 -j DNAT --to-destination 192.168.100.1
	iptables -t nat -A PREROUTING -d 2.2.2.2 -j DNAT --to-destination 192.168.101.1
 * @author xiaoyong.huang
 *
 */

public class RadiusUATest extends CsUATestcase {
	def SAMPLES = [
		["SVT_RADIUS_PARSER_FUNC_001_0", 
			["User-Name":"13760439582",
			 "CHAP-Password":"07c42a8ac9bb36d2bcdc4ae8e05fcb8e8b",
			 "Calling-Station-Id":"00:26:82:B7:F5:BF",
			 "NAS-IP-Address":"192.168.1.88",
			 "Called-Station-Id":"00-17-7B-2B-D3-0C:zteopen"
			] 			
		],
		["SVT_RADIUS_PARSER_FUNC_001_1", 
			["User-Name":"13760439583",
			 "CHAP-Password":"07c42a8ac9bb36d2bcdc4ae8e05fcb8e8b",
			 "Calling-Station-Id":"00:26:82:B7:F5:BF",
			 "NAS-IP-Address":"192.168.1.88",
			 "Called-Station-Id":"00-17-7B-2B-D3-0C:zteopen"
			]
		] 
	];

	@Test(dataProvider="Data_TestStep",groups=["FUNC"])
	public void TestStep2(String name, HashMap<String, String> hm) {
		def svrAcct = create_server("radius://127.0.0.1:1813/udp?type=acct");		
		def cltAcct = create_client("radius://127.0.0.1:1813/udp?type=acct");		
		start_all_ua();

		hm.put("Acct-Status-Type", "Start");
		cltAcct.send_message(cltAcct.create_request(hm));

		def req = svrAcct.recv_message("User-Name",
				hm.get("User-Name"));
		assertTrue(req != null);
		def hm1 = new HashMap<String, String>();
		hm1.put("type-code", svrAcct.ACCOUNTING_RESPONSE);
		svrAcct.send_message(svrAcct.create_response(hm1, req));
		def resp = cltAcct.recv_message();
		assertTrue(resp != null);
		assertTrue(cltAcct.is_packet_type(resp, cltAcct.ACCOUNTING_RESPONSE));
		assertTrue(cltAcct.is_response(req, resp));
	}

	@DataProvider(name = "Data_TestStep")
	public Object[][] Data_TestStep() {
		return SAMPLES;
	}

	@Test(dataProvider="Data_TestStep",groups=["FUNC"])
	public void TestStep(String name, HashMap<String, String> hm) {
		def svrAuth = create_server("radius://127.0.0.1:1812/udp");
		def cltAuth =create_client("radius://127.0.0.1:1812/udp");
		start_all_ua();

		cltAuth.send_message(cltAuth.create_request(hm));

		def req = svrAuth.recv_message("User-Name", hm.get("User-Name"));
		assertTrue(req != null);

		def hm1 = new HashMap<String, String>();
		hm1.put("type-code", svrAuth.ACCESS_ACCEPT);
		hm1.put("Framed-MTU", "1500");

		svrAuth.send_message(cltAuth.create_response(hm1, (RadiusPacket) req));

		def resp = cltAuth.recv_message();
		assertTrue(resp != null);
		assertTrue(cltAuth.is_packet_type(resp, cltAuth.ACCESS_ACCEPT));
		assertTrue(cltAuth.has_attrs(resp, "Framed-MTU", "1500"));
	}

	@Test
	public void Test_AutoResp() {
		def svrAuth = create_server("radius://127.0.0.1:1812/udp");
		def hm = new HashMap<String, String>();
		hm.put("type-code", svrAuth.ACCESS_ACCEPT);
		hm.put("Framed-MTU", "1500");
		svrAuth.set_auto_response(hm);
		def cltAuth = create_client("radius://127.0.0.1:1812/udp");
		start_all_ua();

		hm = new HashMap<String, String>();
		hm.put("User-Name", "13760439583");
		hm.put("CHAP-Password", "07c42a8ac9bb36d2bcdc4ae8e05fcb8e8b");
		hm.put("Calling-Station-Id", "00:26:82:B7:F5:BF");
		hm.put("NAS-IP-Address", "192.168.1.88");
		hm.put("Called-Station-Id", "00-17-7B-2B-D3-0C:zteopen");

		for (int i = 0; i < 1000; i++) {
			long name = 13760439583L + i;
			hm.put("User-Name", name + "");
			cltAuth.send_message(cltAuth.create_request(hm));		
		}
		
		log.info(STEP+".2 clt recv 1000 response"); 

		assertTrue("预期Client收到1000个响应",cltAuth.expect_rcvd_count(1000,5000));
	}

	@Test
	public void Test_Auth() {
		def svr = create_server("radius://127.0.0.1:1812/udp");
		def clt = create_client("radius://127.0.0.1:1812/udp");
		this.start_all_ua();

		def hm = new HashMap<String, String>();
		hm.put("User-Name", "13760439582");
		hm.put("& User-Name", "13760439583");
		hm.put("&1 User-Name", "13760439584");
		hm.put("CHAP-Password", "07c42a8ac9bb36d2bcdc4ae8e05fcb8e8b");
		hm.put("Calling-Station-Id", "00:26:82:B7:F5:BF");
		hm.put("NAS-IP-Address", "192.168.1.88");
		hm.put("Called-Station-Id", "00-17-7B-2B-D3-0C:zteopen");
		clt.send_message(clt.create_request(hm));
		def req = svr.recv_message("User-Name", "13760439582");
		assertTrue(req != null);

		hm.clear();
		hm.put("type-code", svr.ACCESS_ACCEPT);
		hm.put("Framed-MTU", "1500");
		svr.send_message(svr.create_response(hm, req));
		def resp = clt.recv_message();
		assertTrue(resp != null);
		assertTrue(resp.getPacketType() == RadiusPacket.ACCESS_ACCEPT);
		assertTrue(resp.getAttribute("Framed-MTU").getAttributeValue()
				.equals("1500"));
	}

	@Test
	public void Test_Acct() {
		def svr = create_server("radius://127.0.0.1:1813/udp");
		assertTrue(svr instanceof RadiusUA);
		svr.change_type(RadiusUA.UA_TYPE_ACCT);
		def clt = create_client("radius://127.0.0.1:1813/udp");
		this.start_all_ua();

		clt.change_type(RadiusUA.UA_TYPE_ACCT);
		def hm = new HashMap<String, String>();
		hm.put("Acct-Status-Type", "Start");
		hm.put("User-Name", "13760439582");
		hm.put("Calling-Station-Id", "00:26:82:B7:F5:BF");
		hm.put("NAS-IP-Address", "192.168.1.88");
		hm.put("Called-Station-Id", "00-17-7B-2B-D3-0C:zteopen");
		clt.send_message(clt.create_request(hm));
		def req = svr.recv_message("User-Name", "13760439582");
		assertTrue(req != null);

		hm.clear();
		hm.put("type-code", svr.ACCOUNTING_RESPONSE);
		svr.send_message(svr.create_response(hm, req));
		def resp = clt.recv_message();
		assertTrue(resp != null);
		assertTrue(resp.getPacketType() == RadiusPacket.ACCOUNTING_RESPONSE);
		assertTrue(resp.getPacketIdentifier() == req.getPacketIdentifier());
		log.debug("done");
	}

	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();

		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}

}
