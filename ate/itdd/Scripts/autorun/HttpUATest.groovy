import java.lang.reflect.Method;

import org.apache.http.*;
import org.testng.annotations.*
import ate.testcase.CsUATestcase;
import ate.ua.http.HttpUA;
import java.util.*;

public class HttpUATest extends CsUATestcase {
	//@Test
	public void http_client() {
		def clt = create_client("http://www.baidu.com/");
		start_all_ua();
		
		assertTrue(clt.is_ready());
		def req = clt.create_request([
			"pkttype":"Get",
			"uri":"/"
			]);
		
		//clt.set_timeout(100000);
		clt.send_message(req);
		
		def resp= clt.recv_message();	
		
		def sc=resp.getStatusLine().getStatusCode();
		assertTrue(sc==200||sc==300);
		
		log.info("headers count: "+resp.getAllHeaders().length);
		log.info("content length: "+resp.getEntity());
		
	}
	String localuri="http://$sip:80/";
	int cnt=0;
	
	//@Test
	public void auto_response() {
		def svr =create_server(localuri,new HttpUA(){
			public Object get_auto_resp_msg(Object session, Object req) {
				cnt++;
				return create_response(req, [
					"status_code":"200",
					"content":""+"<html><title>test${cnt}</title></html>"
					])							
			}
		});
		def clt = create_client("http://localhost/");
		start_all_ua();
		svr.set_auto_response([:]);
		
		def o = clt.create_request([
			"pkttype":"GET",
			"uri":"/",
			"Host":"ping.ie.sogou.com",
			"Cache-Control":"no-cache"
			]);
		
		clt.send_message(o);
		
		def resp= clt.recv_message();
		
		assertTrue("response code",resp.getStatusLine().getStatusCode()==200);
		
		log.info("headers count: "+resp.getAllHeaders().length);
	}
	@Test
	public void web_server() {
		iterative_mode(true)
		def svr =create_server(localuri,new HttpUA(){
			public Object get_auto_resp_msg(Object session, Object req) {
				cnt++;
				return create_response(req, [
					"status_code":"200",
					"content":""+"<html><title>test${cnt}</title></html>"
					])
			}
		});
		
		start_all_ua();
		wait_yes();
		println "total response send: $cnt"
	}
	
	//@Test
	public void basic() {
		def svr = create_server(localuri);
		def clt = create_client("http://localhost/");
		start_all_ua();
		
		assertTrue(svr.is_ready());
		assertTrue(clt.is_ready());
		
		def o = clt.create_request([
			"pkttype":"GET",
			"uri":"/",
			"Host":"ping.ie.sogou.com",
			"Cache-Control":"no-cache"
			]);
		
		clt.send_message(o);
		
		def req =svr.recv_message();
		
		assertTrue(req.getRequestLine().getMethod().equals("GET"));
		
		def response=svr.create_response(req,[
			"status_code":"300",
			"Host":"ping.ie.sogou.com",
			"content":"no-cache"
			]);
		svr.send_message(response);
		
		def resp= clt.recv_message();
		
		assertTrue("response code��ȷ",resp.getStatusLine().getStatusCode()==300);
		
		log.info("headers count: "+resp.getAllHeaders().length);
	}
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		log.debug("after Method " + m);
		super.shutdown_all_ua();

		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}

}
