/** 
*@summary PAT模式
*@description 测试内网代理功能,转换后为ip
*
*<p>
*@pre-set PC1---DUT---PC2
              e0   e1
*
*<p>
*@author xiaoyong.huang
*@since  2012-11-7 16:29:33
*@product 
*@revision 
*    日期                    修改人                       修改说明
*   
*/
import org.testng.*
import org.testng.annotations.*

import static org.testng.AssertJUnit.*;
import java.lang.reflect.*;
import ate.util.*
import ate.testcase.*


public class SVT_NAT_NA_FUNC_001 extends CsUATestcase{
	def svrport=8000
	def NFE_WAN_IP
    def SAMPLES=[
//         ["SVT_NAT_NA_FUNC_001_0",
//              [传输协议:"TCP",
//               vlan:"1"
//         ]],
//         ["SVT_NAT_NA_FUNC_001_1",
//              [传输协议:"TCP",
//               vlan:"4094"
//         ]],
         ["SVT_NAT_NA_FUNC_001_2",
              [传输协议:"UDP",
               vlan:"1"
         ]],
         ["SVT_NAT_NA_FUNC_001_3",
              [传输协议:"UDP",
               vlan:"4094"
         ]]
     ]

    @Test(dataProvider="Data_TestStep",groups=["FUNC"])
    public void TestStep(name,samples){		
        log.info "${STEP}. PAT模式:"
        log.info "Testcase:$name, $samples"
		def trans=samples.get("传输协议")
		def vlan=samples.get("vlan")
		def uri=trans+":"+HOST.get("WAN_IP")+"/"+svrport
		def uri_local1=trans+":"+HOST.get("LAN_IP")+"/"+12345
		def uri_local2=trans+":"+HOST.get("LAN_IP")+"/"+12346
		
		
		def svr=createServer(uri)		
        
        //@STEP 在网络配置->Vlan中配置e0端口Vlan为[vlan]
        //@EXPECT ok
        log.info "${STEP}.1 在网络配置->Vlan中配置e0端口Vlan为[vlan],预期结果为:ok"        
		DUT.COM.enterMode("config")
	    assertTrue expect_end(DUT.COM.exec("vlan physical-port ge0 default-vlan $vlan"),">")
		assertTrue expect_end(DUT.COM.exec("vlan physical-port ge1 default-vlan $vlan"),">")
		
		//@STEP 新增内网代理策略,转换后地址勾选e1地址,按照样本点配置[vlan]
		//@EXPECT ok
		log.info "${STEP}.2 新增内网代理策略,转换后地址勾选e1地址,按照样本点配置[vlan],预期结果为:ok"		
		assertTrue expect_end(DUT.COM.exec("pat-proxy add name pat link ge1 vlan $vlan convert-addr-type external-address status enable"),">")
		sleep(2000)
		
        //@STEP PC1向PC2发起[传输协议]的请求
        //@EXPECT ok
        log.info "${STEP}.3 PC1向PC2发起[传输协议]的请求,预期结果为:ok"
        def clt=createClient(uri_local1,uri)
		clt.sendData("hello world");

        //@STEP PC2查看数据报文并响应,确认PC1用户IP转换成e1口地址
        //@EXPECT ok
        log.info "${STEP}.4 PC2查看数据报文并响应,确认PC1用户IP转换成e1口地址,预期结果为:ok"
		sleep(1000)
        def resp=svr.getMsg()		
        assertTrue resp,resp=="hello world"

        //@STEP PC1查看报文,确认能正确收到PC2的响应
        //@EXPECT ok
        log.info "${STEP}.5 PC1查看报文,确认能正确收到PC2的响应,预期结果为:ok"
        def nat1=clt.getMsg()		
        assertTrue nat1,expect_contains(nat1,NFE_WAN_IP)

        //@STEP PC1再次向PC2发起新的1路[传输协议]的请求
        //@EXPECT ok
        log.info "${STEP}.6 PC1再次向PC2发起新的1路[传输协议]的请求,预期结果为:ok"
		def clt1=createClient(uri_local2,uri)
		clt1.sendData("hello world1")

        //@STEP pc2查看数据报文并响应,确认PC1用户IP转换成e1口地址+另一个port
        //@EXPECT ok
        log.info "${STEP}.7 pc2查看数据报文并响应,确认PC1用户IP转换成e1口地址+另一个port,预期结果为:ok"
		sleep(1000)
		resp=svr.getMsg()		
		assertTrue resp,resp=="hello world1"
		        

        //@STEP pc1查看数据报文，确认能够收到PC2的响应
        //@EXPECT ok
        log.info "${STEP}.8 pc1查看数据报文，确认能够收到PC2的响应,预期结果为:ok"
		def nat2=clt1.getMsg()		  
		assertTrue nat2,expect_contains(nat2,NFE_WAN_IP)

        //@STEP 修改e0端口vlan为其他值
        //@EXPECT ok
        log.info "${STEP}.9 修改e0端口vlan为其他值,预期结果为:ok"
		assertTrue expect_end(DUT.COM.exec("vlan physical-port ge0 default-vlan 2"),">")
        

        //@STEP PC1向PC2发起[传输协议]的请求,确认PC2不会收到响应
        //@EXPECT ok
        log.info "${STEP}.10 PC1向PC2发起[传输协议]的请求,确认PC2不会收到响应,预期结果为:ok"
		clt1.clearQ()
		svr.clearQ()
		clt1.sendData("hello world3");
		sleep(1000)
		resp=svr.getMsg()		
		assertFalse resp,resp=="hello world3"
		
    }

    @DataProvider(name="Data_TestStep")
    public Object[][] Data_TestStep(){
        return SAMPLES;
    }

	def HOST
	def DUT
	@BeforeClass
	public void BeforeClass(){
		get_device("TESTSUITE").switchTestsuite("1")		
		DUT=get_device("NFE")
		HOST=get_device("HOST")		
		NFE_WAN_IP=DUT.get("WAN_IP")
		super.BeforeClass()
	}

    @BeforeMethod
    public void BeforeMethod(Method m){
        super.BeforeMethod(m);
//		DUT.COM.exec("no pat-proxy name pat")
//		DUT.COM.exec("vlan physical-port ge0 default-vlan 1")
//		DUT.COM.exec("vlan physical-port ge1 default-vlan 1")
    }

    @AfterMethod
    public void AfterMethod(Method m){
		super.AfterMethod(m);		
		DUT.COM.exec("no pat-proxy name pat")
		DUT.COM.exec("vlan physical-port ge0 default-vlan 1")
		DUT.COM.exec("vlan physical-port ge1 default-vlan 1")
        
    }

    @AfterClass
    public void AfterClass(){
        super.AfterClass();	 
    }
}
