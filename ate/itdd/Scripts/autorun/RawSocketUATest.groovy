import java.lang.reflect.Method;

import org.testng.annotations.*
import ate.testcase.RawUATestcase;
import ate.ua.arp.*;
import ate.ua.raw.RawSocketUA.FILTER;
import ate.testcase.RawUATestcase.REPLAY;

public class RawSocketUATest extends RawUATestcase {
	def ip1="10.10.0.1"
	def ip2="10.10.0.2"
	
	@Test
	void dump() {
		log.info("dump");
		def ua = create_ua("raw://172.16.5.112");
		ua.set_file_dumper("test.cap");
		ua.enable_capture(true);
		this.start_all_ua();

		sleep(5000);

		assertTrue(ua.rcvd() > 5);
	}
	
	//@Test
	public void snd_rcv(){
		log.info("dump");
		def uac = create_ua("raw://$ip1");
		def uas = create_ua("raw://$ip2");
		uas.set_file_dumper("test1.cap");
		uas.enable_capture(true);
		uac.set_file_dumper("test.cap");
		uac.enable_capture(true);
		start_all_ua();
				
		assertTrue(uac.expect_rcvd_count(0,1000));
		
		def pkt=uas.create_packet(PArp.REQUEST);
		uas.send_message(pkt);
		assertTrue(uac.expect_rcvd_ge(1,1000));
		assertTrue(uas.expect_rcvd_ge(1,1000));
		
		uac.send_message(pkt);
		assertTrue(uas.expect_rcvd_ge(2,1000));
		assertTrue(uac.expect_rcvd_ge(2,1000));
	}
	//@Test
	public void replay(){
		log.info("$STEP. replay");
		def uac = create_ua("raw://$ip1");
		def uas = create_ua("raw://$ip2");		
		
		log.info("$STEP.1. 设置dump调试文件 ");
		uac.set_file_dumper("test2.cap");
		uas.set_file_dumper("test1.cap");		
		
		log.info("$STEP.2. 使能capture功能 ");
		uac.enable_capture(true);
		uas.enable_capture(true);		
		
		log.info("$STEP.3. 设置jitter");
		def filter=create_io_filter(FILTER.Jitter);
		filter.set_min(200);
		filter.set_max(500);
		
		uac.add_first_io_filter(filter);
		uas.add_first_io_filter(filter);
		
		log.info("$STEP.4. 启动 all ua ");
		this.start_all_ua();
		
		log.info("$STEP.5. 初始化replayer ");
		def replay=create_replayer(uac,uas);
		
		log.info("$STEP.6. 设置replay方式为DIRECTMAC ");
		def handler=create_handler(replay,REPLAY.DIRECTMAC);
		handler.set_smac("8ca0480002a1");
		
		log.info("$STEP.7. replay google.pcap文件 ");
		replay.replay("google.pcap", handler);
		assertTrue(replay.get_seq()>0);
				
	}
	@Test
	public void ip4ex(){
		log.info("$STEP. ip4ex replay");
		def uac = create_ua("raw://$ip1");
		def uas = create_ua("raw://$ip2");
		
		log.info("$STEP.1. 设置dump调试文件 ");
		uac.set_file_dumper("test2.cap");
		uas.set_file_dumper("test1.cap");
		
		log.info("$STEP.2. 使能capture功能 ");
		uac.enable_capture(true);
		uas.enable_capture(true);
		
		log.info("$STEP.4. 启动 all ua ");
		this.start_all_ua();
		
		log.info("$STEP.5. 初始化replayer ");
		def replay=create_replayer(uac,uas);
		
		log.info("$STEP.6. 设置replay方式为IP4EX ");
		def handler=create_handler(replay,REPLAY.IP4EX);
		handler.set_sip("172.16.5.112");
		handler.set_sips(["1.1.1.1","1.1.1.2","1.1.1.3"]);
		
		log.info("$STEP.7. replay google.pcap文件 ");
		replay.replay("google.pcap", handler);
		assertTrue(replay.get_seq()>0);
				
	}
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		// set_log_level(DEBUG);
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
