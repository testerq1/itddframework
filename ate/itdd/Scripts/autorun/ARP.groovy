import java.lang.reflect.Method;

import org.testng.annotations.*;
import ate.testcase.UATestcase;
import ate.ua.arp.*

public class ArpUATest extends UATestcase{
	def ip1="10.10.0.1"
	def ip2="10.10.0.2"
	@Test
	public void snd_rcv(){
		log.info("dump");
		def uac = create_ua("arp://$ip1");
		def uas = create_ua("arp://$ip2");
		uas.set_file_dumper("test1.cap");
		uas.enable_capture(true);
		uac.set_file_dumper("test.cap");
		uac.enable_capture(true);
		this.start_all_ua();
				
		assertTrue(uac.expect_rcvd_count(0,1000));
		assertTrue(uas.expect_rcvd_count(0,1000));
		
		def pkt=uas.create_arp_packet(PArp.REQUEST);
		
		pkt.build(PArp.SENDERIP, "1.2.3.4")
		   .payload("sssssssssssssssssss")
		    .complete();
		uas.send_message(pkt.get_packet());
		
		assertTrue(uac.expect_rcvd_ge(1,1000));
		assertTrue(uas.expect_rcvd_ge(1,1000));
		
		pkt.build(PArp.SENDERIP, "4.5.6.7").complete();		
		uac.send_message(pkt.get_packet());
		assertTrue(uas.expect_rcvd_ge(2,1000));
		assertTrue(uac.expect_rcvd_ge(2,1000));
	}
	
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}

	@BeforeClass
	public void beforeClass() {
		//set_log_level(DEBUG);
		super.BeforeClass();
	}

	@AfterClass
	public void afterClass() {
		super.AfterClass();
	}
}
