import ate.testcase.*
import org.testng.annotations.*
import org.testng.TestNG
import org.testng.TestListenerAdapter
import static org.testng.AssertJUnit.*;
import java.lang.reflect.*;
import common.*;

public class SDV_REP_MANUALREP_FUNC extends Testcase  {	
	def WEB
	@BeforeClass
	public void setUp() throws Exception {		
		log.debug("BeforeClass")
		this.WEB=new Web(Web.create_driver("ie"),"http://172.16.5.201/")
		super.BeforeClass()
	}

	@Test
	public void testA() throws Exception {
		WEB.open("/reporter/index.php");
		WEB.type("id=user_name", "admin");
		WEB.type("id=password", "admin");
		WEB.waitForPageToLoad("30000");
		WEB.click("id=login");
		WEB.waitForPageToLoad("50000");
		//WEB.driver.navigate().to("javascript:document.getElementById('overridelink').click()");
		WEB.open("/reporter/index.php/ReptMenualReporter/index")//("link=手动报表");
		//WEB.driver.findElement(By.linkText("手动报表")).click();
		//WEB.click("link=手动报表");
		WEB.waitForPageToLoad("30000");
		WEB.driver.findElement(By.id("minDate")).clear();
		WEB.driver.findElement(By.id("minDate")).sendKeys("2013-03-01 10:30");
		WEB.driver.findElement(By.id("maxDate")).clear();
		WEB.driver.findElement(By.id("maxDate")).sendKeys("2013-03-10 10:30");
		WEB.driver.findElement(By.id("2")).click();
		WEB.waitForPageToLoad("30000");
	}	

	@AfterClass
	public void tearDown() throws Exception {
		super.AfterClass()
	}
}
