import ate.testcase.*
import org.testng.annotations.*
import org.testng.TestNG
import org.testng.TestListenerAdapter
import static org.testng.AssertJUnit.*;
import java.lang.reflect.*;
import ate.nativ.*
import ate.util.*

/**
* 
* @summary 测试ICMP防火墙功能 
*<p>
* @description 当NFE上配置完全过滤从某源地址到某目的地址的ICMP流量后，从该源地址发出的ping包不应该收到echo response
*<p>
* @pre-set    
     SW------------DUT
     |              |
     |              |NIC_LAN
     |______________PC
               NIC_WAN
   1，PC双网卡分别连DUT和SW，通过LAN口的流量经NFE后到WAN口
   2，NFE配置可以通过PC LAN口进行TELNET执行配置命令
*
*<p>
*@author 黄小勇
*@since  20120425
*@product NFE
*@revision 
*    日期                    修改人                       修改说明
*   20120426    huangxy        规范化
*   
*/

public class SVT_DEMO_ICMPFW_FUNC extends TestFlows{
    def HOST_WAN_IP
	def HOST
	def NFE
	@BeforeClass
	public void BeforeClass(){
		NFE=get_device("NFE")
		HOST=get_device("HOST")
		HOST_WAN_IP=HOST.get("WAN_IP")
		super.BeforeClass()
	}
	
    @Test
    void SVT_DEMO_ICMPFW_FUNC_001(){
        //@STEP 从小网网口ping大网网口,确认能够ping到
        //@EXPECT 丢包率<100%
        log.info "${STEP}.1 从小网网口ping大网网口,确认能够ping到："	
            def rtn=HOST.ping(HOST_WAN_IP, "-n 5")
            assertTrue "ping ok!",expect(rtn,"Packets: Sent = 5, Received = 5, Lost = 0 (0% loss)")
        
        //@STEP 配置本地路由使流量出口为小网网口
        //@EXPECT ok
        log.info "${STEP}.2 配置本地路由使流量出口为小网网口："
            HOST.add_route(HOST_WAN_IP, "255.255.255.255", NFE.get("LAN_IP"), "1");
            
        //@STEP NFE配置block所有到大网IP的ICMP流量
        //@EXPECT ok
        log.info "${STEP}.3 NFE配置block所有到大网IP的ICMP流量："
            NFE.CLI.enterMode("config")
            NFE.CLI.exec("firewall clear policy all")
            NFE.CLI.exec("firewall set policy from ANY to ANY src-addr ANY dst-addr $HOST_WAN_IP service \"ICMP_ANY\" action deny status enable ")
            NFE.CLI.exec("firewall set policy from ANY to ANY src-addr ANY dst-addr ANY service \"ANY\" action permit status enable")
    
		//@STEP 从小网网口ping大网网口，确认ping不到
		//@EXPECT 丢包率100%
		log.info "${STEP}.4 从小网网口ping大网网口，确认ping不到："
            rtn=HOST.ping(HOST_WAN_IP, "-n 2")
            assertTrue "ping ok",expect(rtn,"Packets: Sent = 2, Received = 0, Lost = 2 (100% loss)")
		
		//@STEP 恢复环境配置
		//@EXPECT ok			
        log.info "${STEP}.5 恢复环境配置："
            HOST.del_route(HOST_WAN_IP)
            NFE.CLI.enterMode("config")
            NFE.CLI.exec("firewall clear policy all")
            NFE.CLI.exec("firewall set policy from ANY to ANY src-addr ANY dst-addr ANY service \"ANY\" action permit status enable")
    }
    @AfterMethod
    public void AfterMethod(){
		super.AfterMethod()
        HOST.del_route(HOST_WAN_IP);
    }
}