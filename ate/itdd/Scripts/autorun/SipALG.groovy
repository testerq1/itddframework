import ate.testcase.*
import org.testng.annotations.*

import java.lang.reflect.*;

class SipALG extends CsUATestcase{
	def sip_reg="REGISTER sip:172.16.5.152 SIP/2.0\r\n"+
"Contact: <sip:xiaoyong.huang@172.16.5.111:5060>\r\n"+
"Expires: 60\r\n"+
"To: <sip:xiaoyong.huang@172.16.5.152>\r\n"+
"Call-ID: 402805048@172.16.5.152\r\n"+
"Via: SIP/2.0/UDP 172.16.5.111:5060;rport;branch=z9hG4bK517129394\r\n"+
"From: <sip:xiaoyong.huang@172.16.5.152>;tag=86123391\r\n"+
"CSeq: 1 REGISTER\r\n"+
"User-Agent: YATE/4.1.0\r\n"+
"Max-Forwards: 70\r\n"+
"Allow: ACK, INVITE, BYE, CANCEL, OPTIONS, INFO\r\n"+
"Content-Length: 0\r\n\r\n"

	@Test
	public void TestStep1(){
		def uas=create_server("sip://127.0.0.1:5060/udp")
		def uac=create_client("sip://127.0.0.1:5060/udp")
		start_all_ua()
		
		uac.send_message(sip_reg);
		def resp=uas.recv_request("REGISTER","xiaoyong.huang","xiaoyong.huang")
		assertTrue(resp!=null)
		println resp
		//wait_yes()
	}
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}
  
	@AfterMethod
	public void afterMethod(Method m) {
		super.shutdown_all_ua();
		super.AfterMethod(m);
	}
	@BeforeClass
	public void BeforeClass() {
	   super.BeforeClass();
	}
}
