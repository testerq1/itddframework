import org.testng.annotations.Test;

import ate.ua.telnet.TelnetUA;

import java.lang.reflect.Method;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ate.testcase.CsUATestcase;
import ate.testcase.UATestcase;
import ate.ua.ssh.SSHUA;

public class VTTest extends UATestcase {
	@Test
	public void ssh_login() {
		log.info "$STEP. ssh_login " 
		def ssh = create_ua("ssh://root:passwd@172.16.5.118/?line_mode=r");
		this.start_all_ua();
		assertTrue(ssh.is_ready());

        ssh.login(/(]$ |: |]# |->)$/, "[root");
		ssh.clear_q();
		ssh.send_line("ls");
		String o=ssh.recv_message();
		assertTrue(o!=null);
		log.info(o);
	}

	@Test
	public void telnet_login() {
		log.info "$STEP. telnet_login "
		
		def telnet = create_ua("telnet://root:passwd@172.16.5.118/?line_mode=r");
		this.start_all_ua();
		assertTrue(telnet.is_ready());

		telnet.login("login: ", "root", "assword: ", "passwd", /(]$ |: |]# |->)$/,
				"[root");
		
		telnet.send_line("ls");
		String o=telnet.recv_message();
		assertTrue(o!=null);
		log.info(o);
	}
			
	@BeforeMethod
	public void beforeMethod(Method m) {
		super.BeforeMethod(m);
	}

	@AfterMethod
	public void afterMethod(Method m) {
		log.debug("after Method " + m);
		super.shutdown_all_ua();

		super.AfterMethod(m);
	}
}
