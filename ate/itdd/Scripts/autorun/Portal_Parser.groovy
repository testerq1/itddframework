import org.apache.mina.core.session.IoSession;

import java.lang.reflect.Method;
import java.util.HashMap;
import org.testng.annotations.*

import ate.testcase.CsUATestcase;
import ate.ua.portal.*
/**
 * eth0   192.168.100.1 40:16:9F:F2:31:81       
 * eth1   192.168.101.1 50:E5:49:21:1D:76
 * 
 * arp -s 1.1.1.1 40:16:9F:F2:31:81 -i eth1
   arp -s 2.2.2.2 50:E5:49:21:1D:76  -i eth0
   ip route add 1.1.1.1  dev eth1 
   ip route add 2.2.2.2 dev eth0 
	iptables -t nat -A POSTROUTING -d 2.2.2.2 -j SNAT --to-source 1.1.1.1
	iptables -t nat -A POSTROUTING -d 1.1.1.1 -j SNAT --to-source 2.2.2.2
	iptables -t nat -A PREROUTING -d 1.1.1.1 -j DNAT --to-destination 192.168.100.1
	iptables -t nat -A PREROUTING -d 2.2.2.2 -j DNAT --to-destination 192.168.101.1
 * @author xiaoyong.huang
 *
 */
 
public class PortalUATest extends CsUATestcase{
	//for DUT
	//def svr_inner_ip="192.168.101.1"	
	//def svr_exter_ip="2.2.2.2"
	
	// for debug
	def svr_inner_ip="127.0.0.1"
	def svr_exter_ip=svr_inner_ip
	//@Test
	public void all_flow() {
		log.info "${STEP}. Portal认证完整流程"
		
		log.info "${STEP}.1 初始化radius和portal："
		def portal=create_client("portal://${svr_exter_ip}:2000/udp");
		def ac_portal=create_server("portal://${svr_inner_ip}:2000/udp");
		def radius_auth=create_server("radius://${svr_inner_ip}:1812/udp");
		def ac_radius_auth=create_client("radius://${svr_exter_ip}:1812/udp");
		def radius_acct=create_server("radius://${svr_inner_ip}:1813/udp");
		def ac_radius_acct=create_client("radius://${svr_exter_ip}:1813/udp");
		ac_radius_acct.change_type(ac_radius_acct.UA_TYPE_ACCT);
		radius_acct.change_type(radius_acct.UA_TYPE_ACCT);
		start_all_ua();
		
		assertTrue(portal.is_ready());
		assertTrue(ac_portal.is_ready());
		assertTrue(radius_auth.is_ready());
		assertTrue(ac_radius_auth.is_ready());
		assertTrue(radius_acct.is_ready());
		assertTrue(ac_radius_acct.is_ready());
		
		log.info "${STEP}.2 portal向AC端发送REQ_CHALLENGE:"
		def pkt=portal.create_packet("REQ_CHALLENGE");
		pkt.setUserip("11.0.0.1");
		portal.send_message(pkt);
		
		def req = ac_portal.rcv_message("REQ_CHALLENGE",""+pkt.serialno,""+pkt.reqid);
		assertTrue(req!=null);
		assertTrue(req.serialno!=0);
		
		log.info "${STEP}.3 AC向Portal响应ACK_CHALLENGE：" 
		def hm = ["pkttype":"ACK_CHALLENGE","errcode":"random"] 
		def pktack=ac_portal.create_packet(hm,req);
		pktack.addAttr(pktack.createAttr("chappassword","11111111111111"));
		ac_portal.send_message(pktack);
		//ac_portal.send_message(ac_portal.create_packet(hm,req));
		
		def pkt2 =portal.recv_message(); 
		assertTrue(pkt2!=null);
		assertTrue(pkt2.isType("ACK_CHALLENGE")); 
		assertTrue(pkt2.reqid!=0);
		assertTrue(pkt2.getAttr("chappassword")==null);
		//assertTrue(resp.getAttribute("Framed-MTU").getAttributeValue().equals("1500"));
		
		log.info "${STEP}.4 portal向AC端发送REQ_AUTH:"
		def pkt3=portal.create_packet("REQ_AUTH");
		pkt3.setUserip("11.0.0.1");
		pkt3.reqid=pkt3.newReqID();
		portal.send_message(pkt3);
		
		def req2 = ac_portal.rcv_message("REQ_AUTH",""+pkt3.serialno,""+pkt3.reqid);
		assertTrue(req2!=null);
		assertTrue(req2.serialno!=0);		
				
		log.info "${STEP}.5 AC向Radius发送Access_Request："
		def hm10=["User-Name":"13760439582","CHAP-Password":"07c42a8ac9bb36d2bcdc4ae8e05fcb8e8b","Calling-Station-Id":"00:26:82:B7:F5:BF","NAS-IP-Address":"11.0.0.1","Called-Station-Id":"00-17-7B-2B-D3-0C:zteopen"]
		ac_radius_auth.send_message(ac_radius_auth.create_request(hm10));
		
		log.info "${STEP}.6 Radius向AC响应Access_Accept："
		def req13 = radius_auth.recv_message("User-Name", hm10.get("User-Name"));
		assertTrue(req13 != null);
  
		HashMap<String, String> hm1 = new HashMap<String, String>();
		hm1.put("type-code", radius_auth.ACCESS_ACCEPT);
		hm1.put("Framed-MTU", "1500");
  
		radius_auth.send_message(radius_auth.create_response(hm1,req13));
  
		def resp13 =ac_radius_auth.recv_message();
		assertTrue(resp13 != null);
		assertTrue(ac_radius_auth.is_packet_type(resp13, ac_radius_auth.ACCESS_ACCEPT));
		assertTrue(ac_radius_auth.has_attrs(resp13, "Framed-MTU", "1500"));
		 
		log.info "${STEP}.7 AC向Portal响应ACK_AUTH："
		HashMap<String, String> hm33 = new HashMap<String, String>();
		hm33.put("pkttype", "ACK_AUTH");
		hm33.put("chappassword", "abcdef");
		ac_portal.send_message(ac_portal.create_packet(hm33,req2));
		
		ac_portal.clear_q();
		HashMap<String, String> hm44 = new HashMap<String, String>();
		hm44.put("pkttype", "AFF_ACK_AUTH");
		hm44.put("chappassword", "abcdef");
		portal.send_message(portal.create_packet(hm44, req2));
		
		log.info "${STEP}.8 portal向AC端发送AFF_ACK_AUTH:"
		def pkt4 = ac_portal.recv_message();
		assertTrue(pkt4!=null);
		assertTrue(pkt4.isType("AFF_ACK_AUTH"));
		assertTrue(pkt4.reqid!=0);
		assertTrue(pkt4.serialno!=0);
		
		
		log.info "${STEP}.9 AC向Radius发送Account_Request/start："
		HashMap<String, String> hm66 = new HashMap<String, String>();
		hm66.put("User-Name", "13760439582");
		hm66.put("CHAP-Password", "07c42a8ac9bb36d2bcdc4ae8e05fcb8e8b");
		hm66.put("Calling-Station-Id", "00:26:82:B7:F5:BF");
		hm66.put("NAS-IP-Address", "11.0.0.1");
		hm66.put("Called-Station-Id", "00-17-7B-2B-D3-0C:zteopen");
		hm66.put("Acct-Status-Type", "Start");
		ac_radius_acct.send_message(ac_radius_acct.create_request(hm66));
			  
		def req12 = radius_acct.recv_message("User-Name",hm66.get("User-Name"));
		assertTrue(req12!=null);
		
		log.info "${STEP}.10 Radius向AC响应Account_Response/start："
		HashMap<String, String> hm77 = new HashMap<String, String>();
		 hm77.put("type-code", radius_acct.ACCOUNTING_RESPONSE);
		radius_acct.send_message(radius_acct.create_response(hm77, req12));
		 
		def resp22 = ac_radius_acct.recv_message();
		assertTrue(resp22 !=null);
		assertTrue(ac_radius_acct.is_packet_type(resp22,ac_radius_acct.ACCOUNTING_RESPONSE));
		assertTrue(ac_radius_acct.is_response(req12,resp22));
		
		//todo
		
		log.info "${STEP}.11 AC向Radius发送Account_Request/stop："
		
		log.info "${STEP}.12 Radius向AC响应Account_Response/stop："
	}
	 
	@Test
	public void AutoResponse2() {		
		log.info "$STEP AutoResponse2"	
		def svr=this.create_server("portal://127.0.0.1:2000/udp",new CMCCPortalUA(){
			public Object get_auto_resp_msg(Object session, Object req) {				
				def pkt=req.createResponse(null, null)
				log.info "in get_auto_resp_msg "+pkt
				if(pkt!=null)
					pkt.errcode=1
				return pkt;
			}
		});
	    log.info "$STEP.1 AutoResponse2"
		def clt=this.create_client("portal://127.0.0.1:2000/udp")
		assertTrue(!clt.is_ready());
		assertTrue(!svr.is_ready());
		this.start_all_ua();
		assertTrue(clt.is_ready());
		assertTrue(svr.is_ready());
		
		log.info "$STEP.2 Client Send REQ_AUTH"
		def pkt=clt.create_packet("REQ_AUTH");
		pkt.setUserip("11.0.0.1");
		pkt.reqid=pkt.newReqID();
		clt.send_message(pkt);
		
		log.info "$STEP.3 Client Recv ACK_AUTH"
		pkt = clt.recv_message();
		assertTrue(pkt!=null);
		assertTrue(pkt.isType("ACK_AUTH"));
		assertTrue(pkt.reqid!=0);
		assertTrue(pkt.getAttr("chappassword")==null);
		
		log.info "$STEP.4 Client Send AFF_ACK_AUTH"
		svr.clear_q();
		clt.send_message(pkt.createResponse(null, null));
		
		log.info "$STEP.5 Server Recv AFF_ACK_AUTH"
		pkt = svr.recv_message();
		assertTrue(pkt!=null);
		assertTrue(pkt.isType("AFF_ACK_AUTH"));
		assertTrue(pkt.reqid!=0);
		assertTrue(pkt.serialno!=0);
	}
	//@Test
	public void AutoResponse() {
		  def svr=this.create_server("portal://127.0.0.1:2000/udp");
		  def clt=this.create_client("portal://127.0.0.1:2000/udp");
		  assertTrue(!clt.is_ready());
		  assertTrue(!svr.is_ready());
		  this.start_all_ua();
		  assertTrue(clt.is_ready());
		  assertTrue(svr.is_ready());
		  svr.set_auto_response(new HashMap());
		  
		  CMCCPortalPacket pkt=clt.create_packet("REQ_AUTH");
		  pkt.setUserip("11.0.0.1");
		  pkt.reqid=pkt.newReqID();
		  clt.send_message(pkt);
		  
		  pkt = (CMCCPortalPacket)clt.recv_message();
		  assertTrue(pkt!=null);
		  assertTrue(pkt.isType("ACK_AUTH"));
		  assertTrue(pkt.reqid!=0);
		  assertTrue(pkt.getAttr("chappassword")==null);
		  
		  svr.clear_q();
		  clt.send_message(pkt.createResponse(null, null));
		  
		  pkt = (CMCCPortalPacket)svr.recv_message();
		  assertTrue(pkt!=null);
		  assertTrue(pkt.isType("AFF_ACK_AUTH"));
		  assertTrue(pkt.reqid!=0);
		  assertTrue(pkt.serialno!=0);
	  }
	
  //@Test
  public void REQ_CHALLENGE() {
	  CMCCPortalUA svr=(CMCCPortalUA)this.create_server("portal://127.0.0.1:2000/udp");
	  CMCCPortalUA clt=(CMCCPortalUA)this.create_client("portal://127.0.0.1:2000/udp");
	  assertTrue(!clt.is_ready());
	  assertTrue(!svr.is_ready());
	  this.start_all_ua();
	  assertTrue(clt.is_ready());
	  assertTrue(svr.is_ready());
	  
	  CMCCPortalPacket pkt=clt.create_packet("REQ_CHALLENGE");
	  pkt.setUserip("11.0.0.1");
	  clt.send_message(pkt);
	  
	  CMCCPortalPacket req = svr.rcv_message("REQ_CHALLENGE",""+pkt.serialno,""+pkt.reqid);
	  assertTrue(req!=null);
	  assertTrue(req.serialno!=0);
	  
	  HashMap<String, String> hm = new HashMap<String, String>();
	  hm.put("pkttype", "ACK_CHALLENGE");
	  hm.put("chappassword", "abcdef");
	  svr.send_message(svr.create_packet(hm,req));
	  
	  pkt = (CMCCPortalPacket)clt.recv_message();
	  assertTrue(pkt!=null);
	  assertTrue(pkt.isType("ACK_CHALLENGE"));
	  assertTrue(pkt.reqid!=0);
	  assertTrue(pkt.getAttr("chappassword")==null);
	  //assertTrue(resp.getAttribute("Framed-MTU").getAttributeValue().equals("1500"));
  }
  //@Test
  public void REQ_AUTH() {
	  CMCCPortalUA svr=(CMCCPortalUA)this.create_server("portal://127.0.0.1:2000/udp");
	  CMCCPortalUA clt=(CMCCPortalUA)this.create_client("portal://127.0.0.1:2000/udp");
	  assertTrue(!clt.is_ready());
	  assertTrue(!svr.is_ready());
	  this.start_all_ua();
	  assertTrue(clt.is_ready());
	  assertTrue(svr.is_ready());
	  
	  CMCCPortalPacket pkt=clt.create_packet("REQ_AUTH");
	  pkt.setUserip("11.0.0.1");
	  pkt.reqid=pkt.newReqID();
	  clt.send_message(pkt);
	  
	  CMCCPortalPacket req = svr.rcv_message("REQ_AUTH",""+pkt.serialno,""+pkt.reqid);
	  assertTrue(req!=null);
	  assertTrue(req.serialno!=0);
	  
	  HashMap<String, String> hm = new HashMap<String, String>();
	  hm.put("pkttype", "ACK_AUTH");
	  hm.put("chappassword", "abcdef");
	  //svr.send_message(svr.create_packet(hm,req));
	  svr.send_message(req.createResponse(null, null));
	  
	  
	  pkt = (CMCCPortalPacket)clt.recv_message();
	  assertTrue(pkt!=null);
	  assertTrue(pkt.isType("ACK_AUTH"));
	  assertTrue(pkt.reqid!=0);
	  assertTrue(pkt.getAttr("chappassword")==null);
	  
	  svr.clear_q();
	  clt.send_message(pkt.createResponse(null, null));
	  
	  pkt = (CMCCPortalPacket)svr.recv_message();
	  assertTrue(pkt!=null);
	  assertTrue(pkt.isType("AFF_ACK_AUTH"));
	  assertTrue(pkt.reqid!=0);
	  assertTrue(req.serialno!=0);
  }
  
  @BeforeMethod
  public void beforeMethod(Method m) {
	  super.BeforeMethod(m);
  }

  @AfterMethod
  public void afterMethod(Method m) {
	  super.shutdown_all_ua();
	  super.AfterMethod(m);
  }
  @BeforeClass
  public void BeforeClass() {
	 super.BeforeClass();  
  }
}
