
import com.thoughtworks.selenium.Selenium;
import org.openqa.selenium.WebDriverBackedSelenium
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.openqa.selenium.firefox.FirefoxDriver;

import ate.testcase.*
import org.testng.annotations.*
import org.testng.TestNG
import org.testng.TestListenerAdapter
import static org.testng.AssertJUnit.*;
import java.lang.reflect.*;
import org.openqa.selenium.*
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.remote.DesiredCapabilities
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.CoreMatchers.*; 
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.CapabilityType;

public class SDV_DEMO_WEB_FUNC extends Testcase  {		
	def WEB
	def src_ip
	def dst_ip
	@BeforeClass
	public void setUp() throws Exception {		
		def NFE=get_device("NFE")
		def HOST=get_device("HOST")
		WEB=NFE.WEB
		src_ip=NFE.get("WEB_IP")
		dst_ip=HOST.get("WAN_IP")
		super.BeforeClass()
	} 
	@Test
	public void SDV_DEMO_WEB_FUNC_001() throws Exception {	
		
		log.info "${STEP}. 测试流量安全表的添加和删除"
		log.info "${STEP}.1  通过browser登录:"
		WEB.login()
		
		log.info "${STEP}.2 在流量安全节点下增加一行:"
		WEB.add_flow_security(src_ip,dst_ip)
		
		log.info "${STEP}.3 确认增加的数据正确："
		def table = "//table[@id='table2']/tbody[@id='kkk1']/tr";
		assertTrue(WEB.getText(table + "[1]/td[2]").equalsIgnoreCase(
				src_ip));
		assertTrue(WEB.getText(table + "[1]/td[3]").equalsIgnoreCase(
				dst_ip));
		assertTrue(WEB.getText(table + "[1]/td[9]").equalsIgnoreCase(
				"deny"));
		
		log.info "${STEP}.4  测试数据删除:"
		WEB.del_flow_security()
	}

	@AfterClass
	public void tearDown() throws Exception {
		super.AfterClass()
	}
	
}