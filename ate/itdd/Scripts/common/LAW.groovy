package common;
import java.nio.*
import java.net.*

import org.eclipse.jetty.util.log.Log;

import ate.util.*
import ate.sandbox.cli.*
import ate.testcase.*
import static org.testng.AssertJUnit.*;
import ate.rm.dev.*

/**
 * Device类的父类，要求所有设备类从该类继承
 * @author Administrator
 *
 */
def abstract class LAW extends DeviceImp{		
	/*所有类型 */
	public static final int T_ALL=7
	/*缺省类型，CLI+HOST */
	public static final int T_DEFAULT=0
	/*COM */
	public static final int T_COM=1
	/*WEB*/
	public static final int T_WEB=2
	
	public static final int T_CLI=4
	/*CLI handler*/
	def CLI
	/*Web handler*/
	def WEB
	/*COM handler,应该限制使用*/
	protected def COM
	/*HOST handler*/
	def WIN
	
	/**
	 * 初始化com连接
	 * @param port_name
	 * @param baud
	 * @return
	 */
	def init_com(port_name,baud){	
		if(COM!=null)
			return			
		COM= CLIUtil.spawn("rxtx",get("SERIAL_NAME"), get("SERIAL_BAUD"))
		COM.init(this)
		
		println "COM initiated $COM"
	}
	/**
	 * 初始化web连接
	 * @param url
	 * @return
	 */
	def init_web(url){		
		if(WEB!=null)
			return
					
		this.WEB=new Web(Web.create_driver("firefox"),url)
		WEB.login()
		println "WEB initiated $WEB"
	}
	/**
	 * 初始化win
	 * @return
	 */
	def init_win(){
		if(WIN!=null)
			return	
		WIN=new Win()
		println "WIN initiated $WIN"		
	}
	/**
	 * 初始化Telnet连接
	 * @param ip
	 * @param port
	 * @return
	 */
	def init_cli(ip,port){		
		if(CLI!=null)
			return
		CLI= CLIUtil.spawn("telnet",ip,port)
		CLI.init(this)
		println "CLI initiated $CLI"
	}
	/**
	 * 初始化Telnet连接
	 * @param ip
	 * @param port
	 * @return
	 */
	def init_ssh(ip,port,uname,passwd){
		if(CLI!=null)
			return
		CLI= CLIUtil.spawn("ssh",ip,port,uname,passwd)
		CLI.init(this)
		println "CLI initiated $CLI"
	}
	/**
	 * disconnect all terminal
	 * @return
	 */
	def quit(){
		if(CLI!=null)
			CLI.disconnect()
		if(COM!=null){
			COM.setPrompt("")
			COM.exec("exit")			
			COM.exec("exit")
			COM.exec("exit")
			COM.disconnect()
		}
		if(WEB!=null)
			WEB.disconnect()
		println "Quit done!"	
	}
	
	/**
	 * 检查ftp server上是否有新版本,ftp参数都在properties文件中配置
	 * 可以用此方法下载文件夹，由于NFE支持自己下载,因此如果提供ftp的发布方式，则只需判断，并在本地保存一个标记(mkdir)
	 * 
	 * @return null或者新版本所在文件夹中的所有文件名ArrayList
	 */
	def has_new_version(){
		log.debug("has_new_version:")
		def ftp_svr=get_from_ini("FTP.server");
		def ftp_uname=get_from_ini("FTP.uname");
		def ftp_passwd=get_from_ini("FTP.passwd");
		def local=get_from_ini("HOST.FTP_HOME");
		AntBuilder ant=new AntBuilder()
		
		println "$ftp_svr:$ftp_uname@$ftp_passwd"
		
		def ftp=new FTPUtil(ftp_svr,ftp_uname,ftp_passwd);
		def ftp_newest=ftp.getNewestName(FSUtil.DIR_TYPE)
		
		def smb_newest = new SMBUtil(local).getNewestName(FSUtil.DIR_TYPE)
		log.debug ftp_newest
		log.debug smb_newest
		
		if(ftp_newest.compareTo(smb_newest)>0){			
			ant.mkdir(dir:local+"/"+ftp_newest)
			return ftp.walk(ftp_newest)
//			def o =ant.ftp(action:"list",server:ftpsvr,userid:uname,password:passwd,binary:"yes",systemTypeKey:"UNIX",
//				     listing:local+"/"+ftp_newest+"/ftp.listing"){
//					fileset() {
//						include(name:"$ftp_newest/**")
//					}
//				}
//			println o
//			log.debug("down $ftp_newest from $ftpsvr")
//			ant.sequential {
//				echo("inside sequential")
//				ftp(action:"get",server:ftpsvr,userid:uname,password:passwd,binary:"yes",systemTypeKey:"UNIX") {
//					fileset(dir:local) {
//						include(name:"$ftp_newest/**")
//					}
//				}
//				echo("done")
//			}
			return ftp_newest
		}
		
		return null	
	}
}