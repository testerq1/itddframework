package common;
import java.io.*;
import java.net.*
import ate.sandbox.cli.*
import ate.util.*
import ate.rm.dev.*
/**
 * 封装linux下的CLI
 * @author Administrator
 *
 */
class Lin extends Linux{
	/**
	 * 性能优先
	 * @param loop
	 * @param seconds
	 * @param file
	 * @return
	 */
	 
	def http_load_parallel(loop,seconds,file){
		return exec0("http_load -parallel $loop -seconds $seconds $file")
	}
	/**
	 * -c  客户端数目
	 * -i  运行几次测试，多次取平均值
	 * --number-of-queries=50000：query次数
	 * @return
	 */
	def mysqlslap(){
		switchTo("")
		return exec("mysqlslap -uroot -p123456 -h localhost -c 10,50,100,200,400 -i 2 --engine=innodb --auto-generate-sql-load-type=mixed --number-of-queries=50000 --number-char-cols=5 --number-int-cols=5 --auto-generate-sql")
	}
	
	/**
	 * 每秒速率
	 * @param loop
	 * @param seconds
	 * @param file
	 * @return
	 */
	def http_load_rate(loop,seconds,file){
		switchTo("")
		return exec("http_load -rate $loop -seconds $seconds $file")
	}
	
	def sysbench_io(){
		switchTo("")
		def rtn=""
		rtn+=exec("sysbench --test=fileio --file-total-size=150G prepare")
		rtn+=exec("sysbench --test=fileio --file-total-size=150G --file-test-mode=rndrw "+
			               "--init-rnd=on --max-time=300 --max-request=0 run")
		rtn+=exec("sysbench --test=fileio --file-total-size=150G cleanup")
		return rtn
	}
	
	def sysbench_cpu(){
		switchTo("")
		return exec("sysbench --test=cpu --cpu-max-prime=20000 run")
	}
	def sysbench_oltp(){
		switchTo("")
		def rtn=""
		rtn+=exec("sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test "+
			               "--mysql-user=root prepare")
		rtn+=exec("sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test "+
			               "--mysql-user=root --max-time=60 --oltp-read-only=on --max-requests=0 "+
			               "--num-threads=8 run")
		return rtn
	}
	def dbt2_(){
			
	}
	/**
	 * yum install mysql-bench
	 * @return
	 */
	def sql_bench_all(){
		switchTo("/usr/share/sql-bench")
		return exec("run-all-test --server=mysql --user=root --log --fast")
	}
	def sql_bench_insert(){
		switchTo("/usr/share/sql-bench")
		return exec("test-insert")
	}
	/**
	 * yum install perl
	 * @param home
	 * @param log
	 * @return
	 */
	def mysql_dump_slow(home,log){
		switchTo(home)
		//访问次数最多的20个sql
		def rtn=exec("mysqldumpslow -s c -t 20 $log")
		//返回记录集最多的20个sql
		rtn+=exec("mysqldumpslow -s r -t 20 $log")
		rtn+=exec("mysqldumpslow -s t -t 10 $log")
		///path/mysqldumpslow -s t -t 10 -g “left join” /database/mysql/slow-log
		return rtn
	}
	/**
	 * 进程运行时间
	 */
	def uptime(process){
		return exec("ps -eo pid,comm,lstart,etime,time,args|grep -i $process |awk '{print \$8}' | head -n 1")
	}
	
	public static void main(String[] args){
		Lin lin=new Lin()
		//println win.start_dynamips()
		println lin.ls("")
	}
}
