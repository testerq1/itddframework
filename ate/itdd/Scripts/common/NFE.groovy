package common;
import ate.util.*
import ate.sandbox.cli.*
import ate.testcase.*
import static org.testng.AssertJUnit.*;
import ate.rm.dev.*
import org.openide.util.lookup.*;

/**
 * NFE的类 
 * @author huangxy
 *
 */
def class NFE extends LAW{
	//debug开关，为true时，不执行初始配置任务
	def isConfiged=false
	
	/**
	 * 为避免复杂性，默认只提供CLI/WEB调用接口，COM口不对外提供直接调用接口
	 * @param b
	 * @return
	 */
	def init(b){
		init_win()		
		
		if((b&1)>0){
			this.init_com(get("SERIAL_NAME"), get("SERIAL_BAUD"))			
			if(!isConfiged)
				init_nfe(COM)			
		}		
		if((b&T_WEB)>0){ 
			def MGT_IP=get("WEB_IP")			 
			init_web("https://$MGT_IP:8888")
		}	
			
		if((b&4)>0){
			init_cli(get("WEB_IP"),get("TELNET_PORT"))			
		}
		
	}
	/**
	 * clear在线AP表
	 * debug模式下执行 feSetAPIPInit(1)
	 * @return
	 */
	def clear_ap_online(){
		CLI.enterMode("debug");
		assert CLI.expect("feSetAPIPInit(1)","error")==false
	}
	
	/**
	 * nfe的初始化配置，包括telnet的enable等，只需要执行一次
	 * @param COM
	 */
	private void init_nfe(COM){
		def NFE_WAN_IP=get("WAN_IP")//172.16.5.115
		def NFE_WAN_GATEWAY=get("WAN_GATEWAY")//172.16.5.1
		def MGT_IP=get("WEB_IP")
		COM.enterMode("");
		
		if(type=="linux")
			return			
			
		COM.exec("set admin telnet enable")
		COM.exec("set vif vif0 delete")
		COM.exec("set vif vif1 delete")
		COM.exec("set vif new vif0-10 phy-if ge0 10")
		COM.exec("set vif new vif1-20 phy-if ge1 20")
		COM.exec("set vif vif0-10 zone l3-in")
		COM.exec("set vif vif1-20 zone l3-out")
		COM.exec("set vif vif0-10 ip $MGT_IP netmask 255.255.255.0")
		COM.exec("set vif vif1-20 ip $NFE_WAN_IP netmask 255.255.255.0")
		COM.exec("set vif vif1-20 filters enable web,telnet,ssh,ping,snmp")
		COM.exec("set route ip 0.0.0.0 0.0.0.0 $NFE_WAN_GATEWAY")
		COM.exec("set vlan-port-mapping phy-if ge0 vid 10 8021p 0")
		COM.exec("set vlan-port-mapping phy-if ge1 vid 20 8021p 0")
		COM.exec("firewall set policy from ANY to ANY src-addr ANY dst-addr ANY service \"ANY\" action permit status enable ")
		COM.exec("firewall set policy from ANY to ANY src-addr ANY dst-addr ANY service \"ANY\" action permit status enable ")
		COM.exec("set vif vif1-20 nat")
		COM.exec("set admin telnet timeout 3600")
		COM.exec("set admin web timeout 3600")		
	}
	
	/**
	 * set file-server ftp
	 * @param svr
	 * @param uname
	 * @param passwd
	 * @return
	 */
	def c_set_ftp(svr,uname,passwd){
		CLI.enterMode("")
		assert CLI.expect("set file-server ftp $svr $uname $passwd","error")==false
	}
	
	/**
	 * NFE8080/root_sys->download file ss ss
	 * This operation may take a few minutes, continue?
	 * @param src
	 * @param dst
	 * @return
	 */
	def c_download(src,dst){
		CLI.enterMode("");
		//CLI.exec("download file $src $dst","continue?")
		//def rtn=CLI.exec("y")
		//assert rtn.trim().contains("Error:")==false
		assert CLI.expect(["download file $src $dst","continue?"],"error")==false
		assert CLI.expect("y","error")==false
	}
	
	/**
	 *
	 *	library
	 *	license
	 *	sys                       --- sys <firmware | hardcode | bootrom> <file-name>
	 * @param type
	 * @param filename
	 * @return
	 */
	def c_update(type,filename){
		CLI.enterMode("");
		def rtn=""
		if(type.startsWith("lic")){
			rtn=CLI.exec("update license $filename")
			assert rtn.trim().contains("Error:")==false
			return
		}		
				
		if(type.startsWith("fir")){
			CLI.exec("update sys firmware $filename","y/[n])")
		}else if(type.startsWith("hard")){
			CLI.exec("update sys hardcode $filename","y/[n])")
		}else if(type.startsWith("boo")){
			CLI.exec("update sys bootrom $filename","y/[n])")
		}else{
			println "update missing type:$type!"
			return
		}
		CLI.multiTimeout(100)
		rtn=CLI.exec("y")
		CLI.resetTimeout()
		assert rtn.toLowerCase().trim().contains("error:")==false
	}
	
	/**
	 * reboot
	 * @return
	 */
	def c_reboot(){
		CLI.enterMode("");
		def timeout=CLI.getTimeout();
		CLI.exec("reboot","y/[n])")
		CLI.exec("y","y/[n])")
		CLI.exec("y")
		CLI.multiTimeout(60)
		CLI.disconnect()
		sleep(20)
		CLI.enterMode("config");
	}
	
	/**
	 * update
	 * @return
	 */
	def update(){		
		def folder=has_new_version()
		if(folder==null){
			log.warn "没有新版本"
			return false
		}
		def ftp_svr=get_from_ini("FTP.server");
		def ftp_uname=get_from_ini("FTP.uname");
		def ftp_passwd=get_from_ini("FTP.passwd");
		
		c_set_ftp(ftp_svr,ftp_uname,ftp_passwd)
		
		folder.each{
			if(it.toLowerCase().contains("hardcode"))
				c_update("hardcode",it)
			else if(it.toLowerCase().contains("firmware"))
				c_update("firmware",it)		
			else if(it.toLowerCase().contains("bootrom"))
				c_update("bootrom",it)
			else if(it.toLowerCase().contains("xianhua.li.dat"))
				c_update("license",it)
		}
		c_reboot()		
		return true
		
	}
	
	def static main(args){
		def nfe=new NFE()
		nfe.init(0)
		nfe.c_reboot()
		nfe.CLI.multiTimeout(100)
		println nfe.CLI.login()
		
		//System.out.println(nfe.update())
		
//		def s="a b c d   f\r\npp ff asdn ";
//		def t="bcd"
//		def e="bcf"
//		println law.containLine(s,t)
//		println law.containLine(s,e)
//		COM.disconnect()
	}
}