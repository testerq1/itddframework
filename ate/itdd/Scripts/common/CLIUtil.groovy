package common;
import com.jcraft.jsch.JSchException;
import ate.rm.dev.DeviceImp;
import ate.sandbox.cli.*;
import ate.util.*

/**
 * The Class CLIUtil.
 */
public class CLIUtil extends Expect{	
	public static CLIUtil spawn(String protocol,String ... args ) throws NumberFormatException, JSchException{
		if(protocol.equalsIgnoreCase("telnet")){
			if(args.length==4)
				return new CLIUtil(new TelnetSession(args[0],Integer.parseInt(args[1]),args[2],args[3]));
			else
				return new CLIUtil(new TelnetSession(args[0],Integer.parseInt(args[1])));
		}
		
		if(protocol.equalsIgnoreCase("ssh"))
			return new CLIUtil(new SSHSession(args[0],Integer.parseInt(args[1]),args[2],args[3]));
		
		if(protocol.equalsIgnoreCase("rxtx")){
			return new CLIUtil(new RxTxSession(args[0],Integer.parseInt(args[1])));
		}
		
		return null;
	}

	/** The LOGI n_ nam e_ e. */
	public String LOGIN_NAME_E="rname:";
	
	/** The LOGI n_ passw d_ e. */
	public String LOGIN_PASSWD_E="sword:";

	/** The LOGI n_ prompt. */
	public String LOGIN_PROMPT="]";
	
	/** The LOGI n_ prompt. */
	public String ENTER_MODE_PROMPT=">";
	
	/** The need_start_crlf. */
	public boolean need_start_crlf=true;
	/** The RETUR n_ key. */
	public String RETURN_KEY="exit";
	/** The vt. */
	private VTImp vt;
	
	private String currScreen;
	
	private String current_mode;
	
	private CLIUtil(VTImp vt){
		this.vt=vt;
	}
	/**
	 * Disconnect.
	 */
	public void disconnect(){
		vt.disconnect();
	}
	public void setLoggerOn(boolean b){
		vt.setLoggerOn(b);
	}
	public void setBadCommand(String flag){
		vt.setBadCommand(flag);
	}
	public void setContinue(String s){
		vt.setContinueFlag(s);
	}
	/**
	 * Enter mode.
	 *
	 * @param mode the mode
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean enterMode(String mode)throws Exception{
		log.debug("enter mode "+mode);		
		int cnt=10;
		while(!login()&&cnt!=0){
			disconnect();
			multiTimeout(10*cnt);
			cnt--;
		}
		if(cnt==0){
			log.warn("cant login!");
			current_mode=null;
			return false;
		}
		current_mode=mode;
		setPrompt(ENTER_MODE_PROMPT);
		
		if(mode.length()==0||currScreen.endsWith(ENTER_MODE_PROMPT)){
			return true;
		}
		
		if(null==vt.exec(mode, ENTER_MODE_PROMPT)){
			vt.exec(RETURN_KEY,"");
			return enterMode(mode);
		}
		else
			return true;
	}

	/**
	 * Exec, ɾ��������β��WSP��Ȼ�󷵻�
	 *
	 * @param cmd the cmd
	 * @return the string
	 */
	public String exec(String cmd){
		String rtn=vt.exec(cmd);
		if(rtn!=null){
			String[] ss=rtn.split("\n");
			String value = "";
			for(int i=0;i<ss.length;i++){
					value+=X2X.trimEnd(ss[i]);
			}
			return value;
		}
		return null;
	}
	
	/**
	 * Exec.
	 *
	 * @param cmd the cmd
	 * @param token the token
	 * @return the string
	 */
	public String exec(String cmd,String token){
		int loop=10;
		while(loop-->0){
			this.enterMode(this.current_mode);
			String rtn=vt.exec(cmd, token);
			if(rtn!=null)
				return rtn;
			else 
				log.error("CLIUtil exec failure!");	
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see ate.cli.Expect#expect(java.lang.Object, java.lang.String, boolean, int)
	 */
	public boolean expect(Object cmd,String rtn,boolean exp,int type){
		String line=null;
		if(cmd instanceof String)
			line = this.exec(cmd.toString());
		else if(cmd instanceof ArrayList){
			ArrayList al=(ArrayList)cmd;
			line = this.exec(al.get(0).toString(),al.get(1).toString());
		}
		return match(line,rtn,exp,type);
	}
	
	/**
	 * Gets the timeout.
	 *
	 * @return the timeout
	 */
	public int getTimeout(){
		return vt.getTimeout();
	}
	
	/**
	 * Gets the vt.
	 *
	 * @return the vt
	 */
	public VTImp getVt() {
		return vt;
	}
	
	/**
	 * Instantiates a new cLI util.
	 *
	 * @param dev the dev
	 */
	public void init(DeviceImp dev){
		if(dev.get("LOGIN_NAME")!=null)
			vt.username=dev.get("LOGIN_NAME");
		if(dev.get("LOGIN_PASSWD")!=null)
			vt.password=dev.get("LOGIN_PASSWD");
		if(dev.get("LOGIN_NAME_E")!=null)
			LOGIN_NAME_E=dev.get("LOGIN_NAME_E");
		if(dev.get("LOGIN_PASSWD_E")!=null)
			LOGIN_PASSWD_E=dev.get("LOGIN_PASSWD_E");
		if(dev.get("LOGIN_PROMPT")!=null)
			LOGIN_PROMPT=dev.get("LOGIN_PROMPT");
		if(dev.get("RETURN_KEY")!=null)
			RETURN_KEY=dev.get("RETURN_KEY");
		if(dev.type=="linux")
			vt.setMode(IVT.MODE_UNIX)
//			this.need_start_crlf=true;	
	}
	public void init(HashMap dev){		
		if(dev.get("LOGIN_NAME")!=null)
			vt.username=dev.get("LOGIN_NAME");
		if(dev.get("LOGIN_PASSWD")!=null)
			vt.password=dev.get("LOGIN_PASSWD");
		if(dev.get("LOGIN_NAME_E")!=null)
			LOGIN_NAME_E=dev.get("LOGIN_NAME_E");
		if(dev.get("LOGIN_PASSWD_E")!=null)
			LOGIN_PASSWD_E=dev.get("LOGIN_PASSWD_E");
		if(dev.get("LOGIN_PROMPT")!=null)
			LOGIN_PROMPT=dev.get("LOGIN_PROMPT");
		if(dev.get("RETURN_KEY")!=null)
			RETURN_KEY=dev.get("RETURN_KEY");
		if(dev.get("type")=="linux")
			vt.setMode(IVT.MODE_UNIX)
//			this.need_start_crlf=true;
	}
	/**
	 * ���serverһ��ʼֻ����һ���ո�������ú����������
	 * ����timeout������Ըı�connect�ȴ�ʱ��.
	 *
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean login() throws Exception{
		long deadline=new Date().getTime()+vt.getTimeout();
		if(vt.isConnected()){
			vt.sendLine("");
		}else {
			while (!vt.isConnected()) {
				log.debug("Try to login by "+vt.toString());
				vt.connect();
				if (new Date().getTime() > deadline){
					log.error("login failed!");
					return false;
				}
				Thread.currentThread().sleep(500);
			}
			log.info("Connect successful!")
			if(need_start_crlf)
				vt.sendLine("");
		}
		
		if(!vt.expect("")){
			log.warn("[ATE]login timeout, please check port!");
			return false;
		}
		currScreen=vt.getScreen().trim();
		if(currScreen.endsWith(LOGIN_PROMPT)||currScreen.endsWith(this.ENTER_MODE_PROMPT))
			return true;
		
		int cnt=10;
		if(!currScreen.endsWith(LOGIN_NAME_E)){
			vt.sendLine("");
			while(!vt.expect(LOGIN_NAME_E)&& cnt-->0){
				if(vt.getScreen().endsWith(LOGIN_PROMPT))
					return true;
				vt.sendLine("");
			}
		}
		
		if(cnt>0)
		{
			vt.sendLine(vt.username);
			if(vt.expect(LOGIN_PASSWD_E, 1000))
				vt.sendLine(vt.password);
		}else{
			log.warn("[ATE]login maybe failed, not return username: "+LOGIN_NAME_E);
			log.warn(currScreen);
			//return false;
		}
		return vt.expect(LOGIN_PROMPT, 1000);
	}
	
	/**
	 * Multi timeout.
	 *
	 * @param f the f
	 */
	public void multiTimeout(double f){
		vt.setTimeout((int)f*IVT.TIMEOUT);
	}
	
	/**
	 * Reset timeout.
	 */
	public void resetTimeout(){
		multiTimeout(1);
	}
	
	/**
	 * Sets the mode.
	 *
	 * @param mode the new mode
	 */
	public void setMode(int mode) {
		vt.setMode(mode);
	}

	/**
	 * Sets the prompt.
	 *
	 * @param prompt the new prompt
	 */
	public void setPrompt(String prompt){
		vt.setPrompt(prompt);
	}
	
	/**
	 * Sets the start crlf.
	 *
	 * @param bool the new start crlf
	 */
	public void setStartCRLF(boolean bool){
		this.need_start_crlf=bool;
	} 
	
	/**
	 * Sets the vt.
	 *
	 * @param vt the new vt
	 */
	public void setVt(VTImp vt) {
		this.vt = vt;
	}
}
