package common;
import ate.rm.dev.Host;

class NTools extends Host{
	
	public NTools(){
		super("tools")
	}
	
	def tcp_port_scan(host){
		switchTo("nmap")
		def info=exec("nmap $host")
		return info
	}
	
	def udp_port_scan(host){
		switchTo("nmap")
		def info=exec("nmap -sU $host")
		return info
	} 
	
	def scan_snmp_hole(host){
		switchTo("nmap")
		def passwd=getPath()+"password.txt"
		return expect("nmap -sU --script snmp-brute $host --script-args snmp-brute.communitiesdb=\"$passwd\"","Valid credentials")
	}
	
	def assert_mtu(host, mtu){
		switchTo("nmap")
		return expect("nmap --script path-mtu $host","PMTU == $mtu")
	}
	def get_conn(app){
		switchTo("batch")
		return  exec("findc.bat $app")
	}
	
	def test(){
		println "adssd"
	}
	
	@Override
	public String capabilities(){
		
		return ""
	}
	
	public static void main(String[] args){
		def nmap=new NTools()
//		def info=nmap.scan_port("192.168.1.1")
//		println nmap.match(info,"8888/tcp  open")		
		
		//nmap.get_conn("qq")
		nmap.test()
		
	}
}