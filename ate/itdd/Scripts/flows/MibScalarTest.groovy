import java.lang.reflect.Method;

import ate.testcase.*
import org.testng.annotations.*
import org.testng.TestNG
import org.testng.TestListenerAdapter
import static org.testng.AssertJUnit.*;
import java.lang.reflect.*;
 
 
class MibScalarTest extends MibTestcase {
	@BeforeClass
	public void BeforeClass(){
		super.BeforeClass()
	}
	
	@AfterClass
	public void AfterClass(){
		super.AfterClass()
	}
	@BeforeMethod
	public void BeforeMethod(Method m){
		super.BeforeMethod(m);
	}
	@AfterMethod
	public void AfterMethod(Method m){
		super.AfterMethod(m);
	}
	@Test(dataProvider="Data_Func_Get",groups=["FUNC"])
	final void Test_FUNC_Get(oid,expect_value){
		log.info "${STEP}. 查询 ${oid}，确认值符合预期${expect_value}"
		get.add(SNMP.createVB1(oid,"0"))
		
		log.info "${STEP}.1. GET：${oid}"
		def rtn=sendPDU(get)
		assertTrue "查询返回失败",rtn.isok()
		log.info rtn.getVB(oid)
		assertTrue "查询结果不同于预期值",rtn.getVB(oid).equals(expect_value)
	}
}
