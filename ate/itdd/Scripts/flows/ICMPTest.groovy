import ate.testcase.*
import org.testng.annotations.*
import org.testng.TestNG
import org.testng.TestListenerAdapter
import static org.testng.AssertJUnit.*;
import java.lang.reflect.*;
import java.io.*
import java.net.*
import java.util.*
import java.util.concurrent.*;

import org.savarese.vserv.tcpip.*;
import com.savarese.rocksaw.net.*;
import static com.savarese.rocksaw.net.RawSocket.PF_INET;
import static com.savarese.rocksaw.net.RawSocket.PF_INET6;
import static com.savarese.rocksaw.net.RawSocket.getProtocolByName;
import static org.savarese.vserv.tcpip.ICMPPacket.OFFSET_ICMP_CHECKSUM;
 
class ICMPTest extends Testcase {
	@BeforeClass
	public void BeforeClass(){
		super.BeforeClass()
	}
	
	@AfterClass
	public void AfterClass(){
		super.AfterClass()
	}
	
	@Test(dataProvider="Data_Func_Get",groups=["FUNC"])
	final void Test_FUNC_Get(oid,expect_value){
		log.info "${STEP}. 查询 ${oid}，确认值符合预期${expect_value}"
		get.add(createVB1(oid,"0"))
		
		log.info "${STEP}.1. GET：${oid}"
		def rtn=sendPDU(get)
		assertTrue "查询返回失败",rtn.isok()
		assertTrue "查询结果不同于预期值",rtn.getVB(oid).equals(expect_value)
	}
}
