import ate.testcase.*
import org.testng.annotations.*
import org.testng.TestNG
import org.testng.TestListenerAdapter
import static org.testng.AssertJUnit.*;
import java.lang.reflect.*;
/**
*
*/
public class TestFlows extends Testcase{
	@BeforeClass
	public void BeforeClass(){
		super.BeforeClass();
	}
	@AfterClass
	public void AfterClass(){
		super.AfterClass();
	}
	@BeforeMethod
	public void BeforeMethod(Method m){
		super.BeforeMethod(m);
	}
	@AfterMethod
	public void AfterMethod(Method m){
		super.AfterMethod(m);
	}
	/** 
	*
	*/
	@Test(dataProvider="Data_Func_TestInherit")
	final void Test_Func_TestInherit(para){
		log.info "${STEP}.1. Test_Func_TestInherit ${para}"
		log.info "${STEP}.2. Test_Func_TestInherit ${para}"
	}

}