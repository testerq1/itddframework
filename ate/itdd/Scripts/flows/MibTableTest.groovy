/**
 * 
 */
import ate.testcase.*
import org.testng.annotations.*
import org.testng.TestNG
import org.testng.TestListenerAdapter
import static org.testng.AssertJUnit.*;
import java.lang.reflect.*;

public class MibTableTest extends MibTestcase{

	def max(){
		def hm=new HashMap<String,String>()
		this.writeable_nodes.each{x->
			def al=this.deft_datas.get(x)
			hm.put(x,al.get(al.size()-1))
		}
		return hm
	}
	def min(){
		def hm=new HashMap<String,String>()
		this.writeable_nodes.each{x->
			def al=this.deft_datas.get(x)
			hm.put(x,al.get(0))
		}
		return hm
	}
	
	@BeforeClass
	public void BeforeClass(){
		super.BeforeClass()
	}
	
	@AfterClass
	public void AfterClass(){
		super.AfterClass()
		if(!isDebug){
			delAllAddedLines()
			set.add(createVB2(rowstatus, deftIndex,"6"))
		}
	}
	
	@BeforeMethod
	public void BeforeMethod(Method m){
		def tmp=m.getName()
		super.BeforeMethod()
		if(!m.getName()==tmp){
			if(m.getName().contains("Modify")||
					m.getName().contains("AddRow")){
				set.add(createVB2(rowstatus,deftIndex,"4"))
				this.writeable_nodes.each{x->
					def pos=this.all_nodes.indexOf(x)
					set.add(createVB2(x,deftIndex,deftRow[pos]))
				}
				sendPDU(set)
				set.clear()
			}else{
				log.info "删除已经添加的记录："
				def pu=walk(rowstatus)
				pu.getVBs().each{x->
					set.clear()
					set.add(createVB2(x.getOid().toString()),"int","6")
					sendPDU(set)
					set.clear()					
				}
			}			
		}
	}
	
	@AfterMethod
	public void AfterMethod(){
		super.AfterMethod()
	}

	/**
	*ec: error code
	*/
	@Test(dataProvider="Data_FIT_Modify",groups=["FIT"])
	final void Test_FIT_Modify(row,paras,ec,desc){
		log.info "${STEP}. ${desc}"		
		def index=genIndex(row)		
		paras.entrySet().each{x->
			set.add(createVB2(x.getKey(),index,x.getValue()))	
			get.add(createVB1(x.getKey(),index))
		}
//		log.info "${STEP}.1 保存旧数据"
//		def old=sendPDU(get)
//		assertTrue old.isok(),"指定的行不存在"+index
//		log.info "${STEP}.2 确认旧数据和传入的行数据相同"
//		paras.entrySet().each{x->
//			def pos=this.all_nodes.indexOf(x.getKey())
//			assertTrue old.contains(x.getKey(),row[pos]),"指定的列值不正确"+x		
//		}
		
		log.info "${STEP}.1. 修改数据："+paras		
		def rtn=sendPDU(set)
		if(ec=="0"){
			assertTrue rtn.isok(),"修改返回失败"
			assertTrue rtn.vbsEquals(set),"查询结果不同于设置值"
		}else if(ec=="false"){
			assertTrue rtn.getErrorStatus()!=0,"修改返回成功"
		}else{
			assertTrue rtn.errorStatusIs(ec),"修改返回值不符合预期"+ec
		}
		
		log.info "${STEP}.2. 查询确认修改结果符合预期：ec="+ec
		rtn=sendPDU(get)
		assertTrue rtn.isok()
		if(ec=="0"){			
			paras.entrySet().each{x->
				assertTrue rtn.contains(x.getKey(),x.getValue()),"查询得到的值不同于修改值"
			}
		}else{
			paras.entrySet().each{x->
				def pos=this.all_nodes.indexOf(x.getKey())
				assertTrue rtn.contains(x.getKey(),row[pos]),"查询确认指定值没有被修改"
			}
		}
		
		log.info "${STEP}.3. 恢复环境："
		if(ec=="0"){
			set.clear()
			paras.entrySet().each{x->
				def pos=this.all_nodes.indexOf(x.getKey())
				set.add(createVB2(x.getKey(),index,row[pos]))		
			}		
			assertTrue sendPDU(set).isok(),"恢复环境失败"+index
		}else{
			log.info "没有修改，无需恢复"
		}			
	}
	
	/**
	*
	*/
	@Test(dataProvider="Data_FIT_AddRow",groups=["FIT"])
	final void Test_FIT_AddRow(indexParas,paras,ec,desc){
		log.info "${STEP}. ${desc}"
		log.info "${STEP}.1. 添加一行数据"		
		def index=genIndex(indexParas)
		set.add(createVB2(rowstatus,index,"4"))
		this.all_nodes.each{x->
			if(paras.get(x)!=null)
				set.add(createVB2(x,index,paras.get(x)))
		}
		def rtn=sendPDU(set)
		
		log.info "${STEP}.2. 确认返回的错误码符合预期：ec="+ec
		if(ec=="false"){
			assertTrue rtn.getErrorStatus()!=0;
		}else if(ec=="0"){
			assertTrue rtn.isok()			
		}else{
			assertTrue rtn.errorStatusIs(ec)
		}
		
		log.info "${STEP}.3. 确认添加结果：ec="+ec
		get.add(createVB2(rowstatus,index))
		rtn=senPDU(get)
		if(ec=="0"){
			assertTrue rtn.isok(),"没有添加"+index
		}else{
			assertFalse rtn.isok(),"该行被添加"+index
		}
		
		log.info "${STEP}.4. 恢复环境："
		set.clear()
		if(ec=="0"){
			set.add(createVB2(rowstatus,index,"6"))
			assertTrue sendPDU(set).isok(),"恢复环境失败，该行删除不了："+index
		}		
	}
	
	/**
	*
	*/
	@Test(dataProvider="Data_FUNC_AddRow",groups=["FUNC"])
	final void Test_FUNC_AddRow(row,desc){
		log.info "${STEP}. ${desc}"
		log.info "${STEP}.1. 添加一行数据"+row
		def index=genIndex(row)
		set.add(createVB2(rowstatus,index,"4"))
		this.writeable_nodes.each{x->
			def pos=this.all_nodes.indexOf(x)
			if(row[pos]!=null){
				set.add(createVB2(x,index,row[pos]))
				get.add(createVB1(x,index))
			}
		}				
		assertTrue sendPDU(set).isok(),"行添加失败"
		
		log.info "${STEP}.2. 查询确认已经添加："
		def rtn=sendPDU(get)
		assertTrue rtn.isok(),"查询返回失败"
		assertTrue rtn.vbsEquals(set),"查询结果不同于设置值"
		
		log.info "${STEP}.3. 删除行数据："
		set.clear()
		set.add(createVB2(rowstatus,index,"6"))
		assertTrue sendPDU(set).isok(),"删除数据失败"+index
		
	}
	@Test(dataProvider="Data_FUNC_Modify",groups=["FUNC"])
	final void Test_FUNC_Modify(row,paras,desc){
		Test_FIT_Modify(row,paras,"0",desc)		
	}
	
	/**
	*
	*/
	@Test(dataProvider="Data_FIT_AddRows",groups=["FIT"])
	final void Test_FIT_AddRows(rows,ecs,desc){
		log.info "${STEP}. ${desc}"		
		
		def cnt=0
		for(int i=0;i<rows.size();i++){
			log.info "${STEP}.${cnt} 添加行,预期结果："+ecs[i]
			set.clear()
			def index=genIndex(rows[i])
			set.add(createVB2(rowstatus,index,"4"))
			this.writeable_nodes.each{x->
				def pos=this.all_nodes.indexOf(x)
				if(rows[i][pos]!=null)
					set.add(createVB2(x,index,rows[i][pos]))
			}
			def rtn=sendPDU(set)
			if(ecs[i]=="false"){
				assertTrue rtn.getErrorStatus()!=0,"添加成功"+index
			}else if(ecs[i]=="0"){
				assertTrue rtn.isok(),"添加失败"+index
			}else{
				assertTrue rtn.errorStatusIs(ecs[i]),"添加结果不符合预期"+rtn.getErrorStatus()
			}
			cnt++
		}
		
		log.info "${STEP}.${cnt} 恢复环境："
		for(int i=0;i<ecs.size();i++){			
			if(ecs[i]=="0"){
				set.clear()
				def index=genIndex(rows[i])
				set.add(createVB2(rowstatus,index,"6"))
				assertTrue sendPdu(set).isok(),"恢复环境删除该行失败"+index
			}
		}
		
	}
	/**
	*
	*/
	@Test(dataProvider="Data_FUNC_AddRows",groups=["FUNC"])
	final void Test_FUNC_AddRows(rows,desc){
		def ecs
		for(int i=0;i=rows.size();i++){
			ecs[i]="0"
		}
		Test_FIT_AddRows(rows,ecs,desc)
	}
	/**
	*
	*/
	@Test(dataProvider="Data_FUNC_DelRefLine",groups=["FUNC"])
	final void Test_FUNC_DelRefLine(tablename,datas,errorcode,desc){
		log.info "${STEP}. ${desc}"
		log.info "---Test_FUNC_DelRefLine---"
		log.info datas
		log.info "${STEP}.1 "
	}
	/**
	*
	*/
	@Test(dataProvider="Data_PERF_Add",groups=["PERF"])
	final void Test_PERF_Add(row,ec){
		log.info "${STEP}. 连续加同一行5次"
		log.info "${STEP}.1 "
	}
	/**
	*
	*/
	@Test(dataProvider="Data_PERF_Del",groups=["PERF"])
	final void Test_PERF_Del(row,ec){
		log.info "${STEP}. 连续删除同一行5次"
		log.info "${STEP}.1 "
	}
	@Test(dataProvider="Data_PERF_Full",groups=["PERF"])
	final void Test_PERF_Full(rows,ec){
		log.info "${STEP}. 满配测试"
		log.info "${STEP}.1 "
		
	}
}