-- TTOOL-MANAGEMENT-MIB.mib	1.0 11/06/6
-- 
-- Copyright 2011, 2011 Ravi huang. All rights reserved.
-- This software is the proprietary information of ravi haung.
-- Use is subject to license terms.
--
-- The TTOOL-MANAGEMENT-MIB Module
--
-- See ttoolManagementMIB MODULE-IDENTITY for a description overview.
--

TTOOL-MANAGEMENT-MIB DEFINITIONS ::= BEGIN

IMPORTS
    MODULE-IDENTITY, OBJECT-TYPE, NOTIFICATION-TYPE, Counter32, Gauge32, IpAddress,
	Integer32, Counter64, TimeTicks, enterprises
        FROM SNMPv2-SMI
    DisplayString, TEXTUAL-CONVENTION, RowStatus, RowPointer, DateAndTime
        FROM SNMPv2-TC
    MODULE-COMPLIANCE, OBJECT-GROUP, NOTIFICATION-GROUP
        FROM SNMPv2-CONF;

-- Module Identity
------------------

ttMgtMIB MODULE-IDENTITY 
    LAST-UPDATED "201106062300Z"
    -- Format is "YYYYMMDDhhmmZ"
    ORGANIZATION "private"
    CONTACT-INFO "ravi huang@gmail.com"            
    DESCRIPTION
            "This module defines the MIB that provides access to the TTool monitoring data."
    REVISION     "201106062300Z"
    -- Format is "YYYYMMDDhhmmZ"
    DESCRIPTION
            "
            TTOOL-MANAGEMENT-MIB - Final Release 1.0
            "

    ::= { ravi 1 }


-- Enterprise OIDs
------------------

--        internet          OBJECT IDENTIFIER ::= { iso(1) org(3) dod(6) 1 }
--        private           OBJECT IDENTIFIER ::= { internet 4 }
--        enterprises       OBJECT IDENTIFIER ::= { private 1 }
	 ravi	  	  OBJECT IDENTIFIER ::= { enterprises 65535 }
	 ttMgtObjects		  OBJECT IDENTIFIER ::= { ttMgtMIB 1 }
	 ttMgtNotifications OBJECT IDENTIFIER ::= { ttMgtMIB 2 }     
   ttMgtActions		  OBJECT IDENTIFIER ::= { ttMgtObjects 1 }
   
   ttSaveConfig OBJECT-TYPE
       SYNTAX      Integer32 {null(0),save(1) }
       MAX-ACCESS  read-write
       STATUS      current
       DESCRIPTION
               ""
       ::= { ttMgtActions 1 }

   ttSysReboot OBJECT-TYPE
       SYNTAX      DisplayString
       MAX-ACCESS  read-write
       STATUS      current
       DESCRIPTION
               ""
       ::= { ttMgtActions 2 }
   ttSysReloadLogConfig OBJECT-TYPE
       SYNTAX      Integer32  {null(0),reload(1) }
       MAX-ACCESS  read-write
       STATUS      current
       DESCRIPTION
               ""
       ::= { ttMgtActions 3 }
       
   ttSysTaskTable OBJECT-TYPE
       SYNTAX     SEQUENCE OF TtSysTaskEntry
       MAX-ACCESS not-accessible
       STATUS     current
       DESCRIPTION
               "The (conceptual) table listing the capabilities of
               the local SNMP application acting as a command
               responder with respect to various MIB modules.
               SNMP entities having dynamically-configurable support
               of MIB modules will have a dynamically-varying number
               of conceptual rows."
       ::= { ttMgtObjects 2 }


   ttSysTaskEntry OBJECT-TYPE
       SYNTAX     TtSysTaskEntry
       MAX-ACCESS not-accessible
       STATUS     current
       DESCRIPTION
               "An entry (conceptual row) in the ttSysSvcTable."
       INDEX      { ttSysTaskIndex }
       ::= { ttSysTaskTable 1 }

   TtSysTaskEntry ::= SEQUENCE {
       ttSysTaskIndex     Integer32,
       ttSysTaskType        DisplayString,    
       ttSysTaskIsLocal        Integer32,
       ttSysTaskDesc     DisplayString,   
       ttSysTaskParas     DisplayString,
       ttSysTaskSendResultTo   DisplayString,
       ttSysTaskCommit    Integer32
   }

   ttSysTaskIndex OBJECT-TYPE
       SYNTAX     Integer32 (0..255)
       MAX-ACCESS not-accessible
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysTaskEntry 1 }

   ttSysTaskType OBJECT-TYPE
       SYNTAX     DisplayString
       MAX-ACCESS read-only
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysTaskEntry 2 }
                       
   ttSysTaskIsLocal OBJECT-TYPE
       SYNTAX     Integer32 {false(0),true(1)}
       MAX-ACCESS read-write
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysTaskEntry 3 }       
                        
   ttSysTaskDesc OBJECT-TYPE
       SYNTAX     DisplayString
       MAX-ACCESS read-only
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysTaskEntry 4 }
       
   ttSysTaskParas OBJECT-TYPE
       SYNTAX     DisplayString
       MAX-ACCESS read-write
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysTaskEntry 5 }   
           
   ttSysTaskSendResultTo OBJECT-TYPE
       SYNTAX     DisplayString
       MAX-ACCESS read-write
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysTaskEntry 6 }    
   ttSysTaskCommit OBJECT-TYPE
       SYNTAX     Integer32 {suspend(0),commit(1)}
       MAX-ACCESS read-write
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysTaskEntry 7 }                	        
    	 
  ttSysAllRunTaskTable OBJECT-TYPE
       SYNTAX     SEQUENCE OF TtSysAllRunTaskEntry
       MAX-ACCESS not-accessible
       STATUS     current
       DESCRIPTION
               "The (conceptual) table listing the capabilities of
               the local SNMP application acting as a command
               responder with respect to various MIB modules.
               SNMP entities having dynamically-configurable support
               of MIB modules will have a dynamically-varying number
               of conceptual rows."
       ::= { ttMgtObjects 3 }         
       
  	ttSysAllRunTaskEntry OBJECT-TYPE
       SYNTAX     TtSysAllRunTaskEntry
       MAX-ACCESS not-accessible
       STATUS     current
       DESCRIPTION
               "An entry (conceptual row) in the ttSysAllRunTaskTable."
       INDEX      { ttSysTaskIndex }
       ::= { ttSysAllRunTaskTable 1 }

   TtSysAllRunTaskEntry ::= SEQUENCE {
       ttSysAllRunTaskIndex     Integer32,      
       ttSysAllRunTaskType     DisplayString,       
       ttSysAllRunTaskParas     DisplayString,
       ttSysAllRunTaskCronExpression   DisplayString,
       ttSysAllRunTaskSendResultTo     DisplayString,
       ttSysAllRunTaskCommit    Integer32
   }
   ttSysAllRunTaskIndex OBJECT-TYPE
       SYNTAX     Integer32 (0..255)
       MAX-ACCESS not-accessible
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysAllRunTaskEntry 1 }

   ttSysAllRunTaskType OBJECT-TYPE
       SYNTAX     DisplayString
       MAX-ACCESS read-create
       STATUS     current
       DESCRIPTION
               "   runscript
                    heartbeat                    
                "
       ::= { ttSysAllRunTaskEntry 2 }       
   
   ttSysAllRunTaskParas OBJECT-TYPE
       SYNTAX     DisplayString
       MAX-ACCESS read-create
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysAllRunTaskEntry 3 }   
           
   ttSysAllRunTaskCronExpression OBJECT-TYPE
       SYNTAX     DisplayString
       MAX-ACCESS read-create
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysAllRunTaskEntry 4 }   
        
   ttSysAllRunTaskSendResultTo OBJECT-TYPE
       SYNTAX     DisplayString
       MAX-ACCESS read-create
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysAllRunTaskEntry 5 }          
   ttSysAllRunTaskCommit OBJECT-TYPE
       SYNTAX     Integer32 {suspend(0),commit(1)}
       MAX-ACCESS read-write
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysAllRunTaskEntry 6 }   
   ttSysSvcTable OBJECT-TYPE
       SYNTAX     SEQUENCE OF TtSysSvcEntry
       MAX-ACCESS not-accessible
       STATUS     current
       DESCRIPTION
               "The (conceptual) table listing the capabilities of
               the local SNMP application acting as a command
               responder with respect to various MIB modules.
               SNMP entities having dynamically-configurable support
               of MIB modules will have a dynamically-varying number
               of conceptual rows."
       ::= { ttMgtObjects 4 }


   ttSysSvcEntry OBJECT-TYPE
       SYNTAX     TtSysSvcEntry
       MAX-ACCESS not-accessible
       STATUS     current
       DESCRIPTION
               "An entry (conceptual row) in the ttSysSvcTable."
       INDEX      { ttSysSvcIndex }
       ::= { ttSysSvcTable 1 }

   TtSysSvcEntry ::= SEQUENCE {
       ttSysSvcIndex     Integer32,
       ttSysSvcName        DisplayString,       
       ttSysSvcDesc     DisplayString,
       ttSysSvcAction     Integer32
   }

   ttSysSvcIndex OBJECT-TYPE
       SYNTAX     Integer32 (0..255)
       MAX-ACCESS not-accessible
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysSvcEntry 1 }
       
   ttSysSvcName OBJECT-TYPE
       SYNTAX     DisplayString
       MAX-ACCESS read-only
       STATUS     current
       DESCRIPTION
               "must be unique value"
       ::= { ttSysSvcEntry 2 }
       
   ttSysSvcDesc OBJECT-TYPE
       SYNTAX     DisplayString 
       MAX-ACCESS read-only
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysSvcEntry 3 } 
              
   ttSysSvcAction OBJECT-TYPE
       SYNTAX     Integer32 {stop(0), start(1)} 
       MAX-ACCESS read-write
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysSvcEntry 4 }    
       
   ttSysClientTable OBJECT-TYPE
       SYNTAX     SEQUENCE OF TtSysClientEntry
       MAX-ACCESS not-accessible
       STATUS     current
       DESCRIPTION
               "The (conceptual) table listing the capabilities of
               the local SNMP application acting as a command
               responder with respect to various MIB modules.
               SNMP entities having dynamically-configurable support
               of MIB modules will have a dynamically-varying number
               of conceptual rows."
       ::= { ttMgtObjects 5 }


   ttSysClientEntry OBJECT-TYPE
       SYNTAX     TtSysClientEntry
       MAX-ACCESS not-accessible
       STATUS     current
       DESCRIPTION
               "An entry (conceptual row) in the ttSysSvcTable."
       INDEX      { ttSysClientIndex }
       ::= { ttSysClientTable 1 }

   TtSysClientEntry ::= SEQUENCE {
       ttSysClientIndex     Integer32,
       ttSysClientName        DisplayString
   }

   ttSysClientIndex OBJECT-TYPE
       SYNTAX     Integer32 (0..255)
       MAX-ACCESS not-accessible
       STATUS     current
       DESCRIPTION
               ""
       ::= { ttSysClientEntry 1 }
       
   ttSysClientName OBJECT-TYPE
       SYNTAX     DisplayString
       MAX-ACCESS read-only
       STATUS     current
       DESCRIPTION
               "must be unique value"
       ::= { ttSysClientEntry 2 }
       
                     
	 coldStart NOTIFICATION-TYPE
       STATUS  current
       DESCRIPTION
               "A coldStart trap signifies that the SNMP entity,
               supporting a notification originator application, is
               reinitializing itself and that its configuration may
               have been altered."
       ::= { ttMgtNotifications 1 }

   warmStart NOTIFICATION-TYPE
       STATUS  current
       DESCRIPTION
               "A warmStart trap signifies that the SNMP entity,
               supporting a notification originator application,
               is reinitializing itself such that its configuration
               is unaltered."
       ::= { ttMgtNotifications 2 }
END