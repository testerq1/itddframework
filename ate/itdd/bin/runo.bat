@echo off
setlocal ENABLEDELAYEDEXPANSION

REM init variables
set CMD_LINE_ARGS=

REM Get standard environment variables
set PRGDIR=%~dp0

pushd %PRGDIR%..
set TTOOL_HOME=%CD%
popd

set CMD_LINE_ARGS=%*

IF [%1]==[] set CMD_LINE_ARGS=-h

java -DTTOOL_HOME=%TTOOL_HOME% -jar %TTOOL_HOME%\bin\start.jar %CMD_LINE_ARGS%

set ERROR_CODE=%ERRORLEVEL%
endlocal & set ERROR_CODE=%ERROR_CODE%
exit /B %ERROR_CODE%
