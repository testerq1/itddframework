@echo off
setlocal ENABLEDELAYEDEXPANSION

REM Get standard environment variables
set PRGDIR=%~dp0

pushd %PRGDIR%..
set TTOOL_HOME=%CD%
popd

java -DTTOOL_HOME=%TTOOL_HOME% -jar %TTOOL_HOME%\bin\start.jar

set ERROR_CODE=%ERRORLEVEL%
endlocal & set ERROR_CODE=%ERROR_CODE%
exit /B %ERROR_CODE%
