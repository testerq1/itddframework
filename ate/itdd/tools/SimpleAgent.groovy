import org.snmp4j.CommandResponderEvent;
import org.snmp4j.PDU;
import org.snmp4j.*;
import com.ravi.snmp.*

/**
 * addVB类型
 *  int
	rowstatus
	counter32||uinteger32||gauge32||opaque
	octet||displaystring
	hex
	bit
	null
	object
	timeticks
	ip
 */
class AnAgent extends SnmpAgent{	
	public AnAgent(String v){
		super(v)
	}
	
	public void processRequestEvent(CommandResponderEvent event){
		PDU pdu = event.getPDU();
		if(pdu==null)
			return;
		if(getOid(pdu,0).startsWith("1.3.6.1.4.1.12.1.2.1.2")){
			addVB("1.3.6.1.4.1.12.1.2.1.2.1", "octet", "中华人民共和国");
			addVB("1.3.6.1.4.1.12.1.2.1.2.2", "octet", "中华人民共和国1");
			addVB("1.3.6.1.4.1.12.1.2.1.2.3", "octet", "中华人民共和国2");
			addVB("1.3.6.1.4.1.12.1.2.1.2.4", "octet", "中华人民共和国3");
		}else{
			addVB("1.3.6.1.4.1.12.1.1.0", "uinteger32", "1000");
		}
		super.processRequestEvent(event);
	}
}
AnAgent agt=new AnAgent("v2c");
agt.setLocalAddress("udp:172.16.5.111/161");
agt.listen();