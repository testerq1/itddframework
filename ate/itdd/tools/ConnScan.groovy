import com.ravi.actions.*
import com.ravi.taskman.*

/**
 * F:\java\MCTool>bin\rund.bat tools\ConnScan.groovy iexplore "0/20 * * * * ?"
 * F:\java\MCTool>bin\rund.bat tools\ConnScan.groovy iexplore
 * Ĭ��ÿ2��ִ��1��
 * @author Administrator
 *
 */
public class ConScan implements ICronRunner{
	Win win
	String cron="0/2 * * * * ?"
	String app="qq"
	def last
	File output
	public ConScan(String[] args){
		win = new Win()
		if(args.size()>0)
			this.app=args[0]
		if(args.size()>1)
			this.cron=args[1]	
			
		last=new String[0]
		output=new File("conn_diff_${app}.log")
		if(args.size()>2&&args[2]=="overwrite"){}
			output.write("")
	}	
	public String cron(){
		return cron
	}
	public void run(){
		def rtn=win.getConections(app)
		def arr=rtn.split("\n")
		String diff=DiffUtil.diff(last,arr);
		last=arr;
		if(diff.trim().length()>0){
			output.append("["+new Date()+"]\n")
			output.append(diff+"\n")
		}
	}
	
	public static void main(String[] args){		
		CronPub cron=CronPub.createInstance()
		cron.startCron(new ConScan(args))
	}
}