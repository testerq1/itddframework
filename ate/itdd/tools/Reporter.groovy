import org.apache.mina.core.buffer.*;
import org.jnetpcap.packet.*;
import org.jnetpcap.protocol.*;
import org.jnetpcap.protocol.lan.*;
import com.ravi.*
import com.ravi.pcap.*;
import com.ravi.util.*;

public class MyReporter extends Reporter{
	public MyReporter(){
		super("172.16.5.111",13362);
	}
	protected Object parseMessage(IoBuffer message){
		try {
			JPacket packet= new JMemoryPacket(n2r,message.array());			
			if(packet.hasHeader(n2r)){			
				log.info(packet.toString());
			}else
				log.error("unknown packet:"+packet);
		} catch (Exception e) {
			//e.printStackTrace();
			byte[] bs=message.array();
			log.error("Length:"+bs.size()+"--"+X2X.bs2chs(message.array(), " "));
		}
		return null;
	}
	
	public static void main(String[] args){
		new MyReporter().start();
	}
}